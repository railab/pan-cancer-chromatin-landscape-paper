# going to test out a development strategy where develop and test code here in code chunks
# then copy-paste those code chunks to functions in the main scripts for reproducible analysis


if __name__ == '__main__':

	#%%
	# main.py imports
	import json
	import sys
	import glob
	import os
	import re
	import time
	from colorama import init 
	from termcolor import colored
	import colored_traceback

	import broadpeak_pipeline as broadpipe
	import broadpeak_helper_functions as hfnxns
	import multiprocessing as mp
	import matplotlib
	matplotlib.use('agg')

	# to quick print: hfnxns.quick_dumps(test_data_dict, 'test_data_dict')



	#%%
	#function definitions
	# from importlib import reload
	# reload(broadpipe)
	from broadpeak_pipeline import *

	# # reload updating imports
	# reload(hfnxns)
	# reload(broadpipe)


	# main.py setup

	# settings_file = 'home_pipeline_settings.json'
	settings_file = 'test_pipeline_settings.json'

	with open(settings_file, 'r') as readfile:
		settings = json.load(readfile)
	if settings.get('print_timing') is True: 
		start_setup = time.time()


	# sets verbosity settings (if you want to have more or less printed from different steps)
	pipeline_steps = ['set_absolute_filepaths', 'setup_input_files', \
		'set_default_settings', 'check_pipe_settings', \
		'merge_size_comparison_scatter', \
		'specific_group_analysis', 'quantile_plot']
	verbosity_dict = broadpipe.setup_verbosity(pipeline_steps, \
		settings.get('verbose'), settings.get('specific_verbosity'))

	broadpipe.set_absolute_filepaths(settings, \
		verbose=verbosity_dict.get('set_absolute_filepaths'))


	test_data_dict, control_data_dict = \
		broadpipe.find_organize_input_files(settings, \
			verbose = verbosity_dict.get('setup_input_files'))


	required_settings = {
		'core_analysis': {
			'general_settings_list': \
				['merge_len', 'sample_common_perc', 'type_common_perc'],
			'specific_settings_list': \
				['file_pattern', 'title_pattern', 'len_or_dens', \
					'split_type', 'greater_or_less', 'cutoff', 'make_non'],
			'no_default_list': \
				['file_pattern', 'title_pattern', 'len_or_dens', \
					'greater_or_less', 'cutoff']
		},
		'data_info': {
			'required_data_info': \
				['chr_index', 'start_index', 'end_index'],
			'optional_data_info': \
				['density_index']
		},
	}

	colors_required = {
				'len_change_analysis': 			'not_plot', 
				'len_dens_change': 				'not_plot', 
				'plot_len_quantiles': 			'standard_colors', 
				'plot_dens_quantiles': 			'standard_colors', 
				'split_plot': 					'standard_colors', 
				'common_table_plot': 			'standard_colors', 
				'core_path_analysis': 			'standard_colors', 
				'peak_count_bar_plot': 			'standard_colors', 
				'len_change_cancer_plots': 		'standard_colors', 
				'broad_sharp_cancer_plots': 	'standard_colors', 
				'merge_size_scatter': 			'colors', 
				'path_dots': 					'colors', 
				'len_change_kde': 				'colors', 
				# 'kde_peak_dens_plot': 		'not_used', 
				# 'upsetr_plot': 				'not_used',
		}

	built_in_sample_handling = {
				'merge_size_scatter': {'drop_control': True},
				'merge_beds': {},
				'plot_len_quantiles': {},
				'plot_dens_quantiles': {},
				'split_beds': {},
				'split_plot': {},
				'common_table_plot': {'drop_control': True},
				# 'common_table_subplot': {},
				'common_table_subplot': \
					{'fuse_control': True},
				'sample_common_perc': \
					{'output_common_format': True, 'fuse_control': True},
				'type_common_perc': \
					{'input_common_format': True, 'all_common': True, \
						'fuse_control': True},
				'match_tss_gene_names_input': \
					{'input_common_format': True, 'all_common': True, \
						'fuse_control': True},
				'match_tss_gene_names_output': \
					{'output_common_format': True, \
						'input_common_format': True, 'all_common': True, \
							'fuse_control': True, \
							'saving_in_load_dir': True},
				'core_path_analysis': \
					{'input_common_format': True, \
						'fuse_control': True, 'all_common': True,},
				'peak_count_bar_plot': \
					{},
				'comparison_common_beds': {'drop_test': True, 'input_common_format': False,
					'output_common_format': True},
				'comparison_unique_1': \
					{'input_common_format': True, 'all_common': False, \
						'fuse_control': False},
				'comparison_unique_2': \
					{'output_common_format': True, \
						'input_common_format': True, 'all_common': False, \
							'fuse_control': False, \
							'saving_in_load_dir': False},
				'compar_match_tss_gene_names_input': \
					{'input_common_format': True, 'all_common': False, \
						'fuse_control': False},
				'compar_match_tss_gene_names_output': \
					{'output_common_format': True, \
						'input_common_format': True, 'all_common': False, \
							'fuse_control': False, \
							'saving_in_load_dir': True},
				'compar_path_analysis': \
					{'input_common_format': True, \
						'fuse_control': False, 'all_common': False,},
				'get_peak_size_merged': {'input_common_format': False},
				'get_peak_size_unique_common': {'input_common_format': True, \
					'output_common_format': True},
				'peak_size_change_1': {'input_common_format': True, \
					'output_common_format': True, 'use_input_groups': True},
				'peak_size_change_2': {'input_common_format': True, \
					'use_input_groups': True},
				'compar_kde_size_change': {'input_common_format': True, \
					'use_input_groups': True},
				'compar_cancer_plot': {'input_common_format': True, \
					'drop_control': True},
				'len_change_common_beds': {'input_common_format': False, \
					'output_common_format': True},
				'len_change_compar_0_common_beds': {'input_common_format': False, \
					'output_common_format': True},
				'len_pipeline_len_filter': {'input_common_format': True, \
					'output_common_format': True, 'use_input_groups': True},
				'len_pipeline_no_change_len_filter': {'input_common_format': True, \
					'use_input_groups': True, 'saving_in_load_dir': True},
				'len_change_match_tss_gene_names_input': \
					{'input_common_format': True, 'all_common': False, \
					'use_input_groups': True},
				'len_change_match_tss_gene_names_output': \
					{'output_common_format': True, \
						'input_common_format': True, 'all_common': False, \
							'saving_in_load_dir': True, \
							'use_input_groups': True},
				'len_change_path_analysis': \
					{'input_common_format': True, 'all_common': False, \
					'use_input_groups': True},
				'len_change_cancer_plot': {'input_common_format': True, \
					'drop_control': True, 'use_input_groups': True},
				# 'len_change_unique_1': \
				# 	{'input_common_format': True, 'all_common': False, \
				# 		'fuse_control': False},
				# 'len_change_unique_2': \
				# 	{'output_common_format': True, \
				# 		'input_common_format': True, 'all_common': False, \
				# 			'fuse_control': False, \
				# 			'saving_in_load_dir': False},
		}
											
	default_step_file_settings = {
								'input_common_format': None, 
								'output_common_format': None, 
								'all_common': None, 
								'fuse_test': False, 
								'fuse_control': False,
								'drop_test': False, 
								'drop_control': False, 
								'saving_in_load_dir': False, 
								'loading_fused': None
							}


	if settings.get('cpu_cores') is None:
		settings['cpu_cores'] = mp.cpu_count()
	# settings['workers'] = mp.Pool(settings.get('cpu_cores'))
	if settings.get('cpu_cores') == 1:
		settings['workers'] = None
	else:
		settings['workers'] = hfnxns.pool_wrapper(settings['cpu_cores'])
	# print('cpu count: ', settings.get('cpu_cores'))






	broadpipe.set_and_check_default_settings(settings, \
			required_settings, control_data_dict, test_data_dict, \
			colors_required, built_in_sample_handling, \
			default_step_file_settings, \
			verbose=verbosity_dict.get('set_default_settings')
											)

	settings['verbosity_dict'] = verbosity_dict

	if settings.get('print_timing') is True: 
		print('Setup time: {}'.format(round(time.time()-start_setup,2)))


	broadpipe.main_pipeline(settings, test_data_dict, control_data_dict)


	# need to update for script - final main.py setup

	# need to update
	# this_mark = "E"

	"""
	for this_mark in test_data_dict:
		loop_test_data = test_data_dict.get(this_mark)
		loop_control_data = control_data_dict.get(this_mark)

		settings['data_loading_info']['test_dict'] = loop_test_data
		settings['data_loading_info']['control_dict'] = loop_control_data
		settings['data_loading_info']['which_mark'] = this_mark
		settings['main_pipeline_dirs']['loop_top_save_dir'] = \
			os.path.join(settings.get('main_pipeline_dirs')\
				.get('top_save_dir'), this_mark)

		broadpipe.make_save_dirs(settings)

		broadpipe.main_pipeline(settings)
	"""




	# gonna set up the comparison pipeline. i need to get a thorough understand of 
		# what happens in it, and then i need to implement all of that in a more
		# organized fashion that i did in the past...


	# broadpipe.core_pipeline(settings)


	# raise


	# def specific_comparison_analysis(settings, compare_loop):
	# 	pass

	# def comparison_pipeline(settings):
		# def setup_comp_pipe_paths(settings, comparison, loop_comparison_saves, \
		# 	which_lists, compare_merge_dict):
		# 	def add_unique_common_paths(loop_comparison_saves, top_compare_dir):
		# 		unique_common = os.path.join(top_compare_dir, \
		# 			'unique_common_beds')
		# 		loop_comparison_saves['top_compare_dir']['unique_common'] = {
		# 			'path': unique_common,
		# 			'normal_common_beds': {'path': \
		# 				os.path.join(unique_common, 'normal_common_beds')},
		# 			'unique_peak_beds': {'path': \
		# 				os.path.join(unique_common, 'unique_peak_beds')},
		# 		}

		# 		control_common_dir = os.path.join(\
		# 			unique_common, 'normal_common_beds')
		# 		loop_comparison_saves['top_compare_dir']['unique_common']\
		# 			['normal_common_beds'] = {
		# 			'path': control_common_dir,
		# 			'group1_normal_common': {'path': \
		# 				os.path.join(unique_common, 'group1_normal_common')},
		# 			'group2_normal_common': {'path': \
		# 				os.path.join(unique_common, 'group2_normal_common')},
		# 		}

		# 		unique_peak_dir = os.path.join(unique_common, \
		# 			'unique_peak_beds')
		# 		loop_comparison_saves['top_compare_dir']['unique_common']\
		# 			['unique_peak_beds'] = {
		# 			'path': unique_peak_dir,
		# 			'g1_control_minus_g2_control': {'path': \
		# 				os.path.join(unique_common, \
		# 					'g1_control_minus_g2_control')},
		# 			'g1_test_minus_g2_test': {'path': \
		# 				os.path.join(unique_common, \
		# 					'g1_test_minus_g2_test')},
		# 			'g2_control_minus_g1_control': {'path': \
		# 				os.path.join(unique_common, \
		# 					'g2_control_minus_g1_control')},
		# 			'g2_test_minus_g1_test': {'path': \
		# 				os.path.join(unique_common, \
		# 					'g2_test_minus_g1_test')},
		# 		}

		# 	def add_peak_size_paths(loop_comparison_saves, top_compare_dir):
		# 		peak_sizes = os.path.join(top_compare_dir, 'peak_size_dir')
		# 		loop_comparison_saves['top_compare_dir']['peak_sizes'] = {
		# 			'path': peak_sizes,
		# 			'unique_g1_control_vs_g2': {'path': \
		# 				os.path.join(peak_sizes, \
		# 					'unique_g1_control_vs_g2')},
		# 			'unique_g2_control_vs_g1': {'path': \
		# 				os.path.join(peak_sizes, \
		# 					'unique_g2_control_vs_g1')},
		# 			'unique_g1_test_vs_g2': {'path': \
		# 				os.path.join(peak_sizes, \
		# 					'unique_g1_test_vs_g2')},
		# 			'unique_g2_test_vs_g1': {'path': \
		# 				os.path.join(peak_sizes, \
		# 					'unique_g2_test_vs_g1')},
		# 		}

		# 	compare_saves = {}

		# 	peak_size_dict = settings.get('save_dirs')\
		# 		.get('comparison_pipeline_dirs').get('path')
		# 	# .get('top_compare_dir')\
		# 	# peak_size_dict = comparison.get('top_compare_dir')\
		# 	# 	.get('peak_sizes')
		# 	# for dir_key in [this_key for this_key in \
		# 	# 	peak_size_dict if this_key is not 'path']:
		# 	# 	dir_path = peak_size_dict.get(dir_key).get('path')


		# 	top_compare_dir = os.path.join(settings\
		# 		.get('main_pipeline_dirs').get('top_save_dir'), \
		# 		'_v_'.join(comparison))

		# 	loop_comparison_saves.update({
		# 		'top_compare_dir': {'path': top_compare_dir}, 
		# 		'compare_analysis_indices': which_lists, 
		# 		'compare_merge_dict': compare_merge_dict
		# 	})
		# 	top_compare_path = loop_comparison_saves.get('top_compare_dir')\
		# 		.get('path')

		# 	top_merged_dir = os.path.join(\
		# 		top_compare_path, 'merged_beds')
		# 	loop_comparison_saves['top_compare_dir']['top_merged_dir'] = \
		# 		{'path': top_merged_dir}

		# 	add_unique_common_paths(loop_comparison_saves, top_compare_dir)

		# 	add_peak_size_paths(loop_comparison_saves, top_compare_dir)

		# if settings.get('print_timing') is True: 
		# 	start_comparison_pipeline = time.time()

		# # settings.get('save_dirs')\
		# # 	.get('core_pipeline_dirs')\
		# # 	.get('analyses')

		# for compare_loop, comparison in \
		# 	enumerate(settings.get('analysis_settings').get('comparison_analyses')):
		# 	loop_comparison_saves = {}
		# 	# which_analyses_to_compare = \
		# 	# 	comparison.get('which_analyses')
		# 	which_lists, compare_merge_dict = \
		# 		choose_comparison(comparison, \
		# 			settings.get('list_settings'))

		# 	setup_comp_pipe_paths(settings, comparison, loop_comparison_saves, \
		# 		which_lists, compare_merge_dict)
			
		# 	hfnxns.quick_dumps(loop_comparison_saves, 'comparison')

		# 	specific_comparison_analysis(settings, compare_loop)
		# 	if settings.get('print_timing') is True: 
		# 		print('\tComparison pipeline {} time: {} s'\
		# 			.format(compare_loop, \
		# 				round(time.time()-start_comparison_pipeline,2)))

		# 	raise
		# 	if settings.get('print_timing') is True: 
		# 		print('\tComparison pipeline {} time: {} s'\
		# 			.format(compare_loop, \
		# 				round(time.time()-start_comparison_pipeline,2)))

		# return




	# 	if settings.get('print_timing') is True: 
	# 		print('\tcomparison_pipeline')

	# 	print(settings.get('compare_analyses')[compare_loop]\
	# 		.get('top_compare_dir'))


	# comparison_pipeline(settings)

	# return
















	# broadpeak_pipeline imports

	# import shutil
	# import json
	# import glob
	# import os
	# import re
	# import time

	# import broadpeak_helper_functions as hfnxns




	# setup specific group analysis
	# group = list(settings.get('analysis_settings')\
	# 	.get('core_analysis').get('analyses').keys())[0]



	# common pattern for specific group core analyses:
		# settings.get('analysis_settings').get('core_analysis').get('analyses')
		# .get(group).get('analysis_name')
	# this is just a verbose way to grab the name of the current group analysis 
		# in a generalizable way



	# split_beds - need to update for script - specific group analysis





























