import multiprocessing as mp
import time
import datetime
from scipy.stats import chisquare
from scipy import stats
import json
import os
from multiprocessing import Queue
import sys
import traceback
# from colorama import init 
from termcolor import colored
import threading
import _thread
import pickle
import broadpeak_helper_functions as hfnxns
import operator
import glob
import matplotlib.pyplot as plt
import matplotlib as mpl
import colored_traceback
import numpy as np
import copy
from textwrap import wrap
import statsmodels
from statsmodels.stats.proportion import proportions_ztest
colored_traceback.add_hook()



class pool_wrapper(object):
	def __init__(self, cpu_count=None, let_child_processes_fail=False, extra_queue=1, 
		error_message_first_line_color='red', *args, **kwargs):
		# argument descriptions
			# cpu_count allows users to define the number of process in mp.Pool()
				# defaults to None -> mp.Pool() uses mp.cpu_count()
			# let_child_processes_fail should probably stay False:
				# if False: main process is killed when child processes error
				# if True: child process errors will be printed but the main
					# process will continue going. This isn't recommended
					# for the same reason that a blanket try-except isn't recommended
			# error_message_first_line_color sets the color for the first line of error
				# messages. This only matters if:
					# 1. you don't want to install termcolor
						# in which case, set this to None
					# 2. you want to customize this color (use colors available for
						# termcolor.colored)

		# __init__ variable explanations
			# making the multiprocessing workers that this is built around
				# args/kwargs to __init__ are passed directly to mp.Pool()
			# extra_queue is used when submitting jobs to the pool
				# it counts the number of extra jobs submitted (beyond 1 per
					# process). This exists so submission doesn't slow down the program
			# self.error_queue is passed to child processes to send error messages
				# back to the main process -> process to provide transparency 
				# regarding child process errors
			# self.error_thread is the thread that the error checker runs in
				# error checker = self.check_for_errors()
				# this checks the error_queue every 2s and if an error message has
					# been sent, it 
			# self.added_jobs is a dictionary storing added jobs that have not
				# yet been submitted to self.pool:
				# keys = job ids
				# values = inputs required to submit a job to self.pool
			# self.submitted_jobs is a dictionary storing the async results
				# of jobs that have been submitted to the pool but aren't done yet:
				# keys = job ids
				# values = asyncResult object for this job
			# self.finished_jobs is a dictionary storing async results for 
				# completed jobs:
				# keys = job ids
				# values = asyncResult object for this job
			# self.job_groups is a dictionary storing lists of jobs associated
				# with a specific job group for later prioritization:
				# keys = job group name
				# values = list of job ids associated with the group
			# self.all_submitted_async_results is a dictionary storing the async results
				# of all jobs that have been submitted to the pool:
				# jobs are not removed from this dictionary when completed
				# keys = job ids
				# values = asyncResult object for this job
			# self.error_queue_messages is a dictionary storing error messages from 
				# failed child processes:
				# keys = job id of failed child process
				# values = traceback returned from child process using self.error_queue
			# self.job_added_order is a list of job ids added to the object
				# order is important because jobs will be submitted to the pool in the
				# order that they were added unless some function requires prioritization
			# self.priority_job_order is empty unless a function calls for prioritization
				# this where the job group-prioritized job list is stored. This list
				# takes priority over self.job_added_order for completion if not empty
				# for jobs in the prioritized group, this preserves added order
			# self.priority_job_groups is a list of currently prioritized job groups
				# if newly added jobs are in these groups, they're added straight to
					# the priority job list
				# this is set by self.prioritize_specific_job_group(job_group)
				# and is reset by default when blocking
					# set reset_prio=False when blocking to keep priority groups

		if cpu_count is None:
			self.cpu_count = mp.cpu_count()
			self.pool = mp.Pool(*args, **kwargs)
		else:
			self.pool = mp.Pool(cpu_count, *args, **kwargs)
			self.cpu_count = cpu_count

		# user supplied settings as described above
		self.let_child_processes_fail = let_child_processes_fail
		self.error_message_first_line_color = error_message_first_line_color

		# setting up self.error_queue
		self.manager = mp.Manager()		
		self.error_queue = self.manager.Queue()
		self.results_queue = self.manager.Queue()

		# setting up intended_active_jobcount
		self.intended_active_jobcount = self.cpu_count + extra_queue

		# setting up job id set
		self.all_job_ids = set()

		# setting up job dictionaries to keep track of job objects
		self.added_jobs = {}
		self.submitted_jobs = {}
		self.finished_jobs = {}
		self.job_groups = {}
		self.all_submitted_async_results = {}

		# setting dictionary of error queue messages
		self.error_queue_messages = {}
		self.job_outputs = {}

		# setting up ordered lists of submitted job ids for job queueing order
		self.job_added_order = []
		self.priority_job_order = []
		self.priority_job_groups = []

		# setting up error checking thread
		self.lock = threading.Lock()
		self.error_thread = threading.Thread(target=self.check_for_errors, daemon=True)
		self.error_thread.start()
		
	# this function is run in self.error_thread to check for error messages
		# the thread is daemon=True, so this runs self.raise_error_message every
		# 2 seconds for the duration of the script 
	def check_for_errors(self):
		while True:
			with self.lock:
				self.check_queues()
			# self.raise_error_message()
			# self.check_for_results()
			time.sleep(2)

	def check_queues(self):
		found_errors = False
		while not self.error_queue.empty():
			found_errors = True
			error_output = self.error_queue.get()
			self.error_queue_messages[error_output[0]] = error_output[1]

		if found_errors:
			for this_key in sorted(list(self.error_queue_messages.keys())):
				mess1 = this_key
				mess2 = [inner_key for inner_key in self.job_groups.keys() if
								this_key in list(self.job_groups.get(inner_key))][0]
				if self.error_message_first_line_color is not None:
					print(colored('Error for Job {} of job group {}:'
						.format(mess1, mess2), 
								self.error_message_first_line_color))
				else:
					print('Error for Job {} of job group {}:'
						.format(mess1, mess2))
				print(self.error_queue_messages.get(this_key))
			if not self.let_child_processes_fail:
				print('Killing main process because errors arose in child processes')
				_thread.interrupt_main()

		while not self.results_queue.empty():
			results_output = self.results_queue.get()
			self.job_outputs[results_output[0]] = results_output[1]

	def block_for_specific_result(self, job_id):
		if self.added_jobs.get(job_id) is not None:
			with self.lock:
				if job_id in self.priority_job_order:
					self.priority_job_order.remove(job_id)
				self.priority_job_order.insert(0, job_id)
			while self.added_jobs.get(job_id) is not None:
				self.submit_job()
				time.sleep(0.1)

		while self.finished_jobs.get(job_id) is None:
			time.sleep(0.5)

		if self.finished_jobs.get(job_id) is not None:
			with self.lock:
				self.check_queues()
			return self.job_outputs.get(job_id)

	# this function checks for error messages in self.error_queue_messages
		# if there are errors, this will print them then kill the main process
			# unless let_child_processes_fail was false
		# can change error message 1st line color w/ error_message_first_line_color
			# if you want. setting it to None will remove the
			# termcolor package requirement (will print default color)
	# def raise_error_message(self):
		# if len(list(self.error_queue_messages.keys())) > 0:
		# 	for this_key in sorted(list(self.error_queue_messages.keys())):
		# 		mess1 = this_key
		# 		mess2 = [inner_key for inner_key in self.job_groups.keys() if
		# 						this_key in list(self.job_groups.get(inner_key))][0]
		# 		if self.error_message_first_line_color is not None:
		# 			print(colored('Error for Job {} of job group {}:'
		# 				.format(mess1, mess2), 
		# 						self.error_message_first_line_color))
		# 		else:
		# 			print('Error for Job {} of job group {}:'
		# 				.format(mess1, mess2))
		# 		print(self.error_queue_messages.get(this_key))
		# 	if not self.let_child_processes_fail:
		# 		print('Killing main process because errors arose in child processes')
		# 		_thread.interrupt_main()

	# quick helper function to list job ids in self.added_jobs
	def get_added_job_ids(self):
		return list(self.added_jobs.keys())

	# quick helper function to list job ids in self.submitted_jobs
	def get_submitted_job_ids(self):
		return list(self.submitted_jobs.keys())

	# quick helper function to list job ids in self.finished_jobs
	def get_finished_job_ids(self):
		return list(self.finished_jobs.keys())

	# print the list of added but not yet submitted job ids for a specific job group
		# this is used by self.block_for_specific_job_group and 
		# self.prioritize_specific_job_group to grab a list of waiting jobs in a
		# job group to prioritize job submission
	def get_waiting_jobs_by_group(self, job_group):
		added_job_ids = self.get_added_job_ids()
		job_group_jobs = list(self.job_groups.get(job_group))
		return [job_id for job_id in job_group_jobs if job_id in added_job_ids]

	# this checks how many jobs are currently submitted and the number of jobs waiting
		# to be submitted, and submits a new job if:
			# currently submitted < intended total submitted at a time
			# and
			# there are jobs waiting ot be submittes
	def should_submit_job(self):
		return (len(self.get_submitted_job_ids()) < self.intended_active_jobcount) and \
			(len(self.get_added_job_ids()) > 0)

	# this is a callback passed for wrap_apply_async. it gets called whenever a job ends
		# important: asyncResult.ready() (aka job done) only becomes True after this ends
			# you also can't check asyncResult.successful() until after .ready() is True
			# so this uses the error_queue to see if there was an issue
		# this also removed the job from the submitted dict
		# this also submits more jobs for as long as self.should_submit_job() is True
			# there is no reason that it should submit more than 1 job, but
				# i put it in a while loop just in case...
	def completed_job(self, job_id):
		# while not self.error_queue.empty():
		# 	error_output = self.error_queue.get()
		# 	self.error_queue_messages[error_output[0]] = error_output[1]
		with self.lock:
			job_result = self.submitted_jobs.get(job_id)
			del self.submitted_jobs[job_id]
			self.finished_jobs[job_id] = job_result
		while self.should_submit_job():
			self.submit_job()

	# this just generates a unique job id every time a job is added
		# there's probably a faster way to do this (eg, just iterate a number)
			# but if i ever decide to delete completed jobs and use released number,
			# this is more flexible. The slow down is also probably basically irrelevant
	def new_job_id(self):
		current_id = 0
		while True:
			if current_id in self.all_job_ids:
				current_id += 1
			else:
				break
		return current_id

	# this is the function that gets passed to child queues
		# everything that users touch should occur inside the try-except block ->
			# this should avoid all dropped errors in the child process
		# formatted error tracebacks are send back up through the error_queue
			# to be printed in the main process
		# job_id is returned and picked up by self.completed_job so that
			# the object can log which jobs are completed
	def wrap_apply_async(error_queue, result_queue, job_id, fnxn, args=None, kwargs=None):
		if args is None:
			args = []
		if kwargs is None:
			kwargs = {}
		try:
			fnxn_result = fnxn(*args, **kwargs)
		except Exception as exep:
			exc_group, exc_value, exc_tb = sys.exc_info()
			error_queue.put((job_id, 
				''.join(traceback.format_exception(exc_group, exc_value, exc_tb))))

		if fnxn_result is not None:
			try:
				test_fnxn_result = pickle.dumps(fnxn_result)
			except:
				error_queue.put((job_id, 
				'Cannot pickle function output. Outputs of functions run async using\
				\n\tpool_wrapper must be pickleable in order to send back to main process'))
			result_queue.put((job_id, fnxn_result))

		return job_id

	def job_group_exists(self, job_group):
		return self.job_groups.get(job_group) is not None

	# this blocks the process until all jobs from a specific job group are completed
		# used for example in broadpeak pipeline to ensure that all pathway analyses
			# are completed before path dots are called
		# has a few advantages compared to vanilla pool.close(); pool.join():
			# this does not close the pool (doesn't require re-opening it)
			# this runs prioritized jobs first (minimize blocked time)
			# all jobs can be run asynchronously through apply_async
				# with blocking that doesn't require you to keep track of
				# any specific state variable
		# can take a single group name or a list of group names
			# if a list is provided: prioritizes jobs from all provided groups
		# first part is same as self.prioritize_specific_job_group():
			# filter self.job_added_order -> self.priority_job_order
			# self.priority_job_order jobs are run before self.job_added_order jobs
			# jobs in self.priority_job_order are still run in the order they were added
		# next it submits all jobs in self.priority_job_order (ignoring normal jobcount)
		# lastly, it blocks until all jobs from all groups are done
			# grab_job_results() returns list of asyncResults for all prioritized jobs
		# if verbose=True, it will print # of jobs left every 2s if it decreases
	def block_for_specific_job_group(self, job_group, reset_prio=True, verbose=True):
		def grab_job_results(self, job_group, str_or_list):
			if str_or_list == 'str':
				return [self.all_submitted_async_results.get(job_id) for \
					job_id in list(self.job_groups.get(job_group))]
			elif str_or_list == 'list':
				return_list = []
				for this_group in job_group:
					return_list.extend([self.all_submitted_async_results.get(job_id) for \
						job_id in list(self.job_groups.get(this_group))])
				return return_list
			else:
				raise

		if reset_prio:
			self.priority_job_groups = []
		if isinstance(job_group, str):
			if self.job_groups.get(job_group) is None:
				raise ValueError(
					'called pool_wrapper.block_for_specific_job_group for group: {}\
					\n\tbut job group is not in the queue')
			str_or_list = 'str'
			with self.lock:
				jobs_to_submit = self.get_waiting_jobs_by_group(job_group)
				self.priority_job_order = [job_id for job_id in self.job_added_order if\
					job_id in jobs_to_submit]
		elif hasattr(job_group, '__iter__'):
			str_or_list = 'list'
			self.priority_job_order = []
			with self.lock:
				for this_group in job_group:
					if self.job_groups.get(this_group) is None:
						raise ValueError(
							'called pool_wrapper.block_for_specific_job_group for group: {}\
							\n\tbut job group is not in the queue')
					jobs_to_submit = self.get_waiting_jobs_by_group(this_group)
					self.prioritize_specific_job_group.extend([
						job_id for job_id in self.job_added_order if\
							job_id in jobs_to_submit])

		while len(self.priority_job_order) > 0:
			self.submit_job()

		ongoing_jobs = grab_job_results(self, job_group, str_or_list)

		last_job_left_count = len([job_id for job_id in ongoing_jobs if \
				not job_id.ready()])
		while not all([job_id.ready() for job_id in ongoing_jobs]):
			if verbose is True:
				new_job_left_count = len([job_id for job_id in ongoing_jobs if \
					not job_id.ready()])
				if new_job_left_count < last_job_left_count:
					print('Blocking for {} more jobs from group {}'.format(
						new_job_left_count, job_group))
					last_job_left_count = new_job_left_count
			time.sleep(1)
		
	# this prioritizes a specific job group w/o blocking
		# useful to get ahead if you know are going to block for this job group soon
			# eg, like example above: can start prioritizing pathways ahead of time
		# same as the first step of self.block_for_specific_job_group()
		# this also sets self.priority_job_groups(job_group) so that future incoming
			# jobs from the prioritized job group are added to the priority list
			# this variable can be optionally reset when blocking
			# or, if absolutely necessary, it can be manually reset with: 
				# {wrapper object name}.priority_job_groups = []
			# this should not be necessary basically ever though...
	def prioritize_specific_job_group(self, job_group):
		if isinstance(job_group, str):
			if self.job_groups.get(job_group) is None:
				raise ValueError(
					'called pool_wrapper.block_for_specific_job_group for group: {}\
					\n\tbut job group is not in the queue')
			with self.lock:
				self.priority_job_groups.append(job_group)
				jobs_to_submit = self.get_waiting_jobs_by_group(job_group)
				self.priority_job_order = [job_id for job_id in self.job_added_order if\
					job_id in jobs_to_submit]
		elif hasattr(job_group, '__iter__'):
			self.priority_job_order = []
			with self.lock:
				for this_group in job_group:
					if self.job_groups.get(this_group) is None:
						raise ValueError(
							'called pool_wrapper.block_for_specific_job_group for group: {}\
							\n\tbut job group is not in the queue')
					self.priority_job_groups.append(this_group)
					jobs_to_submit = self.get_waiting_jobs_by_group(this_group)
					self.prioritize_specific_job_group.extend([
						job_id for job_id in self.job_added_order if\
							job_id in jobs_to_submit])

	# this submits added jobs from the added job dict to mp.Pool's queue
		# added in order based first on the order of items in the priority job list
			# then on the order of items in the job_added_order list
		# main functions: actually calls mp.pool.apply_async()
			# moves variables from "added" dicts/lists -> "submitted"
		# most of the complicated conceptual steps happen when adding or in the callback
	def submit_job(self):
		also_rem_add_order = False
		if self.priority_job_order is not None:
			if len(self.priority_job_order) > 0:
				get_list = self.priority_job_order
				also_rem_add_order = True
			else:
				get_list = self.job_added_order
		else:
			get_list = self.job_added_order
		if len(get_list) < 1:
			raise ValueError('should not be runnign submit_job if len of added lists are 0')
		with self.lock:
			next_job_id = get_list.pop(0)
			job_info = self.added_jobs.get(next_job_id)
			del self.added_jobs[next_job_id]
			if also_rem_add_order:
				self.job_added_order.remove(next_job_id)

		async_result = self.pool.apply_async(pool_wrapper.wrap_apply_async,
			job_info, callback=self.completed_job)

		with self.lock:
			self.submitted_jobs[next_job_id] = async_result
			self.all_submitted_async_results[next_job_id] = async_result

	# just returns asyncResult.ready() for all jobs in self.all_submitted_async_results dict
		# used when blocking to check if done
	def check_all_submitted_if_done(self):
		return all([this_job.ready() for this_job in 
			list(self.all_submitted_async_results.values())])

	# as the name says, it returns a list of asyncResult objects for all unfinished
		# jobs. It could return an actual count, but this is more flexible
		# this also lets other functions monitor unfinished jobs on their own in wanted
	def count_unfinished_jobs(self):
		return len([this_job for this_job in 
			list(self.all_submitted_async_results.values()) if not this_job.ready()])

	# this adds jobs to the object's queues. This should be the main function used
		# to add jobs. jobs should never be submitted manually
		# basic workflow:
			# it generates a unique job id and adds it to the set of unique job ids
			# it manages self variables:
				# adds the job to a job_group list of it has a job group
				# adds the job to self.job_added_order list
				# if job group is prioritized: also adds to self.priority_job_order list
				# adds to self.added_jobs dict
				# also of these are required for further job management
		# by default, this  checks that all submitted args for apply_async can be pickled.
			# this slows down running but is recommended because errors due to running
			# apply_async on un-picklable objects is silent and difficult to identify
			# this should probably not be turned off unless you know what you're doing
			# it can be turned off using: pickle_check=False
	def add_job(self, fnxn, args=None, kwargs=None, job_group=None, pickle_check=True):
		job_id = self.new_job_id()
		self.all_job_ids.add(job_id)
		if job_group is not None:
			with self.lock:
				if self.job_groups.get(job_group) is None:
					self.job_groups[job_group] = [job_id]
				else:
					self.job_groups[job_group].append(job_id)

		if not callable(fnxn):
			raise ValueError('The "fnxn" argument to pool_wrapper.add_job must\
				\n\tbe a callable object, eg a function')
		if not isinstance(args, tuple) and not isinstance(args, list) and args is not None:
			raise ValueError('The "args" argument to pool_wrapper.add_job must\
				\n\tbe a list or a tuple or None. Was {}'.format(group(args))) 
		if not isinstance(kwargs, dict) and kwargs is not None:
			raise ValueError('The "kwargs" argument to pool_wrapper.add_job must\
				\n\tbe a dict or None. Was {}'.format(group(kwargs))) 

		job_info = (self.error_queue, self.results_queue, job_id, fnxn, args, kwargs) 
		with self.lock:
			self.job_added_order.append(job_id)
			self.added_jobs[job_id] = job_info
		
		if pickle_check:
			try:
				test_job_info = pickle.dumps(job_info)
			except:
				for item in job_info:
					try:
						test_item = pickle.dumps(item)
					except:
						raise ValueError('Cannot pickle job input. \
							\n\tThis could cause confusing errors. Failed input:\
							\n\t{}'.format(item))
		
		# want this down here because I want to be sure that this job is
			# formatted and tested before it can possibly be submitted
		if job_group is not None:
			with self.lock:
				if job_group in self.priority_job_groups:
					self.priority_job_order.append(job_id)

		while self.should_submit_job():
			self.submit_job()

		return pool_wrapper.get_job_result(self, job_id)

	# instantiat with:
		# pool_wrapper.get_job_result(self, job_id) ???
	class get_job_result(object):
		def __init__(self, parent_pool_wrapper, job_id):
			self.job_id = job_id
			self.parent_pool_wrapper = parent_pool_wrapper

		def result(self):
			return self.parent_pool_wrapper.block_for_specific_result(self.job_id)
		
	# def block_for_specific_result(self, job_id):

	# this blocks while all added jobs are completed
		# it submits all outstanding jobs (still prioritizes priority list)
		# it then loops through time.sleep(1) until all jobs end
		# if verbose=True, this will print the number of outstanding jobs
			# whenever a jobs ends every second or longer
		# by default, it will reset the priority job groups going forward
			# can set reset_prio=False to keep priority groups
	def block_for_all_jobs(self, reset_prio=True, verbose=True):
		if reset_prio:
			self.priority_job_groups = []
		while len(self.get_added_job_ids()) > 0:
			self.submit_job()

		loop_time = 5
		no_update_time = 30
		this_no_u = no_update_time
		last_job_left_count = self.count_unfinished_jobs()
		while len(self.get_submitted_job_ids()) > 0:
			if verbose is True:
				new_job_left_count = self.count_unfinished_jobs()
				if new_job_left_count < last_job_left_count:
					print('{} jobs still ongoing'.format(new_job_left_count))
					last_job_left_count = new_job_left_count
			time.sleep(loop_time)
			this_no_u -= loop_time
			if this_no_u <= 0:
				print('{} jobs still ongoing. No update in {}s'
					.format(new_job_left_count, no_update_time))
				this_no_u = no_update_time

	# this blocks for all remaining jobs and runs pool.close() + pool.join()
		# this basically ends use of this object
		# use of this isn't really recommended because I don't think this terminates
			# child processes, so it's not recommended 
			# to close this and start multiple pool_wrappers
	def close_wrapper(self, verbose=True):
		self.block_for_all_jobs(verbose=verbose)
		self.pool.close()
		self.pool.join()

def run_test(in_data):
	print(in_data)

def test_print(in_str):
	time.sleep(1)
	# raise
	print(colored(in_str, 'red'))
	# print(in_str)

	return 'test_{}'.format(in_str), 'stuff'

def other_test_print(in_str):
	time.sleep(3)
	print(colored(in_str, 'blue'))
	# print(in_str)
	
	return 'other_{}'.format(in_str)

def test_return(input_data):
	output_data = input_data
	output_data.append(1)

	return output_data

# old code
	# def jobs_done(self, print_output = False):
		# if print_output is True:
		# 	print([this_job.ready() for this_job in \
		# 			list(self.all_submitted_async_results.values())])
		# else:
		# 	return [this_job.ready() for this_job in \
		# 			list(self.all_submitted_async_results.values())]


	# if check_worked is False:
	# else:
	# 	if return_str is False:
	# 		print([this_job.ready() for this_job in \
	# 				list(self.all_submitted_async_results.values())])
	# 	else:
	# 		return [this_job.ready() for this_job in \
	# 				list(self.all_submitted_async_results.values())]


	# init()

	# print(list(self.error_queue_messages.keys()))


			# while True:
				# current_job_ids = list(self.added_jobs.keys())
				# current_jobs = list(self.added_jobs.values())
				# while True:
				# 	if not all([this_job.ready() for this_job in current_jobs]):
				# 		time.sleep(0.1)
				# 	else:
				# 		break
				# finished_job_ids = [job_id for job_id in current_job_ids if 
				# 	self.job_queue.get('all').get(job_id).ready()]
				# for this_id in finished_job_ids:
				# 	if self.job_queue.get('all').get(this_id).successful() is False:
				# 		pass


			# class test_class(object):
				# def __init__():
				# 	pass

				# def run_class(in_data):
				# 	print(in_data)

			# self.job_queue = {'all': {}, 'job_groups': {}}
			# self.finished_queue = {'all': {}, 'job_groups': {}}
			# self.finished_jobs = set()
				# print('checking for errors')
				# raise ValueError('Killing main process because errors arose in child processes')
				# print('Job {}'.format(error_output[0]))
				# print(error_output[1])
				# raise ValueError('Child process returned errors. Killing main process')
			# print('job {} finished!'.format(job_id))
			# self.finished_jobs.add(job_id)
			# this_job = self.added_jobs.get(job_id)
			# print('job done:', this_job.ready())

			# pass
			# while not self.error_queue.empty():
			# 	print(self.error_queue.get())
			# if this_job.ready():
			# 	print('job successful:', this_job.successful())
			# time.sleep(0.1)
			# print('callback early time: {}s'.format(
			# 	round(time.time() - callback_called, 3)))
			# add callback to wrap_apply_axync?

		# print(colored('starting {}'.format(job_id), 'white'))
		# time.sleep(1)
			# print('ran function {}'.format(loop_num))

		# queue.put('message_from {}'.format(job_id))
		# old code
			# jobs_to_submit = self.get_waiting_jobs_by_group(job_group)
			# self.priority_job_order = [job_id for job_id in self.job_added_order if\
			# 	job_id in jobs_to_submit]
			# while len(self.priority_job_order) > 0:
			# 	self.submit_job()
			# ongoing_jobs = [self.all_submitted_async_results.get(job_id) for \
			# 	job_id in list(self.job_groups.get(job_group))]



		# ongoing_jobs = [self.all_submitted_async_results.get(job_id) for \
		# 	job_id in list(self.job_groups.get(job_group))]

		# last_job_left_count = len([job_id for job_id in ongoing_jobs if \
		# 		not job_id.ready()])
		# while not all([job_id.ready() for job_id in ongoing_jobs]):
		# 	new_job_left_count = len([job_id for job_id in ongoing_jobs if \
		# 		not job_id.ready()])
		# 	if new_job_left_count < last_job_left_count:
		# 		print('Blocking for {} more jobs from group {}'.format(new_job_left_count,
		# 			job_group))
		# 		last_job_left_count = new_job_left_count
		# 	time.sleep(1)
		# async_result = self.pool.apply_async(pool_wrapper.wrap_apply_async,
		# 	(self.error_queue, job_id, test_print, ('running {}'.format(job_id),)),
		# 	callback=self.completed_job)
		# old code
			# print('applying')
			# print(colored('testing {}'.format(job_id), 'red'))

			# self.added_jobs[job_id] = \
			# 	(self.error_queue, job_id, test_print, ('running {}'.format(job_id),))
			# self.job_added_order.append(job_id)


			# if job_group is not None:
			# 	if self.job_queue.get(job_group) is None:
			# 		self.job_queue[job_group] = {}
			# 	self.job_queue[job_group][job_id] = async_result
			# 	self.job_queue['job_groups'][job_id] = job_group
			
			# if args is None:
			# 	args = []
			# if kwargs is None:
			# 	kwargs = {}
			
			# pool_wrapper.wrap_apply_async(self, job_id, fnxn, args, kwargs)
			# async_result = self.pool.apply_async(pool_wrapper.wrap_apply_async, 
			# 	(self, job_id, fnxn, args, kwargs))
			# async_result = self.pool.apply_async(pool_wrapper.wrap_apply_async, 
			# 	(self, job_id, print, (job_id,), kwargs))
			# async_result = self.pool.apply_async(print, ('running {}'.format(job_id),))
			# pool_wrapper.test_print(self, 'running {}'.format(job_id))
			# return
			# async_result = self.pool.apply_async(test_print,
			# 	('running {}'.format(job_id),))
			# async_result = self.pool.apply_async(pool_wrapper.test_print,
			# 	('running {}'.format(job_id),))
			# pool_wrapper.test_print('running {}'.format(job_id))


	# def pool_wrapper(queue, fnxn, loop_num, args=None, kwargs=None):
		# # queue.put('something')
		# time.sleep(5)
		# if args is None:
		# 	args = []
		# if kwargs is None:
		# 	kwargs = {}
		# try:
		# 	print('ran function {}'.format(loop_num))
		# 	fnxn(*args, **kwargs)
		# except Exception as exep:
		# # 	print('failed function {}'.format(loop_num))
		# 	exc_group, exc_value, exc_tb = sys.exc_info()
		# # 	# print(group(exc_group), group(exc_value), group(exc_tb))
		# # 	# print(group(traceback.format_exception(exc_group, exc_value, exc_tb)))
		# 	queue.put(''.join(traceback.format_exception(exc_group, exc_value, exc_tb)))
		# # 	# print(exc_group, exc_value, exc_tb)
		# # 	# queue.put((exep, exc_group, exc_value, exc_tb))
		# # 	print('get here')


	# def test_fnxn(workers, queue):


		# for time_loop in range(1):
		# 	print('applying async')
		# 	# workers.apply_async(my_sleep, args=(3, time_loop))
		# 	async_result = workers.apply_async(pool_wrapper, args=(queue, my_sleep, time_loop, (5, time_loop)))
		# 	# async_result = workers.apply_async(my_sleep, args=(5, time_loop, queue))
		# 	# my_sleep(5, time_loop, queue)
		# 	# pool_wrapper(queue, my_sleep, time_loop, (3, time_loop))

		# # while True:
		# while True:
		# 	try:
		# 		output = queue.get(timeout=2)
		# 		print(colored('found something', 'red'))
		# 		print(output)
		# 		break
		# 	except KeyboardInterrupt:
		# 		raise
		# 	except:
		# 		print(colored('didnt find anything', 'red'))
		# 		print('\tprocess done:', async_result.ready())
		# 		if async_result.ready():
		# 			print('\tprocess successful:', async_result.successful())
		# 			break
		# 	# if async_result.ready():
		# 	# 	print('done')
		# 	# 	print(async_result.successful())
		# 	# 	break
		# 	# else:
		# 	# 	time.sleep(0.2)
		# # print(colored(output, 'red'))
		# # raise ValueError('Child process failed, see above traceback')
		# workers.close()
		# workers.join()
		# # 	if isinstance(output, Exception):
		# # 		raise output
		# # 	else:
		# # 		print([type(thing) for thing in output], output)
		
		# # time.sleep(10)

	# def callback():
		# return True

	# def pretend_run_thing_async(fnxn,  args=None, kwargs=None):
		# if args is None:
		# 	args = []
		# if kwargs is None:
		# 	kwargs = {}
		# return play_promise(fnxn, args, kwargs)

	# class play_promise(object):
		# def __init__(self, callback_fnxn, callback_args, callback_kwargs):
		# 	self.callback_fnxn = callback_fnxn
		# 	self.callback_args = callback_args
		# 	self.callback_kwargs = callback_kwargs
		# def resolve(self):
		# 	return self.callback_fnxn(*self.callback_args, **self.callback_kwargs)

	# def update_var_from_fnxn(update_var, callback, args=None, kwargs=None):
		# global update_var
		# print(update_var)
		# if args is None:
		# 	args = []
		# if kwargs is None:
		# 	kwargs = {}
		# update_var.append(fnxn(*args, **kwargs))
		# print(update_var)

def make_tss_beds(tss_file_path, save_path, overlap_dist):
	tss_peaks = []
	with open(tss_file_path, 'r') as textfile:
		for line in textfile:
			line = line.strip('\n').split('\t')
			tss_peaks.append([line[0], int(line[1]), line[3]])

	# print('\n'.join([str(thing) for thing in tss_peaks[:10]]))
	tss_peaks = sorted(tss_peaks, key=operator.itemgetter(2,0,1))


	final_beds = []
	current_peak = None
	for line_loop, line in enumerate(tss_peaks):
		if line[2] == 'zbtb16':
			print_line = True
		else:
			print_line = False
		peak_done = False
		if current_peak == None:
			current_peak = [line[0], line[1]-overlap_dist, line[1]+overlap_dist, line[2]]
			continue
		else:
			if line[2] != current_peak[3]:
				peak_done = True
			elif current_peak[0] != line[0]:
				peak_done = True
			elif line[1] - overlap_dist > current_peak[2]:
				peak_done = True
			elif line[1] - overlap_dist < current_peak[2] and line[1] + overlap_dist > current_peak[1]:
				if line[1] + overlap_dist > current_peak[2]:
					current_peak[2] = line[1] + overlap_dist
			else:
				peak_done = True
				# raise ValueError('shouldn\'t be here')
				# print(current_peak, line, current_peak[1], line[1] - overlap_dist, current_peak[2], line[1] + overlap_dist)
				# print('\t', line[2] != current_peak[3], current_peak[0] != line[0], line[1] - overlap_dist > current_peak[2],
				# 	line[1] - overlap_dist > current_peak[2] and line[1] + overlap_dist < current_peak[1])
				# print('\t\t', line[1] - overlap_dist > current_peak[2], line[1] + overlap_dist < current_peak[1])
				# if line_loop > 10:
				# 	raise
		if peak_done:
			# 'appending: {}, {}'.format(current_peak, )
			final_beds.append(current_peak)
			current_peak = [line[0], line[1]-overlap_dist, line[1]+overlap_dist, line[2]]

	final_beds = sorted(final_beds, key=operator.itemgetter(0,1,2,3))

	# print('\n'.join([str(thing) for thing in final_beds[:10]]))

	hfnxns.write_bed(save_path, final_beds)

	# raise

def my_sleep(wait_time, loop_num, queue):
	print('starting wait: {}'.format(loop_num))
	time.sleep(wait_time)
	try:
		raise
	except:
		# pass
		queue.put('it failed')
	print('ending wait: {}\
		\n\tat {}\n'.format(loop_num, datetime.datetime.now()\
			.strftime("%d/%m/%Y %H:%M:%S")))

def test_fnxn(my_int):
	return list(range(my_int))

def grab_sorted_peak_lens(input_bed, subtract):
	peak_list = []
	with open(input_bed, 'r') as readfile:
		for peak in readfile:
			peak = peak.split('\t')
			if subtract is True:
				peak_len = float(peak[6]) - float(peak[5])
			else:
				peak_len = float(peak[5])
			peak_ratio = float(peak[6]) / float(peak[5])
			peak_list.append([peak[0], int(peak[1]), int(peak[2]), peak_len, peak_ratio])
	return sorted(peak_list, key=operator.itemgetter(0,1,2))

		# print(peak, current_peak)
		# if peak_loop > 10:
		# 	raise
# def compare_print_list(peak_list1, peak_list2):
	# # hfnxns.write_bed('test_sub1.txt', subpro_to_list(\
	# # 	subprocess.check_output(['subtractBed', '-A', '-a', \
	# # 		comparison1_dict.get(test_group).get(sample_type)[0], '-b',  \
	# # 		comparison2_dict.get(test_group).get(sample_type)[0]])))

	# # for peak_loop, peak in enumerate(peak_list1):
	# # 	if len(peak) < 2:
	# # 		continue
	# 	# peak_loop, window_index, output_list, finished_list = \
	# output_list = hfnxns.compare_peak_list_col_values(
	# 	peak_list1, peak_list2, keep_over_or_nonover='over', 
	# 	chr_index=0, peak_min_index=1, peak_max_index=2, overlap_range=0)

	# 	# if finished_list == True:
	# 	# 	break

	# # hfnxns.quick_dumps(peak_list1[:5])
	# # hfnxns.quick_dumps(peak_list2[:5])
	# # hfnxns.quick_dumps(output_list[:5])
	# # write_bed(save_path,output_list)
	# return output_list

def gene_list_dict(gene_list, list_order, bed_dict, subgroup_dict, relevant_dict, group_names, verbose=False):
	all_gene_counts = {}
	all_gene_lists  = {}
	all_file_gene_counts = {}
	all_file_gene_lists  = {}
	for bar_loop, bar in enumerate(gene_list):
		if verbose:
			print('sample type: {}, this_common: {}'.format(list_order[bar_loop][0], list_order[bar_loop][1]))

		# print(list_order[bar_loop][1], list_order[bar_loop][0])
		gene_file_path = bed_dict.get(list_order[bar_loop][1]).get(list_order[bar_loop][0])
		with open(gene_file_path, 'r') as readfile:
			file_genes = readfile.readlines()
		file_genes = set([gene.strip('\n').lower() for gene in file_genes])

		for mini_bar_loop, mini_bar in enumerate(bar):
			correct = 0
			incorrect = 0
			corr_list = []
			group_dict = {'TSG': 0, 'Oncogene': 0, 'Neither': 0, None: 0, 'all': 0}
			group_dict_lists = {'TSG': set(), 'Oncogene': set(), 'Neither': set(), None: set(), 'all': set()}
			for gene_loop, gene in enumerate(mini_bar):
				gene_tuple = (gene, relevant_dict.get(gene), subgroup_dict.get(relevant_dict.get(gene)))
				group_dict[subgroup_dict.get(relevant_dict.get(gene))] += 1
				group_dict_lists[subgroup_dict.get(relevant_dict.get(gene))].add(gene_tuple)
				group_dict['all'] += 1
				group_dict_lists['all'].add(gene_tuple)
			if group_names[mini_bar_loop] is None:
				non_none_count 	= 	sum([group_dict.get(key) for key in group_dict if key is not None])
				all_count 		= 	sum([group_dict.get(key) for key in group_dict])
				percent_dict = {key:round(group_dict.get(key)/all_count*100,1) for key in group_dict}
				if verbose:
					print('\tmini-bar type: {}, len: {}, genefile: {}, %s: {}'.format(group_names[mini_bar_loop], len(mini_bar), 
						len(file_genes), percent_dict))
					print('\t\tplot genes: {}, non-None: {}'.format(group_dict, non_none_count))
		file_corr_list = []
		file_group_dict = {'TSG': 0, 'Oncogene': 0, 'Neither': 0, None: 0, 'all': 0}
		file_group_dict_lists = {'TSG': set(), 'Oncogene': set(), 'Neither': set(), None: set(), 'all': set()}
		for gene_loop, gene in enumerate(file_genes):
			gene_tuple = (gene, relevant_dict.get(gene), subgroup_dict.get(relevant_dict.get(gene)))
			file_group_dict[subgroup_dict.get(relevant_dict.get(gene))] += 1
			file_group_dict_lists[subgroup_dict.get(relevant_dict.get(gene))].add(gene_tuple)
			file_group_dict['all'] += 1
			file_group_dict_lists['all'].add(gene_tuple)
		non_none_count = 	sum([file_group_dict.get(key) for key in file_group_dict if key is not None])
		all_count = 		sum([file_group_dict.get(key) for key in file_group_dict])
		percent_dict = {key:round(file_group_dict.get(key)/all_count,1) for key in file_group_dict}
		if verbose:
			print('\t\tfile genes: {}, non-None: {}'.format(group_dict, non_none_count))
		all_gene_counts['{}-{}'.format(list_order[bar_loop][1], list_order[bar_loop][0])] = \
			group_dict
		all_gene_lists['{}-{}'.format(list_order[bar_loop][1], list_order[bar_loop][0])] = \
			group_dict_lists
		all_file_gene_counts['{}-{}'.format(list_order[bar_loop][1], list_order[bar_loop][0])] = \
			file_group_dict
		all_file_gene_lists['{}-{}'.format(list_order[bar_loop][1], list_order[bar_loop][0])] = \
			file_group_dict_lists

	return all_gene_counts, all_gene_lists, all_file_gene_counts, all_file_gene_lists

def bed_file_dict(kwargs):
	bed_dict = {}
	for dirpath in kwargs.keys():
		original_file_beds = sorted(list(glob.iglob(os.path.join(kwargs.get(dirpath), '*'))))
		bed_dict[dirpath] = {}
		for filepath in original_file_beds:
			if '_' in filepath.split('/')[-1]:
				bed_dict[dirpath][filepath.split('/')[-1].split('_')[0]] = filepath 
			else:
				bed_dict[dirpath][filepath.split('/')[-1].split('.')[0]] = filepath 
				# bed_dict[dirpath][filepath.split('/')[-1].split('.')[0]] = filepath 
			# print(dirpath, filepath.split('/')[-1].split('.')[0])
	return bed_dict

def check_cancer_plots(bed_dict_input, peak_plot_list, old_peak_plot_list, relevant_list, group_names, old_types, types, list_order, 
	subgroup_dict):
	bed_dict = bed_file_dict(bed_dict_input)
	with open(peak_plot_list, 'r') as readfile:
		peak_dict = json.load(readfile)
		gene_list = peak_dict.get('peak_list_to_plot')
		gene_list = [[[gene.lower() for gene in stuff] for stuff in thing] for thing in gene_list]

	with open(old_peak_plot_list, 'r') as readfile:
		peak_dict = json.load(readfile)
		old_gene_list = peak_dict.get('peak_list_to_plot')
		if old_types is not None:
			old_gene_list = [this_list for list_loop, this_list in enumerate(old_gene_list) if old_types[list_loop] not in ['E', 'all']]
		old_gene_list = [[[gene.lower() for gene in stuff] for stuff in thing] for thing in old_gene_list]
	relevant_dict = {}
	with open(relevant_list, 'r') as readfile:
		for line in readfile:
			line = line.strip('\n').split('\t')
			if len(line) > 2:
				raise ValueError('this line: {}'.format(line))
			if len(line) > 1:
				relevant_dict[line[0].lower()] = line[1]
			else:
				print('line: {}'.format(line))

	new_all_gene_counts, new_all_gene_lists, new_all_file_gene_counts, new_all_file_gene_lists = \
			gene_list_dict(gene_list, list_order, bed_dict, subgroup_dict, relevant_dict, group_names)
	old_all_gene_counts, old_all_gene_lists, old_all_file_gene_counts, old_all_file_gene_lists = \
			gene_list_dict(old_gene_list, list_order, bed_dict, subgroup_dict, relevant_dict, group_names)

	counts = [new_all_gene_counts, new_all_file_gene_counts, old_all_gene_counts, old_all_file_gene_counts]
	lists = [new_all_gene_lists, new_all_file_gene_lists, old_all_gene_lists, old_all_file_gene_lists]
	list_keys = ['{}-{}'.format(list_val[1], list_val[0]) for list_val in list_order]
	for this_key in list_keys:
		print(this_key)
		print('\t new counts: ', new_all_gene_counts.get(this_key))
		print('\t fil counts: ', old_all_file_gene_counts.get(this_key))
		print('\t new set co: ', ['{}: {}'.format(inner_key, len(list(new_all_gene_lists.get(this_key).get(inner_key)))) for \
			inner_key in new_all_gene_lists.get(this_key).keys()])
		print('\t old set co: ', ['{}: {}'.format(inner_key, len(list(old_all_gene_lists.get(this_key).get(inner_key)))) for \
			inner_key in old_all_gene_lists.get(this_key).keys()])
		print('\t old counts: ', old_all_gene_counts.get(this_key))
		for inner_key in new_all_gene_lists.get(this_key).keys():
			new_set = new_all_gene_lists.get(this_key).get(inner_key)
			old_set = old_all_gene_lists.get(this_key).get(inner_key)
			if len(new_set.difference(old_set)) > 0:
				print('\t\tnew {}, {} genes: {}'.format(this_key, inner_key, new_set.difference(old_set)))
			if len(old_set.difference(new_set)) > 0:
				print('\t\told {}, {} genes: {}'.format(this_key, inner_key, old_set.difference(new_set)))

	# old code
		# peak_plot_list = '/Users/jschulz1/Documents/project_scripts/ccle_project/chipseq_broadpeak_pipeline/cosmic_peak_plot_list.bed'
		# gene_list = []
		# with open(peak_plot_list, 'r') as readfile:
		# 	for line in readfile:
		# 		gene_list.append([json.loads(col) for col in line.strip('\n').replace("'",'"').split('\t')])
		# old_peak_plot_list = '/Users/jschulz1/Documents/project_data/ccle_project/analyses/2019_11_18-full_4kb_vs_1kb_run/full_4kb_vs_1kb_run/data_for_figures/cancer_barplots/len_cosmic'
			# gene_list = [this_list for list_loop, this_list in enumerate(gene_list) if old_types[list_loop] not in ['E', 'all']]
		# relevant_list = '/Users/jschulz1/Documents/project_scripts/ccle_project/chipseq_broadpeak_pipeline/cosmic_relevant_list.bed'
		# group_names = ['TSG', 'Oncogene', 'Neither', None]

def check_text(fig, ax, my_str, how_align, xspot=0.5, yspot=0.5, fontsize=20):
	ax.scatter([0,1,0,1], [0,0,1,1], c='k')
	ax.scatter(xspot, yspot, c='C5',zorder=9,s=0.3)
	new_text, loc = fig.asterisk_and_location(ax, xspot, yspot, my_str, color='r', ha='center', \
		va=how_align, weight='semibold', fontsize=fontsize, zorder=8)
	height = loc[1][1] - loc[1][0]
	new_height = height/2
	width = loc[0][1] - loc[0][0]
	ymiddle = yspot - (new_height/2)
	ybottom = yspot - new_height
	ax.scatter([loc[0][0], loc[0][1], loc[0][0], loc[0][1]], 
		[loc[1][0], loc[1][0], loc[1][1], loc[1][1]], s=0.3, c='r', zorder=10)
	ax.scatter([xspot, xspot], 
		[ymiddle, ybottom], s=0.3, c='C5', zorder=10)
	return width, new_height, xspot, ymiddle

def check_ast(fig, ax, xspot=0.5, yspot=0.2, fontsize=20):
	ax.scatter([0,1,0,1], [0,0,1,1], c='k')
	ax.scatter(xspot, yspot, c='k',zorder=9,s=0.3)
	new_text, loc = fig.asterisk_and_location(ax, xspot, yspot, color='r', ha='center', \
		weight='semibold', fontsize=fontsize, zorder=8)
	xmiddle, ymiddle, width, new_height = loc
	# height = loc[1][1] - loc[1][0]
	# new_height = height/2
	# width = loc[0][1] - loc[0][0]
	# ymiddle = yspot - (new_height/2)
	# ybottom = yspot - new_height
	ax.scatter([xmiddle-(width/2), xmiddle+(width/2), xmiddle-(width/2), xmiddle+(width/2)], 
		[ymiddle-(new_height/2), ymiddle-(new_height/2), ymiddle+(new_height/2), ymiddle+(new_height/2)], s=0.3, c='r', zorder=10)
	ax.scatter([xmiddle, xmiddle, xmiddle], 
		[ymiddle, ymiddle+(new_height/2), ymiddle-(new_height/2)], s=0.3, c='C5', zorder=10)
	return width, new_height, xmiddle, ymiddle

def check_text_plot(fig, ax_list, my_str, fontsize=20):
	va_list = ['top', 'bottom', 'center', 'baseline', 'center_baseline']
	for va_loop, this_va in enumerate(va_list):
		check_text(fig, ax_list[va_loop], my_str, this_va, fontsize=fontsize)

def get_plot_item_bbox(fig, ax, plt_obj):
	trans = ax.transData.inverted()
	renderer = fig.canvas.get_renderer()
	obj_bbox = plt_obj.get_window_extent(renderer=renderer)
	
	obj_bbox = trans.transform(obj_bbox)

	x_vals = [obj_bbox[0][0], obj_bbox[1][0]]
	y_vals = [obj_bbox[1][1], obj_bbox[0][1]]
	obj_bbox = [[min(x_vals), max(x_vals)],[min(y_vals), max(y_vals)]]
	ax.scatter([obj_bbox[0][0], obj_bbox[0][1], obj_bbox[0][0], obj_bbox[0][1]], 
		[obj_bbox[1][0], obj_bbox[1][0], obj_bbox[1][1], obj_bbox[1][1]], s=0.3, c='r', zorder=10)

	return obj_bbox

def add_stuff(ax, midx, midy, height, width):
	ax.plot([midx-(width*(multi/2)),midx-(width*(multi/2)),midx+(width*(multi/2)),midx+(width*(multi/2),
		midx-(width*(multi/2)))], 
		[midy-(height*(multi/2)),midy+(height*(multi/2)),midy+(height*(multi/2)),midy-(height*(multi/2),
		midy-(height*(multi/2)))], 
		c='k')
	ax.scatter([midx,midx,midx-(width*(multi/2)),midx+(width*(multi/2))], 
		[midy+(height*(multi/2)),midy-(height*(multi/2)),midy,midy], 
		c='k')

def new_add_stuff(ax, midx, midy, height, width):
	# print(midx, midy, height, width)
	ax.plot([midx-(width/2),midx-(width/2),midx+(width/2),midx+(width/2),
		midx-(width/2)], 
		[midy-(height/2),midy+(height/2),midy+(height/2),midy-(height/2),
		midy-(height/2)], 
		c='k')
	ax.scatter([midx,midx,midx-(width/2),midx+(width/2)], 
		[midy+(height/2),midy-(height/2),midy,midy], 
		c='k')

def marker_real_bbox(fig, ax, xspot, yspot, *args, **kwargs):
	mpl_marker_multi = 3
	obj = ax.scatter(xspot, yspot, *args, **kwargs)
	bbox = get_plot_item_bbox(fig,ax,obj)
	height = (bbox[1][1] - bbox[1][0])*mpl_marker_multi
	width = (bbox[0][1] - bbox[0][0])*mpl_marker_multi

	return obj, width, height

def new_marker_real_bbox(fig, ax, xspot, yspot, *args, **kwargs):
	mpl_marker_multi = 3
	obj = ax.scatter(xspot, yspot, *args, **kwargs)
	trans = ax.transData.inverted()
	renderer = fig.canvas.get_renderer()
	obj_bbox = obj.get_window_extent(renderer=renderer)
	obj_bbox = trans.transform(obj_bbox)

	x_vals = [obj_bbox[0][0], obj_bbox[1][0]]
	y_vals = [obj_bbox[1][1], obj_bbox[0][1]]
	obj_bbox = [[min(x_vals), max(x_vals)],[min(y_vals), max(y_vals)]]
	height = (bbox[1][1] - bbox[1][0])*mpl_marker_multi
	width = (bbox[0][1] - bbox[0][0])*mpl_marker_multi

	return obj, width, height

def add_dots(fig, ax, xspot, yspot, width, height):
	new_add_stuff(ax, 0.5, 0.5, height, width)
	new_add_stuff(ax, 0.5, 0.5-height, height, width)
	new_add_stuff(ax, 0.5+width, 0.5, height, width)


	pass

def add_other_positions(fig, ax, obj, size, marker, multi):
	bbox = get_plot_item_bbox(fig,ax,obj)
	height = bbox[1][1] - bbox[1][0]
	obj2 = ax.scatter(0.5, 0.5-(height*multi), c='C5',zorder=9,s=size,marker=marker)
	bbox2 = get_plot_item_bbox(my_fig.fig,ax,obj2)
	width = bbox[0][1] - bbox[0][0]
	obj3 = ax.scatter(0.5+(width*multi), 0.5, c='C5',zorder=9,s=size,marker=marker)
	bbox3 = get_plot_item_bbox(my_fig.fig,ax,obj3)
	add_stuff(ax, 0.5, 0.5, height, width)
	add_stuff(ax, 0.5, 0.5-(height*multi), height, width)
	add_stuff(ax, 0.5+(width*multi), 0.5, height, width)

	return obj2, bbox2, obj3, bbox3

def add_test_bars(ax, box_locations):
	for location in box_locations:
		bottom = location[1]
		height = location[3] - location[1]
		width = location[2] - location[0]
		xmid = location[0] + (width/2)
		ax.bar(xmid, height, width=width, bottom=bottom)

def fig2data(fig):
    """
    @brief Convert a Matplotlib figure to a 4D numpy array with RGBA channels and return it
    @param fig a matplotlib figure
    @return a numpy 3D array of RGBA values
    """
    # draw the renderer
    fig.canvas.draw()
 
    # Get the RGBA buffer from the figure
    w,h = fig.canvas.get_width_height()
    buf = np.fromstring(fig.canvas.tostring_argb(), dtype=np.uint8)
    # buf.shape = (w, h, 4)
 
    # canvas.tostring_argb give pixmap in ARGB mode. Roll the ALPHA channel to have it in RGBA mode
    # buf = np.roll(buf, 3, axis=2)
    return buf

def test_sterisk_buffer(my_fig, ax, box_locations, loop_ind, failed_list, ast_list=None, fontsize = 50, which_buffer='bottom', 
	min_fontsize=10, min_vbuffer=0.5, min_hbuffer=0.25, color='k', 
	weight='semibold', zorder=2, pref_buffer=1, just_ind=None, *args, **kwargs):
	if ast_list is None:
		ast_list = box_locations
	add_test_bars(ax, box_locations)
	ax.patch.set_alpha(0.5)

	any_failed = False
	# fontsize = 50
	# print('')
	for loc_loop, location in enumerate(box_locations):
		# ax, allowed_x, allowed_y, fontsize, pref_buffer, *args,
		# which_buffer='bottom', min_fontsize=None, min_vbuffer=None, min_hbuffer=None, x_in_xlim=True, y_in_ylim=True, 
		# **kwargs
		# if just_ind is None:
		text_obj, loc_info, failed = \
			my_fig.buffer_ast_in_bar(ax, (ast_list[loc_loop][0], ast_list[loc_loop][2]), 
				(ast_list[loc_loop][1], ast_list[loc_loop][3]), fontsize, *args, pref_buffer=pref_buffer,
				which_buffer=which_buffer, min_fontsize=min_fontsize, min_vbuffer=min_vbuffer, 
				min_hbuffer=min_hbuffer, color=color, weight=weight, zorder=zorder, **kwargs)
		fontsize_ratio, new_xmiddle, new_ymiddle, width, height = loc_info
		# fontsize_ratio, new_xmiddle, new_ymiddle, x_move, y_move, width, height = loc_info
		if loc_loop == 0 and not any(failed_list):
			print_str = '\t{}: '.format(loop_ind)
			start_str = print_str
			if failed:
				any_failed = True
				min_block_width = round(width*min_hbuffer*2, 2)
				min_block_height = round(height*min_vbuffer*2, 2)
				print_str += 'fitting failed, '
				if min_block_height > hfnxns.num_diff(location[1], location[3]):
					print_str += 'min_block_height: {}, '.format(min_block_height)
				if min_block_width > hfnxns.num_diff(location[0], location[2]):
					print_str += 'min_block_width: {}, '.format(min_block_width)
					
			if print_str != start_str:
				print(print_str)
		#
			# .format(round(width*min_hbuffer*2, 2), round(height*min_vbuffer*2, 2))
				# else:
				# 	if fontsize_ratio != 1:
				# 		print_str += 'fontsize_ratio: {}, '.format(round(fontsize_ratio, 3))
				# 	if x_move != 0:
				# 		print_str += 'x_move: {}, '.format(round(x_move, 3))
				# 	if y_move != 0:
				# 		print_str += 'y_move: {}, '.format(round(y_move, 3))
			# elif just_ind == loc_loop:
			# 	print('not removing', loc_loop)
			# 	text_obj, loc_info, failed = \
			# 		my_fig.buffer_ast_in_bar(ax, (ast_list[loc_loop][0], ast_list[loc_loop][2]), 
			# 			(ast_list[loc_loop][1], ast_list[loc_loop][3]), fontsize, *args, pref_buffer=pref_buffer,
			# 			which_buffer=which_buffer, min_fontsize=min_fontsize, min_vbuffer=min_vbuffer, 
			# 			min_hbuffer=min_hbuffer, color=color, weight=weight, zorder=zorder, **kwargs)
			# 	fontsize_ratio, new_xmiddle, new_ymiddle, width, height = loc_info
			# 	# fontsize_ratio, new_xmiddle, new_ymiddle, x_move, y_move, width, height = loc_info
			# 	if loc_loop == 0 and not any(failed_list):
			# 		print_str = '\t{}: '.format(loop_ind)
			# 		start_str = print_str
			# 		if failed:
			# 			any_failed = True
			# 			min_block_width = round(width*min_hbuffer*2, 2)
			# 			min_block_height = round(height*min_vbuffer*2, 2)
			# 			print_str += 'fitting failed, '
			# 			if min_block_height > hfnxns.num_diff(location[1], location[3]):
			# 				print_str += 'min_block_height: {}, '.format(min_block_height)
			# 			if min_block_width > hfnxns.num_diff(location[0], location[2]):
			# 				print_str += 'min_block_width: {}, '.format(min_block_width)
							
			# 				# .format(round(width*min_hbuffer*2, 2), round(height*min_vbuffer*2, 2))
			# 		# else:
			# 		# 	if fontsize_ratio != 1:
			# 		# 		print_str += 'fontsize_ratio: {}, '.format(round(fontsize_ratio, 3))
			# 		# 	if x_move != 0:
			# 		# 		print_str += 'x_move: {}, '.format(round(x_move, 3))
			# 		# 	if y_move != 0:
			# 		# 		print_str += 'y_move: {}, '.format(round(y_move, 3))
			# 		if print_str != start_str:
			# 			print(print_str)
			# 	pass
			# elif just_ind != loc_loop:
			# 	print('removing', loc_loop)
			# 	hfnxns.easy_fig.format_panel(ax, hide_panel=True)
			# 	# ax.remove()

	return any_failed
		

	# 	if not failed:
	# 		print('expected_xvals: {}, real_xvals: {}\
	# 			\n\texpected_yvals: {}, real_yvals: {}\n'
	# 			.format((location[0], location[2]), 
	# 				(round(new_xmiddle-(width/2), 3), round(new_xmiddle, 3), round(new_xmiddle+(width/2), 3)), 
	# 				(location[1], location[3]), 
	# 				(round(new_ymiddle-(height/2), 3), round(new_ymiddle, 3), round(new_ymiddle+(height/2), 3))))
	# 	else:
	# 		print('expected_xvals: {}, width: {}\
	# 			\n\texpected_yvals: {}, height: {}\n'
	# 			.format((location[0], location[2]), round(width, 3),
	# 				(location[1], location[3]), round(height, 3)))
	# print('\n\n')
















	# print('starting: {}, {}'.format(obj, my_obj))
		# print('\tlooping: {}'.format(my_obj))
	# print('1 done: {}'.format(my_obj))

	# print('1 done: {}, {}, {}'.format(obj, my_obj2, my_obj))
			# print('\tlooping: {}'.format(my_obj2))
	# print('\tlooping: {}'.format(obj))

if __name__ == "__main__":
	pass
	# print(statsmodels.stats.proportion.proportions_ztest([9,810], [290, 23399]))
	print(proportions_ztest([9,810], [290, 23399]))

	# print(810/23399*281)

		# print(i, chisquare([9,810],[290,23399]))
	# print(stats.chi2_contingency([[9,810],[290,23399]]))
	# for i in range(1,5):
		# print(stats.chi2_contingency([[1,1*(i**2)],[100,100*(i**2)],[3,3*(i**2)],[4,4*(i**2)],[5,5*(i**2)],[6,6*(i**2)]]))
		# print(stats.chisquare([[1,1*(i**2)],[100,100*(i**2)],[3,3*(i**2)],[4,4*(i**2)],[5,5*(i**2)],[6,6*(i**2)]]))
	# print(stats.chisquare([[1,1,1,1,1],[2,2,2,2,2],[3,3,3,3,3],[4,4,4,4,4],[5,5,5,5,5]]))
	# print(stats.chisquare([[10000,10000],[20000,20000],[30000,30000],[40000,40000],[50000,50000]]))
	# print(stats.chisquare([[1,1,1,1,1],[2,2,2,2,2]]))
	# print(stats.chisquare([[9,810],[290,23399]]))



	# my_str = 'this is my string for testing'
	# test_str = 'stuff'
	# print(my_str.find(test_str))
	# print(hfnxns.format_pathname_kegg_hallmark('this hallmark is a thing', add_sep=' '))
	# for i in range(5):
	# 	first_list = []
	# 	for e in range(100):
	# 		time1 = time.time()
	# 		my_str.find(test_str)
	# 		first_list.append(time.time() - time1)
	# 	second_list = []
	# 	for e in range(100):
	# 		time1 = time.time()
	# 		test_str in my_str
	# 		second_list.append(time.time() - time1)
	# 	print('first: {}\n\tsecond: {}'.format(hfnxns.mean(first_list), hfnxns.mean(second_list)))





	# a = [1,2,3]
	# b = a + [3]
	# print(a)
	# print(b)




	# dict_list = [{'a':[1,7], 'b': 2}, {'c': 3, 'd': 4}, {'e': 5, 'f': 6}]
	# print(dict_list)
	# hfnxns.update_by_key_ind_list(dict_list, [1,'c'], fnxn = lambda x: x * 10)
	# hfnxns.update_by_key_ind_list(dict_list, [0,'b'], new_val = 1000)
	# print(hfnxns.update_by_key_ind_list(dict_list, [2,'e']))
	# print(hfnxns.update_by_key_ind_list(dict_list, [0,'a',0], pop_val=True))
	# print(dict_list)

































	# my_val = [1,[2,3,[4,5,6],7],8]




	# grab_fnxn = lambda x: x[1].get('c')
	# grab_fnxn = lambda x: x[1][2][2]
	# grab_list = [1,2,2]
	# print(my_val)
	# update_by_key_ind_list(my_val, [1,2,2], lambda x: x * 10)
	# print(my_val)

	# # other_check = my_val[1][2]
	# to_check = grab_fnxn(my_val)
	# # to_check = grab_fnxn(dict_list)
	# print(to_check)
	# # to_check[2] = 30
	# my_val[1][2][2] = 30
	# # vs
	# print(to_check)

	# # to_check[1] = 10
	# # other_check = [6,5,4]

	# print(my_val)




	# ####################################################################
	# ######### Very, very thorough asterisk autoformatting test #########
	# ####################################################################

	# 
		# width_height_list = [(1,1),(1,2),(2,1),(2,2),(3,1),(1,3)]
		# outer_loc_list = []
		# for this_size in width_height_list:
		# 	outer_loc_list.append([
		# 							(0,0,0+this_size[0],0+this_size[1]), 
		# 							(0,5,0+this_size[0],5+this_size[1]), 
		# 							(5,0,5+this_size[0],0+this_size[1]), 
		# 							(5,5,5+this_size[0],5+this_size[1])
		# 						])

		# # box_locations = [
		# # 	[(0,0,1,1), (0,2,1,3), (2,0,3,1), (2,2,3,3)],
		# # 	[(0,0,1,1), (0,2,1,3), (2,0,3,1), (2,2,3,3)],
		# # 	[(0,0,1,1), (0,2,1,3), (2,0,3,1), (2,2,3,3)],
		# # 	[(0,0,1,1), (0,2,1,3), (2,0,3,1), (2,2,3,3)],
		# # 	[(0,0,1,1), (0,2,1,3), (2,0,3,1), (2,2,3,3)],
		# # 	[(0,0,1,1), (0,2,1,3), (2,0,3,1), (2,2,3,3)],
		# # ]

		# # settings:
		# 	# fontsize = 50, which_buffer='bottom', 
		# 	# min_fontsize=10, min_vbuffer=0.5, min_hbuffer=0.25, color='k', 
		# 	# weight='semibold', zorder=2
		# alignments = ['left', 'right', 'top', 'bottom']
		# # alignments = ['right', 'top', 'bottom']
		# fontsizes = [5,50,500]
		# pref_buffers = [0.1,1,10,100]
		# pref_buffers.reverse()
		# min_vbuffers = [0.1,1,10,100]
		# # min_vbuffers = [0.1]
		# min_hbuffers = [0.1,1,10,100]
		# rem_list = ['left', 'right', 'top', 'bottom', 'none']
		# keep_only_ind = None

		# if False:
		# 	align = 'left'
		# 	this_rem = 'right'
		# 	fontsize = 50
		# 	pref_buffer = 0.1
		# 	min_vbuf = 0.1
		# 	min_hbuf = 10



		# 	this_loc_list = copy.deepcopy(outer_loc_list)
		# 	if this_rem == 'left' or this_rem == 'both':
		# 		# print_rem = 'left, bottom'
		# 		for ind1 in range(len(this_loc_list)):
		# 			for ind2 in range(len(this_loc_list[ind1])):
		# 				this_loc_list[ind1][ind2] = list(this_loc_list[ind1][ind2])
		# 				# print(this_loc_list[ind1][ind2])
		# 				this_loc_list[ind1][ind2][0] = None
		# 				# this_loc_list[ind1][ind2][1] = None
		# 	if this_rem == 'right' or this_rem == 'both':
		# 		# print_rem = 'right, top'
		# 		for ind1 in range(len(this_loc_list)):
		# 			for ind2 in range(len(this_loc_list[ind1])):
		# 				this_loc_list[ind1][ind2] = list(this_loc_list[ind1][ind2])
		# 				this_loc_list[ind1][ind2][2] = None
		# 				# this_loc_list[ind1][ind2][3] = None
		# 	if this_rem == 'bottom' or this_rem == 'both':
		# 		# print_rem = 'left, bottom'
		# 		for ind1 in range(len(this_loc_list)):
		# 			for ind2 in range(len(this_loc_list[ind1])):
		# 				this_loc_list[ind1][ind2] = list(this_loc_list[ind1][ind2])
		# 				# print(this_loc_list[ind1][ind2])
		# 				# this_loc_list[ind1][ind2][0] = None
		# 				this_loc_list[ind1][ind2][1] = None
		# 	if this_rem == 'top' or this_rem == 'both':
		# 		# print_rem = 'right, top'
		# 		for ind1 in range(len(this_loc_list)):
		# 			for ind2 in range(len(this_loc_list[ind1])):
		# 				this_loc_list[ind1][ind2] = list(this_loc_list[ind1][ind2])
		# 				# this_loc_list[ind1][ind2][2] = None
		# 				this_loc_list[ind1][ind2][3] = None



		# 	print(colored('fontsize: {:<4}, which_buffer: {:<6}, is None: {:<6}, pref_buffer: {:<4}, min_vbuffer: {:<4}, min_hbuffer: {:<4}'
		# 		.format(fontsize, align, this_rem, pref_buffer, min_vbuf, min_hbuf), 'red'))
		# 	my_fig = hfnxns.easy_fig(figsize=(9,9))
		# 	my_fig.add_panel(top_left=(0,0), bottom_right=(1,1),x_count=3,y_count=2, subpanel_pad_percent=10)
		# 	failed_list = []
		# 	for loc_loop, loc_list in enumerate(outer_loc_list):
		# 		if keep_only_ind is None: 
		# 			any_failed = test_sterisk_buffer(my_fig, my_fig.axes[0][loc_loop], loc_list,
		# 				fontsize=fontsize,
		# 				which_buffer=align,
		# 				pref_buffer=pref_buffer,
		# 				min_vbuffer=min_vbuf,
		# 				min_hbuffer=min_hbuf,
		# 				loop_ind=loc_loop,
		# 				failed_list=failed_list,
		# 				color='k',
		# 				just_ind=3,
		# 				ast_list=this_loc_list[loc_loop])
		# 			failed_list.append(any_failed)
		# 		elif keep_only_ind == loc_loop:
		# 			any_failed = test_sterisk_buffer(my_fig, my_fig.axes[0][loc_loop], loc_list,
		# 				fontsize=fontsize,
		# 				which_buffer=align,
		# 				pref_buffer=pref_buffer,
		# 				min_vbuffer=min_vbuf,
		# 				min_hbuffer=min_hbuf,
		# 				loop_ind=loc_loop,
		# 				failed_list=failed_list,
		# 				color='k',
		# 				just_ind=3,
		# 				ast_list=this_loc_list[loc_loop])
		# 			failed_list.append(any_failed)
		# 		elif keep_only_ind != loc_loop:
		# 			my_fig.axes[0][loc_loop].remove()
		# 		# raise
		# 	# if any(failed_list):
		# 	# 	plt.close()
		# 		# continue
		# 	buf_obj = fig2data(my_fig.fig)
		# 	# print(np.array_equal(buf_obj, fig2data(my_fig.fig)))
		# 	# print(type(buf_obj))
		# 	plt.show()
		# 	plt.close()
		# 	raise

		# # for settings in settings_list:
		# # did_larger = True
		# last_fig_array = None
		# for align in alignments:
		# 	for fontsize in fontsizes:
		# 		for pref_buffer in pref_buffers:
		# 			for min_vbuf in min_vbuffers:
		# 				# prev_did_larger = did_larger
		# 				if align in ['bottom', 'top'] and min_vbuf > pref_buffer:
		# 					# did_larger = False
		# 					continue
		# 				# else:
		# 				# 	did_larger = True
		# 				for min_hbuf in min_hbuffers:
		# 					# prev_did_larger = did_larger
		# 					# print(did_larger, prev_did_larger)
		# 					if align in ['left', 'right'] and min_hbuf > pref_buffer:
		# 						# did_larger = False
		# 						continue
		# 					# else:
		# 					# 	did_larger = True
		# 					this_rem = rem_list.copy()
		# 					this_rem.remove(align)
		# 					for this_rem in this_rem:
		# 						this_loc_list = copy.deepcopy(outer_loc_list)
		# 						if this_rem == 'left' or this_rem == 'both':
		# 							# print_rem = 'left, bottom'
		# 							for ind1 in range(len(this_loc_list)):
		# 								for ind2 in range(len(this_loc_list[ind1])):
		# 									this_loc_list[ind1][ind2] = list(this_loc_list[ind1][ind2])
		# 									# print(this_loc_list[ind1][ind2])
		# 									this_loc_list[ind1][ind2][0] = None
		# 									# this_loc_list[ind1][ind2][1] = None
		# 						if this_rem == 'right' or this_rem == 'both':
		# 							# print_rem = 'right, top'
		# 							for ind1 in range(len(this_loc_list)):
		# 								for ind2 in range(len(this_loc_list[ind1])):
		# 									this_loc_list[ind1][ind2] = list(this_loc_list[ind1][ind2])
		# 									this_loc_list[ind1][ind2][2] = None
		# 									# this_loc_list[ind1][ind2][3] = None
		# 						if this_rem == 'bottom' or this_rem == 'both':
		# 							# print_rem = 'left, bottom'
		# 							for ind1 in range(len(this_loc_list)):
		# 								for ind2 in range(len(this_loc_list[ind1])):
		# 									this_loc_list[ind1][ind2] = list(this_loc_list[ind1][ind2])
		# 									# print(this_loc_list[ind1][ind2])
		# 									# this_loc_list[ind1][ind2][0] = None
		# 									this_loc_list[ind1][ind2][1] = None
		# 						if this_rem == 'top' or this_rem == 'both':
		# 							# print_rem = 'right, top'
		# 							for ind1 in range(len(this_loc_list)):
		# 								for ind2 in range(len(this_loc_list[ind1])):
		# 									this_loc_list[ind1][ind2] = list(this_loc_list[ind1][ind2])
		# 									# this_loc_list[ind1][ind2][2] = None
		# 									this_loc_list[ind1][ind2][3] = None
		# 						# hfnxns.quick_dumps(this_loc_list)
		# 						# raise
		# 						print(colored('fontsize: {:<4}, which_buffer: {:<6}, is None: {:<6}, pref_buffer: {:<4}, min_vbuffer: {:<4}, min_hbuffer: {:<4}'
		# 							.format(fontsize, align, this_rem, pref_buffer, min_vbuf, min_hbuf), 'red'))
		# 					# if prev_did_larger is True:
		# 						my_fig = hfnxns.easy_fig(figsize=(9,9))
		# 						my_fig.add_panel(top_left=(0,0), bottom_right=(1,1),x_count=3,y_count=2, subpanel_pad_percent=10)
		# 						failed_list = []
		# 						for loc_loop, loc_list in enumerate(outer_loc_list):
		# 							any_failed = test_sterisk_buffer(my_fig, my_fig.axes[0][loc_loop], loc_list,
		# 								fontsize=fontsize,
		# 								which_buffer=align,
		# 								pref_buffer=pref_buffer,
		# 								min_vbuffer=min_vbuf,
		# 								min_hbuffer=min_hbuf,
		# 								loop_ind=loc_loop,
		# 								failed_list=failed_list,
		# 								ast_list=this_loc_list[loc_loop])
		# 							failed_list.append(any_failed)
		# 							# raise
		# 						if any(failed_list):
		# 							plt.close()
		# 							continue
		# 						# fig_array = fig2data(my_fig.fig)
		# 						# print(fig_array)
		# 						# if last_fig_array is not None:
		# 						# 	if np.array_equal(fig_array, last_fig_array):
		# 						plt.show()
		# 						plt.close()
		# 						# 	else:
		# 						# 		print('was same')
		# 						# 		plt.close()
		# 						# else:
		# 						# 	plt.show()
		# 						# 	plt.close()
		# 						# last_fig_array = fig_array
		# 					# else:
		# 					# 	# print(colored('fontsize: {:<4}, which_buffer: {:<6}, is None: {:<6}, pref_buffer: {:<4}, min_vbuffer: {:<4}, min_hbuffer: {:<4}'
		# 					# 	# 	.format(fontsize, align, this_rem, pref_buffer, min_vbuf, min_hbuf), 'red'))
		# 					# 	print('\talready did larger')

	# ####################################################################
	# ######### Very, very thorough asterisk autoformatting test #########
	# ####################################################################





	#
		# size = 1000
		# fontsize = 40
		# markerx = 0.7
		# markery = 0.5
		# my_fig.add_panel(top_left=(0,0), bottom_right=(1,1),x_count=3,y_count=2)
		# check_text_plot(my_fig, my_fig.axes[0],'*', fontsize=fontsize)
		# my_fig.axes[0][0].scatter([0,1,0,1], [0,0,1,1], c='k')
		# obj1, width1, height1 = marker_real_bbox(my_fig.fig, my_fig.axes[0][0], markerx, markery, c='C5',zorder=9,s=size,
		# 	marker=(6,2,0))
		# # add_dots(my_fig.fig, my_fig.axes[0][0], markerx, markery, width1, height1)
		# my_fig.axes[0][1].scatter([0,1,0,1], [0,0,1,1], c='k')
		# obj2, width2, height2 = marker_real_bbox(my_fig.fig, my_fig.axes[0][1], markerx, markery, c='C5',zorder=9,s=size,
		# 	marker=(6,2,0))
		# # add_dots(my_fig.fig, my_fig.axes[0][1], markerx, markery, width2, height2)
		# my_fig.axes[0][2].scatter([0,1,0,1], [0,0,1,1], c='k')
		# obj3, width3, height3 = marker_real_bbox(my_fig.fig, my_fig.axes[0][2], markerx, markery, c='C5',zorder=9,s=size,
		# 	marker=(6,2,0))
		# # add_dots(my_fig.fig, my_fig.axes[0][2], markerx, markery, width3, height3)
		# my_fig.axes[0][3].scatter([0,1,0,1], [0,0,1,1], c='k')
		# obj4, width4, height4 = marker_real_bbox(my_fig.fig, my_fig.axes[0][3], markerx, markery, c='C5',zorder=9,s=size,
		# 	marker=(6,2,0))
		# # add_dots(my_fig.fig, my_fig.axes[0][3], markerx, markery, width4, height4)
		# my_fig.axes[0][4].scatter([0,1,0,1], [0,0,1,1], c='k')
		# obj5, width5, height5 = marker_real_bbox(my_fig.fig, my_fig.axes[0][4], markerx, markery, c='C5',zorder=9,s=size,
		# 	marker=(6,2,0))
		# # add_dots(my_fig.fig, my_fig.axes[0][4], markerx, markery, width5, height5)
		# my_fig.axes[0][5].scatter([0,1,0,1], [0,0,1,1], c='k')
		# obj6, width6, height6 = marker_real_bbox(my_fig.fig, my_fig.axes[0][5], markerx, markery, c='C5',zorder=9,s=size,
		# 	marker=(6,2,0))
		# plt.savefig('test_symbols_plot.png', dpi=300)
		# plt.close()
		# add_dots(my_fig.fig, my_fig.axes[0][5], 0.5, 0.5, width6, height6)
		# obj1, width1, height1 = marker_real_bbox(my_fig.fig, my_fig.axes[0][0], markerx, markery, c='C5',zorder=9,s=size,marker='+')
		# # add_dots(my_fig.fig, my_fig.axes[0][0], markerx, markery, width1, height1)
		# my_fig.axes[0][1].scatter([0,1,0,1], [0,0,1,1], c='k')
		# obj2, width2, height2 = marker_real_bbox(my_fig.fig, my_fig.axes[0][1], markerx, markery, c='C5',zorder=9,s=size,marker='*')
		# # add_dots(my_fig.fig, my_fig.axes[0][1], markerx, markery, width2, height2)
		# my_fig.axes[0][2].scatter([0,1,0,1], [0,0,1,1], c='k')
		# obj3, width3, height3 = marker_real_bbox(my_fig.fig, my_fig.axes[0][2], markerx, markery, c='C5',zorder=9,s=size,marker=(6,2,0))
		# # add_dots(my_fig.fig, my_fig.axes[0][2], markerx, markery, width3, height3)
		# my_fig.axes[0][3].scatter([0,1,0,1], [0,0,1,1], c='k')
		# obj4, width4, height4 = marker_real_bbox(my_fig.fig, my_fig.axes[0][3], markerx, markery, c='C5',zorder=9,s=size,marker='.')
		# # add_dots(my_fig.fig, my_fig.axes[0][3], markerx, markery, width4, height4)
		# my_fig.axes[0][4].scatter([0,1,0,1], [0,0,1,1], c='k')
		# obj5, width5, height5 = marker_real_bbox(my_fig.fig, my_fig.axes[0][4], markerx, markery, c='C5',zorder=9,s=size,marker='o')
		# # add_dots(my_fig.fig, my_fig.axes[0][4], markerx, markery, width5, height5)
		# my_fig.axes[0][5].scatter([0,1,0,1], [0,0,1,1], c='k')
		# obj6, width6, height6 = marker_real_bbox(my_fig.fig, my_fig.axes[0][5], markerx, markery, c='C5',zorder=9,s=size,marker=',')
		# # add_dots(my_fig.fig, my_fig.axes[0][5], 0.5, 0.5, width6, height6)

	# ####################################################################
	# ###################### Testing mpl marker bbox #####################
	# ####################################################################
	#
		# my_fig = hfnxns.easy_fig(figsize=(9,9))
		# size = 1000
		# my_fig.add_panel(top_left=(0,0), bottom_right=(1,1),x_count=3,y_count=2)
		# my_fig.axes[0][0].scatter([0,1,0,1], [0,0,1,1], c='k')
		# obj1 = my_fig.axes[0][0].scatter(0.5, 0.5, c='C5',zorder=9,s=size,marker='+')
		# my_fig.axes[0][1].scatter([0,1,0,1], [0,0,1,1], c='k')
		# obj2 = my_fig.axes[0][1].scatter(0.5, 0.5, c='C5',zorder=9,s=size,marker='*')
		# my_fig.axes[0][2].scatter([0,1,0,1], [0,0,1,1], c='k')
		# obj3 = my_fig.axes[0][2].scatter(0.5, 0.5, c='C5',zorder=9,s=size,marker=(6,2,0))
		# my_fig.axes[0][3].scatter([0,1,0,1], [0,0,1,1], c='k')
		# obj4 = my_fig.axes[0][3].scatter(0.5, 0.5, c='C5',zorder=9,s=size,marker='.')
		# my_fig.axes[0][4].scatter([0,1,0,1], [0,0,1,1], c='k')
		# obj5 = my_fig.axes[0][4].scatter(0.5, 0.5, c='C5',zorder=9,s=size,marker='o')
		# my_fig.axes[0][5].scatter([0,1,0,1], [0,0,1,1], c='k')
		# obj6 = my_fig.axes[0][5].scatter(0.5, 0.5, c='C5',zorder=9,s=size,marker=',')

		# multi=3
		# obj21, bbox21, obj31, bbox31 = add_other_positions(my_fig.fig, my_fig.axes[0][0], obj1, size, '+', multi)
		# obj22, bbox22, obj32, bbox32 = add_other_positions(my_fig.fig, my_fig.axes[0][1], obj2, size, '*', multi)
		# obj23, bbox23, obj33, bbox33 = add_other_positions(my_fig.fig, my_fig.axes[0][2], obj3, size, (6,2,0), multi)
		# obj24, bbox24, obj34, bbox34 = add_other_positions(my_fig.fig, my_fig.axes[0][3], obj4, size, '.', multi)
		# obj25, bbox25, obj35, bbox35 = add_other_positions(my_fig.fig, my_fig.axes[0][4], obj5, size, 'o', multi)
		# obj26, bbox26, obj36, bbox36 = add_other_positions(my_fig.fig, my_fig.axes[0][5], obj6, size, ',', multi)
	# ####################################################################
	# ###################### Testing mpl marker bbox #####################
	# ####################################################################

	# old code
		# bbox1 = get_plot_item_bbox(my_fig.fig,my_fig.axes[0][0],obj1)
		# height1 = bbox1[1][1] - bbox1[1][0]
		# obj21 = my_fig.axes[0][0].scatter(0.5, 0.5-(height1*multi), c='C5',zorder=9,s=200,marker='+')
		# bbox21 = get_plot_item_bbox(my_fig.fig,my_fig.axes[0][0],obj21)

		# bbox2 = get_plot_item_bbox(my_fig.fig,my_fig.axes[0][1],obj2)
		# height2 = bbox2[1][1] - bbox2[1][0]
		# obj22 = my_fig.axes[0][1].scatter(0.5, 0.5-(height2*multi), c='C5',zorder=9,s=200,marker='*')
		# bbox22 = get_plot_item_bbox(my_fig.fig,my_fig.axes[0][1],obj22)

		
		# bbox3 = get_plot_item_bbox(my_fig.fig,my_fig.axes[0][2],obj3)
		# height3 = bbox3[1][1] - bbox3[1][0]
		# obj23 = my_fig.axes[0][2].scatter(0.5, 0.5-(height3*multi), c='C5',zorder=9,s=200,marker=(6,2,0))
		# bbox23 = get_plot_item_bbox(my_fig.fig,my_fig.axes[0][2],obj23)

		
		# bbox4 = get_plot_item_bbox(my_fig.fig,my_fig.axes[0][3],obj4)
		# height4 = bbox4[1][1] - bbox4[1][0]
		# obj24 = my_fig.axes[0][3].scatter(0.5, 0.5-(height4*multi), c='C5',zorder=9,s=200,marker='.')
		# bbox24 = get_plot_item_bbox(my_fig.fig,my_fig.axes[0][3],obj24)

		
		# bbox5 = get_plot_item_bbox(my_fig.fig,my_fig.axes[0][4],obj5)
		# height5 = bbox5[1][1] - bbox5[1][0]
		# obj25 = my_fig.axes[0][4].scatter(0.5, 0.5-(height5*multi), c='C5',zorder=9,s=200,marker='o')
		# bbox25 = get_plot_item_bbox(my_fig.fig,my_fig.axes[0][4],obj25)

		
		# bbox6 = get_plot_item_bbox(my_fig.fig,my_fig.axes[0][5],obj6)
		# height6 = bbox6[1][1] - bbox6[1][0]
		# obj26 = my_fig.axes[0][5].scatter(0.5, 0.5-(height6*multi), c='C5',zorder=9,s=200,marker=',')
		# bbox26 = get_plot_item_bbox(my_fig.fig,my_fig.axes[0][5],obj26)


	# plt.show()
	# plt.close()
	# raise

	# ####################################################################
	# ######################### Making check_ast #########################
	# ####################################################################

	#
		# fontsize=20
		# # my_fig.add_panel(top_left=(0,0), bottom_right=(0.45,0.45),x_count=3,y_count=2)
		# my_fig.add_panel(top_left=(0,0), bottom_right=(1,1),x_count=3,y_count=2)
		# first_vals = check_ast(my_fig, my_fig.axes[0][0], fontsize=5)[0]
		# # print(first_vals)
		# print(1)
		# print(round(check_ast(my_fig, my_fig.axes[0][1], fontsize=10)[0]/first_vals/2,2))
		# print(round(check_ast(my_fig, my_fig.axes[0][2], fontsize=20)[0]/first_vals/4,2))
		# print(round(check_ast(my_fig, my_fig.axes[0][3], fontsize=40)[0]/first_vals/8,2))
		# print(round(check_ast(my_fig, my_fig.axes[0][4], fontsize=80)[0]/first_vals/16,2))
		# print(round(check_ast(my_fig, my_fig.axes[0][5], fontsize=160)[0]/first_vals/32,2))
		# # check_text_plot(my_fig, my_fig.axes[0],'*')
		# # my_fig.add_panel(top_left=(0.55,0), bottom_right=(1,0.45),x_count=3,y_count=2)
		# # check_text_plot(my_fig, my_fig.axes[1],'g')
		# # my_fig.add_panel(top_left=(0,0.55), bottom_right=(0.45,1),x_count=3,y_count=2)
		# # check_text_plot(my_fig, my_fig.axes[2],'l')
		# # my_fig.show()
		# plt.savefig('test_text_plot.png', dpi=300)
		# plt.close()
		# # my_fig.close()

		# pass

	# ####################################################################
	# ######################### Making check_ast #########################
	# ####################################################################

	# thing = None
	# if thing:
	# 	print('yes')

	# ####################################################################
	# #################### Checking Advanced Sorting #####################
	# ####################################################################
	#
		# a = [ \
		# 		[1,[5,3,9,53],63,67], \
		# 		[2,[4,4,7,55],64,57], \
		# 		[3,[3,5,8,56],65,47], \
		# 		[4,[2,6,4,57],66,37], \
		# 	]

		# print('')
		# print('\n'.join([str(thing) for thing in sorted(a, key=lambda x: x[1][2])]))
		# print('')
		# print('\n'.join([str(thing) for thing in sorted(a, key=operator.itemgetter(2))]))
		# print('')
		# print('\n'.join([str(thing) for thing in sorted(a, key=operator.itemgetter(3))]))

	# ####################################################################
	# #################### Checking Advanced Sorting #####################
	# ####################################################################
	# raise






	# raise

	# ####################################################################
	# ################### Checking Peak Gene Overlaps ####################
	# ####################################################################
	
	# 
		# new_tss_gene_file = '/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_04_16/E/length_change_pipeline_dirs/referencing_NonCancer/length_change_analysis/lengthened_peaks_analysis/no_cutoff/tss_intersect_gene_names/control/LUNG.bed'
		# # new_tss_gene_file = '/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_04_16/E/core_pipeline_dirs/4kb_broad/tss_intersect_gene_names/test/LUNG.bed'
		# old_tss_gene_file = '/Users/jschulz1/Documents/project_data/ccle_project/analyses/2020_01_27test_run_2020_01_27_windows_compatible/len_change/most_lengthen_shorten/tss_gene_lists/normal/LUNG_lengthened_tss_gene_names.bed'
		# # old_tss_gene_file = '/Users/jschulz1/Documents/project_data/ccle_project/analyses/2019_11_18-full_4kb_vs_1kb_run/full_4kb_vs_1kb_run/4kb_25_50/tss_intersect_gene_names/LUNG_4kb_25_50_tss_gene_names.bed'

		# new_genes = []
		# with open(new_tss_gene_file, 'r') as readfile:
		# 	for line in readfile:
		# 		new_genes.append(line.strip('\n').split('\t'))
		# 		new_genes[-1][1] = int(new_genes[-1][1])
		# 		new_genes[-1][2] = int(new_genes[-1][2])
		# old_genes = []
		# with open(old_tss_gene_file, 'r') as readfile:
		# 	for line in readfile:
		# 		old_genes.append(line.strip('\n').split('\t'))
		
		# print_list, final_print_list = hfnxns.compare_peak_list_col_values(
		# 	hfnxns.merge_peak_genes(new_genes, gene_col=4), hfnxns.merge_peak_genes(old_genes, gene_col=7), keep_over_or_nonover='over', 
		# 	chr_index=0, peak_min_index=1, peak_max_index=2, overlap_range=0, return_unique=True)

		# hfnxns.write_bed('len_change_len_overlapped_test_peaks.bed', print_list)
		# hfnxns.write_bed('len_change_len_overlapped_test_peaks_unique.bed', final_print_list)

		# peak_lists = [[],[],[]]
		# with open('len_change_len_overlapped_test_peaks_unique.bed', 'r') as readfile:
		# 	for line in readfile:
		# 		line = line.strip('\n').split('\t')
		# 		if line[3] in ['[None]', '[]'] and line[4] != '[]':
		# 			peak_lists[1].append(line)
		# 		elif line[4] in ['[None]', '[]'] and line[3] != '[]':
		# 			peak_lists[2].append(line)
		# 		else:
		# 			peak_lists[0].append(line)
		# # print(peak_lists[0][0])
		# # print(peak_lists[1][0])
		# # print(peak_lists[2][0])

		# # print([len(thing) for thing in peak_lists])
		# hfnxns.write_bed('len_change_len_overlapped_test_peaks_both.bed', peak_lists[0])
		# hfnxns.write_bed('len_change_len_overlapped_test_peaks_old.bed', peak_lists[1])
		# hfnxns.write_bed('len_change_len_overlapped_test_peaks_new.bed', peak_lists[2])

	# ####################################################################
	# ################### Checking Peak Gene Overlaps ####################
	# ####################################################################




	# print(hfnxns.mean([467, hfnxns.mean([297, 1509]), 972, hfnxns.mean([548, 1262, 1213]), 9648, 
	# 	hfnxns.mean([1980, 2275, 378]), 10258]))

	# tss_file_path = '/Users/jschulz1/Documents/project_data/ccle_project/raw_data/used_in_analysis/new_tss_spot_gene.bed'
	# # print('\trunning this 10000')
	# make_tss_beds(tss_file_path, 'tss_bed_file.bed', 10000)
	# # print('\trunning this 10')
	# make_tss_beds(tss_file_path, 'tss_bed_file_just_tss.bed', 10)
	# raise

	# dicts = {'a':1, 'b':2, 'c':3}
	# print([thing for thing in dicts])
	# print(dicts)
	# raise

	# a = set([1,2,3])
	# b = set([1,2,3,4,5])
	# print(a.difference(b), len(a.difference(b)))
	# raise


	# ####################################################################
	# ################### Checking Peak Gene Overlaps ####################
	# ####################################################################

	#
		# new_dict = {
		# 	'TSG': set(['TSG', 'TSG, fusion', 'oncogene, TSG']),
		# 	'Oncogene': set(['oncogene', 'oncogene, TSG', 'oncogene, TSG, fusion', 'oncogene, fusion']),
		# 	'Neither': set(['', 'fusion']),
		# 	None: set([None])
		# }

		# 16
		# [4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4]
		# [[35, 53, 17, 2061], [50, 42, 21, 6138], [12, 32, 8, 855], [102, 100, 62, 10713], [4, 14, 2, 434], \
		# [97, 102, 53, 8620], [12, 28, 4, 912], [126, 110, 72, 10543], [113, 134, 57, 7290], [44, 75, 28, 6856], \
		# [71, 90, 33, 3900], [47, 70, 28, 6257], [15, 34, 7, 1333], [161, 145, 88, 14707], [105, 143, 71, 8547], [39, 77, 30, 6492]]
	

	# ####################################################################
	# ####################################################################
	# ##################### Checking Cancer Plot #s ######################
	# ####################################################################

	#
		# tuson_or_cosmic = 'cosmic'
		# len_or_len_change = 'len_change'
		# new_genes = True





		# if tuson_or_cosmic == 'cosmic':
		# 	subgroup_dict = {
		# 		'TSG': 'TSG',
		# 		'TSG, fusion': 'TSG',
		# 		'oncogene, TSG': 'TSG',
		# 		'oncogene, TSG, fusion': 'TSG',
		# 		'oncogene': 'Oncogene',
		# 		'oncogene, TSG': 'Oncogene',
		# 		'oncogene, TSG, fusion': 'Oncogene',
		# 		'oncogene, fusion': 'Oncogene',
		# 		'': 'Neither',
		# 		'fusion': 'Neither',
		# 		None: None
		# 	}
		# 	group_names = ['TSG', 'Oncogene', 'Neither', None]
		# 	relevant_list = '/Users/jschulz1/Documents/project_scripts/ccle_project/chipseq_broadpeak_pipeline/cosmic_relevant_list.bed'

		# if tuson_or_cosmic == 'tuson':
		# 	subgroup_dict = {
		# 		'TSG': 'TSG',
		# 		'oncogene': 'Oncogene',
		# 	}
		# 	group_names = ['TSG', 'Oncogene', None]
		# 	relevant_list = '/Users/jschulz1/Documents/project_scripts/ccle_project/chipseq_broadpeak_pipeline/tuson_relevant_list.bed'





		# if len_or_len_change == 'len':
		# 	types = ['BRCA', 'COAD', 'GBMC', 'LUNG', 'OVCA', 'PAAD', 'SCRC', 'SKCM']
		# 	list_order = [
		# 		('BRCA', '4kb_broad'), ('BRCA', '1kb_sharp'), ('COAD', '4kb_broad'), ('COAD', '1kb_sharp'), \
		# 		('GBMC', '4kb_broad'), ('GBMC', '1kb_sharp'), ('LUNG', '4kb_broad'), ('LUNG', '1kb_sharp'), \
		# 		('OVCA', '4kb_broad'), ('OVCA', '1kb_sharp'), ('PAAD', '4kb_broad'), ('PAAD', '1kb_sharp'), \
		# 		('SCRC', '4kb_broad'), ('SCRC', '1kb_sharp'), ('SKCM', '4kb_broad'), ('SKCM', '1kb_sharp')
		# 	]
		
		# 	old_types = [
		# 					"BRCA", "BRCA", "COAD", "COAD", "E", "E", "GBMC", \
		# 					"GBMC", "LUNG", "LUNG", "OVCA", "OVCA", "PAAD", \
		# 					"PAAD", "SCRC", "SCRC", "SKCM", "SKCM", "all", "all" \
		# 				]

		# 	# if new_genes:
		# 	bed_dict_input = {
		# 		'1kb_sharp': '/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_04_16/E/core_pipeline_dirs/1kb_sharp/gene_lists/test',
		# 		'4kb_broad': '/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_04_16/E/core_pipeline_dirs/4kb_broad/gene_lists/test'
		# 	}
		# 	old_bed_dict_input = {
		# 		'1kb_sharp': '/Users/jschulz1/Documents/project_data/ccle_project/analyses/2019_11_18-full_4kb_vs_1kb_run/full_4kb_vs_1kb_run/1kb_sharp_25_50/gene_lists',
		# 		'4kb_broad': '/Users/jschulz1/Documents/project_data/ccle_project/analyses/2019_11_18-full_4kb_vs_1kb_run/full_4kb_vs_1kb_run/4kb_25_50/gene_lists'
		# 	}

		# if len_or_len_change == 'len_change':
		# 	types = ['BRCA', 'COAD', 'GBMC', 'LUNG', 'OVCA', 'SKCM']
		# 	list_order = [('BRCA', 'len'), ('BRCA', 'short'), ('BRCA', 'neither'), ('COAD', 'len'), ('COAD', 'short'), \
		# 	('COAD', 'neither'), ('GBMC', 'len'), ('GBMC', 'short'), ('GBMC', 'neither'), ('LUNG', 'len'), ('LUNG', 'short'), \
		# 	('LUNG', 'neither'), ('OVCA', 'len'), ('OVCA', 'short'), ('OVCA', 'neither'), ('SKCM', 'len'), ('SKCM', 'short'), \
		# 	('SKCM', 'neither')
		# 	]
		# 	old_types = None

		# 	# if new_genes:
		# 	bed_dict_input = {
		# 		'len': '/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_04_16/E/length_change_pipeline_dirs/referencing_NonCancer/length_change_analysis/lengthened_peaks_analysis/no_cutoff/gene_lists/control',
		# 		'short': '/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_04_16/E/length_change_pipeline_dirs/referencing_NonCancer/length_change_analysis/shortened_peaks_analysis/no_cutoff/gene_lists/control',
		# 		'neither': '/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_04_16/E/length_change_pipeline_dirs/referencing_NonCancer/length_change_analysis/no_change_peaks_analysis/no_cutoff/gene_lists/control'
		# 	}
		# 	# old_bed_dict_input = {
		# 	# 	'len': '/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_04_16/E/length_change_pipeline_dirs/referencing_NonCancer/length_change_analysis/lengthened_peaks_analysis/no_cutoff/gene_lists/control',
		# 	# 	'short': '/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_04_16/E/length_change_pipeline_dirs/referencing_NonCancer/length_change_analysis/shortened_peaks_analysis/no_cutoff/gene_lists/control',
		# 	# 	'neither': '/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_04_16/E/length_change_pipeline_dirs/referencing_NonCancer/length_change_analysis/no_change_peaks_analysis/no_cutoff/gene_lists/control'
		# 	# }

		# if tuson_or_cosmic == 'tuson' and len_or_len_change == 'len':
		# 	peak_list1 = '/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_04_16/E/data_for_figures/cancer_plots/len/len_tuson'
		# 	peak_list2 = '/Users/jschulz1/Documents/project_data/ccle_project/analyses/2019_11_18-full_4kb_vs_1kb_run/full_4kb_vs_1kb_run/data_for_figures/cancer_barplots/len_tuson'
		# if tuson_or_cosmic == 'cosmic' and len_or_len_change == 'len':
		# 	peak_list1 = '/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_04_16/E/data_for_figures/cancer_plots/len/len_cosmic'
		# 	peak_list2 = '/Users/jschulz1/Documents/project_data/ccle_project/analyses/2019_11_18-full_4kb_vs_1kb_run/full_4kb_vs_1kb_run/data_for_figures/cancer_barplots/len_cosmic'

		# if tuson_or_cosmic == 'tuson' and len_or_len_change == 'len_change':
		# 	peak_list1 = '/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_04_16/E/data_for_figures/cancer_plots/len_change_no_cutoff/len_change_tuson'
		# 	peak_list2 = '/Users/jschulz1/Documents/project_data/ccle_project/analyses/2019_11_18-full_4kb_vs_1kb_run/full_4kb_vs_1kb_run/data_for_figures/cancer_barplots/len_change_tuson'
		# if tuson_or_cosmic == 'cosmic' and len_or_len_change == 'len_change':
		# 	peak_list1 = '/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_04_16/E/data_for_figures/cancer_plots/len_change_no_cutoff/len_change_cosmic'
		# 	peak_list2 = '/Users/jschulz1/Documents/project_data/ccle_project/analyses/2019_11_18-full_4kb_vs_1kb_run/full_4kb_vs_1kb_run/data_for_figures/cancer_barplots/len_change_cosmic'

		# check_cancer_plots(bed_dict_input, 
		# 		peak_list1, 
		# 		peak_list2,
		# 		relevant_list,
		# 		group_names,
		# 		old_types,
		# 		types,
		# 		list_order,
		# 		subgroup_dict)

	# ####################################################################
	# ##################### Checking Cancer Plot #s ######################
	# ####################################################################
	# ####################################################################

	# ####################################################################
	# subgroup_dict = {
	# 	'TSG': 'TSG',
	# 	'oncogene': 'Oncogene',
	# }

	# types = ['BRCA', 'COAD', 'GBMC', 'LUNG', 'OVCA', 'PAAD', 'SCRC', 'SKCM']
	# list_order = [
	# 	('BRCA', '4kb_broad'), ('BRCA', '1kb_sharp'), ('COAD', '4kb_broad'), ('COAD', '1kb_sharp'), \
	# 	('GBMC', '4kb_broad'), ('GBMC', '1kb_sharp'), ('LUNG', '4kb_broad'), ('LUNG', '1kb_sharp'), \
	# 	('OVCA', '4kb_broad'), ('OVCA', '1kb_sharp'), ('PAAD', '4kb_broad'), ('PAAD', '1kb_sharp'), \
	# 	('SCRC', '4kb_broad'), ('SCRC', '1kb_sharp'), ('SKCM', '4kb_broad'), ('SKCM', '1kb_sharp')
	# ]

	# bed_dict_input = {
	# 			'1kb_sharp': '/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_04_16/E/core_pipeline_dirs/1kb_sharp/gene_lists/test',
	# 			'4kb_broad': '/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_04_16/E/core_pipeline_dirs/4kb_broad/gene_lists/test'
	# 		}

	# check_cancer_plots(bed_dict_input, 
	# 		'/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_04_16/E/data_for_figures/cancer_plots/len/len_cosmic', 
	# 		'/Users/jschulz1/Documents/project_data/ccle_project/analyses/2019_11_18-full_4kb_vs_1kb_run/full_4kb_vs_1kb_run/data_for_figures/cancer_barplots/len_cosmic',
	# 		'/Users/jschulz1/Documents/project_scripts/ccle_project/chipseq_broadpeak_pipeline/cosmic_relevant_list.bed',
	# 		['TSG', 'Oncogene', 'Neither', None],
	# 		None,
	# 		types,
	# 		list_order,
	# 		subgroup_dict)
	# ####################################################################

	# bed_dict = {}
	# broad_bed_dir = '/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_04_16/E/core_pipeline_dirs/4kb_broad/gene_lists/test'
	# original_broad_beds = sorted(list(glob.iglob(os.path.join(broad_bed_dir, '*'))))
	# sharp_bed_dir = '/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_04_16/E/core_pipeline_dirs/1kb_sharp/gene_lists/test'
	# original_sharp_beds = sorted(list(glob.iglob(os.path.join(sharp_bed_dir, '*'))))
	# bed_dict['1kb_sharp'] = {}
	# for filepath in original_sharp_beds:
	# 	bed_dict['1kb_sharp'][filepath.split('/')[-1].split('.')[0]] = filepath 





	# bed_dict = bed_file_dict(
	# 	{
	# 		'1kb_sharp': '/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_04_16/E/core_pipeline_dirs/1kb_sharp/gene_lists/test',
	# 		'4kb_broad': '/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_04_16/E/core_pipeline_dirs/4kb_broad/gene_lists/test'
	# 	}
	# )
	# # hfnxns.quick_dumps(bed_dict)
	# # raise
	# peak_plot_list = '/Users/jschulz1/Documents/project_scripts/ccle_project/chipseq_broadpeak_pipeline/cosmic_peak_plot_list.bed'
	# gene_list = []
	# with open(peak_plot_list, 'r') as readfile:
	# 	for line in readfile:
	# 		gene_list.append([json.loads(col) for col in line.strip('\n').replace("'",'"').split('\t')])
	# peak_plot_list = '/Users/jschulz1/Documents/project_data/ccle_project/analyses/2019_11_18-full_4kb_vs_1kb_run/full_4kb_vs_1kb_run/data_for_figures/cancer_barplots/len_cosmic'
	# with open(peak_plot_list, 'r') as readfile:
	# 	peak_dict = json.load(readfile)
	# 	# print(list(peak_dict.keys()))
	# 	old_gene_list = peak_dict.get('peak_list_to_plot')
	# 	old_gene_list = [this_list for list_loop, this_list in enumerate(old_gene_list) if old_types[list_loop] not in ['E', 'all']]
	# 	old_gene_list = [[[gene.lower() for gene in stuff] for stuff in thing] for thing in old_gene_list]
	# # print(len(gene_list))
	# # print([len(thing) for thing in gene_list])
	# # print([[len(stuff) for stuff in thing] for thing in gene_list])
	# # print(len(old_gene_list))
	# # print([len(thing) for thing in old_gene_list])
	# # print([[len(stuff) for stuff in thing] for thing in old_gene_list])
	# # raise
	# relevant_list = '/Users/jschulz1/Documents/project_scripts/ccle_project/chipseq_broadpeak_pipeline/cosmic_relevant_list.bed'
	# group_names = ['TSG', 'Oncogene', 'Neither', None]
	# relevant_dict = {}
	# with open(relevant_list, 'r') as readfile:
	# 	for line in readfile:
	# 		line = line.strip('\n').split('\t')
	# 		if len(line) > 2:
	# 			raise ValueError('this line: {}'.format(line))
	# 		if len(line) > 1:
	# 			relevant_dict[line[0].lower()] = line[1]
	# 		else:
	# 			print('line: {}'.format(line))

	# new_all_gene_counts, new_all_gene_lists, new_all_file_gene_counts, new_all_file_gene_lists = \
	# 		gene_list_dict(gene_list, list_order, bed_dict, subgroup_dict, relevant_dict, group_names)
	# old_all_gene_counts, old_all_gene_lists, old_all_file_gene_counts, old_all_file_gene_lists = \
	# 		gene_list_dict(old_gene_list, list_order, bed_dict, subgroup_dict, relevant_dict, group_names)

	# counts = [new_all_gene_counts, new_all_file_gene_counts, old_all_gene_counts, old_all_file_gene_counts]
	# lists = [new_all_gene_lists, new_all_file_gene_lists, old_all_gene_lists, old_all_file_gene_lists]
	# list_keys = ['{}-{}'.format(list_val[1], list_val[0]) for list_val in list_order]
	# for this_key in list_keys:
	# 	print(this_key)
	# 	print('\t', new_all_gene_counts.get(this_key))
	# 	# print('\t', new_all_file_gene_counts.get(this_key))
	# 	print('\t', old_all_file_gene_counts.get(this_key))
	# 	print('\t', ['{}: {}'.format(inner_key, len(list(new_all_gene_lists.get(this_key).get(inner_key)))) for \
	# 		inner_key in new_all_gene_lists.get(this_key).keys()])
	# 	print('\t', ['{}: {}'.format(inner_key, len(list(old_all_gene_lists.get(this_key).get(inner_key)))) for \
	# 		inner_key in old_all_gene_lists.get(this_key).keys()])
	# 	print('\t', old_all_gene_counts.get(this_key))
		# for 
	# print(list_keys)
	# print(list(new_all_gene_lists.keys()))

	# print(relevant_dict)
	# for bar_loop, bar in enumerate(gene_list):
	# 	# print('sample type: {}'.format(types[bar_loop % len(types)]))
	# 	print('sample type: {}, this_common: {}'.format(list_order[bar_loop][0], list_order[bar_loop][1]))

	# 	# print(list_order[bar_loop][0], list_order[bar_loop][1])
	# 	gene_file_path = bed_dict.get(list_order[bar_loop][1]).get(list_order[bar_loop][0])
	# 	with open(gene_file_path, 'r') as readfile:
	# 		file_genes = readfile.readlines()
	# 	file_genes = set([gene.strip('\n').lower() for gene in file_genes])
	# 	# print(len(file_genes))

	# 	for mini_bar_loop, mini_bar in enumerate(bar):
	# 		correct = 0
	# 		incorrect = 0
	# 		# inc_list = []
	# 		corr_list = []
	# 		group_dict = {'TSG': 0, 'Oncogene': 0, 'Neither': 0, None: 0}
	# 		group_dict_lists = {'TSG': [], 'Oncogene': [], 'Neither': [], None: []}
	# 		for gene_loop, gene in enumerate(mini_bar):
	# 			gene_tuple = (gene, relevant_dict.get(gene), subgroup_dict.get(relevant_dict.get(gene)))
	# 			# try:
	# 			# print(group_names[mini_bar_loop], subgroup_dict.get(relevant_dict.get(gene)))
	# 			# raise
	# 			# if group_names[mini_bar_loop] == subgroup_dict.get(relevant_dict.get(gene)):
	# 			# 	correct += 1
	# 			# 	corr_list.append(gene_tuple)
	# 			# else:
	# 			# 	incorrect += 1
	# 			group_dict[subgroup_dict.get(relevant_dict.get(gene))] += 1
	# 			group_dict_lists[subgroup_dict.get(relevant_dict.get(gene))].append(gene_tuple)
	# 			# if group_names[mini_bar_loop] is None:
	# 			# 	pass
	# 		if group_names[mini_bar_loop] is None:
	# 			non_none_count = 	sum([group_dict.get(key) for key in group_dict if key is not None])
	# 			all_count = 		sum([group_dict.get(key) for key in group_dict])
	# 			percent_dict = {key:round(group_dict.get(key)/all_count*100,1) for key in group_dict}
	# 			print('\tmini-bar type: {}, len: {}, genefile: {}, %s: {}'.format(group_names[mini_bar_loop], len(mini_bar), 
	# 				len(file_genes), percent_dict))
	# 			print('\t\tplot genes: {}, non-None: {}'.format(group_dict, non_none_count))
	# 			# print('\t\tcorrect count: {}, incorrect count: {}\n\t\tgenes: {}'.format(correct, incorrect, group_dict))
	# 	file_corr_list = []
	# 	file_group_dict = {'TSG': 0, 'Oncogene': 0, 'Neither': 0, None: 0}
	# 	file_group_dict_lists = {'TSG': [], 'Oncogene': [], 'Neither': [], None: []}
	# 	for gene_loop, gene in enumerate(file_genes):
	# 		gene_tuple = (gene, relevant_dict.get(gene), subgroup_dict.get(relevant_dict.get(gene)))
	# 		# print(gene)
	# 		# if gene_loop > 10:
	# 		# 	raise
	# 		# try:
	# 		# print(group_names[mini_bar_loop], subgroup_dict.get(relevant_dict.get(gene)))
	# 		# raise
	# 		# if group_names[mini_bar_loop] == subgroup_dict.get(relevant_dict.get(gene)):
	# 		# 	correct += 1
	# 		# 	file_corr_list.append(gene_tuple)
	# 		# else:
	# 			# incorrect += 1
	# 		file_group_dict[subgroup_dict.get(relevant_dict.get(gene))] += 1
	# 		file_group_dict_lists[subgroup_dict.get(relevant_dict.get(gene))].append(gene_tuple)
	# 	# print('\tgene_file_list, len: {}, genefile: {}'.format(len(mini_bar), len(file_genes)))
	# 	# print('\t\tgenes: {}'.format(file_group_dict))
	# 	non_none_count = 	sum([file_group_dict.get(key) for key in file_group_dict if key is not None])
	# 	all_count = 		sum([file_group_dict.get(key) for key in file_group_dict])
	# 	percent_dict = {key:round(file_group_dict.get(key)/all_count,1) for key in file_group_dict}
	# 	print('\t\tfile genes: {}, non-None: {}'.format(group_dict, non_none_count))
	# 			# if group_names[mini_bar_loop] is None:
	# 			# 	pass
	# 				# inc_list.append((gene, relevant_dict.get(gene), subgroup_dict.get(relevant_dict.get(gene)), group_names[mini_bar_loop]))
	# 			# except:


	# 			# print('name: {}\n\ttype: {}\n\ttype_set: {}\n\texpected_type: {}\n\ttype right: {}'.format(gene, relevant_dict.get(gene), subgroup_dict.get(relevant_dict.get(gene)), group_names[mini_bar_loop],
	# 			# 	group_names[mini_bar_loop] in subgroup_dict.get(relevant_dict.get(gene))))
	# 			# if gene_loop > 5:
	# 			# 	raise
	# 		# print('\t\tcorrect count: {}, incorrect count: {}\n\t\tgenes: {}'.format(correct, incorrect, inc_list))
	# 		# if incorrect > 0:
	# 		# 	print('\tmini-bar type: {}'.format(group_names[mini_bar_loop]))
	# 		# 	# print('\t\tcorrect count: {}, incorrect count: {}\n\t\tgenes: {}'.format(correct, incorrect, group_dict))
	# 		# 	print('\t\tcorrect count: {}, incorrect count: {}\n\t\tgenes: {}'.format(corr_list[:20], incorrect, 
	# 		# 		['{}: {}'.format(key, group_dict_lists.get(key)[:20]) for key in group_dict_lists]))
	# 		# 	raise
	# 	# raise


	# print(gene_list[0])
	# print(len(gene_list))
	# print([len(thing) for thing in gene_list])
	# print([[len(stuff) for stuff in thing] for thing in gene_list])


	# ####################################################################
	# ################### Checking Peak Gene Overlaps ####################
	# ####################################################################



	# my_list = [1,2,3,4,5,6,7,8,9]

	# print(my_list[:3])
	# print(my_list[6:])
	# a = [1,2,3] + [list([] for num in range(5))]
	# a.append(list([] for num in range(5)))
	# a[-1][-1].append(5)

	# hfnxns.quick_dumps(a)




	######################################################################################################
	# random stuff for above peak genes
	######################################################################################################
	# print_list = compare_print_list(merge_peak_genes(new_genes), merge_peak_genes(old_genes))

	# final_print_list = []
	# for line_loop, line in enumerate(print_list):
	# 	# try:
	# 		unique1 = [gene for gene in line[3] if gene not in line[4]]
	# 		unique2 = [gene for gene in line[4] if gene not in line[3]]

	# 		final_print_list.append(line[:3] + [unique1] + [unique2])
	# 		# if len(unique1) > 0 or len(unique2) > 0:
	# 		# 	print(line[:3], unique1, unique2)
	# 	# 	else:
	# 	# 		print(line)
	# 	# except:
	# 	# 	print(line)
	# 	# 	raise
	# 	# if line_loop > 10:
	# 	# 	break

	# raise

	# new_gene_files = '/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_04_16/E/core_pipeline_dirs/4kb_broad/gene_lists/test/LUNG.bed'
	# old_gene_files = '/Users/jschulz1/Documents/project_data/ccle_project/analyses/2019_11_18-full_4kb_vs_1kb_run/full_4kb_vs_1kb_run/1kb_sharp_25_50/gene_lists/LUNG_1kb_sharp_25_50_gene_names.txt'

	# new_genes = []
	# with open(new_gene_files, 'r') as readfile:
	# 	for line in readfile:
	# 		new_genes.append(line.strip('\n'))
	# old_genes = []
	# with open(old_gene_files, 'r') as readfile:
	# 	for line in readfile:
	# 		old_genes.append(line.strip('\n'))
	# new_set = sorted(set(new_genes))
	# old_set = sorted(set(old_genes))
	# print(len(list(new_set)), len(list(old_set)))
	# # print(new_genes)
	# # print([gene for gene in new_genes if gene not in old_genes])
	# # print([gene for gene in old_genes if gene not in new_genes])

	# raise

	# print(hfnxns.kde_median([0,0,0,1,1]))
	# raise

	# new_lens = '/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_04_16/E/comparison_pipeline_dirs/4kb_broad_v_1kb_sharp/peak_size_comparison_beds/4kb_broad_Cancer_vs_NonCancer_comparsion/test/LUNG.bed'
	# old_lens = '/Users/jschulz1/Documents/project_data/ccle_project/analyses/2019_11_18-full_4kb_vs_1kb_run/full_4kb_vs_1kb_run/4kb_25_50_v_1kb_sharp_25_50/compare_beds/cancer_ref/all_dir/LUNG_all_common.bed'
	# new_list = grab_sorted_peak_lens(new_lens, subtract=True)
	# old_list = grab_sorted_peak_lens(old_lens, subtract=False)
	# final_list = []
	# for line_loop, new_peak in enumerate(new_list):
	# 	old_peak = old_list[line_loop]
	# 	final_list.append([new_peak[0], old_peak[0], new_peak[1], old_peak[1], new_peak[2], old_peak[2], round(new_peak[3],3), round(old_peak[3],3), round(new_peak[3]-old_peak[3],3)])
	# print('\n'.join([','.join(['{:>15}'.format(stuff) for stuff in thing]) for thing in final_list]))
	# print('\n'.join([str(thing) for thing in list(zip(new_list, old_list))]))










	# ####################################################################
	# ############ Checking if stuff is on the same side of 0 ############
	# ####################################################################
	# a, b, c, d = 1,2,-1,-2

	# def check_same_side(inp1, inp2):
	# 	return sum([inp1>0, inp2>0]) != 1

	# print(check_same_side(a,b)) #true
	# print(check_same_side(b,a)) #true
	# print(check_same_side(c,b))
	# print(check_same_side(b,c))
	# print(check_same_side(a,c))
	# print(check_same_side(c,d)) #true
	# print(check_same_side(d,b))

	# ####################################################################
	# ########### For messing with text, worth checking later  ###########
	# ####################################################################
	# my_fig = hfnxns.easy_fig()
	# my_fig.add_panel(x_count=2)
	# my_fig.axes[0][0].plot([-2,-1,0,1,2], [-2,-1,0,1,2])
	# my_fig.axes[0][0].scatter([-2,-1,0,1,2], [-2,-1,0,1,2])
	# my_fig.axes[0][1].scatter([0,1], 
	# 	[0,1])

	# # this fnxn will take take an ax + ax.text args -> put the text in the
	# 	# ax + catch the text object + get the locations of the edges of the
	# 	# text box. Can pass a matplotlib transform object to decide
	# 	# what scale it is received in.
	# 	# if not transform object passed, defaults to ax data scale
	# # want to use this to format "*" text better for barplots
	# new_text, loc = my_fig.text_and_location(my_fig.axes[0][0], 0.5,0.5,'string', 
	# 	ha='center', va='center')
	# my_fig.axes[0][0].scatter([loc[0][0], loc[0][1], loc[0][0], loc[0][1]], 
	# 	[loc[1][0], loc[1][0], loc[1][1], loc[1][1]])
	# my_fig.axes[0][0].scatter([loc[0][0]], 
	# 	[loc[1][0]])
	# my_fig.axes[0][0].scatter([loc[0][1]], 
	# 	[loc[1][0]])
	# my_fig.axes[0][0].scatter([loc[0][0]], 
	# 	[loc[1][1]])
	# my_fig.axes[0][0].scatter([loc[0][1]], 
	# 	[loc[1][1]])


	# my_fig.show()
	# # plt.close()


	# second_fig = hfnxns.easy_fig()
	# second_fig.add_panel(x_count=2)
	# r = my_fig.fig.canvas.get_renderer()
	# new_text = my_fig.axes[0][0].text(0.5,0.5,'string', ha='center', va='center')
	# second_fig.axes[0][1].scatter([0,1], 
		# [0,1])
	# new_text, loc = second_fig.text_and_location(second_fig.axes[0][0], 0.5,0.5,'string', 
	# 	ha='center', va='center')
	# print(loc)
	# my_fig.axes[0][0].scatter([loc[0][0], loc[1][0], loc[0][0], loc[1][0]], 
	# 	[loc[0][1], loc[1][1], loc[1][1], loc[0][1]])
    # new_text.set_visible(False)







	# dealing with mutable input -> change -> output child process stuff
		# this causes somewhat unexplainable output differences b/w mp and non-parallel
		# because mp won't get the mutations back unlike non-parallel

		# pool = hfnxns.pool_wrapper(4, check_mutated=False)

		# input_data = [1,2,3,4]
		# results = pool.add_job(test_return, (input_data,))

		# output_data = results.result()

		# print('using mp')
		# print(output_data)
		# print(input_data)
		
		# input_data = [1,2,3,4]
		# output_data = test_return(input_data)

		# print('np mp')
		# print(output_data)
		# print(input_data)



		# a = [1,2,3,4]
		# b = a
		# c = [1,2,3,4]
		# d = [1,2,3,4]
		# e = [1,2,3,4]
		# a.append(5)

		# # a = 'asdf'
		# # b = a
		# # c = 'asdf'
		# # d = 'asdf'
		# # e = 'asdf'


		# print(a in [b,c,d])
		# print(e in [b,c,d])
		# print(a == b, a == c, a == d)
		# print(e == b, e == c, e == d)
		# print(a is b, a is c, a is d)
		# print(e is b, e is c, e is d)








	# gonna try the idea of passing a variable to a function and having the function update it (possibly with a callback)

	# my_var = ['placeholder']

	# print(my_var)

	# my_var = update_var_from_fnxn(my_var, callback, (4,))

	# print(my_var)

	# my_thing = pretend_run_thing_async(test_fnxn, (4,))

	# new_thing = my_thing.resolve()

	# print(my_thing)
	# print(new_thing)

	# pool = mp.Pool()

























	################### checking pickle error ###################
	# test_queue = mp.Queue()
	# thing = pickle.dumps(test_queue)






	# # time_per = []
	# maxtasksperchild=None
	# pool = pool_wrapper(maxtasksperchild=maxtasksperchild) #, 
	# 			# let_child_processes_fail=True) #, 
	# 			# error_message_first_line_color=None)
	
	# result_objects = []
	# other_result_objects = []
	# for e in range(5):
	# 	for i in range(5):
	# 		if i == 0:
	# 			result_objects.append(
	# 				pool.add_job(test_print, ('running {}_{}'.format(e,i),), 
	# 				job_group='test_print')
	# 			)
	# 		else:
	# 			pool.add_job(test_print, ('running {}_{}'.format(e,i),), 
	# 				job_group='test_print')
	# 	for i in range(5):
	# 		if i == 0:
	# 			other_result_objects.append(
	# 				pool.add_job(other_test_print, ('running {}_{}'.format(e,i),), 
	# 				job_group='other_test_print')
	# 			)
	# 		else:
	# 			pool.add_job(other_test_print, ('running {}_{}'.format(e,i),), 
	# 				job_group='other_test_print')

	# # pool.block_for_specific_job_group('outer_print')

	# # going to prioritize blue one (other print)
	# pool.prioritize_specific_job_group('other_test_print')

	# # this should print non-other test running
	# for result in result_objects:
	# 	# print('\t', result)
	# 	out1, out2 = result.result()
	# 	print('\t\t', out2)

	# pool.block_for_all_jobs(verbose=True)
	# time.sleep(2)












	# other code from early multiprocessing testing
		# for test in range(20):
		# 	start_time = time.time()
		# 	init()

		# workers = mp.Pool(mp.cpu_count())
		# for i in range(5):
		# 	# run_test('stuff')
		# 	workers.apply_async(print, (i,))

		# for q in range(3):
				# run_test('ran {}'.format(i),)
				# run_test('ran {}'.format(i),)
		# print(pool.job_queue)

		# pool.jobs_done()
		# while len(pool.get_submitted_job_ids()) > 0:
		# 	print('added:', pool.get_added_job_ids())
		# 	print('submitted:', pool.get_submitted_job_ids())
		# 	print('\tprio:', pool.priority_job_order)
		# 	print('\tall:', pool.job_added_order)
		# 	print('')
		# 	time.sleep(1)

		# for i in range(10):
		# 	print('not blocking')
		# 	time.sleep(1)

		# print()
		# pool.jobs_done()
		# pool.jobs_done(check_worked=True)
		# pool.close_wrapper()

		# 	time_for_loop = round(time.time() - start_time, 3)
		# 	print('Took {}s w/ {} tasksperchild'.format(time_for_loop, 
		# 		maxtasksperchild))
		# 	time_per.append(time_for_loop)
		# print('average: {}\
		# 	\n\tall: {}'.format(sum(time_per)/len(time_per), time_per))

		# workers.close()
		# workers.join()

		# manager = mp.Manager()
		# queue = manager.Queue()
		# for test_loop in range(1):
		# 	test_fnxn(workers, queue)
		# queue = Queue()
		# for i in range(10):
		# 	queue.put(i)
		# queue.put('END')
		# while True:
		# 	# try:
		# 	output = queue.get()
		# 	if output == 'END':
		# 		break
		# 	else:
		# 		print(output)
			# except:
			# 	print('nothing')
			# 	time.sleep(0.2)

	# using sets to test gene list outputs/chisquare pvals for old vs new cancer barplots
		# print(chisquare([.2,4,.4], [.5,.1,.4]))
		# old_dir = '/Users/jschulz1/Documents/project_data/ccle_project/analyses/2019_11_18-full_4kb_vs_1kb_run/full_4kb_vs_1kb_run/data_for_figures/cancer_barplots'
		# new_dir = '/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_03_31/E/data_for_figures/cancer_plots/cancer_barplots'

		# for this_plot in ['len_cosmic', 'len_tuson']:
		# 	with open(os.path.join(old_dir, this_plot), 'r') as textfile:
		# 		old_dict = json.load(textfile)
		# 	with open(os.path.join(new_dir, this_plot), 'r') as textfile:
		# 		new_dict = json.load(textfile)
		# type_order = list(new_dict.get('types'))
		# print([old_dict.get('types').index(this_type) for this_type in list(new_dict.get('types'))])
		# new_sets = [[set([gene.lower() for gene in stuff]) for stuff in thing] for thing in new_dict.get('peak_list_to_plot')]
		# old_sets = [[set([gene.lower() for gene in stuff]) for stuff in thing] for thing in old_dict.get('peak_list_to_plot')]
		# for bar_loop in range(len(old_sets)):
		# 	for mini_loop in range(len(old_sets[bar_loop])-1):
		# 		old_sets[bar_loop][-1] = old_sets[bar_loop][-1].difference(old_sets[bar_loop][mini_loop])
		# for bar_loop in range(len(new_sets)):
		# 	print('bar: {}'.format(bar_loop))
		# 	for mini_loop in range(len(new_sets[bar_loop])):
		# 		print('\t', len(new_sets[bar_loop][mini_loop]), len(old_sets[(2*old_dict.get('types').index(type_order[int(bar_loop/2)]))+(bar_loop%2)][mini_loop]))
		# 		print('\t', len(new_sets[bar_loop][mini_loop].difference(old_sets[(2*old_dict.get('types').index(type_order[int(bar_loop/2)]))+(bar_loop%2)][mini_loop])),
		# 			len(old_sets[(2*old_dict.get('types').index(type_order[int(bar_loop/2)]))+(bar_loop%2)][mini_loop].difference(new_sets[bar_loop][mini_loop])))
		# 		if len(new_sets[bar_loop][mini_loop].difference(old_sets[(2*old_dict.get('types').index(type_order[int(bar_loop/2)]))+(bar_loop%2)][mini_loop])) > 0:
		# 			print('\tnew.dif(old)')
		# 			print('\t\t', new_sets[bar_loop][mini_loop].difference(old_sets[(2*old_dict.get('types').index(type_order[int(bar_loop/2)]))+(bar_loop%2)][mini_loop]), '\n')
		# 		if len(old_sets[(2*old_dict.get('types').index(type_order[int(bar_loop/2)]))+(bar_loop%2)][mini_loop].difference(new_sets[bar_loop][mini_loop])) > 0:
		# 			print('\told.dif(tnew)')
		# 			print('\t\t', old_sets[(2*old_dict.get('types').index(type_order[int(bar_loop/2)]))+(bar_loop%2)][mini_loop].difference(new_sets[bar_loop][mini_loop]), '\n')
		# 		print('')

		# 		# print('\t', len(new_sets[bar_loop][mini_loop]), len(old_sets[(2*old_dict.get('types').index(type_order[int(bar_loop/2)]))+(bar_loop%2)][mini_loop]))
		# 		# print('\t', bar_loop, (2*old_dict.get('types').index(type_order[int(bar_loop/2)]))+(bar_loop%2))


		# set1 = {1,2,3}
		# set2 = {3,4,5}

		# print(set1.intersection(set2))
		# print(set1.union(set2))
		# print(set1)
		# print(set2)



		# print('')
		# print(list(old_dict.get('types')))
		# print(list(new_dict.get('types')))
		# print('')
		# print([[len(list(set([gene.lower() for gene in stuff]))) for stuff in thing] for thing in old_dict.get('peak_list_to_plot')])
		# print([sum([len(list(set([gene.lower() for gene in stuff]))) for stuff in thing]) for thing in old_dict.get('peak_list_to_plot')])
		# print('')
		# print([[len(list(set([gene.lower() for gene in stuff]))) for stuff in thing] for thing in new_dict.get('peak_list_to_plot')])
		# print([sum([len(list(set([gene.lower() for gene in stuff]))) for stuff in thing]) for thing in new_dict.get('peak_list_to_plot')])
		# print('')
		# print('')
		# print('')
		# print([[len([gene.lower() for gene in stuff]) for stuff in thing] for thing in old_dict.get('peak_list_to_plot')])
		# print([sum([len([gene.lower() for gene in stuff]) for stuff in thing]) for thing in old_dict.get('peak_list_to_plot')])
		# print('')
		# print([[len([gene.lower() for gene in stuff]) for stuff in thing] for thing in new_dict.get('peak_list_to_plot')])
		# print([sum([len([gene.lower() for gene in stuff]) for stuff in thing]) for thing in new_dict.get('peak_list_to_plot')])













