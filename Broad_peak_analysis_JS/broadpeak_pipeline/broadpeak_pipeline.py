
# import sys
# sys.path.append('/Users/jschulz1/Documents/project_scripts')

import os
import glob
import json
import multiprocessing as mp
import re
import shutil
from termcolor import colored
import time

import broadpeak_helper_functions as hfnxns

# this sets up the save_dirs dictionary. i want this version to make ALL 
	# save files for every pipeline,
	# so that it's clear immediately what directories are being made
def make_save_dirs(settings):
	def make_dirs_each_core_analysis(analysis_key, analysis_dict):
		analysis_path = analysis_dict.get('path')
		analysis_dict['split_dir'] = {'path': \
			os.path.join(analysis_path, 'split_beds')}
		analysis_dict['common_save'] = {'path': \
			os.path.join(analysis_path, 'common_beds')}
		analysis_dict['tss_gene_dir'] = {'path': \
			os.path.join(analysis_path, 'tss_intersect_gene_names')}
		analysis_dict['gene_dir'] = {'path': \
			os.path.join(analysis_path, 'gene_lists')}
		analysis_dict['plot_dir'] = {'path': \
			os.path.join(settings.get('save_dirs').get('main_plot_dir').get('path'), \
				analysis_key)}

	def make_core_pipeline_dirs(save_dirs, analyses):
		core_pipeline_path = os.path.join(\
			save_dirs.get('path'), 'core_pipeline_dirs')
		save_dirs['core_pipeline_dirs'] = {'path': core_pipeline_path}
		save_dirs['core_pipeline_dirs']['top_plot_dir'] = {'path': \
			os.path.join(settings.get('save_dirs').get('main_plot_dir').get('path'), \
				'general_plots')}
		save_dirs['core_pipeline_dirs']['analyses'] = {'path': \
			os.path.join(core_pipeline_path, 'analyses')}

		for analysis_key in analyses:
			save_dirs['core_pipeline_dirs']['analyses'][analysis_key] = \
				{'path': os.path.join(core_pipeline_path, analysis_key)}
			make_dirs_each_core_analysis(analysis_key, save_dirs.get('core_pipeline_dirs')\
				.get('analyses').get(analysis_key))

	def make_comparison_pipeline_dirs(save_dirs, comparison_list, \
		control_group_name, test_group_name, analyses):
		def make_dirs_each_comparison_analysis(comparison_dict, \
			control_group_name, test_group_name, analyses):
			def add_unique_common_dirs(comparison_dict, \
				control_group_name, test_group_name, analyses):
				def add_specific_unique_beds(val1, val2, which_group, \
					unique_common, **kwargs):
					path_string = \
						'{{{val1}}}_{{{which_group}}}_subtract_{{{val2}}}_{{{which_group}}}'\
						.format(val1=val1, val2=val2, which_group=which_group)
					top_path = os.path.join(unique_common, \
						path_string.format(control=kwargs['control_name'], \
						test=kwargs['test_name'], \
						g1=kwargs['g1'], g2=kwargs['g2']))
					return_dict = {
						'path': top_path,
						'unique_beds': {'path': os.path.join(top_path, 'unique_beds')},
						'tss_gene_dir': {'path': os.path.join(top_path, 'tss_intersect_gene_names')},
						'gene_dir': {'path': os.path.join(top_path, 'gene_lists')},
					}

					return return_dict

				unique_common = os.path.join(\
					comparison_dict.get('path'), \
					'unique_common_beds_for_comparison')
				comparison_dict['unique_common'] = {
					'path': unique_common,
				}

				control_common_dir = os.path.join(unique_common, \
					'normal_common_beds')
				comparison_dict['unique_common']\
					['normal_common_beds'] = {
					'path': control_common_dir,
					'group1_normal_common': {'path': \
						os.path.join(unique_common, \
						'{group1}_normal_common'.format(\
						group1=analyses[0], group2=analyses[1]))},
					'group2_normal_common': {'path': \
						os.path.join(unique_common, \
						'{group2}_normal_common'.format(\
						group1=analyses[0], group2=analyses[1]))},
				}

				unique_peak_dir = os.path.join(unique_common, \
					'unique_peak_beds')
				comparison_dict['unique_common']['unique_peak_beds'] = {
					'path': unique_peak_dir,
					}
				name_dict = {'control_name': control_group_name, \
					'test_name': test_group_name, \
					'g1': analyses[0], 'g2': analyses[1]}
				unique_beds = comparison_dict.get('unique_common')\
					.get('unique_peak_beds')
				unique_beds['g1_control_subtract_g2_control'] = \
					add_specific_unique_beds('g1', 'g2', 'control', \
						unique_common, **name_dict)
					
				unique_beds['g1_test_subtract_g2_test'] = \
					add_specific_unique_beds('g1', 'g2', 'test', \
						unique_common, **name_dict)
					
				unique_beds['g2_control_subtract_g1_control'] = \
					add_specific_unique_beds('g2', 'g1', 'control', \
						unique_common, **name_dict)
					
				unique_beds['g2_test_subtract_g1_test'] = \
					add_specific_unique_beds('g2', 'g1', 'test', \
						unique_common, **name_dict)

			def add_peak_size_dirs(comparison_dict, control_group_name, \
				test_group_name, analyses):
				peak_sizes = os.path.join(comparison_dict.get('path'), \
					'unique_peak_size_beds_for_comparison')
				comparison_dict['peak_sizes'] = {
					'path': peak_sizes,
					'unique_g1_control_vs_test': \
						{'path': os.path.join(peak_sizes, \
							'unique_{g1}_{control_name}_vs_{test_name}'\
							.format(control_name=control_group_name, \
							test_name=test_group_name, \
							g1=analyses[0], g2=analyses[1]))},

					'unique_g2_control_vs_test': \
						{'path': os.path.join(peak_sizes, \
							'unique_{g2}_{control_name}_vs_{test_name}'\
							.format(control_name=control_group_name, \
							test_name=test_group_name, \
							g1=analyses[0], g2=analyses[1]))},

					'unique_g1_test_vs_control': \
						{'path': os.path.join(peak_sizes, \
							'unique_{g1}_{test_name}_vs_{control_name}'\
							.format(control_name=control_group_name, \
							test_name=test_group_name, \
							g1=analyses[0], g2=analyses[1]))},

					'unique_g2_test_vs_control': \
						{'path': os.path.join(peak_sizes, \
							'unique_{g2}_{test_name}_vs_{control_name}'\
							.format(control_name=control_group_name, \
							test_name=test_group_name, \
							g1=analyses[0], g2=analyses[1]))},

				}

				peak_size_dict = comparison_dict.get('peak_sizes')
				for dir_key in [this_key for this_key in peak_size_dict \
					if this_key is not 'path']:
					dir_path = peak_size_dict.get(dir_key).get('path')
					reference_name = '{}_peak_lengths'.format(\
						'_'.join(dir_key.split('_')[:2]))
					comparison_name = 'comparison_{}_peak_lengths'\
						.format(dir_key.split('_')[-1])
					peak_size_dict[dir_key]['reference_name'] = \
						{'path': os.path.join(peak_size_dict\
							.get(dir_key).get('path'), reference_name)}
					peak_size_dict[dir_key]['comparison_name'] = \
						{'path': os.path.join(peak_size_dict\
							.get(dir_key).get('path'), comparison_name)}

			def add_comparison_dirs(comparison_dict, control_group_name, \
				test_group_name, analyses):
				comparison = os.path.join(comparison_dict.get('path'), \
					'peak_size_comparison_beds')
				comparison_dict['comparison'] = {
					'path': comparison,
					'g1_control_vs_test_comparsion': \
						{'path': os.path.join(comparison, \
							'{g1}_{control_name}_vs_{test_name}_comparsion'\
							.format(control_name=control_group_name, \
							test_name=test_group_name, \
							g1=analyses[0], g2=analyses[1]))},

					'g2_control_vs_test_comparsion': \
						{'path': os.path.join(comparison, \
							'{g2}_{control_name}_vs_{test_name}_comparsion'\
							.format(control_name=control_group_name, \
							test_name=test_group_name, \
							g1=analyses[0], g2=analyses[1]))},

					'g1_test_vs_control_comparsion': \
						{'path': os.path.join(comparison, \
							'{g1}_{test_name}_vs_{control_name}_comparsion'\
							.format(control_name=control_group_name, \
							test_name=test_group_name, \
							g1=analyses[0], g2=analyses[1]))},

					'g2_test_vs_control_comparsion': \
						{'path': os.path.join(comparison, \
							'{g2}_{test_name}_vs_{control_name}_comparsion'\
							.format(control_name=control_group_name, \
							test_name=test_group_name, \
							g1=analyses[0], g2=analyses[1]))},

				}

			add_unique_common_dirs(comparison_dict, control_group_name, \
				test_group_name, analyses)

			add_peak_size_dirs(comparison_dict, control_group_name, \
				test_group_name, analyses)
			
			add_comparison_dirs(comparison_dict, control_group_name, \
				test_group_name, analyses)

		comparison_pipeline_path = os.path.join(save_dirs.get('path'), \
			'comparison_pipeline_dirs')
		save_dirs['comparison_pipeline_dirs'] = {'path': \
			comparison_pipeline_path}

		for comparison_loop, comparison in enumerate(comparison_list):
			comparison_name = '_v_'.join(comparison)
			comparison_key = 'comp_analysis_{}'.format(comparison_loop)

			save_dirs['comparison_pipeline_dirs'][comparison_key] = \
				{'path': os.path.join(comparison_pipeline_path, \
				comparison_name), \
				'plot_dir': {'path': os.path.join(\
					save_dirs.get('main_plot_dir').get('path'), \
					'comparison_pipeline', \
					comparison_name)
					}}

			make_dirs_each_comparison_analysis(\
				save_dirs.get('comparison_pipeline_dirs').get(\
				comparison_key), control_group_name, test_group_name, \
				analyses)

	def make_length_change_pipeline_dirs(save_dirs, \
		control_group_name, test_group_name, density_cutoff_dict):
		length_change_pipeline_path = os.path.join(\
			save_dirs.get('path'), 'length_change_pipeline_dirs')
		save_dirs['length_change_pipeline_dirs'] = {
			'path': \
				length_change_pipeline_path, 
			'common_beds': 	{'path': \
				os.path.join(length_change_pipeline_path, \
				'common_beds')}
			}

		group_names = ['control', 'test']
		for reference_loop, reference_group in \
			enumerate([control_group_name, test_group_name]):
			
			ref_str = 'referencing_{}'.format(reference_group)
			plot_ref_str = 'length_change/{}'.format(ref_str)
			reference_path = os.path.join(length_change_pipeline_path, \
				ref_str)

			length_path = os.path.join(reference_path, \
					'length_change_analysis')
			shortened_path = os.path.join(length_path, 'shortened_peaks_analysis')
			lengthened_path = os.path.join(length_path, 'lengthened_peaks_analysis')
			no_change_path = os.path.join(length_path, 'no_change_peaks_analysis')
			save_dirs['length_change_pipeline_dirs']\
				[group_names[reference_loop]] = {
				'path': reference_path, 
				'len_change_setup': {'path': \
					os.path.join(reference_path, 'len_change_setup')},
				'length_change_analysis': {
					'path': length_path,
					'plot_dir': {'path': \
						os.path.join(settings.get('save_dirs')
							.get('main_plot_dir').get('path'), \
							plot_ref_str, 'general_len_change_pipeline')},
					'lengthened_peaks_analysis': {
						'path': lengthened_path,
					},
					'shortened_peaks_analysis': {
						'path': shortened_path,
					},
					'no_change_peaks_analysis': {
						'path': no_change_path,
					}
				},
			}

			for specific_cutoff in density_cutoff_dict.get('lengthen').keys():
				save_dirs['length_change_pipeline_dirs']\
					[group_names[reference_loop]]['length_change_analysis']\
					['lengthened_peaks_analysis'][specific_cutoff] = {
						'path': os.path.join(lengthened_path, specific_cutoff)
					}
				save_dirs['length_change_pipeline_dirs']\
					[group_names[reference_loop]]['length_change_analysis']\
					['no_change_peaks_analysis'][specific_cutoff] = {
						'path': os.path.join(no_change_path, specific_cutoff)
					}
			for specific_cutoff in density_cutoff_dict.get('shorten').keys():
				save_dirs['length_change_pipeline_dirs']\
					[group_names[reference_loop]]['length_change_analysis']\
					['shortened_peaks_analysis'][specific_cutoff] = {
						'path': os.path.join(shortened_path, specific_cutoff)
					}

			comparison_setup_dict = save_dirs.get(\
				'length_change_pipeline_dirs')\
				.get(group_names[reference_loop])\
				.get('len_change_setup')
			comparison_setup_path = comparison_setup_dict.get('path')
			comparison_size_path = os.path.join(comparison_setup_path, \
					'peak_size_beds_for_comparison')

			comparison_setup_dict.update({
				'peak_size_beds_for_comparison': 	\
					{'path': comparison_size_path,
					'ref_group_sizes': {'path': 
							os.path.join(comparison_size_path, \
							'ref_group_sizes')}, \
					'compar_group_sizes': {'path': 
							os.path.join(comparison_size_path, \
							'compar_group_sizes')}, 
					},
				'size_comparison_beds': 	\
					{'path': os.path.join(comparison_setup_path, \
						'size_comparison_beds'), \
					},
				'other_0_common': 	\
					{'path': os.path.join(comparison_setup_path, \
					'other_0_common')},
			})

			length_change_analysis_dict = save_dirs.get(\
				'length_change_pipeline_dirs')\
				.get(group_names[reference_loop])\
				.get('length_change_analysis')
			
			for len_key in save_dirs.get('length_change_pipeline_dirs')\
				.get(group_names[reference_loop]).get('length_change_analysis'):
				if len_key is not 'path':
					for dens_cutoff_key in save_dirs.get('length_change_pipeline_dirs')\
						.get(group_names[reference_loop])\
						.get('length_change_analysis')\
						.get(len_key):
						if dens_cutoff_key is not 'path':
							make_dirs_each_core_analysis(
								'{}/{}/{}'.format(plot_ref_str, len_key, dens_cutoff_key),
								save_dirs.get('length_change_pipeline_dirs')\
								.get(group_names[reference_loop])
								.get('length_change_analysis')
								.get(len_key)
								.get(dens_cutoff_key))

	def make_all_broadpeak_paths(save_dirs):
		for this_key in save_dirs.keys():
			if this_key == 'path':
				os.makedirs(save_dirs.get(this_key), exist_ok=True)
			else:
				make_all_broadpeak_paths(save_dirs.get(this_key))

	test_group_name = settings.get('data_loading_info')\
		.get('test_group_name')
	control_group_name = settings.get('data_loading_info')\
		.get('control_group_name')

	top_save_path = settings.get('main_pipeline_dirs')\
		.get('loop_top_save_dir')
	settings['save_dirs'] = {
		'path': top_save_path,
		'loaded_data_dir': {'path': os.path.join(
			top_save_path, 'loaded_data')},
		'data_for_figures': {'path': os.path.join(
			top_save_path, 'data_for_figures')},
		'merged_dir': {'path': os.path.join(
			top_save_path, 'merged_beds')},
		'main_plot_dir': {'path': os.path.join(
			top_save_path, 'plots')}
		}

	make_core_pipeline_dirs(settings.get('save_dirs'), 
		settings.get('analysis_settings').get('core_analysis')
		.get('analyses').get('analysis_list'))

	make_comparison_pipeline_dirs(settings.get('save_dirs'), 
		settings.get('analysis_settings').get('comparison_analyses'), 
		control_group_name, test_group_name, 
		settings.get('analysis_settings').get('core_analysis')
		.get('analyses').get('analysis_list'))

	make_length_change_pipeline_dirs(settings.get('save_dirs'), 
		control_group_name, test_group_name, 
		settings.get('analysis_settings').get('len_change_analysis')
			.get('density_cutoffs'))

	make_all_broadpeak_paths(settings.get('save_dirs'))

def set_and_check_default_settings(settings, required_settings, \
	control_data_dict, test_data_dict, colors_required, \
	built_in_sample_handling, 
	default_step_file_settings, verbose=False):
	# this sets up defaults and formats the settings dictionary

	def core_analysis_settings(req_settings_dict, settings, \
		logger=None, verbose=False):
		# about
			# this sets defaults and checks settings for the core section of the pipeline
			# the logic is:
				# it loops through core analyses (eg, 4kb_broad, 1kb_sharp) then 
					# loops through settings from the required settings dictionary
				# if a required setting is not in the analysis settings AND isn't 
					# in the default settings dictionary, it raises an error
			# it does a similar check for the general_settings dictionary
			# it doesn't check for the value of the setting, just that it exists. 
				# I would like to add that later.

		for settings_key in settings.get('analyses').keys():
			for setting in req_settings_dict.get(\
					'specific_settings_list'):
				if setting not in settings.get('analyses')\
					.get(settings_key):
					if setting not in settings.get(\
						'default_settings'):
						raise ValueError(\
							'Required setting {} not found in analysis {} or default settings'\
							.format(setting, settings_key))
					else:
						settings['analyses'][settings_key][setting] = \
							settings.get('default_settings')\
							.get(setting)
		for setting in req_settings_dict.get('general_settings_list'):
			if setting not in settings.get('general_settings'):
				if setting not in settings.get('default_settings'):
					raise ValueError(\
						'Required general setting {} not found general or default settings'\
						.format(setting, settings_key))
				else:
					settings['general_settings'] = settings.get(\
						'default_settings').get(setting)

	def data_info_settings(data_info, required_data_info_dict, \
		control_data_dict):
		# about
			# this sets up the data_info settings dictionary. 
				# These settings relate to the format of the input beds/xls 
				# (what column data is in)
				# this probably shouldn't change unless the density_index is different, 
					# or the beds/xls don't have information about peak height/density
			# first, this checks to see if there are any control samples in the 
				# control_data_dict
				# if there are, then this will setup both test and control settings, 
					# otherwise it will only setup test settings
			# if there is a "general" dictionary under the "data_info" dictionary, 
				# and either test or control settings are completely missing, then they
				# will be set to the general setting values
				# if any specific settings are missing, this will try to set them 
					# to the value from the general settings
			# this will check that all required settings are present

		no_control = True
		for mark_key in control_data_dict.keys():
			if control_data_dict.get(mark_key) is not None:
				for group_key in control_data_dict.get(mark_key):
					if control_data_dict.get(mark_key).get(group_key) \
						is not None:
						no_control = False
		if no_control is False:
			test_groups = ['test', 'control']
		else:
			test_groups = ['test']

		for this_key in test_groups:
			if data_info.get(this_key) is None:
				data_info[this_key] = data_info.get('general')
			else:
				for this_setting in required_data_info_dict.get(\
						'required_data_info'):
					if data_info.get(this_key).get(this_setting) is \
						None:
						if data_info.get('general').get(this_setting) \
							is not None:
							data_info[this_key][this_setting] = \
								data_info.get('general').get(this_setting)
						else:
							raise ValueError(\
								'Missing data info {} for {} and general settings list'\
								.format(this_setting, this_key))
				for this_setting in required_data_info_dict.get(\
					'optional_data_info'):
					if data_info.get(this_key).get(this_setting) \
						is None:
						data_info[this_key][this_setting] = \
							data_info.get('general').get(this_setting)
		
		return no_control

	def common_perc_settings(general_settings_dict, data_info_settings):
		# about
			# this checks the "general_settings" for the core pipeline
			# these settings should be constant between the analyses 
				# (eg, merge length, common percentage)
			# just checks to make sure required settings are included and 
				# formatted appropriately
			# unlike some other functions, required settings are hard coded here. 
				# That could be an issue with later updates...

		sample_percs = general_settings_dict.get('sample_common_perc')
		if sample_percs.get('all') is not None:
			if sample_percs.get('test') is None:
				sample_percs['test'] = sample_percs.get('all')
			if sample_percs.get('control') is None:
				sample_percs['control'] = sample_percs.get('all')
		else:
			if sample_percs.get('test') is None:
				raise ValueError(\
					'No sample common percentage for "all" or for "test"')
			if sample_percs.get('control') is None:
				raise ValueError(\
					'No sample common percentage for "all" or for "control"')
		sample_percs[data_info_settings.get('test_group_name')] = \
			sample_percs.get('test')
		sample_percs[data_info_settings.get('control_group_name')] = \
			sample_percs.get('control')
		
		if general_settings_dict.get('type_common_perc')\
			.get('percent') is None:
			raise ValueError('No type common percentage was supplied')

	def check_general_pipeline_settings(settings, no_control, \
		colors_required, built_in_sample_handling, \
		default_step_file_settings, test_data_dict, verbose=False):
		def what_colors_needed(step_settings, colors_required, \
			which_analysis):
			color_step_info = [[step, step_settings.get(step)] \
				for step in colors_required.keys() \
				if colors_required.get(step) \
				== 'colors' and which_analysis.get(step) is True]
			# logic here (from top priority to bottom):
				# if no_control is True: don't need control_all or control_groups
				# if any of the things have None, then need control_groups and test_groups
				# if any fuse_control is True: need control_all
				# if any fuse_test is True: need test_all
				# if any fuse_control is False: need control_groups
				# if any fuse_test is False: need test_groups

				# also should check if and fuse + drop are True because that'll 
					# save some confusion

			which_colors = {
						'test_all': None,
						'test_groups': None,
						'control_all': None,
						'control_groups': None,
					}
			none_steps = [step for step in color_step_info if \
				step[1] is None]
			not_none_steps = [step for step in color_step_info if \
				step[1] is not None]
			if any([step[1].get('fuse_control') is True and \
				step[1].get('drop_control') is True for step in \
				not_none_steps]):
				raise ValueError(\
					'There is a setting for fuse_control in steps that are also dropping control samples, {}, this may cause issues:\n\t{}'\
					.format('\n\t'.join([str(step) for step in \
					not_none_steps if step[1].get('fuse_control') \
					is True and step[1].get('drop_control') is True])))
			if any([step[1].get('fuse_test') is True and \
				step[1].get('drop_test') is True for step in \
				not_none_steps]):
				raise ValueError(\
					'There is a setting for fuse_test in steps that are also dropping test samples, this may cause issues:\n\t{}'\
					.format('\n\t'.join([str(step) for step in \
					not_none_steps if step[1].get('fuse_test') \
					is True and step[1].get('drop_test') is True])))
			if no_control is True:
				which_colors['control_all'] = False
				which_colors['control_groups'] = False
			if len(none_steps) > 0:
				if which_colors.get('control_groups') is None:
					which_colors['control_groups'] = True
				which_colors['test_groups'] = True
			else:
				if no_control is False:
					if any([step[1].get('fuse_control') is \
						True for step in not_none_steps]):
						which_colors['control_all'] = True
					if any([step[1].get('fuse_control') is \
						None for step in not_none_steps]):
						which_colors['control_all'] = True
					if any([step[1].get('fuse_control') is \
						False for step in not_none_steps]):
						which_colors['control_groups'] = True
				if any([step[1].get('fuse_test') is True for \
					step in not_none_steps]):
					which_colors['test_all'] = True
				if any([step[1].get('fuse_test') is None for \
					step in not_none_steps]):
					which_colors['test_all'] = True
				if any([step[1].get('fuse_test') is False for \
					step in not_none_steps]):
					which_colors['test_groups'] = True
			return which_colors

		def fuse_step_settings(step_settings, \
			built_in_sample_handling, default_step_file_settings):
			for step in built_in_sample_handling.keys():
				if step_settings.get(step) is None:
					step_settings[step] = built_in_sample_handling\
						.get(step)
				for specific_setting in default_step_file_settings\
					.keys():
					if built_in_sample_handling.get(step).get(\
						specific_setting) is not None:
						if step_settings.get(step).get(\
							specific_setting) is not None:
							if verbose is True:
								print(\
									'Setting {} for step {} changed to required value {}'\
									.format(specific_setting, step, \
									built_in_sample_handling\
									.get(step).get(specific_setting)))
						step_settings[step][specific_setting] = \
							built_in_sample_handling.get(step)\
							.get(specific_setting)
					elif step_settings.get(step).get(\
						specific_setting) is None:
						step_settings[step][specific_setting] = \
							default_step_file_settings\
							.get(specific_setting)
				if step_settings.get(step).get('fuse_groups') is True:
					step_settings[step]['fuse_test'] = True
					step_settings[step]['fuse_control'] = True

		def all_settings_color_dict(settings_colors, settings, \
			which_colors):
			settings_color_dict = {}
			if settings_colors.get('general') is not None:
				set_settings_color_dict(settings_color_dict, \
					'general', settings.get('data_color_info'))
			if settings_colors.get('test') is not None:
				print('made test')
				set_settings_color_dict(settings_color_dict, \
					'test', settings.get('data_color_info'))
			elif settings_colors.get('general') is not None:
				print('replaced test')
				settings_color_dict['test'] = \
					settings_color_dict.get('general')
			elif which_colors.get('test_all') is True or \
				which_colors.get('test_groups') is True:
				raise ValueError(\
					'Need colors for test data, but no general colors or test colors supplied')
			if settings_colors.get('control') is not None:
				print('made control')
				set_settings_color_dict(settings_color_dict, \
					'control', settings.get('data_color_info'))
			elif settings_colors.get('general') is not None:
				print('replaced control')
				settings_color_dict['control'] = \
					settings_color_dict.get('general')
			elif which_colors.get('control_all') is True or \
				which_colors.get('control_groups') is True:
				raise ValueError(\
					'Need colors for control data, but no general colors or control colors supplied')

			return settings_color_dict

		def set_settings_color_dict(settings_color_dict, \
			which_setting, color_info):
			settings_color_dict[which_setting] = {}
			if color_info.get(which_setting).get('data_color_list') \
				is not None:
				for color_loop in range(len(color_info\
					.get(which_setting).get('data_color_list'))):
					settings_color_dict[which_setting][color_info\
						.get(which_setting)\
						.get('data_group_list')[color_loop]] = \
						color_info.get(which_setting)\
						.get('data_color_list')[color_loop]

		def set_color_dict(color_dict, which_group, sample_groups, \
			which_colors, settings_color_dict):
			if which_colors.get('{}_all'.format(which_group)) is \
				True or which_colors.get('{}_groups'\
				.format(which_group)) is True:
				color_dict[which_group] = {}
				if which_colors.get('{}_groups'.format(which_group)) \
					is True:
					for sample_type in sample_groups:
						if settings_color_dict.get(which_group)\
							.get(sample_type) is None:
							if settings_color_dict.get('general')\
								.get(sample_type) is None:
								raise ValueError(\
									'Need a color for {which_group} data for sample type {}, was not found in general or {which_group} color settings'\
									.format(sample_type, \
									which_group=which_group))
							else:
								color_dict[which_group][sample_type] = \
									settings_color_dict.get('general')\
									.get(sample_type)
						else:
							color_dict[which_group][sample_type] = \
								settings_color_dict.get(which_group)\
								.get(sample_type)
				if which_colors.get('{}_all'.format(which_group)) is \
					True:
					if settings_color_dict.get(which_group).get('all') \
						is None:
						if settings_color_dict.get('general')\
							.get('all') is None:
							raise ValueError(\
								'Need a color for {which_group} data for sample type "all", was not found in general or {which_group} color settings \
								\n\tThis setting is used for when {which_group} samples are fused into a single group'\
								.format(which_group=which_group))
						else:
							color_dict[which_group]['all'] = \
								settings_color_dict.get('general')\
								.get('all')
					else:
						color_dict[which_group]['all'] = \
							settings_color_dict.get(which_group)\
							.get('all')

		def convert_colors_to_hex(color_dict, r_color_name_file, \
			r_color_number_file):
			hex_colors, _ = hfnxns.get_hex_colors(r_color_name_file, \
				r_color_number_file)
			for test_group in color_dict.keys():
				for sample_group in color_dict.get(test_group).keys():
					if color_dict.get(test_group).get(\
						sample_group)[0] != '#':
						if hex_colors.get(color_dict.get(\
							test_group).get(sample_group)) is not None:
							color_dict[test_group][sample_group] = \
								hex_colors.get(color_dict\
								.get(test_group).get(sample_group))
						else:
							raise ValueError(\
								'Color for {} {}: {} is not hex and is not found in color file'\
									.format(test_group, sample_group, \
									color_dict.get(test_group).get(sample_group)))
			return color_dict
		
		def no_color_info_setup(sample_groups, which_colors, \
			default_color_list):
			def set_colors(color_dict, which_group, sample_groups, \
				which_colors, modified_default_color_list, \
				group_type_dict):
				not_enough_colors_error_string = \
					'There were not enough default color values to match all the groups. This can be changed with "default_color_list"\
					\n\tin the settings file. The default list has 10 values (C0-C9 from matplotlib)'
				if which_colors.get('{}_all'.format(which_group)) is \
					True or which_colors.get('{}_groups'.format(\
					which_group)) is True:
					color_dict[which_group] = {}
					if which_colors.get('{}_groups'\
						.format(which_group)) is True:
						for sample_type in sample_groups:
							if group_type_dict.get(sample_type) is \
								not None:
								color_dict[which_group][sample_type] = \
									roup_type_dict.get(sample_type)
							else:
								if len(modified_default_color_list) < 1:
									raise ValueError(\
										not_enough_colors_error_string)
								color = modified_default_color_list\
									.pop(0)
								color_dict[which_group][sample_type] = \
									color
								group_type_dict[sample_type] = color
					if which_colors.get('{}_all'.format(which_group)) \
						is True:
						if group_type_dict.get('all') is not None:
							color_dict[which_group]['all'] = \
								group_type_dict.get('all')
						else:
							if len(modified_default_color_list) < 1:
								raise ValueError(\
									not_enough_colors_error_string)
							color = modified_default_color_list.pop(0)
							color_dict[which_group]['all'] = color
							group_type_dict['all'] = color

			color_dict = {}
			group_type_dict = {}
			modified_default_color_list = default_color_list.copy()
			set_colors(color_dict, 'test', sample_groups, \
				which_colors, modified_default_color_list, \
				group_type_dict)
			set_colors(color_dict, 'control', sample_groups, \
				which_colors, modified_default_color_list, \
				group_type_dict)

			return color_dict
		
		if settings.get('compare_analyses') is not None and \
			no_control is True:
			if verbose is True:
				print(\
					'compare_analyses is not None but there are no control samples, this may cause an error later')
		if settings.get('compare_analyses') is not None:
			for analysis_group in settings.get('compare_analyses'):
				for comparison_group in analysis_group\
					.get('which_analyses'):
					if comparison_group not in [analysis_group\
						.get('analysis_name') for analysis_group in \
						settings.get('list_settings')]:
						if verbose is True:
							print(comparison_group)
							print([analysis_group.get('analysis_name') \
								for analysis_group in list_settings])
							print('Analysis name {} not in list_settings'\
								.format(comparison_group))
		if settings.get('which_analysis').get('len_change_analysis') \
			is True and no_control is True:
			if verbose is True:
				print('len_change_analysis is True but there are no control samples, this would cause an error in that step')
		
		if settings.get('data_loading_info').get('test_group_name') \
			is None:
			settings['data_loading_info']['test_group_name'] \
				= 'test'
			if verbose is True:
				print('No value found for "test_group_name", will be set to default: test')

		if settings.get('data_loading_info')\
			.get('control_group_name') is None:
			settings['data_loading_info']['control_group_name'] = \
				'control'
			if verbose is True:
				print('No value found for "control_group_name", will be set to default: control')
		settings['data_loading_info']['which_groupname'] = {}
		settings['data_loading_info']['which_groupname'][settings\
			.get('data_loading_info').get('test_group_name')] = 'test'
		settings['data_loading_info']['which_groupname'][settings\
			.get('data_loading_info').get('control_group_name')] = \
			'control'
		
		step_settings = settings.get('how_to_handle_file_groups')
		fuse_step_settings(step_settings, built_in_sample_handling, \
			default_step_file_settings)
		which_colors = what_colors_needed(step_settings, colors_required, \
			settings.get('which_analysis'))

		sample_groups = set()
		for mark_key in test_data_dict.keys():
			if test_data_dict.get(mark_key) is not None:
				for group_key in test_data_dict.get(mark_key):
					if test_data_dict.get(mark_key).get(group_key) \
						is not None:
						sample_groups.add(group_key)

		if settings.get('data_color_info').get('general') \
			is not None and settings.get('data_color_info')\
				.get('test') is not None \
			and settings.get('data_color_info').get('control') \
				is not None:
			settings_colors = settings.get('data_color_info')

			settings_color_dict = all_settings_color_dict(\
				settings_colors, settings, which_colors)
			color_dict = {}
			set_color_dict(color_dict, 'test', sample_groups, \
				which_colors, settings_color_dict)			
			set_color_dict(color_dict, 'control', sample_groups, \
				which_colors, settings_color_dict)			
			convert_colors_to_hex(color_dict, settings.get(\
				'data_color_info').get('r_color_name_file'), \
				settings.get('data_color_info')\
				.get('r_color_number_file'))
		else:
			if settings.get('data_color_info').get(\
				'default_color_list') is not None:
				default_color_list = settings.get(\
					'data_color_info').get('default_color_list')
			else:
				default_color_list = \
					['C0','C6','C7','C4','C1','C5','C3','C2','C8','C9']
			color_dict = no_color_info_setup(sample_groups, \
				which_colors, default_color_list)
			convert_colors_to_hex(color_dict, settings.get(\
				'data_color_info').get('r_color_name_file'), \
				settings.get('data_color_info').get(\
				'r_color_number_file'))
		color_dict[settings.get('data_loading_info').get(\
			'test_group_name')] = color_dict.get('test')
		color_dict[settings.get('data_loading_info').get(\
			'control_group_name')] = color_dict.get('control')
		settings['data_color_info']['color_dict'] = color_dict

	core_analysis_settings(
		req_settings_dict 	= required_settings.get(\
			'core_analysis'), 
		settings 			= settings.get('analysis_settings')\
			.get('core_analysis'), 
		verbose 			= verbose)

	settings['analysis_settings']['core_analysis']['analyses']\
		['analysis_list'] = \
		list(settings.get('analysis_settings').get('core_analysis')\
		.get('analyses').keys())

	settings['setup_variables'] = {}

	no_control = data_info_settings(settings.get('data_info'), \
		required_settings.get('data_info'), control_data_dict)

	check_general_pipeline_settings(settings, no_control, \
		colors_required, built_in_sample_handling, \
		default_step_file_settings, test_data_dict, verbose)

	common_perc_settings(settings.get('analysis_settings')\
		.get('core_analysis').get('general_settings'), \
		settings.get('data_loading_info'))


	return settings

def mark_specific_setup(settings):
	if settings.get('print_timing') is True: 
		start_mark_setup = time.time()

	make_save_dirs(settings)

	# i want to load the raw data files into the analysis directory in a structure 
		# that can be easily used during analysis
	load_raw_data(
		test_data_dict = \
			settings.get('data_loading_info').get('test_dict'), 
		control_data_dict = \
			settings.get('data_loading_info').get('control_dict'), 
		test_data_dir = \
			settings.get('data_loading_info').get('test_data_dir'), 
		control_data_dir = \
			settings.get('data_loading_info').get('control_data_dir'), 
		test_format = \
			settings.get('data_loading_info').get('test_load_format'), 
		control_format = \
			settings.get('data_loading_info').get('control_load_format'),
		current_mark = \
			settings.get('data_loading_info').get('which_mark'), 
		loaded_dir = \
			settings.get('save_dirs').get('loaded_data_dir').get('path'), 
		data_info = \
			settings.get('data_info'))

	# working on file handler

	# factory function that will produce a file handler function -> can call the 
		# function while supplying which step it's being called for + what directory 
		# to load from -> get a listof files to load (can also supply save dir 
		# to get a list of save paths to save to)
	# this is not fully setup yet, but i'd like to use it to simplify file loading 
		# logic in other stepswant file handler to spit out a list of files for 
		# each function to operate on
	file_handler = hfnxns.make_file_handler(
		file_settings_dict = settings.get('how_to_handle_file_groups'), 
		test_data_dict = settings.get('data_loading_info')\
			.get('test_dict'), 
		control_data_dict = settings.get('data_loading_info')\
			.get('control_dict'),
		test_group_name = settings.get('data_loading_info')\
			.get('test_group_name'),
		control_group_name = settings.get('data_loading_info')\
			.get('control_group_name'),
		print_dropped_types = settings.get('file_loader_verbosity')
	)

	# merge size scatter
	mapping_function = hfnxns.make_map_sample_names(\
		test_mapping_filepath=\
			settings.get('data_loading_info').get('test_name_mapping_file'), \
		control_mapping_filepath=\
			settings.get('data_loading_info').get('control_name_mapping_file'), \
		verbose=True)

	if settings.get('print_timing') is True: 
		print('\tmark_setup time: {} s'\
		.format(round(time.time()-start_mark_setup,2)))

	return file_handler, mapping_function


# this is the main pipeline that runs all of the sub-pipelines
def main_pipeline(settings, test_data_dict, control_data_dict):
	# these "print_timing" lines are included to let me easily print how long \
		# each step is taking.
		# if a certain step is taking longer, hopefully this lets me identify \
			# that faster
	if settings.get('print_timing') is True: 
		start_main_pipeline = time.time()
	if settings.get('print_timing') is True: 
		start_core_pipeline = time.time()

	if settings.get('print_timing') is True: 
		print('starting main_pipeline')
	for this_mark in test_data_dict:
		loop_test_data = test_data_dict.get(this_mark)
		loop_control_data = control_data_dict.get(this_mark)

		settings['data_loading_info']['test_dict'] = loop_test_data
		settings['data_loading_info']['control_dict'] = loop_control_data
		settings['data_loading_info']['which_mark'] = this_mark
		settings['main_pipeline_dirs']['loop_top_save_dir'] = \
			os.path.join(settings.get('main_pipeline_dirs')\
				.get('top_save_dir'), this_mark)

		file_handler, mapping_function = mark_specific_setup(settings)

		# hfnxns.quick_dumps(settings.get('save_dirs'))
		# raise

		core_pipeline(settings, file_handler, mapping_function)
		if settings.get('print_timing') is True: 
			print('\tCore pipeline time: {} s'\
				.format(round(time.time()-start_core_pipeline,2)))

		if settings.get('analysis_settings')\
			.get('comparison_analyses') is not None:
			if settings.get('print_timing') is True: 
				start_comparison_pipeline = time.time()
			comparison_pipeline(settings, file_handler, mapping_function)
			if settings.get('print_timing') is True: 
				print('\tComparison pipeline time: {} s'\
				.format(round(time.time()-start_comparison_pipeline,2)))

		if settings.get('which_analysis').get('len_change_analysis') \
			is True:
			if settings.get('print_timing') is True: 
				start_len_change_pipeline = time.time()
			len_change_pipeline(settings, file_handler, mapping_function)
			if settings.get('print_timing') is True: 
				print('\tLength Change pipeline time: {} s'\
				.format(round(time.time()-start_len_change_pipeline,2)))

	with open(os.path.join(settings.get('save_dirs')\
		.get('data_for_figures').get('path'),
		'figure_pipeline_settings.json'), 'w') as textfile:
		sample_type_list = set()
		sample_type_list.update(list(settings.get('data_loading_info').get('test_dict').keys()))
		sample_type_list.update(list(settings.get('data_loading_info').get('control_dict').keys()))
		sample_type_list.add('all')

		json.dump(
			{
				'test_group_name': settings.get('data_loading_info').get('test_group_name'),
				'control_group_name': settings.get('data_loading_info').get('control_group_name'),
				'sample_types': list(sample_type_list),
				'len_short_dict': settings.get('analysis_settings') \
									.get('len_change_analysis')
									.get('dens_cutoff_comparisons'),
			},
			textfile
		)

	if settings.get('workers') is not None:
		settings.get('workers').block_for_all_jobs(reset_prio=False)

	if settings.get('print_timing') is True: 
		print('Main pipeline time: {} s'\
			.format(round(time.time()-start_main_pipeline,2)))

# core pipeline, as described elsewhere
# core pipeline main part
def core_pipeline(settings, file_handler, mapping_function):
	if settings.get('print_timing') is True: 
		print('\tstarting core_pipeline')

	sample_groups = sorted(list(settings.get('data_loading_info')\
		.get('test_dict').keys()))

	step_name = 'merge_size_scatter'
	if settings.get('which_analysis').get(step_name) is True:

		if settings.get('print_timing') is True: 
			start_merge_size_comparison_scatter = time.time()
		merge_size_comparison_scatter(
			plot_dir =			settings.get('save_dirs')\
									.get('core_pipeline_dirs')\
									.get('top_plot_dir')\
									.get('path'), 
			fig_data_save = 	settings.get('save_dirs')\
									.get('data_for_figures')\
									.get('path'), 
			color_dict =		settings.get('data_color_info')\
									.get('color_dict'), 
			merge_sizes =		[0,1000,2000,3000,4000,5000,6000,\
									7000,8000,9000,10000], 
			save_names =        {'figdata_json': 'merge_scatter_json', \
									'all_plot': \
										'merge_size_scatter_line_all.png', \
									'subplots': \
										'merge_size_scatter_line_subplots.png'},
			file_dict =         file_handler(step_name, \
									settings.get('save_dirs')\
									.get('loaded_data_dir')\
									.get('path'),
								),
			verbose = 			settings.get('verbosity_dict')\
									.get('merge_size_comparison_scatter'), 
			cutoff =			4000
		)
		if settings.get('print_timing') is True: 
			print('\t\tmerge_size_comparison_scatter time: {} s'\
				.format(\
				round(time.time()-start_merge_size_comparison_scatter,2)))

	# merging beds
	if settings.get('analysis_settings').get('core_analysis')\
		.get('general_settings').get('merge_len') \
		is not None:
		merge_len = settings.get('analysis_settings').get('core_analysis')\
								.get('general_settings').get('merge_len')
	else:
		merge_len = 0
	if settings.get('print_timing') is True: 
		start_make_shape_merged_bed = time.time()
	hfnxns.make_shape_merged_bed(
		top_merged_dir 	= 	settings.get('save_dirs').get('merged_dir')\
								.get('path'), 
		file_dict 		=	file_handler('merge_beds', \
								settings.get('save_dirs')
									.get('loaded_data_dir')\
									.get('path'), settings.get('save_dirs')
									.get('merged_dir')\
									.get('path')
							),
		merge_len 		= 	merge_len, 
		data_info 		= 	settings.get('data_info')
	)

	if settings.get('print_timing') is True: 
		print('\t\t\tmake_shape_merged_bed time: {} s'\
		.format(round(time.time()-start_make_shape_merged_bed,2)))

	# making quantile plots
	quantile_percents = [1,5,25,50,75,95,99,99.9]
	step_name = 'plot_len_quantiles'
	if (settings.get('which_analysis').get(step_name) is True):
		if settings.get('print_timing') is True: 
			start_make_sample_quantile_plots = time.time()
		hfnxns.make_sample_quantile_plots(
			plot_dir 					=	settings.get('save_dirs')\
												.get('core_pipeline_dirs')\
												.get('top_plot_dir')\
												.get('path'), 
			fig_data_save 				=	settings.get('save_dirs')\
												.get('data_for_figures')\
												.get('path'), 
			file_dict 					=	file_handler(step_name, \
												settings.get('save_dirs')\
												.get('merged_dir')\
												.get('path'),
								),
			percents 					= 	quantile_percents,
			verbose 					= 	settings.get('verbosity_dict')\
												.get('quantile_plot'),
			len_or_dens 				=	'len',
			merge_size 					=	settings.get('analysis_settings')\
												.get('core_analysis')\
												.get('general_settings')\
												.get('merge_len'), 
			cutoff 						=	4000, 
			percent_keep 				=	95,
			mapping_function			=	mapping_function,
			test_group_name 			= 	settings.get('data_loading_info')\
												.get('test_group_name'),
			control_group_name 			= 	settings.get('data_loading_info')\
												.get('control_group_name')
			)

		if settings.get('print_timing') is True: 
			print('\t\t\tmake_sample_quantile_plots time: {} s'\
			.format(round(time.time()-start_make_sample_quantile_plots,2)))

	step_name = 'plot_dens_quantiles'
	if (settings.get('which_analysis').get(step_name) is True):
		if settings.get('print_timing') is True: 
			start_make_sample_quantile_plots = time.time()
		hfnxns.make_sample_quantile_plots(
			plot_dir 					=	settings.get('save_dirs')\
												.get('core_pipeline_dirs')\
												.get('top_plot_dir')\
												.get('path'), 
			fig_data_save 				=	settings.get('save_dirs')\
												.get('data_for_figures')\
												.get('path'), 
			file_dict 					=	file_handler(step_name, \
												settings.get('save_dirs')\
												.get('merged_dir')\
												.get('path'),
											),
			percents 					=	quantile_percents,
			verbose 					=	settings.get('verbosity_dict')\
												.get('quantile_plot'),
			len_or_dens 				=	'dens',
			merge_size 					=	settings.get('analysis_settings')\
												.get('core_analysis')\
												.get('general_settings')\
												.get('merge_len'), 
			cutoff 						=	4000, 
			percent_keep 				=	95,
			mapping_function			=	mapping_function,
			test_group_name 			=	settings.get('data_loading_info')\
												.get('test_group_name'),
			control_group_name 			=	settings.get('data_loading_info')\
												.get('control_group_name')
			)
		if settings.get('print_timing') is True: 
			print('\t\t\tmake_sample_quantile_plots time: {} s'\
			.format(round(time.time()-start_make_sample_quantile_plots,2)))

		for group in settings.get('analysis_settings')\
			.get('core_analysis').get('analyses').keys():
			if isinstance(settings.get('analysis_settings')\
			.get('core_analysis').get('analyses').get(group), dict):
				if settings.get('print_timing') is True: 
					start_specific_group_analysis = time.time()
				specific_group_analysis(settings, group, sample_groups, file_handler, \
					mapping_function)
				if settings.get('print_timing') is True: 
					print('\t\tspecific_group_analysis {} time: {} s'\
						.format(group, round(time.time()- \
						start_specific_group_analysis,2)))

# this is for analysis of specific versions of the core analysis 
	# (eg, 4kb_25_50, 1kb_sharp_25_50)
def specific_group_analysis(settings, group, sample_groups, file_handler, \
	mapping_function):
	if settings.get('print_timing') is True: 
		print('\t\tstarting {} specific_group_analysis'.format(group))
	print_list = ['Starting Analysis '+str(group)+':']

	for setting in settings.get('analysis_settings')\
		.get('core_analysis').get('analyses').get(group).keys():
		print_list.append('{}: {}'.format(setting, settings\
			.get('analysis_settings').get('core_analysis')\
			.get('analyses').get(group).get(setting)))
	if settings.get('verbosity_dict').get('specific_group_analysis') \
		is True:
		print('\n\t'.join(print_list))
	if settings.get('print_timing') is True: 
		start_pipeline_file_split = time.time()
	hfnxns.split_beds(
			file_dict 		=	file_handler('split_beds', \
									settings.get('save_dirs')\
										.get('merged_dir')\
										.get('path'), \
									settings.get('save_dirs')\
										.get('core_pipeline_dirs')\
										.get('analyses')\
										.get(group)\
										.get('split_dir')\
										.get('path')
								),
			len_or_dens		=	settings.get('analysis_settings')\
									.get('core_analysis')\
									.get('analyses')\
									.get(group)\
									.get('len_or_dens'), 
			val_or_perc 	=	settings.get('analysis_settings')\
									.get('core_analysis')\
									.get('analyses')\
									.get(group)\
									.get('split_type'), 
			cutoff 			=	settings.get('analysis_settings')\
									.get('core_analysis')\
									.get('analyses')\
									.get(group)\
									.get('cutoff'), 
			greater_less 	=	settings.get('analysis_settings')\
									.get('core_analysis')\
									.get('analyses')\
									.get(group)\
									.get('greater_or_less'),
			directories 	=	sample_groups
			)
	if settings.get('print_timing') is True: 
		print('\t\t\tpipeline_file_split time: {} s'\
		.format(round(time.time()-start_pipeline_file_split,2)))

	# making split plots
	step_name = 'split_plot'
	if (settings.get('which_analysis').get(step_name) == True):
		if settings.get('print_timing') is True: 
			start_make_split_plots = time.time()
		hfnxns.make_split_plots(
			file_dict 		=		file_handler(step_name, \
										settings.get('save_dirs')\
											.get('merged_dir')\
											.get('path'), \
										settings.get('save_dirs')\
											.get('core_pipeline_dirs')\
											.get('analyses')\
											.get(group)\
											.get('split_dir')\
											.get('path')
									),
			plot_dir		=		settings.get('save_dirs')\
										.get('core_pipeline_dirs')\
										.get('analyses')\
										.get(group)\
										.get('plot_dir')\
										.get('path'), 
			fig_data_save	=		settings.get('save_dirs')\
										.get('data_for_figures')\
										.get('path'), 
			analysis_name	=		group,
			mp_workers 		= 		settings.get('workers')
		)
		if settings.get('print_timing') is True: 
			print('\t\t\tmake_split_plots time: {} s'\
			.format(round(time.time()-start_make_split_plots,2)))


	# common percents plot
	# new common peak counts are slightly different from old counts. I trust the 
		# new version more but need to make sure that's right
	step_name = 'common_table_plot'
	if (settings.get('which_analysis').get(step_name) == True):
		if settings.get('print_timing') is True: 
			start_common_percents_plot = time.time()
		hfnxns.common_percents_plot(
			file_dict 		=	file_handler(step_name, \
									settings.get('save_dirs')\
										.get('core_pipeline_dirs')\
										.get('analyses')\
										.get(group)\
										.get('split_dir')\
										.get('path'),
								),
			plot_dir		=	settings.get('save_dirs')\
									.get('core_pipeline_dirs')\
									.get('analyses')\
									.get(group)\
									.get('plot_dir')\
									.get('path'), 
			cutoff_percents	=	list(range(0,125,25)),
			fuse_outputs	= {
								settings.get('data_loading_info')\
									.get('test_group_name'):\
									settings.get(\
										'how_to_handle_file_groups')\
										.get(step_name)\
										.get('fuse_test'),
								settings.get('data_loading_info')\
									.get('control_group_name'):\
									settings.get(\
										'how_to_handle_file_groups')\
										.get(step_name)\
										.get('fuse_control')
						}
		)
		if settings.get('print_timing') is True: 
			print('\t\t\tcommon_percents_plot time: {} s'\
			.format(round(time.time()-start_common_percents_plot,2)))


	# common percents subplot
	# same as above, but it's only different for the non-cancer samples. I 
		# need to look into that later
	step_name = 'common_table_subplot'
	if settings.get('print_timing') is True: 
		start_common_percent_subplot = time.time()
	hfnxns.common_percent_subplot(
		fig_data_save	=	settings.get('save_dirs')\
								.get('data_for_figures')\
								.get('path'), 
		analysis_name	=	group, 
		file_dict		=	file_handler(step_name, \
								settings.get('save_dirs')\
								.get('core_pipeline_dirs')\
								.get('analyses')\
								.get(group)\
								.get('split_dir')\
								.get('path')
							),
		plot_dir		=	settings.get('save_dirs')\
								.get('core_pipeline_dirs')\
								.get('analyses')\
								.get(group)\
								.get('plot_dir')\
								.get('path'), 
		cutoff_percents	=	list(range(0,125,25)),
		fuse_outputs	= {
							settings.get('data_loading_info')\
								.get('test_group_name'):
									settings.get('how_to_handle_file_groups')\
										.get(step_name)\
										.get('fuse_test'),
							settings.get('data_loading_info')\
								.get('control_group_name'):
									settings.get('how_to_handle_file_groups')\
										.get(step_name)\
										.get('fuse_control')
						}
	)

	if settings.get('print_timing') is True: 
		print('\t\t\tcommon_percent_subplot time: {} s'\
			.format(round(time.time()-start_common_percent_subplot,2)))

	# making sample common beds
	step_name = 'sample_common_perc'
	if settings.get('analysis_settings').get('core_analysis')\
		.get('general_settings').get(step_name) is not None:
		if settings.get('print_timing') is True: 
			start_make_common_beds = time.time()
		hfnxns.make_common_beds(
			file_percent 				=	settings.get('analysis_settings')\
												.get('core_analysis')\
												.get('general_settings')\
												.get(step_name), 
			file_dict					=	file_handler(
												'sample_common_perc', 
												settings.get('save_dirs')\
													.get('core_pipeline_dirs')\
													.get('analyses')\
													.get(group)\
													.get('split_dir')\
													.get('path'),
												settings.get('save_dirs')\
													.get('core_pipeline_dirs')\
													.get('analyses')\
													.get(group)\
													.get('common_save')\
													.get('path'),
											),
			mp_workers 					=	settings.get('workers'),
			merge_col_dict				=	{3:'avg', 4:'avg'}
			)

		if settings.get('workers') is not None:
			if settings.get('workers').job_group_exists('common_beds'):
				settings.get('workers').block_for_specific_job_group('common_beds', 
					reset_prio=False)

	# making type common beds
	if settings.get('print_timing') is True: 
		print('\t\t\tmake_common_beds time: {} s'\
		.format(round(time.time()-start_make_common_beds,2)))

	if settings.get('analysis_settings').get('core_analysis')\
		.get('general_settings').get('type_common_perc') is not None:
		if settings.get('print_timing') is True: 
			start_make_all_common_beds = time.time()
		hfnxns.make_all_common_beds(
			file_percent	=	settings.get('analysis_settings')\
									.get('core_analysis')\
									.get('general_settings')\
									.get('type_common_perc'), 
			file_dict		=	file_handler(
								'type_common_perc', 
								settings.get('save_dirs')\
									.get('core_pipeline_dirs')\
									.get('analyses')\
									.get(group)\
									.get('common_save')\
									.get('path'),
								),
			test_groups_dict={'test': settings.get('data_loading_info')\
									.get('test_group_name'), \
										'control': \
										settings.get('data_loading_info')\
									.get('control_group_name')}
		)

		if settings.get('print_timing') is True: 
			print('\t\t\tmake_all_common_beds time: {} s'\
			.format(round(time.time()-start_make_all_common_beds,2)))

	# generating gene lists
	if settings.get('print_timing') is True: 
		start_match_tss_gene_names = time.time()
	hfnxns.match_tss_gene_names(
		bed_10kb_tss	=	settings.get('main_pipeline_dirs')\
								.get('bed_10kb_tss'),
		input_dict		=	file_handler(
								'match_tss_gene_names_input', 
								settings.get('save_dirs')\
									.get('core_pipeline_dirs')\
									.get('analyses')\
									.get(group)\
									.get('common_save')\
									.get('path'),
						),
		output_dict		=	file_handler('match_tss_gene_names_output', \
								settings.get('save_dirs')\
									.get('core_pipeline_dirs')\
									.get('analyses')\
									.get(group)\
									.get('tss_gene_dir')\
									.get('path'),
								settings.get('save_dirs')\
									.get('core_pipeline_dirs')\
									.get('analyses')\
									.get(group)\
									.get('gene_dir')\
									.get('path'),
						)
	)

	if settings.get('print_timing') is True: 
		print('\t\t\tmatch_tss_gene_names time: {} s'\
		.format(round(time.time()-start_match_tss_gene_names,2)))

	# pathway analysis of in core pipeline
	step_name = 'core_path_analysis'
	if (settings.get('which_analysis').get(step_name) == True):
		if settings.get('print_timing') is True: 
			start_pathway_analysis = time.time()
		settings['setup_variables'] = pathway_analysis(
			file_dict			=	file_handler(
										step_name, 
										settings.get('save_dirs')\
											.get('core_pipeline_dirs')\
											.get('analyses')\
											.get(group)\
											.get('gene_dir')\
											.get('path'),
									),
			plot_dir			=	settings.get('save_dirs')\
										.get('core_pipeline_dirs')\
										.get('analyses')\
										.get(group)\
										.get('plot_dir')\
										.get('path'), 
			pathway_gene_files 	=	settings.get('pathway_gene_files'),
			setup_variables 	= 	settings.get('setup_variables'),
			mp_workers 			= 	settings.get('workers'),
		)

		if settings.get('workers') is not None:
			if settings.get('workers').job_group_exists('pathway_analysis'):
				settings.get('workers').block_for_specific_job_group('pathway_analysis', reset_prio=False)
		# raise

		if settings.get('print_timing') is True: 
			print('\t\t\tpathway_analysis time: {} s'\
			.format(round(time.time()-start_pathway_analysis,2)))

	if settings.get('workers') is not None:
		if settings.get('workers').job_group_exists('common_beds'):
			settings.get('workers').prioritize_specific_job_group('common_beds')

	step_name = 'peak_count_bar_plot'
	if (settings.get('which_analysis').get(step_name) == True):
		if settings.get('print_timing') is True: 
			start_bar_plot = time.time()
		hfnxns.bar_plot(
			file_dict			=	file_handler(
										step_name, 
										settings.get('save_dirs')\
											.get('core_pipeline_dirs')\
											.get('analyses')\
											.get(group)\
											.get('split_dir')\
											.get('path'),
									),
			plot_dir			=	settings.get('save_dirs')\
										.get('core_pipeline_dirs')\
										.get('analyses')\
										.get(group)\
										.get('plot_dir')\
										.get('path'), 
			title_pattern		=	settings.get('analysis_settings')\
										.get('core_analysis')\
										.get('analyses')\
										.get(group)\
										.get('title_pattern'), 
			useful_file_dir		=	settings.get('top_dirs')\
										.get('useful_file_dir'),
			mapping_function			=	mapping_function,
			test_group_name 			= 	settings.get('data_loading_info')\
												.get('test_group_name'),
			control_group_name 			= 	settings.get('data_loading_info')\
												.get('control_group_name')
		)
		if settings.get('print_timing') is True: 
			print('\t\t\tbar_plot time: {} s'\
			.format(round(time.time()-start_bar_plot,2)))

		step_name = 'bar_plot_common'
		if settings.get('print_timing') is True: 
			start_bar_plot_common = time.time()
		hfnxns.bar_plot_common(
			fig_data_save	=	settings.get('save_dirs')
								.get('data_for_figures')
								.get('path'), 
			file_dict		=	file_handler(
									step_name, 
									settings.get('save_dirs')
										.get('core_pipeline_dirs')
										.get('analyses')
										.get(group)
										.get('common_save')
										.get('path'),
								),
			plot_dir		=	settings.get('save_dirs')
									.get('core_pipeline_dirs')
									.get('analyses')
									.get(group)
									.get('plot_dir')
									.get('path'), 
			title_pattern	=	settings.get('analysis_settings')
									.get('core_analysis')
									.get('analyses')
									.get(group)
									.get('title_pattern'), 
			analysis_name	=	group, 
			common_counts	=	hfnxns.generate_required_peaks_count(
					file_percent 	=	settings.get('analysis_settings')
											.get('core_analysis')
											.get('general_settings')
											.get('sample_common_perc')
											.get('all'), 
					file_dict		=	file_handler(
											'sample_common_perc', 
											settings.get('save_dirs')
												.get('core_pipeline_dirs')
												.get('analyses')
												.get(group)
												.get('split_dir')
												.get('path'),
											settings.get('save_dirs')
												.get('core_pipeline_dirs')
												.get('analyses')
												.get(group)
												.get('common_save')
												.get('path'),
										)
					),
			log10			=	False
		)

		if settings.get('print_timing') is True: 
			print('\t\t\tbar_plot_common time: {} s'\
			.format(round(time.time()-start_bar_plot_common,2)))

		if settings.get('print_timing') is True: 
			start_bar_plot_common = time.time()
		hfnxns.bar_plot_common(
			fig_data_save	=	settings.get('save_dirs')\
								.get('data_for_figures')\
								.get('path'), 
			file_dict			=	file_handler(
										step_name, 
										settings.get('save_dirs')\
											.get('core_pipeline_dirs')\
											.get('analyses')\
											.get(group)\
											.get('common_save')\
											.get('path'),
									),
			plot_dir			=	settings.get('save_dirs')\
										.get('core_pipeline_dirs')\
										.get('analyses')\
										.get(group)\
										.get('plot_dir')\
										.get('path'), 
			title_pattern	=	settings.get('analysis_settings')\
									.get('core_analysis')\
									.get('analyses')\
									.get(group)\
									.get('title_pattern'), 
			analysis_name	=	group, 
			common_counts	=	hfnxns.generate_required_peaks_count(
					file_percent 	=	settings.get('analysis_settings')
											.get('core_analysis')
											.get('general_settings')
											.get('sample_common_perc')
											.get('all'), 
					file_dict		=	file_handler(
											'sample_common_perc', 
											settings.get('save_dirs')\
												.get('core_pipeline_dirs')\
												.get('analyses')\
												.get(group)\
												.get('split_dir')\
												.get('path'),
											settings.get('save_dirs')\
												.get('core_pipeline_dirs')\
												.get('analyses')\
												.get(group)\
												.get('common_save')\
												.get('path'),
										)
					),
			log10=True
		)
		if settings.get('print_timing') is True: 
			print('\t\t\tbar_plot_common time: {} s'\
			.format(round(time.time()-start_bar_plot_common,2)))

		step_name = 'bar_plot_intersect_percent'
		if settings.get('print_timing') is True: 
			start_bar_plot_intersect_percent = time.time()
		hfnxns.bar_plot_intersect_percent(
			file_dict			=	file_handler(
										step_name, 
										settings.get('save_dirs')\
											.get('core_pipeline_dirs')\
											.get('analyses')\
											.get(group)\
											.get('common_save')\
											.get('path'),
										settings.get('save_dirs')\
											.get('core_pipeline_dirs')\
											.get('analyses')\
											.get(group)\
											.get('tss_gene_dir')\
											.get('path'),
									),
			plot_dir			=	settings.get('save_dirs')\
										.get('core_pipeline_dirs')\
										.get('analyses')\
										.get(group)\
										.get('plot_dir')\
										.get('path'), 
			title_pattern	=	settings.get('analysis_settings')\
									.get('core_analysis')\
									.get('analyses')\
									.get(group)\
									.get('title_pattern'), 
		)
		if settings.get('print_timing') is True: 
			print('\t\t\tbar_plot_intersect_percent time: {} s'\
			.format(\
				round(time.time()-start_bar_plot_intersect_percent,2)))

# comparison pipeline - this is not set up yet
	# it only has save paths and the save dict setup
	# i have random "return" commands sitting in this code sometimes 
		# because it lets me finish a function 
	# to keep testing later parts of the pipeline without raising an 
		# error or anything
def comparison_pipeline(settings, file_handler, mapping_function):

	if settings.get('print_timing') is True: 
		start_comparison_pipeline = time.time()


	for compare_loop, comparison in \
		enumerate(settings.get('analysis_settings').get('comparison_analyses')):
		specific_comparison_analysis(settings, comparison, \
			'comp_analysis_{}'.format(compare_loop), file_handler, mapping_function)
		if settings.get('print_timing') is True: 
			print('\tComparison pipeline {} time: {} s'\
				.format(comparison, \
					round(time.time()-start_comparison_pipeline,2)))

	if settings.get('print_timing') is True: 
		print('\tcomparison_pipeline')

def specific_comparison_analysis(settings, comparison, compare_key, \
	file_handler, mapping_function):
	for analysis_loop, specific_analysis in enumerate(comparison):
		# comparison = [4kb_broad, 1kb_sharp]
		other_analysis = [analysis for analysis in comparison if \
			analysis != specific_analysis][0]
		analysis_key = 'group{}_normal_common'.format(analysis_loop+1)

		# making sample common beds
		step_name = 'comparison_common_beds'
		if settings.get('print_timing') is True: 
			start_make_common_beds = time.time()
		# making control common beds for comparison w/ test common beds for each
			# analysis
		hfnxns.make_common_beds(
			file_percent 				=	settings.get('analysis_settings')\
												.get('core_analysis')\
												.get('general_settings')\
												.get(step_name), 
			split_type 					=	settings.get('analysis_settings')\
												.get('core_analysis')\
												.get('analyses')\
												.get(specific_analysis)\
												.get('split_type'), 
			file_dict					=	file_handler(
												'comparison_common_beds', 
												settings.get('save_dirs')\
													.get('core_pipeline_dirs')\
													.get('analyses')\
													.get(specific_analysis)\
													.get('split_dir')\
													.get('path'),
												settings.get('save_dirs')\
													.get('comparison_pipeline_dirs')\
													.get(compare_key)\
													.get('unique_common')\
													.get('normal_common_beds')\
													.get(analysis_key)\
													.get('path'),
											), 
			mp_workers 					=	settings.get('workers'), 
			merge_col_dict				=	{3:'avg', 4:'avg'}
			)

		if settings.get('workers') is not None:
			if settings.get('workers').job_group_exists('common_beds'):
				settings.get('workers').block_for_specific_job_group('common_beds', reset_prio=False)

	for analysis_loop, specific_analysis in enumerate(comparison):
		other_analysis = [analysis for analysis in comparison if \
			analysis != specific_analysis][0]
		analysis_key = 'group{}_normal_common'.format(analysis_loop+1)

		# make tissue type-wise control common beds for each group
		for group in ['test', 'control']:
			if analysis_loop == 0:
				ind1 = 1
				ind2 = 2
			else:
				ind1 = 2
				ind2 = 1
			if group == 'test':
				other_group = 'control'
			else:
				other_group = 'test'
			unique_key = 'g{ind1}_{group}_subtract_g{ind2}_{group}'.format(
				ind1=ind1, ind2=ind2, group=group)

			if group == 'test':
				input1	= 	settings.get('save_dirs')\
								.get('core_pipeline_dirs')\
								.get('analyses')\
								.get(specific_analysis)\
								.get('common_save')\
								.get('path')
				input2	= 	settings.get('save_dirs')\
								.get('core_pipeline_dirs')\
								.get('analyses')\
								.get(other_analysis)\
								.get('common_save')\
								.get('path')
				kwargs = {'drop_control': True, 'drop_test': False}
			else:
				input1	= 	settings.get('save_dirs')\
								.get('comparison_pipeline_dirs')\
								.get(compare_key)\
								.get('unique_common')\
								.get('normal_common_beds')\
								.get('group{}_normal_common'.format(ind1))\
								.get('path')
				input2	= 	settings.get('save_dirs')\
								.get('comparison_pipeline_dirs')\
								.get(compare_key)\
								.get('unique_common')\
								.get('normal_common_beds')\
								.get('group{}_normal_common'.format(ind2))\
								.get('path')
				kwargs = {'drop_test': True, 'drop_control': False}

			# make unique common beds for each group for spec vs other analysis
			# run pipeline -> tss_gene + pathways + path dots
			comparison1_dict	=	file_handler(
										'comparison_unique_1', 
										input1,
										**kwargs
								)
			comparison2_dict	=	file_handler('comparison_unique_2', \
										input2,
										settings.get('save_dirs')\
											.get('comparison_pipeline_dirs')\
											.get(compare_key)\
											.get('unique_common')\
											.get('unique_peak_beds')\
											.get(unique_key)\
											.get('unique_beds')\
											.get('path'),
										**kwargs
								)

			# do subtractbed subtract peaks from beds in the other analysis group
				# and the same test group
				# eg, subtract Cancer 1kb sharp from Cancer 4kb broad
				# want to get analysis-specific peaks in a test group
			hfnxns.comparison_unique_common(
				comparison1_dict			=	comparison1_dict,
				comparison2_dict			=	comparison2_dict,
				)
			
			# this is followed by generating gene lists + performing pathway
				# analysis using the unique peak list
			if (settings.get('which_analysis').get('step_name') == True) or \
				(settings.get('which_analysis').get('comparison_genes') == True):
				if settings.get('print_timing') is True: 
					start_match_tss_gene_names = time.time()
				hfnxns.match_tss_gene_names(
					bed_10kb_tss	=	settings.get('main_pipeline_dirs')\
											.get('bed_10kb_tss'),
					input_dict		=	file_handler(
											'compar_match_tss_gene_names_input', \
											settings.get('save_dirs')\
												.get('comparison_pipeline_dirs')\
												.get(compare_key)\
												.get('unique_common')\
												.get('unique_peak_beds')\
												.get(unique_key)\
												.get('unique_beds')\
												.get('path'),
												**kwargs
									),
					output_dict		=	file_handler(\
											'compar_match_tss_gene_names_output', \
											settings.get('save_dirs')\
												.get('comparison_pipeline_dirs')\
												.get(compare_key)\
												.get('unique_common')\
												.get('unique_peak_beds')\
												.get(unique_key)\
												.get('tss_gene_dir')\
												.get('path'),
											settings.get('save_dirs')\
												.get('comparison_pipeline_dirs')\
												.get(compare_key)\
												.get('unique_common')\
												.get('unique_peak_beds')\
												.get(unique_key)\
												.get('gene_dir')\
												.get('path'),
												**kwargs
									)
				)

				if settings.get('print_timing') is True: 
					print('\t\t\tmatch_tss_gene_names time: {} s'\
					.format(round(time.time()-start_match_tss_gene_names,2)))

			step_name = 'compar_path_analysis'
			if (settings.get('which_analysis').get('comparison_paths') == True):
				if settings.get('print_timing') is True: 
					start_pathway_analysis = time.time()
				group_path_name = settings.get('save_dirs')\
													.get('comparison_pipeline_dirs')\
													.get(compare_key)\
													.get('unique_common')\
													.get('unique_peak_beds')\
													.get(unique_key)\
													.get('path').split('/')[-1]
				path_plot_dir = os.path.join(settings.get('save_dirs')\
												.get('comparison_pipeline_dirs')\
												.get(compare_key)\
												.get('plot_dir')\
												.get('path'),
										'pathway_analysis',
										group_path_name)
				os.makedirs(path_plot_dir, exist_ok=True)
				path_file_dict = file_handler(
												step_name, 
												settings.get('save_dirs')\
													.get('comparison_pipeline_dirs')\
													.get(compare_key)\
													.get('unique_common')\
													.get('unique_peak_beds')\
													.get(unique_key)\
													.get('gene_dir')\
													.get('path'),
													**kwargs
											)
				path_types = sorted(list(path_file_dict\
					.get(list(path_file_dict.keys())[0])\
					.keys()))
				settings['setup_variables'] = pathway_analysis(
					file_dict			=	path_file_dict,
					plot_dir			=	path_plot_dir, 
					pathway_gene_files 	=	settings.get('pathway_gene_files'),
					mp_workers 			=	settings.get('workers'),
					setup_variables 	= 	settings.get('setup_variables'),
				)

				if settings.get('workers') is not None:
					if settings.get('workers').job_group_exists('pathway_analysis'):
						settings.get('workers')\
							.prioritize_specific_job_group('pathway_analysis')

				if settings.get('print_timing') is True: 
					after_pway_analysis_time = time.time()

			if settings.get('print_timing') is True: 
				start_peak_len = time.time()
			# wait, i need to run this 2x?
				# 1. run bedop_merge to get length of unique peaks in own group
				# 2. same but to get length of matching peaks in other group
				# this lets me then compare peak sizes b/w groups?
			# inputs: unique key unique beds,
				# opposing merged beds (cancer vs nl)
			# outputs: 

			# this grabs unique common beds and merged beds from the other
				# test group (eg, unique 4kb broad cancer vs control merged)
				# this generates common beds from the merged beds ->
				# checks overlaps to grab only common peaks that overlap
				# with unique peaks from the other group
				# eg: all noncancer common peaks that overlap w/ unique
					# broad cancer peaks
			# this is run 2x to ensure thoroughness:
				# 1st: grab all common that overlap in the same group
				# 2nd: grab all common that overlap in other group
				# that way, avg peak lengths can be more thoroughly measured
					# for both test groups (test and control)
			peak_subdir_str = 'unique_g{ind1}_{group}_vs_{other_group}'\
				.format(ind1=ind1, group=group, other_group=other_group)
			compare_dict_key = 'g{ind1}_{group}_vs_{other_group}_comparsion'\
				.format(ind1=ind1, group=group, other_group=other_group)
			if group == 'control':
				kwargs1 = {'drop_control': False, 'drop_test': True}
				kwargs2 = {'drop_control': True, 'drop_test': False}
			else:
				kwargs1 = {'drop_control': True, 'drop_test': False}
				kwargs2 = {'drop_control': False, 'drop_test': True}

			hfnxns.get_peak_avg_sizes(
				merged_dict		=	file_handler(
										'get_peak_size_merged', \
										settings.get('save_dirs')\
											.get('merged_dir')\
											.get('path'),
											**kwargs1
								),
				compar_dict		=	file_handler(\
										'get_peak_size_unique_common', \
										settings.get('save_dirs')\
											.get('comparison_pipeline_dirs')\
											.get(compare_key)\
											.get('unique_common')\
											.get('unique_peak_beds')\
											.get(unique_key)\
											.get('unique_beds')\
											.get('path'),
										settings.get('save_dirs')\
											.get('comparison_pipeline_dirs')\
											.get(compare_key)\
											.get('peak_sizes')\
											.get(peak_subdir_str)\
											.get('reference_name')\
											.get('path'),
											**kwargs1
								),
				mp_workers 		= 	settings.get('workers')
			)

			hfnxns.get_peak_avg_sizes(
				merged_dict		=	file_handler(
										'get_peak_size_merged', \
										settings.get('save_dirs')\
											.get('merged_dir')\
											.get('path'),
											**kwargs2
								),
				compar_dict		=	file_handler(\
										'get_peak_size_unique_common', \
										settings.get('save_dirs')\
											.get('comparison_pipeline_dirs')\
											.get(compare_key)\
											.get('unique_common')\
											.get('unique_peak_beds')\
											.get(unique_key)\
											.get('unique_beds')\
											.get('path'),
										settings.get('save_dirs')\
											.get('comparison_pipeline_dirs')\
											.get(compare_key)\
											.get('peak_sizes')\
											.get(peak_subdir_str)\
											.get('comparison_name')\
											.get('path'),
											**kwargs1
								),
				mp_workers 		= 	settings.get('workers')
			)

			if settings.get('workers') is not None:
				if settings.get('workers').job_group_exists('avg_peaksize'):
					settings.get('workers').block_for_specific_job_group('avg_peaksize', reset_prio=False)

			if settings.get('print_timing') is True: 
				print('\t\t\tpeak length time: {} s'\
				.format(round(time.time()-start_peak_len,2)))

			if settings.get('print_timing') is True: 
				start_peak_len = time.time()

			# this grabs the common beds with average lengths from the 2 group from the last step and
				# compares the length b/w the 2 groups of beds
				# output format:
					# 4th and 5th columns are length and density ratio changes
					# 6th and 7th columns are lengths in reference and comparison groups
					# 8th and 9th columns are densities in reference and comparison groups
			kde_plot_dir_name = 'change_kde'
			hfnxns.peak_size_change_beds(
				ref_dict		=	file_handler(
										'peak_size_change_1', \
										settings.get('save_dirs')\
											.get('comparison_pipeline_dirs')\
											.get(compare_key)\
											.get('peak_sizes')\
											.get(peak_subdir_str)\
											.get('reference_name')\
											.get('path'),
										settings.get('save_dirs')\
											.get('comparison_pipeline_dirs')\
											.get(compare_key)\
											.get('comparison')\
											.get(compare_dict_key)\
											.get('path'),
											**kwargs1
								),
				other_dict		=	file_handler(\
										'peak_size_change_2', \
										settings.get('save_dirs')\
											.get('comparison_pipeline_dirs')\
											.get(compare_key)\
											.get('peak_sizes')\
											.get(peak_subdir_str)\
											.get('comparison_name')\
											.get('path'),
											**kwargs1
								)
			)
			if settings.get('print_timing') is True: 
				print('\t\t\tpeak length time: {} s'\
				.format(round(time.time()-start_peak_len,2)))


			if (settings.get('which_analysis').get('comparison_kde') == True):
				if settings.get('print_timing') is True: 
					start_comparison_kde = time.time()

				# plots kdes (basically smoothed histograms) of peak length or 
					# density changes b/w the 2 groups
					# 1st call is for length, 2nd is for density
				hfnxns.plot_kde_size_change(
					file_dict			=	file_handler(
												'compar_kde_size_change', \
												settings.get('save_dirs')\
													.get('comparison_pipeline_dirs')\
													.get(compare_key)\
													.get('comparison')\
													.get(compare_dict_key)\
													.get('path'),
													**kwargs1
											), \
					plot_dir 			= 	os.path.join(settings.get('save_dirs')\
													.get('comparison_pipeline_dirs')\
													.get(compare_key)\
													.get('plot_dir')\
													.get('path'),
												'change_kde'
											), \
					data_for_figures	= 	settings.get('save_dirs')\
												.get('data_for_figures').get('path'), \
					name_str			= 	'{specific_analysis}_{group_name}_to_{other_group}_all'\
												.format(specific_analysis=specific_analysis, \
													group_name=settings\
														.get('data_loading_info')\
														.get('{}_group_name'.format(group)),
													other_group=settings\
														.get('data_loading_info')\
														.get('{}_group_name'\
															.format(other_group))
											), \
					color_dict			=	settings.get('data_color_info')\
												.get('color_dict').get(group), 
					# value_index			=	3, \
					# log2				=	True, \
					value_indices		=	(5,6),
					len_or_dens 		=	'len',
				)
				
				if settings.get('print_timing') is True: 
					print('\t\t\tcomparison_kde 1 time: {} s'\
					.format(round(time.time()-start_comparison_kde,2)))
				if settings.get('print_timing') is True: 
					start_comparison_kde = time.time()
				hfnxns.plot_kde_size_change(
					file_dict			=	file_handler(
												'compar_kde_size_change', \
												settings.get('save_dirs')\
													.get('comparison_pipeline_dirs')\
													.get(compare_key)\
													.get('comparison')\
													.get(compare_dict_key)\
													.get('path'),
													**kwargs1
											), \
					plot_dir 			= 	os.path.join(settings.get('save_dirs')\
													.get('comparison_pipeline_dirs')\
													.get(compare_key)\
													.get('plot_dir')\
													.get('path'),
												'change_kde'
											), \
					data_for_figures	= 	settings.get('save_dirs')\
												.get('data_for_figures').get('path'), \
					name_str			= 	'{specific_analysis}_{group_name}_to_{other_group}_all'\
												.format(specific_analysis=specific_analysis, \
													group_name=settings\
														.get('data_loading_info')\
														.get('{}_group_name'\
															.format(group)),
													other_group=settings\
														.get('data_loading_info')\
														.get('{}_group_name'\
															.format(other_group))
											), \
					color_dict			=	settings.get('data_color_info')\
												.get('color_dict').get(group), 
					# value_index			=	3, \
					# log2				=	True, \
					value_indices		=	(7,8),
					len_or_dens 		=	'dens',
				)
				
				if settings.get('print_timing') is True: 
					print('\t\t\tcomparison_kde 2 time: {} s'\
					.format(round(time.time()-start_comparison_kde,2)))


		if (settings.get('which_analysis').get('compar_cancer_plot') == True):
			tsg_file = settings.get('main_pipeline_dirs').get('tuson_tsg_file')
			og_file = settings.get('main_pipeline_dirs').get('tuson_og_file')

			data_for_figures_dir = os.path.join(settings.get('save_dirs')\
							.get('data_for_figures').get('path'),
							'cancer_plots',
							'len')
			save_dir = os.path.join(settings.get('save_dirs')\
												.get('comparison_pipeline_dirs')\
												.get(compare_key)\
												.get('plot_dir')\
												.get('path'),
											'cancer_gene_plots')
			os.makedirs(data_for_figures_dir, exist_ok=True)
			os.makedirs(save_dir, exist_ok=True)			
			hfnxns.cancer_list_plots(
				analysis_dict		=	file_handler('compar_cancer_plot', 
											settings.get('save_dirs')
												.get('core_pipeline_dirs')
												.get('analyses')
												.get(specific_analysis)
												.get('gene_dir')
												.get('path'),
										), 
				other_dict			=	file_handler('compar_cancer_plot', 
											settings.get('save_dirs')
												.get('core_pipeline_dirs')
												.get('analyses')
												.get(other_analysis)
												.get('gene_dir')
												.get('path'),
										), 
				save_dir			=	save_dir, 
				len_or_len_change	=	'len', 
				tuson_or_cosmic		=	'tuson', 
				data_for_figures	=	data_for_figures_dir, 
				tuson_tsg			=	tsg_file, 
				tuson_og			=	og_file,
				mp_workers 			=	settings.get('workers')
				)

			hfnxns.cancer_list_plots(
				analysis_dict		=	file_handler('compar_cancer_plot', 
											settings.get('save_dirs')
												.get('core_pipeline_dirs')
												.get('analyses')
												.get(specific_analysis)
												.get('gene_dir')
												.get('path'),
										), 
				other_dict			=	file_handler('compar_cancer_plot', 
											settings.get('save_dirs')
												.get('core_pipeline_dirs')
												.get('analyses')
												.get(other_analysis)
												.get('gene_dir')
												.get('path'),
										), 
				save_dir			=	save_dir, 
				len_or_len_change	=	'len', 
				tuson_or_cosmic		=	'cosmic', 
				data_for_figures	=	data_for_figures_dir, 
				tuson_tsg			=	tsg_file, 
				tuson_og			=	og_file,
				mp_workers 			=	settings.get('workers'),
				cosmic_list 		= 	settings.get('main_pipeline_dirs')\
											.get('cosmic_file')
				)
		# raise
	
	if settings.get('workers') is not None:
		if settings.get('workers').job_group_exists('pathway_analysis'):
			settings.get('workers').block_for_specific_job_group('pathway_analysis', 
				reset_prio=False)

	step_name = 'comparison_path_dots'
	if (settings.get('which_analysis').get(step_name) == True):
		for analysis_loop, specific_analysis in enumerate(comparison):
			other_analysis = [analysis for analysis in comparison if \
				analysis != specific_analysis][0]
			analysis_key = 'group{}_normal_common'.format(analysis_loop+1)

			# make tissue type-wise control common beds for each group
			for group in ['test', 'control']:
				if analysis_loop == 0:
					ind1 = 1
					ind2 = 2
				else:
					ind1 = 2
					ind2 = 1
				if group == 'test':

					other_group = 'control'
				else:
					other_group = 'test'
				unique_key = 'g{ind1}_{group}_subtract_g{ind2}_{group}'.format(
					ind1=ind1, ind2=ind2, group=group)

				group_path_name = settings.get('save_dirs')\
													.get('comparison_pipeline_dirs')\
													.get(compare_key)\
													.get('unique_common')\
													.get('unique_peak_beds')\
													.get(unique_key)\
													.get('path').split('/')[-1]
				path_plot_dir = os.path.join(settings.get('save_dirs')\
												.get('comparison_pipeline_dirs')\
												.get(compare_key)\
												.get('plot_dir')\
												.get('path'),
										'pathway_analysis',
										group_path_name)

				if settings.get('print_timing') is True: 
					start_comparison_kde = time.time()


				for path_db_loop in ['hallmark_kegg_go_paths', 'hallmark_kegg_paths']:
					data_for_figures_dir = os.path.join(settings.get('save_dirs')\
							.get('data_for_figures').get('path'),
							'path_dots', path_db_loop)
					os.makedirs(data_for_figures_dir, exist_ok=True)
					hfnxns.unique_broad_sharp_data(
						textfile_dir		=	os.path.join(path_plot_dir, 
													path_db_loop, 
													'textfiles', 
													settings\
														.get('data_loading_info')\
														.get('{}_group_name'\
															.format(group))
												), \
						sample_types 		=	path_types, 
						plot_dir 			= 	os.path.join(settings.get('save_dirs')\
														.get('comparison_pipeline_dirs')\
														.get(compare_key)\
														.get('plot_dir')\
														.get('path'),
													'path_dots',
													path_db_loop
												), \
						name_str			= 	group_path_name, \
						color_dict			=	settings.get('data_color_info')\
													.get('color_dict').get(group), 
						data_for_figures	= 	data_for_figures_dir, \
						debug_print			=	settings.get('specific_verbosity')\
													.get('path_dots_debug_text')
					)

					if settings.get('print_timing') is True: 
						print('\t\t\tcomparison_kde time: {} s'\
						.format(round(time.time()-start_comparison_kde,2)))

# len change pipeline - haven't started on this
def len_change_pipeline(settings, file_handler, mapping_function):
	# making sample common beds
	step_name = 'len_change_common_beds'
	if settings.get('print_timing') is True: 
		start_make_common_beds = time.time()
	hfnxns.make_common_beds(
		file_percent 				=	settings.get('analysis_settings')\
											.get('core_analysis')\
											.get('general_settings')\
											.get(step_name), 
		file_dict					=	file_handler(
											'len_change_common_beds', 
											settings.get('save_dirs')\
												.get('merged_dir')\
												.get('path'),
											settings.get('save_dirs')\
												.get('length_change_pipeline_dirs')\
												.get('common_beds')\
												.get('path')
										),
		mp_workers 					= 	settings.get('workers'),
		merge_col_dict				=	{3:'avg', 4:'avg'}
		)

	if settings.get('workers') is not None:
		if settings.get('workers').job_group_exists('common_beds'):
			settings.get('workers').block_for_specific_job_group('common_beds', 
				reset_prio=False)

	if settings.get('print_timing') is True: 
		print(colored('\t\t\tlen_change common bed time: {} s'\
		.format(round(time.time()-start_make_common_beds,2)), 'red'))

	for group in ['test', 'control']:
		if settings.get('print_timing') is True: 
			start_make_common_beds = time.time()
		if group == 'test':
			other_group = 'control'
		else:
			other_group = 'test'

		kwargs = {'drop_{}'.format(other_group): False, \
			'drop_{}'.format(group): True}
		step_name = 'len_change_compar_0_common_beds'
		hfnxns.make_common_beds(
			file_percent 				=	0, 
			file_dict					=	file_handler(
												'len_change_compar_0_common_beds', 
												settings.get('save_dirs')\
													.get('merged_dir')\
													.get('path'),
												settings.get('save_dirs')\
													.get('length_change_pipeline_dirs')\
													.get(group)\
													.get('len_change_setup')\
													.get('other_0_common')\
													.get('path'),
													**kwargs
											), 
			mp_workers 					=	settings.get('workers'),
			merge_col_dict				=	{3:'avg', 4:'avg'}
			)

		if settings.get('workers') is not None:
			if settings.get('workers').job_group_exists('common_beds'):
				settings.get('workers').block_for_specific_job_group('common_beds', 
					reset_prio=False)
		
		if settings.get('print_timing') is True: 
			print(colored('\t\t\tlen_change common bed time: {} s'\
			.format(round(time.time()-start_make_common_beds,2)), 'red'))

		if settings.get('print_timing') is True: 
			start_peak_len = time.time()

		kwargs1 = {'drop_{}'.format(group): False, \
			'drop_{}'.format(other_group): True}
		kwargs2 = {'drop_{}'.format(other_group): False, \
			'drop_{}'.format(group): True}


		hfnxns.get_peak_avg_sizes(
			merged_dict		=	file_handler(
									'get_peak_size_merged', \
									settings.get('save_dirs')\
										.get('merged_dir')\
										.get('path'),
										**kwargs1
							),
			compar_dict		=	file_handler(\
									'get_peak_size_unique_common', \
									settings.get('save_dirs')\
										.get('length_change_pipeline_dirs')\
										.get('common_beds')\
										.get('path'),
									settings.get('save_dirs')\
										.get('length_change_pipeline_dirs')\
										.get(group)\
										.get('len_change_setup')\
										.get('peak_size_beds_for_comparison')\
										.get('ref_group_sizes')\
										.get('path'),
										**kwargs1
							),
			mp_workers 		= 	settings.get('workers')
		)

		hfnxns.get_peak_avg_sizes(
			merged_dict		=	file_handler(
									'get_peak_size_merged', \
									settings.get('save_dirs')\
										.get('merged_dir')\
										.get('path'),
										**kwargs2
							),
			compar_dict		=	file_handler(\
									'get_peak_size_unique_common', \
									settings.get('save_dirs')\
										.get('length_change_pipeline_dirs')\
										.get(group)\
										.get('len_change_setup')\
										.get('other_0_common')\
										.get('path'),
									settings.get('save_dirs')\
										.get('length_change_pipeline_dirs')\
										.get(group)\
										.get('len_change_setup')\
										.get('peak_size_beds_for_comparison')\
										.get('compar_group_sizes')\
										.get('path'),
										**kwargs2
							),
			mp_workers 		= 	settings.get('workers')
		)

		if settings.get('workers') is not None:
			if settings.get('workers').job_group_exists('avg_peaksize'):
				settings.get('workers').block_for_specific_job_group('avg_peaksize', 
					reset_prio=False)

		if settings.get('print_timing') is True: 
			print(colored('\t\t\tpeak length time: {} s'\
			.format(round(time.time()-start_peak_len,2)), 'red'))

		if settings.get('print_timing') is True: 
			start_peak_len = time.time()
		hfnxns.peak_size_change_beds(
			ref_dict		=	file_handler(
									'peak_size_change_1', \
									settings.get('save_dirs')\
										.get('length_change_pipeline_dirs')\
										.get(group)\
										.get('len_change_setup')\
										.get('peak_size_beds_for_comparison')\
										.get('ref_group_sizes')\
										.get('path'),
									settings.get('save_dirs')\
										.get('length_change_pipeline_dirs')\
										.get(group)\
										.get('len_change_setup')\
										.get('size_comparison_beds')\
										.get('path'),
										**kwargs1
							),
			other_dict		=	file_handler(\
									'peak_size_change_2', \
									settings.get('save_dirs')\
										.get('length_change_pipeline_dirs')\
										.get(group)\
										.get('len_change_setup')\
										.get('peak_size_beds_for_comparison')\
										.get('compar_group_sizes')\
										.get('path'),
										**kwargs2
							)
		)

		cutoff_settings =	settings.get('analysis_settings') \
									.get('len_change_analysis')
		if settings.get('print_timing') is True: 
			print(colored('\t\t\tpeak length time: {} s'\
			.format(round(time.time()-start_peak_len,2)), 'red'))
		no_change_cutoff = {}
		for dens_cutoff_setting in cutoff_settings.get('density_cutoffs')\
			.get('lengthen'):
			less_than_list = []
			greater_than_list = []
			less_than_cutoff = cutoff_settings.get('density_cutoffs')\
									.get('lengthen')\
									.get(dens_cutoff_setting)\
									.get('less_than')
			if less_than_cutoff is not None:
				less_than_list.append(less_than_cutoff)
			greater_than_cutoff = cutoff_settings.get('density_cutoffs')\
									.get('lengthen')\
									.get(dens_cutoff_setting)\
									.get('greater')
			if greater_than_cutoff is not None:
				greater_than_list.append(greater_than_cutoff)

			less_than_cutoff = cutoff_settings.get('density_cutoffs')\
									.get('shorten')\
									.get(cutoff_settings\
										.get('dens_cutoff_comparisons')\
										.get(dens_cutoff_setting))\
									.get('less_than')
			if less_than_cutoff is not None:
				less_than_list.append(less_than_cutoff)
			greater_than_cutoff = cutoff_settings.get('density_cutoffs')\
									.get('shorten')\
									.get(cutoff_settings\
										.get('dens_cutoff_comparisons')\
										.get(dens_cutoff_setting))\
									.get('greater')
			if greater_than_cutoff is not None:
				greater_than_list.append(greater_than_cutoff)
			if len(less_than_list) > 0:
				final_less_than = max(less_than_list)
			else:
				final_less_than = None
			if len(greater_than_list) > 0:
				final_greater_than = max(greater_than_list)
			else:
				final_greater_than = None

			no_change_cutoff[dens_cutoff_setting] = {
									'less_than': None,
									'greater': None
								}
		for len_setting in cutoff_settings.get('length_cutoffs').keys():
			if cutoff_settings.get('length_cutoffs').get(len_setting) is None:
				raise ValueError('length_cutoffs is missing setting {} from\
					\n\tdensity_cutoffs settings'.format(len_setting))
			for dens_cutoff_setting in cutoff_settings.get('density_cutoffs')\
				.get(len_setting):
				if settings.get('print_timing') is True: 
					start_peak_len = time.time()
				# gonna setup no_change_dict -> 
				if len_setting == 'lengthen':
					no_change_dict 		=	file_handler(
											'len_pipeline_no_change_len_filter', 
											settings.get('save_dirs')
												.get('length_change_pipeline_dirs')
												.get(group)
												.get('length_change_analysis')
												.get('no_change_peaks_analysis')
												.get(dens_cutoff_setting)
												.get('common_save')
												.get('path'),
												**kwargs1
									)

					this_dens_no_change_cutoff = no_change_cutoff.get(dens_cutoff_setting)
				else:
					no_change_dict = None
					this_dens_no_change_cutoff = None

				hfnxns.filter_len_dens_peaks(
					file_dict			=	file_handler(
												'len_pipeline_len_filter', 
												settings.get('save_dirs')
													.get('length_change_pipeline_dirs')
													.get(group)
													.get('len_change_setup')
													.get('size_comparison_beds')
													.get('path'),
												settings.get('save_dirs')
													.get('length_change_pipeline_dirs')
													.get(group)
													.get('length_change_analysis')
													.get('{}ed_peaks_analysis'
														.format(len_setting))
													.get(dens_cutoff_setting)
													.get('common_save')
													.get('path'),
													**kwargs1
										),
					no_change_dict 		=	no_change_dict,
					len_cutoff 			=	cutoff_settings.get('length_cutoffs'),
					dens_cutoff 		=	cutoff_settings.get('density_cutoffs')\
												.get(len_setting).get(dens_cutoff_setting),
					no_change_cutoff 	=	this_dens_no_change_cutoff,
					len_or_short 		=	len_setting
					)
				if settings.get('print_timing') is True: 
					print(colored('\t\t\tpeak length time: {} s'\
					.format(round(time.time()-start_peak_len,2)), 'red'))

				if (settings.get('which_analysis').get('step_name') == True) or \
					(settings.get('which_analysis').get('comparison_genes') == True):
					if settings.get('print_timing') is True: 
						start_match_tss_gene_names = time.time()
					hfnxns.match_tss_gene_names(
						bed_10kb_tss	=	settings.get('main_pipeline_dirs')\
												.get('bed_10kb_tss'),
						input_dict		=	file_handler(
												'len_change_match_tss_gene_names_input', \
												settings.get('save_dirs')
													.get('length_change_pipeline_dirs')
													.get(group)
													.get('length_change_analysis')
													.get('{}ed_peaks_analysis'
														.format(len_setting))
													.get(dens_cutoff_setting)
													.get('common_save')
													.get('path'),
													**kwargs1
										),
						output_dict		=	file_handler(\
												'len_change_match_tss_gene_names_output', \
												settings.get('save_dirs')
													.get('length_change_pipeline_dirs')
													.get(group)
													.get('length_change_analysis')
													.get('{}ed_peaks_analysis'
														.format(len_setting))
													.get(dens_cutoff_setting)
													.get('tss_gene_dir')
													.get('path'),
												settings.get('save_dirs')
													.get('length_change_pipeline_dirs')
													.get(group)
													.get('length_change_analysis')
													.get('{}ed_peaks_analysis'
														.format(len_setting))
													.get(dens_cutoff_setting)
													.get('gene_dir')
													.get('path'),
													**kwargs1
										)
					)

					if settings.get('print_timing') is True: 
						print(colored('\t\t\tmatch_tss_gene_names time: {} s'\
						.format(round(time.time()-start_match_tss_gene_names,2)), 'red'))

					# pathway analysis of in core pipeline
					step_name = 'len_change_path_analysis'
					if (settings.get('which_analysis').get('comparison_paths') == True):
						if settings.get('print_timing') is True: 
							start_pathway_analysis = time.time()
						group_path_name = '{}ed_{}_dens_peaks_analysis' \
							.format(len_setting, dens_cutoff_setting)
						path_plot_dir = os.path.join(settings.get('save_dirs')
														.get('length_change_pipeline_dirs')
														.get(group)
														.get('length_change_analysis')
														.get('{}ed_peaks_analysis'
															.format(len_setting))
														.get(dens_cutoff_setting)
														.get('plot_dir')\
														.get('path'),
												'pathway_analysis') #,
						os.makedirs(path_plot_dir, exist_ok=True)
						path_file_dict = file_handler(
														step_name, 
														settings.get('save_dirs')
															.get('length_change_pipeline_dirs')
															.get(group)
															.get('length_change_analysis')
															.get('{}ed_peaks_analysis'
																.format(len_setting))
															.get(dens_cutoff_setting)
															.get('gene_dir')
															.get('path'),
															**kwargs1
													)
						path_types = sorted(list(path_file_dict\
							.get(list(path_file_dict.keys())[0])\
							.keys()))
						settings['setup_variables'] = pathway_analysis(
							file_dict			=	path_file_dict,
							plot_dir			=	path_plot_dir, 
							pathway_gene_files 	=	settings.get('pathway_gene_files'),
							mp_workers 			=	settings.get('workers'),
							setup_variables 	= 	settings.get('setup_variables'),
						)

						if settings.get('workers') is not None:
							if settings.get('workers').job_group_exists('pathway_analysis'):
								settings.get('workers')\
									.prioritize_specific_job_group('pathway_analysis')


		if settings.get('print_timing') is True: 
			start_kde_size_change = time.time()
		hfnxns.plot_kde_size_change(
			file_dict			=	file_handler(
										'compar_kde_size_change', \
										settings.get('save_dirs')\
											.get('length_change_pipeline_dirs')\
											.get(group)\
											.get('len_change_setup')\
											.get('size_comparison_beds')\
											.get('path'),
											**kwargs1
									), \
			plot_dir 			= 	os.path.join(settings.get('save_dirs')
											.get('length_change_pipeline_dirs')
											.get(group)
											.get('length_change_analysis')
											.get('plot_dir')
											.get('path'),
										'change_kde'
									), \
			data_for_figures	= 	os.path.join(settings.get('save_dirs')\
										.get('data_for_figures').get('path'),
											'len_ch_ref_'+group), \
			name_str			= 	'dens_change_kde',
			color_dict			=	settings.get('data_color_info')\
										.get('color_dict').get(group), 
			# value_index			=	3, \
			# log2				=	True, \
			value_indices		=	(7,8),
			len_or_dens 		=	'dens',
		)
		if settings.get('print_timing') is True: 
			print(colored('\t\t\tkde_size_change time: {} s'\
			.format(round(time.time()-start_kde_size_change,2)), 'red'))
				
		len_setting = 'no_change'
		for dens_cutoff_setting in cutoff_settings.get('density_cutoffs')\
			.get('lengthen'):
			if settings.get('print_timing') is True: 
				start_kde_size_change = time.time()

			if (settings.get('which_analysis').get('step_name') == True) or \
				(settings.get('which_analysis').get('comparison_genes') == True):
				if settings.get('print_timing') is True: 
					start_match_tss_gene_names = time.time()
				hfnxns.match_tss_gene_names(
					bed_10kb_tss	=	settings.get('main_pipeline_dirs')\
											.get('bed_10kb_tss'),
					input_dict		=	file_handler(
											'len_change_match_tss_gene_names_input', \
											settings.get('save_dirs'
												)
												.get('length_change_pipeline_dirs')
												.get(group)
												.get('length_change_analysis')
												.get('no_change_peaks_analysis')
												.get(dens_cutoff_setting)
												.get('common_save')
												.get('path'),
												**kwargs1
									),
					output_dict		=	file_handler(\
											'len_change_match_tss_gene_names_output', \
											settings.get('save_dirs')
												.get('length_change_pipeline_dirs')
												.get(group)
												.get('length_change_analysis')
												.get('no_change_peaks_analysis')
												.get(dens_cutoff_setting)
												.get('tss_gene_dir')
												.get('path'),
											settings.get('save_dirs')
												.get('length_change_pipeline_dirs')
												.get(group)
												.get('length_change_analysis')
												.get('no_change_peaks_analysis')
												.get(dens_cutoff_setting)
												.get('gene_dir')
												.get('path'),
												**kwargs1
									)
				)

				if settings.get('print_timing') is True: 
					print(colored('\t\t\tmatch_tss_gene_names time: {} s'\
					.format(round(time.time()-start_match_tss_gene_names,2)), 'red'))

			# pathway analysis of in core pipeline
			step_name = 'len_change_path_analysis'
			if (settings.get('which_analysis').get('comparison_paths') == True):
				if settings.get('print_timing') is True: 
					start_pathway_analysis = time.time()
				group_path_name = '{}ed_{}_dens_peaks_analysis' \
					.format(len_setting, dens_cutoff_setting)
				path_plot_dir = os.path.join(settings.get('save_dirs')
												.get('length_change_pipeline_dirs')
												.get(group)
												.get('length_change_analysis')
												.get('no_change_peaks_analysis')
												.get(dens_cutoff_setting)
												.get('plot_dir')\
												.get('path'),
										'pathway_analysis')
				os.makedirs(path_plot_dir, exist_ok=True)
				path_file_dict = file_handler(
												step_name, 
												settings.get('save_dirs')
													.get('length_change_pipeline_dirs')
													.get(group)
													.get('length_change_analysis')
													.get('no_change_peaks_analysis')
													.get(dens_cutoff_setting)
													.get('gene_dir')
													.get('path'),
													**kwargs1
											)
				path_types = sorted(list(path_file_dict\
					.get(list(path_file_dict.keys())[0])\
					.keys()))
				settings['setup_variables'] = pathway_analysis(
					file_dict			=	path_file_dict,
					plot_dir			=	path_plot_dir, 
					pathway_gene_files 	=	settings.get('pathway_gene_files'),
					mp_workers 			=	settings.get('workers'),
					setup_variables 	= 	settings.get('setup_variables'),
				)
				
				if settings.get('workers') is not None:
					if settings.get('workers').job_group_exists('pathway_analysis'):
						settings.get('workers')\
							.prioritize_specific_job_group('pathway_analysis')
				if settings.get('print_timing') is True: 
					print(colored('\t\t\tpathway_analysis time: {} s'\
					.format(round(time.time()-start_pathway_analysis,2)), 'red'))

			# what am I looping through?
				# looping through test_groups -> dens cutoff groups?
			if (settings.get('which_analysis').get('comparison_kde') == True):
				if settings.get('print_timing') is True: 
					start_comparison_kde = time.time()

				hfnxns.plot_kde_size_change(
					file_dict			=	file_handler(
												'compar_kde_size_change', 
												settings.get('save_dirs')
													.get('length_change_pipeline_dirs')
													.get(group)
													.get('len_change_setup')
													.get('size_comparison_beds')
													.get('path'),
													**kwargs1
											), 
					plot_dir 			= 	os.path.join(settings.get('save_dirs')
													.get('length_change_pipeline_dirs')
													.get(group)
													.get('length_change_analysis')
													.get('plot_dir')
													.get('path'),
												'change_kde'
											), 
					data_for_figures	= 	os.path.join(settings.get('save_dirs')\
												.get('data_for_figures').get('path'),
													'len_ch_ref_'+group), 
					name_str			= 	dens_cutoff_setting, 
					color_dict			=	settings.get('data_color_info')
												.get('color_dict').get(group), 
					# value_index			=	3, 
					# log2				=	True, 
					value_indices		=	(5,6),
					len_or_dens 		=	'len',
					dens_cutoff 		=	no_change_cutoff.get(dens_cutoff_setting),
					dens_cutoff_ind 	=	(7,8),
					abs_x_min_max 		=	(-10000,10000)
				)
				
				if settings.get('print_timing') is True: 
					print(colored('\t\t\tcomparison_kde time: {} s'\
					.format(round(time.time()-start_comparison_kde,2)), 'red'))

			if (settings.get('which_analysis').get('comparison_kde') == True):
				if settings.get('print_timing') is True: 
					start_comparison_kde = time.time()

				hfnxns.len_dens_change_scatter(
					no_change_dict 		=	file_handler(
												'len_change_match_tss_gene_names_input', \
												settings.get('save_dirs')
													.get('length_change_pipeline_dirs')
													.get(group)
													.get('length_change_analysis')
													.get('no_change_peaks_analysis')
													.get(dens_cutoff_setting)
													.get('common_save')
													.get('path'),
													**kwargs1
												), 
					lengthened_dict 	=	file_handler(
												'len_change_match_tss_gene_names_input', \
												settings.get('save_dirs')
													.get('length_change_pipeline_dirs')
													.get(group)
													.get('length_change_analysis')
													.get('lengthened_peaks_analysis')
													.get(dens_cutoff_setting)
													.get('common_save')
													.get('path'),
													**kwargs1
												), 
					shortened_dict		=	file_handler(
												'len_change_match_tss_gene_names_input', \
												settings.get('save_dirs')
													.get('length_change_pipeline_dirs')
													.get(group)
													.get('length_change_analysis')
													.get('shortened_peaks_analysis')
													.get(cutoff_settings
														.get('dens_cutoff_comparisons')
														.get(dens_cutoff_setting))
													.get('common_save')
													.get('path'),
													**kwargs1
											), 
					all_no_change_dict 	=	file_handler(
												'len_change_match_tss_gene_names_input', \
												settings.get('save_dirs')
													.get('length_change_pipeline_dirs')
													.get(group)
													.get('length_change_analysis')
													.get('no_change_peaks_analysis')
													.get('no_cutoff')
													.get('common_save')
													.get('path'),
													**kwargs1
											), 
					all_lengthened_dict =	file_handler(
												'len_change_match_tss_gene_names_input', \
												settings.get('save_dirs')
													.get('length_change_pipeline_dirs')
													.get(group)
													.get('length_change_analysis')
													.get('lengthened_peaks_analysis')
													.get('no_cutoff')
													.get('common_save')
													.get('path'),
													**kwargs1
											), 
					all_shortened_dict	=	file_handler(
												'len_change_match_tss_gene_names_input', \
												settings.get('save_dirs')
													.get('length_change_pipeline_dirs')
													.get(group)
													.get('length_change_analysis')
													.get('shortened_peaks_analysis')
													.get('no_cutoff')
													.get('common_save')
													.get('path'),
													**kwargs1
											), 
					plot_dir 			=	settings.get('save_dirs')
													.get('length_change_pipeline_dirs')
													.get(group)
													.get('length_change_analysis')
													.get('plot_dir')
													.get('path'),
					data_for_figures	= 	os.path.join(settings.get('save_dirs')\
												.get('data_for_figures').get('path'),
													'len_ch_ref_'+group), 
					name_str 			=	'len_dens_change_scatter/{}'
												.format(dens_cutoff_setting),
					color_dict			=	settings.get('data_color_info')
												.get('color_dict').get(group), 
					len_indices		=	(5,6),
					dens_indices 	=	(7,8),
					), 

				if settings.get('print_timing') is True: 
					print(colored('\t\t\tlen_dens_scatter time: {} s'\
					.format(round(time.time()-start_comparison_kde,2)), 'red'))
				if settings.get('print_timing') is True: 
					start_comparison_kde = time.time()
				hfnxns.plot_kde_size_change(
					file_dict			=	file_handler(
												'compar_kde_size_change', 
												settings.get('save_dirs')
													.get('length_change_pipeline_dirs')
													.get(group)
													.get('len_change_setup')
													.get('size_comparison_beds')
													.get('path'),
													**kwargs1
											), 
					plot_dir 			= 	os.path.join(settings.get('save_dirs')
													.get('length_change_pipeline_dirs')
													.get(group)
													.get('length_change_analysis')
													.get('plot_dir')
													.get('path'),
												'change_kde'
											), 
					data_for_figures	= 	os.path.join(settings.get('save_dirs')\
												.get('data_for_figures').get('path'),
													'len_ch_ref_'+group), 
					name_str			= 	dens_cutoff_setting, 
					color_dict			=	settings.get('data_color_info')
												.get('color_dict').get(group), 
					# value_index			=	3, 
					# log2				=	True, 
					value_indices		=	(5,6),
					len_or_dens 		=	'len',
					dens_cutoff 		=	no_change_cutoff.get(dens_cutoff_setting),
					dens_cutoff_ind 	=	(7,8),
					abs_x_min_max 		=	(-10000,10000)
				)
				
				if settings.get('print_timing') is True: 
					print(colored('\t\t\tcomparison_kde time: {} s'\
					.format(round(time.time()-start_comparison_kde,2)), 'red'))

			if (settings.get('which_analysis').get('compar_cancer_plot') == True):
				tsg_file = settings.get('main_pipeline_dirs').get('tuson_tsg_file')
				og_file = settings.get('main_pipeline_dirs').get('tuson_og_file')

				data_for_figures_dir = os.path.join(settings.get('save_dirs')\
								.get('data_for_figures').get('path'), 
								'len_ch_ref_'+group, 'cancer_plots', 
								'len_change_{}'.format(dens_cutoff_setting))
				save_dir = os.path.join(settings.get('save_dirs')
													.get('length_change_pipeline_dirs')
													.get(group)
													.get('length_change_analysis')
													.get('plot_dir')
													.get('path'),
												'cancer_gene_plots',
												dens_cutoff_setting)
				os.makedirs(data_for_figures_dir, exist_ok=True)
				os.makedirs(save_dir, exist_ok=True)			
				hfnxns.cancer_list_plots(
					analysis_dict		=	file_handler(
												'len_change_cancer_plot', \
												settings.get('save_dirs')
													.get('length_change_pipeline_dirs')
													.get(group)
													.get('length_change_analysis')
													.get('lengthened_peaks_analysis')
													.get(dens_cutoff_setting)
													.get('gene_dir')
													.get('path'),
													**kwargs1
												), 
					other_dict			=	file_handler(
												'len_change_cancer_plot', \
												settings.get('save_dirs')
													.get('length_change_pipeline_dirs')
													.get(group)
													.get('length_change_analysis')
													.get('shortened_peaks_analysis')
													.get(cutoff_settings
														.get('dens_cutoff_comparisons')
														.get(dens_cutoff_setting))
													.get('gene_dir')
													.get('path'),
													**kwargs1
												), 
					third_dict			=	file_handler(
												'len_change_cancer_plot', \
												settings.get('save_dirs')
													.get('length_change_pipeline_dirs')
													.get(group)
													.get('length_change_analysis')
													.get('no_change_peaks_analysis')
													.get(dens_cutoff_setting)
													.get('gene_dir')
													.get('path'),
													**kwargs1
												), 
					save_dir			=	save_dir, 
					len_or_len_change	=	'len_change', 
					tuson_or_cosmic		=	'tuson', 
					data_for_figures	=	data_for_figures_dir, 
					tuson_tsg			=	tsg_file, 
					tuson_og			=	og_file,
					mp_workers 			=	settings.get('workers')
					)

				hfnxns.cancer_list_plots(
					analysis_dict		=	file_handler(
												'len_change_cancer_plot', \
												settings.get('save_dirs')
													.get('length_change_pipeline_dirs')
													.get(group)
													.get('length_change_analysis')
													.get('lengthened_peaks_analysis')
													.get(dens_cutoff_setting)
													.get('gene_dir')
													.get('path'),
													**kwargs1
												), 
					other_dict			=	file_handler(
												'len_change_cancer_plot', \
												settings.get('save_dirs')
													.get('length_change_pipeline_dirs')
													.get(group)
													.get('length_change_analysis')
													.get('shortened_peaks_analysis')
													.get(cutoff_settings
														.get('dens_cutoff_comparisons')
														.get(dens_cutoff_setting))
													.get('gene_dir')
													.get('path'),
													**kwargs1
												), 
					third_dict			=	file_handler(
												'len_change_cancer_plot', \
												settings.get('save_dirs')
													.get('length_change_pipeline_dirs')
													.get(group)
													.get('length_change_analysis')
													.get('no_change_peaks_analysis')
													.get(dens_cutoff_setting)
													.get('gene_dir')
													.get('path'),
													**kwargs1
												), 
					save_dir			=	save_dir, 
					len_or_len_change	=	'len_change', 
					tuson_or_cosmic		=	'cosmic', 
					data_for_figures	=	data_for_figures_dir, 
					tuson_tsg			=	tsg_file, 
					tuson_og			=	og_file,
					mp_workers 			=	settings.get('workers'),
					cosmic_list 		= 	settings.get('main_pipeline_dirs')\
												.get('cosmic_file')
					)

	if settings.get('workers') is not None:
		if settings.get('workers').job_group_exists('pathway_analysis'):
			settings.get('workers').block_for_specific_job_group('pathway_analysis', reset_prio=False)

	for group in ['test', 'control']:
		if group == 'test':
			other_group = 'control'
		else:
			other_group = 'test'

		kwargs1 = {'drop_{}'.format(group): False, \
			'drop_{}'.format(other_group): True}
		kwargs2 = {'drop_{}'.format(other_group): False, \
			'drop_{}'.format(group): True}

		for len_setting in cutoff_settings.get('density_cutoffs').keys():
			for dens_cutoff_setting in cutoff_settings.get('density_cutoffs')\
				.get(len_setting):

				step_name = 'len_change_path_analysis'
				group_path_name = '{}ed_{}_dens_peaks_analysis' \
					.format(len_setting, dens_cutoff_setting)
				path_plot_dir = os.path.join(settings.get('save_dirs')
							.get('length_change_pipeline_dirs')
							.get(group)
							.get('length_change_analysis')
							.get('{}ed_peaks_analysis'
								.format(len_setting))
							.get(dens_cutoff_setting)
							.get('plot_dir')\
							.get('path'),
					'pathway_analysis')
				path_file_dict = file_handler(
						step_name, 
						settings.get('save_dirs')
							.get('length_change_pipeline_dirs')
							.get(group)
							.get('length_change_analysis')
							.get('{}ed_peaks_analysis'
								.format(len_setting))
							.get(dens_cutoff_setting)
							.get('gene_dir')
							.get('path'),
							**kwargs1
					)
				path_types = sorted(list(path_file_dict\
					.get(list(path_file_dict.keys())[0])\
					.keys()))

				if settings.get('print_timing') is True: 
					start_comparison_kde = time.time()


				for path_db_loop in ['hallmark_kegg_go_paths', \
						'hallmark_kegg_paths']:
					data_for_figures_dir = os.path.join(
							settings.get('save_dirs')\
							.get('data_for_figures').get('path'),
							'len_ch_ref_'+group, 'path_dots', path_db_loop)
					os.makedirs(data_for_figures_dir, exist_ok=True)
					hfnxns.unique_broad_sharp_data(
						textfile_dir	=	os.path.join(path_plot_dir, 
												path_db_loop, 
												'textfiles', 
												settings\
													.get('data_loading_info')\
													.get('{}_group_name'\
														.format(group))
											), \
						sample_types 	=	path_types, 
						# only have dens cutoff plot dir
							# either have to put dots there or manually
							# split -> higher dir in the dir tree
						plot_dir 		= 	os.path.join('/'.join(settings.get('save_dirs')
												.get('length_change_pipeline_dirs')
												.get(group)
												.get('length_change_analysis')
												.get('{}ed_peaks_analysis'
													.format(len_setting))
												.get(dens_cutoff_setting)
												.get('plot_dir')\
												.get('path').split('/')[:-2]),
											'path_dots',
											path_db_loop
											), \
						name_str		= 	group_path_name, \
						color_dict		=	settings.get('data_color_info')\
												.get('color_dict').get(group), 
						data_for_figures	= 	data_for_figures_dir, \
						debug_print			=	settings.get('specific_verbosity')\
													.get('path_dots_debug_text')
					)

					if settings.get('print_timing') is True: 
						print(colored('\t\t\tcomparison_kde time: {} s'\
						.format(round(time.time()-start_comparison_kde,2)), 
							'red'))










# these are "helper functions" for the pipeline
	# the reason these are here rather than in the helper functions 
	# file (theoretically) is that these would ONLY be used in this pipeline, 
	# whereas i might import the functions in the other pipeline for 
	# other chip-seq related analyses that reason might not hold up in all 
	# cases, but that's the thought right now also some of these functions 
	# are holdovers from the previous version of this code, and they need 
	# to be fixed or removed

def setup_verbosity(pipeline_steps, overall_verbose, specific_verbose_dict):
	# this is for the "verbosity dict" which will tell the pipeline what steps 
		# to print verbose information aboutit have an overall_verbose argument, 
		# which is basically a default for all steps, and a specific_verbose_dict, 
		# whichallows settings specific to specific stepsthis sets all steps 
		# without a specific verbose setting to the default value, so that it 
		# will be simpler to check in later steps

	if overall_verbose not in [True, False]:
		raise ValueError('overall_verbose setting must be in [True, False]')
	verbosity_dict = {}
	added = False
	for pipe_step in pipeline_steps:
		if specific_verbose_dict is not None:
			if specific_verbose_dict.get(pipe_step) is not None:
				verbosity_dict[pipe_step] = specific_verbose_dict.get(pipe_step)
				if specific_verbose_dict.get(pipe_step) not in [True, False]:
					raise ValueError(\
						'specific_verbosity for {} was not in [True, False, or None]'\
						.format(pipe_step))
				added = True
		if added is False:
			verbosity_dict[pipe_step] = overall_verbose
	return verbosity_dict

def set_absolute_filepaths(settings, verbose):
	# this function sets overall filepaths for pipeline files that can be inside 
		# other directories
		# the goal of this function is that, for example, for bed_10kb_tss, it 
			# can be inside the "useful_file_dir", in which case
			# the pipeline takes a relative path to the file, or it can be 
			# elsewhere, and given an absolute filepath
		# if it is not given as an absolute filepath, then the path given is 
			# joined with the path to the directory that it's expected in
			# in this example, it would be: 
				# option1: /path/to/bed_10kb_tss
				# option2: /path/to/useful_file_dir/path/to/bed_10kb_tss
	# the function only takes the pipeline settings dict + a verbosity argument

	def set_absolute_path(settings, this_setting, verbose):
		this_path = settings.get(this_setting[0][0]).get(this_setting[0][1])
		if verbose == True:
			print(this_setting)
			print(this_path)
		if this_path[0] == '/':
			pass
		else:
			this_path = os.path.join(settings.get(this_setting[1][0])\
				.get(this_setting[1][1]), this_path)
			settings[this_setting[0][0]][this_setting[0][1]] = this_path
			if verbose == True:
				print(this_path)
		if verbose == True:
			print('')

	path_join_dict = {
		('main_pipeline_dirs', 'bed_10kb_tss'): \
			('top_dirs', 'useful_file_dir'), 
		('main_pipeline_dirs', 'cosmic_file'): \
			('top_dirs', 'useful_file_dir'), 
		('main_pipeline_dirs', 'tuson_tsg_file'): \
			('top_dirs', 'useful_file_dir'), 
		('main_pipeline_dirs', 'tuson_og_file'): \
			('top_dirs', 'useful_file_dir'), 
		
		('pathway_gene_files', 'unique_gene_list_location'): \
			('top_dirs', 'useful_file_dir'), 
		('pathway_gene_files', 'kegg_lists_location'): \
			('top_dirs', 'useful_file_dir'), 
		('pathway_gene_files', 'hallmark_lists_location'): \
			('top_dirs', 'useful_file_dir'), 
		('pathway_gene_files', 'all_paths_location'): \
			('top_dirs', 'useful_file_dir'), 
		('pathway_gene_files', 'go_lists_location'): \
			('top_dirs', 'useful_file_dir'), 

		('data_color_info', 'r_color_name_file'): \
			('top_dirs', 'useful_file_dir'), 
		('data_color_info', 'r_color_number_file'): \
			('top_dirs', 'useful_file_dir'), 

		('data_loading_info', 'test_data_dir'): \
			('top_dirs', 'top_peakfile_dir'), 
		('data_loading_info', 'control_data_dir'): \
			('top_dirs', 'top_peakfile_dir'),

		('main_pipeline_dirs', 'top_save_dir'): \
			('top_dirs', 'analysis_top_dir'), 
	}

	for this_setting in path_join_dict.keys():
		set_absolute_path(settings, [this_setting, \
			path_join_dict.get(this_setting)], verbose)

def find_organize_input_files(settings, verbose=True):
	# this just grabs the relevant settings for organizing files and 
		# passes them to the "get_files_using_tag_filename" function, 
		# which actually organizes files
	# explanation of arguments:
		# settings: the main settings dictionary for the pipeline

	test_load_format = settings.get('data_loading_info')\
		.get('test_load_format')
	test_data_dir = settings.get('data_loading_info')\
		.get('test_data_dir')
	control_load_format = settings.get('data_loading_info')\
		.get('control_load_format')
	control_data_dir = settings.get('data_loading_info')\
		.get('control_data_dir')

	test_data_dict = \
		get_files_using_tag_filename(test_data_dir, \
			test_load_format, verbose=verbose)
	control_data_dict = \
		get_files_using_tag_filename(control_data_dir, \
			control_load_format, verbose=verbose)

	if verbose is True:
		for mark in test_data_dict.keys():
			if control_data_dict.get(mark) is None:
				print('No normal data for mark {}'.format(mark))
			else:
				for group in test_data_dict.get(mark).keys():
					if control_data_dict.get(mark).get(group) is None:
						print('No normal data for group {} in mark {}'\
							.format(mark, group))

	return test_data_dict, control_data_dict
	
def get_files_using_tag_filename(load_dir, load_format, verbose=True):
	# this function organizes files/samples for the pipeline. They are loaded 
		# (aka copied and slightly modified) into the analysis directory later by 
		# "load_raw_data"
		# this function is called 2x: once for test group data and once for 
			# control group data
	#explanation of arguments:
		# load_dir: the path to the directory containing the input files
		# load_format: gives the format for the filenames for the data. This is 
			# important because histone mark, group name, and sample id can
			# all be in the name of the input files and can be inferred using 
			# this function if the load format setting is setup correctly

	def get_all_filepaths(dir_to_walk):
		# this grabs all filepaths below "dir_to_walk" in a single, joined 
			# list for further handling this is why file name formats can 
			# have "/" in the name, because this will walk all the way down 
			# the directory tree looking for files

		walk_list = os.walk(dir_to_walk)

		all_filepaths = []
		for path in walk_list:
			for file in path[2]:
				all_filepaths.append(os.path.join(path[0], file).replace(dir_to_walk,''))

		if len(all_filepaths) is 0:
			raise ValueError('No files found at {}'.format(dir_to_walk))


		return all_filepaths

	def make_file_tag_dict(load_format, all_filepaths, verbose):
		# this function grabs "tags" for each filepath (from "get_all_filepaths"). 
			# Tags are the sample info that is pulled from filenames 
			# (eg, histone mark, and sample id)
			# they are found in the filepath by looking for the {group_name} or 
				# {sample_id} patterns in the file path

		def pipeline_regex_split_steps(input_string):
			# this splits the load_format string to grab a list of all string 
				# sections (inside tags and out) and a list of all tag strings
				# this is used for later regex to ID filepaths that match the 
					# non-tag parts of the filepath
				# and to match tag parts of the filepath to the appropriate path

			regex = []
			for find in list(re.finditer('\{(.*?)\}', input_string)):
				regex.append(find.start())
				regex.append(find.end())
			if 0 not in regex:
				regex.insert(0,0)
			if regex[-1] != len(input_string):
				regex.append(len(input_string))

			string_split = []
			for this_index in range(len(regex)-1):
				string_split.append(input_string[regex[this_index]:regex[this_index+1]])
			had_tag = False
			has_tag_list = []
			for this_item in string_split:
				if re.match('\{(.*?)\}', this_item) is not None:
					if had_tag is True:
						raise ValueError(\
							'Cannot have 2 tags with nothing between them. This is impossible to parse. {}'\
							.format(input_string))
					had_tag = True
					has_tag_list.append(this_item)
				else:
					had_tag = False
			return string_split, has_tag_list

		def pipeline_split_file_by_regex(string_split, the_file, verbose):
			# this splits the filepaths based on regex pulled from the load_format 
				# string to identify files that should be loadedand identify what 
				# their values are for each tagbasically, regex is used to see if 
				# a file matches all of the non-tag regions from the load_format 
				# stringand if it does, then the function grabs all of the parts 
				# of the filepath that don't match non-tag regions and matches 
				# them tothe correct tag based on the order of the regions
				# eg, load_format: /Documents/{histone_mark}/{group_id}/{sample_id}, 
					# filepath: /Documents/E/BRCA/B000
					# histone mark = E, group_id = BRCA, sample_id = B000
				# IMPORTANT: the strings between tags CANNOT also be found in tags.
					# there is no way to address this without requiring the tags to 
						# include strict regex, which i thought was a bigger 
						# inconvenience
					# this will break if you have something like: 
						# /Documents/{group_id}/{sample_id}
						# /Documents/BR/CA/B000
						# group_id = BR/CA, sample_id = B000

			split_file = []
			file_tags = []
			original_tags = []
			original_splitters = []
			file_to_modify = the_file
			tag_dict = {}
			failed = False
			for item_loop, item in enumerate(string_split):
				if re.match('\{(.*?)\}', item) is None:
					split_file.append(item)
					file_to_modify = file_to_modify.replace(item, '', 1)
					original_splitters.append(item)
				else:
					if item_loop+1 < len(string_split):
						split_on = string_split[item_loop+1]
						find = list(re.finditer(re.escape(split_on), file_to_modify))
						if len(find) > 0:
							my_ind = find[0].start()
							my_str = file_to_modify[:my_ind]
						else:
							my_str = file_to_modify
					else:
						my_str = file_to_modify
					original_tags.append(item[1:-1])
					file_tags.append(my_str)
					tag_name = item[1:-1]
					if tag_dict.get(tag_name) is not None:
						if tag_dict.get(tag_name) != my_str:
							if verbose is True:
								print('Not all instances of tag {} had the same string for file \n\t{}'\
									.format(tag_name, the_file))
							failed = True
					tag_dict[tag_name] = my_str
					file_to_modify = file_to_modify.replace(my_str, '', 1)
					split_file.append(my_str)
			form_string = ''.join(string_split)
			recreated_string = form_string.format(**tag_dict)
			if not recreated_string == the_file or any([tag_dict.get(this_tag) is \
				None for this_tag in original_tags]) or \
				any([this_str not in the_file for this_str in original_splitters]):
				if verbose is True:
					print('File was not accepted: \n\t{}\nVersion after formatting: \n\t{}\n'\
						.format(the_file, recreated_string))
				failed = True
			if failed is True:
				return None, None, None
			else:
				return split_file, file_tags, tag_dict

		regex_split = pipeline_regex_split_steps(load_format)[0]
		file_tags = {}
		for filepath in all_filepaths:
			file_tags[filepath] = \
				pipeline_split_file_by_regex(regex_split, filepath, verbose=verbose)[2]

		if len(list(file_tags.keys())) == 0:
			raise ValueError('No files found for load_format {}'\
				.format(load_format))

		return file_tags

	def make_mark_group_dict(file_tags):
		# this takes the dictionary from make_file_tag_dict 
			# (key: filepath, value: tag list) and converts it to a 
			# multi-level dictionary that should
			# be easier to loop through later

		marks = {}
		for filename in file_tags.keys():
			if file_tags.get(filename) is None:
				continue
			group_name = file_tags.get(filename).get('group_name')
			sample_id = file_tags.get(filename).get('sample_id')
			mark = file_tags.get(filename).get('mark')
			if mark not in marks.keys():
				marks[mark] = {}
			if group_name not in marks[mark].keys():
				marks[mark][group_name] = []
			marks[mark][group_name].append(sample_id)
		return marks


	all_filepaths = get_all_filepaths(load_dir)

	file_tags = make_file_tag_dict(load_format, all_filepaths, verbose)

	return make_mark_group_dict(file_tags)
	
def merge_size_comparison_scatter(plot_dir, fig_data_save, color_dict, \
	merge_sizes, save_names, file_dict, verbose, cutoff=4000):
	if verbose is True:
		print('Making Merge Size Plots')
	data_save_spot = os.path.join(fig_data_save, save_names.get('figdata_json'))
	if (not os.path.isfile(os.path.join(plot_dir, save_names.get('all_plot')))) or \
		(not os.path.isfile(os.path.join(plot_dir, save_names.get('subplots')))):
		if not os.path.isfile(data_save_spot):
			peak_counts = []
			dir_peak_count = {}
			group_names = []
			for dir_loop, test_group in enumerate(sorted(list(file_dict.keys()))):
				for sample_group in list(file_dict.get(test_group).keys()):
					group_names.append('{} {}'.format(test_group, sample_group))
					merge_peak_count = {}
					for this_size in merge_sizes:
						file_peak_num = {}
						sample_list = file_dict.get(test_group).get(sample_group)
						if sample_list is not None:
							for filepath in sorted(sample_list):
								sample_id = filepath[0]
								filepath = filepath[1]
								peak_num = 0
								with open(filepath, 'r') as textfile:
									current_line = None
									for line in textfile:
										line = line.rstrip('\n').split('\t')
										if len(line) > 1:
											if current_line == None:
												current_line = line
											else:
												if (line[0] == current_line[0]) and \
													(int(line[1]) < \
													(int(current_line[2])+int(this_size))):
													if int(line[2]) > int(current_line[2]):
														current_line[2] == line[2]
												else:
													peak_num += 1
													current_line = line
								file_peak_num[sample_id] = peak_num
						merge_peak_count[this_size] = file_peak_num
					dir_peak_count[group_names[-1]] = merge_peak_count
			merge_scatter_dict = {
				'raw_data': dir_peak_count,
				'merge_sizes': merge_sizes,
				'sample_groups': group_names,
				'color_dict': color_dict,
				'plot_dir': plot_dir,
				'save_names': save_names,
			}
			with open(data_save_spot, 'w') as textfile:
				json.dump(merge_scatter_dict, textfile)	
			merge_scatter_dict = json.loads(json.dumps(merge_scatter_dict))
		else:
			with open(data_save_spot, 'r') as textfile:
				merge_scatter_dict = json.load(textfile)				
		hfnxns.plot_merge_scatter(merge_scatter_dict.get('raw_data'), \
			merge_scatter_dict.get('merge_sizes'), \
			merge_scatter_dict.get('sample_groups'), \
			color_dict=merge_scatter_dict.get('color_dict'), \
			save_dir = merge_scatter_dict.get('plot_dir'), \
			save_names = save_names, verbose=verbose)
		
def pathway_analysis(file_dict, plot_dir, pathway_gene_files, alpha=0.05, \
	paths_per_analysis=1000, go_paths=True, mp_workers=None, setup_variables=None):

	setup_variables['no_go_paths'] = hfnxns.run_overlap_over_gene_lists(
		file_dict, plot_dir, 
		alpha=alpha, paths_per_analysis=paths_per_analysis, go_paths=True, \
		mp_workers=mp_workers, setup_variables=setup_variables.get('no_go_paths'), \
		**pathway_gene_files)
	setup_variables['go_paths'] = hfnxns.run_overlap_over_gene_lists(
		file_dict, plot_dir, 
		alpha=alpha, paths_per_analysis=paths_per_analysis, go_paths=False, \
		mp_workers=mp_workers, setup_variables=setup_variables.get('go_paths'), \
		**pathway_gene_files)

	return setup_variables

def intervene_analysis(intervene_dir, save_dirs, list_settings, make_non=False, \
	num_combos=40):
	print('Intervene Analysis')
	for_yes_upset = os.path.join(intervene_dir,'for_upsetr')
	yes_upset_dir = os.path.join(intervene_dir,'upsetr')
	os.makedirs(yes_upset_dir, exist_ok=True)
	os.makedirs(for_yes_upset, exist_ok=True)
	for filepath in glob.iglob(save_dirs.get('common_save')+"/*"+"_common.bed"):
		if not filepath.split('/')[-1].split('_')[0] == 'all':
			shutil.copy2(filepath, os.path.join(for_yes_upset, \
				filepath.split('/')[-1].split('_')[0]+'.bed'))
	if not os.path.isfile(os.path.join(yes_upset_dir, 'Intervene_upset.pdf')):
		subprocess.run(["intervene upset -i "+for_yes_upset+\
			"/*.bed --ninter "+str(num_combos)+\
			" --overlap-thresh=5 --save-overlap --output "+yes_upset_dir], shell=True)
	if not os.path.isfile(os.path.join(yes_upset_dir, 'Intervene_pairwise_frac.pdf')):
		subprocess.run(["intervene pairwise -i "+for_yes_upset+\
			"/*.bed  --output "+yes_upset_dir], shell=True)
	shutil.rmtree(for_yes_upset)

	if make_non == True:
		for_non_upset = os.path.join(intervene_dir,'for_non_upsetr')
		non_upset_dir = os.path.join(intervene_dir,'non_upsetr')
		os.makedirs(for_non_upset, exist_ok=True)
		os.makedirs(non_upset_dir, exist_ok=True)
		print('check for dir')
		for filepath in glob.iglob(common_save+"/*_non_common.bed"):
			if not filepath.split('/')[-1].split('_')[0] == 'all':
				shutil.copy2(filepath, os.path.join(for_non_upset, \
					filepath.split('/')[-1].split('_')[0]+'.bed'))
		if not os.path.isfile(os.path.join(non_upset_dir, 'Intervene_upset.pdf')):
			subprocess.run(["intervene upset -i "+for_non_upset+\
				"/*.bed --ninter "+str(num_combos)+\
				" --overlap-thresh=5 --save-overlap --output "+non_upset_dir], shell=True)
		if not os.path.isfile(os.path.join(non_upset_dir, 'Intervene_pairwise_frac.pdf')):
			subprocess.run(["intervene pairwise -i "+for_non_upset+\
				"/*.bed  --output "+non_upset_dir], shell=True)
		shutil.rmtree(for_non_upset)
		
	if make_non == True:
		upset_dirs = (yes_upset_dir, non_upset_dir)
	else:
		upset_dirs = (yes_upset_dir)

	return upset_dirs

def load_raw_data(test_data_dict, control_data_dict, test_data_dir, \
	control_data_dir, test_format, control_format, current_mark, loaded_dir, \
	data_info):
	sample_groups = set()
	test_data_info = data_info.get('test')
	control_data_info = data_info.get('control')		

	remove_from_control_dict = []
	for group_name in control_data_dict.keys():
		if group_name not in test_data_dict:
			remove_from_control_dict.append(group_name)
	for group_name in remove_from_control_dict:
		print('Removing control group because no matching test group found: {}'\
			.format(group_name))
		del control_data_dict[group_name]
	for group_name in test_data_dict.keys():
		relative_save_dir_test = os.path.join('test', group_name)
		save_dir_test = os.path.join(loaded_dir, 'test', group_name)
		os.makedirs(save_dir_test, exist_ok=True)
		for sample_loop, sample_id in enumerate(test_data_dict.get(group_name)):
			file_name = '{}{}'.format(test_data_dir, test_format.format(\
				group_name=group_name, sample_id=sample_id, mark=current_mark))
			relative_save_spot = os.path.join(relative_save_dir_test, \
				'{sample_id}.bed'.format(sample_id=sample_id))
			test_data_dict[group_name][sample_loop] = [sample_id, relative_save_spot]
			save_spot = os.path.join(save_dir_test, '{sample_id}.bed'\
				.format(sample_id=sample_id))
			if not os.path.isfile(save_spot):
				keep_lines = False
				with open(file_name, 'r') as readfile:
					with open(save_spot, 'w') as writefile:
						for line_loop, line in enumerate(readfile):
							line = line.strip('\n').split('\t')
							if 'chr' in line[0] and line[1].isnumeric():
								keep_lines = True
							if keep_lines is True:
								print('\t'.join(\
									[line[test_data_info.get('chr_index')], \
									line[test_data_info.get('start_index')], 
									line[test_data_info.get('end_index')], \
									line[test_data_info.get('density_index')]]), \
									file=writefile)

		if control_data_dict.get(group_name) is not None:
			relative_save_dir_control = os.path.join('control', group_name)
			save_dir_control = os.path.join(loaded_dir, 'control', group_name)
			os.makedirs(save_dir_control, exist_ok=True)
			for sample_loop, sample_id in enumerate(control_data_dict.get(group_name)):
				file_name = '{}{}'.format(control_data_dir, \
					control_format.format(group_name=group_name, \
					sample_id=sample_id, mark=current_mark))
				relative_save_spot = os.path.join(relative_save_dir_control, \
					'{sample_id}.bed'.format(sample_id=sample_id))
				control_data_dict[group_name][sample_loop] = \
					[sample_id, relative_save_spot]
				save_spot = os.path.join(save_dir_control, \
					'{sample_id}.bed'.format(sample_id=sample_id))
				if not os.path.isfile(save_spot):
					keep_lines = False
					with open(file_name, 'r') as readfile:
						with open(save_spot, 'w') as writefile:
							for line_loop, line in enumerate(readfile):
								line = line.strip('\n').split('\t')
								if 'chr' in line[0] and line[1].isnumeric():
									keep_lines = True
								if keep_lines is True:
									print('\t'.join(\
										[line[control_data_info.get('chr_index')], \
										line[control_data_info.get('start_index')], \
										line[control_data_info.get('end_index')], \
										line[control_data_info.get('density_index')]]), \
										file=writefile)































































# old functions
	# def core_pipeline(settings):

		# def check_for_normal(control_dict, sample_groups):
		# 	control_samples = False
		# 	for group_key in control_dict:
		# 		if control_dict.get(group_key) is not None:
		# 			control_samples = True

		# 	return control_samples

		# if settings.get('print_timing') is True: 
		# 	print('\tstarting core_pipeline')

		# sample_groups = sorted(list(settings.get('data_loading_info')\
		# 	.get('test_dict').keys()))

		# control_samples = check_for_normal(settings\
		# 	.get('data_loading_info').get('control_dict'), sample_groups)


		# if settings.get('print_timing') is True: 
		# 	start_load_raw_data = \
		# time.time()

		# # i want to load the raw data files into the analysis directory in a structure 
		# 	# that can be easily used during analysis
		# load_raw_data(
		# 	test_data_dict = settings.get('data_loading_info')\
		# 		.get('test_dict'), 
		# 	control_data_dict = settings.get('data_loading_info')\
		# 		.get('control_dict'), 
		# 	test_data_dir = settings.get('data_loading_info')\
		# 		.get('test_data_dir'), 
		# 	control_data_dir = settings.get('data_loading_info')\
		# 		.get('control_data_dir'), 
		# 	test_format = settings.get('data_loading_info')\
		# 		.get('test_load_format'), 
		# 	control_format = settings.get('data_loading_info')\
		# 		.get('control_load_format'),
		# 	current_mark = settings.get('data_loading_info')\
		# 		.get('which_mark'), 
		# 	loaded_dir = settings.get('save_dirs').get('loaded_data_dir')\
		# 		.get('path'), 
		# 	data_info = settings.get('data_info'))

		# if settings.get('print_timing') is True: 
		# 	print('\t\tload_raw_data time: {} s'\
		# 		.format(round(time.time()-start_load_raw_data,2)))

		# merge_sizes = \
		# 	[0,1000,2000,3000,4000,5000,6000,7000,8000,9000,10000]
		# if settings.get('which_analysis').get('merge_size_scatter') is True:

		# 	# factory function that will produce a file handler function -> can call 
		# 		# the function while supplying which step it's being called for + 
		# 		# what directory to load from -> get a list of files to load 
		# 		# (can also supply save dir to get a list of save paths to save to)
		# 	# this is not fully setup yet, but i'd like to use it to simplify file 
		# 		# loading logic in other steps
		# 	file_handler = hfnxns.make_file_handler(
		# 		file_settings_dict = settings.get(\
		# 			'how_to_handle_file_groups')\
		# 			.get('merge_size_comparison_scatter'), 
		# 		group_list = settings.get('data_color_info')\
		# 		.get('data_group_list'), 
		# 	)

		# 	if settings.get('print_timing') is True: 
		# 		start_merge_size_comparison_scatter = time.time()
		# 	merge_size_comparison_scatter(
		# 		loaded_data_dir =	settings.get('save_dirs')\
		# 			.get('loaded_data_dir').get('path'), 
		# 		plot_dir =			settings.get('save_dirs')\
		# 			.get('top_plot_dir').get('path'), 
		# 		fig_data_save = 	settings.get('save_dirs')\
		# 			.get('data_for_figures').get('path'), 
		# 		color_dict =		settings.get('data_color_info')\
		# 			.get('color_dict'), 
		# 		merge_sizes =		merge_sizes, 
		# 		sample_groups = 	merge_scatter_groups,
		# 		which_mark =		settings.get('data_loading_info')\
		# 			.get('which_mark'), 
		# 		verbose = 			settings.get('verbosity_dict')\
		# 			.get('merge_size_comparison_scatter'), 
		# 		cutoff =			4000
		# 	)
		# 	if settings.get('print_timing') is True: 
		# 		print('\t\tmerge_size_comparison_scatter time: {} s'\
		# 			.format(round(time.time()- \
		# 			start_merge_size_comparison_scatter,2)))

		# if settings.get('analysis_settings').get('core_analysis')\
		# 	.get('general_settings').get('merge_len') is not None:
		# 	if settings.get('print_timing') is True: 
		# 		start_make_shape_merged_bed = time.time()
		# 	hfnxns.make_shape_merged_bed(
		# 		top_merged_dir = 			settings.get('save_dirs')\
		# 			.get('merged_dir').get('path'), 
		# 		loaded_data_dir = 			settings.get('save_dirs')\
		# 			.get('loaded_data_dir').get('path'), 
		# 		sample_groups = 			sample_groups, 
		# 		merge_len = 				settings\
		# 										.get('analysis_settings')\
		# 										.get('core_analysis')\
		# 										.get('general_settings')\
		# 										.get('merge_len'), 
		# 		data_info = 				settings\
		# 										.get('data_info')
		# 	)
		# 	if settings.get('print_timing') is True: 
		# 		print('\t\t\tmake_shape_merged_bed time: {} s'\
		# 			.format(round(time.time()- \
		# 			start_make_shape_merged_bed,2)))

		# else:
		# 	if settings.get('print_timing') is True: 
		# 		start_make_shape_merged_bed = time.time()
		# 	hfnxns.make_shape_merged_bed(
		# 		top_merged_dir = 			settings.get('save_dirs')\
		# 										.get('merged_dir') \
		# 										.get('path'), 
		# 		loaded_data_dir = 			settings.get('save_dirs')\
		# 										.get('loaded_data_dir') \
		# 										.get('path'), 
		# 		sample_groups = 			sample_groups, 
		# 		merge_len = 				0, 
		# 		data_info = 				settings.get('data_info')
		# 	)
		# 	if settings.get('print_timing') is True: 
		# 		print('\t\t\tmake_shape_merged_bed time: {} s'\
		# 			.format(round(time.time()- \
		# 			start_make_shape_merged_bed,2)))

		# quantile_percents = [1,5,25,50,75,95,99,99.9]
		# if (settings.get('which_analysis').get('plot_len_quantiles') \
		# 	is True):
		# 	if settings.get('print_timing') is True: 
		# 		start_make_sample_quantile_plots = time.time()
		# 	hfnxns.make_sample_quantile_plots(
		# 		fig_data_save =		settings.get('save_dirs')\
		# 								.get('data_for_figures')\
		# 								.get('path'), 
		# 		sample_groups = 	sample_groups,
		# 		top_merged_dir = 	settings.get('save_dirs')\
		# 								.get('merged_dir')\
		# 								.get('path'), 
		# 		top_plot_dir =		settings.get('save_dirs')\
		# 								.get('top_plot_dir')\
		# 								.get('path'), 
		# 		useful_file_dir =	settings.get('top_dirs')\
		# 								.get('useful_file_dir'), 
		# 		percents = 			quantile_percents,
		# 		verbose = 			settings.get('verbosity_dict')\
		# 								.get('quantile_plot'),
		# 		len_or_dens =		'len',
		# 		merge_size =		settings.get('analysis_settings')\
		# 								.get('core_analysis')\
		# 								.get('general_settings')\
		# 								.get('merge_len'), 
		# 		cutoff =			4000, 
		# 	)
		# 	if settings.get('print_timing') is True: 
		# 		print('\t\t\tmake_sample_quantile_plots time: {} s'\
		# 			.format(round(time.time()- \
		# 			start_make_sample_quantile_plots,2)))

		# if (settings.get('which_analysis').get('plot_dens_quantiles') \
		# 	is True):
		# 	if settings.get('print_timing') is True: 
		# 		start_make_sample_quantile_plots = time.time()
		# 	hfnxns.make_sample_quantile_plots(
		# 		fig_data_save =		settings.get('save_dirs')\
		# 								.get('data_for_figures')\
		# 								.get('path'), 
		# 		sample_groups = 	sample_groups,
		# 		top_merged_dir = 	settings.get('save_dirs')\
		# 								.get('merged_dir')\
		# 								.get('path'), 
		# 		top_plot_dir =		settings.get('save_dirs')\
		# 								.get('top_plot_dir')\
		# 								.get('path'), 
		# 		useful_file_dir =	settings.get('top_dirs')\
		# 								.get('useful_file_dir'), 
		# 		percents = 			quantile_percents,
		# 		verbose = 			settings.get('verbosity_dict')\
		# 								.get('quantile_plot'),
		# 		len_or_dens =		'dens',
		# 		merge_size =		settings.get('analysis_settings')\
		# 								.get('core_analysis')\
		# 								.get('general_settings')\
		# 								.get('merge_len'), 
		# 		cutoff =			4000, 
		# 	)
		# 	if settings.get('print_timing') is True: 
		# 		print('\t\t\tmake_sample_quantile_plots time: {} s'\
		# 		.format(round(time.time()- \
		# 		start_make_sample_quantile_plots,2)))

		# for group in settings.get('analysis_settings')\
		# 	.get('core_analysis').get('analyses').keys():
		# 	if settings.get('print_timing') is True: 
		# 		start_specific_group_analysis = time.time()
		# 	specific_group_analysis(settings, group, sample_groups)
		# 	if settings.get('print_timing') is True: 
		# 		print('\t\tspecific_group_analysis {} time: {} s'\
		# 			.format(group, round(time.time()- \
		# 			start_specific_group_analysis,2)))

	# def comparison_pipeline(settings, compare_loop):
		# def setup_comp_pipe_paths(settings):
		# 	def add_unique_common_paths(comparison, top_compare_dir):
		# 		unique_common = os.path.join(top_compare_dir, \
		# 			'unique_common_beds')
		# 		comparison['top_compare_dir']['unique_common'] = {
		# 			'path': unique_common,
		# 			'normal_common_beds': {'path': \
		# 				os.path.join(unique_common, 'normal_common_beds')},
		# 			'unique_peak_beds': {'path': \
		# 				os.path.join(unique_common, 'unique_peak_beds')},
		# 		}

		# 		control_common_dir = os.path.join(\
		# 			unique_common, 'normal_common_beds')
		# 		comparison['top_compare_dir']['unique_common']\
		# 			['normal_common_beds'] = {
		# 			'path': control_common_dir,
		# 			'group1_normal_common': {'path': \
		# 				os.path.join(unique_common, 'group1_normal_common')},
		# 			'group2_normal_common': {'path': \
		# 				os.path.join(unique_common, 'group2_normal_common')},
		# 		}

		# 		unique_peak_dir = os.path.join(unique_common, \
		# 			'unique_peak_beds')
		# 		comparison['top_compare_dir']['unique_common']\
		# 			['unique_peak_beds'] = {
		# 			'path': unique_peak_dir,
		# 			'g1_control_minus_g2_control': {'path': \
		# 				os.path.join(unique_common, \
		# 					'g1_control_minus_g2_control')},
		# 			'g1_test_minus_g2_test': {'path': \
		# 				os.path.join(unique_common, \
		# 					'g1_test_minus_g2_test')},
		# 			'g2_control_minus_g1_control': {'path': \
		# 				os.path.join(unique_common, \
		# 					'g2_control_minus_g1_control')},
		# 			'g2_test_minus_g1_test': {'path': \
		# 				os.path.join(unique_common, \
		# 					'g2_test_minus_g1_test')},
		# 		}

		# 	def add_peak_size_paths(comparison, top_compare_dir):
		# 		peak_sizes = os.path.join(top_compare_dir, 'peak_size_dir')
		# 		comparison['top_compare_dir']['peak_sizes'] = {
		# 			'path': peak_sizes,
		# 			'unique_g1_control_vs_g2': {'path': \
		# 				os.path.join(peak_sizes, \
		# 					'unique_g1_control_vs_g2')},
		# 			'unique_g2_control_vs_g1': {'path': \
		# 				os.path.join(peak_sizes, \
		# 					'unique_g2_control_vs_g1')},
		# 			'unique_g1_test_vs_g2': {'path': \
		# 				os.path.join(peak_sizes, \
		# 					'unique_g1_test_vs_g2')},
		# 			'unique_g2_test_vs_g1': {'path': \
		# 				os.path.join(peak_sizes, \
		# 					'unique_g2_test_vs_g1')},
		# 		}

		# 		peak_size_dict = comparison.get('top_compare_dir')\
		# 			.get('peak_sizes')
		# 		for dir_key in [this_key for this_key in \
		# 			peak_size_dict if this_key is not 'path']:
		# 			dir_path = peak_size_dict.get(dir_key).get('path')

		# 	for compare_loop, comparison in \
		# 		enumerate(settings.get('compare_analyses')):
		# 		which_analyses_to_compare = \
		# 			comparison.get('which_analyses')
		# 		which_lists, compare_merge_dict = \
		# 			choose_comparison(which_analyses_to_compare, \
		# 				settings.get('list_settings'))

		# 		top_compare_dir = os.path.join(settings\
		# 			.get('main_pipeline_dirs').get('top_save_dir'), \
		# 			'_v_'.join(which_analyses_to_compare))

		# 		comparison.update({
		# 			'top_compare_dir': {'path': top_compare_dir}, 
		# 			'compare_analysis_indices': which_lists, 
		# 			'compare_merge_dict': compare_merge_dict
		# 		})
		# 		top_compare_path = comparison.get('top_compare_dir')\
		# 			.get('path')

		# 		top_merged_dir = os.path.join(\
		# 			top_compare_path, 'merged_beds')
		# 		comparison['top_compare_dir']['top_merged_dir'] = \
		# 			{'path': top_merged_dir}

		# 		add_unique_common_paths(comparison, top_compare_dir)

		# 		add_peak_size_paths(comparison, top_compare_dir)


		# 	comparison_pipeline(settings, compare_loop)
		# 	if settings.get('print_timing') is True: 
		# 		print('\tComparison pipeline {} time: {} s'\
		# 			.format(compare_loop, \
		# 				round(time.time()-start_comparison_pipeline,2)))
		# 	return
		# if settings.get('print_timing') is True: 
		# 	start_comparison_pipeline = time.time()

		# if settings.get('print_timing') is True: 
		# 	print('\tComparison pipeline {} time: {} s'\
		# 		.format(compare_loop, \
		# 			round(time.time()-start_comparison_pipeline,2)))


		# if settings.get('print_timing') is True: 
		# 	print('\tcomparison_pipeline')

		# print(settings.get('compare_analyses')[compare_loop]\
		# 	.get('top_compare_dir'))
		# return

# old code
	# hfnxns.quick_dumps(file_handler('merge_beds', \
	# 							settings.get('save_dirs')
	# 								.get('loaded_data_dir')\
	# 								.get('path'), settings.get('save_dirs')
	# 								.get('merged_dir')\
	# 								.get('path')))
	# if settings.get('analysis_settings').get('core_analysis').get('analyses')
		# .get(group).get('split_beds') is not None:

	# hfnxns.quick_dumps(file_handler('peak_count_bar_plot', \
	# 							settings.get('save_dirs')\
	# 								.get('core_pipeline_dirs')\
	# 								.get('analyses')\
	# 								.get(group)\
	# 								.get('tss_gene_dir')\
	# 								.get('path'),
	# 							settings.get('save_dirs')\
	# 								.get('core_pipeline_dirs')\
	# 								.get('analyses')\
	# 								.get(group)\
	# 								.get('gene_dir')\
	# 								.get('path'),),
	# 				'file_dict')
			# directories		=	sample_groups, 
			# top_merged_dir	=	settings.get('save_dirs').get('merged_dir')\
			# 						.get('path'), 
			# split_dir		=	settings.get('save_dirs')\
			# 						.get('per_analysis')\
			# 						.get(group)\
			# 						.get('split_dir')\
			# 						.get('path'), 
			# file_pattern	=	settings.get('analysis_settings')\
			# 						.get('core_analysis')\
			# 						.get('analyses')\
			# 						.get(group)\
			# 						.get('file_pattern'), 
			# directories		=	sample_groups, 
			# plot_dir		=	settings.get('save_dirs')\
			# 						.get('per_analysis')\
			# 						.get(group)\
			# 						.get('plot_dir')\
			# 						.get('path'), 
			# common_save		=	settings.get('save_dirs')\
			# 						.get('per_analysis')\
			# 						.get(group)\
			# 						.get('common_save')\
			# 						.get('path'), 
			# tss_gene_dir	=	settings.get('save_dirs')\
			# 						.get('per_analysis')\
			# 						.get(group)\
			# 						.get('tss_gene_dir')\
			# 						.get('path'), 
			# title_pattern	=	settings.get('analysis_settings')\
			# 						.get('core_analysis')\
			# 						.get('analyses')\
			# 						.get(group)\
			# 						.get('title_pattern'), 
			# directories		=	sample_groups,

	# old code
		# # 'specific_settings_list': ['analysis_name', 'file_pattern', 'title_pattern', 'len_or_dens', 'split_type', \
		#         'greater_or_less', 'cutoff', 'make_non'],
		# 'no_default_list': ['analysis_name', 'file_pattern', 'title_pattern', 'len_or_dens', 'greater_or_less', 'cutoff']
		# sample_groups_control = sample_groups.copy()
		# if control_samples is True:
		# 	sample_groups_control.append('control')

		# return sample_groups_control
		# merge_sizes = [0,1000,2000,3000,4000,5000,6000,7000,8000,9000,10000]
			# file_settings_dict = settings.get('how_to_handle_file_groups').get('merge_size_comparison_scatter'), 
			# load_dir = , 
			# save_dir = , 
			# group_list = settings.get('data_color_info').get('data_group_list'), 
		# print('\ncontrol')
		# a = file_handler('merge_size_comparison_scatter', settings.get('save_dirs').get('loaded_data_dir').get('path'))
		# print('\ntest')
		# print(file_handler('merge_size_comparison_scatter', settings.get('save_dirs').get('loaded_data_dir').get('path')).get('test'))
		# # input_common_format=False, output_common_format=False, all_common=False, \
		# # 		fuse_test=False, fuse_control=False, drop_test=False, drop_control=False
		# file_dict = file_handler(
		#     'merge_beds', 
		#     settings.get('save_dirs').get('merged_dir').get('path'), 
		#     settings.get('save_dirs').get('core_pipeline_dirs').get('analyses')\
		#         .get(group).get('split_dir').get('path'),
		#     input_common_format     =           False, 
		#     output_common_format    =           False, 
		#     all_common              =           True, 
		#     fuse_test               =           False, 
		#     fuse_control            =           False, 
		#     drop_test               =           False, 
		#     drop_control            =           False
		#     )


		# print(list(file_dict.keys()))

		# print([list(file_dict.get(thing).keys()) for thing in file_dict.keys()])

		# first_key = list(file_dict.keys())[0]
		# # first_key = 'all'
		# second_key = list(file_dict.get(first_key).keys())[0]
		# print('\n', file_dict.get(first_key).get(second_key)[:5])

		# first_key = list(file_dict.keys())[1]
		# second_key = list(file_dict.get(first_key).keys())[0]
		# print('\n', file_dict.get(first_key).get(second_key)[:5])

		# first_key = list(file_dict.keys())[2]
		# second_key = list(file_dict.get(first_key).keys())[0]
		# print('\n', file_dict.get(first_key).get(second_key)[:5])
		# group = list(settings.get('analysis_settings').get('core_analysis').get('analyses').keys())[0]
			# if settings.get('how_to_handle_controls').get('merge_size_comparison_scatter') is True:
			# 	merge_scatter_groups = sample_groups
			# else:
			# 	merge_scatter_groups = sample_groups
				# loaded_data_dir =	settings.get('save_dirs').get('loaded_data_dir').get('path'), 
				# sample_groups = 	sample_groups,
				# sample_groups = 	merge_scatter_groups,
				# which_mark =		settings.get('data_loading_info').get('which_mark'), 
			#top_merged_dir
			# print(settings.get('analysis_settings').get('core_analysis').get('analyses').get(group).get('analysis_name'))
			# print(settings.get('save_dirs').keys())
			# print(settings.get('save_dirs').values())
			# print(settings.get('save_dirs').get(settings.get('analysis_settings').get('core_analysis').\
				#get('analyses').get(group).get('analysis_name')))
		# if group_loop == 0:
				# sample_groups = 	sample_groups,
				# top_merged_dir = 	settings.get('save_dirs').get('merged_dir').get('path'), 
				# useful_file_dir =	settings.get('top_dirs').get('useful_file_dir'), 
				# plot_dir =			settings.get('save_dirs').get('core_pipeline_dirs').\
				#                         get('top_plot_dir').get('path'), 
				# fig_data_save = 	settings.get('save_dirs').get('data_for_figures').get('path'), 
				# color_dict =		settings.get('data_color_info').get('color_dict'), 
				# merge_sizes =		[0,1000,2000,3000,4000,5000,6000,7000,8000,9000,10000], 
				# save_names =        {'figdata_json': 'merge_scatter_json', \
				#                         'all_plot': 'merge_size_scatter_line_all.png', \
				#                         'subplots': 'merge_size_scatter_line_subplots.png'},
				# file_dict =         file_handler('merge_size_comparison_scatter', \
				#                         settings.get('save_dirs').get('loaded_data_dir')\
				#                         .get('path')).get('test'),
				# verbose = 			settings.get('verbosity_dict').get('merge_size_comparison_scatter'), 
				# cutoff =			4000
		# for group in settings.get('analysis_settings').get('core_analysis').get('analyses').keys():
		# 	if settings.get('print_timing') is True: start_specific_group_analysis = time.time()
		# 	specific_group_analysis(settings, group, sample_groups)
		# 	if settings.get('print_timing') is True: print('\t\tspecific_group_analysis {} time: {} s'\
		# 		.format(group, round(time.time()-start_specific_group_analysis,2)))
		# print(json.dumps(settings.get('save_dirs'), indent=4))
		# return
			# pipeline_file_split(settings, group, sample_groups)
					# split_dir 		=	os.path.join(settings.get('save_dirs').get('per_analysis').get(
					# 						settings.get('analysis_settings').get('core_analysis').get('analyses').get(group)\
					# 						.get('analysis_name')).get('split_dir'), file_group),
					# merged_dir 		=	os.path.join(settings.get('save_dirs').get('per_analysis').get(
					# 						settings.get('analysis_settings').get('core_analysis').get('analyses').get(group)\
					# 						.get('analysis_name')).get('top_merged_dir'), file_group),
					# file_pattern 	=	settings.get('analysis_settings').get('core_analysis').get('analyses').get(group)\
					# 						.get('file_pattern'), 
				# merged_dir		=		settings.get('save_dirs').get('per_analysis')\
				#                             .get(settings.get('analysis_settings').get('core_analysis').get('analyses').get(group)\
				#                             .get('analysis_name')).get('top_merged_dir').get('path'), 
				# split_dir		=		settings.get('save_dirs').get('per_analysis')\
				#                             .get(settings.get('analysis_settings').get('core_analysis').get('analyses').get(group)\
				#                             .get('analysis_name')).get('split_dir').get('path'), 
				# directories 	=		sample_groups
		# if include_e == True:
		# 	if not 'E' in directories: directories.append('E')
					# pipeline_dirs, settings.get('analysis_settings').get('core_analysis').get('analyses').get(group).get('analysis_name'), save_dirs[group_loop].get('split_dir'), save_dirs[group_loop].get('plot_dir'), directories)
			# old code
				# group_pattern 		    =	settings.get('analysis_settings').get('core_analysis').get('analyses').get(group).get('file_pattern'), 
				# directories 		=	sample_groups, 
				# common_save			=	settings.get('save_dirs').get('per_analysis')\
				#                                 .get(settings.get('analysis_settings').get('core_analysis').get('analyses').get(group)\
				#                                 .get('analysis_name')).get('common_save').get('path'), 
				# top_merged_dir		=	settings.get('save_dirs').get('per_analysis')\
				#                                 .get(settings.get('analysis_settings').get('core_analysis').get('analyses').get(group)\
				#                                 .get('analysis_name')).get('top_merged_dir').get('path'), 
				# split_dir			=	settings.get('save_dirs').get('per_analysis')\
				#                                 .get(settings.get('analysis_settings').get('core_analysis').get('analyses').get(group)\
				#                                 .get('analysis_name')).get('split_dir').get('path'),
									# settings.get('save_dirs').get('core_pipeline_dirs').get('analyses')\
									# 	.get(group).get('common_save').get('path'),
									# output_common_format = True,
			# old code
				# directories 	=	sample_groups, 
				# common_save		=	settings.get('save_dirs').get('per_analysis')\
				#                             .get(group).get('common_save').get('path'), 
				# split_dir		=	settings.get('save_dirs').get('per_analysis')\
				#                             .get(group).get('split_dir').get('path'),
				# group_pattern 	=	settings.get('analysis_settings').get('core_analysis').get('analyses').get(group).get('file_pattern'), 
			#old code
				# common_save				=	settings.get('save_dirs').get('per_analysis')\
				# 									.get(group).get('common_save').get('path'), 
				# top_merged_dir			=	settings.get('save_dirs').get('per_analysis')\
				# 									.get(group).get('top_merged_dir').get('path'), 
				# split_dir				=	settings.get('save_dirs').get('per_analysis')\
				# 									.get(group).get('split_dir').get('path'),
				# tss_gene_dir			=	settings.get('save_dirs').get('per_analysis')\
				# 									.get(group).get('tss_gene_dir').get('path'), 
				# gene_dir				=	settings.get('save_dirs').get('per_analysis')\
				# 									.get(group).get('gene_dir').get('path'),
				# sample_common_perc 		=	settings.get('analysis_settings').get('core_analysis').get('analyses').get(group).get('sample_common_perc'), 
				# split_type 				=	settings.get('analysis_settings').get('core_analysis').get('analyses').get(group).get('split_type'), 
			# old code
				# common_save			=	settings.get('save_dirs').get('per_analysis')\
				# 								.get(group).get('common_save').get('path'),
				# gene_dir			=	settings.get('save_dirs').get('per_analysis')\
				# 								.get(group).get('gene_dir').get('path'), 
		# def bar_plot(directories, save_dirs, plot_dir, top_merged_dir, split_dir, list_settings, file_pattern, title_pattern, file_dir):
				# save_dirs=save_dirs[group_loop], 
				# list_settings=settings.get('analysis_settings').get('core_analysis').get('analyses').get(group), 
				# file_dir = top_dirs.get('useful_file_dir')
			# def bar_plot_common(plot_dir, common_save, title_pattern, analysis_name, directories, pipeline_dirs, common_counts, log10=False):
				# save_dirs=save_dirs[group_loop], 
				# list_settings=settings.get('analysis_settings').get('core_analysis').get('analyses').get(group), 
				# directories = directories, 
				# pipeline_dirs=pipeline_dirs, 
				# common_counts = common_counts, 
				# save_dirs=save_dirs[group_loop], 
				# list_settings=settings.get('analysis_settings').get('core_analysis').get('analyses').get(group), 
				# directories = directories, 
				# pipeline_dirs=pipeline_dirs, 
				# common_counts = common_counts, 
			# def bar_plot_intersect_percent(plot_dir, common_save, tss_gene_dir, title_pattern, directories):

				# save_dirs=save_dirs[group_loop], 
				# list_settings=settings.get('analysis_settings').get('core_analysis').get('analyses').get(group), 
				# directories = directories, 
				# pipeline_dirs=pipeline_dirs, 
				# logger=logger


		# 'merge_size_scatter'
		# 'merge_beds'
		# 'plot_len_quantiles'
		# 'plot_dens_quantiles'
		# 'split_beds'
		# 'split_plot'
		# 'common_table_plot'
		# 'common_table_subplot'
		# 'sample_common_perc'
		# 'type_common_perc'
		# 'match_tss_gene_names_input'
		# 'match_tss_gene_names_output'
		# 'path_analysis'




# newer old code

	# from colorama import init 
	# init()
	# from importlib import reload
	# reload(hfnxns)

	"""
	save dict:
	{
	    "per_analysis": {
	        "4kb_25_50": {
	            "top_merged_dir": "/Users/jschulz1/Documents/\
	            	project_data/ccle_project/analyses/test_2019_11_07/\
	            	E/4kb_25_50/merged_beds",
	            "split_dir": "/Users/jschulz1/Documents/\
	            	project_data/ccle_project/analyses/test_2019_11_07/\
	            	E/4kb_25_50/split_beds",
	            "common_save": "/Users/jschulz1/Documents/\
	            	project_data/ccle_project/analyses/test_2019_11_07/\
	            	E/4kb_25_50/common_beds",
	            "tss_gene_dir": "/Users/jschulz1/Documents/\
	            	project_data/ccle_project/analyses/test_2019_11_07/\
	            	E/4kb_25_50/tss_intersect_gene_names",
	            "gene_dir": "/Users/jschulz1/Documents/\
	            	project_data/ccle_project/analyses/test_2019_11_07/\
	            	E/4kb_25_50/gene_lists",
	            "plot_dir": "/Users/jschulz1/Documents/\
	            	project_data/ccle_project/analyses/test_2019_11_07/\
	            	E/4kb_25_50/plots"
	        },
	        "1kb_sharp_25_50": {
	            "top_merged_dir": "/Users/jschulz1/Documents/\
	            	project_data/ccle_project/analyses/test_2019_11_07/\
	            	E/1kb_sharp_25_50/merged_beds",
	            "split_dir": "/Users/jschulz1/Documents/\
	            	project_data/ccle_project/analyses/test_2019_11_07/\
	            	E/1kb_sharp_25_50/split_beds",
	            "common_save": "/Users/jschulz1/Documents/\
	            	project_data/ccle_project/analyses/test_2019_11_07/\
	            	E/1kb_sharp_25_50/common_beds",
	            "tss_gene_dir": "/Users/jschulz1/Documents/\
	            	project_data/ccle_project/analyses/test_2019_11_07/\
	            	E/1kb_sharp_25_50/tss_intersect_gene_names",
	            "gene_dir": "/Users/jschulz1/Documents/\
	            	project_data/ccle_project/analyses/test_2019_11_07/\
	            	E/1kb_sharp_25_50/gene_lists",
	            "plot_dir": "/Users/jschulz1/Documents/\
	            	project_data/ccle_project/analyses/test_2019_11_07/\
	            	E/1kb_sharp_25_50/plots"
	        }
	    },
	    "general_settings": {
	        "top_plot_dir": "/Users/jschulz1/Documents/\
	        	project_data/ccle_project/analyses/test_2019_11_07/\
	        	E/general_plots",
	        "loaded_data_dir": "/Users/jschulz1/Documents/\
	        	project_data/ccle_project/analyses/test_2019_11_07/\
	        	E/loaded_data",
	        "data_for_figures": "/Users/jschulz1/Documents/\
	        	project_data/ccle_project/analyses/test_2019_11_07/\
	        	E/data_for_figures"
	    }
	}

	"""



				# old code
					# '\{{val1}\}_\{{which_group}\}'\
					# '\{{val1}\}_\{{which_group}\}_subtract_\{{val2}\}_\{{which_group}\}'\
					
					# print(path_string)
					# print(top_path)
					# 'normal_common_beds': {'path': \
					# 	os.path.join(unique_common, \
					# 	'normal_common_beds')},
					# 'unique_peak_beds': {'path': \
					# 	os.path.join(unique_common, \
					# 	'unique_peak_beds')},
					# {'path': os.path.join(unique_common, \
					# 	'{g1}_{control_name}_subtract_{g2}_{control_name}'\
					# 	.format(control_name=control_group_name, \
					# 	test_name=test_group_name, \
					# 	g1=analyses[0], g2=analyses[1]))},

					# 'g1_test_subtract_g2_test': \
					# 	{'path': os.path.join(unique_common, \
					# 		'{g1}_{test_name}_subtract_{g2}_{test_name}'\
					# 		.format(control_name=control_group_name, \
					# 		test_name=test_group_name, \
					# 		g1=analyses[0], g2=analyses[1]))},

					# 'g2_control_subtract_g1_control': \
					# 	{'path': os.path.join(unique_common, \
					# 		'{g2}_{control_name}_subtract_{g1}_{control_name}'\
					# 		.format(control_name=control_group_name, \
					# 		test_name=test_group_name, \
					# 		g1=analyses[0], g2=analyses[1]))},

					# 'g2_test_subtract_g1_test': \
					# 	{'path': os.path.join(unique_common, \
					# 		'{g2}_{test_name}_subtract_{g1}_{test_name}'\
					# 		.format(control_name=control_group_name, \
					# 		test_name=test_group_name, \
					# 		g1=analyses[0], g2=analyses[1]))},


				# save_dirs['length_change_pipeline_dirs']\
				# 	[group_names[reference_loop]]['length_change_analysis']\
				# 	['no_change_peaks_analysis'][specific_cutoff] = {
				# 		'path': os.path.join(no_change_path, specific_cutoff)
				# 	}
		# old code
			# length_dens_path = os.path.join(reference_path, \
			# 		'length_and_height_change_analysis')
				# 'length_and_height_change_analysis': {
				# 	'path': length_dens_path
				# },

			# make_dirs_each_core_analysis('length_change', \
			# 	length_change_analysis_dict)
							# print(save_dirs.get('length_change_pipeline_dirs')\
							# 	.get(group_names[reference_loop])
							# 	.get('length_change_analysis')
							# 	.get(len_key)
							# 	.get(dens_cutoff_key))

			# hfnxns.quick_dumps(save_dirs.get('length_change_pipeline_dirs'))
			# raise
			# length_and_height_change_dict = save_dirs.get(\
			# 	'length_change_pipeline_dirs')\
			# 	.get(group_names[reference_loop])\
			# 	.get('length_and_height_change_analysis')
			# make_dirs_each_core_analysis(\
			# 	'length_and_height_change', \
			# 	length_and_height_change_dict)

		# hfnxns.quick_dumps(save_dirs)



		# hfnxns.quick_dumps(settings.get('save_dirs'))
		# raise

			# if compare_analyses is not None:
		# settings.get('workers').close()
		# settings.get('workers').join()
		# settings['workers'] = mp.Pool(settings.get('cpu_cores'))
		# raise

			# if compare_analyses is not None:
		# print(colored('blocking_for_all_jobs', 'red'))
		# print(colored('done blocking_for_all_jobs', 'red'))

		# settings['workers'] = \
		# 	hfnxns.close_reopen_workers(settings.get('workers'), \
		# 		settings.get('cpu_cores'))

		# raise
		# 	# len_change_dir = os.path.join(pipeline_dirs\
		# 	# 	.get('top_save_dir'), 'len_change')
		# 	if settings.get('print_timing') is True: 
		# 		start_len_change_pipeline = time.time()
		# 	len_change_analysis(len_change_dir, color_dict, \
		# 		tumor_controls, pipeline_dirs, directories, \
		# 		data_info, which_analysis, pathway_gene_files, \
		# 		verbose, logger)
		# 	if settings.get('print_timing') is True: 
		# 		print('\tLen_change pipeline time: {}'\
		# 		.format(round(time.time()-start_len_change_pipeline,2)))
	# def check_for_normal(control_dict, sample_groups):
		# control_samples = False
		# for group_key in control_dict:
		# 	if control_dict.get(group_key) is not None:
		# 		control_samples = True
		# return control_samples

	# control_samples = check_for_normal(settings.get('data_loading_info')\
	# 	.get('control_dict'), sample_groups)


		# settings['workers'] = \
		# 	hfnxns.close_reopen_workers(settings.get('workers'), \
		# 		settings.get('cpu_cores'))

			# cpu_count 			=	settings.get('cpu_cores')
		# if returned_workers is not None:
		# 	setting['workers'] = returned_workers
	# print('gonna block?')
	# settings.get('workers').block_for_all_jobs()
	# raise

	# raise

	# return
		# def setup_comp_pipe_paths(settings, comparison, loop_comparison_saves): #, \
			# # which_lists, compare_merge_dict):
			# def add_unique_common_paths(loop_comparison_saves, top_compare_dir):
				# unique_common = os.path.join(top_compare_dir, \
				# 	'unique_common_beds')
				# loop_comparison_saves['top_compare_dir']['unique_common'] = {
				# 	'path': unique_common,
				# 	'normal_common_beds': {'path': \
				# 		os.path.join(unique_common, 'normal_common_beds')},
				# 	'unique_peak_beds': {'path': \
				# 		os.path.join(unique_common, 'unique_peak_beds')},
				# }

				# control_common_dir = os.path.join(\
				# 	unique_common, 'normal_common_beds')
				# loop_comparison_saves['top_compare_dir']['unique_common']\
				# 	['normal_common_beds'] = {
				# 	'path': control_common_dir,
				# 	'group1_normal_common': {'path': \
				# 		os.path.join(unique_common, 'group1_normal_common')},
				# 	'group2_normal_common': {'path': \
				# 		os.path.join(unique_common, 'group2_normal_common')},
				# }

				# unique_peak_dir = os.path.join(unique_common, \
				# 	'unique_peak_beds')
				# loop_comparison_saves['top_compare_dir']['unique_common']\
				# 	['unique_peak_beds'] = {
				# 	'path': unique_peak_dir,
				# 	'g1_control_minus_g2_control': {'path': \
				# 		os.path.join(unique_common, \
				# 			'g1_control_minus_g2_control')},
				# 	'g1_test_minus_g2_test': {'path': \
				# 		os.path.join(unique_common, \
				# 			'g1_test_minus_g2_test')},
				# 	'g2_control_minus_g1_control': {'path': \
				# 		os.path.join(unique_common, \
				# 			'g2_control_minus_g1_control')},
				# 	'g2_test_minus_g1_test': {'path': \
				# 		os.path.join(unique_common, \
				# 			'g2_test_minus_g1_test')},
				# }

			# def add_peak_size_paths(loop_comparison_saves, top_compare_dir):
				# peak_sizes = os.path.join(top_compare_dir, 'peak_size_dir')
				# loop_comparison_saves['top_compare_dir']['peak_sizes'] = {
				# 	'path': peak_sizes,
				# 	'unique_g1_control_vs_g2': {'path': \
				# 		os.path.join(peak_sizes, \
				# 			'unique_g1_control_vs_g2')},
				# 	'unique_g2_control_vs_g1': {'path': \
				# 		os.path.join(peak_sizes, \
				# 			'unique_g2_control_vs_g1')},
				# 	'unique_g1_test_vs_g2': {'path': \
				# 		os.path.join(peak_sizes, \
				# 			'unique_g1_test_vs_g2')},
				# 	'unique_g2_test_vs_g1': {'path': \
				# 		os.path.join(peak_sizes, \
				# 			'unique_g2_test_vs_g1')},
				# }

			# compare_saves = {}

			# peak_size_dict = settings.get('save_dirs')\
			# 	.get('comparison_pipeline_dirs').get('path')
			# .get('top_compare_dir')\
			# peak_size_dict = comparison.get('top_compare_dir')\
			# 	.get('peak_sizes')
			# for dir_key in [this_key for this_key in \
			# 	peak_size_dict if this_key is not 'path']:
			# 	dir_path = peak_size_dict.get(dir_key).get('path')


			# top_compare_dir = os.path.join(settings\
			# 	.get('main_pipeline_dirs').get('top_save_dir'), \
			# 	'_v_'.join(comparison))

			# loop_comparison_saves.update({
			# 	'top_compare_dir': {'path': top_compare_dir}, 
			# 	# 'compare_analysis_indices': which_lists, 
			# 	# 'compare_merge_dict': compare_merge_dict
			# })
			# top_compare_path = loop_comparison_saves.get('top_compare_dir')\
			# 	.get('path')

			# top_merged_dir = os.path.join(\
			# 	top_compare_path, 'merged_beds')
			# loop_comparison_saves['top_compare_dir']['top_merged_dir'] = \
			# 	{'path': top_merged_dir}

			# add_unique_common_paths(loop_comparison_saves, top_compare_dir)

			# add_peak_size_paths(loop_comparison_saves, top_compare_dir)

		# loop_comparison_saves = {}
		# which_analyses_to_compare = \
		# 	comparison.get('which_analyses')
		# which_lists, compare_merge_dict = \
		# 	choose_comparison(comparison, \
		# 		settings.get('list_settings'))

		# setup_comp_pipe_paths(settings, comparison, loop_comparison_saves) #, \
		# 	# which_lists, compare_merge_dict)
		
		# hfnxns.quick_dumps(loop_comparison_saves, 'comparison')


		# raise
		# if settings.get('print_timing') is True: 
		# 	print('\tComparison pipeline {} time: {} s'\
		# 		.format(compare_loop, \
		# 			round(time.time()-start_comparison_pipeline,2)))


			# will_make_common_barplot 	=	settings.get('which_analysis')\
			# 									.get('peak_count_bar_plot'),


		# settings['workers'] = \
		# 	hfnxns.close_reopen_workers(settings.get('workers'), \
		# 		settings.get('cpu_cores'))
		
					# cpu_count 			=	settings.get('cpu_cores')

				# if returned_workers is not None:
				# 	setting['workers'] = returned_workers
			# settings.get('workers').block_for_all_jobs()
			# raise


			# settings.get('workers').block_for_all_jobs()
			# raise
			# settings['workers'] = \
			# 	hfnxns.close_reopen_workers(settings.get('workers'), \
			# 		settings.get('cpu_cores'))
			# data_for_figures_dir = os.path.join(settings.get('save_dirs')\
			# 				.get('data_for_figures').get('path'),
			# 				'cancer_plots', 
			# 				'len_change_{}'.format(dens_cutoff_setting))
	# start_worker_closing_time = time.time()
	# print('closing + restarting workers')
	# print(settings.get('workers')._inqueue.__dict__)
	# print(settings.get('workers')._outqueue.__dict__)
	# print(settings.get('workers')._taskqueue.__dict__)
	# settings['workers'] = \
	# 	hfnxns.close_reopen_workers(settings.get('workers'), \
	# 		settings.get('cpu_cores'))
	# print('\t\t\tworker closing time: {} s'\
	# 	.format(round(time.time()-start_worker_closing_time,2)))

	# print(colored('blocking_for_all_jobs', 'red'))
	# settings.get('workers').block_for_all_jobs()
	# print(colored('done blocking_for_all_jobs', 'red'))
	
	# old code
		# if settings.get('analysis_settings').get('core_analysis')\
		# 	.get('general_settings').get(step_name) is not None:
		# [4kb_broad, 1kb_sharp]
				# plot_name = '{spefic_analysis}_{group_name}_vs_{other_group}_all_size_change_kde'
				# normal_broad_list = (True, False, True, False)
				# print(compare_key, compare_dict_key, specific_analysis,
				# 	group, other_group)
				# for analysis_loop, specific_analysis in enumerate(comparison):
					# for group in ['test', 'control']:
					# for group_loop in range(len(group_dirs)):
					# 	if (group_loop == 0) or (group_loop == 1):
					# 		title_pattern = list_settings[which_lists[0]].get('title_pattern')
					# 	else:
					# 		title_pattern = list_settings[which_lists[1]].get('title_pattern')
					# 	plot_kde_size_change(title_pattern, color_dict=color_dict, \
					# 		nl_broad=normal_broad_list[group_loop])
					# x_min_max			=	(-3,3), 
					# x_min_max			=	(-3,3), 

		# print(input1)
		# print(input2)
		# hfnxns.quick_dumps(settings.get('save_dirs')\
		# 					.get('core_pipeline_dirs')\
		# 					.get('analyses')\
		# 					.get(specific_analysis)\
		# 					.get('common_save'))
		# hfnxns.quick_dumps(settings.get('save_dirs')\
		# 					.get('comparison_pipeline_dirs')\
		# 					.get(compare_key)\
		# 					.get('unique_common')\
		# 					.get('normal_common_beds')\
		# 					.get('group{}_normal_common'.format(ind1)))
		# print('dicts for: {}, {}'.format(specific_analysis, group))
		# hfnxns.quick_dumps(comparison1_dict, 'comparison1_dict')
		# hfnxns.quick_dumps(comparison2_dict, 'comparison2_dict')
		# cancer_unique_dirs = intersect_for_peak_len(\
			# save_dirs_compare.get('ref_cancer_unique'), \
			# save_dirs_compare.get('compare_cancer_unique'), \
			# save_dirs[which_lists[0]].get('common_save'), 
			# save_dirs[which_lists[1]].get('common_save'), \
			# cancer_unique_types, normals=False)
		# normal_unique_dirs = intersect_for_peak_len(\
			# save_dirs_compare.get('ref_normal_unique'), \
			# save_dirs_compare.get('compare_normal_unique'), \
			# save_dirs_compare.get('ref_normal_common'), \
			# save_dirs_compare.get('compare_normal_common'), \
			# directories, \
			# normals=True)
		# mp_workers.close()
		# mp_workers.join()
		# data_dir			=	[save_dirs[which_lists[0]].get('gene_dir'), 
		# 							save_dirs[which_lists[1]].get('gene_dir')], 
		# data_dir			=	[save_dirs[which_lists[0]].get('gene_dir'), 
		# 							save_dirs[which_lists[1]].get('gene_dir')], 
			# cancer_list_plots(
				# output_dict		=	file_handler('match_tss_gene_names_output', 
				# 						settings.get('save_dirs')
				# 							.get('core_pipeline_dirs')
				# 							.get('analyses')
				# 							.get(group)
				# 							.get('tss_gene_dir')
				# 							.get('path'),
				# 						settings.get('save_dirs')
				# 							.get('core_pipeline_dirs')
				# 							.get('analyses')
				# 							.get(group)
				# 							.get('gene_dir')
				# 							.get('path'),
				# 				),
				# data_dir			=	[save_dirs[which_lists[0]].get('gene_dir'), 
				# 							save_dirs[which_lists[1]].get('gene_dir')], 
				# save_dir			=	save_dirs_compare.get('plot_dir'), 
				# len_or_len_change	=	'len', 
				# tuson_or_cosmic		=	'cosmic', 
				# data_for_figures	=	pipeline_dirs.get('data_for_figures'), 
				# cosmic_list			=	pipeline_dirs.get('cosmic_file'))

		# if settings.get('print_timing') is True: 
		# 	after_pway_analysis_time = time.time()
		# 	print('starting to close pathways pool')
		# if settings.get('print_timing') is True: 
		# 	print('\t\t\tpool close time: {} s'\
		# 	.format(round(time.time()-after_pway_analysis_time,2)))
				# [4kb_broad, 1kb_sharp]
					# if settings.get('print_timing') is True: 
					# 	after_pway_analysis_time = time.time()
						# print('\t\t\tpathway_analysis unfair time: {} s'\
						# .format(round(time.time()-after_pway_analysis_time,2)))
					# if settings.get('print_timing') is True: 
					# 	print('\t\t\tpool close time: {} s'\
					# 	.format(round(time.time()-after_pway_analysis_time,2)))
					# 	print('\t\t\tpathway_analysis time: {} s'\
					# 	.format(round(time.time()-start_pathway_analysis,2)))
		# settings.get('workers').close()
		# settings.get('workers').join()
		# settings['workers'] = mp.Pool(settings.get('cpu_cores'))
		# raise

		# pass



		# raise
	# settings['workers'] = \
	# 	hfnxns.close_reopen_workers(settings.get('workers'), \
	# 		settings.get('cpu_cores'))

		# settings['workers'] = \
		# 	hfnxns.close_reopen_workers(settings.get('workers'), \
		# 		settings.get('cpu_cores'))
		# old code
			# if group == 'test':
			# 	input1	= 	settings.get('save_dirs')\
			# 					.get('core_pipeline_dirs')\
			# 					.get('analyses')\
			# 					.get(specific_analysis)\
			# 					.get('common_save')\
			# 					.get('path')
			# 	input2	= 	settings.get('save_dirs')\
			# 					.get('core_pipeline_dirs')\
			# 					.get('analyses')\
			# 					.get(other_analysis)\
			# 					.get('common_save')\
			# 					.get('path')
			# 	kwargs = {'drop_control': True, 'drop_test': False}
			# else:
			# input1	= 	settings.get('save_dirs')\
			# 				.get('length_change_pipeline_dirs')\
			# 				.get('common_beds')\
			# 				.get('path')
			# input2	= 	settings.get('save_dirs')\
			# 				.get('length_change_pipeline_dirs')\
			# 				.get('common_beds')\
			# 				.get('path')
			# make unique common beds for each group for spec vs other analysis
			# run pipeline -> tss_gene + pathways + path dots
			# comparison1_dict	=	file_handler(
			# 							'len_change_unique_1', 
			# 							input1,
			# 							**kwargs1
			# 					)
			# comparison2_dict	=	file_handler('len_change_unique_2', \
			# 							input2,
			# 							settings.get('save_dirs')\
			# 								.get('length_change_pipeline_dirs')\
			# 								.get(group)\
			# 								.get('len_change_setup')\
			# 								.get('unique_common_beds')\
			# 								.get('path'),
			# 							**kwargs2
			# 					)

			# comparison2_dict	=	file_handler('len_change_unique_2', \
			# 							input2,
			# 							settings.get('save_dirs')\
			# 								.get('length_change_pipeline_dirs')\
			# 								.get(group)\
			# 								.get('len_change_setup')\
			# 								.get('other_0_common')\
			# 								.get('path'),
			# 							**kwargs2
			# 					)

			# hfnxns.comparison_unique_common(
			# 	comparison1_dict			=	comparison1_dict,
			# 	comparison2_dict			=	comparison2_dict,
			# 	)
		
			# wait, i need to run this 2x?
				# 1. run bedop_merge to get length of unique peaks in own group
				# 2. same but to get length of matching peaks in other group
				# this lets me then compare peak sizes b/w groups?
			# inputs: unique key unique beds,
				# opposing merged beds (cancer vs nl)
			# outputs: 
			# peak_subdir_str = 'unique_g{ind1}_{group}_vs_{other_group}'\
			# 	.format(ind1=ind1, group=group, other_group=other_group)
			# compare_dict_key = 'g{ind1}_{group}_vs_{other_group}_comparsion'\
			# 	.format(ind1=ind1, group=group, other_group=other_group)
			# if group == 'control':
			# 	kwargs1 = {'drop_control': False, 'drop_test': True}
			# 	kwargs2 = {'drop_control': True, 'drop_test': False}
			# else:
			# 	kwargs1 = {'drop_control': True, 'drop_test': False}
			# 	kwargs2 = {'drop_control': False, 'drop_test': True}
		# settings['workers'] = \
		# 	hfnxns.close_reopen_workers(settings.get('workers'), \
		# 		settings.get('cpu_cores'))
			# if settings.get('print_timing') is True: 
			# 	start_peak_len = time.time()
			# gonna setup no_change_dict -> 
			# if len_setting == 'lengthen':
			# 	no_change_dict 		=	file_handler(
			# 							'len_pipeline_no_change_len_filter', 
			# 							settings.get('save_dirs')
			# 								.get('length_change_pipeline_dirs')
			# 								.get(group)
			# 								.get('length_change_analysis')
			# 								.get('no_change_peaks_analysis')
			# 								.get(dens_cutoff_setting)
			# 								.get('common_save')
			# 								.get('path'),
			# 								**kwargs1
			# 					)
				# for this_compar in cutoff_settings.get('dens_cutoff_comparisons'):
					# 	# print(this_compar)
					# 	if dens_cutoff_setting in list(this_compar.values()):
					# 		break
					# print(this_compar, cutoff_settings.get('density_cutoffs'))
					# for this_len_setting in ['lengthen', 'shorten']:
					# cutoff_settings.get('dens_cutoff_comparisons')
					# print('\t', this_len_setting)
					# print('\t\t', this_compar.get(this_len_setting), cutoff_settings.get('density_cutoffs')\
					# 							.get(this_len_setting).get(this_compar.get(this_len_setting)).get('less_than'))

			# no_change_cutoff[dens_cutoff_setting] = {
			# 						'less_than': final_less_than,
			# 						'greater': final_greater_than
			# 					}
					# for this_compar in cutoff_settings.get('dens_cutoff_comparisons'):
						# 	# print(this_compar)
						# 	if dens_cutoff_setting in list(this_compar.values()):
						# 		break
						# print(this_compar, cutoff_settings.get('density_cutoffs'))
						# for this_len_setting in ['lengthen', 'shorten']:
						# cutoff_settings.get('dens_cutoff_comparisons')
						# print('\t', this_len_setting)
						# print('\t\t', this_compar.get(this_len_setting), cutoff_settings.get('density_cutoffs')\
						# 							.get(this_len_setting).get(this_compar.get(this_len_setting)).get('less_than'))
					
						# less_than_list = []
						# greater_than_list = []
						# less_than_cutoff = cutoff_settings.get('density_cutoffs')\
						# 						.get('lengthen')\
						# 						.get(dens_cutoff_setting)\
						# 						.get('less_than')
						# if less_than_cutoff is not None:
						# 	less_than_list.append(less_than_cutoff)
						# greater_than_cutoff = cutoff_settings.get('density_cutoffs')\
						# 						.get('lengthen')\
						# 						.get(dens_cutoff_setting)\
						# 						.get('greater')
						# if greater_than_cutoff is not None:
						# 	greater_than_list.append(greater_than_cutoff)

						# less_than_cutoff = cutoff_settings.get('density_cutoffs')\
						# 						.get('shorten')\
						# 						.get(cutoff_settings\
						# 							.get('dens_cutoff_comparisons')\
						# 							.get(dens_cutoff_setting))\
						# 						.get('less_than')
						# if less_than_cutoff is not None:
						# 	less_than_list.append(less_than_cutoff)
						# greater_than_cutoff = cutoff_settings.get('density_cutoffs')\
						# 						.get('shorten')\
						# 						.get(cutoff_settings\
						# 							.get('dens_cutoff_comparisons')\
						# 							.get(dens_cutoff_setting))\
						# 						.get('greater')
						# if greater_than_cutoff is not None:
						# 	greater_than_list.append(greater_than_cutoff)
						# if len(less_than_list) > 0:
						# 	final_less_than = max(less_than_list)
						# else:
						# 	final_less_than = None
						# if len(greater_than_list) > 0:
						# 	final_greater_than = max(greater_than_list)
						# else:
						# 	final_greater_than = None

						# no_change_cutoff = {
						# 						'less_than': final_less_than,
						# 						'greater': final_greater_than
						# 					}
					# print(dens_cutoff_setting)
					# print(no_change_cutoff)
					# raise
				# print(len_setting, dens_cutoff_setting)


					# len_cutoff 			=	cutoff_settings.get('length_cutoffs')
					# 							.get(len_setting),

												# group_path_name)
							# cpu_count 			=	settings.get('cpu_cores')

						# if returned_workers is not None:
						# 	setting['workers'] = returned_workers
						# if settings.get('print_timing') is True: 
						# 	print(colored('\t\t\tpathway_analysis time: {} s'\
						# 	.format(round(time.time()-start_pathway_analysis,2)), 'red'))
						# if settings.get('print_timing') is True: 
						# 	after_pway_analysis_time = time.time()
			# old code
				# for this_compar in cutoff_settings.get('dens_cutoff_comparisons'):
					# print(this_compar)
					# if dens_cutoff_setting in list(this_compar.values()):
					# 	break
				# less_than_list = []
				# greater_than_list = []
				# less_than_cutoff = cutoff_settings.get('density_cutoffs')\
				# 						.get('lengthen')\
				# 						.get(dens_cutoff_setting)\
				# 						.get('less_than')
				# if less_than_cutoff is not None:
				# 	less_than_list.append(less_than_cutoff)
				# greater_than_cutoff = cutoff_settings.get('density_cutoffs')\
				# 						.get('lengthen')\
				# 						.get(dens_cutoff_setting)\
				# 						.get('greater')
				# if greater_than_cutoff is not None:
				# 	greater_than_list.append(greater_than_cutoff)

				# less_than_cutoff = cutoff_settings.get('density_cutoffs')\
				# 						.get('shorten')\
				# 						.get(cutoff_settings\
				# 							.get('dens_cutoff_comparisons')\
				# 							.get(dens_cutoff_setting))\
				# 						.get('less_than')
				# if less_than_cutoff is not None:
				# 	less_than_list.append(less_than_cutoff)
				# greater_than_cutoff = cutoff_settings.get('density_cutoffs')\
				# 						.get('shorten')\
				# 						.get(cutoff_settings\
				# 							.get('dens_cutoff_comparisons')\
				# 							.get(dens_cutoff_setting))\
				# 						.get('greater')
				# if greater_than_cutoff is not None:
				# 	greater_than_list.append(greater_than_cutoff)
				# if len(less_than_list) > 0:
				# 	final_less_than = max(less_than_list)
				# else:
				# 	final_less_than = None
				# if len(greater_than_list) > 0:
				# 	final_greater_than = max(greater_than_list)
				# else:
				# 	final_greater_than = None

				# if settings.get('print_timing') is True: 
				# 	print(colored('\t\t\tno_change_setup time: {} s'\
				# 	.format(round(time.time()-start_kde_size_change,2)), 'red'))
				# no_change_cutoff = {
				# 						'less_than': final_less_than,
				# 						'greater': final_greater_than
				# 					}
						# no_change_dict 		=	file_handler(
										# 		'len_pipeline_no_change_len_filter', 
										# 		settings.get('save_dirs')
										# 			.get('length_change_pipeline_dirs')
										# 			.get(group)
										# 			.get('length_change_analysis')
										# 			.get('no_change_peaks_analysis')
										# 			.get(dens_cutoff_setting)
										# 			.get('common_save')
										# 			.get('path'),
										# 			**kwargs1
										# )

										# group_path_name)


					# cpu_count 			=	settings.get('cpu_cores')

				# if returned_workers is not None:
				# 	setting['workers'] = returned_workers
				# if settings.get('print_timing') is True: 
				# 	after_pway_analysis_time = time.time()

			# old code
				# settings['workers'] = \
				# 	hfnxns.close_reopen_workers(settings.get('workers'), \
				# 		settings.get('cpu_cores'))
				# print('did stuff')
				# settings.get('workers').close()
				# settings.get('workers').join()
				# settings['workers'] = mp.Pool(settings.get('cpu_cores'))
				# raise

				# for len_setting in cutoff_settings.get('density_cutoffs').keys():
				# 	if cutoff_settings.get('length_cutoffs').get(len_setting) is None:
				# 		raise ValueError('length_cutoffs is missing setting {} from\
				# 			\n\tdensity_cutoffs settings'.format(len_setting))
				# continue
				# for dens_cutoff_setting in cutoff_settings.get('density_cutoffs')\
				# 	.get('lengthen'):
				# 	for this_compar in cutoff_settings.get('dens_cutoff_comparisons'):
				# 		# print(this_compar)
				# 		if dens_cutoff_setting in list(this_compar.values()):
				# 			break
				# 	less_than_list = []
				# 	greater_than_list = []
				# 	for this_len_setting in cutoff_settings.get('density_cutoffs').keys():
				# 		less_than_cutoff = cutoff_settings.get('density_cutoffs')\
				# 								.get(this_len_setting)\
				# 								.get(this_compar.get(this_len_setting))\
				# 								.get('less_than')
				# 		if less_than_cutoff is not None:
				# 			less_than_list.append(less_than_cutoff)
				# 		greater_than_cutoff = cutoff_settings.get('density_cutoffs')\
				# 								.get(this_len_setting)\
				# 								.get(this_compar.get(this_len_setting))\
				# 								.get('greater')
				# 		if greater_than_cutoff is not None:
				# 			greater_than_list.append(greater_than_cutoff)

				# 	if len(less_than_list) > 0:
				# 		final_less_than = max(less_than_list)
				# 	else:
				# 		final_less_than = None
				# 	if len(greater_than_list) > 0:
				# 		final_greater_than = max(greater_than_list)
				# 	else:
				# 		final_greater_than = None
				# 	no_change_cutoff = {
				# 							'less_than': final_less_than,
				# 							'greater': final_greater_than
				# 						}

				# print(dens_cutoff_setting)
				# print(no_change_cutoff)
				# raise

				# old code
					# so basically, for 
					# print(group, dens_cutoff_setting)
					# hfnxns.quick_dumps(settings.get('save_dirs')
					# 		.get('length_change_pipeline_dirs')
					# 		.get(group)
					# 		.get('length_change_analysis')
					# 		.get('no_change_peaks_analysis'))
					# hfnxns.quick_dumps(settings.get('save_dirs')
					# 		.get('length_change_pipeline_dirs')
					# 		.get(group)
					# 		.get('length_change_analysis')
					# 		.get('lengthened_peaks_analysis'))
					# hfnxns.quick_dumps(settings.get('save_dirs')
					# 		.get('length_change_pipeline_dirs')
					# 		.get(group)
					# 		.get('length_change_analysis')
					# 		.get('shortened_peaks_analysis'))

												# 'len_dens_change_scatter'
					# plot_dir 			=	os.path.join(settings.get('save_dirs')
					# 								.get('length_change_pipeline_dirs')
					# 								.get(group)
					# 								.get('length_change_analysis')
					# 								.get('plot_dir')
					# 								.get('path'),
					# 							'len_dens_change_scatter'
											# ),
					# load_spot, save_spot, cancer_type, dict_save_spot, len_dens_ind)



			# continue

	# print([(job_id, settings.get('workers').submitted_jobs.get(job_id).ready(), \
	# 	[this_key for this_key in settings.get('workers').job_groups.keys() if \
	# 		job_id in settings.get('workers').job_groups.get(this_key)]) for job_id in \
	# 	settings.get('workers').submitted_jobs.keys()])
	# print([(job_id, settings.get('workers').all_submitted_async_results.get(job_id).ready(), \
	# 	[this_key for this_key in settings.get('workers').job_groups.keys() if \
	# 		job_id in settings.get('workers').job_groups.get(this_key)]) for job_id in \
	# 	settings.get('workers').all_submitted_async_results.keys()])
	# print(colored('blocking_for_all_jobs', 'red'))
	# settings.get('workers').block_for_all_jobs(reset_prio=False)
	
	
	# print(colored('done blocking_for_all_jobs', 'red'))

	# settings['workers'] = \
	# 	hfnxns.close_reopen_workers(settings.get('workers'), \
	# 		settings.get('cpu_cores'))
	# raise


				# old code
					# if (settings.get('which_analysis').get(step_name) == True):
					# 	for analysis_loop, specific_analysis in enumerate(comparison):
					# 		other_analysis = [analysis for analysis in comparison if \
					# 			analysis != specific_analysis][0]
					# 		analysis_key = 'group{}_normal_common'.format(analysis_loop+1)

					# 		# make tissue type-wise control common beds for each group
					# 		for group in ['test', 'control']:
					# 			if analysis_loop == 0:
					# 				ind1 = 1
					# 				ind2 = 2
					# 			else:
					# 				ind1 = 2
					# 				ind2 = 1
					# 			if group == 'test':

					# 				other_group = 'control'
					# 			else:
					# 				other_group = 'test'
					# 			unique_key = 'g{ind1}_{group}_subtract_g{ind2}_{group}'.format(
					# 				ind1=ind1, ind2=ind2, group=group)

								# group_path_name = settings.get('save_dirs')\
								# 						.get('comparison_pipeline_dirs')\
								# 						.get(compare_key)\
								# 						.get('unique_common')\
								# 						.get('unique_peak_beds')\
								# 						.get(unique_key)\
								# 						.get('path').split('/')[-1]


				# old code
					# print(kwargs1)
					# print(settings.get('save_dirs')
					# 			.get('length_change_pipeline_dirs')
					# 			.get(group)
					# 			.get('length_change_analysis')
					# 			.get('{}ed_peaks_analysis'
					# 				.format(len_setting))
					# 			.get(dens_cutoff_setting)
					# 			.get('gene_dir')
					# 			.get('path'))
					# print(path_file_dict)
					# print(path_types)


					# path_plot_dir = os.path.join(settings.get('save_dirs')\
					# 						.get('comparison_pipeline_dirs')\
					# 						.get(compare_key)\
					# 						.get('plot_dir')\
					# 						.get('path'),
					# 					'pathway_analysis',
					# 					group_path_name)



					# save_dir			=	save_dir, 


	# group_path_name = '{}ed_{}_dens_peaks_analysis' \
	# 	.format(len_setting, dens_cutoff_setting)
	# path_plot_dir = os.path.join(settings.get('save_dirs')
	# 								.get('length_change_pipeline_dirs')
	# 								.get(group)
	# 								.get('length_change_analysis')
	# 								.get('{}ed_peaks_analysis'
	# 									.format(len_setting))
	# 								.get(dens_cutoff_setting)
	# 								.get('plot_dir')\
	# 								.get('path'),
	# 						'pathway_analysis') #,
	# 						# group_path_name)
	# os.makedirs(path_plot_dir, exist_ok=True)
	# path_file_dict = file_handler(
	# 								step_name, 
	# 								settings.get('save_dirs')
	# 									.get('length_change_pipeline_dirs')
	# 									.get(group)
	# 									.get('length_change_analysis')
	# 									.get('{}ed_peaks_analysis'
	# 										.format(len_setting))
	# 									.get(dens_cutoff_setting)
	# 									.get('gene_dir')
	# 									.get('path'),
	# 									**kwargs1
	# 							)
	# path_types = sorted(list(path_file_dict\
	# 	.get(list(path_file_dict.keys())[0])\
	# 	.keys()))
	# settings['setup_variables'] = pathway_analysis(
	# 	file_dict			=	path_file_dict,
	# 	plot_dir			=	path_plot_dir, 
	# 	pathway_gene_files 	=	settings.get('pathway_gene_files'),
	# 	mp_workers 			=	settings.get('workers'),
	# 	setup_variables 	= 	settings.get('setup_variables'),
	# 	# cpu_count 			=	settings.get('cpu_cores')

	# )

	# group_path_name = '{}ed_{}_dens_peaks_analysis' \
	# .format(len_setting, dens_cutoff_setting)
	# path_plot_dir = os.path.join(settings.get('save_dirs')
	# 						.get('length_change_pipeline_dirs')
	# 						.get(group)
	# 						.get('length_change_analysis')
	# 						.get('no_change_peaks_analysis')
	# 						.get(dens_cutoff_setting)
	# 						.get('plot_dir')\
	# 						.get('path'),
	# 				'pathway_analysis') #,
	# 				# group_path_name)
	# os.makedirs(path_plot_dir, exist_ok=True)
	# path_file_dict = file_handler(
	# 						step_name, 
	# 						settings.get('save_dirs')
	# 							.get('length_change_pipeline_dirs')
	# 							.get(group)
	# 							.get('length_change_analysis')
	# 							.get('no_change_peaks_analysis')
	# 							.get(dens_cutoff_setting)
	# 							.get('gene_dir')
	# 							.get('path'),
	# 							**kwargs1
	# 					)
	# path_types = sorted(list(path_file_dict\
	# .get(list(path_file_dict.keys())[0])\
	# .keys()))
	# settings['setup_variables'] = pathway_analysis(
	# file_dict			=	path_file_dict,
	# plot_dir			=	path_plot_dir, 
	# pathway_gene_files 	=	settings.get('pathway_gene_files'),
	# mp_workers 			=	settings.get('workers'),
	# setup_variables 	= 	settings.get('setup_variables'),
	# # cpu_count 			=	settings.get('cpu_cores')

	# )














	# these are the old functions for the comparison pipeline and len change pipeline. 
		# they're here for reference while working on
		# setting up the new versions of them
	# def comparison_analysis(top_compare_dir, which_lists, \
		# list_settings_compare, color_dict, pipeline_dirs, \
		# tumor_controls, data_info, save_dirs, list_settings, \
		# which_analysis, pathway_gene_files, verbose, logger):
		# save_dirs_compare = {}
		# save_dirs_compare['top_merged_dir'] = \
		# 	os.path.join(top_compare_dir, 'merged_beds')
		# save_dirs_compare['unique_common'] = \
		# 	os.path.join(top_compare_dir, 'unique_commons')
		# save_dirs_compare['peak_size_dir'] = \
		# 	os.path.join(top_compare_dir, 'peak_size_dir')
		# save_dirs_compare['ref_normal_common'] = \
		# 	os.path.join(save_dirs_compare.get('unique_common'), \
		# 		'ref_normal_common')
		# save_dirs_compare['ref_normal_unique'] = \
		# 	os.path.join(save_dirs_compare.get('unique_common'), \
		# 		'ref_normal_unique')
		# save_dirs_compare['ref_cancer_unique'] = \
		# 	os.path.join(save_dirs_compare.get('unique_common'), \
		# 		'ref_cancer_unique')
		# save_dirs_compare['size_ref_normal_common'] = \
		# 	os.path.join(save_dirs_compare.get('peak_size_dir'), \
		# 		'ref_normal_common')
		# save_dirs_compare['size_ref_cancer_common'] = \
		# 	os.path.join(save_dirs_compare.get('peak_size_dir'), \
		# 		'ref_cancer_common')
		# save_dirs_compare['size_compare_normal_common'] = \
		# 	os.path.join(save_dirs_compare.get('peak_size_dir'), \
		# 		'compare_normal_common')
		# save_dirs_compare['size_compare_cancer_common'] = \
		# 	os.path.join(save_dirs_compare.get('peak_size_dir'), \
		# 		'compare_cancer_common')
		# save_dirs_compare['compare_normal_common'] = \
		# 	os.path.join(save_dirs_compare.get('unique_common'), \
		# 		'compare_normal_common')
		# save_dirs_compare['compare_normal_unique'] = \
		# 	os.path.join(save_dirs_compare.get('unique_common'), \
		# 		'compare_normal_unique')
		# save_dirs_compare['compare_cancer_unique'] = \
		# 	os.path.join(save_dirs_compare.get('unique_common'), \
		# 		'compare_cancer_unique')
		# save_dirs_compare['tss_gene_dir'] = \
		# 	os.path.join(top_compare_dir, 'tss_intersect_gene_names')
		# save_dirs_compare['gene_dir'] = \
		# 	os.path.join(top_compare_dir, 'gene_lists')
		# save_dirs_compare['plot_dir'] = \
		# 	os.path.join(top_compare_dir, 'plots')
		# save_dirs_compare['compare_beds_dir'] = \
		# 	os.path.join(top_compare_dir, 'compare_beds')

		# for this_key in list(save_dirs_compare.keys()):
		# 	os.makedirs(save_dirs_compare.get(this_key), exist_ok=True)

		# directories = get_type_list(color_dict, tumor_controls, \
		# 	include_all=False)
		# if not 'E' in directories: directories.append('E')
		# hfnxns.make_shape_merged_bed(save_dirs_compare, pipeline_dirs, \
		# 	directories, list_settings_compare, data_info)
		# if 'E' in directories: directories.remove('E')

		# if (which_analysis.get('broad_sharp_cancer_plots') == True):
		# 	tsg_file = pipeline_dirs.get('tuson_tsg_file')
		# 	og_file = pipeline_dirs.get('tuson_og_file')
		# 	hfnxns.cancer_list_plots([save_dirs[which_lists[0]]\
		# 		.get('gene_dir'), save_dirs[which_lists[1]]\
		# 		.get('gene_dir')], 
		# 		save_dirs_compare.get('plot_dir'), 'len', 'tuson', \
		# 		pipeline_dirs.get('data_for_figures'), 
		# 		tuson_tsg=tsg_file, tuson_og=og_file)
		# 	hfnxns.cancer_list_plots([save_dirs[which_lists[0]]\
		# 		.get('gene_dir'), save_dirs[which_lists[1]]\
		# 		.get('gene_dir')], 
		# 		save_dirs_compare.get('plot_dir'), 'len', 'cosmic', \
		# 		pipeline_dirs.get('data_for_figures'), 
		# 		cosmic_list=pipeline_dirs.get('cosmic_file'))

		# for cancer_type in directories:
		# 	cancer_type_controls = tumor_controls\
		# 		.get(cancer_type+'_control')
		# 	normal_common_list = (save_dirs_compare\
		# 		.get('ref_normal_common'), \
		# 		save_dirs_compare.get('compare_normal_common'))
		# 	for which_loop in range(len(which_lists)):
		# 		hfnxns.intersect_common_beds(\
		# 			save_dirs[which_lists[which_loop]].get('split_dir'),\
		# 			 normal_common_list[which_loop], cancer_type_controls, 
		# 			cancer_type, group_pattern='.bed', file_percent=0)
		# cancer_unique_types = \
		# 	[thing.split('/')[-1].split('_')[0] for thing in \
		# 	glob.iglob(os.path.join(save_dirs[which_lists[0]]\
		# 	.get('common_save'),'*')) if \
		# 	thing.split('/')[-1].split('_')[0] not in ['all', 'E']]
		# cancer_unique_dirs = hfnxns.intersect_for_peak_len(
		# 	save_dirs_compare.get('ref_cancer_unique'), 
		# 	save_dirs_compare.get('compare_cancer_unique'), 
		# 	save_dirs[which_lists[0]].get('common_save'), 
		# 	save_dirs[which_lists[1]].get('common_save'), 
		# 	cancer_unique_types, 
		# 	normals=False
		# 	)
		# normal_unique_dirs = hfnxns.intersect_for_peak_len(
		# 	save_dirs_compare.get('ref_normal_unique'), 
		# 	save_dirs_compare.get('compare_normal_unique'), 
		# 	save_dirs_compare.get('ref_normal_common'), 
		# 	save_dirs_compare.get('compare_normal_common'), 
		# 	directories, 
		# 	normals=True)
		# for this_dir in (*cancer_unique_dirs, *normal_unique_dirs):
		# 	this_group_dirs = {
		# 		'common_save': this_dir,
		# 		'gene_dir': os.path.join(\
		# 			save_dirs_compare.get('gene_dir'), \
		# 			this_dir.split('/')[-1]),
		# 		'tss_gene_dir': os.path.join(\
		# 			save_dirs_compare.get('tss_gene_dir'), \
		# 			this_dir.split('/')[-1]),
		# 		'plot_dir': os.path.join(\
		# 			save_dirs_compare.get('plot_dir'), \
		# 			this_dir.split('/')[-1])
		# 	}
		# 	for this_key in list(this_group_dirs.keys()):
		# 		os.makedirs(this_group_dirs.get(this_key), exist_ok=True)
		# 	hfnxns.match_tss_gene_names(pipeline_dirs=pipeline_dirs, \
		# 		save_dirs=this_group_dirs, other_pipeline=True, \
		# 		verbose = verbose, other_pattern='.bed', \
		# 		keep_indices=[0,3], logger=logger)
		# 	if (which_analysis.get('path_analysis') == True):
		# 		pathway_analysis(pipeline_dirs=pipeline_dirs, \
		# 			save_dirs=this_group_dirs, \
		# 			pathway_gene_files=pathway_gene_files, logger=logger)
		# if (which_analysis.get('path_dots') == True):
		# 	for this_dir in (cancer_unique_dirs, normal_unique_dirs):
		# 		broad_dir = os.path.join(os.path.join(\
		# 			save_dirs_compare.get('plot_dir'), \
		# 			this_dir[0].split('/')[-1]), 
		# 			'common_pathways/no_go_paths/textfile')
		# 		sharp_dir = os.path.join(os.path.join(\
		# 			save_dirs_compare.get('plot_dir'), \
		# 			this_dir[1].split('/')[-1]), 
		# 			'common_pathways/no_go_paths/textfile')
		# 		save_dir = os.path.join(\
		# 			pipeline_dirs.get('data_for_figures'), 'path_dots', \
		# 			this_dir[0].split('/')[-1])
		# 		plot_save = os.path.join(os.path.join(\
		# 			save_dirs_compare.get('plot_dir'), \
		# 			this_dir[0].split('/')[-1] + '.png'))
		# 		hfnxns.unique_broad_sharp_data(color_dict, directories, \
		# 			broad_dir, sharp_dir, save_dir, plot_save, \
		# 			verbose=False)
		# group_dirs = (save_dirs_compare.get('size_ref_normal_common'), \
		# 	save_dirs_compare.get('size_ref_cancer_common'), 
		# 	save_dirs_compare.get('size_compare_normal_common'), \
		# 	save_dirs_compare.get('size_compare_cancer_common'))
		# ref_dirs = (normal_unique_dirs[0], cancer_unique_dirs[0], \
		# 	normal_unique_dirs[1], cancer_unique_dirs[1])
		# normal_broad_list = (True, False, True, False)
		# group_names = ('normal_ref', 'cancer_ref', 'normal_compare', \
		# 	'cancer_compare')
		# for cancer_type in directories:
		# 	cancer_type_controls = \
		# 		tumor_controls.get(cancer_type+'_control')
		# 	for group_loop in range(len(group_dirs)):
		# 		hfnxns.get_peak_avg_sizes(
		# 			save_dirs_compare.get('top_merged_dir'), 
		# 			ref_dirs[group_loop], 
		# 			os.path.join(group_dirs[group_loop], 'ref_peaksize'), 
		# 			cancer_type_controls, 
		# 			cancer_type, 
		# 			common_vals=True, 
		# 			control_broad=normal_broad_list[group_loop])
		# 		hfnxns.get_peak_avg_sizes(
		# 			save_dirs_compare.get('top_merged_dir'), 
		# 			ref_dirs[group_loop], 
		# 			os.path.join(\
		# 				group_dirs[group_loop], 'compare_peaksize'), 
		# 			cancer_type_controls, 
		# 			cancer_type, 
		# 			common_vals=False, 
		# 			control_broad=normal_broad_list[group_loop])
		# 		hfnxns.peak_size_change_beds(
		# 			os.path.join(\
		# 				group_dirs[group_loop], 'ref_peaksize'), 
		# 			os.path.join(\
		# 				group_dirs[group_loop], 'compare_peaksize'), 
		# 			cancer_type,
		# 			os.path.join(\
		# 				save_dirs_compare.get('compare_beds_dir'), \
		# 				group_names[group_loop]), 
		# 			control_broad=normal_broad_list[group_loop])
		# for group_loop in range(len(group_dirs)):
		# 	if (group_loop == 0) or (group_loop == 1):
		# 		title_pattern = list_settings[which_lists[0]]\
		# 		.get('title_pattern')
		# 	else:
		# 		title_pattern = list_settings[which_lists[1]]\
		# 		.get('title_pattern')
		# 	hfnxns.plot_kde_size_change(pipeline_dirs, \
		# 		os.path.join(save_dirs_compare.get('compare_beds_dir'), \
		# 		group_names[group_loop], 'all_dir'), \
		# 		save_dirs_compare.get('plot_dir'), \
		# 		title_pattern, color_dict=color_dict, \
		# 		control_broad=normal_broad_list[group_loop], \
		# 		value_index=3, log2=True, x_min_max = (-3,3))

	# def len_change_analysis(len_change_dir, color_dict, tumor_controls, \
		# pipeline_dirs, directories, data_info, which_analysis, pathway_gene_files, \
		# verbose, logger):
		# save_dirs = {}
		# save_dirs['longest_shortest_dir'] = \
		# 	os.path.join(len_change_dir, 'most_lengthen_shorten')
		# save_dirs['top_cosmic_dir'] = \
		# 	os.path.join(save_dirs.get('longest_shortest_dir'), 'most_len_short')
		# save_dirs['peak_size_dir'] = \
		# 	os.path.join(len_change_dir, 'peak_size_dir')

		# save_dirs['common_save'] = \
		# 	os.path.join(len_change_dir, 'cancer_common')
		# save_dirs['compare_beds_dir'] = \
		# 	os.path.join(len_change_dir, 'compare_beds')
		# save_dirs['formatted_cosmic'] = \
		# 	os.path.join(save_dirs.get('top_cosmic_dir'), 'formatted_files')
		# save_dirs['gene_dir'] = \
		# 	os.path.join(len_change_dir, 'gene_lists')
		# save_dirs['len_short_no_cosmic'] = \
		# 	os.path.join(save_dirs.get('top_cosmic_dir'), 'no_cosmic_files')
		# save_dirs['len_short_genes'] = \
		# 	os.path.join(save_dirs.get('longest_shortest_dir'), 'gene_lists')
		# save_dirs['len_short_peaks'] = \
		# 	os.path.join(save_dirs.get('longest_shortest_dir'), 'peak_lists')
		# save_dirs['normal_common'] = \
		# 	os.path.join(len_change_dir, 'normal_common')
		# save_dirs['plot_dir'] = \
		# 	os.path.join(len_change_dir, 'plots')
		# save_dirs['ref_cancer_common'] = \
		# 	os.path.join(save_dirs.get('peak_size_dir'), 'ref_cancer_common')
		# save_dirs['ref_normal_common'] = \
		# 	os.path.join(save_dirs.get('peak_size_dir'), 'ref_normal_common')
		# save_dirs['sorted_len_short_peak'] = \
		# 	os.path.join(save_dirs.get('longest_shortest_dir'), 'sorted_peak_lists')
		# save_dirs['len_short_tss_genes'] = \
		# 	os.path.join(save_dirs.get('longest_shortest_dir'), 'tss_gene_lists')
		# save_dirs['sorted_tss_genes'] = \
		# 	os.path.join(save_dirs.get('longest_shortest_dir'), 'sorted_tss_gene_lists')
		# save_dirs['split_dir'] = \
		# 	os.path.join(len_change_dir, 'split_beds')
		# save_dirs['tabbed_cosmic'] = \
		# 	os.path.join(save_dirs.get('top_cosmic_dir'), 'tabbed_files')
		# save_dirs['top_merged_dir'] = \
		# 	os.path.join(len_change_dir, 'merged_beds')
		# save_dirs['tss_gene_dir'] = \
		# 	os.path.join(len_change_dir, 'tss_intersect_gene_names')

		# save_dirs['real_short_dir'] = \
		# 	os.path.join(save_dirs.get('longest_shortest_dir'), 'real_short_dir')
		# save_dirs['real_short_peaks'] = \
		# 	os.path.join(save_dirs.get('real_short_dir'), 'peak_lists')
		# save_dirs['sorted_real_short_peak'] = \
		# 	os.path.join(save_dirs.get('real_short_dir'), 'sorted_peak_lists')
		# save_dirs['real_short_tss_genes'] = \
		# 	os.path.join(save_dirs.get('real_short_dir'), 'tss_gene_lists')
		# save_dirs['real_short_genes'] = \
		# 	os.path.join(save_dirs.get('real_short_dir'), 'gene_lists')
		# save_dirs['sorted_short_genelist'] = \
		# 	os.path.join(save_dirs.get('real_short_dir'), 'sorted_short_genelist')

		# list_settings = {
		# 	'merge_len': 1000,
		# 	'sample_common_perc': ['25', '0'],
		# 	'split_type': None,
		# 	'file_pattern': '_len_change',
		# 	'title_pattern': 'Len Change'
		# }
		# for this_key in list(save_dirs.keys()):
		# 	os.makedirs(save_dirs.get(this_key), exist_ok=True)
		# normal_types = []
		# directories = get_type_list(color_dict, tumor_controls, include_all=True)

		# if not 'E' in directories: directories.append('E')
		# hfnxns.make_shape_merged_bed(save_dirs, pipeline_dirs, directories, \
		# 	list_settings, data_info)
		# if 'E' in directories: directories.remove('E')
		# hfnxns.make_common_beds(file_percent = list_settings.get('sample_common_perc'), \
		# 	pipeline_dirs=pipeline_dirs, directories = directories, \
		# 	save_dirs=save_dirs, group_pattern=list_settings.get('file_pattern'), \
		# 	list_settings= list_settings)
		# hfnxns.make_all_common_beds(file_percent = 0, pipeline_dirs=pipeline_dirs, \
		# 	directories = directories, save_dirs=save_dirs, \
		# 	group_pattern=list_settings.get('file_pattern'), \
		# 	list_settings= list_settings)
		# group_dirs = (save_dirs.get('ref_normal_common'), \
		# 	save_dirs.get('ref_cancer_common'))
		# ref_dirs = (save_dirs.get('normal_common'), save_dirs.get('common_save'))
		# normal_broad_list = (True, False)
		# group_names = ('normal', 'cancer')
		# for cancer_type in directories:
		# 	len_change_main_steps(cancer_type, save_dirs, group_dirs, ref_dirs, \
		# 		normal_broad_list, pipeline_dirs, verbose, pathway_gene_files, \
		# 		group_names, tumor_controls, which_analysis, color_dict, directories, logger)

		# if (which_analysis.get('path_dots') == True):
		# 	for group_loop in range(len(group_dirs)):
		# 		this_group_dirs = {
		# 			'unsorted_common_save': \
		# 				os.path.join(save_dirs.get('len_short_peaks'), \
		# 					group_names[group_loop]),
		# 			'common_save': \
		# 				os.path.join(save_dirs.get('sorted_len_short_peak'), \
		# 					group_names[group_loop]),
		# 			'tss_gene_dir': \
		# 				os.path.join(save_dirs.get('len_short_tss_genes'), \
		# 					group_names[group_loop]),
		# 			'gene_dir': \
		# 				os.path.join(save_dirs.get('len_short_genes'), \
		# 					group_names[group_loop]),
		# 			'plot_dir': \
		# 				os.path.join(save_dirs.get('plot_dir'), \
		# 					group_names[group_loop], 'plots'),
		# 		}
		# 		len_short_dir = os.path.join(this_group_dirs.get('plot_dir'), \
		# 			'common_pathways/no_go_paths/textfile')
		# 		save_dir = os.path.join(pipeline_dirs.get('data_for_figures'), \
		# 			'path_dots')
		# 		plot_save = '/'.join(this_group_dirs.get('plot_dir').split('/')[:-1])+\
		# 			'_path_dots.png'
		# 		hfnxns.lengthen_shorten_data(color_dict, directories, len_short_dir, \
		# 			save_dir, plot_save, verbose=False)
		# for group_loop in range(len(group_dirs)):
		# 	sort_gene_names(group_loop, group_names, save_dirs)

		# if (which_analysis.get('len_change_kde') == True):
		# 	color_dict['all'] = 'k'
		# 	for group_loop in range(len(group_dirs)):
		# 		hfnxns.plot_kde_size_change(pipeline_dirs, \
		# 			os.path.join(save_dirs.get('compare_beds_dir'), \
		# 			group_names[group_loop], 'all_dir'), save_dirs.get('plot_dir'), 
		# 			'All_Peaks', color_dict=color_dict, \
		# 			control_broad=normal_broad_list[group_loop], \
		# 			value_index = 3, log2=True, x_min_max=(-3,3))
		# 		hfnxns.plot_kde_size_change(pipeline_dirs, \
		# 			os.path.join(save_dirs.get('compare_beds_dir'), \
		# 			group_names[group_loop], 'all_dir'), save_dirs.get('plot_dir'), 
		# 			'All_Peaks', color_dict=color_dict, \
		# 			control_broad=normal_broad_list[group_loop], \
		# 			value_index = 5, log2=False, x_min_max=(-4000,4000))

		# if (which_analysis.get('len_change_cancer_plots') == True):
		# 	tsg_file = pipeline_dirs.get('tuson_tsg_file')
		# 	og_file = pipeline_dirs.get('tuson_og_file')
		# 	hfnxns.cancer_list_plots(os.path.join(save_dirs.get('compare_beds_dir'), \
		# 		'normal/all_dir'), save_dirs.get('plot_dir'), 'len_change', 'tuson', \
		# 		pipeline_dirs.get('data_for_figures'), \
		# 		tuson_tsg=tsg_file, tuson_og=og_file, \
		# 		bed_10kb_tss=pipeline_dirs.get('bed_10kb_tss'))
		# 	hfnxns.cancer_list_plots(os.path.join(save_dirs.get('compare_beds_dir'), \
		# 		'normal/all_dir'), save_dirs.get('plot_dir'), 'len_change', 'cosmic', \
		# 		pipeline_dirs.get('data_for_figures'), \
		# 		cosmic_list=pipeline_dirs.get('cosmic_file'), \
		# 		bed_10kb_tss=pipeline_dirs.get('bed_10kb_tss'))

		# if (which_analysis.get('len_dens_change') == True):
		# 	cutoff_groups = ('no_cutoff', 'half', 'three_quarters', 'same')
		# 	for cutoff_loop in range(len(cutoff_groups)):
		# 		for group_loop in range(len(group_dirs)):
		# 			this_group_dirs = {
		# 				'unsorted_common_save': \
		# 					os.path.join(save_dirs.get('real_short_peaks'), \
		# 						group_names[group_loop], cutoff_groups[cutoff_loop]),
		# 				'common_save': \
		# 					os.path.join(save_dirs.get('sorted_real_short_peak'), \
		# 						group_names[group_loop], cutoff_groups[cutoff_loop]),
		# 				'tss_gene_dir': \
		# 					os.path.join(save_dirs.get('real_short_tss_genes'), \
		# 						group_names[group_loop], cutoff_groups[cutoff_loop]),
		# 				'gene_dir': \
		# 					os.path.join(save_dirs.get('real_short_genes'), \
		# 						group_names[group_loop], cutoff_groups[cutoff_loop]),
		# 				'sorted_genedir': \
		# 					os.path.join(save_dirs.get('sorted_short_genelist'), \
		# 						group_names[group_loop], cutoff_groups[cutoff_loop]),
		# 				'plot_dir': \
		# 					os.path.join(save_dirs.get('plot_dir'), \
		# 						'real_short_dir', \
		# 						group_names[group_loop]+'_'+cutoff_groups[cutoff_loop]),
		# 			}				
		# 			if (which_analysis.get('path_dots') == True):
		# 				len_short_dir = os.path.join(this_group_dirs.get('plot_dir'), \
		# 					'common_pathways/no_go_paths/textfile')
		# 				save_dir = os.path.join(pipeline_dirs.get('data_for_figures'), \
		# 					'path_dots',this_group_dirs.get('plot_dir').split('/')[-1])
		# 				plot_save = this_group_dirs.get('plot_dir')+'_path_dots.png'
		# 				hfnxns.lengthen_shorten_data(color_dict, directories, \
		# 					len_short_dir, save_dir, plot_save, verbose=False)

	# def len_change_main_steps(cancer_type, save_dirs, group_dirs, ref_dirs, \
		# normal_broad_list, pipeline_dirs, verbose, pathway_gene_files, group_names, \
		# tumor_controls, which_analysis, color_dict, directories, logger):
		# cancer_type_controls = tumor_controls.get(cancer_type+'_control')
		# hfnxns.intersect_common_beds(save_dirs.get('top_merged_dir'), \
		# 	save_dirs.get('normal_common'), cancer_type_controls, 
		# 	cancer_type, group_pattern='.bed', file_percent=0)
		# for group_loop in range(len(group_dirs)):
		# 	this_loop_name = cancer_type+' '+group_names[group_loop]
		# 	hfnxns.get_peak_avg_sizes(save_dirs.get('top_merged_dir'), \
		# 		ref_dirs[group_loop], os.path.join(group_dirs[group_loop], 'ref_peaksize'), \
		# 		cancer_type_controls, cancer_type, common_vals=True, \
		# 		control_broad=normal_broad_list[group_loop])
		# 	hfnxns.get_peak_avg_sizes(save_dirs.get('top_merged_dir'), \
		# 		ref_dirs[group_loop], os.path.join(group_dirs[group_loop], 'compare_peaksize'), \
		# 		cancer_type_controls, cancer_type, common_vals=False, \
		# 		control_broad=normal_broad_list[group_loop])
		# 	hfnxns.peak_size_change_beds(os.path.join(group_dirs[group_loop], \
		# 		'ref_peaksize'), os.path.join(group_dirs[group_loop], 'compare_peaksize'), \
		# 		cancer_type, os.path.join(save_dirs.get('compare_beds_dir'), \
		# 			group_names[group_loop]), control_broad=normal_broad_list[group_loop])
		# 	hfnxns.get_longest_shortest_peaks(os.path.join(\
		# 		save_dirs.get('compare_beds_dir'), group_names[group_loop],'all_dir'), \
		# 		os.path.join(save_dirs.get('len_short_peaks'), \
		# 		group_names[group_loop]), cancer_type, 20)
		# 	this_group_dirs = {
		# 		'unsorted_common_save': \
		# 			os.path.join(save_dirs.get('len_short_peaks'), \
		# 				group_names[group_loop]),
		# 		'common_save': \
		# 			os.path.join(save_dirs.get('sorted_len_short_peak'), \
		# 				group_names[group_loop]),
		# 		'tss_gene_dir': \
		# 			os.path.join(save_dirs.get('len_short_tss_genes'), \
		# 				group_names[group_loop]),
		# 		'gene_dir': \
		# 			os.path.join(save_dirs.get('len_short_genes'), \
		# 				group_names[group_loop]),
		# 		'plot_dir': \
		# 			os.path.join(save_dirs.get('plot_dir'), \
		# 				group_names[group_loop], 'plots'),
		# 	}
		# 	for this_key in list(this_group_dirs.keys()):
		# 		os.makedirs(this_group_dirs.get(this_key), exist_ok=True)
		# 	for filepath in \
		# 		glob.iglob(os.path.join(this_group_dirs.get('unsorted_common_save'), '*')):
		# 		peak_list = hfnxns.open_and_read(filepath, '\t')
		# 		peak_list = sorted(peak_list, key=operator.itemgetter(0,1))
		# 		save_spot = os.path.join(this_group_dirs.get('common_save'), \
		# 			filepath.split('/')[-1])
		# 		hfnxns.write_bed(save_spot, peak_list)
		# 	hfnxns.match_tss_gene_names(pipeline_dirs=pipeline_dirs, \
		# 		save_dirs=this_group_dirs, other_pipeline=True, verbose = verbose, \
		# 		other_pattern='.bed', keep_indices=[0,6], logger=logger)
		# 	if (which_analysis.get('path_analysis') == True):
		# 		pathway_analysis(pipeline_dirs=pipeline_dirs, \
		# 			save_dirs=this_group_dirs, pathway_gene_files=pathway_gene_files, \
		# 			logger=logger)
		# 	if (which_analysis.get('upsetr_plot') == True):
		# 		os.makedirs(os.path.join(len_change_dirs.get('compare_beds'), \
		# 			this_group, 'intervene'), exist_ok=True)
		# 		upset_dirs = intervene_analysis(intervene_dir=\
		# 			os.path.join(len_change_dirs.get('compare_beds'), this_group, \
		# 			'intervene'), save_dirs=this_group_dirs, list_settings=list_settings)
		# 		if (which_analysis.get('path_analysis') == True):
		# 			pathway_analysis(pipeline_dirs=pipeline_dirs, \
		# 			save_dirs=this_group_dirs, pathway_gene_files=pathway_gene_files, \
		# 			upsetr=True, upset_dirs=upset_dirs, logger=logger)
		# 	if (which_analysis.get('len_dens_change') == True):
		# 		cutoff_groups = ('no_cutoff', 'half', 'three_quarters', 'same')
		# 		cutoffs = (0.0, 0.5, 0.75, 1)
		# 		for cutoff_loop in range(len(cutoff_groups)):
		# 			this_group_dirs = {
		# 				'unsorted_common_save': \
		# 					os.path.join(save_dirs.get('real_short_peaks'), \
		# 						group_names[group_loop], cutoff_groups[cutoff_loop]),
		# 				'common_save': \
		# 					os.path.join(save_dirs.get('sorted_real_short_peak'), \
		# 						group_names[group_loop], cutoff_groups[cutoff_loop]),
		# 				'tss_gene_dir': \
		# 					os.path.join(save_dirs.get('real_short_tss_genes'), \
		# 						group_names[group_loop], cutoff_groups[cutoff_loop]),
		# 				'gene_dir': \
		# 					os.path.join(save_dirs.get('real_short_genes'), \
		# 						group_names[group_loop], cutoff_groups[cutoff_loop]),
		# 				'sorted_genedir': \
		# 					os.path.join(save_dirs.get('sorted_short_genelist'), \
		# 						group_names[group_loop], cutoff_groups[cutoff_loop]),
		# 				'plot_dir': \
		# 					os.path.join(save_dirs.get('plot_dir'), \
		# 						'real_short_dir', \
		# 						group_names[group_loop]+'_'+cutoff_groups[cutoff_loop]),
		# 			}				
		# 			for this_key in list(this_group_dirs.keys()):
		# 				os.makedirs(this_group_dirs.get(this_key), exist_ok=True)
		# 			hfnxns.get_just_shortened_peaks(os.path.join(\
		# 				save_dirs.get('compare_beds_dir'), \
		# 				group_names[group_loop],'all_dir'), \
		# 				this_group_dirs.get('unsorted_common_save'), \
		# 				cancer_type, cutoffs[cutoff_loop])
		# 			for filepath in glob.iglob(os.path.join(\
		# 					this_group_dirs.get('unsorted_common_save'), '*')):
		# 				peak_list = hfnxns.open_and_read(filepath, '\t')
		# 				peak_list = sorted(peak_list, key=operator.itemgetter(0,1))
		# 				save_spot = os.path.join(this_group_dirs.get('common_save'), \
		# 					filepath.split('/')[-1])
		# 				hfnxns.write_bed(save_spot+'.bed', peak_list)
		# 			hfnxns.match_tss_gene_names(pipeline_dirs=pipeline_dirs, \
		# 				save_dirs=this_group_dirs, other_pipeline=True, \
		# 				verbose = False, other_pattern='.bed', \
		# 				keep_indices=[0,7], logger=logger)
		# 			if (which_analysis.get('path_analysis') == True):
		# 				pathway_analysis(pipeline_dirs=pipeline_dirs, \
		# 					save_dirs=this_group_dirs, \
		# 					pathway_gene_files=pathway_gene_files, \
		# 					logger=logger)
		# 			for filepath in glob.iglob(os.path.join(\
		# 				this_group_dirs.get('tss_gene_dir'), '*')):
		# 				peaklist = hfnxns.open_and_read(filepath, '\t')
		# 				genelist = sorted(list(set([(thing[8], float(thing[5]), \
		# 					float(thing[6])) for thing in peaklist])), \
		# 					key=operator.itemgetter(1,0))
		# 				save_spot = os.path.join(this_group_dirs.get('sorted_genedir'), \
		# 					filepath.split('/')[-1].split('_')[0]+'_gene_names.txt')
		# 				hfnxns.write_bed(save_spot, [[str(stuff) for stuff in thing] \
		# 					for thing in genelist])
		# 		if group_names[group_loop] == 'normal':
		# 			os.makedirs(os.path.join(save_dirs.get('plot_dir'), \
		# 				'len_dens_change_scatter'), exist_ok=True)
		# 			bed_load_spot = os.path.join(save_dirs.get('compare_beds_dir'), \
		# 				group_names[group_loop], 'all_dir', cancer_type+'_all_common.bed')
		# 			save_spot = os.path.join(save_dirs.get('plot_dir'), \
		# 				'len_dens_change_scatter', cancer_type)
		# 			dict_save_dir = os.path.join(pipeline_dirs.get('data_for_figures'), \
		# 				'change_scatter')
		# 			os.makedirs(dict_save_dir, exist_ok=True)
		# 			dict_save_spot = os.path.join(dict_save_dir, cancer_type)
		# 			hfnxns.len_dens_change_scatter(bed_load_spot, save_spot, \
		# 				cancer_type, dict_save_spot, len_dens_ind=(5,6))



	# def old_load_raw_data(settings):
		# if settings.get('data_loading_info').get('control_dict') is not None:
		# 	for this_type in settings.get('data_loading_info').get('control_dict').keys():
		# 		file_format_str = os.path.join(settings.get('data_loading_info')\
		# 		.get('control_data_dir'), \
		# 		settings.get('data_loading_info').get('control_load_format'))

		# normal_sample_load_list = []
		# if tumor_controls is not None:
		# 	if normal_type_skip is not None:
		# 		type_list = [thing for thing in list(tumor_controls.keys()) if \
		# 			thing not in normal_type_skip]
		# 	else:
		# 		type_list = list(tumor_controls.keys())
		# 	for this_type in type_list:
		# 		for item in tumor_controls.get(this_type):
		# 			normal_sample_load_list.append(item)
		# in_dir = list(glob.iglob(os.path.join(pipeline_dirs.get('raw_data_dir'), '*')))
		# list_load_files = []
		# for this_path in in_dir:
		# 	if os.path.isdir(this_path):
		# 		sub_paths = list(glob.iglob(os.path.join(this_path, '*')))
		# 		for sub_path in sub_paths:
		# 			if not os.path.isdir(sub_path) and data_info.get('file_patterns') \
		# 				in sub_path:
		# 				list_load_files.append(sub_path)
		# 	elif data_info.get('file_patterns') in this_path:
		# 		list_load_files.append(this_path)
		# for filepath in list_load_files:
		# 	if not os.path.isdir(filepath):
		# 		if filepath.split('/')[-1].split('_')[1] not in \
		# 			['A', 'B', 'C', 'D', 'E', 'F']:
		# 			raise ValueError('File '+filepath.split('/')[-1]+\
		# 				' does not have a valid mark label (aka A-F)') 
		# 		load_file = False
		# 		if pick_samples_to_load(filepath, data_info.get('which_mark')):
		# 			sample_type, sample_name = hfnxns.get_sample_type_and_id(filepath)
		# 			if non_bedname_groups is None:
		# 				save_spot = os.path.join(pipeline_dirs.get('loaded_data_dir'), \
		# 					filepath.split('/')[-1].split('.')[0]+'.bed')
		# 				if sample_type != 'E':
		# 					load_file = True
		# 				else:
		# 					if tumor_controls is not None:
		# 						if sample_name in normal_sample_load_list:
		# 							load_file = True
		# 					else:
		# 						load_file = True
		# 			else:
		# 				for this_type in list(non_bedname_groups.keys()):
		# 					if sample_name in non_bedname_groups.get(this_type):
		# 						save_spot = os.path.join(\
		# 							pipeline_dirs.get('loaded_data_dir'), \
		# 							this_type+'-'+'-'.join(filepath.split('/')[-1]\
		# 								.split('.')[0].split('-')[1:])+'.bed')
		# 						load_file = True
		# 			if load_file == True:
		# 				if not os.path.isfile(save_spot):
		# 					file_lines = []
		# 					with open(filepath, 'r') as textfile:
		# 						for line in textfile:
		# 							line = line.rstrip('\n').split('\t')
		# 							if 'chr' in line[data_info.get('chr_index')]:
		# 								if 'start' not in line[data_info.get('start_index')]:
		# 									if data_info.get('density_index') is not None:
		# 										file_lines.append(\
		# 											[line[data_info.get('chr_index')], \
		# 											line[data_info.get('start_index')], \
		# 											line[data_info.get('end_index')], \
		# 											line[data_info.get('density_index')]])
		# 									else:
		# 										file_lines.append(\
		# 											[line[data_info.get('chr_index')], \
		# 											line[data_info.get('start_index')], \
		# 											line[data_info.get('end_index')]])
		# 					file_lines = hfnxns.sort_bed_on_location(file_lines)
		# 					with open(save_spot, 'w') as textfile:
		# 						for line in file_lines:
		# 							print('\t'.join(line), file=textfile)

	# def pick_samples_to_load(filepath, which_mark):
		# load_file = False
		# sample_type, _ = hfnxns.get_sample_type_and_id(filepath)
		# if filepath.split('/')[-1].split('_')[1] == which_mark:
		# 	load_file = True
		# return load_file

	# def peak_size_kde(pipeline_dirs, save_dirs, directories, merge_sizes, \
		# color_dict, verbose=False):
		# print('Making KDE of peak size plot')
		# hfnxns.broad_cutoff(pipeline_dirs=pipeline_dirs, directories=directories, \
		# 	save_dirs=save_dirs, merge_sizes=merge_sizes, color_dict=color_dict, \
		# 	verbose=verbose)

	# def pipeline_file_split(settings, group, sample_groups):
		# if settings.get('analysis_settings').get('core_analysis')\
		# 	.get('analyses').get(group).get('split_type') == 'perc':
		# 	val_or_perc = 'perc'	
		# elif settings.get('analysis_settings').get('core_analysis')\
		# 	.get('analyses').get(group).get('split_type') == 'units':
		# 	val_or_perc = 'val'	
		# else:
		# 	raise ValueError('split_type must be "units" or "perc"')

		# for file_group in ['test', 'control']:
		# 	hfnxns.split_beds(
		# 		split_dir 		=	os.path.join(settings.get('save_dirs')\
		# 								.get('per_analysis')\
		# 								.get(
		# 								settings.get('analysis_settings')\
		# 								.get('core_analysis')\
		# 								.get('analyses')\
		# 								.get(group)\
		# 								.get('analysis_name'))\
		# 								.get('split_dir'), file_group),
		# 		merged_dir 		=	os.path.join(settings.get('save_dirs')\
		# 								.get('per_analysis')\
		# 								.get(settings.get('analysis_settings')\
		# 									.get('core_analysis')\
		# 									.get('analyses')\
		# 									.get(group)\
		# 									.get('analysis_name'))\
		# 								.get('top_merged_dir'), file_group),
		# 		file_pattern 	=	settings.get('analysis_settings')\
		# 								.get('core_analysis')\
		# 								.get('analyses')\
		# 								.get(group)\
		# 								.get('file_pattern'), 
		# 		len_or_dens		=	settings.get('analysis_settings')\
		# 								.get('core_analysis')\
		# 								.get('analyses')\
		# 								.get(group)\
		# 								.get('len_or_dens'), 
		# 		val_or_perc 	=	val_or_perc, 
		# 		cutoff 			=	settings.get('analysis_settings')\
		# 								.get('core_analysis')\
		# 								.get('analyses')\
		# 								.get(group)\
		# 								.get('cutoff'), 
		# 		greater_less 	=	settings.get('analysis_settings')\
		# 								.get('core_analysis')\
		# 								.get('analyses')\
		# 								.get(group)\
		# 								.get('greater_or_less'),
		# 		directories 	=	sample_groups
		# 		)
		


	# def choose_comparison(which_compare, list_settings):
		# which_lists = [None, None]
		# for group_loop in range(len(list_settings)):
		# 	for which_loop in range(len(which_compare)):
		# 		if list_settings[group_loop].get('analysis_name') == \
		# 			which_compare[which_loop]:
		# 			which_lists[which_loop] = group_loop
		# if list_settings[which_lists[0]].get('merge_len') == \
		# 	list_settings[which_lists[1]].get('merge_len'):
		# 	list_settings_compare = {'merge_len': \
		# 		list_settings[which_lists[0]].get('merge_len')}
		# else:
		# 	raise ValueError('Analyses '+str(which_lists[0])+' and '+\
		# 		str(which_lists[1])+', comparing analyses with different merge lengths')
		# return which_lists, list_settings_compare

	# def get_type_list(color_dict, tumor_controls, include_all):
		# d_values = list(color_dict.keys())
		# if include_all == True:
		# 	d_values.append('all')
		# normal_types = []
		# for this_key in list(tumor_controls.keys()):
		# 	cancer_type = this_key.split('_')[0]
		# 	if cancer_type in d_values:
		# 		normal_types.append(cancer_type)
		# return normal_types

	# def sort_gene_names(group_loop, group_names, save_dirs):
		# load_dir = os.path.join(save_dirs.get('len_short_tss_genes'), group_names[group_loop])
		# save_dir = os.path.join(save_dirs.get('sorted_tss_genes'), group_names[group_loop])
		# os.makedirs(save_dir, exist_ok=True)
		# sort_col = 5
		# file_spot = os.path.join(load_dir, '*'+'shortened_tss_gene_names.bed')
		# sort_1_group_gene_names(save_dir, file_spot, len_vs_short='shortened')
		# file_spot = os.path.join(load_dir, '*'+'lengthened_tss_gene_names.bed')
		# sort_1_group_gene_names(save_dir, file_spot, len_vs_short='lengthened')

	# def sort_1_group_gene_names(save_dir, load_dir, len_vs_short):
		# for filepath in glob.iglob(os.path.join(load_dir, '*'+len_vs_short+\
		# 	'_tss_gene_names.bed')):
		# 	peak_list = hfnxns.open_and_read(filepath, '\t')
		# 	this_dir = filepath.split('/')[-1].split('_')[0]
		# 	peak_list = [[peak_list[i][e] if e != sort_col else int(peak_list[i][e]) \
		# 		for e in range(len(peak_list[i]))] for i in range(len(peak_list))]
		# 	peak_list = sorted(peak_list, key=operator.itemgetter(sort_col))
		# 	peak_list = [[str(peak_list[i][e]) for e in range(len(peak_list[i]))] \
		# 		for i in range(len(peak_list))]
		# 	save_spot = os.path.join(save_dir, this_dir+'_'+len_vs_short+\
		# 		'_sorted_tss_gene.bed')
		# 	with open(save_spot, 'w') as textfile:
		# 		for line in peak_list:
		# 			print(''.join('{:<15s} '.format(x) for x in line), file=textfile)



































































































