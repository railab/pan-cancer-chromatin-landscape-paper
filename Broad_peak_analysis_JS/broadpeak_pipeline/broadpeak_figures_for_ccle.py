#%%
import time
import matplotlib.image as mpimg
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.font_manager import FontProperties
import colored_traceback
from termcolor import colored #color options: grey, red, green, yellow, blue, magenta, cyan, white
colored_traceback.add_hook()
from colorama import init 
from scipy.stats import chisquare
init() 
# print(colored('Start Script Here, Imported for: {}s ####################################################################################################'.format(
# 	round(time.time()-start_time, 2)), 'cyan'))
import matplotlib.pyplot as plt

from importlib import reload
# from broad_peak_everything import *
# from old_pipeline_seadragon.windows_compatible_pipeline import *
import broadpeak_helper_functions as hfnxns
# import chipseq_broadpeak_pipeline.broadpeak_helper_functions as hfnxns
# import list_helper_functions as hfnxns
import datetime
import matplotlib
import os
import glob
import json
import numpy as np
from matplotlib.lines import Line2D
from sklearn.neighbors import KernelDensity
from matplotlib import lines
from matplotlib.colors import LogNorm
from rpy2.robjects.packages import importr
from rpy2.robjects.vectors import FloatVector
from functools import partial
statsr = importr('stats')
import operator
# from windows_compatible_hfuns import mean


def show_interactive(fig):
	def onclick(coords, fig):
		size = fig.get_size_inches()*fig.dpi
	
		def click_coords(event):
			print('click spot:', round(event.x/size[0], 3), round(1-(event.y/size[1]), 3))
		return click_coords
		
	coords = []
	cid = fig.canvas.mpl_connect('button_press_event', onclick(coords, fig))
	plt.show()
	plt.close()



# for json printing variables for testing
# def quick_dumps(input_object, object_name=None, indent=4):
	# if object_name == None:
	# 	print(colored('\nQuick Dumps:', 'red'))
	# else:
	# 	print(colored('\nQuick Dumps: {}'.format(object_name), 'red'))
	# print(json.dumps(input_object, default=other_handler, indent=indent))

# def other_handler(input_object):
    # if isinstance(input_object, datetime.datetime):
    #     return input_object.isoformat()
    # return 'object type {}'.format(type(input_object))


# plotting helper functions
# for making 
def match_list_len(ref_list, other):
	if isinstance(other, list):
		if len(other) == len(ref_list):
			return other
		elif len(other) == 1:
			return [other[0] for ind in range(len(ref_list))]
		else:
			raise ValueError('Length of other list must either be 1 or match ref_list length. This was: {}'\
				.format(len(other)))
	else:
		return [other for ind in range(len(ref_list))]

# for labeling barplots at the top of the bar (grabs bar height then adds text right over)
# def autolabel_common(ax, rects, text_list=None):
	# """
	# Attach a text label above each bar displaying its height
	# """
	# #if more values, need smaller font size - for 12, fontsize 12 covers half: for 24, should cover all
	# #want font 24 for 12, want font 288 for 1
	# top_max = max([thing.get_height() for thing in rects])
	# for i in range(len(rects)):
	# 	width = rects[i].get_height()
	# 	xloc = width+(top_max*0.01) #+ 0.01
	# 	yloc = rects[i].get_x() + rects[i].get_width()/2.0
	# 	if text_list == None:
	# 		ax.text(yloc, xloc, str(int(width)), horizontalalignment='center')
	# 	else:
	# 		ax.text(yloc, xloc, str(text_list[i]), horizontalalignment='center')

# super proid of this. extension of make_panel_list: uses matplotlib subplot2grid (very flexible) to make
	# a list of subplots on a figure formatted as rows/columns 
	# (indexed like english reading: left->right, top->bottom)
	# this also allows variable distance b/w rows + b/w columns
	# this is an extension of make_panel_list because it allows columns to have different width
		# and rows to have different height (required for space saving for quartile plot)
# def variable_size_panel_list(gridsize, top_left, bottom_right, x_count, \
	# y_count, subpanel_pad_percent, panel_pad_percent = 0, img_shapes=None, \
	# vratio=None, hratio=None):
	# # issue with things shifting left: want everything to expend right by a ratio of:
	# 	# 1/(1-(% taken up by 1 in-between))
	# 	# so calculate size of 1 in-between and do totalx/1_between and multiply everything by that?
	# if vratio is None:
	# 	vratio = [1 for vpanel in range(y_count)]
	# else:
	# 	vratio = [vpanel/hfnxns.mean(vratio) for vpanel in vratio]
	# if hratio is None:
	# 	hratio = [1 for hpanel in range(x_count)]
	# else:
	# 	hratio = [vpanel/hfnxns.mean(hratio) for vpanel in hratio]

	# if isinstance(panel_pad_percent, tuple):
	# 	panel_pad_ratio_x = panel_pad_percent[0]/100
	# 	panel_pad_ratio_y = panel_pad_percent[1]/100
	# else:
	# 	panel_pad_ratio_x = panel_pad_percent/100
	# 	panel_pad_ratio_y = panel_pad_percent/100

	# if isinstance(subpanel_pad_percent, tuple):
	# 	subpanel_pad_ratio_x = subpanel_pad_percent[0]/100
	# 	subpanel_pad_ratio_y = subpanel_pad_percent[1]/100
	# else:
	# 	subpanel_pad_ratio_x = subpanel_pad_percent/100
	# 	subpanel_pad_ratio_y = subpanel_pad_percent/100
	# top_y = top_left[1]*gridsize[1]
	# left_x = top_left[0]*gridsize[0]
	# y_size = float((bottom_right[1]-top_left[1]))
	# x_size = float((bottom_right[0]-top_left[0]))
	# y_per_sub = float(y_size/y_count)
	# x_per_sub = float(x_size/x_count)
	# single_x_sub_pad = x_per_sub*subpanel_pad_ratio_x
	# single_y_sub_pad = y_per_sub*subpanel_pad_ratio_y
	# x_up_ratio = x_size / (x_size - single_x_sub_pad)
	# y_up_ratio = y_size / (y_size - single_y_sub_pad)


	# y_per_sub = int(y_per_sub * y_up_ratio * gridsize[1])
	# x_per_sub = int(x_per_sub * x_up_ratio * gridsize[0])
	# single_x_sub_pad = int(single_x_sub_pad * y_up_ratio * gridsize[1])
	# single_y_sub_pad = int(single_y_sub_pad * x_up_ratio * gridsize[0])

	# ax_list = []
	# ongoing_left_x = left_x
	# ongoing_top_y = top_y
	# for sub_loop in range(x_count*y_count):
	# 	x_spot = sub_loop % x_count
	# 	y_spot = int(sub_loop/x_count)
	# 	if x_spot == 0:
	# 		ongoing_left_x = left_x
	# 	if subpanel_pad_ratio_x > 0:
	# 		current_col_xpadding = single_x_sub_pad * x_spot
	# 	else:
	# 		current_col_xpadding = 0
	# 	x_per_sub_padded = int(x_per_sub*(1-subpanel_pad_ratio_x))
	# 	ax_x = int(left_x + x_per_sub*x_spot + current_col_xpadding)
	# 	if subpanel_pad_ratio_y > 0:
	# 		current_col_ypadding = single_y_sub_pad * y_spot
	# 	else:
	# 		current_col_ypadding = 0
	# 	y_per_sub_padded = int(y_per_sub*(1-subpanel_pad_ratio_y))
	# 	ax_y = int(top_y + y_per_sub*y_spot + current_col_ypadding)
	# 	this_x_per_sub = int(x_per_sub_padded * hratio[x_spot])
	# 	this_y_per_sub = int(y_per_sub_padded * vratio[y_spot])
	# 	if img_shapes == None:
	# 		ax_list.append(plt.subplot2grid(gridsize, (int(ongoing_top_y), int(ongoing_left_x)), \
	# 			rowspan=this_y_per_sub, colspan=this_x_per_sub))
	# 	else:
	# 		img_y = img_shapes[sub_loop][0]
	# 		img_x = img_shapes[sub_loop][1]
	# 		img_ratio = img_y/img_x
	# 		panel_ratio = this_y_per_sub/this_x_per_sub
	# 		ax_list.append(plt.subplot2grid(gridsize, (ongoing_top_y, ongoing_left_x), \
	# 			rowspan=int(this_y_per_sub/(panel_ratio/img_ratio)), \
	# 			colspan=this_x_per_sub))
	# 	ongoing_left_x += (this_x_per_sub + subpanel_pad_ratio_x*x_per_sub)
	# 	if x_spot % x_count == x_count-1:
	# 		ongoing_top_y += (this_y_per_sub + subpanel_pad_ratio_y*y_per_sub)

	# return ax_list


# calculating significant differences for cosmic/tuson plots
# def chisquare_prep(total_counts, loop_count, gene_type_count, val_names=None):
	# overall_counts = []
	# for thing in total_counts:
	# 	mini_list = thing[:-1]
	# 	mini_list.append(thing[-1]-sum(thing[:-1]))
	# 	overall_counts.append(mini_list)
	# loop_num = 1
	# overall_vals = []
	# loop_list = []
	# while loop_num <= len(overall_counts):
	# 	if loop_num % loop_count != 0:
	# 		loop_list.append(overall_counts[loop_num-1])
	# 	else:
	# 		for item_loop, item in enumerate(loop_list):
	# 			if val_names is not None:
	# 				overall_vals.append([item, [round(thing*(item[gene_type_count]/\
	# 					overall_counts[loop_num-1][gene_type_count]),1) \
	# 					for thing in overall_counts[loop_num-1]], val_names[item_loop]])
	# 			else:
	# 				overall_vals.append([item, [round(thing*(item[gene_type_count]/\
	# 					overall_counts[loop_num-1][gene_type_count]),1) \
	# 					for thing in overall_counts[loop_num-1]]])
	# 		loop_list = []
	# 	loop_num += 1
	# return overall_vals

# def full_sig_list(data_location, loop_count, gene_type_count):
	# with open(data_location,'r') as textfile:
	# 	len_change_cosmic_data = json.load(textfile)

	# total_counts = [[len(stuff) for stuff in thing] for thing in \
	# 	len_change_cosmic_data.get('peak_list_to_plot')]
	# prepped_list = chisquare_prep(total_counts, loop_count, gene_type_count)
	# pvals = []
	# for thing in prepped_list:
	# 	for item_loop in range(len(thing[0]) - 1):
	# 		to_use = [thing[0][item_loop], thing[0][-1]], [thing[1][item_loop], thing[1][-1]]
	# 		pvals.append(chisquare(*to_use).pvalue)

	# qvals = statsr.p_adjust(FloatVector(pvals), method = 'BH')
	# new_qvals = list(qvals).copy()
	# new_pvals = pvals.copy()
	# grouped_qvals = []
	# grouped_pvals = []
	# while len(new_qvals) > 0:
	# 	grouped_qvals.append(new_qvals[:gene_type_count])
	# 	grouped_pvals.append(new_pvals[:gene_type_count])
	# 	for i in range(gene_type_count):
	# 		del new_qvals[0]
	# 		del new_pvals[0]

	# sig_list = []
	# for thing_loop, thing in enumerate(prepped_list):
	# 	sig_list.append(['sig' if stuff < 0.05 else '' for stuff in grouped_qvals[thing_loop]])

	# return sig_list


# matplotlib wrapper class to simplify making complex plots
	# big advantages are that .add_panel method calls variable_size_panel_list and adds the panel
		# to an internally stored list of subplots (can call once for regular plots, many x for figures)
	# .format_panel method should simplify code for a number of advanced formatting procedures
		# that took a lot of time to figure out and don't have intuitive interfaces
	# can also call more common formatting through format_panel by passing dicts that are used as kwargs
	# can call format_panel multiple times for a single panel if needed
	# .add_text method simply reverses the ypos argument: ypos=(1-ypos) to that 0 is top and 1 is bottom
		# that's more intuitive for me and fits .add_panel ypos convention
# class easy_fig(object):
	# # figsize is passes directly to plt.figure(figsize=)
	# # subplot edge values are passed directly to subplots_adjust
	# # gridsize used for subplots2grid: increasing increases plot placement granularity
	# # .axes attribute is a list holding all subplots. can grab them by index
	# 	# appended every time .add_panel is called
	# def __init__(self, figsize=(12,8), gridsize=(1000,1000), \
	# 	subplot_left=0.05, subplot_right=0.95, subplot_bottom=0.10, \
	# 	subplot_top=0.90):
	# 	self.gridsize = gridsize
	# 	self.figsize = figsize
	# 	self.fig = plt.figure(figsize=figsize)
	# 	plt.subplots_adjust(left=subplot_left, right=subplot_right, \
	# 		bottom=subplot_bottom, top=subplot_top)
	# 	self.axes = []

	# # all args passed directly to variable_size_panel_list, output subplot list is appended to .axes list
	# def add_panel(self,top_left=(0,0), bottom_right=(1,1), \
	# 	x_count=1, y_count=1, subpanel_pad_percent=(0,0), \
	# 	panel_pad_percent = 0, img_shapes=None, vratio=None, hratio=None):

	# 	self.axes.append(variable_size_panel_list(self.gridsize, top_left, \
	# 		bottom_right,x_count,y_count,subpanel_pad_percent, \
	# 		panel_pad_percent, img_shapes, vratio, hratio))

	# # just adds text to the figure. reverses ypos: ypos=(1-ypos) so 0 is top and 1 is bottom
	# 	# because that fits my expectation better
	# # can supply panelnum/axnum indices to add this to a specific panel, defaults to 0, 0
	# # i typically pass transform=fig.transFigure in kwargs for figures so that the positions
	# 	# are based on the position in the entire figure rather than for a specific panel/subplot
	# def add_text(self, xpos, ypos, *args, panelnum=None, \
	# 	axnum=None, **kwargs):
	# 	if panelnum is None:
	# 		panelnum=0
	# 	if axnum is None:
	# 		axnum=0
	# 	self.axes[panelnum][axnum].text(xpos, 1-ypos, *args, **kwargs)

	# # very light instance method wrapper of format_panel. Can call this on specific easy_fig object and pass
	# 	# panelnum + axnum to format a specific subplot of that figure
	# def format_plot(self, panelnum, axnum, *args, **kwargs):
	# 	easy_fig.format_panel(self.axes[panelnum][axnum], *args, **kwargs)
	
	# # this doesn't need to be a method but i prefer that organization.
	# 	# takes a subplot and formats it based on arguments
	# # 2 main kinds of formatting:
	# 	# 1. simplifying complex formatting that i spent time to figure out and don't want to remember:
	# 		# eg. adding highly flexible legends
	# 			# splitting an ax to add a colorbar as in the 2d histograms
	# 		# using these is highly recommended if you're doing this formatting.
	# 			# it will save a lot of googling and reading matplotlib documentation
	# 	# 2. simple formatting (adding title, set_xlim):
	# 		# done by passing a dict that is passed as **kwargs to the formatting method
	# 		# allows formatting a bunch of features of a plot in 1 method call. up to personal preference
	# 		# whether or not you use it
	# # description of arguments:
	# 	# group 1:
	# 		# hide_panel: calls .axis('off'), used to remove all formatting for a subplot
	# 			# useful to hide formatting for imshow, or if a subplot needs to be completely hidden
	# 			# set to True if needed
	# 		# xlog: calls.set_xscale('log') to set x axis to logscale
	# 			# set to True if needed
	# 		# ylog: same as xlog but for y axis
	# 		# xlim: calls .set_xlim(xlim) to set x axis limits. takes 2 int/float tuple/list
	# 		# ylim: same as xlim but for y axis
	# 		# hide_xaxis: calls .get_xaxis().set_visible(False) to hide xticks + xticklabels on a subplot
	# 			# used to make x axis of a subplot look simple/pretty
	# 			# set to True if needed
	# 		# hide_yaxis: same as hide_xaxis but for y axis
	# 		# hide_xticks: calls panel.tick_params(axis=u'x', which=u'both',length=0)
	# 			# sets length of xticks to 0 to hide the ticks 
	# 			# important: allows you to keep xticklabels over invisible ticks
	# 			# set to True if needed
	# 		# hide_yticks: same as hide_xticks but for y axis
	# 		# hide_xticklabels: calls .set_xticklabels([]) to remove xtick_labels
	# 			# set to True if needed
	# 		# hide_yticklabels: same as hide_xticklabels but for y axis
	# 		# invert_x: calls .invert_xaxis() to reverse everything in a subplot horizontally
	# 			# be careful, i think it reverses xlim values in a weird way, so .get_xlim might not
	# 				# work intuitively after using this
	# 			# set to True if needed
	# 		# invert_y: same as invert_x but for y axis
	# 			# i use this more than invert_x, specifically for plots like path dots or quantile plot
	# 				# where y axis relationships are basically arbitrary, but reversing the order 
	# 					# looks better
	# 		# xtick_top: calls .xaxis.tick_top() to put xticks on the top of the subplot
	# 			# good if the bottom of the plot is busy
	# 			# set to True if needed
	# 		# ytick_right: same as xtick_top but for y ticks 
	# 			# (puts them on the right side rather than top for obvious reasons)
	# 		# legend_dict: used to add a legend to plot more flexibly than matlotlib normally allows
	# 			# has 2 parts (in the 1 dict): 1. setting legends values, 2. formatting the legend
	# 			# setting legends values calls Line2D once for each for each item in the legend_value list
	# 				# must have a legend_value list (name for each legend element)
	# 				# can include any arg that Line2D takes:
	# 					# can either be datatype accepted by Line2D or a list of same len as legend_values
	# 					# single value will be repeated for all elements, a list will pass a specific
	# 					# value for each element
	# 			# formatting legend:
	# 				# can pass all arguments accepted by .legend method (passed as **kwargs)
	# 				# for loc + bbox_to_anchor: i don't find these very intuitive, so
	# 					# you can either pass these directly or pass my custom arguments
	# 						# loc + bbox_to_anchor will over-ride custom arguments
	# 				# custom args:
	# 					# whichcorner: says which corner of the subplot to set the legend in relation to
	# 						# eg, "upper right" -> by default, upper right corner of legend will touch
	# 							# upper right corner of plot (minus padding from borderaxespad kwarg)
	# 						# if houtside is True: upper left corner will touch plot upper right corner
	# 							# from outside
	# 						# if voutside if True: lower right corner will touch plot upper right corner
	# 							# from outside
	# 					# hdistance: how far horizontally from default position to go?
	# 						# defaults to 0. negative -> moves left, positive -> moves right
	# 					# vdistance: how far vertically from default position to go
	# 						# defaults to 0. negative -> moves down, positive -> moves up
	# 					# maxwidth: equivalent to 3rd value in bbox_to_anchor:
	# 						# sets max width of legend relative to plot (1 = covers whole plot horizontally)
	# 					# maxheight: equivalent to 4rd value in bbox_to_anchor:
	# 						# sets max height of legend relative to plot (1 = covers whole plot vertically)
	# 					# houtside: if True, legend will be outside plot horizontally
	# 						# some examples given in whichcorner section
	# 						# by default, a corner of the legend will touch a corner of the plot 
	# 							# (minus padding)
	# 							# as such (info for advanced users), 
	# 								# this actually switches the horizontal loc value
	# 					# voutside: if True, legend will be outside plot vertically
	# 						# some examples given in whichcorner section
	# 						# by default, a corner of the legend will touch a corner of the plot 
	# 							# (minus padding)
	# 							# as such (info for advanced users), 
	# 								# this actually switches the vertical loc value
	# 		# plot_divider_dict: used to divide a subplot for some purpose (as in matplotlib "cax" examples)
	# 			# important: this will return a cax object for further formatting
	# 				# this cax object is not stored anywhere in the easy_fig object, so keep track
	# 				# of it if you want to continue using/formatting the mini subplot
	# 				# couldn't figure out an intuitive place to save the reference in the easy_fig object...
	# 			# can either accept a previously produces "cax" object for formatting, or and append_axes
	# 				# dict that will be used as **kwargs for .append_axes in this pattern:
	# 					# divider = make_axes_locatable(ax)
	# 					# cax = divider.append_axes(**plot_divider_dict.get('append_axes'))
	# 				# as such, append_axes sub-dict can have all args accepted by append_axes method
	# 			# can accept colorbar sub-dict that is passed as **kwargs to plt.colorbar(cax, **kwargs) 
	# 				# method
	# 				# used for addings a colorbar in the cax as in the hist2d
	# 				# can include all arguments accepted by the plt.colorbar method
	# 			# plot_divider_dict can also have many of the same formatting arguments that the rest of
	# 				# the function accepts:
	# 					# colorbar, tick_param_dict, hide_xaxis, hide_yaxis, hide_xticks, 
	# 						# xtick_dict, hide_yticks, ytick_dict, hide_xticklabels, xticklabel_dict, 
	# 						# hide_yticklabels, yticklabel_dict, xlog, xlim, ylog, ylim
	# 				# each of these function the same as they do for the main function, but they
	# 					# will be formatting the cax
	# 	# group 2:
	# 		# these dicts are passed as **kwargs to specific formatting methods, as described below:
	# 			# title_dict: 			.set_title()
	# 			# ylabel_dict: 			.set_ylabel()
	# 			# xlabel_dict: 			.set_xlabel()
	# 			# xtick_dict: 			.set_xticks()
	# 			# xticklabel_dict: 		.set_xticklabels()
	# 			# ytick_dict: 			.set_yticks()
	# 			# yticklabel_dict: 		.set_yticklabels()
	# 			# vline_dict: 			.axvline()
	# 			# hline_dict: 			.axhline()
	# 			# tick_param_dict: 		.tick_params()
	# def format_panel(panel, hide_panel=False, \
	# 	xlog=False, ylog=False, xlim=None, ylim=None, hide_xaxis=False, \
	# 	hide_yaxis=False, hide_xticks=False, hide_yticks=False, \
	# 	hide_xticklabels=False, hide_yticklabels=False, invert_y=False, \
	# 	invert_x=False, xtick_top=False, ytick_right=False, legend_dict=None, \
	# 	plot_divider_dict=None, title_dict=None, ylabel_dict=None, xlabel_dict=None, \
	# 	xtick_dict=None, xticklabel_dict=None, ytick_dict=None, yticklabel_dict=None, \
	# 	vline_dict=None, hline_dict=None, tick_param_dict=None):

	# 	def format_legend_args(arg, ind):
	# 		if isinstance(arg, list):
	# 			return_arg = arg[ind]
	# 		else:
	# 			return_arg = arg
	# 		return return_arg

	# 	if title_dict is not None:
	# 		panel.set_title(**title_dict)
	# 	if ylabel_dict is not None:
	# 		panel.set_ylabel(**ylabel_dict)
	# 	if xlabel_dict is not None:
	# 		panel.set_xlabel(**xlabel_dict)
	# 	if vline_dict is not None:
	# 		if isinstance(vline_dict, dict):
	# 			panel.axvline(**vline_dict)
	# 		else:
	# 			for this_dict in vline_dict:
	# 				panel.axvline(**this_dict)
	# 	if hline_dict is not None:
	# 		panel.axhline(**hline_dict)
	# 	if tick_param_dict is not None:
	# 		panel.tick_params(**tick_param_dict)
		
	# 	if hide_panel == True:
	# 		panel.axis('off')
	# 	if xlog == True:
	# 		panel.set_xscale('log')
	# 	if xlim not in [None, False]:
	# 		panel.set_xlim(xlim)
	# 	if ylog == True:
	# 		panel.set_yscale('log')
	# 	if ylim not in [None, False]:
	# 		panel.set_ylim(ylim)

	# 	if hide_xaxis:
	# 		panel.get_xaxis().set_visible(False)
	# 	if hide_yaxis:
	# 		panel.get_yaxis().set_visible(False)

	# 	if hide_xticks:
	# 		panel.tick_params(axis=u'x', \
	# 			which=u'both',length=0)
	# 	elif xtick_dict is not None:
	# 		panel.set_xticks(**xtick_dict)
			
	# 	if hide_yticks:
	# 		panel.tick_params(axis=u'y', \
	# 			which=u'both',length=0)
	# 	elif ytick_dict is not None:
	# 		panel.set_yticks(**ytick_dict)

	# 	if hide_xticklabels:
	# 		panel.set_xticklabels([])		
	# 	elif xticklabel_dict is not None:	
	# 		panel\
	# 			.set_xticklabels(**xticklabel_dict)
		
	# 	if hide_yticklabels:
	# 		panel.set_yticklabels([])	
	# 	elif yticklabel_dict is not None:
	# 			panel\
	# 				.set_yticklabels(**yticklabel_dict)

	# 	if invert_y is not False:
	# 		panel.invert_yaxis()
	# 	if invert_x is not False:
	# 		panel.invert_xaxis()

	# 	if xtick_top is not False:
	# 		panel.xaxis.tick_top()

	# 	if ytick_right is not False:
	# 		panel.yaxis.tick_right()


	# 	if legend_dict is not None:
	# 		if legend_dict.get('legend_values') is None:
	# 			raise ValueError(\
	# 				'For easy_fig formatting, if legend_dict is supplied, it must have "legend_values" in the dict')
	# 		if legend_dict.get('whichcorner') is None:
	# 			legend_dict['whichcorner'] = 'upper right'
	# 		if legend_dict.get('hdistance') is None:
	# 			legend_dict['hdistance'] = 0
	# 		if legend_dict.get('vdistance') is None:
	# 			legend_dict['vdistance'] = 0
	# 		if legend_dict.get('maxwidth') is None:
	# 			legend_dict['maxwidth'] = 0.5
	# 		if legend_dict.get('maxheight') is None:
	# 			legend_dict['maxheight'] = 0.5

	# 		l2d_args = [
	# 			'linewidth', 'lw', 'linestyle', 'color', 'marker', 'markersize', \
	# 			'markeredgewidth', 'markeredgecolor', 'markerfacecolor', \
	# 			'markerfacecoloralt', 'fillstyle', 'antialiased', \
	# 			'dash_capstyle', 'solid_capstyle', 'dash_joinstyle', \
	# 			'solid_joinstyle', 'pickradius', 'drawstyle', 'markevery'
	# 		]

	# 		legend_elements = []
	# 		for val_loop, value in \
	# 			enumerate(legend_dict.get('legend_values')):

	# 			elements_dict = {}
	# 			for this_arg in l2d_args:
	# 				this_val = format_legend_args(legend_dict.get(this_arg), val_loop)
	# 				if this_val is not None:
	# 					elements_dict[this_arg] = this_val
	# 			legend_elements.append(Line2D([0], [0], label=str(value),\
	# 				**elements_dict))

	# 		accepted_whichcorner = ['upper left', 'upper right', \
	# 			'lower left', 'lower right', 'center']
	# 		if legend_dict.get('whichcorner') not in accepted_whichcorner:
	# 			raise ValueError('whichcorner must be in {}. Was {}'\
	# 				.format(accepted_whichcorner, legend_dict.get('whichcorner')))
	# 		if legend_dict.get('whichcorner') == 'center':
	# 			legend_dict['hdistance'] += (0.5 - \
	# 				legend_dict.get('maxwidth')/2)
	# 			legend_dict['vdistance'] += (0.5 - \
	# 				legend_dict.get('maxheight')/2)
	# 		else:
				
	# 			if legend_dict.get('houtside') and 'left' in \
	# 				legend_dict.get('whichcorner'):
	# 				legend_dict['whichcorner'] = \
	# 					legend_dict['whichcorner'].replace('left', 'right')
	# 				legend_dict['hdistance'] += (0 - 0)
				
	# 			elif not legend_dict.get('houtside') and 'left' in \
	# 				legend_dict.get('whichcorner'):
	# 				legend_dict['hdistance'] += (0 - \
	# 					legend_dict.get('maxwidth'))
				
	# 			elif legend_dict.get('houtside') and 'right' in \
	# 				legend_dict.get('whichcorner'):
	# 				legend_dict['whichcorner'] = \
	# 					legend_dict['whichcorner'].replace('right', 'left')
	# 				legend_dict['hdistance'] += (1 - 0)
				
	# 			elif not legend_dict.get('houtside') and 'right' in \
	# 				legend_dict.get('whichcorner'):
	# 				legend_dict['hdistance'] += (1 - \
	# 					legend_dict.get('maxwidth'))
				
				
	# 			if legend_dict.get('voutside') and 'upper' in \
	# 				legend_dict.get('whichcorner'):
	# 				legend_dict['whichcorner'] = \
	# 					legend_dict['whichcorner'].replace('upper', 'lower')
	# 				legend_dict['vdistance'] += (1 - 0)
				
	# 			elif not legend_dict.get('voutside') and 'upper' in \
	# 				legend_dict.get('whichcorner'):
	# 				legend_dict['vdistance'] += (1 - \
	# 					legend_dict.get('maxheight'))
				
	# 			elif legend_dict.get('voutside') and 'lower' in \
	# 				legend_dict.get('whichcorner'):
	# 				legend_dict['whichcorner'] = \
	# 					legend_dict['whichcorner'].replace('lower', 'upper')
	# 				legend_dict['vdistance'] += (0 - 0)
				
	# 			elif not legend_dict.get('voutside') and 'lower' in \
	# 				legend_dict.get('whichcorner'):
	# 				legend_dict['vdistance'] += (0 - \
	# 					legend_dict.get('maxheight'))
			
	# 		if legend_dict.get('loc') is None:
	# 			legend_dict['loc'] = legend_dict.get('whichcorner')

	# 		if legend_dict.get('bbox_to_anchor') is None:
	# 			legend_dict['bbox_to_anchor'] = \
	# 				[
	# 					legend_dict.get('hdistance'), 	\
	# 					legend_dict.get('vdistance'), 	\
	# 					legend_dict.get('maxwidth'), 	\
	# 					legend_dict.get('maxheight'), 	\
	# 				]

	# 		if legend_dict.get('handles') is None:
	# 			legend_dict['handles'] = legend_elements

	# 		if legend_dict.get('legend_values') is not None:
	# 			del legend_dict['legend_values']
	# 		if legend_dict.get('hdistance') is not None:
	# 			del legend_dict['hdistance']
	# 		if legend_dict.get('vdistance') is not None:
	# 			del legend_dict['vdistance']
	# 		if legend_dict.get('maxwidth') is not None:
	# 			del legend_dict['maxwidth']
	# 		if legend_dict.get('maxheight') is not None:
	# 			del legend_dict['maxheight']
	# 		if legend_dict.get('whichcorner') is not None:
	# 			del legend_dict['whichcorner']
	# 		if legend_dict.get('houtside') is not None:
	# 			del legend_dict['houtside']
	# 		if legend_dict.get('voutside') is not None:
	# 			del legend_dict['voutside']
	# 		for this_arg in l2d_args:
	# 			if legend_dict.get(this_arg) is not None:
	# 				del legend_dict[this_arg]

	# 		panel.legend(**legend_dict)


	# 	if plot_divider_dict is not None:
	# 		if plot_divider_dict.get('append_axes') is not None and plot_divider_dict.get('cax') \
	# 			is not None:
	# 			raise ValueError(\
	# 					'plot_divider_dict for format_panel contains "cax" and "append_axes" keys.\
	# 				\nappend_axes replaces cax, so please supply only one')
	# 		elif plot_divider_dict.get('append_axes') is None and plot_divider_dict.get('cax') is None:
	# 			raise ValueError(\
	# 				'plot_divider_dict for format_panel must have either "cax" or "append_axes" to\
	# 				\nprovide or produce (respectively) a divider for formatting')
	# 		elif plot_divider_dict.get('append_axes') is not None:
	# 			divider = make_axes_locatable(panel)
	# 			plot_divider_dict['cax'] = divider.append_axes(**plot_divider_dict.get('append_axes'))
			

	# 		if plot_divider_dict.get('colorbar') is not None:
	# 			plt.colorbar(cax=plot_divider_dict.get('cax'), **plot_divider_dict.get('colorbar'))

	# 		if plot_divider_dict.get('tick_param_dict') is not None:
	# 			plot_divider_dict['cax'].tick_params(**plot_divider_dict.get('tick_param_dict'))

	# 		if plot_divider_dict.get('hide_xaxis'):
	# 			plot_divider_dict['cax'].get_xaxis().set_visible(False)
	# 		if plot_divider_dict.get('hide_yaxis'):
	# 			plot_divider_dict['cax'].get_yaxis().set_visible(False)

	# 		if plot_divider_dict.get('hide_xticks'):
	# 			plot_divider_dict['cax'].tick_params(axis=u'x', \
	# 				which=u'both',length=0)
	# 		elif plot_divider_dict.get('xtick_dict') is not None:
	# 			plot_divider_dict['cax'].set_xticks(**plot_divider_dict.get('xtick_dict'))
				
	# 		if plot_divider_dict.get('hide_yticks'):
	# 			plot_divider_dict['cax'].tick_params(axis=u'y', \
	# 				which=u'both',length=0)
	# 		elif plot_divider_dict.get('ytick_dict') is not None:
	# 			plot_divider_dict['cax'].set_yticks(**plot_divider_dict.get('ytick_dict'))

	# 		if plot_divider_dict.get('hide_xticklabels'):
	# 			plot_divider_dict['cax'].set_xticklabels([])		
	# 		elif plot_divider_dict.get('xticklabel_dict') is not None:	
	# 			plot_divider_dict['cax']\
	# 				.set_xticklabels(**plot_divider_dict.get('xticklabel_dict'))
			
	# 		if plot_divider_dict.get('hide_yticklabels'):
	# 			plot_divider_dict['cax'].set_yticklabels([])	
	# 		elif plot_divider_dict.get('yticklabel_dict') is not None:
	# 				plot_divider_dict['cax']\
	# 					.set_yticklabels(**plot_divider_dict.get('yticklabel_dict'))

	# 		if plot_divider_dict.get('xlog') == True:
	# 			plot_divider_dict['cax'].set_xscale('log')
	# 		if plot_divider_dict.get('xlim') not in [None, False]:
	# 			plot_divider_dict['cax'].set_xlim(plot_divider_dict.get('xlim'))
	# 		if plot_divider_dict.get('ylog') == True:
	# 			plot_divider_dict['cax'].set_yscale('log')
	# 		if plot_divider_dict.get('ylim') not in [None, False]:
	# 			plot_divider_dict['cax'].set_ylim(plot_divider_dict.get('ylim'))

	# 		return plot_divider_dict.get('cax')

def format_files_groupnames(input_list, pipeline_settings, name_ind_list, drop_ind_list, 
	change_ind_list, barplot_drop_test=True, barplot_drop_control=True, 
	barplot_remove_test_group_name=False, replace_str='{}_', 
	change_str='change_ind', drop_str='drop_ind', name_str='name_ind', debug=False):
	
	# print(len(input_list))
	all_list, all_not_list = all([isinstance(val, list) or isinstance(val, tuple) for val in change_ind_list]), \
		all([not isinstance(val, list) and not isinstance(val, tuple) for val in change_ind_list])
	if all_list:
		change_lists = True
	else:
		change_lists = False
	if not (not all_list or not all_not_list):
		# print(all_list, all_not_list)
		# print(not all_list, not all_not_list)
		# print(not all_list or not all_not_list)
		# print([isinstance(val, list) or isinstance(val, tuple) for val in change_ind_list])
		raise ValueError('All values in change_ind_list must be list/tuple or all must not be.')

	all_list, all_not_list = all([isinstance(val, list) or isinstance(val, tuple) for val in drop_ind_list]), \
		all([not isinstance(val, list) and not isinstance(val, tuple) for val in drop_ind_list])
	if all_list:
		drop_lists = True
	else:
		drop_lists = False
	if not (not all_list or not all_not_list):
		raise ValueError('All values in drop_ind_list must be list/tuple or all must not be.')

	if not change_lists:
		num_change_str = [val == change_str for val in change_ind_list]
		if sum(num_change_str) != 1:
			raise ValueError('change_str "{}" should only occur in change_ind_list should only\
				\n\t1x, occurred {}x. Can supply different change_str in args'
					.format(change_str, num_change_str))
	else:
		for change_loop, change_list in enumerate(change_ind_list):
			num_change_str = [val == change_str for val in change_list]
			if sum(num_change_str) != 1:
				raise ValueError('change_str "{}" should only occur in change_ind_list[{}] should only\
					\n\t 1x, occurred {}x. Can supply different change_str in args'
						.format(change_str, change_loop, num_change_str))

	if not drop_lists:
		num_drop_str = [val == drop_str for val in drop_ind_list]
		if sum(num_drop_str) != 1:
			raise ValueError('drop_str "{}" should only occur in drop_ind_list should only\
				\n\t1x, occurred {}x. Can supply different drop_str in args'
					.format(drop_str, num_drop_str))
	else:
		for drop_loop, drop_list in enumerate(drop_ind_list):
			num_drop_str = [val == drop_str for val in drop_list]
			if sum(num_drop_str) != 1:
				raise ValueError('drop_str "{}" should only occur in[{}] drop_ind_list should only\
					\n\t 1x, occurred {}x. Can supply different drop_str in args'
						.format(drop_str, drop_loop, num_drop_str))

	num_name_str = [val == name_str for val in name_ind_list]
	if sum(num_name_str) > 1:
		raise ValueError('name_str "{}" should only occur in name_ind_list should only\
			\n\t0-1x, occurred {}x. Can supply different name_str in args'
				.format(name_str, num_name_str))
	elif sum(num_name_str) == 1:
		grab_name_list = True
	elif sum(num_name_str) == 0:
		grab_name_list = False

	# obj, key_ind_list, fnxn=None, new_val=None, pop_val=False
	# grab_fnxn = partial(hfnxns.update_by_key_ind_list, {'obj': input_list, 'key_ind_list': key_ind_list})
	# print('key_ind_list: {}'.format(key_ind_list))
	# print('key_ind_list: {}'.format(input_list))
	# grab_fnxn = partial(hfnxns.update_by_key_ind_list, obj = input_list, key_ind_list = name_ind_list)
	# grab_fnxn = partial(hfnxns.update_by_key_ind_list, input_list, key_ind_list)
	# barplot_drop_test = False
	# barplot_drop_control = True
	# barplot_remove_test_group_name = True
	# hfnxns.quick_dumps(input_list, 'input_list')
	# hfnxns.quick_dumps(input_list[0], 'input_list[0]')
	# raise
	if grab_name_list:
		data_types = []
		for ind, this_item in enumerate(input_list):
			# print(colored(str(ind),'red'), this_item)
			# print(1)
			this_name_list = \
				[name_ind if name_ind != name_str else ind for name_ind in name_ind_list]
			# print(len(input_list), ind, this_name_list, this_item)
			# print(colored(str([(my_ind, input_list[my_ind].get('sample_groups')) for my_ind in range(len(input_list))]), 'white'))
			data_types.append(hfnxns.update_by_key_ind_list(input_list, this_name_list))
			# print(2)
	else:
		# print(3)
		data_types = hfnxns.update_by_key_ind_list(input_list, name_ind_list)
	# data_types = grab_fnxn()
	# data_types = grab_fnxn(input_list)
	# data_types = [grab_fnxn(this_dict) for this_dict in input_list]
	# print(4)
	if barplot_drop_control:
		# print(5)
		drop_inds = [type_loop for type_loop in range(len(data_types)) if
			check_test_control(data_types[type_loop], pipeline_settings.get('test_group_name'), 
				pipeline_settings.get('control_group_name')) == 'control']
		drop_inds.reverse()
		# print(drop_inds)
		for ind in drop_inds:
			if not drop_lists:
				# print(6)
				this_drop_list = \
					[drop_ind if drop_ind != drop_str else ind for drop_ind in drop_ind_list]
				# print(this_drop_list, len(input_list))
				# print('this_drop_list:', this_drop_list)
				# changed this - # drop/pop ind must be the last ind in the list because i can't do
					# anything with the objects lower in the nest
				# print('will run non-loop')
				hfnxns.update_by_key_ind_list(input_list, this_drop_list, pop_val = True)
			else:
				# print(7)
				for drop_list in drop_ind_list:
					this_drop_list = \
						[drop_ind if drop_ind != drop_str else ind for drop_ind in drop_list]
					# print(8)
					# print('this_drop_list list:', drop_list)
					# print('will run loop')
					hfnxns.update_by_key_ind_list(input_list, this_drop_list, pop_val = True)
	elif barplot_remove_test_group_name:
		# print(11)
		change_inds = [type_loop for type_loop in range(len(data_types)) if
			check_test_control(data_types[type_loop], pipeline_settings.get('test_group_name'), 
				pipeline_settings.get('control_group_name')) == 'control']
		for ind in change_inds:
			if not change_lists:
				this_change_list = \
					[change_ind if change_ind != change_str else ind for change_ind in change_ind_list]
				# changed this - # change/pop ind must be the last ind in the list because i can't do
					# anything with the objects lower in the nest
				hfnxns.update_by_key_ind_list(input_list, this_change_list, 
					fnxn = lambda x: x.replace(replace_str
						.format(pipeline_settings.get('control_group_name')),''))
			else:
				for change_ind, change_list in enumerate(change_ind_list):
					if isinstance(replace_str, list) or isinstance(replace_str, tuple):
						use_str = replace_str[change_ind]
					else:
						use_str = replace_str
					this_change_list = \
						[change_ind if change_ind != change_str else ind for change_ind in change_list]
					hfnxns.update_by_key_ind_list(input_list, this_change_list, 
						fnxn = lambda x: x.replace(use_str
							.format(pipeline_settings.get('control_group_name')),''))

	# print(len(input_list), colored(str([(my_ind, input_list[my_ind].get('sample_groups')) for my_ind in range(len(input_list))]), 'white'))
	# for this_key in input_list[0].keys():
	# 	print(this_key, [len(input_list[my_ind].get(this_key)) for my_ind in range(len(input_list))])
	# 	print('')
	# raise
	# print('len input list, 835: ', len(input_list))
	if grab_name_list:
		data_types = []
		for ind, this_item in enumerate(input_list):
			this_name_list = \
				[name_ind if name_ind != name_str else ind for name_ind in name_ind_list]
			# print(this_name_list)
			data_types.append(hfnxns.update_by_key_ind_list(input_list, this_name_list))
	else:
		data_types = hfnxns.update_by_key_ind_list(input_list, name_ind_list)

	# print(12)
	# data_types = grab_fnxn()
	if barplot_drop_test:
		drop_inds = [type_loop for type_loop in range(len(data_types)) if
			check_test_control(data_types[type_loop], pipeline_settings.get('test_group_name'), 
				pipeline_settings.get('control_group_name')) == 'test']
		drop_inds.reverse()
		for ind in drop_inds:
			if not drop_lists:
				this_drop_list = \
					[drop_ind if drop_ind != drop_str else ind for drop_ind in drop_ind_list]
				# changed this - # drop/pop ind must be the last ind in the list because i can't do
					# anything with the objects lower in the nest
				hfnxns.update_by_key_ind_list(input_list, this_drop_list, pop_val = True)
			else:
				for drop_list in drop_ind_list:
					this_drop_list = \
						[drop_ind if drop_ind != drop_str else ind for drop_ind in drop_list]
					hfnxns.update_by_key_ind_list(input_list, this_drop_list, pop_val = True)
	elif barplot_remove_test_group_name:
		change_inds = [type_loop for type_loop in range(len(data_types)) if
			check_test_control(data_types[type_loop], pipeline_settings.get('test_group_name'), 
				pipeline_settings.get('control_group_name')) == 'test']
		for ind in change_inds:
			if not change_lists:
				this_change_list = \
					[change_ind if change_ind != change_str else ind for change_ind in change_ind_list]
				# changed this - # change/pop ind must be the last ind in the list because i can't do
					# anything with the objects lower in the nest
				hfnxns.update_by_key_ind_list(input_list, this_change_list, 
					fnxn = lambda x: x.replace(replace_str
						.format(pipeline_settings.get('test_group_name')),''))
			else:
				for change_ind, change_list in enumerate(change_ind_list):
					if isinstance(replace_str, list) or isinstance(replace_str, tuple):
						use_str = replace_str[change_ind]
					else:
						use_str = replace_str
					this_change_list = \
						[change_ind if change_ind != change_str else ind for change_ind in change_list]
					hfnxns.update_by_key_ind_list(input_list, this_change_list, 
						fnxn = lambda x: x.replace(use_str
							.format(pipeline_settings.get('test_group_name')),''))
			# grab_fnxn(fnxn = lambda x: x.replace(replace_str
			# 	.format(pipeline_settings.get('control_group_name')),''))
	# data_types = [grab_fnxn(this_dict) for this_dict in input_list]
	# data_types = grab_fnxn()
	# # data_types = grab_fnxn(input_list)
	# if barplot_drop_test:
	# 	drop_inds = [type_loop for type_loop in range(len(data_types)) if
	# 		check_test_control(data_types[type_loop], pipeline_settings.get('test_group_name'), 
	# 			pipeline_settings.get('control_group_name')) == 'test']
	# 	drop_inds.reverse()
	# 	for ind in drop_inds:
	# 		hfnxns.update_by_key_ind_list(input_list, drop_ind_list + [ind], pop_val = True)
	# elif barplot_remove_test_group_name:
	# 	change_inds = [type_loop for type_loop in range(len(data_types)) if
	# 		check_test_control(data_types[type_loop], pipeline_settings.get('test_group_name'), 
	# 			pipeline_settings.get('control_group_name')) == 'test']
	# 	for ind in change_inds:
	# 		grab_fnxn(fnxn = lambda x: x.replace(replace_str))




# main plots for figures
# ccle main broadpeak figure code
def ccle_main_figure(fig_dir, line_name_dict, analysis_dir, \
	other_plots_dir, save_name, \
	which_len_change, save_show='show', save_format='png'):
	
	font = {
	        'size'   : 4.5
	        }
	matplotlib.rc('font', **font)
	fig_width = 8.5
	fig_height = 11
	gridsize = (10000,10000)






	with open(os.path.join(analysis_dir, 
		'data_for_figures/figure_pipeline_settings.json'), 'r') as textfile:
		pipeline_settings = json.load(textfile)

	fig = hfnxns.easy_fig(figsize=(fig_width, fig_height), gridsize=gridsize, \
			subplot_left=0.01, subplot_right=0.99, subplot_bottom=0.01, \
			subplot_top=0.99)

	plot_quartile_dir = os.path.join(analysis_dir, \
		'data_for_figures/len_quantile_plot_dict')
	fig.add_panel(top_left=(0.051,0.04), bottom_right=(0.349,0.14), \
		x_count=1, y_count=1, subpanel_pad_percent=0)
	file_list = glob.iglob(os.path.join(plot_quartile_dir, '*'))
	file_list = sorted(file_list)
	data_list = []
	for filepath in file_list:
		with open(filepath, 'r') as textfile:
			data_list.append(json.load(textfile))
	
	# drop_test=False,
		# drop_control=False, remove_test_group_name=False, test_pattern=None,
		# control_pattern=None
	# new_data_list = []
	# for this_dict in data_list:
	# 	data_name = this_dict.get('sample_groups')
	# 	if check_test_control(data_name, test_pattern, control_pattern) == 'test':
	# 		if not drop_test:
	# 			if not remove_test_group_name:
	# 				new_data_list.append(this_dict)
	# 			else:
	# 				this_dict['sample_groups'] = data_name.replace('{}_'
	# 					.format(test_pattern),'')
	# 				new_data_list.append(this_dict)
	# 	elif not drop_control:
	# 		if not remove_test_group_name:
	# 			new_data_list.append(this_dict)
	# 		else:
	# 			this_dict['sample_groups'] = data_name.replace('{}_'
	# 				.format(control_pattern),'')
	# 			new_data_list.append(this_dict)
	# data_list = new_data_list


	# for this_dict in data_list:
	format_files_groupnames(data_list, 
		name_ind_list=['name_ind', 'sample_groups'], 
		drop_ind_list=['drop_ind'], 
		change_ind_list=['change_ind', 'sample_groups'],
		pipeline_settings=pipeline_settings, 
		barplot_drop_test=False, 
		barplot_drop_control=True, 
		barplot_remove_test_group_name=True, 
		replace_str='{}_')
	# print(len(data_list))
	mean_len_quartiles_just_plot(fig.axes[0], line_name_dict, data_list,
		drop_control=True, remove_test_group_name=True, 
		test_pattern=pipeline_settings.get('test_group_name'),
		control_pattern=pipeline_settings.get('control_group_name'))
	fig.add_text(0.04,0.0375,'A', transform=fig.fig.transFigure, \
		fontsize=15, weight='bold')






	fig.add_panel(top_left=(0.051,0.185), bottom_right=(0.349,0.285), \
		x_count=2, y_count=1, subpanel_pad_percent=(35,0))
	barplot_dir = os.path.join(analysis_dir, \
		'data_for_figures/common_barplot_dict')
	file_list = sorted([thing for thing in \
		glob.iglob(os.path.join(barplot_dir, '*')) \
		if 'perc' not in thing.split('/')[-1]], reverse=True)

	# barplot_drop_test = False
	# barplot_drop_control = True
	# barplot_remove_test_group_name = True
	common_bplot_dict_list = []
	for file_loop in range(len(file_list)):
		with open(file_list[file_loop], 'r') as textfile:
			common_barplot_dict = json.load(textfile)
			common_bplot_dict_list.append(common_barplot_dict)
		# print(common_barplot_dict.get('cancer_types'))
		format_files_groupnames(common_barplot_dict, 
			name_ind_list=['cancer_types'], 
			drop_ind_list=[
				['cancer_types', 'drop_ind'], 
				['x_axis', 'drop_ind'], 
				['peak_array', 'drop_ind'], 
				['common_counts', 'drop_ind']
			], 
			change_ind_list=['cancer_types', 'change_ind'],
			pipeline_settings=pipeline_settings, 
			barplot_drop_test=False, 
			barplot_drop_control=True, 
			barplot_remove_test_group_name=True, 
			replace_str='{} ')
		# print(common_barplot_dict.get('cancer_types'))
		hfnxns.plot_common_barplot(fig.axes[1][file_loop], 
			common_barplot_dict.get('array_names'), 
			common_barplot_dict.get('x_axis'), 
			common_barplot_dict.get('peak_array'), 
			common_barplot_dict.get('width'), 
			common_barplot_dict.get('cancer_types'), 
			common_barplot_dict.get('save_spot'), 
			common_barplot_dict.get('common_counts'), 
			no_title=True, rotation=-45)
	fig.add_text(0.04,0.18,'B', transform=fig.fig.transFigure, 
		fontsize=15, weight='bold')
		# raise
	# print([common_barplot_dict.get('cancer_types') for common_barplot_dict in common_bplot_dict_list])
	# format_files_groupnames(common_bplot_dict_list, 
	# 	key_ind_list=['cancer_types'], 
	# 	drop_ind_list=[['x_axis', 'drop_ind'], ['peak_array', 'drop_ind'], ['common_counts', 'drop_ind']], 
	# 	change_ind_list=['cancer_types', 'change_ind'],
	# 	pipeline_settings=pipeline_settings, 
	# 	barplot_drop_test=False, 
	# 	barplot_drop_control=True, 
	# 	barplot_remove_test_group_name=True, 
	# 	replace_str='{}_')
	# print([common_barplot_dict.get('cancer_types') for common_barplot_dict in common_bplot_dict_list])
	# raise
	#
		# 	# print(common_barplot_dict.get('cancer_types'))
		# 	# print(data_list[-1])
		# 	# if check_test_control(data_types[type_loop], test_pattern, control_pattern) == 'control':
		# 	if barplot_drop_control:
		# 		data_types = common_barplot_dict.get('cancer_types')
		# 		# print('dropping control')
		# 		drop_inds = [type_loop for type_loop in range(len(data_types)) if
		# 			check_test_control(data_types[type_loop], pipeline_settings.get('test_group_name'), 
		# 				pipeline_settings.get('control_group_name')) == 'control']
		# 		# print('\tdropping: {}'.format([data_types[this_ind] for this_ind in drop_inds]))
		# 		drop_inds.reverse()
		# 		for ind in drop_inds:
		# 			for this_key in ['x_axis', 'peak_array', 'common_counts']:
		# 				common_barplot_dict.get(this_key).pop(ind)
		# 	elif barplot_remove_test_group_name:
		# 		change_inds = [type_loop for type_loop in range(len(data_types)) if
		# 			check_test_control(data_types[type_loop], pipeline_settings.get('test_group_name'), 
		# 				pipeline_settings.get('control_group_name')) == 'control']
		# 		for ind in change_inds:
		# 			common_barplot_dict['cancer_types'][ind] = common_barplot_dict.get('cancer_types')[ind].replace('{} '
		# 				.format(pipeline_settings.get('control_group_name')),'')
		# 	if barplot_drop_test:
		# 		data_types = common_barplot_dict.get('cancer_types')
		# 		# print('dropping test')
		# 		drop_inds = [type_loop for type_loop in range(len(data_types)) if
		# 			check_test_control(data_types[type_loop], pipeline_settings.get('test_group_name'), 
		# 				pipeline_settings.get('control_group_name')) == 'test']
		# 		# print('\tdropping: {}'.format([data_types[this_ind] for this_ind in drop_inds]))
		# 		drop_inds.reverse()
		# 		for ind in drop_inds:
		# 			for this_key in ['x_axis', 'peak_array', 'common_counts']:
		# 				common_barplot_dict.get(this_key).pop(ind)
		# 	elif barplot_remove_test_group_name:
		# 		change_inds = [type_loop for type_loop in range(len(data_types)) if
		# 			check_test_control(data_types[type_loop], pipeline_settings.get('test_group_name'), 
		# 				pipeline_settings.get('control_group_name')) == 'test']
		# 		for ind in change_inds:
		# 			common_barplot_dict['cancer_types'][ind] = common_barplot_dict.get('cancer_types')[ind].replace('{} '
		# 				.format(pipeline_settings.get('test_group_name')),'')
		# 	# print(common_barplot_dict.get('cancer_types'))
		# # x_axis, peak_array, cancer_types, common_counts
		# # common_peak_barplot_just_plot(fig.axes[1][file_loop], file_loop, \

	# for common_barplot_dict in common_bplot_dict_list:






	broad_sharp_path = os.path.join(analysis_dir, \
		'data_for_figures/path_dots/hallmark_kegg_paths/4kb_broad_Cancer_subtract_1kb_sharp_Cancer')
	# broad_sharp_path = os.path.join(analysis_dir, \
	# 	'data_for_figures/path_dots/ref_normal_unique/unique_broad_sharp_paths')
	fig.add_panel(top_left=(0.24,0.35), bottom_right=(0.349,0.45), \
		x_count=1, y_count=1, subpanel_pad_percent=0)	
	with open(broad_sharp_path, 'r') as textfile:
		unique_broad_sharp_dict = json.load(textfile)
	hfnxns.path_dots_just_plot(ax=fig.axes[2][0], \
		xpositions=unique_broad_sharp_dict.get('xpositions'), \
		ypositions=unique_broad_sharp_dict.get('ypositions'), \
		new_p_vals=unique_broad_sharp_dict.get('new_p_vals'), 
		color_array=unique_broad_sharp_dict.get('color_array'), \
		type_list=unique_broad_sharp_dict.get('type_list'), \
		all_paths=unique_broad_sharp_dict.get('all_paths'), \
		group1_after_pat=unique_broad_sharp_dict.get('group1_pattern'), \
		# group2_after_pat=unique_broad_sharp_dict.get('group2_pattern'), \
		bottom_cushion=0.5, min_fontsize=5,
		xlim_pad=1, multiply_s_by=0.4, \
		divider_percent='20%', top_cushion=0.5)
	fig.add_text(0.04,0.345,'C', transform=fig.fig.transFigure, \
		fontsize=15, weight='bold')






	# just need to move this
	census_barplot_spot = os.path.join(analysis_dir, \
		'data_for_figures/cancer_plots/len/len_cosmic')
	# census_barplot_spot = os.path.join(analysis_dir, \
	# 	'data_for_figures/cancer_barplots/len_cosmic')
	with open(census_barplot_spot, 'r') as textfile:
		cancer_barplot_dict = json.load(textfile)
	loop_count = 2
	gene_type_count = 3
	sig_list = hfnxns.full_sig_list(cancer_barplot_dict, loop_count, gene_type_count)
	fig.add_panel(top_left=(0.4293,0.04), bottom_right=(0.97,0.21), \
		x_count=1, y_count=1, subpanel_pad_percent=0)	
	hfnxns.cancer_gene_barplot_just_plot(fig.axes[3][0], \
		cancer_barplot_dict.get('peak_list_to_plot'), \
		cancer_barplot_dict.get('gene_types'), \
		cancer_barplot_dict.get('types'), 
		cancer_barplot_dict.get('group_names'), \
		cancer_barplot_dict.get('len_or_len_change'), \
		'len', loop_count, gene_type_count, 
		deffont=4, x_ax_text_ratio=0.75, 
		sig_list=sig_list, text_lower_ratio=0.0, y_lim_multi = 1.3, \
		type_legend=True, type_legend_spot='left', legend_labelspacing=0.25, \
		bottom_label_extra_down=1.9, label_size_multi=1.2, fig=fig)
		# cancer_barplot_dict.get('peak_list_to_plot'), \
		# cancer_barplot_dict.get('gene_types'), \
		# cancer_barplot_dict.get('types'), 
		# cancer_barplot_dict.get('group_names'), \
		# cancer_barplot_dict.get('len_or_len_change'), \
		# 'Cosmic', 'len', \
		# None, 
	fig.add_text(0.41,0.0375,'D', transform=fig.fig.transFigure, \
		fontsize=15, weight='bold')






	# just need to move this
	fig.add_panel(top_left=(0.407,0.26), bottom_right=(1.1,0.46), \
		x_count=1, y_count=1, subpanel_pad_percent=0)	
	broad_igv_dir = os.path.join(other_plots_dir, 'CCLE_Broad_IGV_Myc.png')
	img=mpimg.imread(broad_igv_dir)
	fig.axes[4][0].imshow(img, interpolation='spline36', aspect='equal')
	fig.axes[4][0].axis('off')
	fig.add_text(0.41,0.258,'E', transform=fig.fig.transFigure, \
		fontsize=15, weight='bold')






	# # just need to move this
	# broad_tsne_spot = os.path.join(other_plots_dir, \
	# 	'CCLE_Broad_H3K4me3_tnse_per5_NormalvsMatchedTumor_02.25.2020.png')
	# fig.add_panel(top_left=(0.029,0.48), bottom_right=(0.381,0.70), \
	# 	x_count=1, y_count=1, subpanel_pad_percent=0)	
	# img=mpimg.imread(broad_tsne_spot)
	# fig.axes[5][0].imshow(img, interpolation='spline36', aspect='equal')
	# fig.axes[5][0].axis('off')
	# fig.add_text(0.04,0.473,'F', transform=fig.fig.transFigure, \
	# 	fontsize=15, weight='bold')






	# just need to move this
	kde_size_change_path = os.path.join(analysis_dir, \
		'data_for_figures/kde_size_change/{}_abs_len_change_kde'.format(which_len_change))
	# kde_size_change_path = os.path.join(analysis_dir, \
	# 	'data_for_figures/kde_size_change/nl_All_Peaks_size_change_kde_abs')
	fig.add_panel(top_left=(0.0605,0.48), bottom_right=(0.3581,0.70), \
		x_count=1, y_count=1, subpanel_pad_percent=0)	
	with open(kde_size_change_path, 'r') as textfile:
		kde_size_change_dict = json.load(textfile)
	longname = 'Lengthen'
	change_cutoff=2000
	ratio_name = 'Log2 Ratio Length Change'
	abs_name = 'Absolute Length Change'
	hfnxns.kde_size_change_just_plot(fig.axes[5][0], 
		kde_size_change_dict.get('color_dict'), 
		kde_size_change_dict.get('list_peak_len_change'), 
		[-4000, 4000], 
		# kde_size_change_dict.get('x_min_max'), 
		kde_size_change_dict.get('plot_name'), 
		kde_size_change_dict.get('save_spot'),
		xlabel_value=abs_name,
		longname=longname,
		change_cutoff=change_cutoff
		)
		# kde_size_change_dict.get('color_dict'), 
		# kde_size_change_dict.get('cancer_types'), 
		# kde_size_change_dict.get('list_peak_len_change'), 
		# kde_size_change_dict.get('x_min_max'), 
		# kde_size_change_dict.get('plot_name'), 
		# kde_size_change_dict.get('save_spot1'), 
		# kde_size_change_dict.get('nl_broad'))
	fig.add_text(0.04,0.473,'F', transform=fig.fig.transFigure, \
		fontsize=15, weight='bold')









	other_analysis_dir = '/home/jschulz1000/Documents/ccle_stuff/outputs/test_run_2020_01_27'
	broad_sharp_path = os.path.join(analysis_dir, \
		'data_for_figures/len_ch_ref_control/path_dots/hallmark_kegg_paths/shortened_{}_dens_peaks_analysis'.format(
			pipeline_settings.get('len_short_dict').get(which_len_change)))
		# 'data_for_figures/path_dots/lengthen_shorten_paths')

	# fig.add_panel(top_left=(0.675,0.82), bottom_right=(0.725,0.985), \
	# fig.add_text(0.525,0.79, 'I', transform=fig.fig.transFigure, fontsize=15, \
	# fig.add_panel(top_left=(0.93,0.82), bottom_right=(0.98,0.985), \
	# fig.add_text(0.77,0.79,'J', transform=fig.fig.transFigure, \

	fig.add_panel(top_left=(0.16,0.82), bottom_right=(0.235,0.985), \
		x_count=1, y_count=1, subpanel_pad_percent=0)
	with open(broad_sharp_path, 'r') as textfile:
		unique_broad_sharp_dict = json.load(textfile)
	hfnxns.path_dots_just_plot(ax=fig.axes[6][0], \
		xpositions=unique_broad_sharp_dict.get('xpositions'), \
		ypositions=unique_broad_sharp_dict.get('ypositions'), 
		new_p_vals=unique_broad_sharp_dict.get('new_p_vals'), \
		color_array=unique_broad_sharp_dict.get('color_array'), \
		type_list=unique_broad_sharp_dict.get('type_list'), 
		all_paths=unique_broad_sharp_dict.get('all_paths'), \
		group1_after_pat=unique_broad_sharp_dict.get('group1_pattern'), 
		# group2_after_pat=unique_broad_sharp_dict.get('group2_pattern'), \
		multiply_s_by=1.3, divider_percent='20%', \
		# broad_or_sharp='sharp', multiply_s_by=1.0, divider_percent='20%', \
		top_cushion=0.5, bottom_cushion=0.5, min_fontsize=5, fontsize=5, xlim_pad=0.7,
		format_kegg_hallmark_str=True, wrap_labels=True)
	fig.add_text(0.04,0.79, 'G', transform=fig.fig.transFigure, fontsize=15, \
		weight='bold')







	# just need to move this
	len_short_igv = os.path.join(other_plots_dir, \
		'CCLE_Broad_H3K4me3_Shortening_densitycutoff_IGV_RNA.png')
	# fig.add_panel(top_left=(0.421,0.71), bottom_right=(1.0,1.0), \
	fig.add_panel(top_left=(0.48,0.71), bottom_right=(1.0,1.0), \
		x_count=1, y_count=1, subpanel_pad_percent=0)	

	img=mpimg.imread(len_short_igv)
	fig.axes[7][0].imshow(img, interpolation='spline36', aspect='equal')
	fig.axes[7][0].axis('off')

	fig.add_text(0.489,0.706,'J', transform=fig.fig.transFigure, \
		fontsize=15, weight='bold')






	broad_sharp_path = os.path.join(analysis_dir, \
		'data_for_figures/len_ch_ref_control/path_dots/hallmark_kegg_paths/lengthened_{}_dens_peaks_analysis'.format(which_len_change))
		# 'data_for_figures/path_dots/lengthen_shorten_paths')

	fig.add_panel(top_left=(0.325,0.82), bottom_right=(0.40,0.985), \
		x_count=1, y_count=1, subpanel_pad_percent=0)
	with open(broad_sharp_path, 'r') as textfile:
		unique_broad_sharp_dict = json.load(textfile)
	hfnxns.path_dots_just_plot(ax=fig.axes[8][0], \
		xpositions=unique_broad_sharp_dict.get('xpositions'), \
		ypositions=unique_broad_sharp_dict.get('ypositions'), 
		new_p_vals=unique_broad_sharp_dict.get('new_p_vals'), \
		color_array=unique_broad_sharp_dict.get('color_array'), \
		type_list=unique_broad_sharp_dict.get('type_list'), 
		all_paths=unique_broad_sharp_dict.get('all_paths'), \
		group1_after_pat=unique_broad_sharp_dict.get('group1_pattern'), 
		# group2_after_pat=unique_broad_sharp_dict.get('group2_pattern'), \
		multiply_s_by=1.3, divider_percent='20%', \
		# broad_or_sharp='broad', multiply_s_by=1.0, divider_percent='20%', \
		top_cushion=0.5, bottom_cushion=0.5, min_fontsize=5, fontsize=5, xlim_pad=0.7,
		format_kegg_hallmark_str=True, wrap_labels=True)
	fig.add_text(0.275,0.79,'H', transform=fig.fig.transFigure, \
		fontsize=15, weight='bold')

	shift_right = 0.07
	box = fig.axes[8][0].get_position()
	box.x0 = box.x0 + shift_right
	box.x1 = box.x1 + shift_right
	fig.axes[8][0].set_position(box)





	#
		# # just need to move this
		# broad_tsne_spot = os.path.join(other_plots_dir, \
		# 	'CCLE_Broad_H3K4me3_tnse_per5_NormalvsMatchedTumor_02.25.2020.png')
		# fig.add_panel(top_left=(0.029,0.48), bottom_right=(0.381,0.70), \
		# 	x_count=1, y_count=1, subpanel_pad_percent=0)	
		# img=mpimg.imread(broad_tsne_spot)
		# fig.axes[5][0].imshow(img, interpolation='spline36', aspect='equal')
		# fig.axes[5][0].axis('off')
		# fig.add_text(0.04,0.473,'F', transform=fig.fig.transFigure, \
		# 	fontsize=15, weight='bold')


		# # just need to move this
		# kde_size_change_path = os.path.join(analysis_dir, \
		# 	'data_for_figures/kde_size_change/{}_abs_len_change_kde'.format(which_len_change))
		# # kde_size_change_path = os.path.join(analysis_dir, \
		# # 	'data_for_figures/kde_size_change/nl_All_Peaks_size_change_kde_abs')
		# fig.add_panel(top_left=(0.0515,0.73), bottom_right=(0.3491,0.95), \
		# 	x_count=1, y_count=1, subpanel_pad_percent=0)	
		# with open(kde_size_change_path, 'r') as textfile:
		# 	kde_size_change_dict = json.load(textfile)
		# longname = 'Lengthen'
		# change_cutoff=2000
		# ratio_name = 'Log2 Ratio Length Change'
		# abs_name = 'Absolute Length Change'
		# hfnxns.kde_size_change_just_plot(fig.axes[6][0], 
		# 	kde_size_change_dict.get('color_dict'), 
		# 	kde_size_change_dict.get('list_peak_len_change'), 
		# 	[-4000, 4000], 
		# 	# kde_size_change_dict.get('x_min_max'), 
		# 	kde_size_change_dict.get('plot_name'), 
		# 	kde_size_change_dict.get('save_spot'),
		# 	xlabel_value=abs_name,
		# 	longname=longname,
		# 	change_cutoff=change_cutoff
		# 	)
		# 	# kde_size_change_dict.get('color_dict'), 
		# 	# kde_size_change_dict.get('cancer_types'), 
		# 	# kde_size_change_dict.get('list_peak_len_change'), 
		# 	# kde_size_change_dict.get('x_min_max'), 
		# 	# kde_size_change_dict.get('plot_name'), 
		# 	# kde_size_change_dict.get('save_spot1'), 
		# 	# kde_size_change_dict.get('nl_broad'))
		# fig.add_text(0.04,0.718,'G', transform=fig.fig.transFigure, \
		# 	fontsize=15, weight='bold')






	census_barplot_spot = os.path.join(analysis_dir, \
		'data_for_figures/len_ch_ref_control/cancer_plots/len_change_{}/len_change_cosmic'.format(which_len_change))
	# census_barplot_spot = os.path.join(analysis_dir, \
	# 	'data_for_figures/cancer_barplots/len_change_cosmic')
	fig.add_panel(top_left=(0.4293,0.49), bottom_right=(0.97,0.66), \
		x_count=1, y_count=1, subpanel_pad_percent=0)	
	loop_count = 3
	gene_type_count = 3
	with open(census_barplot_spot, 'r') as textfile:
		cancer_barplot_dict = json.load(textfile)
	sig_list = hfnxns.full_sig_list(cancer_barplot_dict, loop_count, gene_type_count)
	hfnxns.cancer_gene_barplot_just_plot(fig.axes[9][0], \
		cancer_barplot_dict.get('peak_list_to_plot'), \
		cancer_barplot_dict.get('gene_types'), \
		cancer_barplot_dict.get('types'), \
		cancer_barplot_dict.get('group_names'), \
		cancer_barplot_dict.get('len_or_len_change'), \
		'len', loop_count, gene_type_count, x_ax_text_ratio=0.8, \
		deffont=4, sig_list=sig_list, xtick_rotation=-30, \
		y_lim_multi=1.2, text_lower_ratio= -0.2, type_legend=True, \
		type_legend_spot='below', bottom_label_extra_down=2.00, \
		label_size_multi=1.3, fig=fig)
	fig.add_text(0.41,0.473,'I', transform=fig.fig.transFigure, fontsize=15, \
		weight='bold', ha='center')






	if save_show == 'save':
		plt.savefig(os.path.join(fig_dir, save_name), format=save_format, dpi=300)
	else:
		plt.show()
	plt.close()

# ccle supplementary broadpeak figure code
def new_ccle_supp_fig(fig_dir, line_name_dict, analysis_dir, \
	other_plots_dir, other_fig_dir, save_name, which_len_change, 
	save_show='show', save_format='png'):
	start_time = time.time()
	font = {
	        'size'   : 4.5
	        }
	matplotlib.rc('font', **font)
	fig_width = 8.5
	fig_height = 11
	gridsize = (1000,1000)

	fig = hfnxns.easy_fig(figsize=(fig_width, fig_height), gridsize=gridsize, \
		subplot_left=0.01, subplot_right=0.99, subplot_bottom=0.01, \
		subplot_top=0.99)





	with open(os.path.join(analysis_dir, 
		'data_for_figures/figure_pipeline_settings.json'), 'r') as textfile:
		pipeline_settings = json.load(textfile)






	# loading the list of quantile dicts for plotting
	plot_quantile_dir = os.path.join(analysis_dir, \
		'data_for_figures/len_quantile_plot_dict')
	x_ax = 2
	y_ax = 4
	panel_loop = 0
	file_list = glob.iglob(os.path.join(plot_quantile_dir, '*'))
	file_list = sorted(file_list)
	quantile_dict_list = []
	# dicts are loaded and sorted based on the number of samples in each type
	for filepath in file_list:
		with open(filepath, 'r') as textfile:
			quantile_plot_dict = json.load(textfile)
		quantile_dict_list.append([filepath, len(quantile_plot_dict.get('sample_values')), \
			quantile_plot_dict])
		quantile_dict_list = sorted(quantile_dict_list, key=operator.itemgetter(1,0))


	# for name: list ind -> 1 -> sample_values 
		# for remove: list ind
		# for replace: list ind -> 1 -> sample_values
	# print([(quan_dic[1], quan_dic[2].get('sample_groups')) for quan_dic in quantile_dict_list])
	format_files_groupnames(quantile_dict_list, 
		name_ind_list=['name_ind', 2, 'sample_groups'], 
		drop_ind_list=['drop_ind'], 
		change_ind_list=[['change_ind', 2, 'sample_groups'], ['change_ind', 2, 'plot_name']],
		pipeline_settings=pipeline_settings, 
		barplot_drop_test=False, 
		barplot_drop_control=True, 
		barplot_remove_test_group_name=True, 
		replace_str=['{}_', '{} '])
	# print([(quan_dic[1], quan_dic[2].get('sample_groups')) for quan_dic in quantile_dict_list])
	# print('\n'.join([str([merge_key, 
	# 	str(quantile_dict_list[0][2].get(merge_key))[:40]]) for merge_key in list(quantile_dict_list[0][2].keys())]))

	# raise

		# format_files_groupnames(common_barplot_dict, 
		# 	name_ind_list=['cancer_types'], 
		# 	drop_ind_list=[
		# 		['cancer_types', 'drop_ind'], 
		# 		['x_axis', 'drop_ind'], 
		# 		['peak_array', 'drop_ind'], 
		# 		['common_counts', 'drop_ind']
		# 	], 
		# 	change_ind_list=['cancer_types', 'change_ind'],
		# 	pipeline_settings=pipeline_settings, 
		# 	barplot_drop_test=False, 
		# 	barplot_drop_control=True, 
		# 	barplot_remove_test_group_name=True, 
		# 	replace_str='{} ')
	
		# barplot_drop_test = False
		# barplot_drop_control = True
		# barplot_remove_test_group_name = True
		# data_types = [this_dict[2].get('sample_groups') for this_dict in quantile_dict_list]
		# if barplot_drop_control:
		# 	drop_inds = [type_loop for type_loop in range(len(data_types)) if
		# 		check_test_control(data_types[type_loop], pipeline_settings.get('test_group_name'), 
		# 			pipeline_settings.get('control_group_name')) == 'control']
		# 	drop_inds.reverse()
		# 	for ind in drop_inds:
		# 		quantile_dict_list.pop(ind)
		# elif barplot_remove_test_group_name:
		# 	change_inds = [type_loop for type_loop in range(len(data_types)) if
		# 		check_test_control(data_types[type_loop], pipeline_settings.get('test_group_name'), 
		# 			pipeline_settings.get('control_group_name')) == 'control']
		# 	for ind in change_inds:
		# 		quantile_dict_list[ind][2]['sample_groups'] = this_dict[ind][2].get('sample_groups').replace('{}_'
		# 			.format(pipeline_settings.get('control_group_name')),'')
		# data_types = [this_dict[2].get('sample_groups') for this_dict in quantile_dict_list]
		# if barplot_drop_test:
		# 	drop_inds = [type_loop for type_loop in range(len(data_types)) if
		# 		check_test_control(data_types[type_loop], pipeline_settings.get('test_group_name'), 
		# 			pipeline_settings.get('control_group_name')) == 'test']
		# 	drop_inds.reverse()
		# 	for ind in drop_inds:
		# 		quantile_dict_list.pop(ind)
		# elif barplot_remove_test_group_name:
		# 	change_inds = [type_loop for type_loop in range(len(data_types)) if
		# 		check_test_control(data_types[type_loop], pipeline_settings.get('test_group_name'), 
		# 			pipeline_settings.get('control_group_name')) == 'test']
		# 	for ind in change_inds:
		# 		quantile_dict_list[ind][2]['sample_groups'] = quantile_dict_list[ind][2].get('sample_groups').replace('{}_'
		# 			.format(pipeline_settings.get('test_group_name')),'')

	list_ind = -1
	row_heights = []
	for row in range(y_ax):
		list_ind += x_ax
		row_heights.append(quantile_dict_list[list_ind][1])
		# print(quantile_dict_list[list_ind][1], quantile_dict_list[list_ind][2].get('sample_groups'))
	

	fig.add_panel(top_left=(0.083,0.015), bottom_right=(0.423,0.40), \
		x_count=x_ax, y_count=y_ax, subpanel_pad_percent=(33, 23), vratio=row_heights)
	for filepath, _, quantile_plot_dict in quantile_dict_list:
		len_quantiles_just_plot(fig.axes[0][panel_loop], panel_loop, \
			line_name_dict,quantile_plot_dict.get('percents'), \
			quantile_plot_dict.get('sample_values'), \
			quantile_plot_dict.get('sample_groups'), \
			quantile_plot_dict.get('save_path_len'), \
			quantile_plot_dict.get('index_keep'), \
			quantile_plot_dict.get('plot_name'), \
			len(fig.axes[0]), yticklabel_size=4.5, \
			label_markersize=4.5, label_fontsize=4, \
			min_fontsize=5, legend_inside_plot=False, legend_labelspacing=0.1)
		panel_loop += 1
	fig.add_text(0.02,0.02,'A', transform=fig.fig.transFigure, fontsize=15, weight='bold')

	# 
		# 'data_for_figures/len_quartile_plot_dict')
		# file_list = [this_file for this_file in sorted(file_list) if \
		# 	'len_quartile_plot_dict/E' not in this_file]
		# print('here {}'.format())
		# print([(this_dict[1], this_dict[2].get('sample_groups')) for this_dict in quantile_dict_list])
		# for dict_loop, this_dict in enumerate(quantile_dict_list):
			# with open(file_list[file_loop], 'r') as textfile:
			# common_barplot_dict = json.load(textfile)
			# print(common_barplot_dict.get('cancer_types'))
			# print(data_list[-1])
			# if check_test_control(data_types[type_loop], test_pattern, control_pattern) == 'control':
		# print([this_dict[2].get('sample_groups') for this_dict in quantile_dict_list])
			# print(data_types)
			# print('dropping control')
			# print('\tdropping: {}'.format([data_types[this_ind] for this_ind in drop_inds]))
				# for this_key in ['x_axis', 'peak_array', 'common_counts']:
				# 	this_dict.get(this_key).pop(ind)
			# print(data_types)
			# data_types = this_dict.get('sample_groups')
			# print('dropping test')
			# print('\tdropping: {}'.format([data_types[this_ind] for this_ind in drop_inds]))
				# for this_key in ['x_axis', 'peak_array', 'common_counts']:
				# 	this_dict.get(this_key).pop(ind)
			# print(change_inds, pipeline_settings.get('control_group_name'), '{}_'
					# .format(pipeline_settings.get('control_group_name')), quantile_dict_list[0][2].get('sample_groups'), 
					# quantile_dict_list[0][2].get('sample_groups').replace('{}_'
					# 	.format(pipeline_settings.get('test_group_name')),''))
			# print([len(quantile_dict_list[ind]) for ind in range(len(quantile_dict_list))])
				# this_dict['sample_groups'][ind] = this_dict.get('sample_groups')[ind].replace('{} '
				# 		.format(pipeline_settings.get('test_group_name')),'')
		# print([this_dict[2].get('sample_groups') for this_dict in quantile_dict_list])
		# print(row_heights)
		# raise
			# directory_loop = quantile_plot_dict.get('directories')\
			# 	.index(filepath.split('/')[-1])
			# plot_sample_quantiles(\
				# quantile_plot_dict.get('percents'), \
				# quantile_plot_dict.get('sample_values'), 
				# quantile_plot_dict.get('sample_groups'), \
				# quantile_plot_dict.get('save_path'),
				# quantile_plot_dict.get('index_keep'), \
				# quantile_plot_dict.get('plot_name'))
				# quantile_plot_dict.get('keep_line'), \
				# directory_loop, len(fig.axes[0]), yticklabel_size=4.5, \






	fig.add_panel(top_left=(0.545,0.015), bottom_right=(0.965,0.40), \
		x_count=2, y_count=4, subpanel_pad_percent=(25.5,35))
	cancer_types = sorted(pipeline_settings.get('sample_types'))
	# cancer_types = ['SKCM', 'GBMC', 'SCRC', 'OVCA', 'BRCA', 'LUNG', 'COAD', 'PAAD', 'all']
	# cancer_types = sorted(cancer_types)
	peak_scatter_dir = os.path.join(analysis_dir, \
		'data_for_figures/split_plot_dict/4kb_broad')
		# 'data_for_figures/split_plot_dict/4kb_25_50')
	file_list = sorted([thing for thing in glob.iglob(os.path.join(peak_scatter_dir, \
		'*_merged.json')) if any([this_type in thing.split('/')[-1] for this_type in cancer_types])])
	split_list = []
	for file_loop in range(len(file_list)):
		with open(file_list[file_loop], 'r') as textfile:
			split_plot_dict = json.load(textfile)
			split_list.append(split_plot_dict)
	# print([filepath.get('name_var') for filepath in split_list])
	format_files_groupnames(split_list, 
		name_ind_list=['name_ind', 'name_var'], 
		drop_ind_list=['drop_ind'], 
		change_ind_list=['change_ind', 'name_var'],
		pipeline_settings=pipeline_settings, 
		barplot_drop_test=False, 
		barplot_drop_control=True, 
		barplot_remove_test_group_name=True, 
		replace_str='{}_')
	# print([filepath.get('name_var') for filepath in split_list])
	# file_list = sorted([thing for thing in glob.iglob(os.path.join(peak_scatter_dir, \
	# 	'*_merged')) if thing.split('/')[-1].split('_')[0] in cancer_types])
	# print(len(fig.axes))
	# print(len(fig.axes[1]), print(len(split_list)))
					# split_plot_dict = {
					# 	'log_norm': log_norm, 
					# 	'vert_line': vert_line, 
					# 	'name_var': sample_id.replace('_merged', ''), 
					# 	'save_path': merged_save_path,
					# 	'peak_vals': merged_vals, 
					# 	'len_bins': len_bins, 'dens_bins': dens_bins, 
					# 	'vmin': vmin, 'vmax': vmax, 
					# 	'xlimits': xlimits, 'ylimits': ylimits, 
					# 	'max_x_ratio': max_x_ratio, 'max_y_ratio': max_y_ratio,
					# }
	for dict_ind, split_plot_dict in enumerate(split_list):
		split_plot_just_plot(fig.axes[1][dict_ind], split_plot_dict.get('log_norm'), \
			split_plot_dict.get('peak_vals'), \
			# split_plot_dict.get('lengths'), split_plot_dict.get('densities'), \
			split_plot_dict.get('len_bins'), split_plot_dict.get('dens_bins'), \
			split_plot_dict.get('vmin'), split_plot_dict.get('vmax'), \
			split_plot_dict.get('xlimits'), split_plot_dict.get('ylimits'), \
			split_plot_dict.get('vert_line'), split_plot_dict.get('name_var'), \
			split_plot_dict.get('save_path'), split_plot_dict.get('slopes'), \
			split_plot_dict.get('max_x_ratio'), split_plot_dict.get('max_y_ratio'), \
			min_fontsize=5)
	fig.add_text(0.485,0.02,'B', transform=fig.fig.transFigure, fontsize=15, weight='bold')




	# fig.add_panel(top_left=(0.083,0.015), bottom_right=(0.423,0.40), \
	# 	x_count=x_ax, y_count=y_ax, subpanel_pad_percent=(33, 23), vratio=row_heights)

	# fig.add_panel(top_left=(0.545,0.015), bottom_right=(0.965,0.40), \
	# 	x_count=2, y_count=4, subpanel_pad_percent=(25.5,35))

	# fig.add_panel(top_left=(0.055,0.45), bottom_right=(0.176,0.565), \
	# 	x_count=x_count, y_count=y_count, subpanel_pad_percent=0)

	# '/Users/jschulz1/Documents/ccle_stuff/inputs/other_plots_for_broadpeak_figs/heatmaps/final_cropped_heatmaps/*_top.png'
	# '/Users/jschulz1/Documents/project_data/ccle_project/other_plots_for_broadpeak_figs/heatmaps/BRCA_H3K4me3_TSS_input.heatmap_bottom.png'


	x_count=8
	y_count=2
	tallest_img = 0
	img_shapes = []
	file_list = sorted(glob.iglob(os.path.join(other_plots_dir, 'heatmaps/final_cropped_heatmaps/*_top.png')))
	file_list.extend(sorted(glob.iglob(os.path.join(other_plots_dir, 'heatmaps/final_cropped_heatmaps/*_bottom.png'))))
	# print(os.path.join(other_plots_dir, 'heatmaps/final_cropped_heatmaps/*_top.png'))
	# print('\n'.join(file_list))
	# file_list = sorted(glob.iglob(os.path.join(other_plots_dir, 'heatmaps', '*_top.png')))
	# file_list.extend(sorted(glob.iglob(os.path.join(other_plots_dir, 'heatmaps', '*_bottom.png'))))
	for panel_loop in range(x_count*y_count):
		img=mpimg.imread(file_list[panel_loop])
		img_shapes.append(img.shape)
		if img.shape[0] > tallest_img:
			tallest_img = img.shape[0]
	# panel4 = make_panel_list(gridsize, (0.51,0.515), (0.97,1.0), x_count, y_count, (5,0))
	# panel4 = make_panel_list(gridsize, (0.535,0.515), (0.97,1.0), x_count, y_count, (5,5), img_shapes=img_shapes)
	# print(img_shapes)
	# print('')
	# fig.add_panel(top_left=(0.545,0.015), bottom_right=(0.965,0.40), \
	# 	x_count=2, y_count=4, subpanel_pad_percent=(25.5,35))
	# for panel_loop in range(x_count*y_count):
	cy1 = 0.45
	cy2 = 0.48
	fig.add_panel(top_left=(0.065,cy1), bottom_right=(0.470,cy2), \
		x_count=x_count, y_count=1, subpanel_pad_percent=20, img_shapes=img_shapes[:x_count])
	for panel_loop in range(x_count):
		img=mpimg.imread(file_list[panel_loop])
		file_name = file_list[panel_loop].split('/')[-1].split('_')[0]
		fig.axes[2][panel_loop].text(0.5,1.0,file_name, transform=fig.axes[2][panel_loop].transAxes, fontsize=7, weight=550, ha='center', va='bottom')
		if panel_loop == 0:
			fig.axes[2][panel_loop].text(-0.2,1.0,'Sharp:', transform=fig.axes[2][panel_loop].transAxes, fontsize=7, weight=550, ha='right', va='top')
		# elif panel_loop % x_count == 0:
		# 	fig.axes[2][panel_loop].text(-0.2,1.0,'Broad:', transform=fig.axes[2][panel_loop].transAxes, fontsize=7, weight=550, ha='right', va='top')
		fig.axes[2][panel_loop].imshow(img, interpolation='spline36', aspect='auto')
		fig.axes[2][panel_loop].axis('off')

	fig.add_panel(top_left=(0.530,cy1), bottom_right=(0.95,cy2), \
		x_count=x_count, y_count=1, subpanel_pad_percent=20, img_shapes=img_shapes[x_count:])
	for panel_loop in range(x_count):
		img=mpimg.imread(file_list[panel_loop])
		file_name = file_list[panel_loop].split('/')[-1].split('_')[0]
		fig.axes[3][panel_loop].text(0.5,1.0,file_name, transform=fig.axes[3][panel_loop].transAxes, fontsize=7, weight=550, ha='center', va='bottom')
		if panel_loop == 0:
			fig.axes[3][panel_loop].text(-0.2,1.0,'Broad:', transform=fig.axes[3][panel_loop].transAxes, fontsize=7, weight=550, ha='right', va='top')
		# 	fig.axes[3][panel_loop].text(-0.2,1.0,'Sharp:', transform=fig.axes[3][panel_loop].transAxes, fontsize=7, weight=550, ha='right', va='top')
		# elif panel_loop % x_count == 0:
		fig.axes[3][panel_loop].imshow(img, interpolation='spline36', aspect='auto')
		fig.axes[3][panel_loop].axis('off')
		# if panel_loop < x_count:
		# 	file_name += '\nSharp'
		# else:
		# 	file_name += '\nBroad'
		# print(img.shape)
		# print(panel4[panel_loop].viewLim)
		# panel4[panel_loop].imshow(img, interpolation='spline36', va='top')
		# panel4[panel_loop].imshow(img, interpolation='spline36')
		# panel4[panel_loop].imshow(img, interpolation='spline36', extent=(-1, 1, -(img.shape[0]/tallest_img)*5, 5))
		# panel4[panel_loop].imshow(img, interpolation='spline36', extent=(-0.5, img.shape[1]-0.5, img.shape[0]-tallest_img, -tallest_img))
		# print('{}: {}'.format(file_name, file_list[panel_loop]))
	fig.add_text(0.02,0.44,'C', transform=fig.fig.transFigure, fontsize=15, weight='bold')
	# panel4[0].text(0.495,0.50,'D', transform=fig.transFigure, fontsize=15, weight='bold', ha='center')
	# plt.show()
	# plt.close()

	x_count=1
	y_count=1
	merge_scatter_spot = os.path.join(analysis_dir, 'data_for_figures/merge_scatter_json')

	fig.add_panel(top_left=(0.075,0.62), bottom_right=(0.196,0.735), \
	# fig.add_panel(top_left=(0.055,0.45), bottom_right=(0.176,0.565), \
		x_count=x_count, y_count=y_count, subpanel_pad_percent=0)
	with open(merge_scatter_spot, 'r') as textfile:
		merge_scatter_dict = json.load(textfile)

	orig_group_names = merge_scatter_dict.get('sample_groups').copy()
	merge_scatter_dict['sample_inds'] = list(range(len(orig_group_names)))
	format_files_groupnames(merge_scatter_dict, 
		name_ind_list=['sample_groups'], 
		drop_ind_list=[['sample_groups', 'drop_ind'], ['sample_inds', 'drop_ind']], 
		change_ind_list=['sample_groups', 'change_ind'],
		pipeline_settings=pipeline_settings, 
		barplot_drop_test=False, 
		barplot_drop_control=True, 
		barplot_remove_test_group_name=True, 
		replace_str='{} ')
	new_group_names = merge_scatter_dict.get('sample_groups')
	inds = merge_scatter_dict.get('sample_inds')
	new_dict = {}
	for new_ind, new_name in enumerate(new_group_names): 
		vals = merge_scatter_dict.get('raw_data').get(orig_group_names[inds[new_ind]])
		new_dict[new_name] = vals
	merge_scatter_dict['raw_data'] = new_dict
	for sample_type in merge_scatter_dict.get('color_dict').get('test').keys():
		merge_scatter_dict['color_dict'][sample_type] = merge_scatter_dict.get('color_dict').get('test').get(sample_type)
	# hfnxns.quick_dumps(merge_scatter_dict.get('color_dict'))

	# print(orig_group_names)
	# print(new_group_names)
	# print(merge_scatter_dict.get('color_dict'))
	# raise
	# print(inds)
	# raise

	# raise

	merge_scatter_all_just_plot(fig.axes[4][0], merge_scatter_dict.get('raw_data'), \
		merge_scatter_dict.get('merge_sizes'), merge_scatter_dict.get('sample_groups'), 
		color_dict=merge_scatter_dict.get('color_dict'), \
		save_dir = merge_scatter_dict.get('save_dirs'), \
		pipeline_dirs=merge_scatter_dict.get('pipeline_dirs'), include_legend=True, \
		left_label=True, panel_title='Average Peak Count By Type', label_markersize=5, \
		label_fontsize=5, legend_labelspacing=0.1, legend_inside_plot=False, \
		set_ylim=(5000, 20000), min_fontsize=5)
	fig.add_text(0.02,0.605,'D', transform=fig.fig.transFigure, fontsize=15, weight='bold')







	broad_sharp_path = os.path.join(analysis_dir, \
		'data_for_figures/path_dots/hallmark_kegg_paths/1kb_sharp_Cancer_subtract_4kb_broad_Cancer')
		# 'data_for_figures/path_dots/ref_normal_unique/unique_broad_sharp_paths')
	fig.add_panel(top_left=(0.20,0.82), bottom_right=(0.27,0.98), \
	# fig.add_panel(top_left=(0.475,0.45), bottom_right=(0.57,0.58), \
		x_count=x_count, y_count=y_count, subpanel_pad_percent=0)
	with open(broad_sharp_path, 'r') as textfile:
		unique_broad_sharp_dict = json.load(textfile)
	# hfnxns.path_dots_just_plot(ax=fig.axes[2][0], \
		# xpositions=unique_broad_sharp_dict.get('xpositions'), \
		# ypositions=unique_broad_sharp_dict.get('ypositions'), \
		# new_p_vals=unique_broad_sharp_dict.get('new_p_vals'), 
		# color_array=unique_broad_sharp_dict.get('color_array'), \
		# type_list=unique_broad_sharp_dict.get('type_list'), \
		# all_paths=unique_broad_sharp_dict.get('all_paths'), \
		# group1_after_pat=unique_broad_sharp_dict.get('group1_pattern'), \
		# # group2_after_pat=unique_broad_sharp_dict.get('group2_pattern'), \
		# bottom_cushion=0.5, min_fontsize=5,
		# xlim_pad=1, multiply_s_by=0.4, \
		# divider_percent='20%', top_cushion=0.5)
	hfnxns.path_dots_just_plot(ax=fig.axes[5][0], \
		xpositions=unique_broad_sharp_dict.get('xpositions'), \
		ypositions=unique_broad_sharp_dict.get('ypositions'), \
		new_p_vals=unique_broad_sharp_dict.get('new_p_vals'), 
		color_array=unique_broad_sharp_dict.get('color_array'), \
		type_list=unique_broad_sharp_dict.get('type_list'), \
		all_paths=unique_broad_sharp_dict.get('all_paths'), \
		group1_after_pat=unique_broad_sharp_dict.get('group1_pattern'), 
		# group2_after_pat=unique_broad_sharp_dict.get('group2_pattern'), \
		# broad_or_sharp='sharp', multiply_s_by=0.15, divider_percent='20%', \
		multiply_s_by=0.5, divider_percent='20%', \
		top_cushion=0.5, min_fontsize=5, xlim_pad=0.8)
	fig.add_text(0.02,0.80,'E', transform=fig.fig.transFigure, fontsize=15, weight='bold')





	loop_count = 2
	gene_type_count = 2
	census_barplot_spot = os.path.join(analysis_dir, \
		'data_for_figures/cancer_plots/len/len_tuson')
		# 'data_for_figures/cancer_barplots/len_tuson')
	with open(census_barplot_spot, 'r') as textfile:
		cancer_barplot_dict = json.load(textfile)
	sig_list = hfnxns.full_sig_list(cancer_barplot_dict, loop_count, gene_type_count)
	# print(sig_list)

	fig.add_panel(top_left=(0.34,0.62), bottom_right=(0.65,0.75), \
	# fig.add_panel(top_left=(0.64,0.45), bottom_right=(0.925,0.54), \
		x_count=x_count, y_count=y_count, subpanel_pad_percent=0)
	hfnxns.cancer_gene_barplot_just_plot(fig.axes[6][0], \
		cancer_barplot_dict.get('peak_list_to_plot'), \
		cancer_barplot_dict.get('gene_types'), 
		cancer_barplot_dict.get('types'), 
		cancer_barplot_dict.get('group_names'), \
		cancer_barplot_dict.get('len_or_len_change'), 
		# 'Cosmic', 'len', \
		'len', loop_count, gene_type_count, \
		deffont=4, x_ax_text_ratio=0.8, 
		sig_list=sig_list, text_lower_ratio=0.0, y_lim_multi = 1.55, \
		type_legend=True, type_legend_spot='left', bottom_label_extra_down=10.5, \
		label_size_multi=1.3, legend_inside_plot=True, rotate_x=-45, \
		x_ha='left', x_va='top', min_fontsize=5, fig=fig)

	fig.add_text(0.31,0.605,'F', transform=fig.fig.transFigure, fontsize=15, weight='bold')
	# fig.add_text(0.62,0.445,'E', transform=fig.fig.transFigure, fontsize=15, weight='bold')







	x_ax_text_ratio = 0.55
	deffont=4
	loop_count = 3
	gene_type_count = 2
	census_barplot_spot = os.path.join(analysis_dir, \
		'data_for_figures/len_ch_ref_control/cancer_plots/len_change_{}/len_change_tuson'.format(which_len_change))
		# 'data_for_figures/cancer_barplots/len_change_tuson')
	# fig.add_panel(top_left=(0.72,0.62), bottom_right=(0.98,0.74), \
	fig.add_panel(top_left=(0.34,0.82), bottom_right=(0.65,0.95), \
		x_count=1, y_count=1, subpanel_pad_percent=0)
	with open(census_barplot_spot, 'r') as textfile:
		cancer_barplot_dict = json.load(textfile)
	sig_list = hfnxns.full_sig_list(cancer_barplot_dict, loop_count, gene_type_count)
	hfnxns.cancer_gene_barplot_just_plot(fig.axes[7][0], \
		cancer_barplot_dict.get('peak_list_to_plot'), \
		cancer_barplot_dict.get('gene_types'), \
		cancer_barplot_dict.get('types'), 
		cancer_barplot_dict.get('group_names'), \
		cancer_barplot_dict.get('len_or_len_change'), \
		'len_change', loop_count, gene_type_count, x_ax_text_ratio=x_ax_text_ratio, \
		deffont=deffont, sig_list=sig_list, xtick_rotation=-30, y_lim_multi=1.7, 
		text_lower_ratio= -0.2, type_legend=True, type_legend_spot='below', \
		bottom_label_extra_down=10.5, label_size_multi=1.3, rotate_x=-45, \
		x_ha='left', x_va='top', label_fontsize=5, legend_labelspacing=0.1, 
		label_markersize=4, min_fontsize=5, verbose=True, fig=fig)
	fig.add_text(0.31,0.80,'H', transform=fig.fig.transFigure, fontsize=15, \
		weight='bold', ha='center')







	change_scatter_dir = os.path.join(analysis_dir, 'data_for_figures/len_ch_ref_control/len_dens_change_scatter/{}'.format(which_len_change))
	file_list = sorted(glob.iglob(os.path.join(change_scatter_dir, '*')))
	# fig.add_panel(top_left=(0.06,0.62), bottom_right=(0.34,0.97), \
	# 	x_count=2, y_count=3, subpanel_pad_percent=(30,30))
	fig.add_panel(top_left=(0.72,0.62), bottom_right=(0.98,0.97), \
		x_count=2, y_count=3, subpanel_pad_percent=(30,30))
	for panel_loop in range(len(fig.axes[8])):
		with open(file_list[panel_loop], 'r') as textfile:
			change_scatter_dict = json.load(textfile)
		# print('\n'.join([str([merge_key, 
		# 	str(change_scatter_dict.get(merge_key))[:40]]) for merge_key in list(change_scatter_dict.keys())]))
		hfnxns.change_scatter_just_plot(fig.axes[8][panel_loop], \
			change_scatter_dict.get('peak_stats'), \
			change_scatter_dict.get('cancer_type'), min_fontsize=5)
	# fig.add_text(0.02,0.61,'F', transform=fig.fig.transFigure, fontsize=15, weight='bold')
	fig.add_text(0.68,0.605,'G', transform=fig.fig.transFigure, fontsize=15, weight='bold')







	# sharp_tsne_spot = os.path.join(other_fig_dir, \
	# 	'CCLE_Sharp_H3K4me3_tnse_per5_NormalvsMatchedTumor_02.25.2020.png')
	# fig.add_panel(top_left=(0.38,0.61), bottom_right=(0.675,0.78), \
	# 	x_count=1, y_count=1, subpanel_pad_percent=0)
	# img=mpimg.imread(sharp_tsne_spot)
	# fig.axes[6][0].imshow(img, interpolation='spline36', aspect='equal')
	# fig.axes[6][0].axis('off')
	# fig.add_text(0.38,0.61,'G', transform=fig.fig.transFigure, fontsize=15, weight='bold')


	# fig.add_panel(top_left=(0.36,0.62), bottom_right=(0.70,0.74))
	# fig.add_panel(top_left=(0.36,0.76), bottom_right=(0.70,0.98))





	# other_analysis_dir = '/home/jschulz1000/Documents/ccle_stuff/outputs/test_run_2020_01_27'
	# broad_sharp_path = os.path.join(analysis_dir, \
	# 	'data_for_figures/len_ch_ref_control/path_dots/hallmark_kegg_paths/shortened_{}_dens_peaks_analysis'.format(
	# 		pipeline_settings.get('len_short_dict').get(which_len_change)))
	# 	# 'data_for_figures/path_dots/lengthen_shorten_paths')

	# # fig.add_panel(top_left=(0.675,0.82), bottom_right=(0.725,0.985), \
	# # fig.add_text(0.525,0.79, 'I', transform=fig.fig.transFigure, fontsize=15, \
	# # fig.add_panel(top_left=(0.93,0.82), bottom_right=(0.98,0.985), \
	# # fig.add_text(0.77,0.79,'J', transform=fig.fig.transFigure, \

	# fig.add_panel(top_left=(0.615,0.82), bottom_right=(0.69,0.985), \
	# 	x_count=1, y_count=1, subpanel_pad_percent=0)
	# with open(broad_sharp_path, 'r') as textfile:
	# 	unique_broad_sharp_dict = json.load(textfile)
	# hfnxns.path_dots_just_plot(ax=fig.axes[8][0], \
	# 	xpositions=unique_broad_sharp_dict.get('xpositions'), \
	# 	ypositions=unique_broad_sharp_dict.get('ypositions'), 
	# 	new_p_vals=unique_broad_sharp_dict.get('new_p_vals'), \
	# 	color_array=unique_broad_sharp_dict.get('color_array'), \
	# 	type_list=unique_broad_sharp_dict.get('type_list'), 
	# 	all_paths=unique_broad_sharp_dict.get('all_paths'), \
	# 	group1_after_pat=unique_broad_sharp_dict.get('group1_pattern'), 
	# 	# group2_after_pat=unique_broad_sharp_dict.get('group2_pattern'), \
	# 	multiply_s_by=1.3, divider_percent='20%', \
	# 	# broad_or_sharp='sharp', multiply_s_by=1.0, divider_percent='20%', \
	# 	top_cushion=0.5, bottom_cushion=0.5, min_fontsize=5, fontsize=5, xlim_pad=0.7,
	# 	format_kegg_hallmark_str=True, wrap_labels=True)
	# fig.add_text(0.464,0.79, 'I', transform=fig.fig.transFigure, fontsize=15, \
	# 	weight='bold')







	# broad_sharp_path = os.path.join(analysis_dir, \
	# 	'data_for_figures/len_ch_ref_control/path_dots/hallmark_kegg_paths/lengthened_{}_dens_peaks_analysis'.format(which_len_change))
	# 	# 'data_for_figures/path_dots/lengthen_shorten_paths')

	# fig.add_panel(top_left=(0.905,0.82), bottom_right=(0.98,0.985), \
	# 	x_count=1, y_count=1, subpanel_pad_percent=0)
	# with open(broad_sharp_path, 'r') as textfile:
	# 	unique_broad_sharp_dict = json.load(textfile)
	# hfnxns.path_dots_just_plot(ax=fig.axes[9][0], \
	# 	xpositions=unique_broad_sharp_dict.get('xpositions'), \
	# 	ypositions=unique_broad_sharp_dict.get('ypositions'), 
	# 	new_p_vals=unique_broad_sharp_dict.get('new_p_vals'), \
	# 	color_array=unique_broad_sharp_dict.get('color_array'), \
	# 	type_list=unique_broad_sharp_dict.get('type_list'), 
	# 	all_paths=unique_broad_sharp_dict.get('all_paths'), \
	# 	group1_after_pat=unique_broad_sharp_dict.get('group1_pattern'), 
	# 	# group2_after_pat=unique_broad_sharp_dict.get('group2_pattern'), \
	# 	multiply_s_by=1.3, divider_percent='20%', \
	# 	# broad_or_sharp='broad', multiply_s_by=1.0, divider_percent='20%', \
	# 	top_cushion=0.5, bottom_cushion=0.5, min_fontsize=5, fontsize=5, xlim_pad=0.7,
	# 	format_kegg_hallmark_str=True, wrap_labels=True)
	# fig.add_text(0.735,0.79,'J', transform=fig.fig.transFigure, \
	# 	fontsize=15, weight='bold')



	if save_show == 'save':
		plt.savefig(os.path.join(fig_dir, save_name), format=save_format, dpi=300)
	elif save_show == 'show':
		plt.show()
	elif save_show == 'show_interactive':
		show_interactive(fig.fig)


# def on_press(key, save_name, save_dir='image_files'):
# 	if key == KeyCode(char='q'):
# 		grab_specific_image_portion(save_name, save_dir)
# 	elif key == KeyCode(char='e'):
# 		return False
# 		with Listener(on_press=on_press, on_release=on_release) as listener:
# 			listener.join()

	plt.close()


def check_test_control(data_name, test_pattern, control_pattern):
	if test_pattern in data_name and control_pattern in data_name:
		if len(test_pattern) > len(control_pattern):
			return 'test'
		else:
			return 'control'
	elif test_pattern in data_name:
		return 'test'
	elif control_pattern in data_name:
		return 'control'


# individual plots for figure panels
# code for main fig A
def mean_len_quartiles_just_plot(ax, line_name_dict, data_list, \
	min_fontsize=5, increase_size_ratio=5, x_max=17000, sharp_cutoff=1000,
	broad_cutoff=4000, sharp_line=True, broad_line=True, drop_test=False,
	drop_control=False, remove_test_group_name=False, test_pattern=None,
	control_pattern=None):

	# if filepath.split('/')[-1].split('_')[0] != 'NonCancer':
	missing_remove_pattern_str = 'if remove_test_group_name is True, \
		\n\ttest_pattern and control_pattern must not be None'
	missing_drop_pattern_str = 'if drop_{test_group} is True, \
		\n\t{test_group}_pattern must not be None'
	if test_pattern is None:
		if remove_test_group_name:
			raise ValueError(missing_remove_pattern_str)
		if drop_test:
			raise ValueError(missing_drop_pattern_str.format(
				test_group='test'))
	if control_pattern is None:
		if remove_test_group_name:
			raise ValueError(missing_remove_pattern_str)
		if drop_control:
			raise ValueError(missing_drop_pattern_str.format(
				test_group='control'))
	# if len(test_pattern) > len(control_pattern):
	# 	longer_pattern = test_pattern
	# 	longer_drop = drop_test
	# else:
	# 	longer_pattern = control_pattern
	# 	longer_drop = drop_control
	# new_data_list = []
	# for this_dict in data_list:
	# 	data_name = this_dict.get('sample_groups')
	# 	if check_test_control(data_name, test_pattern, control_pattern) == 'test':
	# 		if not drop_test:
	# 			if not remove_test_group_name:
	# 				new_data_list.append(this_dict)
	# 			else:
	# 				this_dict['sample_groups'] = data_name.replace('{}_'
	# 					.format(test_pattern),'')
	# 				new_data_list.append(this_dict)
	# 	elif not drop_control:
	# 		if not remove_test_group_name:
	# 			new_data_list.append(this_dict)
	# 		else:
	# 			this_dict['sample_groups'] = data_name.replace('{}_'
	# 				.format(control_pattern),'')
	# 			new_data_list.append(this_dict)
	# data_list = new_data_list
			
		# print(data_name)
			# print('longer not remove')
			# print('longer remove')
			# print('test not remove')
			# print('test remove')
			# print('control not remove')
			# print('control remove')
		# if test_pattern in data_name and control_pattern in data_name:
		# 	if not longer_drop:
		# 		if not remove_test_group_name:
		# 			new_data_list.append(this_dict)
		# 		else:
		# 			this_dict['sample_groups'] = data_name.replace('{}_'
		# 				.format(longer_pattern),'')
		# 			new_data_list.append(this_dict)
		# elif test_pattern in data_name:
		# elif control_pattern in data_name:



	percents = data_list[0].get('percents')
	# directories = data_list[0].get('directories')
	# directories = [this_data.get('sample_groups').split('_')[-1] for this_data in data_list]
	directories = [this_data.get('sample_groups') for this_data in data_list]
	save_path_len = data_list[0].get('save_path_len')
	index_keep = data_list[0].get('index_keep')

	sample_lens = []
	for this_data in data_list:
		data_vals = this_data.get('sample_values')
		sample_names = sorted(list(data_vals.keys()))
		data_arr = np.asarray([data_vals.get(this_key) for this_key in sample_names])
		data_arr = data_arr.astype('int')
		sample_lens.append(list(np.mean(data_arr,axis=0).astype('int')))


		# data_vals = this_data.get('sample_lens')
		# data_arr = np.asarray(data_vals)[:,1:]
		# data_arr = data_arr.astype('int')
		# sample_lens.append(list(np.mean(data_arr,axis=0).astype('int')))

	sample_lens.reverse()
	directories.reverse()
	# directories = directories[1:]

	color_list = ['C0', 'C1', 'C2', 'C3', 'C4', 'C5', 'C6', 'C7', 'C8', 'C9']
	color_dict = {}
	percents = ['{}%'.format(value) for value in percents]
	for percent_loop, this_percent in enumerate(percents):
		color_dict[this_percent] = color_list[percent_loop]
	legend_elements = []
	
	labels = [color_dict.get(thing) for thing in percents]
	for item_loop in range(len(sample_lens)):
		dots = [int(thing) for thing in sample_lens[item_loop]]
		line_x = (dots[0],dots[-1])
		ax[0].plot(line_x, [item_loop+1]*len(line_x), c='k', zorder=1, \
			linewidth=0.2 * increase_size_ratio)
		ax[0].scatter(dots, [item_loop+1]*len(dots), c=labels, zorder=2, \
			s=5 * increase_size_ratio)

	if broad_line:
		hfnxns.easy_fig.format_panel(ax[0], vline_dict={'x':broad_cutoff, \
			'c':'k', 'linewidth':0.1 * increase_size_ratio})
	if sharp_line:
		hfnxns.easy_fig.format_panel(ax[0], vline_dict={'x':sharp_cutoff, \
			'c':'k', 'linewidth':0.1 * increase_size_ratio})

	hfnxns.easy_fig.format_panel(ax[0], \
		ytick_dict=\
			{'ticks': [thing+1 for thing in range(len(sample_lens))]}, \
		yticklabel_dict=\
			{'labels': directories}, \
		ylim=[0, len(sample_lens)+1], \
		xlim=(0,x_max), \
		
		tick_param_dict=\
			{'axis': 'x', 'which': 'major', 'pad': 1, }, \

		legend_dict=\
			{
				'legend_values': percents, 'marker': 'o', 'color': 'w', 
				'markerfacecolor': \
					[color_dict.get(value) for value in percents],
				'markersize': 5, 'whichcorner': 'upper right', 
				'maxwidth': 0.5, 'maxheight': 0.5, 'vdistance': 0,
				'hdistance': 0, 'borderaxespad': 0
			}, \
		)

	hfnxns.make_xticks_kb(ax[0], min_fontsize)

# code for main fig D, H, supp fig E, H
def cancer_gene_barplot_just_plot(ax, peak_list_to_plot, \
	gene_types, types, type_labels, len_or_len_change, cosmic_or_tuson, \
	len_len_change, canvas_renderer, sig_list, deffont=5, x_ax_text_ratio=1, \
	text_lower_ratio=1, y_lim_multi=1.3, xtick_rotation=-45, type_legend=False, \
	type_legend_spot='below', bottom_label_extra_down=1, label_size_multi=1, \
	legend_inside_plot=True, rotate_x=0, x_ha='center', x_va='center', \
	label_markersize=None, label_fontsize=5, legend_labelspacing=0.5, \
	min_fontsize=5, bar_width=0.9, white_writing_size = 1.3, just_stars=True, \
	bar_colors=['C0','C6','C7','C4','C1','C5','C3','C2','C8','C9']):

	xlabels = []
	x_list = []
	color_list = []
	type_spot = []

	if len_or_len_change == 'len':
		groups = ['Broad', 'Sharp']
	else:
		groups = ['Lengthened', 'Shortened', 'No Change']
	data_to_axis = ax.transData + ax.transAxes.inverted()
	current_x = 0

	max_text_height = 0
	for file_loop in range(len(peak_list_to_plot)):
		this_loop_height = sum([len(thing) for thing in \
			peak_list_to_plot[file_loop][:-1]])/len(peak_list_to_plot[file_loop][-1])*100
		if this_loop_height > max_text_height: 
			max_text_height = this_loop_height
	extra_ind = 0
	for file_loop in range(len(peak_list_to_plot)):
		for group_loop in range(len(peak_list_to_plot[file_loop])-1):
			if file_loop % len(groups) != len(groups)-1:
				dist_from_same = len(groups) - (file_loop % len(groups)) - 1
				numerator = len(peak_list_to_plot[file_loop][group_loop])/ \
					len(peak_list_to_plot[file_loop][-1])
				denominator = \
					len(peak_list_to_plot[file_loop+dist_from_same][group_loop])/ \
					len(peak_list_to_plot[file_loop+dist_from_same][-1])
				try:
					perc_change = int(((numerator/denominator)*100)-100)
				except:
					perc_change = int((((numerator+1)/(denominator+1))*100)-100)
				if perc_change >= 0:
					perc_change = str(perc_change)
				else:
					perc_change = str(perc_change)
				if just_stars:
					if sig_list[file_loop - extra_ind][group_loop] == 'sig':
						perc_change = '*'
					else:
						perc_change = ''
			else:
				perc_change = ''
				if group_loop == 0:
					extra_ind += 1
			bar_height = len(peak_list_to_plot[file_loop][group_loop])/ \
				len(peak_list_to_plot[file_loop][-1])*100
			write_down = 0.2 * text_lower_ratio
			fontsize = deffont*white_writing_size
			if fontsize < min_fontsize:
				fontsize = min_fontsize
			if group_loop == 0:
				ax.bar(current_x, bar_height, width=bar_width, color=bar_colors[group_loop])
				ax.text(current_x, 0, perc_change, color='w', ha='center', \
					va='bottom', weight='semibold', fontsize=fontsize)
			else:
				bottom_spot = sum([len(thing) for thing in \
					peak_list_to_plot[file_loop][:group_loop]])/ \
					len(peak_list_to_plot[file_loop][-1])*100
				ax.bar(current_x, bar_height, width=bar_width, bottom=bottom_spot, \
					color=bar_colors[group_loop])
				ax.text(current_x, bottom_spot - write_down, perc_change, \
					color='w', ha='center', va='bottom', weight='semibold', \
					fontsize=fontsize)
		fontsize = deffont*1.0* label_size_multi
		if fontsize < min_fontsize:
			fontsize = min_fontsize
		ax.text(current_x, sum([len(thing) for thing in peak_list_to_plot[file_loop][:-1]])/ \
			len(peak_list_to_plot[file_loop][-1])*100+(max_text_height*0.01), \
			str(sum([len(thing) for thing in peak_list_to_plot[file_loop][:-1]]))+'/'+ \
			str(len(peak_list_to_plot[file_loop][-1])), rotation=90, \
			ha='center', va='bottom', weight='semibold', fontsize=fontsize)

		xlabels.append(groups[file_loop%len(groups)])
		x_list.append(current_x)
		current_x += 1
		if file_loop % len(groups) == len(groups)-1:
			type_spot.append(current_x - (0.5 + len(groups)/2))
			current_x += 0.5

	fontsize = deffont*label_size_multi
	if fontsize < min_fontsize:
		fontsize = min_fontsize

	easy_fig.format_panel(ax, ylim = (0, max_text_height*y_lim_multi))

	ylabels = ax.get_yticks()
	easy_fig.format_panel(ax, 
		xtick_dict = {'ticks': x_list},
		xticklabel_dict = {'labels': [label[0] for label in xlabels], 
			'rotation': 0, 'ha': "center", \
			'fontsize': fontsize, 'weight': 400},
		ytick_dict = {'ticks': ylabels},
		yticklabel_dict = {'labels': [str(round(float(thing),1)) for thing in ylabels], \
			'fontsize': fontsize, 'weight': 400}
	)

	if len_len_change == 'len':
		text_y = -0.039*deffont
	elif len_len_change == 'len_change':
		text_y = -0.06*deffont
	else:
		raise ValueError('oops')
	text_y *= x_ax_text_ratio
	for cancer_loop in range(len(types)):
		fontsize = deffont*1.1*label_size_multi
		if fontsize < min_fontsize:
			fontsize = min_fontsize
		ax.text(data_to_axis.transform((type_spot[cancer_loop],0))[0], text_y, \
			types[cancer_loop], ha=x_ha, va=x_va, transform=ax.transAxes, \
			fontsize=fontsize, weight=400, rotation=rotate_x)
	left, right = ax.get_xlim()
	x_mid = (right-left)/2
	gene_group_y = text_y-(0.01*deffont)*x_ax_text_ratio*bottom_label_extra_down
	fontsize = deffont*1.2*label_size_multi
	if fontsize < min_fontsize:
		fontsize = min_fontsize
	ax.text(0.5, gene_group_y, 'Gene Groups', ha='center', weight='semibold', \
		fontsize=fontsize, transform=ax.transAxes)
	legend_elements = []

	if label_markersize is None:
		label_markersize = deffont*1.2

	if label_fontsize < min_fontsize:
		label_fontsize = min_fontsize

	ylabel_fontsize = deffont*1.3*label_size_multi
	if ylabel_fontsize < min_fontsize:
		ylabel_fontsize = min_fontsize

	easy_fig.format_panel(ax,
		legend_dict=\
			{
				'legend_values': type_labels + groups, 
				'marker': ['o']*len(type_labels) + ['${}$'.format(this_group[0]) for this_group in groups], 
				'color': ['w']*len(type_labels) + ['k'] * len(groups), 
				'markerfacecolor': bar_colors + ['k'] * len(groups),
				'markersize': [label_markersize]*len(type_labels) + [label_markersize*0.75]*len(groups), 
				'lw': 0, 'whichcorner': 'upper right',
				'maxwidth': 1-(deffont*0.0015), 'maxheight': 1-(deffont*0.0015), 'vdistance': 0,
				'hdistance': 0, 'borderaxespad': 0, 'houtside': not legend_inside_plot,
				'prop': {'size': deffont*1.1, 'weight': 'semibold'}, 
				'fontsize': fontsize, 'labelspacing': legend_labelspacing
			}, \
		ylabel_dict = {'ylabel': 'Percent of Genes', 'weight': 'semibold', 'fontsize': fontsize}
	)

# code for main fig G
def kde_size_change_just_plot(ax, color_dict, cancer_types, list_peak_len_change, x_min_max, \
	plot_name, save_spot1, nl_broad, x_ax_text_ratio=1, bandwidth_divider=15, plot_linewidth=1, \
	ymin_multiple=-.05, ymax_multiple=1.8, too_close_x_multi=40, type_label_upshift = 0.00, \
	type_label_weight='semibold', type_label_rotation=50, change_cutoff=2000, change_lw=0.5, \
	change_color='k', ylabel_value='Normalized Peak Density', ylabel_weight='semibold', ylabel_pad=4,
	shortname='Shorten', midname='No Change', longname='Lengthen', xlabel_value='Log2 of Length Change',
	xlabel_weight='semibold'):
	X_plot = np.linspace((x_min_max[0]), (x_min_max[1]), 1000)[:, np.newaxis]
	d = color_dict
	max_spots = []
	cancer_max_list = []
	top_cancer_max = 0
	for loop_num in range(len(cancer_types)):
		to_fit = np.asarray(list_peak_len_change[loop_num])[:, np.newaxis]
		kde = KernelDensity(kernel='gaussian', bandwidth=\
			(((x_min_max[1])-(x_min_max[0]))/bandwidth_divider))\
			.fit(to_fit)
		log_dens = kde.score_samples(X_plot)
		cancer_max = 0
		for plot_loop in range(len(log_dens)):
			if np.exp(log_dens[plot_loop]) > cancer_max:
				cancer_max = np.exp(log_dens[plot_loop])
				max_spot = X_plot[plot_loop]
				if cancer_max > top_cancer_max:
					top_cancer_max = cancer_max
		cancer_max_list.append(cancer_max)
		ax.plot(X_plot[:, 0], np.exp(log_dens), '-',
			label="kernel = '{0}'".format('gaussian'), c=d.get(cancer_types[loop_num]), \
			lw=plot_linewidth)
		max_spots.append(max_spot)

	too_close_list = [0 for i in range(len(cancer_types))]
	keep_going = True
	while keep_going == True:
		this_loop_good = True
		for max_spot_loop in range(len(max_spots)-1,-1, -1):
			for check_loop in range(len(max_spots)):
				if   ((max_spots[max_spot_loop] + ((x_min_max[1]-x_min_max[0])/too_close_x_multi)) > \
					max_spots[check_loop]) and ((max_spots[max_spot_loop]) < max_spots[check_loop]):
					if too_close_list[max_spot_loop] <= too_close_list[check_loop]:
						too_close_list[max_spot_loop] += 1
						this_loop_good = False
				elif ((max_spots[max_spot_loop] + ((x_min_max[1]-x_min_max[0])/(too_close_x_multi/2))) > \
					max_spots[check_loop]) and ((max_spots[max_spot_loop]) < max_spots[check_loop]):
					if too_close_list[max_spot_loop] < too_close_list[check_loop]:
						too_close_list[max_spot_loop] += 1
						this_loop_good = False
		if this_loop_good == False:
			keep_going = True
		else:
			keep_going = False
	for max_spot_loop in range(len(max_spots)):
		line = lines.Line2D([max_spots[max_spot_loop], max_spots[max_spot_loop]], \
			[top_cancer_max*(1.02+too_close_list[max_spot_loop]*0.03+type_label_upshift), \
			cancer_max_list[max_spot_loop]+type_label_upshift], 
			color='k', lw=0.5, transform=ax.transData)
		line.set_clip_on(False)
		ax.add_line(line)
		ax.text(max_spots[max_spot_loop], \
			top_cancer_max*(1.02+too_close_list[max_spot_loop]*0.03+type_label_upshift), \
			cancer_types[max_spot_loop]+': '+str(round(max_spots[max_spot_loop][0],2)), 
			transform=ax.transData, weight=type_label_weight, ha='left', va='bottom', \
			rotation=type_label_rotation, color=d.get(cancer_types[max_spot_loop]))


	easy_fig.format_panel(ax,
		xlim=(x_min_max[0], x_min_max[1]),
		ylim=(top_cancer_max*ymin_multiple, top_cancer_max*ymax_multiple),
		vline_dict=[{'x': change_cutoff,'lw': change_lw, 'c': change_color}, 
			{'x': -change_cutoff,'lw': change_lw, 'c': change_color}],
		ylabel_dict={'ylabel': ylabel_value, 'weight': ylabel_weight, 'labelpad': ylabel_pad}
	)
	info_y = -0.10*x_ax_text_ratio
	label_y = -0.14*x_ax_text_ratio
	ax.text(0.3, info_y,shortname, transform=ax.transAxes, ha='center', va='center')
	ax.text(0.5, info_y,midname, transform=ax.transAxes, ha='center', va='center')
	ax.text(0.7, info_y,longname, transform=ax.transAxes, ha='center', va='center')
	ax.text(0.5, label_y,xlabel_value, transform=ax.transAxes, \
		weight=xlabel_weight, ha='center', va='center')
	
# code for supp fig C
def merge_scatter_all_just_plot(ax, prepped_data, merge_sizes, directories, color_dict, save_dir, \
	pipeline_dirs, dot_size=2.5, line_width=.7, include_legend=False, left_label=False, \
	panel_title='Average by Type', label_markersize=4, label_fontsize=5, \
	legend_labelspacing=0.5, legend_inside_plot=True, set_ylim=None, min_fontsize=5):
	x_list = merge_sizes
	# y = []
	labels = [color_dict.get(thing) for thing in directories]

	maximum = 0
	for sample_ind, sample_type in enumerate(prepped_data.keys()):
		y_list = []
		for merge_size in prepped_data.get(sample_type).keys():
			y_list.append(hfnxns.mean(prepped_data.get(sample_type).get(merge_size).values()))
			for peak_len in prepped_data.get(sample_type)\
				.get(merge_size).values():
				# print(prepped_data.get(sample_type).get(merge_size))
				# print(prepped_data.get(sample_type).get(merge_size).values())
				# raise
				if int(peak_len) > maximum:
					maximum = int(peak_len)
		# print(x_list)
		# print(y_list)
		ax.scatter(x_list, y_list, c=labels[sample_ind], s=dot_size)
		ax.plot(x_list, y_list, c=labels[sample_ind], lw=line_width)

	# print(prepped_data)
	# peaks_per_cancer = []
	# for i in range(len(prepped_data)):
	# 	y = []
	# 	for e in range(len(prepped_data[i])):
	# 		try:
	# 			y.append(hfnxns.mean(prepped_data[i][e]))
	# 		except:
	# 			print(directories[i])
	# 			print(e)
	# 			print(prepped_data[i])
	# 			raise
	# 	ax.scatter(x, y, c=labels[i], s=dot_size)
	# 	ax.plot(x, y, c=labels[i], lw=line_width)
	# maximum = 0
	# for directory_group in prepped_data:
	# 	for size_group in directory_group:
	# 		for item in size_group:
	# 			if int(item) > maximum:
	# 				maximum = int(item)

	if left_label:
		ylabel_dict = {'ylabel': 'Number of peaks', 'labelpad': 3, \
			'weight': 'semibold', 'fontsize': min_fontsize}
		hide_yaxis = False
	else:

		ylabel_dict = None
		hide_yaxis = True
	if set_ylim is None:
		ylim = (0, 5000*int(math.ceil(maximum/5000)))
	else:
		ylim = set_ylim

	if label_fontsize < min_fontsize:
		label_fontsize = min_fontsize

	hfnxns.easy_fig.format_panel(ax,
		ylabel_dict=ylabel_dict,
		hide_yaxis=hide_yaxis,
		ylim=ylim,
		title_dict={'label': panel_title, 'pad': 2, 'weight': 'semibold', 'fontsize': min_fontsize},
		legend_dict={
			'legend_values': directories,
			'marker': 'o', 
			'color': 'w', 
			'markerfacecolor': [color_dict.get(value) for value in directories], 
			'markersize': label_markersize,
			'houtside': not legend_inside_plot,
			'maxwidth': 0.4,
			'maxheight': 0.35,
			'whichcorner': 'upper right',
			'fontsize': label_fontsize, 
			'labelspacing': legend_labelspacing,
			'borderaxespad': 0,
		}
	)
	
	hfnxns.make_xticks_kb(ax, min_fontsize)

# code for main fig B
def common_peak_barplot_just_plot(ax, loop_num, array_names, x_axis, peak_array, \
	width, cancer_types, save_spot, common_counts, no_title=False, rotate_labels=False, \
	types_to_remove = ['E'], ylabel='Number of Peaks', ylabelpad=2, ylim=None, ymulti=1.2, title_pad=3):
	indices_to_remove = []
	for type_loop in range(len(cancer_types)):
		if cancer_types[type_loop] in types_to_remove:
			indices_to_remove.append(type_loop)
	for spot in indices_to_remove:
		del x_axis[-1]
		del peak_array[0][spot]
		del cancer_types[spot]

	barlist = ax.bar(x_axis, peak_array[0], width)
	ax.set_xticks(x_axis)
	ax.set_ylabel(ylabel, labelpad=ylabelpad)
	if ylim is None:
		ylim = (0, max([thing.get_height() for thing in barlist])*ymulti)
	ax.set_ylim(*ylim)
	autolabel_common(ax, barlist, common_counts)
	if rotate_labels is False:
		ax.set_xticklabels(cancer_types, ha='center')
	else:
		ax.set_xticklabels(cancer_types, rotation=rotate_labels, ha='left')

	if no_title is False:
		ax.set_title(str(array_names[0]), pad=title_pad)

# code for supp fig A
def len_quantiles_just_plot(ax, panel_loop, line_name_dict, percents, sample_lens, \
	directories, save_path_len, index_keep, plot_name, panel_num, \
	# directories, save_path_len, keep_line, index_keep, directory_loop, panel_num, \
	x_max=26000, yticklabel_size=5, label_markersize=4, label_fontsize=5, \
	legend_labelspacing=0.5, min_fontsize=5, legend_inside_plot=True, \
	color_list = ('C0', 'C1', 'C2', 'C3', 'C4', 'C5', 'C6', 'C7', 'C8', 'C9'), \
	plot_linewidth=0.2, cutoff_linewidth=0.3, sharp_cutoff=1000, broad_cutoff=4000, 
	sort_ind=5, title_ha='center'):

	percent_color_dict = {}
	for percent_loop in range(len(percents)):
		percent_color_dict[percents[percent_loop]] = color_list[percent_loop]
	labels = [percent_color_dict.get(percent) for percent in percents]
	raw_sample_names = list(sample_lens.keys())
	raw_sample_names = [this_sample[0] for this_sample in sorted(
							[(raw_name, sample_lens.get(raw_name)) for \
							raw_name in raw_sample_names], key=lambda x: x[1][sort_ind])]
	# print([this_sample[1][sort_ind] for this_sample in [(raw_name, sample_lens.get(raw_name)) for \
	# 						raw_name in raw_sample_names]])
	# for item_loop in range(len(sample_lens)):
	sample_names = []
	for item_loop, name in enumerate(raw_sample_names):
		# dots = [int(sample) for sample in sample_lens[item_loop][1:]]
		dots = [int(sample) for sample in sample_lens.get(name)]
		# keep_line.append(dots[index_keep])
		line_x = list(range(dots[0],dots[-1]))
		ax.plot(line_x, [item_loop+1]*len(line_x), c='k', linewidth=plot_linewidth)
		ax.scatter(dots, [item_loop+1]*len(dots), c=labels, s=5)
		sample_name = line_name_dict.get(name)
		if sample_name is None:
			sample_name = line_name_dict.get(name[0]+name[2:])
		if sample_name is None:
			sample_name = line_name_dict.get('M'+name)
		if sample_name is None:
			sample_name = name
		sample_names.append(sample_name.split(' (')[0])
	# for sample in sample_lens:
	if yticklabel_size < min_fontsize:
		yticklabel_size = min_fontsize

	hfnxns.easy_fig.format_panel(ax,
		vline_dict=[
			{'x': broad_cutoff, 'c': 'k', 'linewidth': cutoff_linewidth},
			{'x': sharp_cutoff, 'c': 'k', 'linewidth': cutoff_linewidth}
		],
		ytick_dict={'ticks': [this_tick+1 for this_tick in range(len(raw_sample_names))]},
		yticklabel_dict={'labels': sample_names, 'fontsize': yticklabel_size},
		ylim=[0, len(raw_sample_names)+1],
		xlim=(0,x_max),
		tick_param_dict={'axis': 'x', 'which': 'major', 'pad': 1}
	)
	
	hfnxns.make_xticks_kb(ax, min_fontsize)
	ax.text(0.45, 1.02, plot_name, transform=ax.transAxes, \
		weight = 'semibold', fontsize=min_fontsize, ha=title_ha)
	if panel_loop == 1: 	
		if label_fontsize < min_fontsize:
			label_fontsize = min_fontsize
		if legend_inside_plot:
			left_x = 0.0
			lower_y = 0.0
		else:
			left_x = 0.025
			lower_y = 0.05
		
		hfnxns.easy_fig.format_panel(ax,
			legend_dict={
				'legend_values': ['{}%'.format(percent) for percent in percents],
				'marker': 'o', 'color': 'w', 'markersize': label_markersize, 
				'markerfacecolor': [percent_color_dict.get(value) for value in percents], 
				'houtside': not legend_inside_plot, 'maxwidth': 0.5, 'maxheight': 0.5, 
				'hdistance': left_x, 'vdistance': lower_y, 'borderaxespad': 0, 
				'fontsize': label_fontsize, 'labelspacing': legend_labelspacing, 
				'whichcorner': 'upper right'
			}
		)

# code for main fig C, supp fig C, D, I, J
def path_dots_just_plot(ax, xpositions, ypositions, new_p_vals, color_array, \
	type_list, all_paths, group1_after_pat, group2_after_pat, broad_or_sharp, \
	group1_before_pat='', group2_before_pat='', multiply_s_by=0.35, \
	divider_percent='10%', top_cushion=0, bottom_cushion=0, fontsize=5, \
	min_fontsize=5, num_p_vals = 4, loop_start = -1, divider_xlim=(-2,4), \
	divider_ylim=(-1,1), p_min = 2, dividerpad=0.02, pval_text_height=0.4, \
	pval_dot_height=-0.3, pval_dot_color='k', xlim_pad=1.5, \
	xticklabel_rotation=60, xticklabel_ha='left'):

	xpositions = np.asarray(xpositions)
	ypositions = np.asarray(ypositions)
	new_p_vals = np.asarray(new_p_vals)
	color_array = np.asarray(color_array)

	row_ind = [0, int(new_p_vals.shape[0]/2)]
	if broad_or_sharp == 'broad':
		col_ind = 0
	elif broad_or_sharp == 'sharp':
		col_ind = 1
		avg_pval = []
		for row_loop, row in enumerate(new_p_vals):
			avg_pval.append(hfnxns.mean([row[col_loop] for col_loop in range(len(row)) \
				if col_loop % 2 == col_ind]))
		everything = sorted(zip(avg_pval, new_p_vals, color_array, all_paths), \
			key=operator.itemgetter(0), reverse = True)
		avg_pval, new_p_vals, color_array, all_paths = map(list,zip(*everything))
		xpositions = np.asarray(xpositions)
		ypositions = np.asarray(ypositions)
		new_p_vals = np.asarray(new_p_vals)
		color_array = np.asarray(color_array)

	else:
		raise ValueError('broad_or_sharp should be "broad" or "sharp", was {}'.format(broad_or_sharp))

	for row_loop in range(row_ind[0], row_ind[1]):
		plot_x = [xpositions[row_loop][col_loop] for col_loop in \
		range(len(xpositions[row_loop])) if col_loop % 2 == 0]
		plot_y = [ypositions[row_loop][col_loop] for col_loop in \
		range(len(ypositions[row_loop])) if col_loop % 2 == col_ind]
		plot_p = [new_p_vals[row_loop][col_loop]*multiply_s_by for \
		col_loop in range(len(new_p_vals[row_loop])) if col_loop % 2 == col_ind]
		plot_c = [color_array[row_loop][col_loop] for col_loop in \
		range(len(color_array[row_loop])) if col_loop % 2 == col_ind]
		ax.scatter(plot_x, plot_y, s=plot_p, c=plot_c)

	cax = easy_fig.format_panel(ax, 
		plot_divider_dict={
			'append_axes': 
				{'position': "bottom", 'size': divider_percent, 'pad': dividerpad},
			'hide_xticks': True,
			'hide_yticks': True,
			'hide_xticklabels': True,
			'hide_yticklabels': True,
			'xlim': divider_xlim,
			'ylim': divider_ylim,
		},
	)	

	p_max = max([max(thing) for thing in new_p_vals])
	if p_min is None:
		p_min = min([min(thing) for thing in new_p_vals])

	p_sizes = []
	if fontsize < min_fontsize:
		fontsize = min_fontsize
	for p_loop in range(loop_start,num_p_vals):
		p_sizes.append(int(p_min+((p_max-p_min)*((p_loop+1)/num_p_vals)))*multiply_s_by)
		cax.text(p_loop, pval_text_height, str(int(p_min+((p_max-p_min)*((p_loop+1)/num_p_vals)))), \
			ha='center', va='center', fontsize=fontsize)
	cax.scatter(range(loop_start,num_p_vals), [pval_dot_height for thing in \
		range(loop_start,num_p_vals)], s=p_sizes, c=[pval_dot_color for thing in \
		range(loop_start,num_p_vals)])
	xtick_spots = list(range(0,new_p_vals.shape[1],2))

	labels_list = []
	for type_loop, this_type in enumerate(type_list):
		if broad_or_sharp == 'broad':
			labels_list.append(group1_before_pat+this_type)

		elif broad_or_sharp == 'sharp':
			labels_list.append(group2_before_pat+this_type)
	if top_cushion != 0 or bottom_cushion != 0:
		bottom, top = ax.get_ylim()
		if top > bottom:
			top += bottom_cushion
			bottom -= top_cushion
		else:
			top -= top_cushion
			bottom += bottom_cushion
	else:
		bottom, top = ax.get_ylim()

	easy_fig.format_panel(ax, 
		xlim=(xtick_spots[0] - xlim_pad, xtick_spots[-1] + xlim_pad),
		xtick_dict={'ticks': xtick_spots},
		xticklabel_dict={'labels': labels_list, 'rotation': xticklabel_rotation, \
			'ha': xticklabel_ha, 'fontsize': fontsize
		},
		ytick_dict={'ticks': range(row_ind[0], row_ind[1])},
		yticklabel_dict={'labels': all_paths, 'fontsize': fontsize
		},
		xtick_top=True,
		invert_y=True,
		ylim=(bottom, top)
	)

# code for supp fig B
def split_plot_just_plot(ax, log_norm, peak_vals, len_bins, dens_bins, \
	vmin, vmax, xlimits, ylimits, vert_line, name_var, \
	save_path, slopes, max_x_ratio, max_y_ratio, sub_x_labelpad=0.5, ylabel_size=5, xlabel_size=5, \
	min_fontsize=5, vert_line_color='k', vert_line_width=1):
	if log_norm == False:
		my_hist = ax.hist2d([peak[1] for peak in peak_vals], 
			[peak[0] for peak in peak_vals], bins=[len_bins,dens_bins], \
			vmin=vmin, vmax=vmax)
	elif log_norm == True:
		my_hist = ax.hist2d([peak[1] for peak in peak_vals], 
			[peak[0] for peak in peak_vals], bins=[len_bins,dens_bins], norm=LogNorm())
	# if log_norm == False:
	# 	my_hist = ax.hist2d(lengths, densities, bins=[len_bins,dens_bins], \
	# 		vmin=vmin, vmax=vmax)
	# elif log_norm == True:
	# 	my_hist = ax.hist2d(lengths, densities, bins=[len_bins,dens_bins], norm=LogNorm())

	if xlabel_size < min_fontsize:
		xlabel_size = min_fontsize
	if ylabel_size < min_fontsize:
		ylabel_size = min_fontsize

	if vert_line != None:
		vline_dict={'x': vert_line, 'c': vert_line_color, 'lw': vert_line_width}
	else:
		vline_dict=None

	hfnxns.easy_fig.format_panel(ax,
		xlim=xlimits,
		ylim=ylimits,
		vline_dict=vline_dict,
		xlabel_dict={'xlabel': 'Peak Lengths', 'labelpad': sub_x_labelpad, 'fontsize': xlabel_size},
		ylabel_dict={'ylabel': 'Peak Densities', 'labelpad': 0.5, 'fontsize': ylabel_size},
		title_dict={'label': name_var.split('_')[0], 'pad': 2.5, \
			'weight': 'semibold', 'fontsize': min_fontsize},
		plot_divider_dict={
			'append_axes': {'position': "right", 'size': "5%", 'pad': 0.05},
			'colorbar': {'mappable': my_hist[3]}
		}
	)

	hfnxns.make_xticks_kb(ax, min_fontsize)

if __name__ == '__main__':
	analysis_dir = \
		'/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_04_16/E'
	other_plots_dir = \
		'/Users/jschulz1/Documents/project_data/ccle_project/other_plots_for_broadpeak_figs'
	# other_plots_dir = \
	# 	'/home/jschulz1000/Documents/ccle_stuff/inputs/other_plots_for_broadpeak_figs'
	poster_dir = \
		'/Users/jschulz1/Documents/presentations/science_day/poster_plots'

	sample_name_file = \
		'/Users/jschulz1/Documents/project_data/ccle_project/raw_data/used_in_analysis/formatted_ccle_cell_line_names.csv'
	line_name_dict = {}
	line_list = []
	with open(sample_name_file, 'r') as textfile:
		for line in textfile:
			line = line.rstrip('\n').split(',')
			line_name_dict[line[1]] = line[0]
			line_list.append(line[1])

	fig_dir = \
		'/Users/jschulz1/Documents/project_data/ccle_project/figure_files'
	other_fig_dir = \
		'/Users/jschulz1/Documents/project_data/ccle_project/figure_files/Broad_Figure_Update'

	# save_date = '2020_05_04_svg'
	save_date = '2020_05_04'
	save_formats = ['svg', 'png']
	# for which_len_change in ['no_cutoff', 'double', 'same']:
	for save_format in save_formats:
		for which_len_change in ['double']:
			ccle_main_figure(fig_dir, line_name_dict, analysis_dir, 
				other_fig_dir, 
				'broadpeak_main_fig_{save_date}_{save_format}_{analysis_type}.{save_format}'.format(
					save_date=save_date, analysis_type=which_len_change, save_format=save_format), 
				save_show='save', which_len_change=which_len_change, save_format=save_format)
			new_ccle_supp_fig(fig_dir, line_name_dict, analysis_dir, 
				other_plots_dir, other_fig_dir, 
				'broadpeak_supp_fig_{save_date}_{save_format}_{analysis_type}.{save_format}'.format(
					save_date=save_date, analysis_type=which_len_change, save_format=save_format), 
				save_show='save', which_len_change=which_len_change, save_format=save_format)

	# which_len_change = 'double'
	# ccle_main_figure(fig_dir, line_name_dict, analysis_dir, \
	# 	other_fig_dir, 'broadpeak_main_fig_2020_04_28_double.png', save_show='save', 
	# 	which_len_change=which_len_change)
	# new_ccle_supp_fig(fig_dir, line_name_dict, analysis_dir, \
	# 	other_plots_dir, other_fig_dir, 'broadpeak_supp_fig_2020_04_28_double.png', \
	# 	save_show='save', which_len_change=which_len_change)

	# which_len_change = 'same'
	# ccle_main_figure(fig_dir, line_name_dict, analysis_dir, \
	# 	other_fig_dir, 'broadpeak_main_fig_2020_04_28_same.png', save_show='save', 
	# 	which_len_change=which_len_change)
	# new_ccle_supp_fig(fig_dir, line_name_dict, analysis_dir, \
	# 	other_plots_dir, other_fig_dir, 'broadpeak_supp_fig_2020_04_28_same.png', \
	# 	save_show='save', which_len_change=which_len_change)

	# ccle_main_figure(fig_dir, line_name_dict, analysis_dir, \
	# 	other_fig_dir, 'broadpeak_main_fig_2020_03_31.png', save_show='save')
	# new_ccle_supp_fig(fig_dir, line_name_dict, analysis_dir, \
	# 	other_plots_dir, other_fig_dir, 'broadpeak_supp_fig_2020_03_24.png', \
	# 	save_show='save')

























































