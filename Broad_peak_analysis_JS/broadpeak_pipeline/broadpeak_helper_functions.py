import collections
import copy
import csv
import datetime
import glob
import json
import logging

from matplotlib.lines import Line2D
from matplotlib import lines
from matplotlib.colors import LogNorm
import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable

import math
import multiprocessing as mp
import numpy as np
import operator
import os
import pickle
import random

from rpy2.robjects.packages import importr
from rpy2.robjects.vectors import FloatVector
statsr = importr('stats')

import scipy.stats as stats
from scipy.stats import chisquare
import shlex

from sklearn import mixture
from sklearn.model_selection import GridSearchCV
from sklearn.neighbors import KernelDensity

import statistics
from statsmodels.stats.proportion import proportions_ztest

import shutil
import subprocess
import sys

from termcolor import colored
from textwrap import wrap

import _thread
import threading
import traceback

import time




# this file contains all broadpeak_pipeline helper functions

def format_time(total_s):
	s_in_m = 60
	s_in_hr = 60*60
	hrs = int(total_s/s_in_hr)
	mins = int((total_s % s_in_hr)/s_in_m)
	secs = int(total_s % s_in_m)
	return '{}hrs, {}mins, and {}s'.format(hrs, mins, secs)

class pool_wrapper(object):
	def __init__(self, cpu_count=None, let_child_processes_fail=False, extra_queue=1, 
		error_message_first_line_color='red', check_mutated = True, *args, **kwargs):
		# argument descriptions
			# cpu_count allows users to define the number of process in mp.Pool()
				# defaults to None -> mp.Pool() uses mp.cpu_count()
			# let_child_processes_fail should probably stay False:
				# if False: main process is killed when child processes error
				# if True: child process errors will be printed but the main
					# process will continue going. This isn't recommended
					# for the same reason that a blanket try-except isn't recommended
			# error_message_first_line_color sets the color for the first line of error
				# messages. This only matters if:
					# 1. you don't want to install termcolor
						# in which case, set this to None
					# 2. you want to customize this color (use colors available for
						# termcolor.colored)

		# __init__ variable explanations
			# making the multiprocessing workers that this is built around
				# args/kwargs to __init__ are passed directly to mp.Pool()
			# extra_queue is used when submitting jobs to the pool
				# it counts the number of extra jobs submitted (beyond 1 per
					# process). This exists so submission doesn't slow down the program
			# self.error_queue is passed to child processes to send error messages
				# back to the main process -> process to provide transparency 
				# regarding child process errors
			# self.error_thread is the thread that the error checker runs in
				# error checker = self.check_for_errors()
				# this checks the error_queue every 2s and if an error message has
					# been sent, it 
			# self.added_jobs is a dictionary storing added jobs that have not
				# yet been submitted to self.pool:
				# keys = job ids
				# values = inputs required to submit a job to self.pool
			# self.submitted_jobs is a dictionary storing the async results
				# of jobs that have been submitted to the pool but aren't done yet:
				# keys = job ids
				# values = asyncResult object for this job
			# self.finished_jobs is a dictionary storing async results for 
				# completed jobs:
				# keys = job ids
				# values = asyncResult object for this job
			# self.job_groups is a dictionary storing lists of jobs associated
				# with a specific job group for later prioritization:
				# keys = job group name
				# values = list of job ids associated with the group
			# self.all_submitted_async_results is a dictionary storing the async results
				# of all jobs that have been submitted to the pool:
				# jobs are not removed from this dictionary when completed
				# keys = job ids
				# values = asyncResult object for this job
			# self.error_queue_messages is a dictionary storing error messages from 
				# failed child processes:
				# keys = job id of failed child process
				# values = traceback returned from child process using self.error_queue
			# self.job_added_order is a list of job ids added to the object
				# order is important because jobs will be submitted to the pool in the
				# order that they were added unless some function requires prioritization
			# self.priority_job_order is empty unless a function calls for prioritization
				# this where the job group-prioritized job list is stored. This list
				# takes priority over self.job_added_order for completion if not empty
				# for jobs in the prioritized group, this preserves added order
			# self.priority_job_groups is a list of currently prioritized job groups
				# if newly added jobs are in these groups, they're added straight to
					# the priority job list
				# this is set by self.prioritize_specific_job_group(job_group)
				# and is reset by default when blocking
					# set reset_prio=False when blocking to keep priority groups

		if cpu_count is None:
			self.cpu_count = mp.cpu_count()
			self.pool = mp.Pool(*args, **kwargs)
		else:
			self.pool = mp.Pool(cpu_count, *args, **kwargs)
			self.cpu_count = cpu_count

		# user supplied settings as described above
		self.let_child_processes_fail = let_child_processes_fail
		self.error_message_first_line_color = error_message_first_line_color

		# setting up self.error_queue
		self.manager = mp.Manager()		
		self.error_queue = self.manager.Queue()
		self.results_queue = self.manager.Queue()

		# setting up intended_active_jobcount
		self.intended_active_jobcount = self.cpu_count + extra_queue

		self.check_mutated = check_mutated

		# setting up job id set
		self.all_job_ids = set()

		# setting up job dictionaries to keep track of job objects
		self.added_jobs = {}
		self.submitted_jobs = {}
		self.finished_jobs = {}
		self.job_groups = {}
		self.all_submitted_async_results = {}

		# setting dictionary of error queue messages
		self.error_queue_messages = {}
		self.job_outputs = {}

		# setting up ordered lists of submitted job ids for job queueing order
		self.job_added_order = []
		self.priority_job_order = []
		self.priority_job_groups = []

		# setting up error checking thread
		self.lock = threading.Lock()
		self.error_thread = threading.Thread(target=self.check_for_errors, daemon=True)
		self.error_thread.start()
	
	def get_unfinished_jobs(self):
		return_dict = {}
		for job_group in self.job_groups:
			if self.job_groups.get(job_group) is not None:
				if len(self.job_groups.get(job_group)) > 0:
					return_dict[job_group] = [job_id for job_id in 
						self.job_groups.get(job_group)
						if self.finished_jobs.get(job_id) is None]
		return return_dict

	# this function is run in self.error_thread to check for error messages
		# the thread is daemon=True, so this runs self.raise_error_message every
		# 2 seconds for the duration of the script 
	def check_for_errors(self):
		start_time = time.time()
		loop_time = 5
		no_update_time = 20
		this_no_u = no_update_time
		while True:
			with self.lock:
				self.check_queues()
			this_no_u -= loop_time
			if this_no_u <= 0:
				ongoing_job_count = len([job_id for job_id in 
					self.all_submitted_async_results.values() if 
					not job_id.ready()])
				waiting_job_count = len(list(self.added_jobs.keys()))
				unfinished_scores = self.get_unfinished_jobs()
				group_counts = ['{}:{}'.format(this_key, \
								len(unfinished_scores.get(this_key))) \
								for this_key in \
								unfinished_scores.keys() if \
								len(unfinished_scores.get(this_key)) > 0]
				if len(group_counts) > 0:
					group_counts_str = 'Groups: '+', '.join(group_counts)
				else:
					group_counts_str = ''
				print(colored(
					'{} ongoing jobs, {} waiting to submit. {} into running.{}'
						.format(ongoing_job_count, waiting_job_count, 
							format_time(round(time.time()-start_time,2)), 
							group_counts_str
							), 'blue'))
				this_no_u = no_update_time
			time.sleep(loop_time)

	def check_queues(self):
		found_errors = False
		while not self.error_queue.empty():
			found_errors = True
			error_output = self.error_queue.get()
			self.error_queue_messages[error_output[0]] = error_output[1]

		if found_errors:
			for this_key in sorted(list(self.error_queue_messages.keys())):
				mess1 = this_key
				mess2 = [inner_key for inner_key in self.job_groups.keys() if
								this_key in list(self.job_groups.get(inner_key))]
				if self.error_message_first_line_color is not None:
					print(colored('Error for Job {} of job group {}:'
						.format(mess1, mess2), 
								self.error_message_first_line_color))
				else:
					print('Error for Job {} of job group {}:'
						.format(mess1, mess2))
				print(self.error_queue_messages.get(this_key))
			if not self.let_child_processes_fail:
				if self.error_message_first_line_color is not None:
					print(colored(
						'Killing main process because errors arose in child processes',
							self.error_message_first_line_color))
				else:
					print(
						'Killing main process because errors arose in child processes',
							)
				_thread.interrupt_main()

		while not self.results_queue.empty():
			results_output = self.results_queue.get()
			self.job_outputs[results_output[0]] = results_output[1]

	def block_for_specific_result(self, job_id):
		if self.added_jobs.get(job_id) is not None:
			with self.lock:
				if job_id in self.priority_job_order:
					self.priority_job_order.remove(job_id)
				self.priority_job_order.insert(0, job_id)
			while self.added_jobs.get(job_id) is not None:
				self.submit_job()
				time.sleep(0.1)

		while self.finished_jobs.get(job_id) is None:
			time.sleep(0.5)

		if self.finished_jobs.get(job_id) is not None:
			with self.lock:
				self.check_queues()
			return self.job_outputs.get(job_id)

	# quick helper function to list job ids in self.added_jobs
	def get_added_job_ids(self):
		return list(self.added_jobs.keys())

	# quick helper function to list job ids in self.submitted_jobs
	def get_submitted_job_ids(self):
		return list(self.submitted_jobs.keys())

	# quick helper function to list job ids in self.finished_jobs
	def get_finished_job_ids(self):
		return list(self.finished_jobs.keys())

	# print the list of added but not yet submitted job ids for a specific job group
		# this is used by self.block_for_specific_job_group and 
		# self.prioritize_specific_job_group to grab a list of waiting jobs in a
		# job group to prioritize job submission
	def get_waiting_jobs_by_group(self, job_group):
		added_job_ids = self.get_added_job_ids()
		job_group_jobs = list(self.job_groups.get(job_group))
		return [job_id for job_id in job_group_jobs if job_id in added_job_ids]

	# this checks how many jobs are currently submitted and the number of jobs waiting
		# to be submitted, and submits a new job if:
			# currently submitted < intended total submitted at a time
			# and
			# there are jobs waiting ot be submittes
	def should_submit_job(self):
		return (len(self.get_submitted_job_ids()) < self.intended_active_jobcount) and \
			(len(self.get_added_job_ids()) > 0)

	# this is a callback passed for wrap_apply_async. it gets called whenever a job ends
		# important: asyncResult.ready() (aka job done) only becomes True after this ends
			# you also can't check asyncResult.successful() until after .ready() is True
			# so this uses the error_queue to see if there was an issue
		# this also removed the job from the submitted dict
		# this also submits more jobs for as long as self.should_submit_job() is True
			# there is no reason that it should submit more than 1 job, but
				# i put it in a while loop just in case...
	def completed_job(self, job_id):
		with self.lock:
			job_result = self.submitted_jobs.get(job_id)
			del self.submitted_jobs[job_id]
			self.finished_jobs[job_id] = job_result
		while self.should_submit_job():
			self.submit_job()

	# this just generates a unique job id every time a job is added
		# there's probably a faster way to do this (eg, just iterate a number)
			# but if i ever decide to delete completed jobs and use released number,
			# this is more flexible. The slow down is also probably basically irrelevant
	def new_job_id(self):
		current_id = 0
		while True:
			if current_id in self.all_job_ids:
				current_id += 1
			else:
				break
		return current_id

	# this is the function that gets passed to child queues
		# everything that users touch should occur inside the try-except block ->
			# this should avoid all dropped errors in the child process
		# formatted error tracebacks are send back up through the error_queue
			# to be printed in the main process
		# job_id is returned and picked up by self.completed_job so that
			# the object can log which jobs are completed
	def wrap_apply_async(error_queue, result_queue, job_id, check_mutated, 
		fnxn, args=None, kwargs=None, job_group = None):
		job_time = time.time()
		if args is None:
			args = []
		if kwargs is None:
			kwargs = {}

		try:
			if check_mutated:
				copied_args = [copy.deepcopy(this_arg) for this_arg in args]

			fnxn_result = fnxn(*args, **kwargs)

			if check_mutated:
				if any([copied_args[arg_loop] != args[arg_loop] for arg_loop in 
						range(len(args))]):
					raise ValueError('This function changed input args in a \
						\n\tchild process, this raises an error by default \
						\n\tbecause it can cause unexpected behavior. Advanced \
						\n\tusers can stop with error with "check_mutated=False" \
						\n\twhencreating the pool_wrapper object')

		except Exception as exep:
			exc_group, exc_value, exc_tb = sys.exc_info()
			error_queue.put((job_id, 
				''.join(traceback.format_exception(exc_group, exc_value, exc_tb))))
			return

		if fnxn_result is not None:
			try:
				test_fnxn_result = pickle.dumps(fnxn_result)
			except:
				error_queue.put((job_id, 
				'Cannot pickle function output. Outputs of functions run \
					\n\tasync usingpool_wrapper must be pickleable in order \
					\n\tto send back to main process'))
			result_queue.put((job_id, fnxn_result))

		if job_group is not None:
			group_str = ', group {} '.format(job_group)
		else:
			group_str = ''
		print(colored('job {} took {}s{}'.format(job_id, 
			round(time.time() - job_time, 1), group_str), 
			'white'))
		return job_id

	def job_group_exists(self, job_group):
		return self.job_groups.get(job_group) is not None

	# this blocks the process until all jobs from a specific job group are completed
		# used for example in broadpeak pipeline to ensure that all pathway analyses
			# are completed before path dots are called
		# has a few advantages compared to vanilla pool.close(); pool.join():
			# this does not close the pool (doesn't require re-opening it)
			# this runs prioritized jobs first (minimize blocked time)
			# all jobs can be run asynchronously through apply_async
				# with blocking that doesn't require you to keep track of
				# any specific state variable
		# can take a single group name or a list of group names
			# if a list is provided: prioritizes jobs from all provided groups
		# first part is same as self.prioritize_specific_job_group():
			# filter self.job_added_order -> self.priority_job_order
			# self.priority_job_order jobs are run before self.job_added_order jobs
			# jobs in self.priority_job_order are still run in the order they were added
		# next it submits all jobs in self.priority_job_order (ignoring normal jobcount)
		# lastly, it blocks until all jobs from all groups are done
			# grab_job_results() returns list of asyncResults for all prioritized jobs
		# if verbose=True, it will print # of jobs left every 2s if it decreases
	def block_for_specific_job_group(self, job_group, reset_prio=True, verbose=True):
		def grab_job_results(self, job_group, str_or_list):
			if str_or_list == 'str':
				return [self.all_submitted_async_results.get(job_id) for \
					job_id in list(self.job_groups.get(job_group))]
			elif str_or_list == 'list':
				return_list = []
				for this_group in job_group:
					return_list.extend([self.all_submitted_async_results.get(job_id) for \
						job_id in list(self.job_groups.get(this_group))])
				return return_list
			else:
				raise

		if reset_prio:
			self.priority_job_groups = []
		if isinstance(job_group, str):
			if self.job_groups.get(job_group) is None:
				raise ValueError(
					'called pool_wrapper.block_for_specific_job_group for group: {}\
					\n\tbut job group is not in the queue')
			str_or_list = 'str'
			with self.lock:
				jobs_to_submit = self.get_waiting_jobs_by_group(job_group)
				self.priority_job_order = [job_id for job_id in self.job_added_order if\
					job_id in jobs_to_submit]
		elif hasattr(job_group, '__iter__'):
			str_or_list = 'list'
			self.priority_job_order = []
			with self.lock:
				for this_group in job_group:
					if self.job_groups.get(this_group) is None:
						raise ValueError(
							'called pool_wrapper.block_for_specific_job_group for group: {}\
							\n\tbut job group is not in the queue')
					jobs_to_submit = self.get_waiting_jobs_by_group(this_group)
					self.prioritize_specific_job_group.extend([
						job_id for job_id in self.job_added_order if\
							job_id in jobs_to_submit])

		while len(self.priority_job_order) > 0:
			self.submit_job()

		ongoing_jobs = grab_job_results(self, job_group, str_or_list)

		last_job_left_count = len([job_id for job_id in ongoing_jobs if \
				not job_id.ready()])
		while not all([job_id.ready() for job_id in ongoing_jobs]):
			if verbose is True:
				new_job_left_count = len([job_id for job_id in ongoing_jobs if \
					not job_id.ready()])
				if new_job_left_count < last_job_left_count:
					print('Blocking for {} more jobs from group {}'.format(
						new_job_left_count, job_group))
					last_job_left_count = new_job_left_count
			time.sleep(1)
		
	# this prioritizes a specific job group w/o blocking
		# useful to get ahead if you know are going to block for this job group soon
			# eg, like example above: can start prioritizing pathways ahead of time
		# same as the first step of self.block_for_specific_job_group()
		# this also sets self.priority_job_groups(job_group) so that future incoming
			# jobs from the prioritized job group are added to the priority list
			# this variable can be optionally reset when blocking
			# or, if absolutely necessary, it can be manually reset with: 
				# {wrapper object name}.priority_job_groups = []
			# this should not be necessary basically ever though...
	def prioritize_specific_job_group(self, job_group):
		if isinstance(job_group, str):
			if self.job_groups.get(job_group) is None:
				raise ValueError(
					'called pool_wrapper.block_for_specific_job_group for group: {}\
					\n\tbut job group is not in the queue')
			with self.lock:
				self.priority_job_groups.append(job_group)
				jobs_to_submit = self.get_waiting_jobs_by_group(job_group)
				self.priority_job_order = [job_id for job_id in self.job_added_order if\
					job_id in jobs_to_submit]
		elif hasattr(job_group, '__iter__'):
			self.priority_job_order = []
			with self.lock:
				for this_group in job_group:
					if self.job_groups.get(this_group) is None:
						raise ValueError(
							'called pool_wrapper.block_for_specific_job_group for group: {}\
							\n\tbut job group is not in the queue')
					self.priority_job_groups.append(this_group)
					jobs_to_submit = self.get_waiting_jobs_by_group(this_group)
					self.prioritize_specific_job_group.extend([
						job_id for job_id in self.job_added_order if\
							job_id in jobs_to_submit])

	# this submits added jobs from the added job dict to mp.Pool's queue
		# added in order based first on the order of items in the priority job list
			# then on the order of items in the job_added_order list
		# main functions: actually calls mp.pool.apply_async()
			# moves variables from "added" dicts/lists -> "submitted"
		# most of the complicated conceptual steps happen when adding or in the callback
	def submit_job(self):
		also_rem_add_order = False
		if self.priority_job_order is not None:
			if len(self.priority_job_order) > 0:
				get_list = self.priority_job_order
				also_rem_add_order = True
			else:
				get_list = self.job_added_order
		else:
			get_list = self.job_added_order
		if len(get_list) < 1:
			raise ValueError('should not be running submit_job if len of added lists are 0')
		with self.lock:
			next_job_id = get_list.pop(0)
			job_info = self.added_jobs.get(next_job_id)
			del self.added_jobs[next_job_id]
			if also_rem_add_order:
				self.job_added_order.remove(next_job_id)

		async_result = self.pool.apply_async(pool_wrapper.wrap_apply_async,
			job_info, kwds={'job_group': str([inner_key for inner_key in 
						self.job_groups.keys() if
						next_job_id in list(self.job_groups.get(inner_key))])}, 
			callback=self.completed_job)

		with self.lock:
			self.submitted_jobs[next_job_id] = async_result
			self.all_submitted_async_results[next_job_id] = async_result

	# just returns asyncResult.ready() for all jobs in self.all_submitted_async_results dict
		# used when blocking to check if done
	def check_all_submitted_if_done(self):
		return all([this_job.ready() for this_job in 
			list(self.all_submitted_async_results.values())])

	# as the name says, it returns a list of asyncResult objects for all unfinished
		# jobs. It could return an actual count, but this is more flexible
		# this also lets other functions monitor unfinished jobs on their own in wanted
	def count_unfinished_jobs(self):
		return len([this_job for this_job in 
			list(self.all_submitted_async_results.values()) if not this_job.ready()])

	# this adds jobs to the object's queues. This should be the main function used
		# to add jobs. jobs should never be submitted manually
		# basic workflow:
			# it generates a unique job id and adds it to the set of unique job ids
			# it manages self variables:
				# adds the job to a job_group list of it has a job group
				# adds the job to self.job_added_order list
				# if job group is prioritized: also adds to self.priority_job_order list
				# adds to self.added_jobs dict
				# also of these are required for further job management
		# by default, this  checks that all submitted args for apply_async can be pickled.
			# this slows down running but is recommended because errors due to running
			# apply_async on un-picklable objects is silent and difficult to identify
			# this should probably not be turned off unless you know what you're doing
			# it can be turned off using: pickle_check=False
	def add_job(self, fnxn, args=None, kwargs=None, job_group=None, pickle_check=True):
		job_id = self.new_job_id()
		self.all_job_ids.add(job_id)
		if job_group is not None:
			with self.lock:
				if self.job_groups.get(job_group) is None:
					self.job_groups[job_group] = [job_id]
				else:
					self.job_groups[job_group].append(job_id)

		if not callable(fnxn):
			raise ValueError('The "fnxn" argument to pool_wrapper.add_job must\
				\n\tbe a callable object, eg a function')
		if not isinstance(args, tuple) and not isinstance(args, list) and args is not None:
			raise ValueError('The "args" argument to pool_wrapper.add_job must\
				\n\tbe a list or a tuple or None. Was {}'.format(args))
		if not isinstance(kwargs, dict) and kwargs is not None:
			raise ValueError('The "kwargs" argument to pool_wrapper.add_job must\
				\n\tbe a dict or None. Was {}'.format(group(kwargs))) 

		job_info = (self.error_queue, self.results_queue, job_id, \
					self.check_mutated, fnxn, args, kwargs) 
		with self.lock:
			self.job_added_order.append(job_id)
			self.added_jobs[job_id] = job_info
		
		if pickle_check:
			try:
				test_job_info = pickle.dumps(job_info)
			except:
				for item in job_info:
					try:
						test_item = pickle.dumps(item)
					except:
						raise ValueError('Cannot pickle job input. \
							\n\tThis could cause confusing errors. Failed input:\
							\n\t{}'.format(item))
		
		# want this down here because I want to be sure that this job is
			# formatted and tested before it can possibly be submitted
		if job_group is not None:
			with self.lock:
				if job_group in self.priority_job_groups:
					self.priority_job_order.append(job_id)

		while self.should_submit_job():
			self.submit_job()

		return pool_wrapper.get_job_result(self, job_id)

	class get_job_result(object):
		def __init__(self, parent_pool_wrapper, job_id):
			self.job_id = job_id
			self.parent_pool_wrapper = parent_pool_wrapper

		def result(self):
			return self.parent_pool_wrapper.block_for_specific_result(self.job_id)
		
	# this blocks while all added jobs are completed
		# it submits all outstanding jobs (still prioritizes priority list)
		# it then loops through time.sleep(1) until all jobs end
		# if verbose=True, this will print the number of outstanding jobs
			# whenever a jobs ends every second or longer
		# by default, it will reset the priority job groups going forward
			# can set reset_prio=False to keep priority groups
	def block_for_all_jobs(self, reset_prio=True, verbose=True):
		if reset_prio:
			self.priority_job_groups = []
		while len(self.get_added_job_ids()) > 0:
			self.submit_job()

		loop_time = 5
		no_update_time = 30
		this_no_u = no_update_time
		last_job_left_count = self.count_unfinished_jobs()
		while len(self.get_submitted_job_ids()) > 0:
			if verbose is True:
				new_job_left_count = self.count_unfinished_jobs()
				if new_job_left_count < last_job_left_count:
					print('{} jobs still ongoing'.format(new_job_left_count))
					last_job_left_count = new_job_left_count
			time.sleep(loop_time)
			this_no_u -= loop_time
			if this_no_u <= 0:
				print('{} jobs still ongoing. No update in {}s'
					.format(new_job_left_count, no_update_time))
				this_no_u = no_update_time

	# this blocks for all remaining jobs and runs pool.close() + pool.join()
		# this basically ends use of this object
		# use of this isn't really recommended because I don't think this terminates
			# child processes, so it's not recommended 
			# to close this and start multiple pool_wrappers
	def close_wrapper(self, verbose=True):
		self.block_for_all_jobs(verbose=verbose)
		self.pool.close()
		self.pool.join()


def filename_to_plot_name(filename):
	title_words = filename.split('_')
	first_letters = [word[0].upper() for word in title_words]
	title_name = ' '.join([first_letters[word_loop]+title_words[word_loop][1:] for \
		word_loop in range(len(title_words))])

	return title_name

# given 2 numbers, checks if both are on the same side of 0, aka both + or both -
def check_same_side(inp1, inp2):
	return sum([inp1>0, inp2>0]) != 1

def num_diff(num1, num2):
	return max(num1, num2) - min(num1, num2)


class easy_fig(object):
	# figsize is passes directly to plt.figure(figsize=)
	# subplot edge values are passed directly to subplots_adjust
	# gridsize used for subplots2grid: increasing increases plot placement granularity
	# .axes attribute is a list holding all subplots. can grab them by index
		# appended every time .add_panel is called
	def __init__(self, figsize=(12,8), gridsize=(1000,1000), \
		subplot_left=0.05, subplot_right=0.95, subplot_bottom=0.10, \
		subplot_top=0.90):
		self.gridsize = gridsize
		self.figsize = figsize
		self.fig = plt.figure(figsize=figsize)
		self.renderer = self.fig.canvas.get_renderer()
		plt.subplots_adjust(left=subplot_left, right=subplot_right, \
			bottom=subplot_bottom, top=subplot_top)
		self.axes = []

	# all args passed directly to variable_size_panel_list, output subplot list is appended to .axes list
	def add_panel(self,top_left=(0,0), bottom_right=(1,1), \
		x_count=1, y_count=1, subpanel_pad_percent=(0,0), \
		panel_pad_percent = 0, img_shapes=None, vratio=None, hratio=None):

		self.axes.append(variable_size_panel_list(self.gridsize, top_left, \
			bottom_right,x_count,y_count,subpanel_pad_percent, \
			panel_pad_percent, img_shapes, vratio, hratio))

	# just adds text to the figure. reverses ypos: ypos=(1-ypos) so 0 is top and 1 is bottom
		# because that fits my expectation better
	# can supply panelnum/axnum indices to add this to a specific panel, defaults to 0, 0
	# i typically pass transform=fig.transFigure in kwargs for figures so that the positions
		# are based on the position in the entire figure rather than for a specific panel/subplot
	def add_text(self, xpos, ypos, *args, panelnum=None, \
		axnum=None, **kwargs):
		if panelnum is None:
			panelnum=0
		if axnum is None:
			axnum=0
		self.axes[panelnum][axnum].text(xpos, 1-ypos, *args, **kwargs)

	# very light instance method wrapper of format_panel. Can call this on specific easy_fig object and pass
		# panelnum + axnum to format a specific subplot of that figure
	def format_plot(self, panelnum, axnum, *args, **kwargs):
		easy_fig.format_panel(self.axes[panelnum][axnum], *args, **kwargs)
	
	# this doesn't need to be a method but i prefer that organization.
		# takes a subplot and formats it based on arguments
	# 2 main kinds of formatting:
		# 1. simplifying complex formatting that i spent time to figure out and don't want to remember:
			# eg. adding highly flexible legends
				# splitting an ax to add a colorbar as in the 2d histograms
			# using these is highly recommended if you're doing this formatting.
				# it will save a lot of googling and reading matplotlib documentation
		# 2. simple formatting (adding title, set_xlim):
			# done by passing a dict that is passed as **kwargs to the formatting method
			# allows formatting a bunch of features of a plot in 1 method call. up to personal preference
			# whether or not you use it
	# description of arguments:
		# group 1:
			# hide_axis: calls .axis('off'), used to remove all formatting for a subplot
				# useful to hide formatting for imshow, or if a subplot needs to be completely hidden
				# set to True if needed
			# xlog: calls.set_xscale('log') to set x axis to logscale
				# set to True if needed
			# ylog: same as xlog but for y axis
			# xlim: calls .set_xlim(xlim) to set x axis limits. takes 2 int/float tuple/list
			# ylim: same as xlim but for y axis
			# hide_xaxis: calls .get_xaxis().set_visible(False) to hide xticks + xticklabels on a subplot
				# used to make x axis of a subplot look simple/pretty
				# set to True if needed
			# hide_yaxis: same as hide_xaxis but for y axis
			# hide_xticks: calls panel.tick_params(axis=u'x', which=u'both',length=0)
				# sets length of xticks to 0 to hide the ticks 
				# important: allows you to keep xticklabels over invisible ticks
				# set to True if needed
			# hide_yticks: same as hide_xticks but for y axis
			# hide_xticklabels: calls .set_xticklabels([]) to remove xtick_labels
				# set to True if needed
			# hide_yticklabels: same as hide_xticklabels but for y axis
			# invert_x: calls .invert_xaxis() to reverse everything in a subplot horizontally
				# be careful, i think it reverses xlim values in a weird way, so .get_xlim might not
					# work intuitively after using this
				# set to True if needed
			# invert_y: same as invert_x but for y axis
				# i use this more than invert_x, specifically for plots like path dots or quantile plot
					# where y axis relationships are basically arbitrary, but reversing the order 
						# looks better
			# xtick_top: calls .xaxis.tick_top() to put xticks on the top of the subplot
				# good if the bottom of the plot is busy
				# set to True if needed
			# ytick_right: same as xtick_top but for y ticks 
				# (puts them on the right side rather than top for obvious reasons)
			# legend_dict: used to add a legend to plot more flexibly than matlotlib normally allows
				# has 2 parts (in the 1 dict): 1. setting legends values, 2. formatting the legend
				# setting legends values calls Line2D once for each for each item in the legend_value list
					# must have a legend_value list (name for each legend element)
					# can include any arg that Line2D takes:
						# can either be datatype accepted by Line2D or a list of same len as legend_values
						# single value will be repeated for all elements, a list will pass a specific
						# value for each element
				# formatting legend:
					# can pass all arguments accepted by .legend method (passed as **kwargs)
					# for loc + bbox_to_anchor: i don't find these very intuitive, so
						# you can either pass these directly or pass my custom arguments
							# loc + bbox_to_anchor will over-ride custom arguments
					# custom args:
						# whichcorner: says which corner of the subplot to set the legend in relation to
							# eg, "upper right" -> by default, upper right corner of legend will touch
								# upper right corner of plot (minus padding from borderaxespad kwarg)
							# if houtside is True: upper left corner will touch plot upper right corner
								# from outside
							# if voutside if True: lower right corner will touch plot upper right corner
								# from outside
						# hdistance: how far horizontally from default position to go?
							# defaults to 0. negative -> moves left, positive -> moves right
						# vdistance: how far vertically from default position to go
							# defaults to 0. negative -> moves down, positive -> moves up
						# maxwidth: equivalent to 3rd value in bbox_to_anchor:
							# sets max width of legend relative to plot (1 = covers whole plot horizontally)
						# maxheight: equivalent to 4rd value in bbox_to_anchor:
							# sets max height of legend relative to plot (1 = covers whole plot vertically)
						# houtside: if True, legend will be outside plot horizontally
							# some examples given in whichcorner section
							# by default, a corner of the legend will touch a corner of the plot 
								# (minus padding)
								# as such (info for advanced users), 
									# this actually switches the horizontal loc value
						# voutside: if True, legend will be outside plot vertically
							# some examples given in whichcorner section
							# by default, a corner of the legend will touch a corner of the plot 
								# (minus padding)
								# as such (info for advanced users), 
									# this actually switches the vertical loc value
			# plot_divider_dict: used to divide a subplot for some purpose (as in matplotlib "cax" examples)
				# important: this will return a cax object for further formatting
					# this cax object is not stored anywhere in the easy_fig object, so keep track
					# of it if you want to continue using/formatting the mini subplot
					# couldn't figure out an intuitive place to save the reference in the easy_fig object...
				# can either accept a previously produces "cax" object for formatting, or and append_axes
					# dict that will be used as **kwargs for .append_axes in this pattern:
						# divider = make_axes_locatable(ax)
						# cax = divider.append_axes(**plot_divider_dict.get('append_axes'))
					# as such, append_axes sub-dict can have all args accepted by append_axes method
				# can accept colorbar sub-dict that is passed as **kwargs to plt.colorbar(cax, **kwargs) 
					# method
					# used for addings a colorbar in the cax as in the hist2d
					# can include all arguments accepted by the plt.colorbar method
				# plot_divider_dict can also have many of the same formatting arguments that the rest of
					# the function accepts:
						# colorbar, tick_param_dict, hide_xaxis, hide_yaxis, hide_xticks, 
							# xtick_dict, hide_yticks, ytick_dict, hide_xticklabels, xticklabel_dict, 
							# hide_yticklabels, yticklabel_dict, xlog, xlim, ylog, ylim
					# each of these function the same as they do for the main function, but they
						# will be formatting the cax
		# group 2:
			# these dicts are passed as **kwargs to specific formatting methods, as described below:
				# title_dict: 			.set_title()
				# ylabel_dict: 			.set_ylabel()
				# xlabel_dict: 			.set_xlabel()
				# xtick_dict: 			.set_xticks()
				# xticklabel_dict: 		.set_xticklabels()
				# ytick_dict: 			.set_yticks()
				# yticklabel_dict: 		.set_yticklabels()
				# vline_dict: 			.axvline()
				# hline_dict: 			.axhline()
				# tick_param_dict: 		.tick_params()
	def format_panel(panel, remove_panel=False, hide_axis=False, \
		xlog=False, ylog=False, xlim=None, ylim=None, hide_xaxis=False, \
		hide_yaxis=False, hide_xticks=False, hide_yticks=False, \
		hide_xticklabels=False, hide_yticklabels=False, invert_y=False, \
		invert_x=False, xtick_top=False, ytick_right=False, legend_dict=None, \
		plot_divider_dict=None, title_dict=None, ylabel_dict=None, xlabel_dict=None, \
		xtick_dict=None, xticklabel_dict=None, ytick_dict=None, yticklabel_dict=None, \
		vline_dict=None, hline_dict=None, tick_param_dict=None):

		def format_legend_args(arg, ind):
			if isinstance(arg, list):
				return_arg = arg[ind]
			else:
				return_arg = arg
			return return_arg

		if remove_panel:
			panel.remove()
			return

		if title_dict is not None:
			panel.set_title(**title_dict)
		if ylabel_dict is not None:
			panel.set_ylabel(**ylabel_dict)
		if xlabel_dict is not None:
			panel.set_xlabel(**xlabel_dict)
		if vline_dict is not None:
			if isinstance(vline_dict, dict):
				panel.axvline(**vline_dict)
			else:
				for this_dict in vline_dict:
					panel.axvline(**this_dict)
		if hline_dict is not None:
			panel.axhline(**hline_dict)
		if tick_param_dict is not None:
			panel.tick_params(**tick_param_dict)
		
		if hide_axis == True:
			panel.axis('off')
		if xlog == True:
			panel.set_xscale('log')
		if xlim not in [None, False]:
			panel.set_xlim(xlim)
		if ylog == True:
			panel.set_yscale('log')
		if ylim not in [None, False]:
			panel.set_ylim(ylim)

		if hide_xaxis:
			panel.get_xaxis().set_visible(False)
		if hide_yaxis:
			panel.get_yaxis().set_visible(False)

		if hide_xticks:
			panel.tick_params(axis=u'x', \
				which=u'both',length=0)
		elif xtick_dict is not None:
			panel.set_xticks(**xtick_dict)
			
		if hide_yticks:
			panel.tick_params(axis=u'y', \
				which=u'both',length=0)
		elif ytick_dict is not None:
			panel.set_yticks(**ytick_dict)

		if hide_xticklabels:
			panel.set_xticklabels([])		
		elif xticklabel_dict is not None:	
			panel\
				.set_xticklabels(**xticklabel_dict)
		
		if hide_yticklabels:
			panel.set_yticklabels([])	
		elif yticklabel_dict is not None:
				panel\
					.set_yticklabels(**yticklabel_dict)

		if invert_y is not False:
			panel.invert_yaxis()
		if invert_x is not False:
			panel.invert_xaxis()

		if xtick_top is not False:
			panel.xaxis.tick_top()

		if ytick_right is not False:
			panel.yaxis.tick_right()


		if legend_dict is not None:
			if legend_dict.get('legend_values') is None:
				raise ValueError(\
					'For easy_fig formatting, if legend_dict is supplied, it must have "legend_values" in the dict')
			if legend_dict.get('whichcorner') is None:
				legend_dict['whichcorner'] = 'upper right'
			if legend_dict.get('hdistance') is None:
				legend_dict['hdistance'] = 0
			if legend_dict.get('vdistance') is None:
				legend_dict['vdistance'] = 0
			if legend_dict.get('maxwidth') is None:
				legend_dict['maxwidth'] = 0.5
			if legend_dict.get('maxheight') is None:
				legend_dict['maxheight'] = 0.5

			l2d_args = [
				'linewidth', 'lw', 'linestyle', 'color', 'marker', 'markersize', \
				'markeredgewidth', 'markeredgecolor', 'markerfacecolor', \
				'markerfacecoloralt', 'fillstyle', 'antialiased', \
				'dash_capstyle', 'solid_capstyle', 'dash_joinstyle', \
				'solid_joinstyle', 'pickradius', 'drawstyle', 'markevery'
			]

			legend_elements = []
			for val_loop, value in \
				enumerate(legend_dict.get('legend_values')):

				elements_dict = {}
				for this_arg in l2d_args:
					this_val = format_legend_args(legend_dict.get(this_arg), val_loop)
					if this_val is not None:
						elements_dict[this_arg] = this_val
				legend_elements.append(Line2D([0], [0], label=str(value),\
					**elements_dict))

			accepted_whichcorner = ['upper left', 'upper right', \
				'lower left', 'lower right', 'center']
			if legend_dict.get('whichcorner') not in accepted_whichcorner:
				raise ValueError('whichcorner must be in {}. Was {}'\
					.format(accepted_whichcorner, legend_dict.get('whichcorner')))
			if legend_dict.get('whichcorner') == 'center':
				legend_dict['hdistance'] += (0.5 - \
					legend_dict.get('maxwidth')/2)
				legend_dict['vdistance'] += (0.5 - \
					legend_dict.get('maxheight')/2)
			else:
				
				if legend_dict.get('houtside') and 'left' in \
					legend_dict.get('whichcorner'):
					legend_dict['whichcorner'] = \
						legend_dict['whichcorner'].replace('left', 'right')
					legend_dict['hdistance'] += (0 - 0)
				
				elif not legend_dict.get('houtside') and 'left' in \
					legend_dict.get('whichcorner'):
					legend_dict['hdistance'] += (0 - \
						legend_dict.get('maxwidth'))
				
				elif legend_dict.get('houtside') and 'right' in \
					legend_dict.get('whichcorner'):
					legend_dict['whichcorner'] = \
						legend_dict['whichcorner'].replace('right', 'left')
					legend_dict['hdistance'] += (1 - 0)
				
				elif not legend_dict.get('houtside') and 'right' in \
					legend_dict.get('whichcorner'):
					legend_dict['hdistance'] += (1 - \
						legend_dict.get('maxwidth'))
				
				
				if legend_dict.get('voutside') and 'upper' in \
					legend_dict.get('whichcorner'):
					legend_dict['whichcorner'] = \
						legend_dict['whichcorner'].replace('upper', 'lower')
					legend_dict['vdistance'] += (1 - 0)
				
				elif not legend_dict.get('voutside') and 'upper' in \
					legend_dict.get('whichcorner'):
					legend_dict['vdistance'] += (1 - \
						legend_dict.get('maxheight'))
				
				elif legend_dict.get('voutside') and 'lower' in \
					legend_dict.get('whichcorner'):
					legend_dict['whichcorner'] = \
						legend_dict['whichcorner'].replace('lower', 'upper')
					legend_dict['vdistance'] += (0 - 0)
				
				elif not legend_dict.get('voutside') and 'lower' in \
					legend_dict.get('whichcorner'):
					legend_dict['vdistance'] += (0 - \
						legend_dict.get('maxheight'))
			
			if legend_dict.get('loc') is None:
				legend_dict['loc'] = legend_dict.get('whichcorner')

			if legend_dict.get('bbox_to_anchor') is None:
				legend_dict['bbox_to_anchor'] = \
					[
						legend_dict.get('hdistance'), 	\
						legend_dict.get('vdistance'), 	\
						legend_dict.get('maxwidth'), 	\
						legend_dict.get('maxheight'), 	\
					]

			if legend_dict.get('handles') is None:
				legend_dict['handles'] = legend_elements

			if legend_dict.get('legend_values') is not None:
				del legend_dict['legend_values']
			if legend_dict.get('hdistance') is not None:
				del legend_dict['hdistance']
			if legend_dict.get('vdistance') is not None:
				del legend_dict['vdistance']
			if legend_dict.get('maxwidth') is not None:
				del legend_dict['maxwidth']
			if legend_dict.get('maxheight') is not None:
				del legend_dict['maxheight']
			if legend_dict.get('whichcorner') is not None:
				del legend_dict['whichcorner']
			if legend_dict.get('houtside') is not None:
				del legend_dict['houtside']
			if legend_dict.get('voutside') is not None:
				del legend_dict['voutside']
			for this_arg in l2d_args:
				if legend_dict.get(this_arg) is not None:
					del legend_dict[this_arg]

			panel.legend(**legend_dict)


		if plot_divider_dict is not None:
			if plot_divider_dict.get('append_axes') is not None and plot_divider_dict.get('cax') \
				is not None:
				raise ValueError(\
						'plot_divider_dict for format_panel contains "cax" and "append_axes" keys.\
					\nappend_axes replaces cax, so please supply only one')
			elif plot_divider_dict.get('append_axes') is None and plot_divider_dict.get('cax') is None:
				raise ValueError(\
					'plot_divider_dict for format_panel must have either "cax" or "append_axes" to\
					\nprovide or produce (respectively) a divider for formatting')
			elif plot_divider_dict.get('append_axes') is not None:
				divider = make_axes_locatable(panel)
				plot_divider_dict['cax'] = divider.append_axes(**plot_divider_dict.get('append_axes'))
			

			if plot_divider_dict.get('colorbar') is not None:
				plt.colorbar(cax=plot_divider_dict.get('cax'), **plot_divider_dict.get('colorbar'))

			if plot_divider_dict.get('tick_param_dict') is not None:
				plot_divider_dict['cax'].tick_params(**plot_divider_dict.get('tick_param_dict'))

			if plot_divider_dict.get('hide_xaxis'):
				plot_divider_dict['cax'].get_xaxis().set_visible(False)
			if plot_divider_dict.get('hide_yaxis'):
				plot_divider_dict['cax'].get_yaxis().set_visible(False)

			if plot_divider_dict.get('hide_xticks'):
				plot_divider_dict['cax'].tick_params(axis=u'x', \
					which=u'both',length=0)
			elif plot_divider_dict.get('xtick_dict') is not None:
				plot_divider_dict['cax'].set_xticks(**plot_divider_dict.get('xtick_dict'))
				
			if plot_divider_dict.get('hide_yticks'):
				plot_divider_dict['cax'].tick_params(axis=u'y', \
					which=u'both',length=0)
			elif plot_divider_dict.get('ytick_dict') is not None:
				plot_divider_dict['cax'].set_yticks(**plot_divider_dict.get('ytick_dict'))

			if plot_divider_dict.get('hide_xticklabels'):
				plot_divider_dict['cax'].set_xticklabels([])		
			elif plot_divider_dict.get('xticklabel_dict') is not None:	
				plot_divider_dict['cax']\
					.set_xticklabels(**plot_divider_dict.get('xticklabel_dict'))
			
			if plot_divider_dict.get('hide_yticklabels'):
				plot_divider_dict['cax'].set_yticklabels([])	
			elif plot_divider_dict.get('yticklabel_dict') is not None:
					plot_divider_dict['cax']\
						.set_yticklabels(**plot_divider_dict.get('yticklabel_dict'))

			if plot_divider_dict.get('xlog') == True:
				plot_divider_dict['cax'].set_xscale('log')
			if plot_divider_dict.get('xlim') not in [None, False]:
				plot_divider_dict['cax'].set_xlim(plot_divider_dict.get('xlim'))
			if plot_divider_dict.get('ylog') == True:
				plot_divider_dict['cax'].set_yscale('log')
			if plot_divider_dict.get('ylim') not in [None, False]:
				plot_divider_dict['cax'].set_ylim(plot_divider_dict.get('ylim'))

			return plot_divider_dict.get('cax')

	def marker_real_bbox(self, ax, xspot, yspot, *args, trans=None, **kwargs):
		if trans is None:
			trans = ax.transData.inverted()
		if not any([ax in this_list for this_list in self.axes]):
			raise ValueError('Must pass ax from this instance of easy_fig to\
				\n\t.text_and_location method')
		mpl_marker_multi = 3

		obj = ax.scatter(xspot, yspot, *args, **kwargs)

		# renderer = fig.canvas.get_renderer()
		obj_bbox = obj.get_window_extent(renderer=self.renderer)
		obj_bbox = trans.transform(obj_bbox)

		x_vals = [obj_bbox[0][0], obj_bbox[1][0]]
		y_vals = [obj_bbox[1][1], obj_bbox[0][1]]
		obj_bbox = [[min(x_vals), max(x_vals)],[min(y_vals), max(y_vals)]]
		height = (obj_bbox[1][1] - obj_bbox[1][0])*mpl_marker_multi
		width = (obj_bbox[0][1] - obj_bbox[0][0])*mpl_marker_multi

		return obj, width, height

	def asterisk_and_location(self, ax, x, y, *args, trans=None, **kwargs):
		if trans is None:
			trans = ax.transData.inverted()
			# trans = ax.transAxes.inverted()
		if not any([ax in this_list for this_list in self.axes]):
			raise ValueError('Must pass ax from this instance of easy_fig to\
				\n\t.text_and_location method')

		text_out = ax.text(x, y, '*', va='bottom', ha='center', *args, **kwargs)
		text_bbox = text_out.get_window_extent(renderer=self.renderer)
		
		text_bbox = trans.transform(text_bbox)

		x_vals = [text_bbox[0][0], text_bbox[1][0]]
		y_vals = [text_bbox[1][1], text_bbox[0][1]]
		text_bbox = [[min(x_vals), max(x_vals)],[min(y_vals), max(y_vals)]]
		height = text_bbox[1][1] - text_bbox[1][0]
		new_height = height/1.6
		width = text_bbox[0][1] - text_bbox[0][0]
		ymiddle = text_bbox[1][0] + (new_height*(3/2))
		xmiddle = text_bbox[0][1] - (width/2)
		#
			# renderer = self.fig.canvas.get_renderer()
			# ybottom = yspot - new_height
			# width = text_bbox.width
			# height = text_bbox.height
			# x0 = text_bbox.x0
			# y0 = text_bbox.y0
			# x1 = text_bbox.x1
			# y1 = text_bbox.y1
			# print(x0,x1,y0,y1)
			# print(inv.transform(x0),inv.transform(x1),inv.transform(y0),inv.transform(y1))

		return text_out, (xmiddle, ymiddle, width, new_height)

	def buffer_ast_in_bar(self, ax, allowed_x, allowed_y, fontsize, pref_buffer, *args,
		which_buffer='bottom', min_fontsize=None, min_vbuffer=None, min_hbuffer=None, x_in_xlim=True, y_in_ylim=True, 
		min_buff_align='edge', verbose=False, **kwargs):
		def check_buffer(min_buffer, allowed_range, min_fontsize, actual_min, pref_mid, text_size, fontsize, 
			min_align, failed):
			# i only automatically set this for 1 min_buffer, not both - need to add logic for if min_buffer is None lol
				# also need to add logic for if 1 of the allowed_range values is none
			# if there is a preferred position, put text there
			if allowed_range[1] is not None:
				max_val = allowed_range[1] - min_buffer
			else:
				max_val = None
			if allowed_range[0] is not None:
				min_val = allowed_range[0] + min_buffer
			else:
				min_val = None
			if None not in allowed_range:
				mid_val = allowed_range[0] + (num_diff(*allowed_range)/2)
				if pref_mid is not None: # can do range == None logic here (can return in the if block if None)
					# should be simple since if 1 limit is None, won't need to calculate much
					pref_max = pref_mid + (text_size/2)
					pref_min = pref_mid - (text_size/2)
				else:
					pref_mid = mid_val
					pref_min = mid_val - (text_size/2)
					pref_max = mid_val + (text_size/2)
				# if the asterisk needs to slide (pref buffer to big, min_val buffer maybe ok)
					# this step is only relevant if a preferred position is given
				if pref_max > max_val or pref_min < min_val:
					if min_align == 'edge':
						if pref_max > max_val:
							new_min = pref_min - (num_diff(pref_max, max_val)*1.000001)
							new_max = pref_max - (num_diff(pref_max, max_val)*1.000001)
						if pref_min < min_val:
							new_min = pref_min + (num_diff(pref_min, min_val)*1.000001)
							new_max = pref_max + (num_diff(pref_min, min_val)*1.000001)
					else:
						new_min = mid_val - (text_size/2)
						new_max = mid_val + (text_size/2)
					# sliding is not enough, need to adjust asterisk size
					if new_max > max_val or new_min < min_val:
						full_len = text_size + (2*min_buffer)
						full_allowed = num_diff(*allowed_range)
						fontsize_ratio = full_allowed/full_len
						new_fontsize = fontsize * fontsize_ratio
						# squished too far, failed to fit the asterisk
						if new_fontsize < min_fontsize:
							failed = True
							return None, None, None, failed
						else:
							new_min = min_val
							new_max = max_val
							new_middle = mid_val
							new_size = text_size * fontsize_ratio
							new_min = new_middle - (new_size/2)
					else:
						new_fontsize = fontsize
						new_middle = new_min + (text_size/2)
				else:
					new_min = pref_min
					new_middle = pref_mid
					new_fontsize = fontsize
			else:
				new_fontsize = fontsize
				mid_val = None
				if min_val is not None:
					new_min = min_val
				if max_val is not None:
					new_min = max_val - text_size
				new_middle = new_min + text_size/2

			return new_min, new_middle, new_fontsize, failed

		if min_buff_align not in ['edge', 'center']:
			raise ValueError('min_buff_align must be in ["edge", "center"]')

		# generating starting asterisk location. This is almost guaranteed to be hidden later
			# i need it to get the size/positioning of the asterisk for math later
			# i don't understand the intricacies of matplotlib formatting, so this is a hacky
				# way to bypass that limitation
		if None not in allowed_x:
			diff = num_diff(*allowed_x)
			xmid = allowed_x[0] + (diff/2)
		else:
			xlim = list(ax.get_xlim())
			diff = num_diff(*xlim)
			xmid = xlim[0] + (diff/2)
		if None not in allowed_y:
			diff = num_diff(*allowed_y)
			ymid = allowed_y[0] + (diff/2)
		else:
			ylim = list(ax.get_ylim())
			diff = num_diff(*ylim)
			ymid = ylim[0] + (diff/2)
		text_obj, loc = self.asterisk_and_location(ax, xmid, ymid, *args, fontsize=fontsize, **kwargs)
		xmiddle, ymiddle, width, height = loc

		# generating all relevant location metrics from original asterisk
		bottom = ymiddle - (height/2)
		top = ymiddle + (height/2)
		left = xmiddle - (width/2)
		right = xmiddle + (width/2)

		# some input pre-processing/checking before we really get moving
		if min_fontsize is None or min_fontsize > fontsize:
			min_fontsize = fontsize
		if which_buffer in ['top', 'bottom']:
			pref_buffer = pref_buffer * height
			if min_vbuffer is None or min_vbuffer > pref_buffer:
				min_vbuffer = pref_buffer
			else:
				min_vbuffer = min_vbuffer * height
			if min_hbuffer is not None:
				min_hbuffer = min_hbuffer * width
			else:
				min_hbuffer = 0
		elif which_buffer in ['left', 'right']:
			pref_buffer = pref_buffer * width
			if min_vbuffer is None or min_vbuffer > pref_buffer:
				min_hbuffer = pref_buffer
			else:
				min_hbuffer = min_hbuffer * width
			if min_vbuffer is not None:
				min_vbuffer = min_vbuffer * height
			else:
				min_vbuffer = 0
		if which_buffer not in ['top', 'bottom', 'left', 'right']:
			raise ValueError('which_buffer must be in ["top", "bottom", "left", "right"]')

		# setting starting locations for further formatting later
			# vertical formatting is based on the bottom location + the height
			# horizontal formatting is based on the left location + the width
		if which_buffer == 'bottom':
			if allowed_y[0] is None:
				raise ValueError('If which_buffer is bottom aligned, allowed_y[0] cannot be None')
			pref_vmid = allowed_y[0] + pref_buffer + (height/2)
			pref_hmid = None
		elif which_buffer == 'top':
			if allowed_y[1] is None:
				raise ValueError('If which_buffer is top aligned, allowed_y[1] cannot be None')
			pref_vmid = allowed_y[1] - pref_buffer - (height/2)
			pref_hmid = None
		elif which_buffer == 'left':
			if allowed_x[0] is None:
				raise ValueError('If which_buffer is left aligned, allowed_x[0] cannot be None')
			pref_hmid = allowed_x[0] + pref_buffer + (height/2)
			pref_vmid = None
		elif which_buffer == 'right':
			if allowed_x[1] is None:
				raise ValueError('If which_buffer is right aligned, allowed_x[1] cannot be None')
			pref_hmid = allowed_x[1] - pref_buffer - (width/2)
			pref_vmid = None

		# true/false trees handling actual formatting
			# prefers to leave asterisk at original fontsize + preferred buffer location
			# failing that, it will attempt to slide the asterisk towards the center of the box
			# if the asterisk is too big, it can slide it to exactly the center + scale it down
		failed = False
		failed_before = failed
		which_run = 'vert'
		new_bottom, new_ymiddle, new_yfontsize, failed = \
			check_buffer(min_vbuffer, allowed_y, min_fontsize, bottom, pref_vmid, height, 
				fontsize, min_buff_align, failed)
		if verbose:
			if failed_before != failed:
				print('Could not fit vertically')
		failed_before = failed
		which_run = 'horiz'
		new_left, new_xmiddle, new_xfontsize, failed = \
			check_buffer(min_hbuffer, allowed_x, min_fontsize, left, pref_hmid, width, 
				fontsize, min_buff_align, failed)
		if verbose:
			if failed_before != failed:
				print('Could not fit horizontally')

		# final formatting and then re-plotting the asterisk in its final location
			# if it managed to be places appropriately
			# if it failed, the original asterisk will be made invisible and
				# the function will returns failed = True + height + width
			# this can help with further plotting decisions/code
		if failed is False:
			new_size = min([new_yfontsize, new_xfontsize])
			# if asterisk needs to be scaled down, this calculates some relevant values
			if new_size != fontsize:
				fontsize_ratio = new_size / fontsize
				new_height = height * fontsize_ratio
				new_width = width * fontsize_ratio
				new_bottom = new_ymiddle - (new_height * 1.5)
				y_move = new_bottom - bottom
				new_left = new_xmiddle - (new_width * 1.5)
				x_move = new_left - left
			# otherwise use original values
			else:
				fontsize_ratio = 1
				new_height = height
				new_width = width
			# if any val needs to be changed, this moves the asterisk (almost inevitable)
			if any([fontsize != new_size, bottom != new_bottom, 
				left != new_left]):
				text_obj.set_visible(False)
				text_obj, loc = self.asterisk_and_location(ax, new_xmiddle, new_ymiddle-(height*(new_size/fontsize)*1.5),
					*args, fontsize=new_size, **kwargs)
			return text_obj, (fontsize_ratio, new_xmiddle, new_ymiddle, width, height), failed
		else:
			# if it failed to fit, hide the asterisk + pass some data back up
			text_obj.set_visible(False)
			return text_obj, (None, None, None, width, height), failed

		#
			# other_pref_buffers=None, **kwargs):
				# print(allowed_range)
				# raise ValueError('check comments, ya dum dum')
				# if min_buffer is not None:
					# if which_run == 'horiz':
					# 	print(allowed_range, round(min_val, 4), round(max_val, 4), round(min_buffer, 4))
						# if which_run == 'horiz':
						# 	print(1)
						# if which_run == 'horiz':
						# 	print(2)
								# if which_run == 'horiz':
								# 	print(5)
								# if which_run == 'horiz':
								# 	print(6, round(pref_min,4), round(min_val,4))
								# print(allowed_range)
								# text_move = (new_min - pref_mid) + (fontsize_ratio*(1 - text_size))
							# text_move = new_min - pref_mid
						# if which_run == 'horiz':
						# 	print(3)
						# new_middle = new_min + (text_size/2)
						# text_move = new_min - pref_mid
					# if which_run == 'horiz':
					# 	print(4)
					# failed = False
						# if which_run == 'horiz':
						# 	print(allowed_range, round(min_val, 4), round(min_buffer, 4))
						# if which_run == 'horiz':
						# 	print(allowed_range, round(max_val, 4), round(min_buffer, 4))
				# if not, put it in the center
				# return new_min, new_middle, text_move, new_fontsize, failed
				# if which_run == 'horiz':
				# 	if allowed_range[0] is not None:
				# 		print(colored('\tshift right: {:<7}, pref_mid: {:<7}, text_size: {:<7}'
				# 			.format(round(new_min - allowed_range[0],4), round(pref_mid,4), round(text_size,4)), 'red'))
			#
				# if other_pref_buffers is None:
				# 	other_pref_buffers = {}
				# if other_pref_buffers.get(which_buffer) is None:
				# 	other_pref_buffers[which_buffer] = pref_buffer

				# making sure allowed x/y limits are appropriate

				# think i'm gonna get rid of all of this since i'm bypassing this in check_buffer?

				# new_allowed_x = [None, None]
				# new_allowed_y = [None, None]
				# if allowed_x[0] is None:
				# 	new_allowed_x[0] = ax.get_xlim()[0]
				# if allowed_x[1] is None:
				# 	new_allowed_x[1] = ax.get_xlim()[1]
				# if allowed_y[0] is None:
				# 	new_allowed_y[0] = ax.get_ylim()[0]
				# if allowed_y[1] is None:
				# 	new_allowed_y[1] = ax.get_ylim()[1]
				# if x_in_xlim:
				# 	xlims = ax.get_xlim()
				# 	if allowed_x[0] is not None:
				# 		if allowed_x[0] < xlims[0]:
				# 			new_allowed_x[0] = xlims[0]
				# 	if allowed_x[1] is not None:
				# 		if allowed_x[1] > xlims[1]:
				# 			new_allowed_x[1] = xlims[1]
				# if new_allowed_x[0] is None:
				# 	new_allowed_x[0] = allowed_x[0]
				# if new_allowed_x[1] is None:
				# 	new_allowed_x[1] = allowed_x[1]
				# if y_in_ylim:
				# 	ylims = ax.get_ylim()
				# 	if allowed_y[0] is not None:
				# 		if allowed_y[0] < ylims[0]:
				# 			new_allowed_y[0] = ylims[0]
				# 	if allowed_y[1] is not None:
				# 		if allowed_y[1] > ylims[1]:
				# 			new_allowed_y[1] = ylims[1]
				# if new_allowed_y[0] is None:
				# 	new_allowed_y[0] = allowed_y[0]
				# if new_allowed_y[1] is None:
				# 	new_allowed_y[1] = allowed_y[1]

				# allowed_y = new_allowed_y
				# allowed_x = new_allowed_x

			# diff = num_diff(*allowed_y)
			# ymid = allowed_y[0] + (diff/2)
			# new_bottom, new_ymiddle, y_move, new_yfontsize, failed = \
			# new_left, new_xmiddle, x_move, new_xfontsize, failed = \
					# ax.scatter(new_xmiddle, new_ymiddle, s=0.5, c='k', zorder=6)
					# ax.scatter(new_xmiddle, new_ymiddle-(height*(new_size/fontsize)*1.5), s=0.5, c='k', zorder=6)
			# ax.scatter(new_xmiddle, new_ymiddle, c='w', s=1, zorder=5)
			# orig_pref_bottom = pref_bottom
			# orig_pref_left = pref_left
			# diff = num_diff(max_val, min_val)
			# if check_same_side(min_val, max_val):
			# 	mid_val = min_val + (max_val - min_val)/2
			# else:
			# 	mid_val = min_val + (max_val + min_val)/2
			# if which_run == 'horiz':
			# 	print(colored('\tdoing horiz', 'red'))
			# 	print('\t\t', min_val, max_val)
			# print('\t\t\tmin_val: {}, mid_val: {}, max_val: {}'.format(min_val, mid_val, max_val))
				# pref_min = min_val
				# pref_max = max_val
			# if which_run == 'horiz':	
			# 	print('\t\t', pref_min, pref_max)
			# print('\t\t\tpref_min: {}, pref_max: {}'.format(pref_min, pref_max))
				# new_min = mid_val - (text_size/2)
				# new_max = mid_val + (text_size/2)
				# print(colored('{}, {}'.format(max_val, min_val), 'blue'))
				# print(pref_max, pref_min)
					# print(colored('{}, {}, {}, {}'
					# 	.format(allowed_range[1], round(pref_max, 3), 
					# 		round(max_val, 3), round(new_max, 3)), 'blue'))
					# print(new_max, new_min)
					# print(colored('{}, {}, {}, {}'
					# 	.format(allowed_range[0], round(pref_min, 3), 
					# 		round(min_val, 3), round(new_min, 3)), 'blue'))
					# print(new_max, new_min)
				# else:
				# 	new_min = pref_min
				# 	new_max = pref_max
				# if which_run == 'horiz':
				# 	print(colored('\tsliding', 'white'))
				# 	print('\t\t', new_min, new_max)
				# print('\t\t\tnew_min: {}, new_max: {}'.format(new_min, new_max))
					# full_len = diff + (2*min_buffer)
					# new_min = mid_val - (text_size/2)
					# new_max = mid_val + (text_size/2)
					# fontsize_ratio = ((max_val - mid_val)/(new_max - mid_val))
					# if which_run == 'horiz':
					# 	print('\t\t', max_val,  mid_val, new_max, mid_val, max_val - mid_val, new_max - mid_val)
					# 	print('\t\t', fontsize_ratio, new_fontsize)
					# print('\t\t\tnums: {}, ratio: {}, new_fontsize: {}'.format(((max_val - mid_val),(new_max - mid_val)), 
						# ((max_val - mid_val)/(new_max - mid_val)), new_fontsize))
						# if which_run == 'horiz':
						# 	print('{} failed'.format(which_run))
						# print(colored('\tsquishing', 'white'))
						# text_move = pref_min - min_val
						# new_size = diff * fontsize_ratio
						# new_text_size = text_size * fontsize_ratio
						# new_min = mid_val - (new_text_size * 1.5)
						# text_move = new_min - min_val


			# print('\t\t\tfontsize: {}, min_fontsize: {}'.format(fontsize, min_fontsize))
			# print(allowed_x)
			# print(new_allowed_x)
			# print(new_allowed_x)
			# if check_same_side(*allowed_x):
			# 	xmid = allowed_x[0] + (allowed_x[1] - allowed_x[0])/2
			# else:
			# 	xmid = allowed_x[0] + (allowed_x[1] + allowed_x[0])/2
			# if check_same_side(*allowed_y):
			# 	ymid = allowed_y[0] + (allowed_y[1] - allowed_y[0])/2
			# else:
			# 	ymid = allowed_y[0] + (allowed_y[1] + allowed_y[0])/2
			# print('\t\t\tymid: {}, xmid: {}'.format(ymid, xmid))
			# print(xmid, xmiddle, width)


				# print(round(pref_buffer,4), round(pref_buffer*width,4), round(width,4))
				# print('\t', min_hbuffer, min_hbuffer/width)
			# print('\t\t\tyvals: ({},{}), xvals: ({},{})'.format(bottom, top, left, right))
				# print('bottom, pref_bottom: {}, allowed_y: {}, pref_buffer: {}'.format(pref_bottom, allowed_y, pref_buffer))
				# vbuff = 0
				# hbuff = None
				# print('top, pref_bottom: {}, allowed_y: {}, pref_buffer: {}'.format(pref_bottom, allowed_y, pref_buffer))
				# vbuff = 1
				# hbuff = None
				# if min_hbuffer == 1:
				# hbuff = 0
				# vbuff = None
				# print('right, pref_left: {}, allowed_x: {}, pref_buffer: {}'.format(round(pref_left,3), allowed_x, round(pref_buffer,3)))
				# vbuff = 1
				# hbuff = None
			# print('min_hbuffer', min_hbuffer, pref_left)
			# print('\t\t\tpref bottom: {}, pref left: {}'.format(pref_bottom, pref_left))
			# print('\t\t\tsetup vertical')
			# print('\t\t\tnew_bottom: {}, new_ymiddle: {}, y_move: {}, new_yfontsize: {}, failed: {}'
				# .format(new_bottom, new_ymiddle, y_move, new_yfontsize, failed))
			# print('\t\t\tsetup horizontal')
			# print('new_xmiddle', new_xmiddle, x_move, new_xfontsize)
			# print('\t\t\tnew_left: {}, new_xmiddle: {}, x_move: {}, new_xfontsize: {}, failed: {}'
				# .format(new_left, new_xmiddle, x_move, new_xfontsize, failed))
			# if min_vbuffer is not None:
				# max_top = allowed_y[1] - min_vbuffer
				# min_bottom = allowed_y[0] + min_vbuffer
				# if pref_bottom is not None:
				# 	pref_top = pref_bottom + height
				# else:
				# 	pref_bottom = bottom
				# 	pref_top = top
				# # if the asterisk needs to slide (pref ybuffer to big, min ybuffer maybe ok)
				# if pref_top > max_top or pref_bottom < min_bottom:
				# 	new_ymiddle = (max_top - min_bottom)/2
				# 	new_bottom = new_ymiddle - (height/2)
				# 	new_top = new_ymiddle + (height/2)
				# 	# sliding not enough, need to adjust asterisk size
				# 	if new_top > max_top or new_bottom < min_bottom:
				# 		fontsize_ratio = ((max_top - new_ymiddle)/(new_top - new_ymiddle))
				# 		new_yfontsize = fontsize * fontsize_ratio
				# 		# squished too far, failed to fit the asterisk
				# 		if new_yfontsize < min_fontsize:
				# 			failed = True
				# 		# else:
				# 			# new_height = height * fontsize_ratio
				# 			# new_bottom = new_ymiddle - (new_height * 1.5)
				# 			# y_move = new_bottom - bottom
				# 	else:
				# 		y_move = new_bottom - bottom
				# 		new_yfontsize = fontsize
				# else:
				# 	new_bottom = pref_bottom
				# 	new_ymiddle = new_bottom + (height/2)
				# 	y_move = 0
				# 	new_yfontsize = fontsize

			# if min_hbuffer is not None:
				# max_right = allowed_x[1] - min_hbuffer
				# min_left = allowed_x[0] + min_hbuffer
				# if pref_left is not None:
				# 	pref_right = pref_left + width
				# else:
				# 	pref_left = left
				# 	pref_right = right
				# # if the asterisk needs to slide (pref xbuffer to big, min xbuffer maybe ok)
				# if pref_right > max_right or pref_left < min_left:
				# 	new_xmiddle = (max_right - min_left)/2
				# 	new_left = new_xmiddle - (width/2)
				# 	new_right = new_xmiddle + (width/2)
				# 	# sliding not enough, need to adjust asterisk size
				# 	if new_right > max_right or new_left < min_left:
				# 		fontsize_ratio = ((max_right - new_xmiddle)/(new_right - new_xmiddle))
				# 		new_xfontsize = fontsize * fontsize_ratio
				# 		# squished too far, failed to fit the asterisk
				# 		if new_xfontsize < min_fontsize:
				# 			failed = True
				# 		# else:
				# 			# new_width = width * fontsize_ratio
				# 			# new_left = new_xmiddle - (new_width * 1.5)
				# 			# x_move = new_left - left
				# 	else:
				# 		x_move = new_left - left
				# 		new_xfontsize = fontsize
				# else:
				# 	new_left = pref_left
				# 	new_xmiddle = new_left + (width/2)
				# 	x_move = 0
				# 	new_xfontsize = fontsize

			# have (fontsize, new_size), (orig_pref_bottom, new_bottom), (orig_pref_left, new_left)

			# print('\t\tymin/max: {}, xmin/max: {}, bottom: {}, left: {}, y_move: {}, x_move: {}'
				# .format(allowed_y, allowed_x, bottom, left, y_move, x_move))
				# if which_buffer == 'left':
					# print('left, pref_left: {}, allowed_x: {}, pref_buffer: {}, new_left: {}, new_ymiddle: {}\
					# 	\n\tx_pref_xbuff: {}, x_min_xbuff: {}'
					# 	.format(round(pref_left, 3), allowed_x, round(pref_buffer, 3), 
					# 		round(new_left, 3), round(new_ymiddle, 3), round((new_left-allowed_x[0])/pref_buffer, 3), 
					# 		round((new_left-allowed_x[0])/min_hbuffer, 3)))
					# raise
				# pass useful data back up
				# raise
				# raise


	def text_and_location(self, ax, *args, trans=None, **kwargs):
		if trans is None:
			trans = ax.transData.inverted()
			# trans = ax.transAxes.inverted()
		if not any([ax in this_list for this_list in self.axes]):
			raise ValueError('Must pass ax from this instance of easy_fig to\
				\n\t.text_and_location method')

		text_out = ax.text(*args, **kwargs)
		# renderer = self.fig.canvas.get_renderer()
		text_bbox = text_out.get_window_extent(renderer=self.renderer)
		
		text_bbox = trans.transform(text_bbox)

		x_vals = [text_bbox[0][0], text_bbox[1][0]]
		y_vals = [text_bbox[1][1], text_bbox[0][1]]
		text_bbox = [[min(x_vals), max(x_vals)],[min(y_vals), max(y_vals)]]
		# width = text_bbox.width
		# height = text_bbox.height
		# x0 = text_bbox.x0
		# y0 = text_bbox.y0
		# x1 = text_bbox.x1
		# y1 = text_bbox.y1
		# print(x0,x1,y0,y1)
		# print(inv.transform(x0),inv.transform(x1),inv.transform(y0),inv.transform(y1))

		return text_out, text_bbox


def variable_size_panel_list(gridsize, top_left, bottom_right, x_count, \
	y_count, subpanel_pad_percent, panel_pad_percent = 0, img_shapes=None, \
	vratio=None, hratio=None):
	# issue with things shifting left: want everything to expend right by a ratio of:
		# 1/(1-(% taken up by 1 in-between))
		# so calculate size of 1 in-between and do totalx/1_between and multiply everything by that?
	# if img_shapes is not None:
	# 	print(len(img_shapes), x_count, y_count)
	if vratio is None:
		vratio = [1 for vpanel in range(y_count)]
	else:
		vratio = [vpanel/mean(vratio) for vpanel in vratio]
	if hratio is None:
		hratio = [1 for hpanel in range(x_count)]
	else:
		hratio = [vpanel/mean(hratio) for vpanel in hratio]

	if isinstance(panel_pad_percent, tuple):
		panel_pad_ratio_x = panel_pad_percent[0]/100
		panel_pad_ratio_y = panel_pad_percent[1]/100
	else:
		panel_pad_ratio_x = panel_pad_percent/100
		panel_pad_ratio_y = panel_pad_percent/100

	if isinstance(subpanel_pad_percent, tuple):
		subpanel_pad_ratio_x = subpanel_pad_percent[0]/100
		subpanel_pad_ratio_y = subpanel_pad_percent[1]/100
	else:
		subpanel_pad_ratio_x = subpanel_pad_percent/100
		subpanel_pad_ratio_y = subpanel_pad_percent/100
	top_y = top_left[1]*gridsize[1]
	left_x = top_left[0]*gridsize[0]
	y_size = float((bottom_right[1]-top_left[1]))
	x_size = float((bottom_right[0]-top_left[0]))
	y_per_sub = float(y_size/y_count)
	x_per_sub = float(x_size/x_count)
	single_x_sub_pad = x_per_sub*subpanel_pad_ratio_x
	single_y_sub_pad = y_per_sub*subpanel_pad_ratio_y
	x_up_ratio = x_size / (x_size - single_x_sub_pad)
	y_up_ratio = y_size / (y_size - single_y_sub_pad)


	y_per_sub = int(y_per_sub * y_up_ratio * gridsize[1])
	x_per_sub = int(x_per_sub * x_up_ratio * gridsize[0])
	single_x_sub_pad = int(single_x_sub_pad * y_up_ratio * gridsize[1])
	single_y_sub_pad = int(single_y_sub_pad * x_up_ratio * gridsize[0])

	ax_list = []
	ongoing_left_x = left_x
	ongoing_top_y = top_y
	for sub_loop in range(x_count*y_count):
		x_spot = sub_loop % x_count
		y_spot = int(sub_loop/x_count)
		if x_spot == 0:
			ongoing_left_x = left_x
		if subpanel_pad_ratio_x > 0:
			current_col_xpadding = single_x_sub_pad * x_spot
		else:
			current_col_xpadding = 0
		x_per_sub_padded = int(x_per_sub*(1-subpanel_pad_ratio_x))
		ax_x = int(left_x + x_per_sub*x_spot + current_col_xpadding)
		if subpanel_pad_ratio_y > 0:
			current_col_ypadding = single_y_sub_pad * y_spot
		else:
			current_col_ypadding = 0
		y_per_sub_padded = int(y_per_sub*(1-subpanel_pad_ratio_y))
		ax_y = int(top_y + y_per_sub*y_spot + current_col_ypadding)
		this_x_per_sub = int(x_per_sub_padded * hratio[x_spot])
		this_y_per_sub = int(y_per_sub_padded * vratio[y_spot])
		if img_shapes == None:
			ax_list.append(plt.subplot2grid(gridsize, (int(ongoing_top_y), int(ongoing_left_x)), \
				rowspan=this_y_per_sub, colspan=this_x_per_sub))
		else:
			img_y = img_shapes[sub_loop][0]
			img_x = img_shapes[sub_loop][1]
			img_ratio = img_y/img_x
			panel_ratio = this_y_per_sub/this_x_per_sub
			# print(gridsize, (ongoing_top_y, ongoing_left_x), 
			# 	int(this_y_per_sub/(panel_ratio/img_ratio)), this_x_per_sub)
			ax_list.append(plt.subplot2grid(gridsize, (int(ongoing_top_y), int(ongoing_left_x)), \
				rowspan=int(this_y_per_sub/(panel_ratio/img_ratio)), \
				colspan=this_x_per_sub))
		ongoing_left_x += (this_x_per_sub + subpanel_pad_ratio_x*x_per_sub)
		if x_spot % x_count == x_count-1:
			ongoing_top_y += (this_y_per_sub + subpanel_pad_ratio_y*y_per_sub)

	return ax_list

def other_handler(input_object):
    if isinstance(input_object, datetime.datetime):
        return input_object.isoformat()
    return 'object type {}'.format(type(input_object))

def quick_dumps(input_object, object_name=None, indent=4, save_spot=None, 
	print_out=True):
	if save_spot is None:
		if object_name == None:
			print(colored('\nQuick Dumps:', 'red'))
		else:
			print(colored('\nQuick Dumps: {}'.format(object_name), 'red'))
		if print_out:
			print(json.dumps(input_object, default=other_handler, indent=indent))
		else:
			return json.dumps(input_object, default=other_handler, indent=indent)
	else:
		with open(save_spot, 'w') as writefile:
			json.dump(input_object, writefile, default=other_handler, indent=indent)

def str_is_float(input_str):
    try: 
        float(input_str)
        return True
    except ValueError:
        return False

def close_reopen_workers(workers, cpu_count):
	workers.close()
	workers.join()
	return mp.Pool(cpu_count)


# for updating xticklabels in plots where those should be in kb
def make_xticks_kb(ax, fontsize):
	xlabels = [int(x) for x in ax.get_xticks()/1000 if x <= ax.get_xlim()[1]/1000]
	xlabels[-1] = '{} Kb'.format(xlabels[-1])
	ax.set_xticklabels(xlabels, fontsize=fontsize)

# this lets you update an immutable value in an arbitrarily nested
	# list/dict object using an arbitrary list of indices/keys
	# important because python can't return references so this is
	# required to update the reference inside the mutable object
def update_by_key_ind_list(obj, key_ind_list, fnxn=None, new_val=None, pop_val=False):
	# print(colored('running fnxn?', 'white'))
	# obj = mutable object
	# key_ind_list = list of keys/indices to grab element
	# fnxn = arbitrary function to update element
	# new_val = specific value to set the element to
		# choice either update fnxn or new_val
		# or leave blank to just return the element
	# print('len input list, 1815: ', len(obj))

	if sum([fnxn is not None, new_val is not None, pop_val is True]) > 1:
		raise ValueError('Cannot supply more than 1 out of fnxn, new_val, pop_val.')

	# print(key_ind_list)
	# print(obj)

	# first, grabbing the value of the immutable element
	if fnxn is not None:
		my_obj = obj
		for key_ind, this_key in enumerate(key_ind_list):
			# print(this_key, my_obj)
			my_obj = my_obj[this_key]
		# print(my_obj)

	# second, grabbing the reference to the immutable element
	# print(colored(str(key_ind_list), 'blue'))
	my_obj2 = obj
	for key_ind, this_key in enumerate(key_ind_list):
		# looping through the ind/key list
		# print('key_ind + len:', key_ind, len(key_ind_list), key_ind_list)
		if key_ind < len(key_ind_list) - 1:
			# print(9)
			# print('not last')
			# print('\t', colored(str(this_key), 'red'))
			# print('\t', colored(str(this_key), 'red'), my_obj2)
			my_obj2 = my_obj2[this_key]
		else:
			# print(10)
			# print('last')
			# print('\t', colored(str(this_key), 'red'))
			# print('\t', colored(str(this_key), 'red'), my_obj2)
			# for the last loop, do not grab the immutable element, 
				# but modify the value in-place using the reference (left)
				# and the already retrieved value (right)
			if fnxn is not None:
				# print(colored('fnxn', 'red'))
				# print(obj)
				# print(my_obj)
				my_obj2[this_key] = fnxn(my_obj)
			else:
				if new_val is not None:
					my_obj2[this_key] = new_val
				elif pop_val is True:
					return my_obj2.pop(this_key)
				else:
					return my_obj2[this_key]

def check_make_dir(input_dir, abs_path=True, verbose=False):
	dir_list = input_dir.split('/')
	new_dir = ''
	if abs_path == True:
		for i in range(len(dir_list[1:])):
			new_dir = new_dir+'/'+dir_list[i+1]
			if i > 4:
				if not os.path.exists(new_dir):
					os.makedirs(new_dir)
					if verbose==True:
						print('Making:', new_dir)
	if abs_path == False:
		for i in range(len(dir_list)):
			new_dir = new_dir+'/'+dir_list[i]
			if new_dir[0] == '/':
				new_dir = new_dir[1:]
			if not os.path.exists(new_dir):
				os.makedirs(new_dir)
				if verbose==True:
					print('Making:', new_dir)

def get_hex_colors(name_list_spot, number_list_spot=None):
	name_list = open_and_read(name_list_spot, '\t')
	name_dict = list_to_dictionary(name_list)
	if number_list_spot is not None:
		number_list = open_and_read(number_list_spot, '\t')
		number_dict = list_to_dictionary(number_list)
		return name_dict, number_dict
	return name_dict

def get_hex_colors_from_files(input_list, name_list_spot, \
	number_list_spot=None):
	name_list = open_and_read(name_list_spot, '\t')
	name_dict = list_to_dictionary(name_list)
	if number_list_spot is not None:
		number_list = open_and_read(number_list_spot, '\t')
		number_dict = list_to_dictionary(number_list)
	hex_names = []
	for item in input_list:
		if number_list_spot is not None:
			try:
				int(item)
				item_type = 'number'
			except:
				item_type = 'not_number'
		else:
			item_type = 'not_number'
		if item_type is 'number':
			hex_names.append(number_dict.get(item))
		elif item[0] == '#':
			hex_names.append(item)
		else:
			this_hex = name_dict.get(item)
			if this_hex is None:
				raise ValueError(str(item)+\
					' is not a name in this name dictionary')
			else:
				hex_names.append(this_hex)
	return hex_names

def sort_bed_on_location(input_bed):
	if isinstance(input_bed[0][1], str):
		was_str = True
	else:
		was_str = False
	for line_loop in range(len(input_bed)):
		input_bed[line_loop][1] = int(input_bed[line_loop][1])
	input_bed = sorted(input_bed, key=operator.itemgetter(0,1))
	if was_str == True:
		for line_loop in range(len(input_bed)):
			input_bed[line_loop][1] = str(input_bed[line_loop][1])
	return input_bed

def make_shape_merged_bed(top_merged_dir, file_dict, merge_len, \
	data_info):
	for test_group in file_dict.keys():
		for sample_type in file_dict.get(test_group).keys():
			for sample in file_dict.get(test_group).get(sample_type):
				if not os.path.isfile(sample[2]):
					manual_merge(sample[1], merge_len, sample[2], \
						**data_info.get('control'))
					add_len_to_bed(sample[2])
	
def old_make_shape_merged_bed(save_dirs, pipeline_dirs, directories, \
	list_settings, data_info):
	file_list1 = []
	for type_loop in range(len(directories)):
		file_list1.append([])
		for filepath in glob.iglob(os.path.join(\
			pipeline_dirs.get('loaded_data_dir'), '*.bed')):
			sample_type, _ = get_sample_type_and_id(filepath)
			if directories[type_loop] == sample_type:
				file_list1[-1].append(filepath)
	for type_loop in range(len(file_list1)):
		for file in file_list1[type_loop]:
			this_dir = os.path.join(save_dirs.get('top_merged_dir'), \
				directories[type_loop])
			os.makedirs(this_dir, exist_ok=True)
			_, sample_id = get_sample_type_and_id(file)
			new_name = os.path.join(this_dir, sample_id+'_merged.bed')
			if not os.path.isfile(new_name):
				manual_merge(file, list_settings.get('merge_len'), \
					new_name, data_info)
				add_len_to_bed(new_name)

def skip_merged_bed(save_dirs, pipeline_dirs, directories, \
	list_settings, data_info):
	file_list1 = []
	for type_loop in range(len(directories)):
		file_list1.append([])
		for filepath in glob.iglob(os.path.join(\
			pipeline_dirs.get('loaded_data_dir'), '*.bed')):
			sample_type, _ = get_sample_type_and_id(filepath)
			if directories[type_loop] == sample_type:
				file_list1[-1].append(filepath)
	for type_loop in range(len(file_list1)):
		for file in file_list1[type_loop]:
			this_dir = os.path.join(save_dirs.get('top_merged_dir'), \
				directories[type_loop])
			os.makedirs(this_dir, exist_ok=True)
			_, sample_id = get_sample_type_and_id(file)
			new_name = os.path.join(this_dir, sample_id+'_merged.bed')
			if not os.path.isfile(new_name):
				copyfile(file, new_name)

def map_sample_names(sample_id, name_mapping_filepath=None, verbose=True):
	if name_mapping_filepath is None:
		return sample_id
	if not os.path.isfile(name_mapping_filepath):
		raise ValueError(\
			'Referenced name mapping filepath that does not exist: {}'\
				.format(name_mapping_filepath))
	with open(name_mapping_filepath, 'r', encoding='utf-8', \
		newline='\n') as textfile:
		for line in textfile:
			line = line.rstrip('\n').split(',')
			if line[0] == sample_id:
				return line[1]
	print('Did not find sample {} in name mapping file'\
		.format(sample_id))
	return sample_id

def make_map_sample_names(test_mapping_filepath=None, \
	control_mapping_filepath=None, verbose=True):
	for name_mapping_filepath in [test_mapping_filepath, control_mapping_filepath]:
		if name_mapping_filepath is not None:
			if not os.path.isfile(name_mapping_filepath):
				raise ValueError(\
					'Referenced name mapping filepath that does not exist: {}'\
						.format(name_mapping_filepath))

	def mapping_function(sample_id, test_or_control, verbose=True):
		if test_or_control is 'test':
			mapping_file = test_mapping_filepath
		elif test_or_control is 'control':
			mapping_file = control_mapping_filepath
		if mapping_file is None:
			return sample_id
		with open(mapping_file, 'r', encoding='utf-8', \
			newline='\n') as textfile:
			for line in textfile:
				line = line.rstrip('\n').split(',')
				if line[0] == sample_id:
					return line[1]
		print('Did not find sample {} in name mapping file'\
			.format(sample_id))
		return sample_id

	return mapping_function

def make_sample_quantile_plots(fig_data_save, plot_dir, percents, \
	len_or_dens, file_dict, test_group_name, control_group_name, \
	mapping_function, verbose=True, merge_size=1000, cutoff=4000, percent_keep=95): 
	if percent_keep not in percents:
		raise ValueError(\
			'Percent keep, {}, for make_sample_quantile_plots not in percents list'\
			.format(percent_keep))
	index_keep = percents.index(percent_keep)
	keep_line = []
	
	for test_group in file_dict.keys():
		if len_or_dens == 'len':
			save_dir = os.path.join(plot_dir, 'percent_plots', \
				'len_percent_plots', test_group)
		else:
			save_dir = os.path.join(plot_dir, 'percent_plots', \
				'dens_percent_plots', test_group)
		os.makedirs(save_dir, exist_ok=True)
		for sample_type in file_dict.get(test_group).keys():
			plot_path = os.path.join(save_dir, sample_type)
			if not os.path.isfile(plot_path):
				sample_values = {}
				for sample in file_dict.get(test_group).get(sample_type):
					sample_groups = '{}_{}'\
						.format(test_group, sample_type)
					if test_group == test_group_name:
						sample_name = mapping_function(sample[0], 'test')
						plot_name = '{} {}'\
							.format(test_group_name, sample_type)
					if test_group == control_group_name:
						sample_name = mapping_function(sample[0], 'control')
						plot_name = '{} {}'\
							.format(control_group_name, sample_type)
					one_sample_values = []
					with open(sample[1], 'r') as textfile:
						values = []
						for line in textfile:
							line = line.rstrip('\n').split('\t')
							if len_or_dens == 'len':
								values.append(int(line[3]))
							else:
								values.append(float(line[4]))
					values = sorted(values)
					for percent in percents:
						one_sample_values.append(\
							values[int(len(values)*(percent/100))])
					sample_values[sample_name] = one_sample_values
				quantile_plot_dict = {
					'percents': percents,
					'sample_values': sample_values,
					'sample_groups': sample_groups,
					'save_path': plot_path,
					'index_keep': index_keep,
					'plot_name': plot_name,
				}
				if len_or_dens == 'len':
					quantile_dict_dir = os.path.join(fig_data_save, \
						'len_quantile_plot_dict')
				else:
					quantile_dict_dir = os.path.join(fig_data_save, \
						'dens_quantile_plot_dict')
				os.makedirs(quantile_dict_dir, exist_ok=True)
				with open(os.path.join(quantile_dict_dir, sample_groups), \
					'w') as textfile:
					json.dump(quantile_plot_dict, textfile)
				plot_sample_quantiles(\
					quantile_plot_dict.get('percents'), \
					quantile_plot_dict.get('sample_values'), 
					quantile_plot_dict.get('sample_groups'), \
					quantile_plot_dict.get('save_path'),
					quantile_plot_dict.get('index_keep'), \
					quantile_plot_dict.get('plot_name'))

def plot_sample_quantiles(percents, sample_vals, group_name, save_path, \
	index_keep, plot_name):
	color_list = ['C0', 'C1', 'C2', 'C3', 'C4', 'C5', 'C6', 'C7', \
	'C8', 'C9']
	color_dict = {}
	for percent_loop in range(len(percents)):
		color_dict[percents[percent_loop]] = color_list[percent_loop]
	labels = [color_dict.get(thing) for thing in percents]
	legend_elements = []
	for value in percents:
		legend_elements.append(Line2D([0], [0], marker='o', color='w', \
			label=str(value)+r'%', \
			markerfacecolor=color_dict.get(value), markersize=9))

	fig,ax = plt.subplots(figsize=(12,10))
	val_names = list(sample_vals.keys())
	val_sort_vals = [sample_vals.get(this_name)[int(len(percents)/2)+1] \
		for this_name in val_names]
	val_names = sorted(list(zip(val_names, val_sort_vals)), \
		key=operator.itemgetter(1))
	val_names = [name[0] for name in val_names]
	for item_loop in range(len(val_names)):
		dots = [int(thing) for thing in \
			sample_vals.get(val_names[item_loop])]
		line_x = list(range(dots[0],dots[-1]))
		plt.plot(line_x, [item_loop+1]*len(line_x), c='k', linewidth=0.2)
		plt.scatter(dots, [item_loop+1]*len(dots), c=labels)
		plt.axvline(x=4000, c='k', linewidth=0.3)
	ax.set_yticks([thing+1 for thing in range(len(val_names))])
	ax.set_yticklabels(val_names)
	plt.ylim([0, len(val_names)+1])
	plt.xlim([0, int(max([sample_vals.get(thing)[-1] for thing in \
		sample_vals.keys()])*1.02)])
	plt.title(plot_name+' Percent Plots')
	if any([len(this_name) > 4 for this_name in val_names]):
		fig.subplots_adjust(right=0.84, left=0.32, bottom=0.13)
	else:
		fig.subplots_adjust(right=0.75, left=0.175, bottom=0.13)
	plt.legend(handles=legend_elements, bbox_to_anchor=(1.02, 1), \
		loc=2, borderaxespad=0)
	plt.savefig(save_path)
	plt.close()
	
def len_dens_change_scatter(no_change_dict, lengthened_dict, shortened_dict, 
	all_no_change_dict, all_lengthened_dict, all_shortened_dict, plot_dir, 
	data_for_figures, name_str, color_dict, len_indices, dens_indices, **kwargs):
	for test_group in all_no_change_dict.keys():
		for sample_type in all_no_change_dict.get(test_group).keys():
			use_name_str = os.path.join(name_str, sample_type)
			save_spot = os.path.join(plot_dir, use_name_str)+'.png'
			dict_save_spot = os.path.join(data_for_figures, use_name_str)
			if not os.path.isfile(save_spot) or not os.path.isfile(dict_save_spot):
				accepted_peaks = set()
				for peak in open_and_read(lengthened_dict.get(test_group)
					.get(sample_type)[0], '\t'):
					accepted_peaks.add((peak[0], int(peak[1]), int(peak[2])))
				for peak in open_and_read(shortened_dict.get(test_group)
					.get(sample_type)[0], '\t'):
					accepted_peaks.add((peak[0], int(peak[1]), int(peak[2])))

				peak_list = []
				for file_dict in [all_no_change_dict, all_lengthened_dict, all_shortened_dict]:
					for peak in open_and_read(file_dict.get(test_group)
						.get(sample_type)[0], '\t'):
						peak_location = (peak[0], int(peak[1]), int(peak[2]))
						peak_len_change = float(peak[len_indices[1]]) - \
							float(peak[len_indices[0]])
						peak_dens_change = float(peak[dens_indices[1]]) / \
							float(peak[dens_indices[0]])
						if peak_location in accepted_peaks:
							peak_color = 1
						else:
							peak_color = 0
						peak_list.append(list(peak_location)+[peak_len_change]+
							[peak_dens_change]+[peak_color])

				change_scatter_dict = {
					'peak_stats': peak_list, 
					'cancer_type': sample_type, 
				}

				os.makedirs(os.path.dirname(save_spot), exist_ok=True)
				os.makedirs(os.path.dirname(dict_save_spot), exist_ok=True)
				if not os.path.isfile(dict_save_spot):
					with open(dict_save_spot, 'w') as textfile:
						json.dump(change_scatter_dict, textfile)

				if not os.path.isfile(save_spot):
					fig = easy_fig(figsize=(8, 8), gridsize=(1000,1000), \
						subplot_left=0.01, subplot_right=0.99, subplot_bottom=0.01, \
						subplot_top=0.99)
					fig.add_panel(top_left=(0.1,0.1), bottom_right=(0.9,0.9), \
						x_count=1, y_count=1, subpanel_pad_percent=0)	

					change_scatter_just_plot(
						fig.axes[0][0], 
						change_scatter_dict.get('peak_stats'), 
						change_scatter_dict.get('cancer_type'), 
						save_spot,
						**kwargs)

					plt.savefig(save_spot, dpi=300)
					plt.close()

# code for supp fig F
def change_scatter_just_plot(ax, peak_stats, cancer_type, x_ax_text_ratio=1, 
	min_fontsize=5, xcutoff=-2000, ycutoff=1, scatter_size=1, scatter_alpha=0.25, 
	scatter_neg_c='#1f77b4', scatter_pos_c='r', middot_size=6, middot_color='k', 
	middot_alpha=1, cutoff_linestyle='--', cutoff_line_color='k', cutoff_linewidth=1, 
	yscale_log=True, xlim=(-30000, 30000), 
	xticks=(-30000, -20000, -10000, 0, 10000, 20000, 30000), ylim=(0.01, 100)):
	ax.scatter(
				[peak[3] for peak in peak_stats], 
				[peak[4] for peak in peak_stats],
				color=[scatter_pos_c if peak[5] == 1 else scatter_neg_c for 
					peak in peak_stats],
				alpha=scatter_alpha,
				s=scatter_size
		)
	ax.scatter(0,1, s=middot_size, color=middot_color, alpha=middot_alpha)
	
	easy_fig.format_panel(ax,
		vline_dict={'x': xcutoff, 'linestyle': cutoff_linestyle, 
			'color': cutoff_line_color, 'linewidth': cutoff_linewidth}, 
		hline_dict={'y': ycutoff, 'linestyle': cutoff_linestyle, 
			'color': cutoff_line_color, 'linewidth': cutoff_linewidth}, 
		ylog=yscale_log,
		xlabel_dict={'xlabel': 'Absolute Length Change', 'labelpad': 0.5, 
			'fontsize': min_fontsize},
		ylabel_dict={'ylabel': 'Log Density Ratio Change', 'labelpad': 0.5, 
			'fontsize': min_fontsize},
		xlim=xlim,
		xtick_dict={'ticks': xticks},
		ylim=ylim,
		title_dict={'label': cancer_type, 'pad': 3}
	)

	make_xticks_kb(ax, min_fontsize)

def old_change_scatter_just_plot(ax, peak_stats, cancer_type, x_ax_text_ratio=1, \
	min_fontsize=5, xcutoff=-2000, ycutoff=1, scatter_size=1, scatter_alpha=0.25, \
	scatter_neg_c='#1f77b4', scatter_pos_c='r', middot_size=6, middot_color='k', middot_alpha=1, \
	cutoff_linestyle='--', cutoff_line_color='k', cutoff_linewidth=1, yscale_log=True, \
	xlim=(-30000, 30000), xticks=(-30000, -20000, -10000, 0, 10000, 20000, 30000), \
	ylim=(0.01, 100)):
	yes_group = [thing for thing in peak_stats if float(thing[0]) < xcutoff and float(thing[1]) > ycutoff]
	no_group = [thing for thing in peak_stats if (not float(thing[0]) < xcutoff) or \
		(not float(thing[1]) > ycutoff)]
	ax.scatter([float(thing[0]) for thing in no_group], [float(thing[1]) for thing in no_group], \
		color=scatter_neg_c, s=scatter_size, alpha=scatter_alpha)
	ax.scatter([float(thing[0]) for thing in yes_group], [float(thing[1]) for thing in yes_group], \
		color=scatter_pos_c, s=scatter_size, alpha=scatter_alpha)
	ax.scatter(0,1, s=middot_size, color=middot_color, alpha=middot_alpha)
	
	easy_fig.format_panel(ax,
		vline_dict={'x': xcutoff, 'linestyle': cutoff_linestyle, 'color': cutoff_line_color, \
			'linewidth': cutoff_linewidth}, 
		hline_dict={'y': ycutoff, 'linestyle': cutoff_linestyle, 'color': cutoff_line_color, \
			'linewidth': cutoff_linewidth}, 
		ylog=yscale_log,
		xlabel_dict={'xlabel': 'Absolute Length Change', 'labelpad': 0.5, 'fontsize': min_fontsize},
		ylabel_dict={'ylabel': 'Log Density Ratio Change', 'labelpad': 0.5, 'fontsize': min_fontsize},
		xlim=xlim,
		xtick_dict={'ticks': xticks},
		ylim=ylim,
		title_dict={'label': cancer_type, 'pad': 3}
	)

	make_xticks_kb(ax, min_fontsize)

def one_type_split_scatter(test_group, sample_type, samples, plot_dir, fig_data_save, 
	analysis_name, ylimits, xlimits, dens_index, len_index, vert_line, 
	max_x_ratio, max_y_ratio, dens_bins, len_bins, log_norm, vmax, vmin):
	merged_dir = os.path.join(plot_dir, 'merged')
	split_dir = os.path.join(plot_dir, 'split')

	save_dir_merged_group = os.path.join(merged_dir, 'all')
	save_dir_split_group = os.path.join(split_dir, 'all')
	os.makedirs(save_dir_merged_group, exist_ok=True)
	os.makedirs(save_dir_split_group, exist_ok=True)

	save_dir_merged = os.path.join(merged_dir, \
		test_group, sample_type)
	save_dir_split = os.path.join(split_dir, \
		test_group, sample_type)
	os.makedirs(save_dir_merged, exist_ok=True)
	os.makedirs(save_dir_split, exist_ok=True)

	group_name = name_var = '{}_{}'.format(test_group, sample_type)
	split_plot_dict_dir = os.path.join(fig_data_save, 
		'split_plot_dict', analysis_name)
	os.makedirs(split_plot_dict_dir, exist_ok=True)
	merged_group_path = os.path.join(save_dir_merged_group, \
		group_name+'.png')
	merged_group_dict_save = os.path.join(split_plot_dict_dir, 
		name_var+'_merged.json')
	split_group_path = os.path.join(save_dir_split_group, \
		group_name+'.png')
	split_group_dict_save = os.path.join(split_plot_dict_dir, 
		name_var+'_split.json')

	all_merged_vals = []
	all_split_vals = []
	for sample in samples:
		sample_id = sample[0]
		merged_path = sample[1]
		split_path = sample[2]
		
		merged_save_path = os.path.join(save_dir_merged, \
			sample_id+'.png')
		merged_dict_save = os.path.join(split_plot_dict_dir, 
			sample_id+'_merged.json')
		split_dict_save = os.path.join(split_plot_dict_dir, 
			sample_id+'_split.json')
		split_save_path = os.path.join(save_dir_split, \
			sample_id+'.png')

		if not os.path.isfile(merged_save_path) or \
			not os.path.isfile(merged_group_path) or \
			not os.path.isfile(merged_group_dict_save) or \
			not os.path.isfile(merged_dict_save): 
			merged_vals = []
			with open(merged_path, 'r') as textfile:
				for line in textfile:
					line = line.rstrip('\n').split('\t')
					merged_vals.append((float(line[dens_index]), 
						float(line[len_index])))
					all_merged_vals.append((float(line[dens_index]), 
						float(line[len_index])))

			if not os.path.isfile(merged_save_path) or \
				not os.path.isfile(merged_dict_save):
				if len(merged_vals) > 0:
					split_plot_dict = {
						'log_norm': log_norm, 
						'vert_line': vert_line, 
						'name_var': sample_id.replace('_merged', ''), 
						'save_path': merged_save_path,
						'peak_vals': merged_vals, 
						'len_bins': len_bins, 'dens_bins': dens_bins, 
						'vmin': vmin, 'vmax': vmax, 
						'xlimits': xlimits, 'ylimits': ylimits, 
						'max_x_ratio': max_x_ratio, 'max_y_ratio': max_y_ratio,
					}

					with open(merged_dict_save, 'w') as \
						textfile:
						json.dump(split_plot_dict, textfile)
					split_plot_just_plotting(**split_plot_dict)

		if not os.path.isfile(split_save_path) or \
			not os.path.isfile(split_group_path) or \
			not os.path.isfile(split_group_dict_save) or \
			not os.path.isfile(split_dict_save):
			split_vals = []
			with open(split_path, 'r') as textfile:
				for line in textfile:
					line = line.rstrip('\n').split('\t')
					split_vals.append((float(line[dens_index]), 
						float(line[len_index])))
					all_split_vals.append((float(line[dens_index]), 
						float(line[len_index])))

			if not os.path.isfile(split_save_path): 
				if len(split_vals) > 0:
					split_plot_dict = {
						'log_norm': log_norm, 
						'vert_line': vert_line, 
						'name_var': sample_id.replace('_split', ''), 
						'save_path': split_save_path,
						'peak_vals': split_vals, 
						'len_bins': len_bins, 'dens_bins': dens_bins, 
						'vmin': vmin, 'vmax': vmax, 
						'xlimits': xlimits, 'ylimits': ylimits, 
						'max_x_ratio': max_x_ratio, 'max_y_ratio': max_y_ratio,
					}
					# split_plot_dict_dir = os.path.join(fig_data_save, 
					# 	'split_plot_dict', analysis_name)
					# os.makedirs(split_plot_dict_dir, exist_ok=True)

					with open(split_dict_save, 'w') as \
						textfile:
						json.dump(split_plot_dict, textfile)
					split_plot_just_plotting(**split_plot_dict)

	if not os.path.isfile(merged_group_path) or \
		not os.path.isfile(merged_group_dict_save):
		if len(all_merged_vals) > 0:
			split_plot_dict = {
				'log_norm': log_norm, 
				'vert_line': vert_line, 
				'name_var': group_name, 
				# 'name_var': group_name+'_merged', 
				'save_path': merged_group_path,
				'peak_vals': all_merged_vals, 
				'len_bins': len_bins, 'dens_bins': dens_bins, 
				'vmin': vmin, 'vmax': vmax, 
				'xlimits': xlimits, 'ylimits': ylimits, 
				'max_x_ratio': max_x_ratio, 'max_y_ratio': max_y_ratio,
			}
			# split_plot_dict_dir = os.path.join(fig_data_save, 
			# 	'split_plot_dict', analysis_name)
			# os.makedirs(split_plot_dict_dir, exist_ok=True)

			with open(merged_group_dict_save, 'w') \
				as textfile:
				json.dump(split_plot_dict, textfile)
			split_plot_just_plotting(**split_plot_dict)

	if not os.path.isfile(split_group_path) or \
		not os.path.isfile(split_group_dict_save):
		if len(all_split_vals) > 0:
			split_plot_dict = {
				'log_norm': log_norm, 
				'vert_line': vert_line, 
				'name_var': group_name, 
				# 'name_var': group_name+'_split', 
				'save_path': split_group_path,
				'peak_vals': all_split_vals, 
				'len_bins': len_bins, 'dens_bins': dens_bins, 
				'vmin': vmin, 'vmax': vmax, 
				'xlimits': xlimits, 'ylimits': ylimits, 
				'max_x_ratio': max_x_ratio, 'max_y_ratio': max_y_ratio,
			}
			# split_plot_dict_dir = os.path.join(fig_data_save, 
			# 	'split_plot_dict', analysis_name)
			# os.makedirs(split_plot_dict_dir, exist_ok=True)

			with open(split_group_dict_save, 'w') \
				as textfile:
				json.dump(split_plot_dict, textfile)
			split_plot_just_plotting(**split_plot_dict)

	# old code
		# def single_file_type_scatters()
		
		# setting up save dirs
		# save_dir_merged_group = os.path.join(plot_dir, merged_string, 'all')
		# save_dir_split_group = os.path.join(plot_dir, split_string, 'all')

		# merged_group_path = os.path.join(save_dir_merged_group, \
		# 	sample_type+'.png')
		# split_group_path = os.path.join(save_dir_split_group, \
		# 	sample_type+'.png')

		# os.makedirs(save_dir_merged, exist_ok=True)
		# os.makedirs(save_dir_split, exist_ok=True)
			# dir_merged_file_list.append(filepath)
			# dir_split_file_list.append(second_path)

			# second_exists = os.path.isfile(second_path)
			# all_lengths = []
			# print('checking to do merge')
				# print('\tgonna run merge')
						# print('\tgonna plot merge')
							# 'slopes': slopes
						# x_axes, y_axes = split_plot_just_plotting(
						# 	split_plot_dict.get('log_norm'), 
						# 	split_plot_dict.get('lengths'), 
						# 	split_plot_dict.get('densities'), 
						# 	split_plot_dict.get('len_bins'), 
						# 	split_plot_dict.get('dens_bins'), 
						# 	split_plot_dict.get('vmin'), 
						# 	split_plot_dict.get('vmax'), 
						# 	split_plot_dict.get('xlimits'), 
						# 	split_plot_dict.get('ylimits'), 
						# 	split_plot_dict.get('vert_line'), 
						# 	split_plot_dict.get('name_var'), 
						# 	split_plot_dict.get('save_path'), 
						# 	split_plot_dict.get('max_x_ratio'), 
						# 	split_plot_dict.get('max_y_ratio')
						# )
			# print('checking to do split')
				# print('\tgonna run split')
						# print('\tgonna plot split')
							# 'slopes': slopes
		# print('checking to do all merge')
			# name_var = '{}_{}_merged'.format(test_group, sample_type)
				# print('\tgonna plot all merge')
					# 'slopes': slopes
			# print('checking to do all split')
			# name_var = '{}_{}_split'.format(test_group, sample_type)
				# print('\tgonna plot all split')
					# 'slopes': slopes
		# raise



					# x_axes, y_axes = split_plot_just_plotting(
					# split_plot_just_plotting(
					# 	split_plot_dict.get('log_norm'), 
					# 	split_plot_dict.get('lengths'), 
					# 	split_plot_dict.get('densities'), 
					# 	split_plot_dict.get('len_bins'), 
					# 	split_plot_dict.get('dens_bins'), 
					# 	split_plot_dict.get('vmin'), 
					# 	split_plot_dict.get('vmax'), 
					# 	split_plot_dict.get('xlimits'), 
					# 	split_plot_dict.get('ylimits'), 
					# 	split_plot_dict.get('vert_line'), 
					# 	split_plot_dict.get('name_var'), 
					# 	split_plot_dict.get('save_path'), 
					# 	split_plot_dict.get('max_x_ratio'), 
					# 	split_plot_dict.get('max_y_ratio')
					# )

				# for files in file_list:


					# i = 0
						# if 'chr' in line[0]:
						# 	if 'start' not in line[1]:

				# plot_peak_len_dens_histo(fig_data_save, \
				# 	analysis_name, [second_path], \
				# 	second_save_path, sample_id, \
				# 	ylimits=ylimits, xlimits=xlimits, \
				# 	dens_index=dens_index, \
				# 	vert_line=vert_line, 
				# 	max_x_ratio=max_x_ratio, 
				# 	max_y_ratio=max_y_ratio)
				# fig_data_save, analysis_name, file_list, 
				# save_path, name_var, 

				# need: save_path

		# def split_plot_just_plotting(log_norm, peak_vals, len_bins, \
		# 	dens_bins, vmin, vmax, xlimits, ylimits, vert_line, name_var, \
		# 	save_path):
		# print('\t\t', save_path)
		# return

		# log_norm: True
		# vert_line: 4000
		# name_var: E028
		# save_path: /Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_04_09_no_mp/E/plots/4kb_broad/split_scatter_plots/NonCancer/BRCA/E028.png
		# len_bins: 1000
		# dens_bins: 200
		# vmin: None
		# vmax: None
		# xlimits: (0, 40000)
		# ylimits: (0, 150)

		# raise
		# , slopes

		# if slopes != None:
		# 	abline(slope1, intercept1)
		# 	abline(slope2, intercept2)

def split_plot_just_plotting(log_norm, vert_line, name_var, save_path, 
	peak_vals, len_bins, dens_bins, vmin, vmax, xlimits, ylimits, 
	max_x_ratio, max_y_ratio):

	fig = plt.figure(figsize=(12,10))
	if log_norm == False:
		plt.hist2d([peak[1] for peak in peak_vals], 
			[peak[0] for peak in peak_vals], bins=[len_bins,dens_bins], \
			vmin=vmin, vmax=vmax)
	elif log_norm == True:
		plt.hist2d([peak[1] for peak in peak_vals], 
			[peak[0] for peak in peak_vals], bins=[len_bins,dens_bins], \
			norm=LogNorm())
	if xlimits is not None:
		plt.xlim(xlimits)
	if ylimits is not None:
		plt.ylim(ylimits)
	axes = plt.gca()
	y_axes = axes.get_ylim()
	x_axes = axes.get_xlim()
	if vert_line != None:
		plt.axvline(x=vert_line, c='b')
	plt.xlabel('Peak Lengths')
	plt.ylabel('Peak Densities')
	if log_norm == True:
		plt.title('LogNorm 2D-Histogram of '+name_var+' Peaks')
	elif log_norm == False:
		plt.title('2D-Histogram of '+name_var+' Peaks')
	plt.colorbar()
	plt.savefig(save_path)
	plt.close()
	return x_axes, y_axes


def make_split_plots(plot_dir, file_dict, fig_data_save, analysis_name, 
	ylimits=(0,150), xlimits=(0,40000), dens_index=4, len_index = 3, 
	vert_line=4000, mp_workers=None, plot_string='split_scatter_plots', 
	len_bins=1000, dens_bins=200, log_norm=True, vmax=None, vmin=None,
	max_x_ratio=1.3, max_y_ratio=1.3):
	# file dict has merged beds and split beds

	# this loops through file groups in the file dict and each loop runs
		# one_type_split_scatter to create sample-specific and type-specific
		# split scatter plots for the file group
	for test_group in file_dict.keys():
		for sample_type in file_dict.get(test_group).keys():
			if mp_workers is not None:
				mp_workers.add_job(one_type_split_scatter,
					(test_group, sample_type, 
					file_dict.get(test_group).get(sample_type)),
					{
						'plot_dir': os.path.join(plot_dir, plot_string), 
						'fig_data_save': fig_data_save, 
						'analysis_name': analysis_name,
						'ylimits': ylimits, 
						'xlimits': xlimits, 
						'dens_index': dens_index, 
						'len_index': len_index, 
						'vert_line': vert_line, 
						'max_x_ratio': max_x_ratio, 
						'max_y_ratio': max_y_ratio,
						'dens_bins': dens_bins,
						'len_bins': len_bins, 
						'log_norm': log_norm, 
						'vmax': vmax,
						'vmin': vmin
					},
					job_group='split_plots')
			else:
				one_type_split_scatter(test_group, sample_type, 
					file_dict.get(test_group).get(sample_type),
					plot_dir = os.path.join(plot_dir, plot_string), 
					fig_data_save = fig_data_save, 
					analysis_name = analysis_name,
					ylimits = ylimits, 
					xlimits = xlimits, 
					dens_index = dens_index, 
					len_index = len_index, 
					vert_line = vert_line, 
					max_x_ratio = max_x_ratio, 
					max_y_ratio = max_y_ratio,
					dens_bins = dens_bins,
					len_bins = len_bins, 
					log_norm = log_norm, 
					vmax = vmax,
					vmin = vmin)


		# old code
			# save_dir_merged = os.path.join(plot_dir, merged_string, \
			# 	test_group, sample_type)
			# save_dir_split = os.path.join(plot_dir, split_string, \
			# 	test_group, sample_type)
			# os.makedirs(save_dir_merged, exist_ok=True)
			# os.makedirs(save_dir_split, exist_ok=True)
			# merged_group_path = os.path.join(save_dir_merged_group, \
			# 	sample_type+'.png')
			# split_group_path = os.path.join(save_dir_split_group, \
			# 	sample_type+'.png')

			# dir_merged_file_list = []
			# dir_split_file_list = []
			# for sample in file_dict.get(test_group).get(sample_type):
			# 	sample_id = sample[0]
			# 	filepath = sample[1]
			# 	second_path = sample[2]
			# 	dir_merged_file_list.append(filepath)
			# 	dir_split_file_list.append(second_path)

			# 	second_exists = os.path.isfile(second_path)
				
			# 	save_path = os.path.join(save_dir_merged, \
			# 		sample_id+'.png')
			# 	second_save_path = os.path.join(save_dir_split, \
			# 		sample_id+'.png')

			# 	if (not os.path.isfile(save_path)) or (second_exists \
			# 		and (not os.path.isfile(second_save_path))):
			# 		if mp_workers is not None:
			# 			mp_workers.add_job(plot_peak_len_dens_histo,
			# 				(fig_data_save, analysis_name, [filepath], \
			# 				save_path, sample_id), \
			# 				{'ylimits': ylimits, 'xlimits': xlimits, \
			# 				'dens_index': dens_index, 'vert_line': vert_line, 
			# 				'max_x_ratio': max_x_ratio, 
			# 				'max_y_ratio': max_y_ratio})
			# 		else:
			# 			plot_peak_len_dens_histo(\
			# 				fig_data_save, analysis_name, [filepath], \
			# 				save_path, sample_id, \
			# 				ylimits=ylimits, xlimits=xlimits, \
			# 				dens_index=dens_index, vert_line=vert_line, 
			# 				max_x_ratio=max_x_ratio, 
			# 				max_y_ratio=max_y_ratio)
			# 		if second_exists == True:
			# 			if mp_workers is not None:
			# 				mp_workers.add_job(plot_peak_len_dens_histo, 
			# 						(fig_data_save, \
			# 						analysis_name, [second_path], \
			# 						second_save_path, sample_id), \
			# 						{'ylimits': ylimits, 'xlimits': xlimits, \
			# 						'dens_index': dens_index, \
			# 						'vert_line': vert_line, 
			# 						'max_x_ratio': max_x_ratio, 
			# 						'max_y_ratio': max_y_ratio}, 
			# 					job_group='split_plots')
			# 			else:
			# 				plot_peak_len_dens_histo(fig_data_save, \
			# 					analysis_name, [second_path], \
			# 					second_save_path, sample_id, \
			# 					ylimits=ylimits, xlimits=xlimits, \
			# 					dens_index=dens_index, \
			# 					vert_line=vert_line, 
			# 					max_x_ratio=max_x_ratio, 
			# 					max_y_ratio=max_y_ratio)

			# if (not os.path.isfile(merged_group_path)):
			# 	if mp_workers is not None:
			# 		mp_workers.add_job(plot_peak_len_dens_histo, 
			# 			(fig_data_save, analysis_name, dir_merged_file_list, \
			# 			merged_group_path, sample_type+'_merged'), \
			# 			{'ylimits': ylimits, 'xlimits': xlimits, \
			# 			'dens_index': dens_index, 'vert_line': vert_line, 
			# 			'max_x_ratio': max_x_ratio, 
			# 			'max_y_ratio': max_y_ratio}, 
			# 			job_group='split_plots')
			# 	else:
			# 		plot_peak_len_dens_histo(\
			# 			fig_data_save, analysis_name, dir_merged_file_list, \
			# 			merged_group_path, sample_type+'_merged', \
			# 			ylimits=ylimits, xlimits=xlimits, \
			# 			dens_index=dens_index, vert_line=vert_line, 
			# 				max_x_ratio=max_x_ratio, 
			# 				max_y_ratio=max_y_ratio)
			# if (not os.path.isfile(split_group_path)):
			# 	if mp_workers is not None:
			# 		mp_workers.add_job(plot_peak_len_dens_histo, 
			# 			(fig_data_save, analysis_name, dir_split_file_list, \
			# 			split_group_path, sample_type+'_split'), \
			# 			{'ylimits': ylimits, 'xlimits': xlimits, \
			# 			'dens_index': dens_index, 'vert_line': vert_line, 
			# 			'max_x_ratio': max_x_ratio, 
			# 			'max_y_ratio': max_y_ratio}, 
			# 			job_group='split_plots')
			# 	else:
			# 		plot_peak_len_dens_histo(\
			# 			fig_data_save, analysis_name, dir_split_file_list, \
			# 			split_group_path, sample_type+'_split', \
			# 			ylimits=ylimits, xlimits=xlimits, \
			# 			dens_index=dens_index, vert_line=vert_line, 
			# 			max_x_ratio=max_x_ratio, 
			# 			max_y_ratio=max_y_ratio)

def common_percent_subplot(fig_data_save, analysis_name, file_dict, \
	plot_dir, fuse_outputs, cutoff_percents=list(range(0,125,25))):
	save_path = os.path.join(plot_dir, 'cutoff_percent_types.png')
	list_by_dir = []

	# setting up type dict: a flat version of the multi-layer file dict
		# that is passed to the function
	type_dict = {}
	for test_group in file_dict.keys():
		if fuse_outputs.get(test_group) is True:
			type_str = '{} all'.format(test_group)
			type_dict[type_str] = []
			for sample_type in file_dict.get(test_group).keys():
				type_dict[type_str]\
					.extend(file_dict.get(test_group).get(sample_type))
		else:
			for sample_type in file_dict.get(test_group).keys():
				type_dict['{} {}'.format(test_group, sample_type)] = \
					file_dict.get(test_group).get(sample_type)

	# keeps count of files for each sample type
	type_file_counts = {}
	for sample_type in type_dict.keys():
		type_file_counts[sample_type] = len(list(type_dict\
			.get(sample_type)))
	# keeps 2d list of number of overlapping peaks in each locus
		# on a cancer type basis
	merged_dict = {}
	for sample_type in type_dict.keys():
		merged_dict[sample_type] = peak_merge_filecount([file[1] for \
			file in type_dict.get(sample_type)], 
			dir_files_or_peaks='file_list', save_location=False,
			special_in_file_handling={3:'avg', 4:'avg'})

		# merged_dict[sample_type] = peak_merge_filecount([file[1] for \
		# 	file in type_dict.get(sample_type)], \
		# 	final_output=None, keep_new_vals=True, \
		# 	dir_files_or_peaks='file_list', save=False)

	# loops through percentages -> 
		# loops through sample types ->
			# dict of the number of peaks that occur in each sample type
				# at a specific perspective
		# dict of each percent-specific type sub-dict
	all_counts = {}
	for perc_loop, percentage in enumerate(cutoff_percents):
		filtered_peaks_by_types = {}
		for sample_type in merged_dict.keys():
			perc_value = \
				round((percentage/100)*type_file_counts[sample_type],1)
			full_len = len(merged_dict.get(sample_type))
			peak_list = \
				[peak for peak in merged_dict.get(sample_type) \
					if int(peak[3]) >= perc_value]
			filtered_len = len(peak_list)
			filtered_peaks_by_types[sample_type] = filtered_len
		all_counts[percentage] = filtered_peaks_by_types

	# sorted list peak counts by percentage -> by sample type
		# outer list = by percentage
			# inner list = by type
		# gets transposed below
	peak_counts_list = []
	percentage_list = sorted([int(val) for val in cutoff_percents])
	type_list = sorted(list(type_dict.keys()))
	for percentage in percentage_list:
		peak_counts_list.append([])
		for sample_type in type_list:
			peak_counts_list[-1]\
				.append(all_counts.get(percentage).get(sample_type))
	# outer list by type, inner list by percentage
	peak_counts_for_plot = np.asarray(peak_counts_list).transpose()
	peak_counts_for_plot = peak_counts_for_plot.tolist()

	percent_common_subplot_dict = {
		'peak_counts_for_plot': peak_counts_for_plot, 
		'cutoff_percents': percentage_list, 
		'sample_types': type_list, 
		'save_path': save_path,
	}
	percent_subplot_dir = os.path.join(fig_data_save, \
		'percent_common_subplot_dict')
	os.makedirs(percent_subplot_dir, exist_ok=True)
	with open(os.path.join(percent_subplot_dir, analysis_name), 'w') as \
		textfile:
		json.dump(percent_common_subplot_dict, textfile)

	common_percent_subplot_just_plotting(percent_common_subplot_dict\
		.get('peak_counts_for_plot'), \
		percent_common_subplot_dict.get('cutoff_percents'), 
		percent_common_subplot_dict.get('sample_types'), \
		percent_common_subplot_dict.get('save_path'))
		
def common_percents_plot(file_dict, plot_dir, fuse_outputs, \
	cutoff_percents=list(range(0,125,25))):
	# set save path + check if file already exists
	save_path = os.path.join(plot_dir, 'cutoff_percent_plot.png')
	if not os.path.isfile(save_path):
		type_file_counts = []

		# setting up type dict
		type_dict = {}
		for test_group in file_dict.keys():
			if fuse_outputs.get(test_group) is True:
				type_dict['{} all'] = []
				for sample_type in file_dict.get(test_group).keys():
					type_dict['{} all'].extend(\
						file_dict.get(test_group).get(sample_type))
			else:
				for sample_type in file_dict.get(test_group).keys():
					type_dict['{} {}'\
						.format(test_group, sample_type)] = \
						file_dict.get(test_group).get(sample_type)
		type_file_counts = {}
		for sample_type in type_dict.keys():
			type_file_counts[sample_type] = \
				len(list(type_dict.get(sample_type)))
		merged_dict = {}
		for sample_type in type_dict.keys():
			merged_dict[sample_type] = peak_merge_filecount([file[1] for \
				file in type_dict.get(sample_type)], 
				dir_files_or_peaks='file_list', save_location=False,
				special_in_file_handling={3:'avg', 4:'avg'})
			# merged_dict[sample_type] = peak_merge_filecount([file[1] 
			# 	for file in type_dict.get(sample_type)], 
			# 	dir_files_or_peaks='file_list', save=False, 
			# 	)
			# merged_dict[sample_type] = peak_merge_filecount([file[1] \
			# 	for file in type_dict.get(sample_type)], \
			# 	final_output=None, keep_new_vals=True, \
			# 	dir_files_or_peaks='file_list', save=False)
		all_counts = {}
		for perc_loop, percentage in enumerate(cutoff_percents):
			filtered_peaks_by_types = {}
			for sample_type in merged_dict.keys():
				perc_value = \
					round((percentage/100)*type_file_counts[sample_type],1)
				full_len = len(merged_dict.get(sample_type))
				peak_list = \
					[peak for peak in merged_dict.get(sample_type) if \
						int(peak[3]) >= perc_value]
				filtered_len = len(peak_list)
				filtered_peaks_by_types[sample_type] = peak_list
			all_counts[percentage] = filtered_peaks_by_types
		
		final_merges = {}
		for perc_loop, percentage in enumerate(cutoff_percents):
			final_merges[percentage] = {}
			peak_list = list(all_counts.get(percentage).values())
			merged_list = peak_merge_filecount(peak_list, 
				dir_files_or_peaks='peak_list', save_location=False,
				special_in_file_handling={3:'avg', 4:'avg'})
			# merged_list = peak_merge_filecount(peak_list, \
			# 	final_output=None, keep_new_vals=True, 
			# 	dir_files_or_peaks='peak_list', save=False)
			for inner_perc_loop, inner_perc in enumerate(cutoff_percents):
				perc_value = round((inner_perc/100)*len(peak_list),1)
				filtered_list = [peak[:4]+[peak[-1]] for peak in \
					merged_list if int(peak[3]) >= perc_value]
				final_merges[percentage][inner_perc] = len(filtered_list)

		peak_counts_list = []
		percentage_list = sorted([int(val) for val in cutoff_percents])
		# outer percentage is for within type common percent
		for percentage in percentage_list:
			peak_counts_list.append([])
			# inner percentage is for between type common percent
			for inner_perc in percentage_list:
				peak_counts_list[-1].append(final_merges\
					.get(percentage).get(inner_perc))
		# this means that each row will be a different within type 
			# common percentage, so we need to transpose the 2d list
		for_plot_peak_list = np.asarray(peak_counts_list).transpose()


		norm = mpl.colors.LogNorm(for_plot_peak_list.min()+1, \
			for_plot_peak_list.max()+1)
		fig = plt.figure(figsize=(10, 3))
		colours = plt.cm.RdYlGn(norm(for_plot_peak_list+1))
		plt.table(
			cellText = np.asarray(for_plot_peak_list),
			rowLabels = [str(thing)+r'%' for thing in cutoff_percents],
			colLabels = [str(thing)+r'%' for thing in cutoff_percents],
			loc = 'center',
			cellColours=colours
			)
		plt.tick_params(axis='x', which='both', bottom=False, \
			top=False, labelbottom=False)
		plt.tick_params(axis='y', which='both', right=False, \
			left=False, labelleft=False)
		x_label = 'Within Sample Type Percent Common'
		y_label = 'Between Sample Type Percent Common'
		plt.text(0.5,0.98,x_label, verticalalignment='center', \
			horizontalalignment='center')
		plt.text(-0.08,0.5,y_label, verticalalignment='center', \
			horizontalalignment='center', rotation=90)
		for pos in ['right','top','bottom','left']:
			plt.gca().spines[pos].set_visible(False)
		plt.savefig(save_path)
		plt.close()

def common_percent_subplot_just_plotting(peak_counts_for_plot, \
	cutoff_percents, sample_types, save_path):
	# outer list by type, inner list by percentage
	fig = plt.figure(figsize=(10, 3))
	norm = mpl.colors.LogNorm(min([min(thing) for thing in \
		peak_counts_for_plot])+1, max([max(thing) for thing in \
			peak_counts_for_plot])+1)
	colours = plt.cm.RdYlGn(norm([[thing+1 for thing in \
		peak_counts_for_plot[plot_loop]] for plot_loop in \
			range(len(peak_counts_for_plot))]))
	plt.table(
		cellText = np.asarray(peak_counts_for_plot),
		colLabels = [str(thing)+r'%' for thing in cutoff_percents],
		rowLabels = [str(thing) for thing in sample_types],
		loc = 'center',
		cellColours=colours
		)
	plt.tick_params(axis='y', which='both', right=False, \
		left=False, labelleft=False)
	x_label = 'Samplewise Common Percent Cutoff'
	plt.text(0.5,0.98,x_label, verticalalignment='center', \
		horizontalalignment='center')
	plt.axis('off')
	plt.savefig(save_path)
	plt.close()

def generate_required_peaks_count(file_dict, split_type=None, file_percent=50):
	common_counts = {}

	for test_group in file_dict.keys():
		common_counts[test_group] = {}
		for sample_type in file_dict.get(test_group).keys():
			file_list = [sample[1] for sample in \
				file_dict.get(test_group).get(sample_type)[0]]
			if isinstance(file_percent, dict):
				required_peak_count = \
					int(math.ceil(len(file_list) * \
						float(file_percent.get(test_group))/100))
			else:
				required_peak_count = \
					int(math.ceil(len(file_list) * \
						float(file_percent)/100))

			common_counts[test_group][sample_type] = \
				str(int(math.ceil(required_peak_count)))+'/'+\
					str(len(file_list))

	return common_counts

def common_bed_mp_wrapper(file_list, save_path, required_peak_count, 
	merge_col_dict=None):
	peak_list_with_counts = \
		peak_merge_filecount(file_list, keep_all_cols=True, 
			special_in_file_handling=merge_col_dict)

	final_peak_list = []
	add_cols = [col+1 for col in sorted(list(merge_col_dict.keys()))]
	for peak in peak_list_with_counts:
		if int(peak[3]) >= required_peak_count:
			add_peak = peak[:3]
			for col in add_cols:
				add_peak.append(peak[col])
			final_peak_list.append(add_peak)

	# final_peak_list = \
	# 	[peak[:3] + [peak[4]] + [peak[5]] for peak in \
	# 		peak_list_with_counts if int(peak[3]) >= \
	# 			required_peak_count]

	write_bed(save_path, final_peak_list)

def make_common_beds(file_dict, split_type=None, file_percent=50, 
	mp_workers=None, merge_col_dict=None):
	for test_group in file_dict.keys():

		for sample_type in file_dict.get(test_group).keys():
			file_list = [sample[1] for sample in \
				file_dict.get(test_group).get(sample_type)[0]]
			if isinstance(file_percent, dict):
				required_peak_count = \
					int(math.ceil(len(file_list) * \
						float(file_percent.get(test_group))/100))
			else:
				required_peak_count = \
					int(math.ceil(len(file_list) * \
						float(file_percent)/100))
			save_path = file_dict.get(test_group).get(sample_type)[1]


			if not os.path.isfile(save_path):
				if mp_workers is None:
					common_bed_mp_wrapper(file_list, save_path, 
						required_peak_count, merge_col_dict)
				else:
					mp_workers.add_job(common_bed_mp_wrapper, 
						(file_list, save_path, required_peak_count, 
							merge_col_dict), 
						job_group='common_beds')

def old_make_common_beds(file_dict, split_type=None, file_percent=50, \
	will_make_common_barplot=False):
	for test_group in file_dict.keys():
		for sample_type in file_dict.get(test_group).keys():
			file_list = [sample[1] for sample in \
				file_dict.get(test_group).get(sample_type)[0]]
			if isinstance(file_percent, dict):
				required_peak_count = \
					int(math.ceil(len(file_list) * \
						float(file_percent.get(test_group))/100))
			else:
				required_peak_count = \
					int(math.ceil(len(file_list) * \
						float(file_percent)/100))
			save_path = file_dict.get(test_group).get(sample_type)[1]

			peak_list_with_counts = \
				peak_merge_filecount(file_list, keep_new_vals=True)

			final_peak_list = \
				[peak[:3] + [peak[5]] + [peak[7]] for peak in \
					peak_list_with_counts if int(peak[3]) >= \
						required_peak_count]

			write_bed(save_path, final_peak_list)

def make_all_common_beds(file_dict, \
	test_groups_dict={'test': 'test', 'control': 'control'}, \
	file_percent={'percent': 50}):

	test_groups = []
	if file_percent.get('remove_control') is not True:
		test_groups.append(test_groups_dict.get('control'))
	if file_percent.get('remove_test') is not True:
		test_groups.append(test_groups_dict.get('test'))

	file_list = []
	for test_group in test_groups:
		for sample_type in file_dict.get(test_group).keys():
			file_list.append(list(file_dict.get(test_group)\
				.get(sample_type))[0])

	if len(file_list) < 1:
		raise ValueError(\
			'No files found for making common peaks. remove_test is {}, and remove_control is {}'\
				.format(file_percent.get('test'), \
					file_percent.get('control')))
	if not all([os.path.isfile(this_file) for this_file in file_list]):
		raise ValueError(\
			'Not all items in the file_list are real files: {}'\
				.format('\n\t'+'\n\t'.join([str(filename) for \
					filename in file_list if not \
					os.path.isfile(filename)])))

	required_peak_count = \
		int(math.ceil(len(file_list) * \
			float(file_percent.get('percent'))/100))
	peak_list_with_counts = peak_merge_filecount(file_list, keep_all_cols=True)
		# special_in_file_handling={3:'avg', 4:'avg'})
	# print(quick_dumps(peak_list_with_counts[:5]))
	# raise
	final_peak_list = \
		[peak[:3] + [peak[4]] + [peak[5]] for peak in \
			peak_list_with_counts if int(peak[3]) >= required_peak_count]
	write_bed(file_dict.get('all').get('all')[0], final_peak_list)

def match_tss_gene_names(bed_10kb_tss, input_dict, output_dict, \
	keep_indices=[0,3]):
	gene_name_file = []
	with open(bed_10kb_tss, 'r') as f:
		for line in f:
			gene_name_file.append(line.strip().split('\t'))

	file_list = []
	for test_group in input_dict.keys():
		for sample_group in input_dict.get(test_group).keys():
			if input_dict.get(test_group).get(sample_group) is not None and \
				output_dict.get(test_group).get(sample_group) is not None:
				file_list.append([])
				for sample in input_dict.get(test_group).get(sample_group):
					file_list[-1].append(sample)
				for sample in output_dict.get(test_group).get(sample_group):
					file_list[-1].append(sample)

	for file_set in file_list:
		if not os.path.isfile(file_set[1]) or not \
			os.path.isfile(file_set[2]):
			peak_list = []
			with open(file_set[0], 'r') as f:
				for line in f:
					peak_list.append(line.strip().split('\t'))

			output_list = []
			window_index = 0
			for line in peak_list:
				if len(line) > 1:
					window_index, output_list, finished_list = \
						check_overlap_sliding_window(window_index, \
							line, gene_name_file, output_list, \
							chr_index=0, 
						peak_min_index=1, peak_max_index=2)
				if finished_list is True:
					break

			intersect_tss_list = \
				[line[keep_indices[0]:keep_indices[1]]+[line[-2]]+\
					[line[-1]] for line in output_list]
			genes = [line[-1] for line in output_list]

			write_bed(file_set[1], intersect_tss_list)
			genes = np.asarray(genes)
			np.savetxt(file_set[2], genes, fmt='%s')

def bar_plot(file_dict, plot_dir, title_pattern, \
	useful_file_dir, mapping_function, test_group_name, control_group_name):
	def bar_plot_plotting(group_name, file_list, plot_dir, width = 0.5, \
		interval = 2, fig_width=23, fig_height=10, bar_color='r', ylim_cushion=1):
		peak_count = []
		bar_plot_dir = os.path.join(plot_dir,'bar_plots')
		for filepath in file_list:
			peak_count.append([filepath[0], 0])
			with open(filepath[1], 'r') as f:
				for line in f:
					line1 = line.strip().split('\t')
					if len(line1) > 1:
						peak_count[-1][-1] += 1
		file_names = [x for x,_ in sorted(peak_count)]
		peak_count = [x for _,x in sorted(peak_count)]
		legend_elements = []
		legend_colors = ['r']
		legend_vals = [title_pattern+' Peaks']
		for legend_loop in range(len(legend_vals)):
			legend_elements.append(Line2D([0], [0], marker='s', \
				color='w', label=str(legend_vals[legend_loop]), \
				markerfacecolor=legend_colors[legend_loop], markersize=9))
		save_spot = os.path.join(bar_plot_dir, group_name+'.png')
		if not os.path.isfile(save_spot):
			fig, ax = plt.subplots(figsize=(fig_width, fig_height))
			x_axis = []
			for peak_loop in range(len(peak_count)):
				x_axis.append(peak_loop*interval*width)
				x_axis.append((peak_loop*interval*width)+(width*2))
			y_vals = []
			for peak_loop in range(len(peak_count)):
				y_vals.append(peak_count[peak_loop])
				y_vals.append(0)
			barlist = ax.barh(x_axis, y_vals, width)
			for peak_loop in range(len(peak_count)):
				barlist[peak_loop*interval].set_color(bar_color)
			x_tick_spots = [(x_spot*interval*width) for x_spot in \
				range(len(peak_count))]
			ax.set_yticks(x_tick_spots)
			ax.set_yticklabels(file_names)
			ax.set_ylim(min(x_tick_spots)-ylim_cushion, max(x_tick_spots)+ylim_cushion)
			title_words = filename_to_plot_name(group_name)
			ax.set_title(title_words)
			ax.set_xlabel('Number of '+title_pattern+' Peaks per Sample')	
			ax.set_ylabel('Sample I.D.')
			if any([len(filename) > 7 for filename in file_names]):
				fig.subplots_adjust(left=0.2)
				plt.legend(handles=legend_elements, \
					bbox_to_anchor=(0.89, 0.98), loc=2, borderaxespad=0)	
			else:
				plt.legend(handles=legend_elements, \
					bbox_to_anchor=(0.90, 0.98), loc=2, borderaxespad=0)	
			autolabel_num(ax, barlist, len(peak_count), interval)
			os.makedirs(bar_plot_dir, exist_ok=True)
			plt.savefig(save_spot)
			plt.close()

	file_list = []
	all_plot = False
	for test_group in file_dict.keys():
		for sample_type in file_dict.get(test_group).keys():
			if len(list(file_dict.get(test_group).get(sample_type))) > 1:
				group_name = '{}_{}'.format(test_group, sample_type)
				file_list = list(file_dict.get(test_group).get(sample_type))
				for sample_loop, sample in enumerate(file_list):
					if test_group == test_group_name:
						file_list[sample_loop][0] = mapping_function(sample[0], 'test')				
					elif test_group == control_group_name:
						file_list[sample_loop][0] = mapping_function(sample[0], 'control')
					else:
						raise 
				bar_plot_plotting(group_name, file_list, plot_dir)
			else:
				all_plot = True
				file_list.append(['{} {}'.format(test_group, sample_type), \
					list(file_dict.get(test_group).get(sample_type)[0])])
	if all_plot is True:
		bar_plot_plotting('all_peaks', file_list, plot_dir)

def bar_plot_common(fig_data_save, file_dict, plot_dir, title_pattern, \
	analysis_name, common_counts, log10=False, width = 0.5):
	if log10 == False:
		save_spot = os.path.join(plot_dir,'bar_plot_common.png')
	elif log10 == True:
		save_spot = os.path.join(plot_dir,'bar_plot_common_log10.png')
	if not os.path.isfile(save_spot):
		num_peaks = []
		cancer_type_peaks = []
		for test_group in file_dict.keys():
			for sample_type in file_dict.get(test_group).keys():
				filepath = list(file_dict.get(test_group).get(sample_type))[0]
				if common_counts.get(test_group) is not None:
					if common_counts.get(test_group).get(sample_type) is not None:
						cancer_type_peaks.append(['{} {}'\
							.format(test_group, sample_type), 0, \
							common_counts.get(test_group).get(sample_type)])
						with open(filepath, 'r') as f:
							for line in f:
								line1 = line.strip().split('\t')
								if len(line1) > 1:
									cancer_type_peaks[-1][1] += 1
		cancer_type_peaks = sorted(cancer_type_peaks, key=operator.itemgetter(0))
		if log10 == False:
			num_peaks = [these_peaks[1] for \
				these_peaks in cancer_type_peaks]
		if log10 == True:
			num_peaks = [math.log10(these_peaks[1]+1) for \
				these_peaks in cancer_type_peaks]
		if log10 == False:
			array_names = ['Common '+title_pattern+' Peak Counts']
		elif log10 == True:
			array_names = ['log10 Common '+title_pattern+' Peak Counts']
		x_axis = np.arange(len(cancer_type_peaks)).tolist()
		common_barplot_dict = {
			'array_names': array_names, 
			'x_axis': x_axis, 
			'peak_array': num_peaks, 
			'width': width, 
			'cancer_types': [these_peaks[0] for \
								these_peaks in cancer_type_peaks], 
			'save_spot': save_spot,
			'common_counts': [these_peaks[2] for \
								these_peaks in cancer_type_peaks],
		}
		if log10 == False:
			common_barplot_dir = os.path.join(fig_data_save, \
				'common_barplot_dict')
			os.makedirs(common_barplot_dir, exist_ok=True)
			with open(os.path.join(common_barplot_dir, analysis_name), \
				'w') as textfile:
				json.dump(common_barplot_dict, textfile)

		fig, ax = plt.subplots(1,1, figsize=(11.5*len(array_names), 10))
		plot_common_barplot(ax, 
			common_barplot_dict.get('array_names'), 
			common_barplot_dict.get('x_axis'), 
			common_barplot_dict.get('peak_array'), 
			common_barplot_dict.get('width'), 
			common_barplot_dict.get('cancer_types'), 
			common_barplot_dict.get('save_spot'), 
			common_barplot_dict.get('common_counts'), fig=fig)
		plt.savefig(save_spot)
		plt.close()

def plot_common_barplot(ax, array_names, x_axis, peak_array, width, \
	cancer_types, save_spot, common_counts, rotation=-45, rot_bottom=0.17, 
	no_title=False, fig=None, ylabel='Number of Peaks', ylabelpad=2,
	ylabel_size=5, ylim=None, ymulti=1.2):
	# fig, ax = plt.subplots(1,1, figsize=(11.5*len(array_names), 10))
	for e in range(len(array_names)):
		barlist = ax.bar(x_axis, peak_array, width)
		ax.set_xticks(x_axis)
		ax.set_ylabel(ylabel, fontsize=ylabel_size, labelpad=ylabelpad)
		autolabel_common(ax, barlist, common_counts)
		if rotation == 0:
			ha='center'
		else:
			ha='left'
			if fig is not None:
				fig.subplots_adjust(bottom=rot_bottom)
		if ylim is None:
			ylim = (0, max([thing.get_height() for thing in barlist])*ymulti)
		ax.set_ylim(*ylim)
		ax.set_xticklabels(cancer_types, rotation=rotation, ha=ha)
		if no_title is False:
			ax.set_title(str(array_names[e]))
	# plt.savefig(save_spot)
	# plt.close()

def bar_plot_intersect_percent(file_dict, plot_dir, title_pattern):
	save_spot = os.path.join(plot_dir, 'bar_plot_intersect_percent.png')
	if not os.path.isfile(save_spot):
		num_peaks = []
		peaks_unique = []
		cancer_types_peaks = []
		for test_group in file_dict.keys():
			for sample_type in file_dict.get(test_group).keys():
				filepath = list(file_dict.get(test_group).get(sample_type))[0]
				unique_filepath = list(file_dict.get(test_group).get(sample_type))[1]
				cancer_types_peaks.append('{} {}'.format(test_group, sample_type))
				num_peaks.append(0)
				with open(filepath, 'r') as f:
					for line in f:
						line1 = line.strip().split('\t')
						if len(line1) > 1:
							num_peaks[-1] += 1

				peaks_unique.append(0)
				prev_spot = [0, 0]
				with open(unique_filepath, 'r') as f:
					for line in f:
						line1 = line.strip().split('\t')
						if len(line1) > 1:
							if (prev_spot[0] != line1[0]) or (prev_spot[1] != line1[1]):
								peaks_unique[-1] += 1
						prev_spot = line1[:2]
		percent_kept_peaks = [round((peaks_unique[x]/num_peaks[x])*100,3) if \
			num_peaks[x]>0 else -1 for x in range(len(num_peaks))]
		all_data_list = sorted(list(zip(cancer_types_peaks, percent_kept_peaks)))
		percent_kept_peaks 	= 	[peak[1] for peak in all_data_list]
		cancer_types 		= 	[peak[0] for peak in all_data_list]
		peak_array = [percent_kept_peaks]
		array_names = [title_pattern+' Peaks Percent within 10kb of TSSs']
		x_axis = np.arange(len(percent_kept_peaks))
		width = 0.5
		fig, ax = plt.subplots(1,1, figsize=(11.5*len(array_names), 10))
		for e in range(len(array_names)):
			barlist = ax.bar(x_axis, peak_array[e], width)
			ax.set_ylim(int((min(peak_array[e])-10)/5)*5,105)
			ax.set_xticks(x_axis)
			ax.set_ylabel('Percent of Peaks', fontsize=13, labelpad=10)
			autolabel_common(ax, barlist)
			ax.set_xticklabels(cancer_types)
			ax.set_title(str(array_names[e]))
		plt.savefig(save_spot)
		plt.close()
	
def broad_sharp_cancer_gene_preproc(analysis_dict, other_dict, relevant_list, \
	subgroup_dict, subgroup_order):
	# grabbing sample type list
	sample_types = sorted(list(analysis_dict.get(list(analysis_dict.keys())[0]).keys()))
	# grabbing analysis group file list
	analysis_common = [analysis_dict.get(list(analysis_dict.keys())[0]).get(this_sample)[0] \
		for this_sample in sample_types]
	# grabbing other group file list
	other_common = [other_dict.get(list(analysis_dict.keys())[0]).get(this_sample)[0] \
		for this_sample in sample_types]

	peak_list_to_plot = []
	for type_loop in range(len(sample_types)):
		# looping through files and grabbing gene lists from 
			# analysis/other file for each type
		analysis_genes = \
			open_and_read(get_item_including_string(analysis_common, 
				sample_types[type_loop]))
		other_genes = \
			open_and_read(get_item_including_string(other_common, 
				sample_types[type_loop]))
		analysis_genes = sorted(list(set([gene.lower() for gene in analysis_genes])))
		other_genes = sorted(list(set([gene.lower() for gene in other_genes])))
		
		analysis_subgroup_list, gene_types = \
			list_to_subgroups(analysis_genes, relevant_list, subgroup_dict, 
				subgroup_order)
		other_subgroup_list, gene_types = \
			list_to_subgroups(other_genes, relevant_list, subgroup_dict, 
				subgroup_order)

		analysis_first_len = len(analysis_subgroup_list[-1])
		analysis_all_lens = [len(thing) for thing in analysis_subgroup_list]
		analysis_sum_lens = sum([len(thing) for thing in analysis_subgroup_list])
		for subgroup in analysis_subgroup_list[:-1]:
			analysis_subgroup_list[-1].extend(subgroup)
		for subgroup in other_subgroup_list[:-1]:
			other_subgroup_list[-1].extend(subgroup)
		analysis_second_len = len(analysis_subgroup_list[-1])
		
		peak_list_to_plot.append(analysis_subgroup_list)
		peak_list_to_plot.append(other_subgroup_list)

	return peak_list_to_plot, sample_types

def cancer_gene_barplots_preproc(analysis_dicts, relevant_list, \
	subgroup_dict, subgroup_order):
	# grabbing sample type list
	sample_types = sorted(list(analysis_dicts[0].get(list(analysis_dicts[0].keys())[0]).keys()))
	# grabbing analysis group file list
	common_list = [[this_dict.get(list(this_dict.keys())[0]).get(this_sample)[0] \
		for this_sample in sample_types] for this_dict in analysis_dicts]
	# analysis_common = [analysis_dict.get(list(analysis_dict.keys())[0]).get(this_sample)[0] \
	# 	for this_sample in sample_types]
	# grabbing other group file list
	# other_common = [other_dict.get(list(analysis_dict.keys())[0]).get(this_sample)[0] \
	# 	for this_sample in sample_types]

	peak_list_to_plot = []
	for type_loop in range(len(sample_types)):
		for this_common in common_list:
			# looping through files and grabbing gene lists from 
				# analysis/other file for each type
			analysis_genes = \
				open_and_read(get_item_including_string(this_common, 
					sample_types[type_loop]))
			# analysis_genes = \
			# 	open_and_read(get_item_including_string(analysis_common, 
			# 		sample_types[type_loop]))
			# other_genes = \
			# 	open_and_read(get_item_including_string(other_common, 
			# 		sample_types[type_loop]))
			analysis_genes = sorted(list(set([gene.lower() for gene in analysis_genes])))
			# analysis_genes = sorted(list(set([gene.lower() for gene in analysis_genes])))
			# other_genes = sorted(list(set([gene.lower() for gene in other_genes])))
			
			# print('sample type: {}, this_common: {}'.format(sample_types[type_loop], this_common[0].split('/')[-4]))
			analysis_subgroup_list, gene_types = \
				list_to_subgroups(analysis_genes, relevant_list, subgroup_dict, 
					subgroup_order)
			# analysis_subgroup_list, gene_types = \
			# 	list_to_subgroups(analysis_genes, relevant_list, subgroup_dict, 
			# 		subgroup_order)
			# other_subgroup_list, gene_types = \
			# 	list_to_subgroups(other_genes, relevant_list, subgroup_dict, 
			# 		subgroup_order)

			# analysis_first_len = len(analysis_subgroup_list[-1])
			# analysis_all_lens = [len(thing) for thing in analysis_subgroup_list]
			# analysis_sum_lens = sum([len(thing) for thing in analysis_subgroup_list])
			for subgroup in analysis_subgroup_list[:-1]:
				analysis_subgroup_list[-1].extend(subgroup)
			# for subgroup in other_subgroup_list[:-1]:
			# 	other_subgroup_list[-1].extend(subgroup)
			# analysis_second_len = len(analysis_subgroup_list[-1])

			peak_list_to_plot.append(analysis_subgroup_list)
		
		# peak_list_to_plot.append(analysis_subgroup_list)
		# peak_list_to_plot.append(other_subgroup_list)
	# raise

	return peak_list_to_plot, sample_types

def join_lists_on_indices(input_list, list_of_indices_list):
	output_list = [[] for thing in range(len(list_of_indices_list)+1)]
	for list_loop in range(len(list_of_indices_list)):
		for index_loop in range(len(list_of_indices_list[list_loop])):
			for thing in input_list[list_of_indices_list[list_loop][index_loop]]:
				output_list[list_loop].append(thing)
	output_list[-1] = input_list[-1]
	return output_list

def list_to_subgroups(input_list, reference_list, subgroup_dict, subgroup_order, 
	reference_types=None):
	if reference_types==None:
		reference_types = sorted(list(set([thing[1] for thing in reference_list])))
		gene_group_dict = {sub_type:set() for sub_type in list(set(subgroup_dict.values()))}
		gene_group_dict['none'] = set()
		reference_dict = {}
		for line in reference_list:
			if reference_dict.get(line[0].lower()) is None:
				reference_dict[line[0].lower()] = set((line[1],))
			else:
				reference_dict[line[0].lower()].add(line[1])
	# print(len(input_list))
	for this_gene in input_list:
		gene_type_list = reference_dict.get(this_gene.lower())
		if gene_type_list is not None:
			for gene_type in gene_type_list:
				final_type = subgroup_dict.get(gene_type)
				if final_type is None:
					raise ValueError('Got gene subtype that wasn\'t included ')
				gene_group_dict[final_type].add(this_gene)
		else:
			gene_group_dict['none'].add(this_gene)	

	gene_group_list = [sorted(list(gene_group_dict.get(this_sub))) for this_sub in subgroup_order]

	relevant_dict = {}
	for line in reference_list:
		# line = line.strip('\n').split('\t')
		if len(line) > 2:
			raise ValueError('this line: {}'.format(line))
		if len(line) > 1:
			relevant_dict[line[0].lower()] = line[1]
		else:
			print('line: {}'.format(line))
	# group_names = ['TSG', 'Oncogene', 'Neither', None]
	# for mini_bar_loop, mini_bar in enumerate(gene_group_list):
		# correct = 0
		# incorrect = 0
		# # inc_list = []
		# corr_list = []
		# group_dict = {'TSG': 0, 'Oncogene': 0, 'Neither': 0, None: 0}
		# group_dict_lists = {'TSG': [], 'Oncogene': [], 'Neither': [], None: []}
		# for gene_loop, gene in enumerate(mini_bar):
		# 	gene_tuple = (gene, relevant_dict.get(gene), subgroup_dict.get(relevant_dict.get(gene)))
		# 	# try:
		# 	# print(group_names[mini_bar_loop], subgroup_dict.get(relevant_dict.get(gene)))
		# 	# raise
		# 	if group_names[mini_bar_loop] == subgroup_dict.get(relevant_dict.get(gene)):
		# 		correct += 1
		# 		corr_list.append(gene_tuple)
		# 	else:
		# 		incorrect += 1
		# 		group_dict[subgroup_dict.get(relevant_dict.get(gene))] += 1
		# 		group_dict_lists[subgroup_dict.get(relevant_dict.get(gene))].append(gene_tuple)
		# 		# inc_list.append((gene, relevant_dict.get(gene), subgroup_dict.get(relevant_dict.get(gene)), group_names[mini_bar_loop]))
		# 	# except:


		# 	# print('name: {}\n\ttype: {}\n\ttype_set: {}\n\texpected_type: {}\n\ttype right: {}'.format(gene, relevant_dict.get(gene), subgroup_dict.get(relevant_dict.get(gene)), group_names[mini_bar_loop],
		# 	# 	group_names[mini_bar_loop] in subgroup_dict.get(relevant_dict.get(gene))))
		# 	# if gene_loop > 5:
		# 	# 	raise
		# # print('\t\tcorrect count: {}, incorrect count: {}\n\t\tgenes: {}'.format(correct, incorrect, inc_list))
		# if incorrect > 0:
		# 	print('\tmini-bar type: {}'.format(group_names[mini_bar_loop]))
		# 	# print('\t\tcorrect count: {}, incorrect count: {}\n\t\tgenes: {}'.format(correct, incorrect, group_dict))
		# 	print('\t\tcorrect count: {}, incorrect count: {}\n\t\tgenes: {}'.format(corr_list[:20], incorrect, 
		# 		['{}: {}'.format(key, group_dict_lists.get(key)[:20]) for key in group_dict_lists]))
		# 	raise
		# else:
		# 	print('\tmini-bar type: {}, len: {}'.format(group_names[mini_bar_loop], len(mini_bar)))
		# 	print('\t\tcorrect count: {}, incorrect count: {}\n\t\tgenes: {}'.format(correct, incorrect, group_dict))


	# print(len(gene_group_list))
	# print([len(thing) for thing in gene_group_list])


	# print([[len(stuff) for stuff in thing] for thing in gene_group_list])
	# raise

	return gene_group_list, reference_types

def get_item_including_string(input_list, search_string, is_filepath=True, return_1=True):
	if is_filepath == True:
		outputs = [thing for thing in input_list if search_string in thing.split('/')[-1]]
	else:
		outputs = [thing for thing in input_list if search_string in thing]
	if return_1 == True:
		return outputs[0]
	else:
		return outputs

def cosmic_len_change_gene_lists(peaks_dir, gene_name_file):
	file_list = sorted([thing for thing in glob.iglob(os.path.join(peaks_dir, '*')) if thing.split('/')[-1].split('_')[0] != 'all'])
	list_gene_lists = []
	for filepath in file_list:
		len_peaks = []
		short_peaks = []
		same_peaks = []
		peak_list = open_and_read(filepath, '\t')
		up = 0
		down = 0
		neither = 0
		for peak in peak_list: 
			if int(peak[5]) >= 2000:
				len_peaks.append(peak)
				up += 1
			elif int(peak[5]) <= -2000:
				short_peaks.append(peak)
				down += 1
			else:
				same_peaks.append(peak)
				neither += 1
		len_peaks = [[thing[0]] + [int(thing[1])] + thing[2:] for thing in len_peaks if len(thing) > 1]
		len_peaks = sorted(len_peaks, key=operator.itemgetter(0,1))
		short_peaks = [[thing[0]] + [int(thing[1])] + thing[2:] for thing in short_peaks if len(thing) > 1]
		short_peaks = sorted(short_peaks, key=operator.itemgetter(0,1))
		same_peaks = [[thing[0]] + [int(thing[1])] + thing[2:] for thing in same_peaks if len(thing) > 1]
		same_peaks = sorted(same_peaks, key=operator.itemgetter(0,1))
		group_gene_lists = []
		groups = [len_peaks, short_peaks, same_peaks]
		for_check = []
		for group in groups:
			window_index = 0
			output_list = []
			for line in group:
				if len(line) < 2:
					continue
				window_index, output_list, finished_list = check_overlap_sliding_window(window_index, line, gene_name_file, output_list, chr_index=0, 
					peak_min_index=1, peak_max_index=2)
				if finished_list == True:
					break
			list_gene_lists.append(sorted(list(set([thing[-1] for thing in output_list]))))
	return list_gene_lists, [thing.split('/')[-1].split('_')[0] for thing in file_list]

def len_change_preproc(analysis_dict, other_dict, third_dict, relevant_list, 
	subgroup_dict, subgroup_order):
	# grabbing sample type list
	# test_group = list(analysis_dict.keys())[0]
	sample_types = sorted(list(analysis_dict.get(list(analysis_dict.keys())[0]).keys()))
	# sample_types = sorted(list(analysis_dict.get(test_group).keys()))
	# grabbing analysis group file list
	# grabbing other group file list
	analysis_common = [analysis_dict.get(test_group).get(this_sample)[0] \
		for this_sample in sample_types]
	other_common = [other_dict.get(test_group).get(this_sample)[0] \
		for this_sample in sample_types]
	third_common = [third_dict.get(test_group).get(this_sample)[0] \
		for this_sample in sample_types]

	peak_list_to_plot = []
	for type_loop in range(len(sample_types)):
		# looping through files and grabbing gene lists from 
			# analysis/other file for each type
		analysis_genes = \
			open_and_read(get_item_including_string(analysis_common, 
				sample_types[type_loop]))
		other_genes = \
			open_and_read(get_item_including_string(other_common, 
				sample_types[type_loop]))
		third_genes = \
			open_and_read(get_item_including_string(third_common, 
				sample_types[type_loop]))
		analysis_genes = sorted(list(set([gene.lower() for gene in analysis_genes])))
		other_genes = sorted(list(set([gene.lower() for gene in other_genes])))
		third_genes = sorted(list(set([gene.lower() for gene in third_genes])))
		
		analysis_subgroup_list, gene_types = \
			list_to_subgroups(analysis_genes, relevant_list, subgroup_dict, 
				subgroup_order)
		other_subgroup_list, gene_types = \
			list_to_subgroups(other_genes, relevant_list, subgroup_dict, 
				subgroup_order)
		third_subgroup_list, gene_types = \
			list_to_subgroups(third_genes, relevant_list, subgroup_dict, 
				subgroup_order)

		for subgroup in analysis_subgroup_list[:-1]:
			analysis_subgroup_list[-1].extend(subgroup)
		for subgroup in other_subgroup_list[:-1]:
			other_subgroup_list[-1].extend(subgroup)
		for subgroup in third_subgroup_list[:-1]:
			third_subgroup_list[-1].extend(subgroup)
		
		peak_list_to_plot.append(analysis_subgroup_list)
		peak_list_to_plot.append(other_subgroup_list)
		peak_list_to_plot.append(third_subgroup_list)

	return peak_list_to_plot, sample_types

def old_len_change_preproc(peaks_dir, relevant_list, indices_list, bed_10kb_tss):
	gene_name_file = []
	with open(bed_10kb_tss, 'r', newline='\n') as f:
		for line in f:
			gene_name_file.append(line.strip('\n').split('\t'))
	list_gene_lists, types = cosmic_len_change_gene_lists(peaks_dir, gene_name_file)
	peak_list_to_plot = []
	for gene_list in list_gene_lists:
		subgroup_list, gene_types = list_to_subgroups(gene_list, relevant_list)
		peak_list_to_plot.append(smaller_subgroup_list)
	return peak_list_to_plot, types

def tuson_subgroup_list(which_col, max_val, tsg_spot, og_spot):
	tsg_list = open_and_read(tsg_spot, ',')
	og_list = open_and_read(og_spot, ',')

	tsg_list = pull_entries_by_sig(tsg_list, which_col, max_val)
	og_list = pull_entries_by_sig(og_list, which_col, max_val)

	output_list = []
	for thing in tsg_list:
		output_list.append([thing[0], 'TSG'])
	for thing in og_list:
		output_list.append([thing[0], 'oncogene'])

	return output_list

def pull_entries_by_sig(input_list, which_col, cutoff):
	output_list = []
	for thing in input_list:
		try:
			thing[which_col] = float(thing[which_col])
			if thing[which_col] < cutoff:
				thing[0] = thing[0].strip('\ufeff')
				output_list.append([thing[0], thing[which_col]])
		except:
			pass
	return output_list

def cancer_list_plots(analysis_dict, other_dict, save_dir, \
	len_or_len_change, tuson_or_cosmic, data_for_figures, \
	tuson_tsg=None, tuson_og=None, cosmic_list=None, bed_10kb_tss=None, \
	mp_workers=None, third_dict=None):

	raw_percent_dict_save = os.path.join(data_for_figures, \
			len_or_len_change+'_'+tuson_or_cosmic)
	if tuson_or_cosmic == 'tuson':
		gene_type_count = 2
		which_column = 1
		max_val = 0.01

		if which_column == 1:
			my_letter = 'p'
		else:
			my_letter = 'q'
		my_num = str(max_val).split('.')[-1]
		for_save = '_'+my_letter+my_num

		percent_change_save = os.path.join(save_dir, \
			'tuson_percent_change_plot'+for_save+'.png')
		raw_percent_save = os.path.join(save_dir, \
			'tuson_raw_percent_plot'+for_save+'.png')
		if not os.path.exists(percent_change_save) or \
			not os.path.exists(raw_percent_save) or \
			not os.path.exists(raw_percent_dict_save):

			relevant_list = tuson_subgroup_list(which_column, \
				max_val, tsg_spot=tuson_tsg, og_spot=tuson_og)

			gene_types = sorted(list(set([thing[1] for thing in \
				relevant_list])))
			subgroup_dict = {
				'TSG': 'TSG',
				'oncogene': 'Oncogene',
			}
			group_names = ['TSG', 'Oncogene', 'none']
	elif tuson_or_cosmic == 'cosmic':
		gene_type_count = 3
		percent_change_save = os.path.join(save_dir, \
			'cosmic_percent_change_plot.png')
		raw_percent_save = os.path.join(save_dir, \
			'cosmic_raw_percent_plot.png')
		if not os.path.exists(percent_change_save) or \
			not os.path.exists(raw_percent_save) or \
			not os.path.exists(raw_percent_dict_save):
			# grabbing the cosmic gene list
			relevant_list = open_and_read(cosmic_list, '\t')
			# processing cosmic gene list
			relevant_list = \
				[[stuff.strip('"') for stuff in thing] for \
					thing in relevant_list[1:]]
			# grabbing gene categories from cosmic list (col2)
			gene_types = \
				sorted(list(set([thing[1] for thing in relevant_list])))
			subgroup_dict = {
				'TSG': 'TSG',
				'TSG, fusion': 'TSG',
				'oncogene, TSG': 'TSG',
				'oncogene, TSG, fusion': 'TSG',
				'oncogene': 'Oncogene',
				'oncogene, TSG': 'Oncogene',
				'oncogene, TSG, fusion': 'Oncogene',
				'oncogene, fusion': 'Oncogene',
				'': 'Neither',
				'fusion': 'Neither',
			}
			group_names = ['TSG', 'Oncogene', 'Neither', 'none']

	else:
		raise ValueError(\
				'tuson_or_cosmic must be "tuson" or "cosmic", was:', \
					tuson_or_cosmic)

	if not os.path.exists(percent_change_save) or \
		not os.path.exists(raw_percent_save) or \
		not os.path.exists(raw_percent_dict_save):
		if len_or_len_change == 'len':
			if third_dict is not None:
				raise ValueError('Running "len" cancer_list_plots but \
					\n\tsupplied third_dict. Did you want the len_change version?')
			loop_count = 2
			# tsg/oncogene/neither gene group lists
				# analysis/other_dict = dictionaries of filepaths per group
				# relevant_list = cosmic/tuson gene lists
				# index lists for tsg/oncogene/neither for grouping
			peak_list_to_plot, types = \
				cancer_gene_barplots_preproc([analysis_dict, \
						other_dict], relevant_list, subgroup_dict, group_names)
		elif len_or_len_change == 'len_change': 
			if third_dict is None:
				raise ValueError('Running len_change cancer_list_plots but \
					\n\tthird_dict is None. third_dict must be a no_change file_dict')
			loop_count = 3
			peak_list_to_plot, types = \
				cancer_gene_barplots_preproc([analysis_dict, 
						other_dict, third_dict], relevant_list, 
						subgroup_dict, group_names)
		else:
			raise ValueError(\
				'len_or_len_change must be "len" or "len_change", was:', \
					len_or_len_change)


		peak_list_to_plot = [[sorted(list(set([gene.lower() for gene in stuff]))) for \
			stuff in thing] for thing in peak_list_to_plot]


		if 'none' in group_names:
			group_names.remove('none')

		# write_bed('{}_relevant_list.bed'.format(tuson_or_cosmic), relevant_list)
		# write_bed('{}_peak_plot_list.bed'.format(tuson_or_cosmic), peak_list_to_plot)
		# print(types)

		# print(tuson_or_cosmic, group_names, types)
		# print([[len(stuff) for stuff in thing] for thing in peak_list_to_plot])
		# print('')
		# raise
		cancer_barplot_dict = {
			'peak_list_to_plot': peak_list_to_plot, 
			'types': types, 
			'group_names': group_names, 
			'len_or_len_change': len_or_len_change,
			'gene_types': gene_types,
		}
		os.makedirs(data_for_figures, exist_ok=True)
		with open(raw_percent_dict_save, 'w') as textfile:
			json.dump(cancer_barplot_dict, textfile)

		if mp_workers is not None:
			mp_workers.add_job(change_percent_plot, 
				args=(cancer_barplot_dict.get('peak_list_to_plot'), \
				cancer_barplot_dict.get('types'), \
				cancer_barplot_dict.get('group_names'), \
				cancer_barplot_dict.get('len_or_len_change'), \
				percent_change_save), 
				job_group='cancer_barplots')

			mp_workers.add_job(cancer_barplot_mp_wrapper,
				(raw_percent_save, loop_count, gene_type_count, 
					0.2, 0.2, 0.8, 0.8, 1, 1, 0, 
					cancer_barplot_dict, 'len', 300),
				{'deffont': 4, 'x_ax_text_ratio': 0.75, 
					'text_lower_ratio': 0.0, 'y_lim_multi':  1.3, 
					'type_legend': True, 'type_legend_spot': 'left', 
					'legend_labelspacing': 0.25, 
					'bottom_label_extra_down': 1.63, 'label_size_multi': 1.2}, 
				job_group='cancer_barplots')

		else:
			change_percent_plot(cancer_barplot_dict.get('peak_list_to_plot'), \
				cancer_barplot_dict.get('types'), \
				cancer_barplot_dict.get('group_names'), \
				cancer_barplot_dict.get('len_or_len_change'), \
				percent_change_save)

			cancer_barplot_mp_wrapper(raw_percent_save, loop_count, gene_type_count, 
					0.2, 0.2, 0.8, 0.8, 1, 1, 0, 
					cancer_barplot_dict, 'len', 300,
					**{'deffont': 4, 'x_ax_text_ratio': 0.75, 
						'text_lower_ratio': 0.0, 'y_lim_multi':  1.3, 
						'type_legend': True, 'type_legend_spot': 'left', 
						'legend_labelspacing': 0.25, 
						'bottom_label_extra_down': 1.63, 'label_size_multi': 1.2})

def cancer_barplot_mp_wrapper(raw_percent_save, loop_count, gene_type_count,
	leftx, topy, rightx, bottomy, xcount, ycount, subp_perc, 
	cancer_barplot_dict, len_or_dens, dpi, **plot_kwargs):

	sig_list = full_sig_list(cancer_barplot_dict, loop_count, gene_type_count)
	fig = easy_fig(figsize=(8, 8), gridsize=(1000,1000), \
		subplot_left=0.01, subplot_right=0.99, subplot_bottom=0.01, \
		subplot_top=0.99)
	fig.add_panel(top_left=(leftx,topy), bottom_right=(rightx,bottomy), \
		x_count=xcount, y_count=ycount, subpanel_pad_percent=subp_perc)	
	cancer_gene_barplot_just_plot(fig.axes[0][0], \
		cancer_barplot_dict.get('peak_list_to_plot'), \
		cancer_barplot_dict.get('gene_types'), \
		cancer_barplot_dict.get('types'), 
		cancer_barplot_dict.get('group_names'), \
		cancer_barplot_dict.get('len_or_len_change'), \
		len_or_dens, loop_count,
		gene_type_count, sig_list=sig_list, **plot_kwargs)

	plt.savefig(raw_percent_save, dpi=dpi)
	plt.close()

def chisquare_prep(total_counts, loop_count, gene_type_count, val_names=None):
	# sets up gene lengths (should be a mini_list for each bar)
		# each mini_list has an item for each sub-bar in the bar
	# for gene_group in total_counts:
	# 	modified_gene_group = [sub_bar/sum(gene_group) for sub_bar in gene_group]
	
	val_index = 0
	final_prepped_vals = [[[],[]]]

	while True:
		# total_counts = a list of sub-lists that have gene lists for every bar in the figure
			# sub-lists are for sub-bars 
		# this loops through that list and formats the final_prepped_vals which normalizes counts + groups vals by sample type
			# each sub-list in final_prepped_vals is for a sample type
				# sub-list[1] is for the reference column for the sample type
				# sub-list[0] is a list of comparison columns (eg, sub-list[1] is no change, sub-list[0][0] is lengthen,
					# and sub-list[0][1] is shorten)
			# comp-loop is looping through the comparison lists for a sample type
				# the list comprehension takes each sub-bar value and multiplies it by:
					# sum(reference sub-bars) / sum(comparison sub-bars)
		if val_index < len(total_counts):
			if (val_index+1) % loop_count == 0:
				final_prepped_vals[-1][1] = total_counts[val_index]
				final_prepped_vals.append([[],[]])
			else:
				final_prepped_vals[-1][0].append(total_counts[val_index])
			val_index += 1
		else:
			final_prepped_vals = final_prepped_vals[:-1]
			break

	return final_prepped_vals

def full_sig_list(len_change_cosmic_data, loop_count, gene_type_count):
	total_counts = [[len(stuff) for stuff in thing] for thing in \
		len_change_cosmic_data.get('peak_list_to_plot')]
	prepped_list = chisquare_prep(total_counts, loop_count, gene_type_count)
	# print(prepped_list)
	# raise
	flat_pval_list = []
	# check_prop = []
	# pvals = []
	# looping through sample types
	for sample_type in prepped_list:
		# pvals.append([])
		# looping through bars in a type (broad/sharp, len/short/nochange)
		for compar_group in sample_type[0]:
			# pvals[-1].append([])
			# looping through sub-bars in bar (all but last. 
				# eg, ['tsg', 'og'] out of ['tsg', 'og', 'none'])
				# doing chi-square of [compar_bar[sub-bar], compar_bar['none' sub-bar]] 
					# vs [ref_bar[sub-bar], ref_bar['none' sub-bar]]
			# adding them to a "flat" pvalue list for easy p-adjustment
			for gene_group_loop in range(len(compar_group)-1):
				# pvals[-1][-1].append(
				# 	chisquare([compar_group[gene_group_loop], compar_group[-1]], 
				# 		[sample_type[1][gene_group_loop], sample_type[1][-1]]).pvalue)
				flat_pval_list.append(
					proportions_ztest([compar_group[gene_group_loop], sample_type[1][gene_group_loop]], 
						[compar_group[-1], sample_type[1][-1]])[1])
					# proportions_ztest([compar_group[gene_group_loop], compar_group[-1]], 
					# 	[sample_type[1][gene_group_loop], sample_type[1][-1]])[1])
				# check_prop.append([round(compar_group[gene_group_loop]/compar_group[-1], 4), 
				# 	round(sample_type[1][gene_group_loop]/sample_type[1][-1], 4)])
				# print(chisquare([compar_group[gene_group_loop], compar_group[-1]], 
				# 	[sample_type[1][gene_group_loop], sample_type[1][-1]]).pvalue,
				# proportions_ztest([compar_group[gene_group_loop], 
				# 	sample_type[1][gene_group_loop]], [compar_group[-1], sample_type[1][-1]]))
	flat_qval_list = statsr.p_adjust(FloatVector(flat_pval_list), method = 'BH')
	# for list_ind, mini_list in enumerate(prepped_list):
	# 	ind1 = list_ind * 2
	# 	ind2 = ind1 + 1
	# 	print(mini_list, 
	# 		'\n\t', (round(flat_pval_list[ind1],4), round(flat_qval_list[ind1],4), check_prop[ind1]),
	# 		'\n\t', (round(flat_pval_list[ind2],4), round(flat_qval_list[ind2],4), check_prop[ind2]))
	# raise
	# looping through the flat qvalue list to convert it to 
		# the same format ad the prepped_list
	qvals = []
	sig_list = []
	which_qval = 0
	for sample_type in prepped_list:
		qvals.append([])
		sig_list.append([])
		for compar_group in sample_type[0]:
			qvals[-1].append([])
			sig_list[-1].append([])
			for gene_group_loop in range(len(compar_group)-1):
				qvals[-1][-1].append(flat_qval_list[which_qval])
				if flat_qval_list[which_qval] < 0.05:
					sig_list[-1][-1].append('sig')
				else:
					sig_list[-1][-1].append('')
				which_qval += 1
	
	# print(sig_list)
	return sig_list

def change_percent_plot(peak_list_to_plot, types, group_names, len_or_len_change, save_spot):
	deffont = 8
	fig, ax = plt.subplots(figsize=(deffont*1.5, deffont*1.0))
	plt.subplots_adjust(bottom=0.3)
	
	new_barplot_list = []
	barplot_text = []
	color_list = []
	my_colors = ['C0','C6','C7']
	gene_groups = int(len(peak_list_to_plot)/len(types))
	x_count = 0
	for this_loop in range(len(peak_list_to_plot)):
		if this_loop % gene_groups == 0:
			this_subloop_info = [[] for thing in range(3)]
			for subloop in range(len(peak_list_to_plot[this_loop]) - 1):
				numerator = []
				for group_loop in range(gene_groups-1):
					if len(peak_list_to_plot[this_loop+group_loop][-1]) > 0:
						numerator.append(len(peak_list_to_plot[this_loop+group_loop][subloop])/len(peak_list_to_plot[this_loop+group_loop][-1]))
					else:
						numerator.append(0)
				denominator = len(peak_list_to_plot[this_loop+(gene_groups-1)][subloop])/len(peak_list_to_plot[this_loop+(gene_groups-1)][-1])
				for this_num in numerator:
					if denominator > 0:
						this_subloop_info[0].append((this_num/denominator)*100-100)
					else:
						this_subloop_info[0].append(1000)
					x_count += 1
					this_subloop_info[1].append(str(round(float(this_num*100),2))+'/'+str(round(float(denominator*100),2)))
					this_subloop_info[2].append(my_colors[subloop])
			for this_loop in range((int(len(this_subloop_info[0])/(gene_groups - 1)))):
				for inner_loop in range(len(this_subloop_info[0])):
					if inner_loop % (gene_groups - 1) == this_loop:
						new_barplot_list.append(this_subloop_info[0][inner_loop])
						barplot_text.append(this_subloop_info[1][inner_loop])
						color_list.append(this_subloop_info[2][inner_loop])
			barplot_text.append('')
			color_list.append('b')
			new_barplot_list.append(0)
			x_count += 1
	x_label_num = []
	x_label_name = []
	if len_or_len_change == 'len':
		num_per_type = (((gene_groups-1)*(len(peak_list_to_plot[0])-1))+1)
		for thing in range(x_count):
			if thing % (num_per_type) == 0:
				x_label_num.append(thing+((num_per_type-2)/2))
				x_label_name.append(str(types[int(thing/num_per_type)])+' Broad')
	else:
		num_per_type = (((gene_groups-1)*(len(peak_list_to_plot[0])-1))+1)
		minigroup_num = int((num_per_type-2)/2)
		for thing in range(x_count):
			if thing % (num_per_type) == 0:
				x_label_num.append(thing+((minigroup_num)*0.5))
				x_label_num.append(thing+((minigroup_num)*1.5+1))
				x_label_name.append(str(types[int(thing/num_per_type)])+' Lenthened')
				x_label_name.append(str(types[int(thing/num_per_type)])+' Shortened')
	legend_elements = []
	top_bar = max(new_barplot_list)
	for legend_loop in range(len(group_names)):
		if group_names[legend_loop] == '': group_names[legend_loop] = 'None'
		legend_elements.append(Line2D([0], [0], marker='o', color='w', label=group_names[legend_loop], markerfacecolor=my_colors[legend_loop], markersize=deffont*1.2))
	ax.legend(handles=legend_elements, bbox_to_anchor=(1-(deffont*0.0015), 1-(deffont*0.0015)), loc=1, borderaxespad=0, prop={'size': deffont*1.1, 'weight': 'semibold'})
	ax.bar(range(x_count), new_barplot_list, color=color_list)
	ax.axhline(lw=1, color='k')
	if min(new_barplot_list) < 0:
		min_spot = min(new_barplot_list) - top_bar*0.2
	else:
		min_spot = 0
	for thing in range(x_count):
		if new_barplot_list[thing] >= 0:
			ax.text(thing, new_barplot_list[thing]+top_bar*0.01, barplot_text[thing], fontsize=deffont*0.8, ha='center', va='bottom', rotation=90, weight='semibold')
		else:
			ax.text(thing, new_barplot_list[thing]-top_bar*0.01, barplot_text[thing], fontsize=deffont*0.8, ha='center', va='top', rotation=90, weight='semibold')
	ax.set_ylim(min_spot, top_bar*1.2)
	ax.set_xticks(x_label_num)
	ax.set_xticklabels(x_label_name, rotation = -45, ha="left", fontsize=deffont)
	ax.set_title('Change in Percentage of Genes in Tuson list from Sharp List to Broad List', fontsize=deffont*1.2, weight='semibold')
	plt.savefig(save_spot, dpi=300)

def prev_cancer_gene_barplot_just_plot(ax, peak_list_to_plot, \
	gene_types, types, type_labels, len_or_len_change, \
	len_len_change, loop_count, gene_type_count, sig_list, deffont=5, x_ax_text_ratio=1, \
	text_lower_ratio=1, y_lim_multi=1.3, xtick_rotation=-45, type_legend=False, \
	type_legend_spot='below', bottom_label_extra_down=1, label_size_multi=1, \
	legend_inside_plot=True, rotate_x=0, x_ha='center', x_va='center', \
	label_markersize=None, label_fontsize=5, legend_labelspacing=0.5, \
	min_fontsize=5, bar_width=0.9, white_writing_size = 1.3, just_stars=True, \
	bar_colors=['C0','C6','C7','C4','C1','C5','C3','C2','C8','C9'], fig=None):

	xlabels = []
	x_list = []
	color_list = []
	type_spot = []

	if len_or_len_change == 'len':
		groups = ['Broad', 'Sharp']
	else:
		groups = ['Lengthened', 'Shortened', 'No Change']
	data_to_axis = ax.transData + ax.transAxes.inverted()
	current_x = 0

	max_text_height = 0
	for file_loop in range(len(peak_list_to_plot)):
		this_denom = len(peak_list_to_plot[file_loop][-1])
		if this_denom == 0:
			this_loop_height = 0
		else:
			this_loop_height = sum([len(thing) for thing in \
				peak_list_to_plot[file_loop][:-1]])/len(peak_list_to_plot[file_loop][-1])*100
		if this_loop_height > max_text_height: 
			max_text_height = this_loop_height
	extra_ind = 0
	for file_loop in range(len(peak_list_to_plot)):
		for group_loop in range(len(peak_list_to_plot[file_loop])-1):
			if file_loop % len(groups) != len(groups)-1:
				dist_from_same = len(groups) - (file_loop % len(groups)) - 1
				if len(peak_list_to_plot[file_loop][-1]) > 0:
					numerator = len(peak_list_to_plot[file_loop][group_loop])/ \
						len(peak_list_to_plot[file_loop][-1])
				else:
					numerator = 0
				denominator = \
					len(peak_list_to_plot[file_loop+dist_from_same][group_loop])/ \
					len(peak_list_to_plot[file_loop+dist_from_same][-1])
				try:
					perc_change = int(((numerator/denominator)*100)-100)
				except:
					perc_change = int((((numerator+1)/(denominator+1))*100)-100)
				if perc_change >= 0:
					perc_change = str(perc_change)
				else:
					perc_change = str(perc_change)
				if just_stars:
					# sig_list format:
						# for sample type
							# for bar (loop_count)
								# for sub_bar (gene_type count)
					# current loops:
						# for file_loop (sample type * loop count)
							# for group loop (gene_type_count)
					current_ind = file_loop - extra_ind
					ind1 = int(current_ind/(loop_count-1))
					ind2 = current_ind % (loop_count-1)
					# print(ind1, ind2, group_loop)

					if sig_list[ind1][ind2][group_loop] == 'sig':
						perc_change = '*'
					else:
						perc_change = ''
			else:
				perc_change = ''
				if group_loop == 0:
					extra_ind += 1
			if len(peak_list_to_plot[file_loop][-1]) > 0:
				bar_height = len(peak_list_to_plot[file_loop][group_loop])/ \
					len(peak_list_to_plot[file_loop][-1])*100
			else:
				bar_height = 0
			write_down = 0.2 * text_lower_ratio
			fontsize = deffont*white_writing_size
			if fontsize < min_fontsize:
				fontsize = min_fontsize
			if group_loop == 0:
				ax.bar(current_x, bar_height, width=bar_width, color=bar_colors[group_loop])
				######################################################################################################
				if fig is not None:
					new_text, loc = fig.text_and_location(ax, current_x, 0, perc_change, color='w', ha='center', \
						va='bottom', weight='semibold', fontsize=fontsize)
					print((current_x, bar_height), loc)
					ax.scatter([loc[0][0], loc[0][1], loc[0][0], loc[0][1]], 
						[loc[1][0], loc[1][0], loc[1][1], loc[1][1]], s=1, c='r', zorder=10)
				else:
					ax.text(current_x, 0, perc_change, color='w', ha='center', \
						va='bottom', weight='semibold', fontsize=fontsize)
				######################################################################################################
			else:
				if len(peak_list_to_plot[file_loop][-1]) > 0:
					bottom_spot = sum([len(thing) for thing in \
						peak_list_to_plot[file_loop][:group_loop]])/ \
						len(peak_list_to_plot[file_loop][-1])*100
				else:
					bottom_spot = 0
				ax.bar(current_x, bar_height, width=bar_width, bottom=bottom_spot, \
					color=bar_colors[group_loop])
				if fig is not None:
					new_text, loc = fig.text_and_location(ax, current_x, bottom_spot - write_down, perc_change, \
						color='w', ha='center', va='bottom', weight='semibold', \
						fontsize=fontsize)
					print((current_x, bar_height), loc)
					ax.scatter([loc[0][0], loc[0][1], loc[0][0], loc[0][1]], 
						[loc[1][0], loc[1][0], loc[1][1], loc[1][1]], s=1, c='r', zorder=10)
				else:
					ax.text(current_x, bottom_spot - write_down, perc_change, \
						color='w', ha='center', va='bottom', weight='semibold', \
						fontsize=fontsize)
		fontsize = deffont*1.0* label_size_multi
		if fontsize < min_fontsize:
			fontsize = min_fontsize
		if len(peak_list_to_plot[file_loop][-1]) > 0:
			y_val = sum([len(thing) for thing in peak_list_to_plot[file_loop][:-1]])/ \
				len(peak_list_to_plot[file_loop][-1])*100+(max_text_height*0.01)
		else:
			y_val = 0
		ax.text(current_x, y_val, \
			str(sum([len(thing) for thing in peak_list_to_plot[file_loop][:-1]]))+'/'+ \
			str(len(peak_list_to_plot[file_loop][-1])), rotation=90, \
			ha='center', va='bottom', weight='semibold', fontsize=fontsize)

		xlabels.append(groups[file_loop%len(groups)])
		x_list.append(current_x)
		current_x += 1
		if file_loop % len(groups) == len(groups)-1:
			type_spot.append(current_x - (0.5 + len(groups)/2))
			current_x += 0.5

	fontsize = deffont*label_size_multi
	if fontsize < min_fontsize:
		fontsize = min_fontsize

	easy_fig.format_panel(ax, ylim = (0, max_text_height*y_lim_multi))

	ylabels = ax.get_yticks()
	easy_fig.format_panel(ax, 
		xtick_dict = {'ticks': x_list},
		xticklabel_dict = {'labels': [label[0] for label in xlabels], 
			'rotation': 0, 'ha': "center", \
			'fontsize': fontsize, 'weight': 400},
		ytick_dict = {'ticks': ylabels},
		yticklabel_dict = {'labels': [str(round(float(thing),1)) for thing in ylabels], \
			'fontsize': fontsize, 'weight': 400}
	)

	if len_len_change == 'len':
		text_y = -0.039*deffont
	elif len_len_change == 'len_change':
		text_y = -0.06*deffont
	else:
		raise ValueError('oops')
	text_y *= x_ax_text_ratio
	for cancer_loop in range(len(types)):
		fontsize = deffont*1.1*label_size_multi
		if fontsize < min_fontsize:
			fontsize = min_fontsize
		ax.text(data_to_axis.transform((type_spot[cancer_loop],0))[0], text_y, \
			types[cancer_loop], ha=x_ha, va=x_va, transform=ax.transAxes, \
			fontsize=fontsize, weight=400, rotation=rotate_x)
	left, right = ax.get_xlim()
	x_mid = (right-left)/2
	gene_group_y = text_y-(0.01*deffont)*x_ax_text_ratio*bottom_label_extra_down
	fontsize = deffont*1.2*label_size_multi
	if fontsize < min_fontsize:
		fontsize = min_fontsize
	ax.text(0.5, gene_group_y, 'Gene Groups', ha='center', weight='semibold', \
		fontsize=fontsize, transform=ax.transAxes)
	legend_elements = []

	if label_markersize is None:
		label_markersize = deffont*1.2

	if label_fontsize < min_fontsize:
		label_fontsize = min_fontsize

	ylabel_fontsize = deffont*1.3*label_size_multi
	if ylabel_fontsize < min_fontsize:
		ylabel_fontsize = min_fontsize

	easy_fig.format_panel(ax,
		legend_dict=\
			{
				'legend_values': type_labels + groups, 
				'marker': ['o']*len(type_labels) + ['${}$'.format(this_group[0]) for this_group in groups], 
				'color': ['w']*len(type_labels) + ['k'] * len(groups), 
				'markerfacecolor': bar_colors + ['k'] * len(groups),
				'markersize': [label_markersize]*len(type_labels) + [label_markersize*0.75]*len(groups), 
				'lw': 0, 'whichcorner': 'upper right',
				'maxwidth': 1-(deffont*0.0015), 'maxheight': 1-(deffont*0.0015), 'vdistance': 0,
				'hdistance': 0, 'borderaxespad': 0, 'houtside': not legend_inside_plot,
				'prop': {'size': deffont*1.1, 'weight': 'semibold'}, 
				'fontsize': fontsize, 'labelspacing': legend_labelspacing
			}, \
		ylabel_dict = {'ylabel': 'Percent of Genes', 'weight': 'semibold', 'fontsize': fontsize}
	)

def prev2_cancer_gene_barplot_just_plot(ax, peak_list_to_plot, \
	gene_types, types, type_labels, len_or_len_change, \
	len_len_change, loop_count, gene_type_count, sig_list, deffont=5, x_ax_text_ratio=1, \
	text_lower_ratio=1, y_lim_multi=1.3, xtick_rotation=-45, type_legend=False, \
	type_legend_spot='below', bottom_label_extra_down=1, label_size_multi=1, \
	legend_inside_plot=True, rotate_x=0, x_ha='center', x_va='center', \
	label_markersize=None, label_fontsize=5, legend_labelspacing=0.5, \
	min_fontsize=5, bar_width=0.9, white_writing_size = 1.3, just_stars=True, \
	bar_colors=['C0','C6','C7','C4','C1','C5','C3','C2','C8','C9'], fig=None):

	xlabels = []
	x_list = []
	color_list = []
	type_spot = []

	if len_or_len_change == 'len':
		groups = ['Broad', 'Sharp']
	else:
		groups = ['Lengthened', 'Shortened', 'No Change']
	data_to_axis = ax.transData + ax.transAxes.inverted()

	
	# setting up bar percents for each bar in the barplot: percent_lists
	extra_ind = 0
	percent_lists = []
	# i think max_text_height might be used to move text up nominally, but proportionally to
		# the height of the actual figure
	max_text_height = 0
	for file_loop in range(len(peak_list_to_plot)):
		percent_lists.append([])
		for group_loop in range(len(peak_list_to_plot[file_loop])-1):
			if len(peak_list_to_plot[file_loop][-1]) > 0:
				percent_lists[-1].append(len(peak_list_to_plot[file_loop][group_loop])/ \
					len(peak_list_to_plot[file_loop][-1]))
			else:
				percent_lists[-1].append(0)
		this_loop_height = sum(percent_lists[-1])*100
		if this_loop_height > max_text_height:
			max_text_height = this_loop_height

	bar_height_list = []
	perc_change_list = []
	current_x = 0
	for perc_loop, this_perc in enumerate(percent_lists):
		bar_height_list.append([])
		perc_change_list.append([])
		for group_loop, this_group in enumerate(this_perc):
			if perc_loop % len(groups) != len(groups)-1:
				dist_from_same = len(groups) - (perc_loop % len(groups)) - 1
				numerator = this_group
				denominator = percent_lists[perc_loop + dist_from_same][group_loop]
				try:
					perc_change = int(((numerator/denominator)*100)-100)
				except:
					print('perc_change didn\'t work, num: {}, denom: {}'.format(numerator, denominator))
					perc_change = 'failed'
				perc_change = str(perc_change)
				if just_stars:
					current_ind = perc_loop - extra_ind
					ind1 = int(current_ind/(loop_count-1))
					ind2 = current_ind % (loop_count-1)

					if sig_list[ind1][ind2][group_loop] == 'sig':
						perc_change = '*'
					else:
						perc_change = ''
			else:
				perc_change = ''
				if group_loop == 0:
					extra_ind += 1
			perc_change_list[-1].append(perc_change)
			bar_height = this_group*100
			bar_height_list[-1].append(bar_height)

	# plotting bars + adding text on the bars
	all_heights = []
	text_under = []
	adjusted_heights = []
	write_down = 0.2 * text_lower_ratio
	fontsize = deffont*white_writing_size
	if fontsize < min_fontsize:
		fontsize = min_fontsize
	for bar_loop, this_bar in enumerate(bar_height_list):
		for inner_loop, bar_height in enumerate(this_bar):

			if inner_loop == 0:
				ax.bar(current_x, bar_height, width=bar_width, color=bar_colors[inner_loop])
			else:
				if len(peak_list_to_plot[bar_loop][-1]) > 0:
					bottom_spot = sum(this_bar[:inner_loop])
				else:
					bottom_spot = 0
				ax.bar(current_x, bar_height, width=bar_width, bottom=bottom_spot, \
					color=bar_colors[inner_loop])
		current_x += 1
		if bar_loop % len(groups) == len(groups)-1:
			type_spot.append(current_x - (0.5 + len(groups)/2))
			current_x += 0.5

	current_x = 0
	font_multi=1
	for bar_loop, this_bar in enumerate(bar_height_list):
		for inner_loop, bar_height in enumerate(this_bar):
			if inner_loop != 0 and len(peak_list_to_plot[bar_loop][-1]) > 0:
				bottom_spot = sum(this_bar[:inner_loop])
			else:
				bottom_spot = 0
			if perc_change_list[bar_loop][inner_loop] == '*':
				if fig is not None:
					obj, width, height = fig.marker_real_bbox(ax, current_x, bottom_spot, marker=(6,2,0), 
						color='w', s=fontsize*font_multi, zorder=2)
					if height > 0:
						all_heights.append((bar_loop,inner_loop,round(height, 3),round(bar_height, 3)))
					# if bar is low enough to squish
					if height > bar_height * 0.5:
						print(bar_loop, inner_loop, height, this_bar[inner_loop-1])
						# if bar is not low enough to move under
						if height < bar_height:
							print('nbd')
							obj.set_visible(False)
							new_y = bar_height/2 + bottom_spot - height
							obj, width, height = fig.marker_real_bbox(ax, current_x, new_y, marker=(6,2,0), 
								color='w', s=fontsize*font_multi, zorder=2)
						# if bar under is 
						elif this_bar[inner_loop-1] >= height*3 and this_bar[inner_loop-1] < height*4:
							print('\tmiddle')
							obj.set_visible(False)
							obj, width, height = fig.marker_real_bbox(ax, current_x, bottom_spot-height-(this_bar[inner_loop-1]/4), 
								marker=(6,2,0), color='w', s=fontsize*font_multi, zorder=2)
						elif this_bar[inner_loop-1] > height*4:
							print('\thigh')
							obj, width, height = fig.marker_real_bbox(ax, current_x, bottom_spot-height-height, 
								marker=(6,2,0), color='w', s=fontsize*font_multi, zorder=2)

						else:
							print('oops, which bar: {}, {}'.format(bar_loop, inner_loop))
					elif inner_loop + 1 < len(this_bar):
						if this_bar[inner_loop+1] < height and bar_height >= height*3 and bar_height < height*4:
							obj.set_visible(False)
							new_y = bottom_spot + (bar_height/4)
							obj, width, height = fig.marker_real_bbox(ax, current_x, new_y, marker=(6,2,0), 
								color='w', s=fontsize*font_multi, zorder=2)
					else:
						pass



			else:
				ax.text(current_x, bottom_spot - write_down, perc_change_list[bar_loop][inner_loop], \
					color='w', ha='center', va='bottom', weight='semibold', \
					fontsize=fontsize)
		fontsize = deffont*1.0* label_size_multi
		if fontsize < min_fontsize:
			fontsize = min_fontsize
		if len(peak_list_to_plot[bar_loop][-1]) > 0:
			y_val = sum(this_bar)+(max_text_height*0.01)
		else:
			y_val = 0
		ax.text(current_x, y_val, \
			str(sum([len(thing) for thing in peak_list_to_plot[bar_loop][:-1]]))+'/'+ \
			str(len(peak_list_to_plot[bar_loop][-1])), rotation=90, \
			ha='center', va='bottom', weight='semibold', fontsize=fontsize)

		xlabels.append(groups[bar_loop%len(groups)])
		x_list.append(current_x)
		current_x += 1
		if bar_loop % len(groups) == len(groups)-1:
			type_spot.append(current_x - (0.5 + len(groups)/2))
			current_x += 0.5
	print('all_heights: \n\t','\n\t'.join([str(thing) for thing in all_heights]))
	print('adj_heights: \n\t','\n\t'.join([str(thing) for thing in adjusted_heights]))

	fontsize = deffont*label_size_multi
	if fontsize < min_fontsize:
		fontsize = min_fontsize

	easy_fig.format_panel(ax, ylim = (0, max_text_height*y_lim_multi))

	ylabels = ax.get_yticks()
	easy_fig.format_panel(ax, 
		xtick_dict = {'ticks': x_list},
		xticklabel_dict = {'labels': [label[0] for label in xlabels], 
			'rotation': 0, 'ha': "center", \
			'fontsize': fontsize, 'weight': 400},
		ytick_dict = {'ticks': ylabels},
		yticklabel_dict = {'labels': [str(round(float(thing),1)) for thing in ylabels], \
			'fontsize': fontsize, 'weight': 400}
	)

	if len_len_change == 'len':
		text_y = -0.039*deffont
	elif len_len_change == 'len_change':
		text_y = -0.06*deffont
	else:
		raise ValueError('oops')
	text_y *= x_ax_text_ratio
	for cancer_loop in range(len(types)):
		fontsize = deffont*1.1*label_size_multi
		if fontsize < min_fontsize:
			fontsize = min_fontsize
		ax.text(data_to_axis.transform((type_spot[cancer_loop],0))[0], text_y, \
			types[cancer_loop], ha=x_ha, va=x_va, transform=ax.transAxes, \
			fontsize=fontsize, weight=400, rotation=rotate_x)
	left, right = ax.get_xlim()
	x_mid = (right-left)/2
	gene_group_y = text_y-(0.01*deffont)*x_ax_text_ratio*bottom_label_extra_down
	fontsize = deffont*1.2*label_size_multi
	if fontsize < min_fontsize:
		fontsize = min_fontsize
	ax.text(0.5, gene_group_y, 'Gene Groups', ha='center', weight='semibold', \
		fontsize=fontsize, transform=ax.transAxes)
	legend_elements = []

	if label_markersize is None:
		label_markersize = deffont*1.2

	if label_fontsize < min_fontsize:
		label_fontsize = min_fontsize

	ylabel_fontsize = deffont*1.3*label_size_multi
	if ylabel_fontsize < min_fontsize:
		ylabel_fontsize = min_fontsize

	easy_fig.format_panel(ax,
		legend_dict=\
			{
				'legend_values': type_labels + groups, 
				'marker': ['o']*len(type_labels) + ['${}$'.format(this_group[0]) for this_group in groups], 
				'color': ['w']*len(type_labels) + ['k'] * len(groups), 
				'markerfacecolor': bar_colors + ['k'] * len(groups),
				'markersize': [label_markersize]*len(type_labels) + [label_markersize*0.75]*len(groups), 
				'lw': 0, 'whichcorner': 'upper right',
				'maxwidth': 1-(deffont*0.0015), 'maxheight': 1-(deffont*0.0015), 'vdistance': 0,
				'hdistance': 0, 'borderaxespad': 0, 'houtside': not legend_inside_plot,
				'prop': {'size': deffont*1.1, 'weight': 'semibold'}, 
				'fontsize': fontsize, 'labelspacing': legend_labelspacing
			}, \
		ylabel_dict = {'ylabel': 'Percent of Genes', 'weight': 'semibold', 'fontsize': fontsize}
	)

def cancer_gene_barplot_just_plot(ax, peak_list_to_plot, \
	gene_types, types, type_labels, len_or_len_change, \
	len_len_change, loop_count, gene_type_count, sig_list, deffont=5, x_ax_text_ratio=1, \
	text_lower_ratio=1, y_lim_multi=1.3, xtick_rotation=-45, type_legend=False, \
	type_legend_spot='below', bottom_label_extra_down=1, label_size_multi=1, \
	legend_inside_plot=True, rotate_x=0, x_ha='center', x_va='center', \
	label_markersize=None, label_fontsize=5, legend_labelspacing=0.5, \
	min_fontsize=5, bar_width=0.9, white_writing_size = 1.3, just_stars=True, \
	bar_colors=['C0','C6','C7','C4','C1','C5','C3','C2','C8','C9'],
	primary_ast_color='w', secondary_ast_color='k', fig=None, verbose=False):

	xlabels = []
	x_list = []
	color_list = []
	type_spot = []

	if len_or_len_change == 'len':
		groups = ['Broad', 'Sharp']
	else:
		groups = ['Lengthened', 'Shortened', 'No Change']
	data_to_axis = ax.transData + ax.transAxes.inverted()

	# setting up bar percents for each bar in the barplot: percent_lists
	extra_ind = 0
	percent_lists = []
	# i think max_text_height might be used to move text up nominally, but proportionally to
		# the height of the actual figure
	max_text_height = 0
	for file_loop in range(len(peak_list_to_plot)):
		percent_lists.append([])
		for group_loop in range(len(peak_list_to_plot[file_loop])-1):
			if len(peak_list_to_plot[file_loop][-1]) > 0:
				percent_lists[-1].append(len(peak_list_to_plot[file_loop][group_loop])/ \
					len(peak_list_to_plot[file_loop][-1]))
			else:
				percent_lists[-1].append(0)
		this_loop_height = sum(percent_lists[-1])*100
		if this_loop_height > max_text_height:
			max_text_height = this_loop_height

	# setting bar heights for each bar (bar_height_list) and 
		# text for each bar (perc_change_list)
	bar_height_list = []
	perc_change_list = []
	current_x = 0
	for perc_loop, this_perc in enumerate(percent_lists):
		bar_height_list.append([])
		perc_change_list.append([])
		for group_loop, this_group in enumerate(this_perc):
			if perc_loop % len(groups) != len(groups)-1:
				dist_from_same = len(groups) - (perc_loop % len(groups)) - 1
				numerator = this_group
				denominator = percent_lists[perc_loop + dist_from_same][group_loop]
				try:
					perc_change = int(((numerator/denominator)*100)-100)
				except:
					print('perc_change didn\'t work, num: {}, denom: {}'.format(numerator, denominator))
					perc_change = 'failed'
				perc_change = str(perc_change)
				if just_stars:
					current_ind = perc_loop - extra_ind
					ind1 = int(current_ind/(loop_count-1))
					ind2 = current_ind % (loop_count-1)

					if sig_list[ind1][ind2][group_loop] == 'sig':
						perc_change = '*'
					else:
						perc_change = ''
			else:
				perc_change = ''
				if group_loop == 0:
					extra_ind += 1
			perc_change_list[-1].append(perc_change)
			bar_height = this_group*100
			bar_height_list[-1].append(bar_height)

	# plotting bars + adding text on the bars
	all_heights = []
	text_under = []
	adjusted_heights = []
	write_down = 0.2 * text_lower_ratio
	fontsize = deffont*white_writing_size
	if fontsize < min_fontsize:
		fontsize = min_fontsize
	for bar_loop, this_bar in enumerate(bar_height_list):
		for inner_loop, bar_height in enumerate(this_bar):

			# plotting bar
			if inner_loop == 0:
				ax.bar(current_x, bar_height, width=bar_width, color=bar_colors[inner_loop])
			else:
				if len(peak_list_to_plot[bar_loop][-1]) > 0:
					bottom_spot = sum(this_bar[:inner_loop])
				else:
					bottom_spot = 0
				ax.bar(current_x, bar_height, width=bar_width, bottom=bottom_spot, \
					color=bar_colors[inner_loop])
		current_x += 1
		if bar_loop % len(groups) == len(groups)-1:
			type_spot.append(current_x - (0.5 + len(groups)/2))
			current_x += 0.5

	current_x = 0
	# adding text
		# adding text in a separate loop because if the xlim/ylim of the plot changes
			# significantly b/w adding asterisks, the positioning of the asterisks
			# can be progressively out of place
	# print(verbose)
	# print(sig_list)
	# print(perc_change_list)
	# print([[round(stuff,3) for stuff in thing] for thing in bar_height_list])
	for bar_loop, this_bar in enumerate(bar_height_list):
		last_top = 0
		height = 0
		for inner_loop, bar_height in enumerate(this_bar):
			if inner_loop != 0 and len(peak_list_to_plot[bar_loop][-1]) > 0:
				bottom_spot = sum(this_bar[:inner_loop])
			else:
				bottom_spot = 0
			if fig is not None:
				# print('sig plot!')
				if perc_change_list[bar_loop][inner_loop] == '*':
					# print('doing fancy text')
					if last_top > bottom_spot:
						bottom_spot = last_top
					# try to put asterisk in the current bar with 1x padding on the bottom
					text_obj, loc_info, failed = \
						fig.buffer_ast_in_bar(ax, (current_x - (bar_width/2), current_x + (bar_width/2)), 
							(bottom_spot, bottom_spot + bar_height), fontsize, pref_buffer=1,
							which_buffer='bottom', min_fontsize=2, min_vbuffer=0.5, min_hbuffer=0.25, 
							color=primary_ast_color, weight='semibold', zorder=2, verbose=verbose)
					fontsize_ratio, xmiddle, ymiddle, width, height = loc_info
					# otherwise try to fit the asterisk in the center of the bar w/ as little as 0 padding
					if failed:
						text_obj, loc_info, failed = \
							fig.buffer_ast_in_bar(ax, (current_x - (bar_width/2), current_x + (bar_width/2)), 
								(bottom_spot, bottom_spot + bar_height), fontsize, pref_buffer=0.25,
								which_buffer='bottom', min_fontsize=2, min_vbuffer=0.0, min_hbuffer=0.25, 
								color=primary_ast_color, weight='semibold', zorder=2, verbose=verbose, min_buff_align='center')
						fontsize_ratio, xmiddle, ymiddle, width, height = loc_info
					# otherwise, if not in the bottom bar, try to fit the asterisk at the top of the bar
						# under the current bar
					if failed and bottom_spot != 0:
						text_obj, loc_info, failed = \
							fig.buffer_ast_in_bar(ax, (current_x - (bar_width/2), current_x + (bar_width/2)), 
								(last_top, bottom_spot), fontsize, pref_buffer=1,
								which_buffer='top', min_fontsize=2, min_vbuffer=0.25, min_hbuffer=0.25, 
								color=primary_ast_color, weight='semibold', zorder=2, verbose=verbose)
						fontsize_ratio, xmiddle, ymiddle, width, height = loc_info
					# last option, put the asterisk in the current bar but allow overflow above
						# if >9ish% overflows -> use the secondary asterisk color
					if failed:
						# print('last attempt')
						if inner_loop == len(this_bar) - 1:
							if height > (bar_height*1.1):
								color = secondary_ast_color
							else:
								color = primary_ast_color
						else:
							color = primary_ast_color
						text_obj, loc_info, failed = \
							fig.buffer_ast_in_bar(ax, (current_x - (bar_width/2), current_x + (bar_width/2)), 
								(bottom_spot, None), fontsize, pref_buffer=0.25,
								which_buffer='bottom', min_fontsize=2, min_vbuffer=0.25, min_hbuffer=0.25, 
								color=color, weight='semibold', zorder=2, verbose=verbose)
						fontsize_ratio, xmiddle, ymiddle, width, height = loc_info
					# print(bar_loop, inner_loop, ymiddle, height, failed)
					last_top = ymiddle + (height/2)

			else:
				ax.text(current_x, bottom_spot - write_down, perc_change_list[bar_loop][inner_loop], \
					color=primary_ast_color, ha='center', va='bottom', weight='semibold', \
					fontsize=fontsize)
		# add peak fraction text. if no asterisk overflow: put right above the bar.
			# otherwise, but above asterisk w/ 1x passing b/w
		fontsize = deffont*1.0* label_size_multi
		if fontsize < min_fontsize:
			fontsize = min_fontsize
		if len(peak_list_to_plot[bar_loop][-1]) > 0:
			y_val = sum(this_bar)+(max_text_height*0.01)
		else:
			y_val = 0
		if fig is not None:
			last_top_spot = last_top+height
			if y_val < last_top_spot:
				y_val = last_top_spot
		ax.text(current_x, y_val, \
			str(sum([len(thing) for thing in peak_list_to_plot[bar_loop][:-1]]))+'/'+ \
			str(len(peak_list_to_plot[bar_loop][-1])), rotation=90, \
			ha='center', va='bottom', weight='semibold', fontsize=fontsize)

		xlabels.append(groups[bar_loop%len(groups)])
		x_list.append(current_x)
		current_x += 1
		if bar_loop % len(groups) == len(groups)-1:
			type_spot.append(current_x - (0.5 + len(groups)/2))
			current_x += 0.5

	# doing further formatting after adding bars + text inside the plot
	fontsize = deffont*label_size_multi
	if fontsize < min_fontsize:
		fontsize = min_fontsize

	easy_fig.format_panel(ax, ylim = (0, max_text_height*y_lim_multi))
	ylabels = ax.get_yticks()

	# panel formatting for labels
	easy_fig.format_panel(ax, 
		xtick_dict = {'ticks': x_list},
		xticklabel_dict = {'labels': [label[0] for label in xlabels], 
			'rotation': 0, 'ha': "center", \
			'fontsize': fontsize, 'weight': 400},
		ytick_dict = {'ticks': ylabels},
		yticklabel_dict = {'labels': [str(round(float(thing),1)) for thing in ylabels], \
			'fontsize': fontsize, 'weight': 400}
	)

	if len_len_change == 'len':
		text_y = -0.039*deffont
	elif len_len_change == 'len_change':
		text_y = -0.06*deffont
	else:
		raise ValueError('oops')
	text_y *= x_ax_text_ratio
	for cancer_loop in range(len(types)):
		fontsize = deffont*1.1*label_size_multi
		if fontsize < min_fontsize:
			fontsize = min_fontsize
		ax.text(data_to_axis.transform((type_spot[cancer_loop],0))[0], text_y, \
			types[cancer_loop], ha=x_ha, va=x_va, transform=ax.transAxes, \
			fontsize=fontsize, weight=400, rotation=rotate_x)
	left, right = ax.get_xlim()
	x_mid = (right-left)/2
	gene_group_y = text_y-(0.01*deffont)*x_ax_text_ratio*bottom_label_extra_down
	fontsize = deffont*1.2*label_size_multi
	if fontsize < min_fontsize:
		fontsize = min_fontsize
	ax.text(0.5, gene_group_y, 'Gene Groups', ha='center', weight='semibold', \
		fontsize=fontsize, transform=ax.transAxes)

	# setting up for legend
	legend_elements = []

	if label_markersize is None:
		label_markersize = deffont*1.2

	if label_fontsize < min_fontsize:
		label_fontsize = min_fontsize

	ylabel_fontsize = deffont*1.3*label_size_multi
	if ylabel_fontsize < min_fontsize:
		ylabel_fontsize = min_fontsize

	# panel formatting for legend
	easy_fig.format_panel(ax,
		legend_dict=\
			{
				'legend_values': type_labels + groups, 
				'marker': ['o']*len(type_labels) + ['${}$'.format(this_group[0]) for this_group in groups], 
				'color': ['w']*len(type_labels) + ['k'] * len(groups), 
				'markerfacecolor': bar_colors + ['k'] * len(groups),
				'markersize': [label_markersize]*len(type_labels) + [label_markersize*0.75]*len(groups), 
				'lw': 0, 'whichcorner': 'upper right',
				'maxwidth': 1-(deffont*0.0015), 'maxheight': 1-(deffont*0.0015), 'vdistance': 0,
				'hdistance': 0, 'borderaxespad': 0, 'houtside': not legend_inside_plot,
				'prop': {'size': deffont*1.1, 'weight': 'semibold'}, 
				'fontsize': fontsize, 'labelspacing': legend_labelspacing
			}, \
		ylabel_dict = {'ylabel': 'Percent of Genes', 'weight': 'semibold', 'fontsize': fontsize}
	)

	# old code
		# print('loops: ({},{}), xvals: {}, yvals: {}'
						# .format(bar_loop, inner_loop, (current_x - (bar_width/2), current_x + (bar_width/2)), 
						# 	(bottom_spot, bottom_spot + bar_height)))
					# try:
					# print(failed)
					# if not failed:
						# print('first run, loops: ({},{}), \
						# 	\n\texpected_xvals: {}, \
						# 	\n\texpected_yvals: {}, \
						# 	\n\treal_xvals: {}, real_yvals: {}'
						# 	.format(bar_loop, inner_loop, (current_x - (bar_width/2), current_x + (bar_width/2)), 
						# 		(bottom_spot, bottom_spot + bar_height), 
						# 		(xmiddle-(width/2), xmiddle, xmiddle+(width/2)), 
						# 		(ymiddle-(height/2), ymiddle, ymiddle+(height/2))))
					# else:
						# print('first run, loops: ({},{}), \
						# 	\n\texpected_xvals: {}, \
						# 	\n\texpected_yvals: {}, \
						# 	\n\twidth: {}, height: {}'
						# 	.format(bar_loop, inner_loop, (current_x - (bar_width/2), current_x + (bar_width/2)), 
						# 		(bottom_spot, bottom_spot + bar_height), width, height))
					# except:
					# 	print(new_xmiddle,width,new_ymiddle,height)
					# 	raise
					# print('ran and failed = {}. {}'.format(failed, [fontsize_ratio, x_move, y_move, width, height]))
					# new_text, loc = fig.text_and_location(ax, current_x, bottom_spot - write_down, perc_change_list[bar_loop][inner_loop], \
					# 	color='w', ha='center', va='bottom', weight='semibold', \
					# 	fontsize=fontsize, zorder=1)
					# height = loc[1][1] - loc[1][0]
					# if height > 0:
					# 	all_heights.append((bar_loop,inner_loop,round(height, 3),round(bar_height, 3)))
					# if bar is low enough to squish
					# if height > bar_height * 0.5:
					# print('\t', loc_info, failed)
					# if bar_loop == 9 and inner_loop == 2:
						# print('({}, {}), initial, {}'.format(bar_loop, inner_loop, failed))
					# if failed:
						# # print('\tsecond run')
						# text_obj, loc_info, failed = \
						# 	fig.buffer_ast_in_bar(ax, (current_x - (bar_width/2), current_x + (bar_width/2)), 
						# 		(bottom_spot, bottom_spot + bar_height), fontsize, pref_buffer=0.25,
						# 		which_buffer='bottom', min_fontsize=None, min_vbuffer=0.25, min_hbuffer=0.25, color='w', 
						# 		weight='semibold', zorder=2)
						# fontsize_ratio, xmiddle, ymiddle, width, height = loc_info
						# # print('\t', loc_info, failed)
						# print('\tsecond run')
						# if bar_loop == 9 and inner_loop == 2:
							# print('\t({}, {}), in center, {}'.format(bar_loop, inner_loop, failed))
							# print(round(bottom_spot, 4), round(bottom_spot + bar_height, 4), 
							# 	round(ymiddle, 4), round(height, 4), )
							# print(round(xmiddle, 4))
							# ax.scatter(xmiddle, ymiddle, s=1.5, c='C5', zorder=5)
						# print('\t', loc_info, failed)
						# print('\tsecond run')
						# print('\t', loc_info, failed)
						# print('\tsecond run')
						# print('\t', loc_info, failed)
						# print('ran again. {}'.format([fontsize_ratio, x_move, y_move, width, height]))


					#
						# print(bar_loop, inner_loop, height, this_bar[inner_loop-1])
						# # if bar is not low enough to move under
						# if height < bar_height:
						# 	print('nbd')
						# 	new_text.set_visible(False)
						# 	new_y = bar_height/2 + bottom_spot - height
						# 	# (xmiddle, ymiddle, width, new_height)
						# 	# text_out, loc = \
						# 	# 	asterisk_and_location(self, ax, x, y, *args, trans=None, **kwargs)
						# 	new_text, loc = fig.text_and_location(ax, current_x, new_y, perc_change_list[bar_loop][inner_loop], color='w', ha='center', \
						# 		va='bottom', weight='semibold', fontsize=fontsize, zorder=1)
						# 	adj_height = loc[1][1] - loc[1][0]
						# 	adjusted_heights.append((bar_loop,inner_loop,round(adj_height, 3),round(bar_height, 3)))
						# elif this_bar[inner_loop-1] >= height*3 and this_bar[inner_loop-1] < height*4:
						# 	print('\tmiddle')
						# 	new_text.set_visible(False)
						# 	new_text, loc = fig.text_and_location(ax, current_x, bottom_spot-height-(this_bar[inner_loop-1]/4), perc_change_list[bar_loop][inner_loop], color='w', ha='center', \
						# 		va='bottom', weight='semibold', fontsize=fontsize, zorder=1)
						# 	adj_height = loc[1][1] - loc[1][0]
						# 	adjusted_heights.append((bar_loop,inner_loop,round(adj_height, 3),round(bar_height, 3)))
						# elif this_bar[inner_loop-1] > height*4:
						# 	print('\thigh')
						# 	new_text, loc = fig.text_and_location(ax, current_x, bottom_spot-height-height, perc_change_list[bar_loop][inner_loop], color='w', ha='center', \
						# 		va='bottom', weight='semibold', fontsize=fontsize, zorder=1)
						# 	adj_height = loc[1][1] - loc[1][0]
						# 	adjusted_heights.append((bar_loop,inner_loop,round(adj_height, 3),round(bar_height, 3)))

						# else:
						# 	print('oops, which bar: {}, {}'.format(bar_loop, inner_loop))
					# elif inner_loop + 1 < len(this_bar):
						# if this_bar[inner_loop+1] < height and bar_height >= height*3 and bar_height < height*4:
						# 	new_text.set_visible(False)
						# 	new_y = bottom_spot + (bar_height/4)
						# 	new_text, loc = fig.text_and_location(ax, current_x, new_y, perc_change_list[bar_loop][inner_loop], color='w', ha='center', \
						# 		va='center_baseline', weight='semibold', fontsize=fontsize, zorder=1)
					# else:
					# print('')
						# try:
						# last_top = bottom_spot + (bar_height/2) + y_move + (width * fontsize_ratio)/2
						# except:
						# 	print(bottom_spot, (bar_height/2), y_move, width, fontsize_ratio)
						# 	raise
			# print(bar_loop, last_top > sum(this_bar), sum(this_bar), last_top)
		# print('all_heights: \n\t','\n\t'.join([str(thing) for thing in all_heights]))
		# print('adj_heights: \n\t','\n\t'.join([str(thing) for thing in adjusted_heights]))




		# print('percent_lists')
					# print('\t\t', len(peak_list_to_plot[file_loop][group_loop]), len(peak_list_to_plot[file_loop][-1]),
					# 	round(percent_lists[-1][-1], 4))
			# print('\t', [round(this_val, 4) for this_val in percent_lists[-1]])
		# print('bar_height_list')
			# for group_loop, this_group in enumerate(this_perc[:-1]):
				# if len(peak_list_to_plot[perc_loop][-1]) > 0:
				# else:
				# 	bar_height = 0
			# print('\t', bar_height_list[-1])
		# print(perc_change_list)
					######################################################################################################
						# bottom_spot = sum([len(thing) for thing in \
						# 	peak_list_to_plot[file_loop][:inner_loop]])/ \
						# 	len(peak_list_to_plot[file_loop][-1])*100
				# if inner_loop == 0:
					# if fig is not None:
					# 	# print('which bar: {}, {}'.format(bar_loop, inner_loop))
					# 	text_under.append(False)
					# 	new_text, loc = fig.text_and_location(ax, current_x, 0, perc_change_list[bar_loop][inner_loop], 
						# color='w', ha='center', \
					# 		va='bottom', weight='semibold', fontsize=fontsize, zorder=10)
					# 	# print('\t', [perc_change_list[bar_loop][inner_loop]], loc[1])
					# 	height = loc[1][1] - loc[1][0]
					# 	if height > bar_height * 0.7:
					# 		# print('\tredo')
					# 		if height < bar_height:
					# 			new_text.set_visible(False)
					# 			new_y = bar_height/2
					# 			new_text, loc = fig.text_and_location(ax, current_x, new_y, 
						# perc_change_list[bar_loop][inner_loop], color='w', ha='center', \
					# 				va='center_baseline', weight='semibold', fontsize=fontsize, zorder=10)
					# 			# print('\t\t', loc)
					# 		else:
					# 			new_text.set_visible(False)
					# 			new_text, loc = fig.text_and_location(ax, current_x, 0, 
									# perc_change_list[bar_loop][inner_loop], color='w', ha='center', \
					# 				va='top', weight='semibold', fontsize=fontsize, zorder=10)
					# 			# print('\t\t', loc)
					# 			text_under[-1] = True

					# 	# print((current_x, bar_height), loc)
					# 	# ax.scatter([loc[0][0], loc[0][1], loc[0][0], loc[0][1]], 
					# 	# 	[loc[1][0], loc[1][0], loc[1][1], loc[1][1]], s=0.3, c='r', zorder=10)
					# else:
					# 	ax.text(current_x, 0, perc_change_list[bar_loop][inner_loop], color='w', ha='center', \
					# 		va='bottom', weight='semibold', fontsize=fontsize, zorder=10)
					# ######################################################################################################
					# bottom_spot = sum([len(thing) for thing in \
						# peak_list_to_plot[file_loop][:inner_loop]])/ \
						# len(peak_list_to_plot[file_loop][-1])*100
					# print('which bar: {}, {}'.format(bar_loop, inner_loop))
					# print('\t', [perc_change_list[bar_loop][inner_loop]], loc[1])
						# print('\tredo')
							# ax.scatter(current_x, new_y, c='C5',zorder=10, s=0.5)
							# ax.scatter([loc[0][0], loc[0][1], loc[0][0], loc[0][1]], 
							# 	[loc[1][0], loc[1][0], loc[1][1], loc[1][1]], s=0.3, c='k', zorder=10)
							# print('\t\t', loc)
						# if bar under is 
							# print('which bar: {}, {}'.format(bar_loop, inner_loop))
							# ax.scatter(current_x, bottom_spot-(this_bar[inner_loop-1]/4), c='C5',zorder=10, s=0.5)
							# ax.scatter([loc[0][0], loc[0][1], loc[0][0], loc[0][1]], 
							# 	[loc[1][0], loc[1][0], loc[1][1], loc[1][1]], s=0.3, c='C5', zorder=10)
							# ax.scatter([loc[0][0], loc[0][1], loc[0][0], loc[0][1]], 
							# 	[loc[1][0], loc[1][0], loc[1][1], loc[1][1]], s=0.3, c='k', zorder=10)
							# print('\t\t', loc)
							# ax.scatter(current_x, bottom_spot-height, c='C5',zorder=10, s=0.5)
							# ax.scatter([loc[0][0], loc[0][1], loc[0][0], loc[0][1]], 
							# 	[loc[1][0], loc[1][0], loc[1][1], loc[1][1]], s=0.3, c='b', zorder=10)
							# ax.scatter([loc[0][0], loc[0][1], loc[0][0], loc[0][1]], 
							# 	[loc[1][0], loc[1][0], loc[1][1], loc[1][1]], s=0.3, c='k', zorder=10)
							# print('\tredo')
						# ax.scatter([loc[0][0], loc[0][1], loc[0][0], loc[0][1]], 
						# 	[loc[1][0], loc[1][0], loc[1][1], loc[1][1]], s=0.3, c='k', zorder=10)
							# print('\t\t', loc)


					# print((current_x, bar_height), loc)
					# ax.scatter([loc[0][0], loc[0][1], loc[0][0], loc[0][1]], 
					# 	[loc[1][0], loc[1][0], loc[1][1], loc[1][1]], s=0.3, c='r', zorder=10)
				# print(bar_loop, sum(this_bar), max_text_height, max_text_height*0.01, y_val)




		# print('peak_list_to_plot')
		# print(len(peak_list_to_plot))
		# print([len(thing) for thing in peak_list_to_plot])
		# print([[len(stuff) for stuff in thing] for thing in peak_list_to_plot])
		# print('sig_list')
		# print(len(sig_list))
		# print([len(thing) for thing in sig_list])
		# print([[len(stuff) for stuff in thing] for thing in sig_list])
		# max_text_height = 0
		# for file_loop in range(len(peak_list_to_plot)):
		# 	this_denom = len(peak_list_to_plot[file_loop][-1])
		# 	if this_denom == 0:
		# 		this_loop_height = 0
		# 	else:
		# 		this_loop_height = sum([len(thing) for thing in \
		# 			peak_list_to_plot[file_loop][:-1]])/len(peak_list_to_plot[file_loop][-1])*100
		# 	if this_loop_height > max_text_height: 
		# 		max_text_height = this_loop_height
		# print(percent_lists)
		# print(bar_height_list)
		# print(perc_change_list)
		# print(len(percent_lists))
		# print(len(bar_height_list))
		# print(len(perc_change_list))

		# raise

		# print(file_loop, group_loop)
		# print('\t',file_loop % len(groups), len(groups)-1)
		# if file_loop % len(groups) != len(groups)-1:
			# dist_from_same = len(groups) - (file_loop % len(groups)) - 1
		# print('\t\tnot ind -1:',dist_from_same, file_loop+dist_from_same)
			# numerator = len(peak_list_to_plot[file_loop][group_loop])/ \
			# 	len(peak_list_to_plot[file_loop][-1])
			# print('num: ', len(peak_list_to_plot[file_loop][group_loop]), len(peak_list_to_plot[file_loop][-1]), 
			# 	len(peak_list_to_plot[file_loop][group_loop])/ \
			# 	len(peak_list_to_plot[file_loop][-1]))
			# numerator = 0
			# print('num: ', 0)
		# print(percent_lists)
		# raise
			# percent_lists.append([])
				# print(file_loop, group_loop)
				# print('\t',file_loop % len(groups), len(groups)-1)
					# print('\t\tnot ind -1:',dist_from_same, file_loop+dist_from_same)
					# if len(peak_list_to_plot[file_loop][-1]) > 0:
					# 	numerator = len(peak_list_to_plot[file_loop][group_loop])/ \
					# 		len(peak_list_to_plot[file_loop][-1])
					# 	print('num: ', len(peak_list_to_plot[file_loop][group_loop]), len(peak_list_to_plot[file_loop][-1]), 
					# 		len(peak_list_to_plot[file_loop][group_loop])/ \
					# 		len(peak_list_to_plot[file_loop][-1]))
					# else:
					# 	numerator = 0
					# 	print('num: ', 0)
					# denominator = \
					# 	len(peak_list_to_plot[file_loop+dist_from_same][group_loop])/ \
					# 	len(peak_list_to_plot[file_loop+dist_from_same][-1])
					# print('denom: ', len(peak_list_to_plot[file_loop+dist_from_same][group_loop]), 
					# 	len(peak_list_to_plot[file_loop+dist_from_same][-1]), 
					# 	len(peak_list_to_plot[file_loop+dist_from_same][group_loop])/ \
					# 	len(peak_list_to_plot[file_loop+dist_from_same][-1]))
						# perc_change = int((((numerator+1)/(denominator+1))*100)-100)
					# if perc_change >= 0:
					# else:
					# 	perc_change = str(perc_change)
						# sig_list format:
							# for sample type
								# for bar (loop_count)
									# for sub_bar (gene_type count)
						# current loops:
							# for file_loop (sample type * loop count)
								# for group loop (gene_type_count)
						# print('\t\tinds: ', current_ind, ind1, ind2, group_loop, sig_list[ind1][ind2][group_loop])
						# print(ind1, ind2, group_loop)

# def intersect_common_beds(split_dir, save_dir, control_files, \
	# cancer_type, group_pattern, file_percent=0):
	# os.makedirs(save_dir, exist_ok=True)
	# group_name = group_pattern
	# use_percent = float(file_percent)
	# file_list = []
	# list_filepath = []
	# for filepath in glob.iglob(os.path.join(split_dir, 'E', '*')):
	# 	if filepath.split('/')[-1].split('_')[0] in control_files:
	# 		file_list.append(filepath)
	# save_spot3 = os.path.join(save_dir, \
	# 	cancer_type+'_control_common.bed')
	# if not os.path.isfile(save_spot3):
	# 	common_bed = []
	# 	file_count = 0
	# 	for filepath in file_list:
	# 		list_filepath.append(filepath)
	# 		file_count += 1
	# 	peak_number = file_count*(int(use_percent)/100)
	# 	var = peak_merge_filecount(list_filepath, keep_new_vals=True)
	# 	var = [[thing[3]]+thing[:3]+thing[5:] for thing in var]
	# 	var_100 = []
	# 	for line in var:
	# 		if len(line) > 1:
	# 			if int(line[0]) >= peak_number:
	# 				var_100.append(line[1:])
	# 	save_spot2 = os.path.join(save_dir, \
	# 		cancer_type+'_common_no_merge.bed')
	# 	write_bed(save_spot2, var_100)
	# 	if len(var_100) > 0:
	# 		write_bed(save_spot3, var_100)
	# 	os.remove(save_spot2)

def comparison_unique_common(comparison1_dict, comparison2_dict):
	for test_group in comparison1_dict.keys():
		for sample_type in comparison1_dict.get(test_group).keys():
			if not os.path.exists(comparison2_dict.get(test_group).get(sample_type)[1]):
				write_bed(comparison2_dict.get(test_group).get(sample_type)[1], subpro_to_list(\
					subprocess.check_output(['subtractBed', '-A', '-a', \
						comparison1_dict.get(test_group).get(sample_type)[0], '-b',  \
						comparison2_dict.get(test_group).get(sample_type)[0]])))


def dir_to_dict(input_dir, directories, type_list=None, group=None):
	output_dict = {}
	file_list = glob.iglob(os.path.join(input_dir, '*'))
	file_list = sorted(file_list)
	for filepath in file_list:
		filename = filepath.split('/')[-1]
		if '_' in filename:
			c_type = filepath.split('/')[-1].split('_')[0]
		else:
			c_type = filepath.split('/')[-1].split('.')[0]
		paths = []
		with open(filepath, 'r', newline='\n') as textfile:
			for line in textfile:
				line = line.rstrip('\n').split('\t')
				paths.append([line[0], float(line[1])])
			paths = sorted(paths, key=operator.itemgetter(1))
		output_dict[c_type] = paths
	return output_dict

def sorted_path_list(input_dict, type_list):
	path_list = []
	dict_paths = [[stuff[0] for stuff in input_dict.get(thing)] for thing in type_list if input_dict.get(thing) is not None]
	for type_loop in range(len(dict_paths)):
		for path_loop in range(len(dict_paths[type_loop])):
			path_list.append(dict_paths[type_loop][path_loop])
	path_list = list(set(path_list))
	path_count = []
	for path_loop in range(len(path_list)):
		path_count.append(sum([path_list[path_loop] in thing for thing in dict_paths]))
	return sorted(zip(path_list, path_count), key=operator.itemgetter(1), reverse=True)

def pick_paths(sorted_list, cutoff_rank, verbose=False):
	output_list = []
	for path_loop in range(len(sorted_list)):
		if any([path_loop < cutoff_rank, sorted_list[path_loop][1] == sorted_list[0][1]]):
			if verbose == True:
				print(sorted_list[path_loop])
			output_list.append(sorted_list[path_loop][0])
	output_list = sorted(output_list)
	return output_list

def slow_ordered_set(input_list):
	output_list = []
	for item in input_list:
		if item not in output_list:
			output_list.append(item)
	return output_list

def get_all_p_vals(all_paths, type_list, first_dict):
	all_p_vals = []
	for path_loop in range(len(all_paths)):
		p_vals = [0 for item in range(len(type_list))]
		for file_loop in range(len(type_list)):
			file_list = first_dict.get(type_list[file_loop])
			if file_list is not None:
				for item_loop in range(len(file_list)):
					if all_paths[path_loop] == file_list[item_loop][0]:
						p_vals[file_loop] = file_list[item_loop][1]
		all_p_vals.append(p_vals)
	all_p_vals = np.asarray(all_p_vals)
	return all_p_vals

def final_prep_broad_sharp(all_p_vals, color_dict, directories, verbose=False):
	xpositions = np.zeros(all_p_vals.shape)
	ypositions = np.zeros(all_p_vals.shape)
	for i in range(all_p_vals.shape[0]):
		for e in range(all_p_vals.shape[1]):
			ypositions[i][e] = i
			xpositions[i][e] = e
			if all_p_vals[i][e] != 0:
				all_p_vals[i][e] = round(-1*math.log(all_p_vals[i][e], 10),2)
	color_array = [color_dict.get(ctype) for ctype in directories]
	
	return all_p_vals, color_array, xpositions, ypositions

def sort_on_p_vals(new_p_vals, all_paths, first=True, verbose=False):
	row_average = []
	if first==True:
		for row_loop in range(len(new_p_vals)):
			row_mean = mean([new_p_vals[row_loop][col_loop] for col_loop in range(len(new_p_vals[row_loop])) if col_loop % 2 == 0])
			if verbose == True:
				print(all_paths[row_loop], [new_p_vals[row_loop][col_loop] for col_loop in range(len(new_p_vals[row_loop])) if col_loop % 2 == 0])
			if all_paths[row_loop] == '':
				break
			row_average.append(round(row_mean,2))
		sorted_array = [(r,x,f) for r,x,f in sorted(zip(row_average,all_paths[:len(row_average)],new_p_vals[:len(row_average)]), key=operator.itemgetter(0), reverse=True)]
		sort_paths = all_paths[:len(row_average)]
		sort_p_vals = new_p_vals[:len(row_average)]
	else:
		record=False
		for row_loop in range(len(new_p_vals)):
			if record == False:
				if all_paths[row_loop] == '':
					record = True
					continue
			else:
				row_mean = mean([new_p_vals[row_loop][col_loop] for col_loop in range(len(new_p_vals[row_loop])) if col_loop % 2 == 1])
				if verbose == True:
					print(all_paths[row_loop])
				row_average.append(round(row_mean,2))
		sort_paths = all_paths[-len(row_average):]
		sort_p_vals = new_p_vals[-len(row_average):]
	sorted_array = [(r,x,f) for r,x,f in sorted(zip(row_average,sort_paths,sort_p_vals), key=operator.itemgetter(0), reverse=True)]
	sorted_array = sorted_array[:10]
	row_average = [thing[0] for thing in sorted_array]
	row_p_vals = [thing[2] for thing in sorted_array]
	row_paths = [thing[1] for thing in sorted_array]

	if verbose == True:
		for thing in sorted_array:
			print(thing)
	return row_p_vals, row_paths

def unique_broad_sharp_data(textfile_dir, sample_types, plot_dir, name_str, \
	color_dict, data_for_figures, verbose=False, cutoff_rank = 5, \
	num_paths_per_group = 10, debug_print=False):

	os.makedirs(plot_dir, exist_ok=True)
	save_spot = os.path.join(plot_dir, name_str+'.png')
	data_fig_save_spot = os.path.join(data_for_figures, name_str)

	if not os.path.exists(save_spot) or not os.path.exists(data_fig_save_spot):
		if len(list(glob.iglob(os.path.join(textfile_dir, '*')))) < 1:
			raise ValueError('No files found in path textfile dir: \
				\n\t{}'.format(textfile_dir))
		broad_dict = dir_to_dict(textfile_dir, sample_types)
		if len(list(broad_dict.keys())) == 0 or \
				len(broad_dict.get(list(broad_dict.keys())[0])) == 0:
			raise ValueError('broad_dict not made correctly: {}'
				.format(str(broad_dict)[:100]))

		sample_types = sorted(sample_types)
		if len(sample_types) == 0:
			raise ValueError('sample_types broke: {}'
				.format(sample_types))

		sorted_broad_paths = sorted_path_list(broad_dict, sample_types)
		if len(sorted_broad_paths) == 0:
			raise ValueError('sample_types broke: {}'
				.format(str(sorted_broad_paths)[:100]))
		
		all_paths = pick_paths(sorted_broad_paths, cutoff_rank, \
			verbose=verbose)
		if len(all_paths) == 0:
			raise ValueError('all_paths broke: {}'
				.format(str(all_paths)[:100]))
		if verbose == True:
			print('')
		if verbose == True:
			print(all_paths)
		all_paths = slow_ordered_set(all_paths)
		if len(all_paths) == 0:
			raise ValueError('all_paths broke: {}'
				.format(str(all_paths)[:100]))

		font = {'size'   : 7}
		mpl.rc('font', **font)

		all_p_vals = get_all_p_vals(all_paths, sample_types, \
			broad_dict)
		if len(all_p_vals) == 0:
			raise ValueError('all_p_vals broke: {}'
				.format(str(all_p_vals)[:100]))

		new_p_vals, color_array, xpositions, ypositions = \
			final_prep_broad_sharp(all_p_vals, color_dict, \
				sample_types, verbose=verbose)
		
		if len(new_p_vals) == 0:
			raise ValueError('new_p_vals broke after final_prep_broad_sharp: {}'
				.format(str(new_p_vals)[:100]))

		new_p_vals, all_paths = sort_on_p_vals(new_p_vals, \
			all_paths, first=True)

		if len(new_p_vals) == 0:
			raise ValueError('new_p_vals broke after sort_on_p_vals: {}'
				.format(str(new_p_vals)[:100]))
		if len(all_paths) == 0:
			raise ValueError('all_paths broke after sort_on_p_vals: {}'
				.format(str(all_paths)[:100]))

		if verbose == True:
			print('')
		new_p_vals = np.asarray(new_p_vals)
		color_array = np.asarray(color_array)
		group1_pattern = ' Broad'

		if new_p_vals.shape[0] >= num_paths_per_group*2:
			xpositions = xpositions[:(num_paths_per_group*2)] 
			ypositions = ypositions[:(num_paths_per_group*2)] 
			new_p_vals = new_p_vals[:(num_paths_per_group*2)] 
		else:
			xpositions = xpositions[:(new_p_vals.shape[0])] 
			ypositions = ypositions[:(new_p_vals.shape[0])] 

		if new_p_vals.shape[0] == 0:
			raise ValueError('new_p_vals array broke: {}'
				.format(str(new_p_vals)[:100]))
		if color_array.shape[0] == 0:
			raise ValueError('color_array broke: {}'
				.format(str(color_array)[:100]))

		unique_broad_sharp_dict = {
			'xpositions': xpositions.tolist(),
			'ypositions': ypositions.tolist(),
			'new_p_vals': new_p_vals.tolist(),
			'color_array': color_array.tolist(),
			'type_list': sample_types,
			'all_paths': all_paths,
			'group1_pattern': group1_pattern,
		}

		with open(data_fig_save_spot, 'w') as textfile:
			json.dump(unique_broad_sharp_dict, textfile)

		fig = easy_fig(figsize=(8, 8), gridsize=(1000,1000), \
			subplot_left=0.01, subplot_right=0.99, subplot_bottom=0.01, \
			subplot_top=0.99)
		fig.add_panel(top_left=(0.5,0.1), bottom_right=(0.9,0.9), \
			x_count=1, y_count=1, subpanel_pad_percent=0)	

		# print(save_spot)
		path_dots_just_plot(fig.axes[0][0], 
			unique_broad_sharp_dict.get('xpositions'), 
			unique_broad_sharp_dict.get('ypositions'), 
			unique_broad_sharp_dict.get('new_p_vals'), 
			unique_broad_sharp_dict.get('color_array'), 
			unique_broad_sharp_dict.get('type_list'), 
			unique_broad_sharp_dict.get('all_paths'), 
			unique_broad_sharp_dict.get('group1_pattern'),
			multiply_s_by=1.0, divider_percent='10%', 
			top_cushion=0.5, bottom_cushion=0.5, min_fontsize=5,
			xlim_pad=1, debug_print=debug_print) 

		plt.savefig(save_spot, dpi=300)
		plt.close()

def path_dots_just_plot(ax, xpositions, ypositions, new_p_vals, color_array, 
	type_list, all_paths, group1_after_pat, group1_before_pat='', multiply_s_by=0.35, 
	divider_percent='10%', top_cushion=0, bottom_cushion=0, fontsize=5, 
	min_fontsize=5, num_p_vals = 4, loop_start = -1, divider_xlim=(-2,4), 
	divider_ylim=(-1,1), p_min = 2, dividerpad=0.02, pval_text_height=0.4, 
	pval_dot_height=-0.3, pval_dot_color='k', xlim_pad=1.5, 
	xticklabel_rotation=60, xticklabel_ha='left', debug_print=False, 
	format_kegg_hallmark_str=False, kegg_hallmark_format_kwargs=None, 
	wrap_amount=27, wrap_labels=False):

	xpositions = np.asarray(xpositions)
	ypositions = np.asarray(ypositions)
	new_p_vals = np.asarray(new_p_vals)
	color_array = np.asarray(color_array)

	avg_pval = []
	for row_loop, row in enumerate(new_p_vals):
		avg_pval.append(mean(row))
	everything = sorted(list(zip(avg_pval, new_p_vals, all_paths)), \
		key=operator.itemgetter(0), reverse = True)
	try:
		avg_pval, new_p_vals, all_paths = map(list,zip(*everything))
	except:
		lens = [len(avg_pval), len(new_p_vals), len(all_paths)]
		print('input lengths: avg_pval {}, new_p_vals {}, all_paths {}'.format(*lens))
		if lens[0] < mean(lens):
			print('avg_pval: {}'.format(avg_pval))
		if lens[1] < mean(lens):
			print('new_p_vals: {}'.format(new_p_vals))
		if lens[2] < mean(lens):
			print('all_paths: {}'.format(all_paths))
		raise

	xpositions = np.asarray(xpositions)
	ypositions = np.asarray(ypositions)
	new_p_vals = np.asarray(new_p_vals)
	color_array = np.asarray(color_array)

	pos_ind = 0
	ytick_list = []
	ylabel_list = []

	if format_kegg_hallmark_str:
		if kegg_hallmark_format_kwargs is None:
			kegg_hallmark_format_kwargs = {}
		# format_pathname_kegg_hallmark()

	no_rows = not any([not any([pval == 0 for pval in p_val_row]) for p_val_row in new_p_vals])
	# print(no_rows, [not any([pval == 0 for pval in p_val_row]) for p_val_row in new_p_vals])
	for row_loop in range(xpositions.shape[0]):
		if not any([pval == 0 for pval in new_p_vals[row_loop]]) or no_rows:
			ax.scatter(xpositions[pos_ind], ypositions[pos_ind], s=new_p_vals[row_loop]*multiply_s_by, 
				c=color_array)
			for col_num, col_val in enumerate(new_p_vals[row_loop]):
				# print(xpositions[row_loop][col_num], ypositions[row_loop][col_num],
				# 	str(round(col_val, 2)))
				if debug_print:
					ax.text(xpositions[pos_ind][col_num], ypositions[pos_ind][col_num],
						str(round(col_val, 2)))
			if debug_print:
				ax.text(xpositions[pos_ind][-1]+1, ypositions[pos_ind][-1],
						str(round(avg_pval[row_loop], 2)))
			ytick_list.append(pos_ind)
			add_str = all_paths[row_loop]
			if format_kegg_hallmark_str:
				# print('format')
				add_str = format_pathname_kegg_hallmark(add_str, *kegg_hallmark_format_kwargs)
			if wrap_labels:
				add_str = "\n".join(sep_based_wrap(add_str, wrap_amount))
				# add_str = "\n".join(wrap(add_str, wrap_amount))
			# print(add_str)
			ylabel_list.append(add_str)
			pos_ind += 1

	cax = easy_fig.format_panel(ax, 
		plot_divider_dict={
			'append_axes': 
				{'position': "bottom", 'size': divider_percent, 'pad': dividerpad},
			'hide_xticks': True,
			'hide_yticks': True,
			'hide_xticklabels': True,
			'hide_yticklabels': True,
			'xlim': divider_xlim,
			'ylim': divider_ylim,
		},
	)	

	p_max = max([max(thing) for thing in new_p_vals])
	if p_min is None:
		p_min = min([min(thing) for thing in new_p_vals])

	p_sizes = []
	if fontsize < min_fontsize:
		fontsize = min_fontsize
	for p_loop in range(loop_start,num_p_vals):
		p_sizes.append(int(p_min+((p_max-p_min)*((p_loop+1)/num_p_vals)))*multiply_s_by)
		cax.text(p_loop, pval_text_height, str(int(p_min+((p_max-p_min)*((p_loop+1)/num_p_vals)))), \
			ha='center', va='center', fontsize=fontsize)
	cax.scatter(range(loop_start,num_p_vals), [pval_dot_height for thing in \
		range(loop_start,num_p_vals)], s=p_sizes, c=[pval_dot_color for thing in \
		range(loop_start,num_p_vals)])
	xtick_spots = list(range(0,new_p_vals.shape[1]))



	labels_list = []
	for type_loop, this_type in enumerate(type_list):
		add_str = group1_before_pat+this_type
		labels_list.append(add_str)
	# print('\t', labels_list)

	if top_cushion != 0 or bottom_cushion != 0:
		bottom, top = ax.get_ylim()
		if top > bottom:
			top += bottom_cushion
			bottom -= top_cushion
		else:
			top -= top_cushion
			bottom += bottom_cushion
	else:
		bottom, top = ax.get_ylim()

	easy_fig.format_panel(ax, 
		xlim=(xtick_spots[0] - xlim_pad, xtick_spots[-1] + xlim_pad),
		xtick_dict={'ticks': xtick_spots},
		xticklabel_dict={'labels': labels_list, 'rotation': xticklabel_rotation, \
			'ha': xticklabel_ha, 'fontsize': fontsize
		},
		ytick_dict={'ticks': ytick_list},
		# ytick_dict={'ticks': range(xpositions.shape[0])},
		yticklabel_dict={'labels': ylabel_list, 'fontsize': fontsize
		# yticklabel_dict={'labels': all_paths, 'fontsize': fontsize
		},
		xtick_top=True,
		invert_y=True,
		ylim=(bottom, top)
	)

def general_overlap_check(window_index, item, search_list, output_list, keep_over_or_nonover='nonover', 
	chr_index=0, peak_min_index=1, peak_max_index=2, indices_to_keep=None, overlap_range = 0):
	finished_list = False
	line_in_reference = False
	while True:
		peak_min = int(item[peak_min_index])
		peak_max = int(item[peak_max_index])
		reference_min = int(search_list[window_index][peak_min_index]) - overlap_range
		reference_max = int(search_list[window_index][peak_max_index]) + overlap_range
		if item[chr_index] == search_list[window_index][0]:
			if ((peak_min <= reference_max) and (peak_max >= reference_min)):
				line_in_reference = True
				window_index += 1
			elif peak_min > reference_max:
				window_index += 1
			elif peak_max < reference_min:
				if keep_over_or_nonover == 'nonover':
					if line_in_reference == False:
						output_list.append(item)
				elif keep_over_or_nonover == 'over':
					if line_in_reference == True:
						output_list.append(item)
				else:
					raise ValueError('missed something')
				break
			else:
				raise ValueError('missed something')
		elif item[chr_index] > search_list[window_index][0]:
			window_index += 1
		elif item[chr_index] < search_list[window_index][0]:
			if keep_over_or_nonover == 'nonover':
				if line_in_reference == False:
					output_list.append(item)
			elif keep_over_or_nonover == 'over':
				if line_in_reference == True:
					output_list.append(item)
			else:
				raise ValueError('missed something')
			break
		else:
			raise ValueError('missed something')
		if (window_index >= len(search_list)) or (len(search_list[window_index]) < 2):
			finished_list = True
			break
	return window_index, output_list, finished_list

def merge_peak_genes(peak_list, gene_col=4):
	output_list = []
	current_peak = None
	for peak_loop, peak in enumerate(peak_list):
		peak[1] = int(peak[1])
		peak[2] = int(peak[2])
		if current_peak is None:
			current_peak = peak[:3]
			current_peak.append([peak[gene_col]])
		else:
			if peak[0] == current_peak[0] and peak[1] < current_peak[2]:
				current_peak[3].append(peak[gene_col])
				if peak[2] > current_peak[2]:
					current_peak[2] = peak[2]
			else:
				output_list.append(current_peak)
				current_peak = peak[:3]
				current_peak.append([peak[gene_col]])

	return output_list

def compare_peak_list_col_values(list1, list2, keep_over_or_nonover='nonover', 
	chr_index=0, peak_min_index=1, peak_max_index=2, indices_to_keep=None, overlap_range = 0, 
	return_unique=False):
	# finished_list = False
	index1 = 0
	index2 = 0
	output_list = []
	line_in_reference = False

	while True:
		# if index2 >= len(list2) or index1 >= len(list1):
		if index2 < len(list2):
			peak2 = list2[index2]
			peak2_min = int(peak2[peak_min_index]) - overlap_range
			peak2_max = int(peak2[peak_max_index]) + overlap_range
			peak2_left = [peak2[chr_index], peak2_min, peak2_max]
			if len(peak2) > 3:
				peak2_right = peak2[3:]
			else:
				peak2_right = [None]

			# output_list.append(peak1_left + peak1_right)
			# index1 += 1
			add2 = True
		else:
			add2 = False
			
		if index1 < len(list1):
			peak1 = list1[index1]
			peak1_min = int(peak1[peak_min_index])
			peak1_max = int(peak1[peak_max_index])
			peak1_left = [peak1[chr_index], peak1_min, peak1_max]
			if len(peak1) > 3:
				peak1_right = peak1[3:]
			else:
				peak1_right = [None]

			# output_list.append(peak2_left + peak2_right)
			# index2 += 1
			add1 = True
		else:
			add1 = False
		if index2 >= len(list2) and index1 >= len(list1):
			break
		elif index2 < len(list2) and index1 < len(list1):
			if peak1[chr_index] == peak2[chr_index]:
				if ((peak1_min <= peak2_max) and (peak1_max >= peak2_min)):
					output_list.append(peak1_left + peak1_right + peak2_right)
					# line_in_reference = True
					index2 += 1
					index1 += 1
				elif peak1_min > peak2_max:
					output_list.append(peak2_left + [[None]] + peak2_right)
					index2 += 1
				elif peak1_max < peak2_min:
					output_list.append(peak1_left + peak1_right + [[None]])
					index1 += 1
				else:
					raise ValueError('missed something')
			elif peak1[chr_index] > peak2[chr_index]:
				output_list.append(peak2_left + [[None]] + peak2_right)
				index2 += 1
			elif peak1[chr_index] < peak2[chr_index]:
				output_list.append(peak1_left + peak1_right + [[None]])
				index1 += 1
			else:
				raise ValueError('missed something')
		elif add2:
			output_list.append(peak2_left + [[None]] + peak2_right)
			index2 += 1
		elif add1:
			output_list.append(peak1_left + peak1_right + [[None]])
			index1 += 1
	if return_unique:
		unique_list = []
		for line_loop, line in enumerate(output_list):
			try:
				unique1 = [gene for gene in line[3] if gene not in line[4]]
				unique2 = [gene for gene in line[4] if gene not in line[3]]

				unique_list.append(line[:3] + [unique1] + [unique2])
			except:
				print(line, line_loop, len(output_list))
				print(list1[0], list2[0])
				raise 
		return output_list, unique_list

		# old code
			# if index2 >= len(list2):
			# 	peak1 = list1[index1]
			# 	peak1_min = int(peak1[peak_min_index])
			# 	peak1_max = int(peak1[peak_max_index])
			# 	peak1_left = [peak1[chr_index], peak1_min, peak1_max]
			# 	if len(peak1) > 3:
			# 		peak1_right = peak1[3:]
			# 	else:
			# 		peak1_right = [None]

			# 	output_list.append(peak1_left + peak1_right)
			# 	index1 += 1
				
			# if index1 >= len(list1):
			# 	peak2 = list2[index2]
			# 	peak2_min = int(peak2[peak_min_index]) - overlap_range
			# 	peak2_max = int(peak2[peak_max_index]) + overlap_range
			# 	peak2_left = [peak2[chr_index], peak2_min, peak2_max]
			# 	if len(peak2) > 3:
			# 		peak2_right = peak2[3:]
			# 	else:
			# 		peak2_right = [None]

			# 	output_list.append(peak2_left + peak2_right)
			# 	index2 += 1

				# else:
				# 	peak2_right = [None]
				# if keep_over_or_nonover == 'nonover':
				# 	if line_in_reference == False:
				# elif keep_over_or_nonover == 'over':
				# 	if line_in_reference == True:
				# 		output_list.append(peak1[:3] + peak1_right + peak2_right)
				# else:
				# 	raise ValueError('missed something')
				# break
			
			# elif peak1[chr_index] < peak2[chr_index]:
			# 	if keep_over_or_nonover == 'nonover':
			# 		if line_in_reference == False:
			# 			output_list.append(peak1)
			# 	elif keep_over_or_nonover == 'over':
			# 		if line_in_reference == True:
			# 			output_list.append(peak1)
			# 	else:
			# 		raise ValueError('missed something')
			# 	break
			# if (index2 >= len(list2)) or (len(peak2) < 2):
			# 	# finished_list = True
			# 	break
	else:
		return output_list

def avg_peak_sizes_mp_wrapper(save_path, unique_common_list, merged_file_list):
	os.makedirs(os.path.dirname(save_path), exist_ok=True)

	ref_peak_list = []
	with open(unique_common_list, 'r') as textfile:
		for line in textfile:
			line = line.rstrip('\n').split('\t')
			ref_peak_list.append(line)

	merge_common_peaks = \
		peak_merge_filecount(merged_file_list,
			special_in_file_handling={3:'avg', 4:'avg'})
	# print(merge_common_peaks[0])
	merge_common_peaks = [[peak[3]]+peak[:3]+peak[4:] for \
		peak in merge_common_peaks]
	# print(merge_common_peaks[0])
	merge_common_peaks = [peak[1:7]+peak[8:] for peak in \
		merge_common_peaks if len(peak) > 1]
	# print(merge_common_peaks[0])
	# raise

	window_index = 0
	output_list = []
	for line in merge_common_peaks:
		if len(line) < 2:
			continue
		window_index, output_list, finished_list = \
			general_overlap_check(window_index, line, \
				ref_peak_list, output_list, \
				keep_over_or_nonover='over', chr_index=0, 
			peak_min_index=1, peak_max_index=2, overlap_range=0)

		if finished_list == True:
			break
	write_bed(save_path,output_list)

def get_peak_avg_sizes(merged_dict, compar_dict, mp_workers=None): #, \
	merged_test_group = list(merged_dict.keys())[0]
	compar_test_group = list(compar_dict.keys())[0]
	for sample_type in compar_dict.get(compar_test_group).keys():
		if merged_dict.get(merged_test_group).get(sample_type) is not None and \
			compar_dict.get(compar_test_group).get(sample_type) is not None:
			merged_file_list = [filepath[1] for filepath in \
				merged_dict.get(merged_test_group).get(sample_type)]
			unique_common_list = \
				compar_dict.get(compar_test_group).get(sample_type)[0]
			save_path = compar_dict.get(compar_test_group).get(sample_type)[1]
			if not os.path.isfile(save_path):
				if mp_workers is None:
					avg_peak_sizes_mp_wrapper(
						save_path, unique_common_list, merged_file_list)
				else:
					mp_workers.add_job(avg_peak_sizes_mp_wrapper, 
						(save_path, unique_common_list, merged_file_list), 
						job_group='avg_peaksize')

def old_get_peak_avg_sizes(merged_dir, ref_dir, save_dir, \
	cancer_type_controls, cancer_type, common_vals, control_broad):
	os.makedirs(save_dir, exist_ok=True)
	if common_vals == True:
		save_spot = os.path.join(save_dir, \
			cancer_type+'_ref_vals.bed')
	else:
		save_spot = os.path.join(save_dir, \
			cancer_type+'_compare_vals.bed')
	if not os.path.isfile(save_spot):
		for filepath in glob.iglob(os.path.join(ref_dir, '*')):
			if cancer_type == filepath.split('/')[-1].split('_')[0]:
				ref_file = filepath
		ref_peaks = []
		with open(ref_file, 'r') as textfile:
			for line in textfile:
				line = line.rstrip('\n').split('\t')
				ref_peaks.append(line)
		merged_beds = []
		if (control_broad == True and common_vals == True) or \
			(control_broad == False and common_vals == False):
			for filepath in glob.iglob(\
					os.path.join(merged_dir, 'E', '*')):
				if filepath.split('/')[-1].split('_')[0] in \
					cancer_type_controls:
					merged_beds.append(filepath)
		else:
			if cancer_type != 'all':
				merged_beds = glob.iglob(os.path.join(merged_dir, \
					cancer_type, '*'))
			else:
				merged_beds = [thing for thing in \
				glob.iglob(os.path.join(merged_dir, '**', '*')) if \
				thing.split('/')[-1][0] != 'E']
		var = peak_merge_filecount(merged_beds, keep_new_vals=True)
		var = [[thing[3]]+thing[:3]+thing[5:] for thing in var]
		var_100 = []
		for line in var:
			if len(line) > 1:
				var_100.append(line[1:])
		i = 0
		start_time = None
		to_show = None
		new_file = []
		window_index = 0
		output_list = []
		for line in var_100:
			if len(line) < 2:
				continue
			window_index, output_list, finished_list = \
				general_overlap_check(window_index, line, \
					ref_peaks, output_list, \
					keep_over_or_nonover='over', chr_index=0, 
				peak_min_index=1, peak_max_index=2, overlap_range=0)
			how_often = 100
			if i %how_often == 0:
				start_time, to_show = progbar(i, len(var_100), \
					start_time, to_show, how_often=how_often)
			i += 1
			if finished_list == True:
				break
		write_bed(save_spot,output_list)

def peak_size_change_beds(ref_dict, other_dict, mean_len_ind=3, mean_dens_ind=4):
	def check_compar_overlap(ref_peaks, compar_peaks, ref_ind, compar_ind):
		def match_this_compar(ref_peaks, compar_peaks, ref_ind, compar_ind):
			# this is very similar to check_compar_overlap, but returns a
				# different output: basically, it orders its own matching peaks
				# by how much they match and checks if those peaks match with it
				# if one other than the peak that originally matched it 
				# matches first, then it will return False, and the previous
				# peak will need to check the next highest matching peak
			# if no better peak matches to this one before getting to the peak
				# that originally matched this one, this returns True ->
				# this peak matches with the one that originally matched it
			# this function is recursive, so if a comparison peak matches best
				# with a different ref peak, that in turn matches best with another
				# comparison peak, this will recursively follow that cycle down
				# and eventually match every peak with the best matching partner
				# that doesn't have a better partner
			overlapping_ref_peaks = []
			ref_add = 0
			overlap_amounts = []
			# grabbing matching peak lists
			while True:
				if ref_ind + ref_add >= len(ref_peaks):
					break
				if compar_peaks[compar_ind][0] < ref_peaks[ref_ind + ref_add][0]:
					break
				elif compar_peaks[compar_ind][2] < ref_peaks[ref_ind + ref_add][1]:
					break
				if ref_peaks[ref_ind + ref_add][1] <= compar_peaks[compar_ind][2]:
					overlapping_ref_peaks.append(ref_peaks[ref_ind + ref_add])
					if ref_add == 0:
						overlapping_ref_peaks[-1].append(1)
					else:
						overlapping_ref_peaks[-1].append(0)
					if ref_peaks[ref_ind + ref_add][2] < compar_peaks[compar_ind][2]:
						overlap_amounts.append(ref_peaks[ref_ind + ref_add][2] - 
							ref_peaks[ref_ind + ref_add][1])
					else:
						overlap_amounts.append(compar_peaks[compar_ind][2] - 
							ref_peaks[ref_ind + ref_add][1])
					ref_add += 1
				else:
					raise ValueError('idk?')

			# sorting the list to put the best matching peak is at the top
			overlap_list = sorted(list(zip(overlap_amounts, overlapping_ref_peaks, 
				list(range(ref_add)))), reverse=True)

			# looping through the list to see if the peak that originally matched
				# this one is at the top, or otherwise to see if any better matching
				# peak is avaiable (aka, check if the better matching peak matches
				# to this one, or if it has a better partner to patch to)
			for comp_peak in overlap_list:
				# if we loop to the peak that matched this peak, return True
				if comp_peak[1][-1] == 1:
					return True
				# if this peak matches to another peak, return False
					# the peak that matched this peak will have to continue looping
					# through its overlapping peaks
				elif match_this_compar(compar_peaks, ref_peaks, compar_ind, 
					ref_ind + comp_peak[2]):
					return False

		overlapping_compar_peaks = []
		compar_add = 0
		overlap_amounts = []
		# first grab the list of peaks that overlap with the ref peak
		while True:
			if compar_ind + compar_add >= len(compar_peaks):
				break
			if ref_peaks[ref_ind][0] < compar_peaks[compar_ind + compar_add][0]:
				break
			elif ref_peaks[ref_ind][2] < compar_peaks[compar_ind + compar_add][1]:
				break
			if compar_peaks[compar_ind + compar_add][1] <= ref_peaks[ref_ind][2]:
				overlapping_compar_peaks.append(compar_peaks[compar_ind + compar_add])
				if compar_peaks[compar_ind + compar_add][2] < ref_peaks[ref_ind][2]:
					overlap_amounts.append(compar_peaks[compar_ind + compar_add][2] - 
						compar_peaks[compar_ind + compar_add][1])
				else:
					overlap_amounts.append(ref_peaks[ref_ind][2] - 
						compar_peaks[compar_ind + compar_add][1])
				compar_add += 1
			else:
				raise ValueError('idk?')
		# sorting the list to put the best matching peak is at the top
		overlap_list = sorted(list(zip(overlap_amounts, overlapping_compar_peaks, 
				list(range(compar_add)))), reverse=True)

		# now loop through the overlap peak list, starting from the
			# peak with the highest overlap -> lowest overlap
			# want to check if the comparison peak matches better
				# with another ref peak before committing to the match
		for comp_peak in overlap_list:
			# this recursively checks the overlaps to see if the current
				# compar peak should be matched to the current ref peak
				# or if it matches another ref peak better
			if match_this_compar(ref_peaks, compar_peaks, ref_ind, 
				compar_ind + comp_peak[2]):
				# returning the list for this peak to append for the output bed file
				return ref_peaks[ref_ind][:3]+ \
					[str(round(float(\
						compar_peaks[compar_ind + comp_peak[2]][3])/\
						float(ref_peaks[ref_ind][3]), 4)), \
					round(float(\
						compar_peaks[compar_ind + comp_peak[2]][4])/\
						float(ref_peaks[ref_ind][4]),3), \
					float(ref_peaks[ref_ind][3]), \
					compar_peaks[compar_ind + comp_peak[2]][3], \
					ref_peaks[ref_ind][4], \
					compar_peaks[compar_ind + comp_peak[2]][4]]
				# columns of output bed:
					# 0-2: peak location
					# 3: compar length over ref length
					# 4: compar density over ref density
					# 5: ref length
					# 6: compar length
					# 7: ref density
					# 8: compar density
		return None
	
	# this function overlaps peaks b/w a "reference" peak list (which picks peak
		# locations for analysis) and a "comparison" peak list
	# the returned bed will have a line for each peak, including the lengths and
		# density values for each peak in the reference and comparison groups
		# this output is used downstream to filter for lengthening/shortening peaks

	# these should always have 1 test group, and they might not be the same
		# test groups are "test" or "control"
	ref_test_group = list(ref_dict.keys())[0]
	other_test_group = list(other_dict.keys())[0]
	for sample_type in ref_dict.get(ref_test_group).keys():
		# only run for sample types that have ref and compar inputs
			# need this in case test and control tissue types don't perfectly
			# overlap. eg, we have pancreatic cancer cell lines but don't have
			# noncancer pancreatic samples
		if other_dict.get(other_test_group).get(sample_type) is not None and \
			not os.path.isfile(ref_dict.get(ref_test_group).get(sample_type)[1]):
			ref_file = ref_dict.get(ref_test_group).get(sample_type)[0]
			compar_file = other_dict.get(other_test_group).get(sample_type)[0]
			# the save_spot should always be in the same test group as the
				# reference files, eg, if reference is test, then output is
				# peak length change from test to control
			save_spot = ref_dict.get(ref_test_group).get(sample_type)[1]
			if not os.path.isfile(save_spot):
				os.makedirs(os.path.dirname(ref_dict.get(ref_test_group)\
					.get(sample_type)[1]),
					exist_ok=True)
				# gonna load both peak lists for a sample type
				compar_peaks = []
				with open(compar_file, 'r') as textfile:
					for peak_num, compar_peak in enumerate(textfile):
						compar_peak = compar_peak.rstrip('\n').split('\t')
						if len(compar_peak) > 1:
							compar_peaks.append(
								[compar_peak[0]] +
								[int(compar_peak[1])] + [int(compar_peak[2])] + 
								[float(compar_peak[mean_len_ind])]+\
								[float(compar_peak[mean_dens_ind])])
				ref_peaks = []
				with open(ref_file, 'r') as textfile:
					for peak_num, ref_peak in enumerate(textfile):
						ref_peak = ref_peak.rstrip('\n').split('\t')
						if len(ref_peak) > 1:
							ref_peaks.append(
								[ref_peak[0]] +
								[int(ref_peak[1])] + [int(ref_peak[2])] + 
								[float(ref_peak[mean_len_ind])]+\
								[float(ref_peak[mean_dens_ind])])
				# make sure peak lists are sorted
				ref_peaks = sorted(ref_peaks, key=operator.itemgetter(0,1,2))
				compar_peaks = sorted(compar_peaks, key=operator.itemgetter(0,1,2))

				ref_ind = 0
				compar_ind = 0
				matched_list = []

				# loop through the ref peak list (peaks from ref file)
					# in all cases, want peaks to be matched to the peak
					# with the most overlap with them (if multiple peaks overlap)
					# this requires a recursive overlap function
				while True:
					# break if we reach the end of either peak list
					if ref_ind >= len(ref_peaks) or compar_ind >= len(compar_peaks):
						break
					# if chromosome numbers don't match or if the ref peak
						# is later than the comparison peak -> go to the next
						# comparison peak and repeat the loop
						# this is the only place where the comparison peak iterates
						# so the ref peak should always either overlap or be later
					if ref_peaks[ref_ind][0] > compar_peaks[compar_ind][0] or \
						(ref_peaks[ref_ind][0] == compar_peaks[compar_ind][0] and \
							ref_peaks[ref_ind][1] > compar_peaks[compar_ind][2]):
						compar_ind += 1
						continue
					next_line = check_compar_overlap(ref_peaks, compar_peaks, 
						ref_ind, compar_ind)
					# iterate to check the next reference peak
					ref_ind += 1
					# check_compar_overlap returns None if no matching peak is
						# chosen. Don't want to add "None" lines to the bed file
					if next_line is None:
						continue
					else:
						matched_list.append('\t'.join([str(col) for col in next_line]))

				with open(save_spot, 'w') as writefile:
					writefile.write('\n'.join(matched_list))

def old_peak_size_change_beds(ref_dict, other_dict, mean_len_ind=3, mean_dens_ind=5):
	ref_test_group = list(ref_dict.keys())[0]
	other_test_group = list(other_dict.keys())[0]
	for sample_type in ref_dict.get(ref_test_group).keys():
		if other_dict.get(other_test_group).get(sample_type) is not None and \
			not os.path.isfile(ref_dict.get(ref_test_group).get(sample_type)[1]):
			ref_file = ref_dict.get(ref_test_group).get(sample_type)[0]
			compare_file = other_dict.get(other_test_group).get(sample_type)[0]
			save_spot = ref_dict.get(ref_test_group).get(sample_type)[1]
			if not os.path.isfile(save_spot):
				os.makedirs(os.path.dirname(ref_dict.get(ref_test_group)\
					.get(sample_type)[1]),
					exist_ok=True)
				compare_vals = []
				with open(compare_file, 'r') as textfile:
					for compar_peak in textfile:
						compar_peak = compar_peak.rstrip('\n').split('\t')
						if len(compar_peak) > 1:
							compare_vals.append(compar_peak[:3]+\
								[compar_peak[mean_len_ind]]+\
								[compar_peak[mean_dens_ind]])
				ref_vals = []
				ref_means = []
				no_compare = []
				window_index = 0
				rounds = 0
				write_str = ''
				with open(ref_file, 'r') as textfile:
					for ref_peak in textfile:
						ref_peak = ref_peak.rstrip('\n').split('\t')
						if len(ref_peak) > 1:
							ref_peak_location = ref_peak[:3]
							ref_peak_mean_len = ref_peak[mean_len_ind]
							ref_peak_mean_dens = ref_peak[mean_dens_ind]
							if window_index >= len(compare_vals):
								break
							while True:
								if window_index >= len(compare_vals):
									break

								# if peaks on same chromosome
								if (compare_vals[window_index][0] == \
									ref_peak_location[0]):
									# if peaks overlap
									if (int(compare_vals[window_index][2]) >= \
										int(ref_peak_location[1]) and int(\
										compare_vals[window_index][1]) <= \
										int(ref_peak_location[2])):
										row_list = ref_peak_location+ \
											[str(round(float(\
												compare_vals[window_index][3])/\
												float(ref_peak_mean_len), 4)), \
											round(float(\
												compare_vals[window_index][4])/\
												float(ref_peak_mean_dens),3), \
											float(ref_peak_mean_len), \
											compare_vals[window_index][3], \
											ref_peak_mean_dens, \
											compare_vals[window_index][4]]
										write_str +='\t'.join([str(column) for \
											column in row_list])+'\n'
										window_index += 1

									# if reference peak later than compar
									elif (int(compare_vals[window_index][2]) < \
										int(ref_peak_location[1])):
										window_index += 1

									# if compar peak later than ref
									elif (int(compare_vals[window_index][1]) > \
										int(ref_peak_location[2])):
										break
									else:
										raise ValueError('missed something')

								else:
									# if reference peak on later chromosome
									if (compare_vals[window_index][0] <= \
										ref_peak_location[0]):
										window_index += 1
									# if compar peak on later chromosome
									elif (compare_vals[window_index][0] >= \
										ref_peak_location[0]):
										break
									else:
										raise ValueError('missed something')

				with open(save_spot, 'w') as writefile:
					writefile.write(write_str.rstrip('\n'))

def old_peak_size_change_beds(ref_dir, compare_dir, cancer_type, \
	save_dir, control_broad):
	all_dir = os.path.join(save_dir, 'all_dir')
	os.makedirs(all_dir, exist_ok=True)
	save_spot_all = os.path.join(all_dir, cancer_type+'_all_common.bed')
	if (not os.path.isfile(save_spot_all)):
		for filepath in glob.iglob(os.path.join(ref_dir, '*')):
			if cancer_type == filepath.split('/')[-1].split('_')[0]:
				ref_file = filepath
		for filepath in glob.iglob(os.path.join(compare_dir, '*')):
			if cancer_type == filepath.split('/')[-1].split('_')[0]:
				compare_file = filepath
		compare_vals = []
		with open(compare_file, 'r') as textfile:
			for line in textfile:
				line = line.rstrip('\n').split('\t')
				if len(line) > 1:
					line_vals = line[4]
					line_mean = line[3]
					line_dens = line[5]
					dens_vals = line[7]
					compare_vals.append([line[:3], line_vals, \
						[line_mean], [line_dens], dens_vals])
		if cancer_type == 'all':
			with open(save_compare_list, 'w') as textfile:
				json.dump(compare_vals, textfile)
		ref_vals = []
		ref_means = []
		no_compare = []
		window_index = 0
		with open(ref_file, 'r') as textfile:
			with open(save_spot_all, 'w') as writefile:
				for line in textfile:
					line = line.rstrip('\n').split('\t')
					if len(line) > 1:
						line_vals = line[4]
						line_mean = line[3]
						line_dens = line[5]
						dens_vals = line[7]
						if (compare_vals[window_index][0][0] == \
							line[0]) and (int(\
								compare_vals[window_index][0][2]) >= \
								int(line[1]) and int(\
								compare_vals[window_index][0][1]) <= \
								int(line[2])):
							print('\t'.join([str(thing) for \
								thing in [line[0], line[1], line[2], \
								str(round(float(\
									compare_vals[window_index][2][0])/\
									float(line_mean), 4)), int(float(\
									line_mean)), \
								str(int(round(float(\
									compare_vals[window_index][2][0]) - \
									float(line_mean), 0))), \
									round(float(\
									compare_vals[window_index][3][0])/\
									float(line_dens),3), \
								compare_vals[window_index][3][0], \
									line[5], \
									compare_vals[window_index][4], \
									line[7]]]), file=writefile)
							window_index += 1
							if window_index >= len(compare_vals):
								break
						elif (compare_vals[window_index][0][0] < \
							line[0]) or \
							(int(compare_vals[window_index][0][2]) < \
							int(line[1])):
							window_index += 1
							if window_index >= len(compare_vals):
								break
						elif (compare_vals[window_index][0][0] > \
							line[0]) or \
							(int(compare_vals[window_index][0][1]) > \
							int(line[2])):
							continue
						else:
							raise ValueError('missed something')

# this is no longer used - it was supposed to come up with median peak length for kde plots, 
	# but it gave different outputs for different kde bandwidths
def kde_median(input_list):
	my_list = input_list.copy()
	med_pileup = sum(my_list)/2
	for item_loop in range(len(my_list)):
		med_pileup -= my_list[item_loop]
		if med_pileup <= 0:
			return item_loop

def plot_kde_size_change(file_dict, plot_dir, data_for_figures, name_str, \
	color_dict, value_indices, len_or_dens, \
	ratio_x_min_max=None, abs_x_min_max=None, dens_cutoff=None, 
	dens_cutoff_ind=None):
	# args needed:
		# file_dict for files to read
		# plot_dir for saving the plot (then set save_spot)
		# data_for_figures path for figure
		# need index for lengths (can re-run for dens?)
		# plot name?
		# color dict
		# control broad?

	ratio_string = 'ratio_{}_change_kde'\
		.format(len_or_dens)
	abs_string = 'abs_{}_change_kde'\
		.format(len_or_dens)
	ratio_size_change_filepath = os.path.join(plot_dir, name_str, ratio_string+'.png')
	abs_size_change_filepath = os.path.join(plot_dir, name_str, abs_string+'.png')
	kde_size_change_dir = os.path.join(\
		data_for_figures, 'kde_size_change')
	ratio_dict_save_path = os.path.join(kde_size_change_dir, \
				name_str+'_'+ratio_string)
	abs_dict_save_path = os.path.join(kde_size_change_dir, \
				name_str+'_'+abs_string)
	os.makedirs(os.path.join(plot_dir, name_str), exist_ok=True)

	if len_or_dens == 'len':
		longname = 'Lengthen'
		change_cutoff=2000
		ratio_name = 'Log2 Ratio Length Change'
		abs_name = 'Absolute Length Change'
	else:
		longname = 'Deepen'
		change_cutoff=50
		ratio_name = 'Log2 Ratio Density Change'
		abs_name = 'Absolute Density Change'

	if not os.path.isfile(ratio_size_change_filepath) or \
		not os.path.isfile(ratio_dict_save_path) or \
		not os.path.isfile(abs_dict_save_path) or \
		not os.path.isfile(abs_size_change_filepath):
		ratio_change_list = []
		abs_change_list = []
		for test_group in file_dict.keys():
			for sample_type in file_dict.get(test_group).keys():
				ratio_change_list.append([sample_type, []])
				abs_change_list.append([sample_type, []])
				with open(file_dict.get(test_group).get(sample_type)[0], 'r') as textfile:
					for peak in textfile:
						peak = peak.rstrip('\n').split('\t')
						if len(peak) > 1:
							if dens_cutoff is not None:
								dens_ratio = float(peak[dens_cutoff_ind[1]])/\
									float(peak[dens_cutoff_ind[0]])
								if dens_cutoff.get('less_than') is not None:
									if dens_ratio <= float(dens_cutoff.get('less_than')):
										less_than = True
									else:
										less_than = False
								else:
									less_than = True
								if dens_cutoff.get('greater') is not None:
									if dens_ratio <= float(dens_cutoff.get('greater')):
										greater = True
									else:
										greater = False
								else:
									greater = True
								if less_than and greater:
									add_peak = True
								else:
									add_peak = False
							else:
								add_peak = True
							if add_peak is True:
								ratio_change_list[-1][-1].append(\
									math.log2(float(peak[value_indices[1]])/
										float(peak[value_indices[0]])))
								abs_change_list[-1][-1].append(float(peak[value_indices[1]]) - 
										float(peak[value_indices[0]]))

		if not os.path.isfile(ratio_size_change_filepath) or \
			not os.path.isfile(ratio_dict_save_path):
			if ratio_x_min_max == None:
				max_val = max([max(thing[1]) for thing in ratio_change_list])
				min_val = min([min(thing[1]) for thing in ratio_change_list])
				if -min_val > max_val:
					max_val = -min_val
				if -min_val < max_val:
					min_val = -max_val
				ratio_x_min_max = (min_val, max_val)
			kde_size_change_dict = {
				'color_dict': color_dict, 
				'list_peak_len_change': ratio_change_list, 
				'x_min_max': ratio_x_min_max, 
				'plot_name': filename_to_plot_name(name_str+'_'+ratio_string), 
				'save_spot': ratio_size_change_filepath,
			}

			os.makedirs(kde_size_change_dir, exist_ok=True)
			with open(ratio_dict_save_path, 'w') as textfile:
				json.dump(kde_size_change_dict, textfile)

			fig = easy_fig(figsize=(8, 8), gridsize=(1000,1000), \
			subplot_left=0.01, subplot_right=0.99, subplot_bottom=0.01, \
			subplot_top=0.99)
			fig.add_panel(top_left=(0.2,0.2), bottom_right=(0.8,0.8), \
				x_count=1, y_count=1, subpanel_pad_percent=0)	

			kde_size_change_just_plot(fig.axes[0][0], \
				kde_size_change_dict.get('color_dict'), \
				kde_size_change_dict.get('list_peak_len_change'), 
				kde_size_change_dict.get('x_min_max'), \
				kde_size_change_dict.get('plot_name'), \
				kde_size_change_dict.get('save_spot'),
				xlabel_value=ratio_name,
				longname=longname,
				change_cutoff=1
				)

			plt.savefig(ratio_size_change_filepath, dpi=300)
			plt.close()

		if not os.path.isfile(abs_size_change_filepath) or \
			not os.path.isfile(abs_dict_save_path):
			if abs_x_min_max == None:
				max_val = max([max(thing[1]) for thing in abs_change_list])
				min_val = min([min(thing[1]) for thing in abs_change_list])
				if -min_val > max_val:
					max_val = -min_val
				if -min_val < max_val:
					min_val = -max_val
				abs_x_min_max = (min_val, max_val)
			kde_size_change_dict = {
				'color_dict': color_dict, 
				'list_peak_len_change': abs_change_list, 
				'x_min_max': abs_x_min_max, 
				'plot_name': filename_to_plot_name(name_str+'_'+abs_string), 
				'save_spot': abs_size_change_filepath,
			}

			with open(abs_dict_save_path, 'w') as textfile:
				json.dump(kde_size_change_dict, textfile)

			fig = easy_fig(figsize=(8, 8), gridsize=(1000,1000), \
			subplot_left=0.01, subplot_right=0.99, subplot_bottom=0.01, \
			subplot_top=0.99)
			fig.add_panel(top_left=(0.2,0.2), bottom_right=(0.8,0.8), \
				x_count=1, y_count=1, subpanel_pad_percent=0)	

			if len_or_dens == 'dens':
				change_cutoff=None
			kde_size_change_just_plot(fig.axes[0][0], \
				kde_size_change_dict.get('color_dict'), \
				kde_size_change_dict.get('list_peak_len_change'), 
				kde_size_change_dict.get('x_min_max'), \
				kde_size_change_dict.get('plot_name'), \
				kde_size_change_dict.get('save_spot'),
				xlabel_value=abs_name,
				longname=longname,
				change_cutoff=change_cutoff
				)
			
			plt.savefig(abs_size_change_filepath, dpi=300)
			plt.close()

def kde_size_change_just_plot(ax, color_dict, list_peak_len_change, x_min_max, 
	plot_name, save_spot, x_ax_text_ratio=1, bandwidth_divider=15, plot_linewidth=1, 
	ymin_multiple=-.05, ymax_multiple=1.8, too_close_x_multi=40, type_label_upshift = 0.00, 
	type_label_weight='semibold', type_label_rotation=50, change_cutoff=2000, change_lw=0.5, 
	change_color='k', ylabel_value='Normalized Peak Density', ylabel_weight='semibold', 
	ylabel_pad=4, shortname='Shorten', midname='No Change', longname='Lengthen', 
	xlabel_value='Log2 of Length Change', xlabel_weight='semibold', x_range = 1000):
	X_plot = np.linspace((x_min_max[0]), (x_min_max[1]), x_range)[:, np.newaxis]
	max_spots = []
	cancer_max_list = []
	cancer_median_list = []
	median_spots = []
	x_list = []
	y_list = []
	top_cancer_max = 0
	for cancer_loop, cancer_type in enumerate(list_peak_len_change):
		to_fit = np.asarray(cancer_type[1])[:, np.newaxis]
		kde = KernelDensity(kernel='gaussian', bandwidth=\
			(((x_min_max[1])-(x_min_max[0]))/40))\
			.fit(to_fit)
		log_dens = kde.score_samples(X_plot)
		for_medians = sorted(cancer_type[1])
		median_info = int(len(for_medians)/2)
		x_list.append(for_medians[median_info])
		log_dens_ind = int(((x_list[-1]-x_min_max[0])/(x_min_max[1] - x_min_max[0]))*x_range)
		y_list.append(np.exp(log_dens)[log_dens_ind])
		cancer_max = 0
		for plot_loop in range(len(log_dens)):
			if np.exp(log_dens[plot_loop]) > cancer_max:
				cancer_max = np.exp(log_dens[plot_loop])
				max_spot = X_plot[plot_loop]
				if cancer_max > top_cancer_max:
					top_cancer_max = cancer_max
		cancer_max_list.append(cancer_max)
		ax.plot(X_plot[:, 0], np.exp(log_dens), '-',
			label="kernel = '{0}'".format('gaussian'), c=color_dict.get(cancer_type[0]), \
			lw=plot_linewidth)
		max_spots.append(max_spot)

	too_close_list = [0 for i in range(len(list_peak_len_change))]
	keep_going = True
	while keep_going == True:
		this_loop_good = True
		for max_spot_loop in range(len(max_spots)-1,-1, -1):
			for check_loop in range(len(max_spots)):
				if   ((max_spots[max_spot_loop] + ((x_min_max[1]-x_min_max[0])/too_close_x_multi)) > \
					max_spots[check_loop]) and ((max_spots[max_spot_loop]) < max_spots[check_loop]):
					if too_close_list[max_spot_loop] <= too_close_list[check_loop]:
						too_close_list[max_spot_loop] += 1
						this_loop_good = False
				elif ((max_spots[max_spot_loop] + ((x_min_max[1]-x_min_max[0])/(too_close_x_multi/2))) > \
					max_spots[check_loop]) and ((max_spots[max_spot_loop]) < max_spots[check_loop]):
					if too_close_list[max_spot_loop] < too_close_list[check_loop]:
						too_close_list[max_spot_loop] += 1
						this_loop_good = False
		if this_loop_good == False:
			keep_going = True
		else:
			keep_going = False
	for max_spot_loop in range(len(max_spots)):
		line = lines.Line2D([x_list[max_spot_loop], x_list[max_spot_loop]], \
			[top_cancer_max*(1.02+too_close_list[max_spot_loop]*0.03+type_label_upshift), \
			y_list[max_spot_loop]+type_label_upshift], 
			color='k', lw=0.5, transform=ax.transData)
		line.set_clip_on(False)
		ax.add_line(line)
		ax.text(x_list[max_spot_loop], \
			top_cancer_max*(1.02+too_close_list[max_spot_loop]*0.03+type_label_upshift), \
			list_peak_len_change[max_spot_loop][0]+': '+str(round(x_list[max_spot_loop],2)), 
			transform=ax.transData, weight=type_label_weight, ha='left', va='bottom', \
			rotation=type_label_rotation, color=color_dict.get(list_peak_len_change[max_spot_loop][0]))

	if change_cutoff is not None:
		vline_dict=[{'x': change_cutoff,'lw': change_lw, 'c': change_color}, 
			{'x': -change_cutoff,'lw': change_lw, 'c': change_color}]
	else:
		vline_dict=[]

	easy_fig.format_panel(ax,
		xlim=(x_min_max[0], x_min_max[1]),
		ylim=(top_cancer_max*ymin_multiple, top_cancer_max*ymax_multiple),
		vline_dict=vline_dict,
		ylabel_dict={'ylabel': ylabel_value, 'weight': ylabel_weight, 'labelpad': ylabel_pad}
	)
	info_y = -0.10*x_ax_text_ratio
	label_y = -0.14*x_ax_text_ratio
	ax.text(0.3, info_y,shortname, transform=ax.transAxes, ha='center', va='center')
	ax.text(0.5, info_y,midname, transform=ax.transAxes, ha='center', va='center')
	ax.text(0.7, info_y,longname, transform=ax.transAxes, ha='center', va='center')
	ax.text(0.5, label_y,xlabel_value, transform=ax.transAxes, \
		weight=xlabel_weight, ha='center', va='center')
			
	# old code
		# (((x_min_max[1])-(x_min_max[0]))/bandwidth_divider))\
		# old_for_medians = [np.exp(thing) for thing in log_dens]
		# old_median_info = kde_median(for_medians)
		# print(colored(str([median_info, old_median_info, len(for_medians), len(old_for_medians)]), 'white'))
		# print(for_medians[:30])
		# print(colored(str(old_for_medians[:30]), 'red'))
		# cancer_median_list.append(for_medians[median_info])
		# median_spots.append(x_min_max[0]+((x_min_max[1]-x_min_max[0])*(median_info/x_range)))
		# print(x_list[-1])
		# print(log_dens[:30])
		# print(np.exp(log_dens[:30]))
		# print(log_dens.shape, np.exp(log_dens).shape)
		# print(log_dens_ind)
		# print(y_list[-1])
		# print(cancer_median_list[-1], median_spots[-1])
		# print(old_for_medians[old_median_info], x_min_max[0]+((x_min_max[1]-x_min_max[0])*(old_median_info/x_range)))
		# raise
		# print(colored('\ncancer_type', 'white'))
		# print(cancer_type[1])
		# raise

		# line = lines.Line2D([max_spots[max_spot_loop], max_spots[max_spot_loop]], \
		# 	[top_cancer_max*(1.02+too_close_list[max_spot_loop]*0.03+type_label_upshift), \
		# 	cancer_max_list[max_spot_loop]+type_label_upshift], 
		# 	color='k', lw=0.5, transform=ax.transData)
		# line = lines.Line2D([use_this_x[max_spot_loop], use_this_x[max_spot_loop]], 
		# 	[top_cancer_max*1.02+(too_close_list[max_spot_loop]*0.03)+this_up, 
		# 	use_this_y[max_spot_loop]+this_up], 
		# ax.text(max_spots[max_spot_loop], \
		# 	top_cancer_max*(1.02+too_close_list[max_spot_loop]*0.03+type_label_upshift), \
		# 	list_peak_len_change[max_spot_loop][0]+': '+str(round(max_spots[max_spot_loop][0],2)), 
		# 	transform=ax.transData, weight=type_label_weight, ha='left', va='bottom', \
		# 	rotation=type_label_rotation, color=color_dict.get(list_peak_len_change[max_spot_loop][0]))
		# axes.text(use_this_x[max_spot_loop], 
		# 	top_cancer_max*1.02+(too_close_list[max_spot_loop]*0.03)+this_up, 
		# 	cancer_types[max_spot_loop]+': '+str(round(use_this_x[max_spot_loop],2)), 
		# 	fontsize=8, transform=axes.transData, weight='semibold', ha='left', va='bottom', 
		# 	rotation=50, color=d.get(cancer_types[max_spot_loop]))

def old_plot_kde_size_change(pipeline_dirs, ref_dir, plot_dir, \
	plot_name, color_dict, control_broad, value_index, \
	log2=True, x_min_max=None, add_lines=False,line_spot=None):
	if control_broad == True:
		if value_index == 3:
			save_spot1 = os.path.join(plot_dir, \
				'control_'+plot_name+'_size_change_kde.png')
		elif value_index == 5:
			save_spot1 = os.path.join(plot_dir, \
				'control_'+plot_name+'_size_change_kde_abs.png')
	else:
		if value_index == 3:
			save_spot1 = os.path.join(plot_dir, \
				'cancer_'+plot_name+'_size_change_kde.png')
		elif value_index == 5:
			save_spot1 = os.path.join(plot_dir, \
				'cancer_'+plot_name+'_size_change_kde_abs.png')
	if (not os.path.isfile(save_spot1)):
		cancer_types = []
		list_peak_len_change = []
		for filepath in glob.iglob(os.path.join(ref_dir, '*')):
			cancer_types.append(filepath.split('/')[-1].split('_')[0])
		list_peak_len_change = []
		for cancer_type in cancer_types:
			for filepath in glob.iglob(os.path.join(ref_dir, '*')):
				if cancer_type == filepath.split('/')[-1].split('_')[0]:
					ref_file = filepath
			compare_vals = []
			compare_means = []
			peak_len_change = []
			with open(ref_file, 'r') as textfile:
				for line in textfile:
					line = line.rstrip('\n').split('\t')
					if len(line) > 1:
						if log2 == True:
							peak_len_change.append(\
								math.log2(float(line[value_index])))
						else:
							peak_len_change.append(float(\
								line[value_index]))
			list_peak_len_change.append(peak_len_change)
		if x_min_max == None:
			max_val = max([max(thing) for thing in list_peak_len_change])
			min_val = min([min(thing) for thing in list_peak_len_change])
			x_min_max = (min_val, max_val)
		kde_size_change_dict = {
			'color_dict': color_dict, 
			'cancer_types': cancer_types, 
			'list_peak_len_change': list_peak_len_change, 
			'x_min_max': x_min_max, 
			'plot_name': plot_name, 
			'save_spot1': save_spot1,
			'control_broad': control_broad,
		}

		kde_size_change_dir = os.path.join(\
			pipeline_dirs.get('data_for_figures'), 'kde_size_change')
		os.makedirs(kde_size_change_dir, exist_ok=True)
		with open(os.path.join(kde_size_change_dir, \
			save_spot1.split('/')[-1].split('.png')[0]), 'w') as \
			textfile:
			json.dump(kde_size_change_dict, textfile)
		kde_size_change_just_plotting(\
			kde_size_change_dict.get('color_dict'), \
			kde_size_change_dict.get('cancer_types'), \
			kde_size_change_dict.get('list_peak_len_change'), 
			kde_size_change_dict.get('x_min_max'), \
			kde_size_change_dict.get('plot_name'), \
			kde_size_change_dict.get('save_spot1'), \
			kde_size_change_dict.get('control_broad'))

def lengthen_shorten_data(color_dict, directories, len_short_dir, \
	save_dir, plot_save, verbose=False):
	num_paths_per_group = 10
	len_dict, type_list = dir_to_dict(len_short_dir, directories, \
		group='lengthened')
	short_dict, type_list = dir_to_dict(len_short_dir, directories, \
		type_list=type_list, group='shortened')

	type_list = sorted(type_list)

	sorted_len_paths = sorted_path_list(len_dict, type_list)
	sorted_short_paths = sorted_path_list(short_dict, type_list)
	cutoff_rank = 5
	short_len_list = pick_paths(sorted_len_paths, cutoff_rank, \
		verbose=verbose)
	if verbose == True:
		print('')
	short_short_list = pick_paths(sorted_short_paths, cutoff_rank, \
		verbose=verbose)
	all_paths = [*short_short_list, '', *short_len_list]
	if verbose == True:
		print(all_paths)
	font = {'size'   : 7}
	mpl.rc('font', **font)


	all_p_vals = get_all_p_vals(all_paths, type_list, \
		short_dict, len_dict)
	new_p_vals, color_array, xpositions, ypositions = \
		final_prep_broad_sharp(all_p_vals, color_dict, \
			directories, verbose=verbose)
	broad_p_vals, broad_paths = sort_on_p_vals(new_p_vals, \
		all_paths, first=True)
	if verbose == True:
		print('')
	sharp_p_vals, sharp_paths = sort_on_p_vals(new_p_vals, \
		all_paths, first=False)
	all_paths = [*broad_paths, '', *sharp_paths]
	new_p_vals = [*broad_p_vals, [0 for item in broad_p_vals[0]], \
		*sharp_p_vals]

	new_p_vals = np.asarray(new_p_vals)
	color_array = np.asarray(color_array)

	group1_pattern = ' Shortened'
	group2_pattern = ' Lengthened'

	if new_p_vals.shape[0] >= num_paths_per_group*2:
		xpositions = xpositions[:(num_paths_per_group*2)] 
		ypositions = ypositions[:(num_paths_per_group*2)] 
		new_p_vals = new_p_vals[:(num_paths_per_group*2)] 
		color_array = color_array[:(num_paths_per_group*2)] 
	else:
		xpositions = xpositions[:(new_p_vals.shape[0])] 
		ypositions = ypositions[:(new_p_vals.shape[0])] 
		color_array = color_array[:(new_p_vals.shape[0])] 



	unique_broad_sharp_dict = {
		'xpositions': xpositions.tolist(),
		'ypositions': ypositions.tolist(),
		'new_p_vals': new_p_vals.tolist(),
		'color_array': color_array.tolist(),
		'type_list': type_list,
		'all_paths': all_paths,
		'group1_pattern': group1_pattern,
		'group2_pattern': group2_pattern,
	}

	os.makedirs(save_dir, exist_ok=True)
	this_save = os.path.join(save_dir, 'lengthen_shorten_paths')
	with open(this_save, 'w') as textfile:
		json.dump(unique_broad_sharp_dict, textfile)

	plot_path_dots_only(unique_broad_sharp_dict.get('xpositions'), \
		unique_broad_sharp_dict.get('ypositions'), \
		unique_broad_sharp_dict.get('new_p_vals'), 
		unique_broad_sharp_dict.get('color_array'), \
		unique_broad_sharp_dict.get('type_list'), \
		unique_broad_sharp_dict.get('all_paths'), \
		unique_broad_sharp_dict.get('group1_pattern'), \
		unique_broad_sharp_dict.get('group2_pattern'), \
		plot_save)

def filter_1_peak_list(peak_list, len_or_short, len_cutoff, dens_cutoff, compar_len_ind, 
	ref_len_ind, compar_dens_ind, ref_dens_ind, which_dens=None):
	if len_or_short is None and which_dens is None:
		# if len_or_short is None, that means this function is being used without
			# specific length filtering (eg, for cancer barplots or len change kde)
			# in that case, which_dens must be supplied to tell the function which
			# density filter to use
		raise ValueError('if len_or_short is None, which_dens cannot be None')

	saved_peak_list  = []
	for peak in peak_list:
		if len_or_short is not None:
			# shorten -> ref > compar -> positive
			# i have shortening as negative change?
			# i guess i should do compar minus ref lol
			peak_len_change = float(peak[compar_len_ind]) - \
				float(peak[ref_len_ind])
			if len_or_short == 'shorten':
				if peak_len_change <= float(len_cutoff.get('shorten')):
					len_change = True
				else:
					len_change = False
			elif len_or_short == 'lengthen':
				# lengthen -> ref < compar -> negative
				if peak_len_change >= float(len_cutoff.get('lengthen')):
					len_change = True
				else:
					len_change = False
			elif len_or_short == 'no_change':
				# lengthen -> ref < compar -> negative
				if peak_len_change <= float(len_cutoff.get('lengthen')) and \
					peak_len_change >= float(len_cutoff.get('shorten')):
					len_change = True
				else:
					len_change = False
			else:
				raise ValueError('len_or_short must be "lengthen", \
					\n\t"shorten", or None. Was {}'.format(len_or_short))
			used_dens_cutoff = dens_cutoff
		else:
			len_change = True
			if float(peak[compar_len_ind]) > float(peak[ref_len_ind]):
				used_dens_cutoff = dens_cutoff.get('lengthen').get(which_dens)
			elif float(peak[compar_len_ind]) < float(peak[ref_len_ind]):
				used_dens_cutoff = dens_cutoff.get('shorten').get(which_dens)
			elif float(peak[compar_len_ind]) == float(peak[ref_len_ind]):
				used_dens_cutoff = {
					'less_than': dens_cutoff.get('lengthen')
						.get(which_dens).get('less_than'), 
					'greater': dens_cutoff.get('shorten')
						.get(which_dens).get('greater')
				}
			else:
				raise ValueError('shouldn\'t be here')

			
		# do i'm doing shortening as < 1 dens ratio
		# shortening: compar dens < ref dens
		# so it should be compar dens over ref dens?
		peak_dens_change = float(peak[compar_dens_ind]) / \
			float(peak[ref_dens_ind])
		if used_dens_cutoff.get('less_than') is None:
			less_than = True
		elif peak_dens_change <= float(used_dens_cutoff.get('less_than')):
			less_than = True
		else:
			less_than = False
		if used_dens_cutoff.get('greater') is None:
			greater = True
		elif peak_dens_change >= float(used_dens_cutoff.get('greater')):
			greater = True
		else:
			greater = False
		if len_change is True and less_than is True and greater is True:
			saved_peak_list.append(peak)

	return saved_peak_list

def filter_len_dens_peaks(file_dict, no_change_dict, len_cutoff, dens_cutoff, len_or_short, 
	ref_len_ind=5, compar_len_ind=6, ref_dens_ind=7, compar_dens_ind=8, no_change_cutoff=None):
	# default columns of input bed:
		# 0-2: peak location
		# 3: compar length over ref length
			# shortened -> ref longer than compar (compar shorter than ref)
				# compar/ref < 1
			# lengthened -> ref shorted than compar (compar longer than ref)
				# compar/ref > 1
		# 4: compar density over ref density
			# got taller -> compar dens > ref dens -> >1
				# if peak lengthens (ref len < compar len), want to make sure
				# peak is shorter or same (ref dens > compar dens)
		# 5: ref length
		# 6: compar length
		# 7: ref density
		# 8: compar density
	all_sample_types = []
	for test_group in file_dict.keys():
		for sample_type in file_dict.get(test_group).keys():
			sample = file_dict.get(test_group).get(sample_type)
			all_sample_types.append(sample)
			if not os.path.isfile(sample[1]):
				peak_list = open_and_read(sample[0], '\t')
				write_bed(sample[1], 
					filter_1_peak_list(
						peak_list, len_or_short, len_cutoff, dens_cutoff, compar_len_ind, 
						ref_len_ind, compar_dens_ind, ref_dens_ind
					)
				)
			if no_change_dict is not None:
				no_change_sample = no_change_dict.get(test_group).get(sample_type)
				if not os.path.isfile(no_change_sample[0]):
					peak_list = open_and_read(sample[0], '\t')

					write_bed(no_change_sample[0], 
						filter_1_peak_list(
							peak_list, 'no_change', len_cutoff, no_change_cutoff, compar_len_ind, 
							ref_len_ind, compar_dens_ind, ref_dens_ind
						)
					)

def open_and_read(file_name, splitter=None, strip_by_item=None):
	output_list = []
	with open(file_name, 'r') as textfile:
		for line in textfile:
			if splitter is not None:
				line = line.rstrip('\n').split(splitter)
				if strip_by_item is not None:
					line = [thing.strip(strip_by_item) for thing in line]
			else:
				line = line.rstrip('\n')
				if strip_by_item is not None:
					line = [thing.strip(strip_by_item) for thing in line]
			output_list.append(line)
	return output_list

def write_bed(file_name, to_write):
	with open(file_name, 'w') as writefile:
		for line in to_write:
			try:
				print('\t'.join([str(col) for col in line]), \
					file=writefile)
			except:
				raise ValueError(line, 'didnt print')

def get_just_shortened_peaks(load_dir, save_dir, cancer_type, \
	dens_cutoff):
	os.makedirs(save_dir, exist_ok=True)
	load_file = list(glob.iglob(os.path.join(load_dir, \
		cancer_type+'*')))[0]
	peak_list = open_and_read(load_file, '\t')
	shortest = [thing for thing in peak_list if \
		float(thing[5]) < -2000 and float(thing[6]) >= dens_cutoff]
	write_bed(os.path.join(save_dir, cancer_type+'_shortened.bed'), \
		shortest)

def get_sample_type_and_id(filepath):
	file_name = filepath.split('/')[-1].split('.')[0]
	piece_list = multi_split_string(file_name, ['_', '-'])
	if check_for_int(piece_list[0]):
		sample_type = remove_int(piece_list[0])
		sample_id = piece_list[0]
	else:
		sample_type = piece_list[0]
		sample_id = piece_list[1]
	return sample_type, sample_id

def broad_cutoff(pipeline_dirs, directories, save_dirs, merge_sizes, \
	color_dict, verbose=False):
	if not os.path.isfile(os.path.join(save_dirs, \
		'all_kde_subplot.png')):
		all_cancer_list = []
		loop_num = 0
		fig, axes = None, None
		dir_peak_list = []
		for dir_loop in range(len(directories)):
			print(directories[dir_loop])
			size_peak_list = []
			for size_loop in merge_sizes:
				peak_list = []
				for filepath in glob.iglob(\
					os.path.join(pipeline_dirs.get('raw_data_dir'), \
					'*.xls')):
					good_file = False
					sample_type, _ = get_sample_type_and_id(filepath)
					if directories[dir_loop] == sample_type:
						if directories[dir_loop] != 'E':
							if filepath.split('/')[-1].split('_')[1] == \
								'E':
								good_file = True
						else:
							good_file = True
					if good_file == True:
						with open(filepath, 'r') as textfile:
							current_line = None
							for line in textfile:
								line = line.rstrip('\n').split('\t')
								if 'chr' in line[0]:
									if 'start' not in line[1]:
										if len(line) > 1:
											if current_line == None:
												current_line = line
											else:
												if (line[0] == current_line[0]) and (int(line[1]) < (int(current_line[2])+int(size_loop))):
													if int(line[2]) > int(current_line[2]):
														current_line[2] == line[2]
												else:
													peak_list.append((int(current_line[2]) - int(current_line[1])))
													current_line = line
				size_peak_list.append(peak_list)
			dir_peak_list.append(size_peak_list)
		all_kde_fig(dir_peak_list, plot_dir=save_dirs, merge_sizes=merge_sizes, color_dict=color_dict, cols=None, 
			directories=directories)

def all_kde_fig(data, plot_dir, directories, merge_sizes, color_dict, \
	rows=None,cols=None, minim=0, maxim=14000, num_vals = 100, fig=None):
	X_plot = np.linspace(minim, maxim, 1000)[:, np.newaxis]
	if fig==None:
		if cols==None:
			cols = int(math.ceil(math.sqrt(len(directories))))
		if rows==None:
			rows = int(len(directories)/cols)
		if rows*cols < len(directories):
			rows += 1
		if rows*cols < len(directories):
			raise ValueError(\
				'Not enough rows/cols for all_kde_fig (already added 1 row)')
		fig, axes = plt.subplots(rows,cols, figsize=(20/3*cols, 20/3*rows))
	d = {'0': 'C0', '1000': 'C1', '2000': 'C2', \
		'3000': 'C3', '4000': 'C4', '5000': 'C5', '6000': 'C6', \
		'7000': 'C7', '8000': 'C8', '9000': 'C9'}
	for loop_num in range(len(directories)):
		for sets in range(len(data[loop_num])):
			to_fit = np.asarray([float(x) for x in \
				data[loop_num][sets]])[:, np.newaxis]
			kde = KernelDensity(kernel='gaussian', \
				bandwidth=100).fit(to_fit)
			log_dens = kde.score_samples(X_plot)
			axes[int(loop_num/cols)][loop_num%cols].plot(X_plot[:, 0], \
				np.exp(log_dens), '-',
					label="kernel = '{0}'".format('gaussian'), \
						c=d.get(merge_sizes[sets]))
			if (loop_num)%cols != 0: # hide y axis except in left row
				axes[int(loop_num/cols)][loop_num%cols].\
					get_yaxis().set_visible(False)
		axes[int(loop_num/cols)][loop_num%cols].set_xlim(0, 6000)
		axes[int(loop_num/cols)][loop_num%cols].set_ylim(0, 0.001)
	fig.suptitle('Normalized Peak Density per Peak Size by Cancer', \
		y=0.94, fontsize=16)
	fig.subplots_adjust(right=0.85, left=0.15, bottom=0.13)
	legend_elements = []
	d_values = list(d.keys())
	for value in d_values:
		legend_elements.append(Line2D([0], [0], marker='o', \
			color='w', label=str(value), markerfacecolor=d.get(value), \
			markersize=9))
	axes[0][cols-1].legend(handles=legend_elements, \
		bbox_to_anchor=(1.016, 1), loc=2, borderaxespad=0)
	for i in range(rows): # put y label on left-side subplots
		axes[i][0].set_ylabel('Normalized Peak Density', \
			fontsize=13, labelpad=10)
	for i in range(loop_num +1):
		axes[int(i/cols)][i%cols].set_title(str(directories[i]))
	for i in range(cols):
		if i < len(directories) % cols:
			axes[rows-1][i].set_xlabel('Peak Size', fontsize=13, \
				labelpad=5)
		if i >= len(directories) % cols:
			axes[rows-2][i].set_xlabel('Peak Size', fontsize=13, \
				labelpad=5)
			axes[rows-1][i].axis('off')
	print('Saving KDE plot')
	plt.savefig(os.path.join(plot_dir, 'all_kde_subplot.png'))
	plt.close()

def plot_merge_scatter(prepped_data, merge_sizes, \
	plot_merge_scatter, color_dict, save_dir, \
	save_names, verbose):
	label_colors = [color_dict.get(sample_group.split(' ')[0])\
		.get(sample_group.split(' ')[1]) for sample_group in \
		plot_merge_scatter]
	legend_elements = []
	for val_loop, value in enumerate(plot_merge_scatter):
		legend_elements.append(Line2D([0], [0], marker='o', \
			color='w', label=str(value), \
			markerfacecolor=label_colors[val_loop], \
			markersize=9))
	fig, ax = plt.subplots()
	fig = plt.figure(num=1, figsize=(8, 6), dpi=90, \
		facecolor='w', edgecolor='k')

	peaks_per_cancer = []
	for group_loop, sample_group in enumerate(plot_merge_scatter):
		y_val = []
		for merge_size in merge_sizes:
			y_val.append(mean(prepped_data.get(sample_group)\
				.get(str(merge_size)).values()))
		plt.scatter(merge_sizes, y_val, c=label_colors[group_loop])
		plt.plot(merge_sizes, y_val, c=label_colors[group_loop])
	fig.subplots_adjust(right=0.75, left=0.175, bottom=0.13)
	plt.legend(handles=legend_elements, bbox_to_anchor=(1.02, 1), \
		loc=2, borderaxespad=0)
	maximum = 0
	for sample_type in prepped_data.keys():
		for merge_size in prepped_data.get(sample_type).keys():
			for peak_len in prepped_data.get(sample_type)\
				.get(merge_size).values():
				if int(peak_len) > maximum:
					maximum = int(peak_len)
	y_axes = (0, 5000*int(math.ceil(maximum/5000)))
	plt.ylim(y_axes)
	plt.title('Average Peak Number by Cancer Type by Merge Size')
	plt.xlabel('Merge Size', fontsize=13, labelpad=5)
	plt.ylabel('Number of peaks', fontsize=13, labelpad=10)
	plt.savefig(os.path.join(save_dir, save_names.get('all_plot')))
	plt.close()
	col_number = int(math.ceil(math.sqrt(len(plot_merge_scatter))))
	row_number = int(len(plot_merge_scatter)/col_number)
	if row_number*col_number < len(plot_merge_scatter):
		row_number += 1
	fig, ax = plt.subplots(row_number,col_number, \
		figsize=(20/3*col_number, 20/3*row_number), dpi=90, \
		facecolor='w', edgecolor='k')
	fig.suptitle('Total Peak Number per Sample by Merge Size', \
		y=0.94, fontsize=16)
	fig.subplots_adjust(right=0.85, left=0.15, bottom=0.13)
	for group_loop, sample_group in enumerate(plot_merge_scatter):
		row = int(group_loop/col_number)
		col = group_loop%col_number
		for this_sample in prepped_data.get(sample_group)\
			.get(str(merge_sizes[0])).keys():
			y_val = []
			for merge_loop, merge_size in enumerate(merge_sizes):
				y_val.append(prepped_data.get(sample_group)\
					.get(str(merge_size)).get(this_sample))
			ax[row,col].scatter(merge_sizes, y_val, \
				c=label_colors[group_loop])
			ax[row,col].plot(merge_sizes, y_val, \
				c=label_colors[group_loop])
		ax[row,col].set_ylim(y_axes)
	ax[0,col_number-1].legend(handles=legend_elements, \
		bbox_to_anchor=(1.02, 1), loc=2, borderaxespad=0)
	for row_loop in range(row_number): # put y label on left-side subplots
		ax[row_loop][0].set_ylabel('Number of peaks', \
			fontsize=13, labelpad=10)
	for col_loop in range(col_number):
		if col_loop < len(prepped_data) % col_number:
			ax[row_number-1][col_loop].set_xlabel('Merge Size', \
				fontsize=13, labelpad=5)
		if (col_loop >= len(prepped_data) % col_number) and \
			(len(prepped_data) % col_number != 0):
			ax[row_number-2][col_loop].set_xlabel('Merge Size', \
				fontsize=13, labelpad=5)
			ax[row_number-1][col_loop].axis('off')
	for panel_num in range(len(prepped_data)):
		ax[int(panel_num/col_number)][panel_num%col_number]\
			.set_title(str(plot_merge_scatter[panel_num]))	
	plt.savefig(os.path.join(save_dir, save_names.get('subplots')))
	plt.close()

def split_beds(file_dict, len_or_dens, val_or_perc, cutoff, \
	greater_less, directories):
	for test_group in file_dict.keys():
		for sample_type in file_dict.get(test_group).keys():
			for sample in file_dict.get(test_group).get(sample_type):
				if not os.path.isfile(sample[2]):
					peak_list = []
					if val_or_perc == 'perc':
						with open(sample[1], 'r') as readfile:
							values = []
							for line in readfile:
								line = line.rstrip('\n').split('\t')
								if len_or_dens == 'len':
									values.append(float(line[3]))
								elif len_or_dens == 'dens':
									values.append(float(line[4]))
							values = sorted(values)
							val_cutoff = values[int(len(values)*\
								((float(cutoff)/100)))]
					else:
						val_cutoff = cutoff
					with open(sample[1], 'r') as readfile:
						for line in readfile:
							line = line.rstrip('\n')
							split_line = line.rstrip('\n').split('\t')
							if len_or_dens == 'len':
								to_check = split_line[3]
							elif len_or_dens == 'dens':
								to_check = split_line[4]
							else:
								raise ValueError('Should be len or dens')
							if greater_less == 'greater':
								if (float(to_check) > float(val_cutoff)):
									peak_list.append(line)
							elif greater_less == 'less':
								if (float(to_check) < float(val_cutoff)):
									peak_list.append(line)
					with open(sample[2], 'w') as writefile:
						for line in peak_list:
							print(line, file=writefile)

def percent_split_beds(save_dirs, directories, list_settings, \
	greater_less, include_e):
	if include_e == True:
		if not 'E' in directories:
			directories.append('E')	
	for directory in directories:
		save_dir = os.path.join(save_dirs.get('split_dir'), directory)
		read_dir = os.path.join(save_dirs.get('top_merged_dir'), directory)
		os.makedirs(save_dir, exist_ok=True)
		for filepath in glob.iglob(os.path.join(read_dir, \
			'*_merged.bed')):
			_, sample_id = get_sample_type_and_id(filepath)
			yes_filename = os.path.join(save_dir, \
				sample_id+list_settings.get('file_pattern')+'.bed')
			if list_settings.get('make_non') == True:
				non_filename = os.path.join(save_dir, \
					sample_id+'_non.bed')
			if list_settings.get('make_non') == True:
				to_go = (not os.path.isfile(yes_filename)) or \
					(not os.path.isfile(non_filename))
			else:
				to_go = (not os.path.isfile(yes_filename))
			if to_go:
				yes_peaks = []
				if list_settings.get('make_non') == True:
					non_peaks = []
				with open(filepath, 'r') as textfile:
					lengths = []
					densities = []
					for line in textfile:
						line = line.rstrip('\n').split('\t')
						if list_settings.get('len_or_dens') == 'len':
							lengths.append(float(line[3]))
						elif list_settings.get('len_or_dens') == 'dens':
							lengths.append(float(line[4]))
					lengths = sorted(lengths)
					len_percent_cutoff = \
						lengths[int(len(lengths)*\
							((float(list_settings.get('cutoff'))/100)))]
				with open(filepath, 'r') as textfile:
					for line in textfile:
						line = line.rstrip('\n')
						line1 = line.rstrip('\n').split('\t')
						if list_settings.get('len_or_dens') == 'len':
							to_check = line1[3]
						elif list_settings.get('len_or_dens') == 'dens':
							to_check = line1[4]
						else:
							raise ValueError('Should be len or dens')
						if greater_less == 'greater':
							if (float(to_check) > \
								float(len_percent_cutoff)):
								yes_peaks.append(line)
							else:
								if list_settings.get('make_non') == True:
									non_peaks.append(line)
						elif greater_less == 'less':
							if (float(to_check) < \
								float(len_percent_cutoff)):
								yes_peaks.append(line)
							else:
								if list_settings.get('make_non') == True:
									non_peaks.append(line)
				with open(yes_filename, 'w') as textfile:
					for line in yes_peaks:
						print(line, file=textfile)
				if list_settings.get('make_non') == True:
					with open(non_filename, 'w') as textfile:
						for line in non_peaks:
							print(line, file=textfile)

def kb_split_beds(save_dirs, directories, list_settings, \
	greater_less, include_e):
	if include_e == True:
		if not 'E' in directories:
			directories.append('E')
	for directory in directories:
		save_dir = os.path.join(save_dirs.get('split_dir'), directory)
		read_dir = os.path.join(save_dirs.get('top_merged_dir'), directory)
		os.makedirs(save_dir, exist_ok=True)
		for filepath in glob.iglob(os.path.join(read_dir, \
			'*_merged.bed')):
			_, sample_id = get_sample_type_and_id(filepath)
			yes_filename = os.path.join(save_dir, \
				sample_id+list_settings.get('file_pattern')+'.bed')
			if list_settings.get('make_non') == True:
				non_filename = os.path.join(save_dir, \
					sample_id+'_non.bed')
			if list_settings.get('make_non') == True:
				to_go = (not os.path.isfile(yes_filename)) or \
					(not os.path.isfile(non_filename))
			else:
				to_go = (not os.path.isfile(yes_filename))
			if to_go:
				yes_peaks = []
				if list_settings.get('make_non') == True:
					non_peaks = []
				with open(filepath, 'r') as textfile:
					for line in textfile:
						line = line.rstrip('\n')
						line1 = line.rstrip('\n').split('\t')
						if list_settings.get('len_or_dens') == 'len':
							to_check = line1[3]
						elif list_settings.get('len_or_dens') == 'dens':
							to_check = line1[4]
						else:
							raise ValueError('Should be len or dens')
						if greater_less == 'greater':
							if (float(to_check) > \
								float(list_settings.get('cutoff'))):
								yes_peaks.append(line)
							else:
								if list_settings.get('make_non') == True:
									non_peaks.append(line)
						elif greater_less == 'less':
							if (float(to_check) < \
								float(list_settings.get('cutoff'))):
								yes_peaks.append(line)
							else:
								if list_settings.get('make_non') == True:
									non_peaks.append(line)
				with open(yes_filename, 'w') as textfile:
					for line in yes_peaks:
						print(line, file=textfile)
				if list_settings.get('make_non') == True:
					with open(non_filename, 'w') as textfile:
						for line in non_peaks:
							print(line, file=textfile)

def get_overlap_names(lists_dir):
	list_cancer_types = []
	for filepath in glob.iglob(os.path.join(lists_dir, '*_gene_names.txt')):
		for item in filepath.split('/')[-1].split('_')[1:-2]:
			if item not in list_cancer_types:
				list_cancer_types.append(item)
	sets_dir = lists_dir.split('/')[:-1]
	sets_dir.append('sets')
	sets_dir = '/'.join(sets_dir)
	cancer_id_map = []
	for filepath in glob.iglob(os.path.join(sets_dir, '*.bed')):
		cancer_id = filepath.split('/')[-1].split('_')[0]
		name_list = filepath.split('/')[-1].split('.')[0].split('_')
		cancer_types_in_name = []
		for name in name_list:
			if name in list_cancer_types:
				cancer_types_in_name.append(name)
		cancer_id_map.append([cancer_id, cancer_types_in_name])
	return cancer_id_map

def run_overlap_one_gene_list(sample, save_spot, text_save_spot, \
	plot_name, setup_variables, kegg_lists_location, \
	hallmark_lists_location, go_lists_location, 
	all_paths_location, unique_gene_list_location, \
	alpha, paths_per_analysis):
	start_time = time.time()
	path_values = run_overlap(dataset_file=sample, alpha=alpha, \
		paths_per_analysis = paths_per_analysis, method='high', \
		**setup_variables)
	if path_values != None:
		with open(text_save_spot, 'w') as writefile:
			for line in path_values:
				print('\t'.join([str(line[0]), str(line[3])]), \
					file=writefile)
		make_dataset_bar_plot(plot_name, path_values[0][6], \
			path_values[0][5], path_values, save_spot, 
			plot_sig_line=True)

def run_overlap_over_gene_lists(file_dict, plot_dir, \
	kegg_lists_location, hallmark_lists_location, 
	go_lists_location, all_paths_location, unique_gene_list_location, 
	cancer_id_map=None, alpha=0.05, paths_per_analysis=5, go_paths=False, \
	mp_workers=None, setup_variables = None, cpu_count=4):
	if go_paths == True:
		plot_dir = os.path.join(plot_dir, \
			'hallmark_kegg_go_paths')
	else:
		plot_dir = os.path.join(plot_dir, 'hallmark_kegg_paths')
	textfile_dir = os.path.join(plot_dir, 'textfiles')
	plot_save_dir = os.path.join(plot_dir, 'plots')

	for test_group in file_dict.keys():
		for sample_type in file_dict.get(test_group).keys():
			sample_list = file_dict.get(test_group).get(sample_type)
			if len(sample_list) == 1:
				specific_plot_dir = \
					os.path.join(plot_save_dir, test_group)
				specific_testfile_dir = \
					os.path.join(textfile_dir, test_group)
				os.makedirs(specific_plot_dir, exist_ok=True)
				os.makedirs(specific_testfile_dir, exist_ok=True)
				save_spot = os.path.join(specific_plot_dir, \
					'{}.png'.format(sample_type))
				text_save_spot = os.path.join(specific_testfile_dir, \
					'{}.txt'.format(sample_type))
				sample = sample_list[0]
				if not os.path.isfile(save_spot) or not \
					os.path.isfile(text_save_spot):
					if setup_variables is None:
						unique_genome_genes, path_genes, genome_gene_count, \
							path_names = \
							overlap_setup(kegg_lists_location, \
							hallmark_lists_location, go_lists_location, 
							all_paths_location, unique_gene_list_location, mp_workers, \
							go_paths=go_paths)
						setup_variables = {
										'unique_genome_genes': unique_genome_genes, 
										'path_genes': path_genes, 
										'genome_gene_count': genome_gene_count, 
										'path_names': path_names
									}

					plot_name = '{} {}'.format(test_group, sample_type)
					if mp_workers is not None:
						mp_workers.add_job(run_overlap_one_gene_list, 
							args=(sample, save_spot, text_save_spot, plot_name, \
								setup_variables, kegg_lists_location, \
								hallmark_lists_location, go_lists_location, \
								all_paths_location, unique_gene_list_location, \
								alpha, paths_per_analysis), 
							job_group='pathway_analysis')
					else:
						run_overlap_one_gene_list(
							sample, save_spot, text_save_spot, plot_name, \
							setup_variables, kegg_lists_location, \
							hallmark_lists_location, go_lists_location, \
							all_paths_location, unique_gene_list_location, \
							alpha, paths_per_analysis)
			else:
				raise ValueError('i didnt think it could get here')

	return setup_variables

def load_unique_gene_list(unique_gene_list_location):
	unique_genome_genes = []
	genome_gene_count = 0

	with open(unique_gene_list_location, 'r') as f:
		for line in f:
			unique_genome_genes.append(line.rstrip('\n'))
			genome_gene_count += 1
	return unique_genome_genes, genome_gene_count

def load_path_lists_location(lists_location):
	path_names = []
	path_genes = []
	with open(lists_location, 'r') as f:
		for line in f:
			line = line.rstrip('\n')
			path_names.append(line.split('\t')[0])
			path_genes.append(line.split('\t')[2:])
	return path_names, path_genes

def overlap_setup(kegg_lists_location, hallmark_lists_location, \
	go_lists_location, all_paths_location, unique_gene_list_location, \
	workers, go_paths = False):

	unique_genome_genes = []
	genome_gene_count = 0
	path_names = []
	path_genes = []

	if workers is not None:
		results = []
		gene_results = workers.add_job(load_unique_gene_list, 
			(unique_gene_list_location,), 
			job_group='pathway_setup')

		results.append(workers.add_job(load_path_lists_location, 
			(kegg_lists_location,), 
			job_group='pathway_setup'))

		results.append(workers.add_job(load_path_lists_location, 
			(hallmark_lists_location,), 
			job_group='pathway_setup'))

		if go_paths is True:
			results.append(workers.add_job(load_path_lists_location, 
				(go_lists_location,), 
				job_group='pathway_setup'))

		workers.block_for_specific_job_group('pathway_setup')

		unique_genome_genes, genome_gene_count = gene_results.result()

		for this_res in results:
			these_path_names, these_path_genes = this_res.result()
			path_names.extend(these_path_names)
			path_genes.extend(these_path_genes)

	else:
		unique_genome_genes, genome_gene_count = load_unique_gene_list(
			unique_gene_list_location)

		these_path_names, these_path_genes = load_path_lists_location(
			kegg_lists_location,)
		path_names.extend(these_path_names)
		path_genes.extend(these_path_genes)

		these_path_names, these_path_genes = load_path_lists_location(
			hallmark_lists_location,)
		path_names.extend(these_path_names)
		path_genes.extend(these_path_genes)

		if go_paths is True:
			these_path_names, these_path_genes = load_path_lists_location(
				go_lists_location,)
			path_names.extend(these_path_names)
			path_genes.extend(these_path_genes)


	return unique_genome_genes, path_genes, genome_gene_count, path_names

def run_overlap(unique_genome_genes, path_genes, genome_gene_count, \
	path_names, dataset_file=None, data_list=None, alpha=0.05, \
	paths_per_analysis=5, method='high'):
	dataset_genes = []
	real_dataset_gene = []
	unique_genome_lower = [thing.lower() for thing in unique_genome_genes]
	if dataset_file != None:
		with open(dataset_file, 'r') as f:
			for line in f:
				line = line.rstrip('\n').lower()
				dataset_genes.append(line)
				if line in unique_genome_lower:
					if line not in real_dataset_gene:
						real_dataset_gene.append(line)
	else:
		not_in_list = 0
		data_list = [thing.lower() for thing in data_list]
		for line in data_list:
			dataset_genes.append(line.lower())
			if line.lower() in unique_genome_lower:
				real_dataset_gene.append(line)
	how_often = 10
	to_show = None
	overlap_list = []
	start_time = None
	for gene_list in path_genes:
		lower_gene_list = [thing.lower() for thing in gene_list]
		gene_overlap = 0
		for gene in real_dataset_gene:
			if gene in lower_gene_list:
				gene_overlap += 1
		overlap_list.append(gene_overlap)
	arg1 = genome_gene_count
	arg2 = path_genes
	arg3 = real_dataset_gene
	arg4 = overlap_list

	p_value_list = []
	if method == 'high':
		for i in range(len(overlap_list)):
			p_val = float(stats.hypergeom.sf(int(arg4[i]) - 1, \
				int(arg1),int(len(arg2[i])),int(len(arg3))))
			p_value_list.append(p_val)

	elif method == 'low':
		for i in range(len(overlap_list)):
			p_val = float(str(stats.hypergeom.cdf(int(arg4[i]), \
				int(arg1),int(len(arg2[i])),int(len(arg3)))))
			p_value_list.append(p_val)
	else:
		raise ValueError(\
			'run_overlap needs a method, either high or low')		
	path_names = [x for _,x in sorted(zip(p_value_list,path_names), \
		reverse=True)]
	overlap_list = [x for _,x in sorted(zip(p_value_list,overlap_list), \
		reverse=True)]
	p_value_list = sorted(p_value_list, reverse=True)

	p_adjust = statsr.p_adjust(FloatVector(p_value_list), method = 'BH')
	path_values = []
	if paths_per_analysis > len(path_names):
		paths_per_analysis = len(path_names)
	for i in range(paths_per_analysis):
		index = -(paths_per_analysis-i)
		if p_adjust[index] <= alpha:
			path_values.append([str(path_names[index]), \
				overlap_list[index],p_value_list[index],p_adjust[index], \
				overlap_list[index], len(real_dataset_gene), \
				len(dataset_genes)])
	if len(path_values) > 0:
		return path_values
	else:
		return None

def make_dataset_bar_plot(joined_cancer_list, total_gene_count, \
	count_of_compared_genes, values_for_paths, save_spot, \
	upsetr=False, plot_sig_line=False, up_or_down=None):
	width = 0.5
	legend_elements = []
	number_of_iter = len(values_for_paths)
	max_paths = 20
	if number_of_iter > max_paths:
		first_val = number_of_iter - max_paths
	else:
		first_val = 0
	min_bars = 8
	assisted = False
	if number_of_iter < min_bars:
		assisted = True
	fig, ax = plt.subplots(figsize=(23, 10))
	x_axis = []
	if assisted == False:
		for i in range(first_val, number_of_iter):
			x_axis.append(i*2*width)
			x_axis.append((i*2*width)+width)
	else:
		for i in range(min_bars):
			x_axis.append(i*2*width)
			x_axis.append((i*2*width)+width)

	y_vals = []
	for i in range(first_val, number_of_iter):
		if values_for_paths[i][3] != 0:
			y_vals.append(-math.log10(values_for_paths[i][3]))
		else:
			y_vals.append(323.3)
		y_vals.append(0)
	if assisted == True:
		for i in range(min_bars - number_of_iter):
			y_vals.insert(0,0)
			y_vals.insert(0,0)
	barlist = ax.barh(x_axis, y_vals, width, color='g')
	if assisted == False:
		x_tick_spots = [(x*2*width) for x in \
			range(first_val, number_of_iter)]
	else:
		x_tick_spots = [(x*2*width) for x in \
			range(min_bars - number_of_iter,min_bars)]
	if number_of_iter < 15:
		num = 15
	elif max_paths < 15:
		num = 15
	elif number_of_iter > 19:
		num = 19
	else:
		num = number_of_iter
	num = 15
	ax.set_yticks(x_tick_spots)
	ax.set_yticklabels([values_for_paths[x][0] for x in \
		range(first_val, number_of_iter)], fontsize=130/num, \
		verticalalignment='center')
	ax.set_title('Pathway Analysis of: {}'.format(joined_cancer_list))
	if plot_sig_line == True:
		plt.axvline(x=1.301, c='k', linewidth=0.3)
	ax.set_xlabel('Negative log10 FDR')
	ax.set_ylabel('Pathway Name')
	fig.subplots_adjust(left=0.2)
	autolabel(ax, barlist[-(number_of_iter*2):], y_vals, num)
	plt.savefig(save_spot)
	plt.close()

def autolabel(ax, rects, y_vals,num):
	largest = 0
	smallest = 0
	for val in y_vals:
		if val > largest:
			largest = val
		if val < smallest:
			smallest = val
	text_offset = (largest - smallest)/200
	for i in range(len(rects)):
		if i%2 != 1:
			width = rects[i].get_width()
			xloc = width + text_offset
			yloc = rects[i].get_y() + rects[i].get_height()/2.0
			if num < 11:
				num = 11
				yloc = rects[i].get_y() + rects[i].get_height()/2.0
			ax.text(xloc, yloc, str(round(width,3)),fontsize=130/num, \
				verticalalignment='center')

def autolabel_num(ax, rects, num, interval):
	for i in range(len(rects)):
		if i%interval != (interval-1):
			width = int(rects[i].get_width())
			xloc = width + 3
			yloc = rects[i].get_y() + rects[i].get_height()/2.0
			if 130/num > 130/11:
				num = 11
				yloc = rects[i].get_y() + \
					rects[i].get_height()/interval
			ax.text(xloc, yloc, str(int(width)),fontsize=130/num, \
				verticalalignment='center')

def autolabel_common(ax, rects, text_list=None):
	top_max = max([thing.get_height() for thing in rects])
	for rect_loop in range(len(rects)):
		width = rects[rect_loop].get_height()
		xloc = width+(top_max*0.005)
		yloc = rects[rect_loop].get_x() + rects[rect_loop].get_width()/2.0
		if text_list == None:
			ax.text(yloc, xloc, str(int(width)), \
				horizontalalignment='center')
		else:
			ax.text(yloc, xloc, str(text_list[rect_loop]), \
				horizontalalignment='center')

def list_to_dictionary(input_list):
	if len(input_list[0]) != 2:
		raise ValueError(\
			'Input list to list_to_dictionary must be a list of 2 values per line, first line had {} lines'\
			.format(len(input_list[0])))
	d = {}
	for item in input_list:
		d[item[0]] = item[1]
	return d

def mean(values):
	if len(values) > 0:
		the_mean = float(sum(values)) / max(len(values), 1)
	else:
		raise ValueError('list for mean fnxn was empty')
	return the_mean

def multi_split_string(the_string, list_of_splits):
	for item in list_of_splits:
		the_string = split_to_one_list(the_string, item)
	return the_string

def split_to_one_list(the_item, split_char):
	if not isinstance(the_item, (list,)):
		new_list = the_item.split(split_char)
	elif isinstance(the_item, (list,)):
		list_of_list = [thing.split(split_char) for thing in the_item]
		new_list = list_of_list[0]
		for item in list_of_list[1:]:
			new_list += item
	else:
		print('oops')
	return new_list

def check_for_int(the_string):
	has_int = False
	for item in the_string:
		try:
			item2 = int(item)
			has_int = True
		except:
			item2 = item
	return has_int

def manual_merge(input_file, separation, merged_path, chr_index=0, \
	start_index=1, end_index=3, density_index=None):
	merged_lines = []
	current_line = None
	with open(input_file, 'r') as textfile:
		for line in textfile:
			line = line.rstrip('\n').split('\t')
			dump_current_line = False
			if len(line) > 1:
				if current_line == None:
					current_line = line
				elif current_line[chr_index] == line[chr_index]:
					if int(current_line[end_index])+int(separation)+1 \
						>= int(line[start_index]):
						current_line[end_index] = int(line[end_index])
						if density_index is not None:
							if float(line[3]) > float(current_line[3]):
								current_line[3] = float(line[3])
					else:
						dump_current_line = True
				else:
					dump_current_line = True
			else:
				dump_current_line = True
			if dump_current_line == True:
				if len(current_line) > 1:
					if density_index is not None:
						merged_lines.append(\
							[str(current_line[chr_index]), \
							str(current_line[start_index]), 
							str(current_line[end_index]), \
							str(current_line[3])])
					else:
						merged_lines.append(\
							[str(current_line[chr_index]), \
							str(current_line[start_index]), 
							str(current_line[end_index])])
					current_line = line
	with open(merged_path, 'w') as textfile:
		for line in merged_lines:
			print('\t'.join(line), file=textfile)

def add_len_to_bed(file):
	len_lines = []
	with open(file, 'r') as textfile:
		for line in textfile:
			line = line.rstrip('\n').split('\t')
			peak_len = int(line[2]) - int(line[1])
			len_lines.append(line[:3]+[str(peak_len)]+line[3:])
	with open(file, 'w') as textfile:
		for line in len_lines:
			print('\t'.join(line), file=textfile)

def remove_int(the_string):
	no_ints = ''
	for item in the_string:
		try:
			item2 = int(item)
		except:
			item2 = item
			no_ints += item2
	return no_ints

def plot_peak_len_dens_histo(fig_data_save, analysis_name, file_list, 
	save_path, name_var, len_index = 3, dens_index = 6, len_bins=1000, 
	dens_bins=200, log_norm=True, vmax=None, vmin=None,
	xlimits=[0,20000], ylimits=[0,60], vert_line=4000, 
	max_x_ratio=1.3, max_y_ratio=1.3):
	# , slopes=None
	# if slopes != None:
	# 	(slope1, intercept1, slope2, intercept2) = slopes
	densities = []
	lengths = []
	for files in file_list:
		with open(files, 'r') as textfile:
			# i = 0
			for line in textfile:
				line = line.rstrip('\n').split('\t')
				# if 'chr' in line[0]:
				# 	if 'start' not in line[1]:
				densities.append(float(line[dens_index]))
				lengths.append(float(line[len_index]))

	if len(densities) > 0:
		split_plot_dict = {
			'log_norm': log_norm, 
			'lengths': lengths, 
			'densities': densities, 
			'len_bins': len_bins, 
			'dens_bins': dens_bins, 
			'vmin': vmin, 
			'vmax': vmax, 
			'xlimits': xlimits, 
			'ylimits': ylimits, 
			'vert_line': vert_line, 
			'name_var': name_var, 
			'save_path': save_path,
			'max_x_ratio': max_x_ratio,
			'max_y_ratio': max_y_ratio,
			# 'slopes': slopes
		}
		split_plot_dict_dir = os.path.join(fig_data_save, 
			'split_plot_dict', analysis_name)
		os.makedirs(split_plot_dict_dir, exist_ok=True)

		with open(os.path.join(split_plot_dict_dir, name_var), 'w') as \
			textfile:
			json.dump(split_plot_dict, textfile)
		# x_axes, y_axes = split_plot_just_plotting(
		split_plot_just_plotting(
			split_plot_dict.get('log_norm'), 
			split_plot_dict.get('lengths'), 
			split_plot_dict.get('densities'), 
			split_plot_dict.get('len_bins'), 
			split_plot_dict.get('dens_bins'), 
			split_plot_dict.get('vmin'), 
			split_plot_dict.get('vmax'), 
			split_plot_dict.get('xlimits'), 
			split_plot_dict.get('ylimits'), 
			split_plot_dict.get('vert_line'), 
			split_plot_dict.get('name_var'), 
			split_plot_dict.get('save_path'), 
			split_plot_dict.get('max_x_ratio'), 
			split_plot_dict.get('max_y_ratio'))
			# split_plot_dict.get('slopes'))
		# return x_axes, y_axes

def old_bedop_merge(bed_input, final_output=None, keep_new_vals=False, \
	dir_files_or_peaks='file_list', save=False):
	# ??? # , keep_dens=True, dens
	# figuring out how to handle inputs based on dir_files_or_peaks arg
	if dir_files_or_peaks not in ['file_dir', 'file_list', 'peak_list']:
		raise ValueError(\
			'For peak_merge_filecount, dir_files_or_peaks must be "file_list", "file_dir", or "peak_list", it was: {}'\
			.format(dir_files_or_peaks))
	# this section primarily generates "list_peak_list", which is a peak list
		# for each input bed (unless input was "peak_list" format, see below)
	bed_count = 0
	bed_name_list = []
	list_peak_lists = []
	# want this if bed_input in pointing at a directory -> globs files
	if dir_files_or_peaks is 'file_dir':
		for filepath in glob.iglob(os.path.join(bed_input, '*.bed')):
			bed_count += 1
			bed_name_list.append(\
				filepath.split('/')[-1].split('.')[0].split('_')[0])
			this_peak_list = []
			with open(filepath, 'r') as textfile:
				for line in textfile:
					line = line.rstrip('\n').split('\t')
					this_peak_list.append(line)
			list_peak_lists.append(this_peak_list)
	# want this if bed_input is a file list
	elif dir_files_or_peaks is 'file_list':
		for filepath in bed_input:
			bed_count += 1
			bed_name_list.append(\
				filepath.split('/')[-1].split('.')[0].split('_')[0])
			this_peak_list = []
			with open(filepath, 'r') as textfile:
				for line in textfile:
					line = line.rstrip('\n').split('\t')
					this_peak_list.append(line)
			list_peak_lists.append(this_peak_list)
	# want this is bed_input is a list of peak lists (aka, generated beds
		# but don't want to have to write to beds before calling this fnxn)
	elif dir_files_or_peaks is 'peak_list':
		for list_loop, peak_list in enumerate(bed_input):
			list_peak_lists.append(peak_list)
			bed_name_list.append(str(list_loop))
			bed_count += 1
	else:
		raise ValueError(\
			'Should not have made it to this option. theres an issue with dir_files_or_peaks for peak_merge_filecount')
	# want to sort lists from longest -> shortest list (and sort bed name list)
	file_lens = [len(list_peak_lists[file_num]) for file_num in \
		range(len(list_peak_lists))]
	sorted_array = [(len_col, name_col, peaks_col) for len_col, \
		name_col, peaks_col in \
		sorted(zip(file_lens,bed_name_list,list_peak_lists), \
			key=operator.itemgetter(0), reverse=True)]
	# regenerating input variables after zip + sorting
	file_lens = [array_row[0] for array_row in sorted_array]
	bed_name_list = [array_row[1] for array_row in sorted_array]
	list_peak_lists = [array_row[2] for array_row in sorted_array]
	# making a sorted flat "all_peaks_list" for peak overlap 
		# counting in the next step
		# each peak is given a "file_num" that will be used later to ensure that
		# peaks counts are only increased if peaks from different files overlap
	# expected format of input peaks:
		# cols 1-3: peak location
		# col 4 (if exists): peak density
		# col 5 (if exists): ???
		# col 6 (if exists): ???
		# col 7+: anything else (doesn't modify these)

	all_peaks_list = []
	# need to convert cols 2/3 to int for sorting
	for file_num, peak_list in enumerate(list_peak_lists):
		for peak_data in peak_list:
			if len(peak_data) > 0:
				if len(peak_data) > 3:
					all_peaks_list.append([peak_data[0]] + \
						[int(peak_data[1])] + [int(peak_data[2])] + \
						[str(file_num)]+peak_data[3:])
				else:
					all_peaks_list.append([peak_data[0]] + \
						[int(peak_data[1])] + [int(peak_data[2])] + \
						[str(file_num)])
	all_peaks_list = sorted(all_peaks_list, \
		key=operator.itemgetter(0,1))
	for line_loop in range(len(all_peaks_list)):
		all_peaks_list[line_loop] = [str(col) for col in \
			all_peaks_list[line_loop]]
	# general strategy, gonna loop through all_peaks_list -> any time peaks overlap
		# merge them with current_peak_val peak and, if the peaks come from
		# multiple different files -> add to the count of peaks that overlap 
		# in that location. 
	# if the next peak does not overlap w/ current_peak_val, then current peak val
		# is added to merged_peak_list and the fnxn moves to the next peak
	current_peak_val = None
	merged_peak_list = []
	make_len_dense_avg = False
	for peak in all_peaks_list:
		peak_len = len(peak)
		# adding a new current_peak_val is there currently isn't one
			# format:
				# cols 1-3: peak location
				# col 4: empty list that will keep track of what files
					# overlapped peaks came from
				# col 5: empty list that will keep track of density values of
					# previous overlapping peaks
				# col 6: empty list that will 
				# col 7: empty list that will 
				# cols 8+: cols 7+ from original list
		if current_peak_val is None:
			if peak_len > 4:
				if str_is_float(peak[4]) and str_is_float(peak[5]):
					make_len_dense_avg = True
					current_peak_val = \
						peak[:3]+[[0]*bed_count]+[[0]*bed_count]+\
						[[0]*bed_count]+[[0]*bed_count]+peak[6:]
					# multi val = current dens value for 
					multi_val = \
						current_peak_val[5][int(peak[3])]/\
						(current_peak_val[5][int(peak[3])]+1)
					current_peak_val[4][int(peak[3])] = \
						(current_peak_val[4][int(peak[3])]*\
							(multi_val)) + (float(peak[4])*\
							(1-(multi_val)))
					current_peak_val[6][int(peak[3])] = \
						(current_peak_val[6][int(peak[3])]*\
							(multi_val)) + (float(peak[5])*\
							(1-(multi_val)))
					current_peak_val[5][int(peak[3])] += 1
			if make_len_dense_avg is False:
				current_peak_val = peak[:3]+[[0]*bed_count]+peak[4:]
			current_peak_val[3][int(peak[3])] = 1
			continue
		else:
			# enter this block if peak overlaps w/ current_peak_val
			if (int(peak[1]) < int(current_peak_val[2])) and \
			(peak[0] == current_peak_val[0]):
				current_peak_val[3][int(peak[3])] = 1
				if make_len_dense_avg is True:
					multi_val = \
						current_peak_val[5][int(peak[3])]/\
						(current_peak_val[5][int(peak[3])]+1)
					current_peak_val[4][int(peak[3])] = \
						(current_peak_val[4][int(peak[3])]*\
						(multi_val)) + (float(peak[4])*\
						(1-(multi_val)))
					current_peak_val[6][int(peak[3])] = \
						(current_peak_val[6][int(peak[3])]*\
						(multi_val)) + (float(peak[5])*\
						(1-(multi_val)))
					current_peak_val[5][int(peak[3])] += 1
				if int(peak[2]) > int(current_peak_val[2]):
					current_peak_val[2] = peak[2]
			else:
				# enter this block if it does not overlap
				which_files = []
				num_files = 0
				for name_loop in range(len(bed_name_list)):
					if current_peak_val[3][name_loop] == 1:
						which_files.append(bed_name_list[name_loop])
						num_files += 1
				if keep_new_vals == True:
					if make_len_dense_avg is True:
						peak_len_list = [int(thing) for thing in \
							current_peak_val[4] if thing != 0]
						peak_dens_list = [float(thing) for thing in \
							current_peak_val[6] if thing != 0]
						current_peak_val = current_peak_val[:3] + \
							[str(num_files)] + ['_'.join(which_files)] + \
							[round(sum(peak_len_list)/float(len(\
							peak_len_list)),1)] + ['_'.join([str(thing) \
							for thing in peak_len_list])] + \
							[round(sum(peak_dens_list)/float(len(\
							peak_dens_list)),1)] + ['_'.join([str(thing) \
							for thing in peak_len_list])] + \
							['_'.join([str(thing) for thing in \
							peak_dens_list])] + current_peak_val[7:]
					else:
						current_peak_val = current_peak_val[:3] + \
							[str(num_files)] + ['_'.join(which_files)] + \
							current_peak_val[4:]
				else:
					if make_len_dense_avg is True:
						peak_len_list = [int(thing) for thing in \
							current_peak_val[4] if thing != 0]
						peak_dens_list = [float(thing) for thing in \
							current_peak_val[6] if thing != 0]
						current_peak_val = current_peak_val[:3] + \
							[round(sum(peak_len_list)/\
								float(len(peak_len_list)),1)] + \
								['_'.join([str(thing) for thing in \
									peak_len_list])] + \
							[round(sum(peak_dens_list)/\
								float(len(peak_dens_list)),1)] + \
								['_'.join([str(thing) for thing in \
									peak_len_list])] + \
							['_'.join([str(thing) for thing in \
								peak_dens_list])] + current_peak_val[7:]
					else:
						current_peak_val = current_peak_val[:3] + \
							current_peak_val[4:]
				merged_peak_list.append([str(thing) for thing in \
					current_peak_val])
				if make_len_dense_avg is True:
					current_peak_val = peak[:3]+[[0]*bed_count]+[[0]*\
						bed_count]+[[0]*bed_count]+[[0]*bed_count]+\
						peak[6:]
					multi_val = current_peak_val[5][int(peak[3])]/\
						(current_peak_val[5][int(peak[3])]+1)
					current_peak_val[4][int(peak[3])] = \
						(current_peak_val[4][int(peak[3])]*\
						(multi_val)) + (float(peak[4])*\
						(1-(multi_val)))
					current_peak_val[6][int(peak[3])] = \
						(current_peak_val[6][int(peak[3])]*\
						(multi_val)) + (float(peak[5])*\
						(1-(multi_val)))
					current_peak_val[5][int(peak[3])] += 1
				else:
					current_peak_val = peak[:3]+[[0]*bed_count]+peak[4:]
				current_peak_val[3][int(peak[3])] = 1
	if save == True:
		write_bed(final_output, merged_peak_list)
	return merged_peak_list

def sep_based_wrap(text, width=70, word_separators=None):

	if word_separators is None:
		word_separators = ['_', '-', ' ']
	output_list = []
	current_spot = 0
	last_spot = 0
	while True:
		current_spot += width
		if text[last_spot] in word_separators:
			last_spot += 1
			current_spot += 1
		# print('\t', current_spot)
		while True:
			# print('\t\t', current_spot)
			if current_spot >= len(text) - 1:
				output_list.append(text[last_spot:])
				# print(text, '\n\t', output_list)
				return output_list

			elif text[current_spot] in word_separators:
				output_list.append(text[last_spot:current_spot])
				last_spot = current_spot
				break
			elif current_spot == last_spot:
				current_spot += width
				output_list.append(text[last_spot:current_spot])
				last_spot = current_spot
				break
			else:
				current_spot -= 1



def format_pathname_kegg_hallmark(pathname, path_types=None, respect_caps=False, word_separators=None,
	add_sep=''):
	if path_types is None:
		path_types = [['HALLMARK', 'H'], ['KEGG', 'K']]
	if word_separators is None:
		word_separators = ['_', '-', ' ']

	if respect_caps:
		check_pathname = pathname
		check_path_type = [this_path[0] for this_path in path_types]
	else:
		check_pathname = pathname.lower()
		check_path_type = [this_path[0].lower() for this_path in path_types]
	
	type_ind_list = [check_pathname.find(ptype) for ptype in check_path_type]
	type_len_list = [len(ptype) for ptype in check_path_type]
	num_match = sum([this_ind != -1 for this_ind in type_ind_list])
	if num_match > 1:
		which_type = type_len_list.index(max(type_len_list))
	elif num_match == 1:
		which_type = type_ind_list.index([this_ind for this_ind in type_ind_list
				if this_ind != -1][0])
	elif num_match == 0:
		return pathname
	this_type = path_types[which_type]
	start_ind, end_ind = type_ind_list[which_type], type_ind_list[which_type] + len(this_type[0])
	# print(pathname[start_ind:end_ind])
	# print(pathname[start_ind-1:end_ind+1])
	# print(0, start_ind, end_ind, len(pathname))
	if start_ind > 0:
		if pathname[start_ind-1] in word_separators:
			start_ind -= 1
	if end_ind < len(pathname) - 1:
		# print(pathname[end_ind+1])
		if pathname[end_ind] in word_separators:
			end_ind += 1
	# print(0, start_ind, end_ind, len(pathname))
	to_remove = pathname[start_ind:end_ind]
	# print(to_remove)
	to_add = '{}({})'.format(add_sep, this_type[1])
	final_str = pathname.replace(to_remove, '') + to_add

	return final_str


		# type_ind = pathname.find(ptype[0])
		# if type_ind 

def peak_merge_filecount(bed_input, special_in_file_handling=None, 
	special_file_bw_handling=None, dir_files_or_peaks='file_list', 
	val_sep_str='_', save_location=False, default_file_bw_file='avg', 
	keep_all_cols=False):
	# , default_col_in_file='avg'
	def new_current_peak(peak, bed_count, file_num, col_types):
		current_peak_val = peak[:3] + [[0]*bed_count]
		current_peak_val[-1][file_num] = 1
		for col_num, col_val in enumerate(col_types):
			if col_val == 1:
				current_peak_val.append(list([] for num in range(bed_count)))
				current_peak_val[-1][file_num].append(peak[col_num])
			elif col_num > 3:
				current_peak_val.append(peak[col_num])
		return current_peak_val

	def set_col_dicts(special_in_file_handling, special_file_bw_handling, 
		default_file_bw_file):
		def get_dict_fnxn(input_fnxn):
			if isinstance(input_fnxn, str):
				if input_fnxn == 'max':
					input_fnxn = max
				elif input_fnxn == 'min':
					input_fnxn = min
				elif input_fnxn == 'avg':
					input_fnxn = mean
			return input_fnxn
			# def_bw_file = True
			# if special_file_bw_handling is not None:
			# 	if special_file_bw_handling.get(real_ind) 
			# 		is not None:

		in_file_dict, bw_file_dict = {}, {}
		for col_num in special_in_file_handling.keys():
			if col_num < 3:
				raise ValueError('None of the first 3 cols can be a special \
					\n\tcolumn, yet {} was designated as such'.format(col_num))
			in_file_dict[col_num+1] = get_dict_fnxn(special_in_file_handling
				.get(col_num))
			added_dict = False
			if special_file_bw_handling is not None:
				if special_file_bw_handling.get(col_num) is not None:
					added_dict = True
					bw_file_dict[col_num+1] = get_dict_fnxn(
									special_file_bw_handling.get(col_num))
			if not added_dict:
				bw_file_dict[col_num+1] = get_dict_fnxn(default_file_bw_file)

		return in_file_dict, bw_file_dict

	if special_in_file_handling is None:
		special_in_file_handling = {}

	# figuring out how to handle inputs based on dir_files_or_peaks arg
	if dir_files_or_peaks not in ['file_dir', 'file_list', 'peak_list']:
		raise ValueError(\
			'For peak_merge_filecount, dir_files_or_peaks must be \
			\n\t"file_list", "file_dir", or "peak_list", it was: {}'\
			.format(dir_files_or_peaks))
	# this section primarily generates "list_peak_list", which is a peak list
		# for each input bed (unless input was "peak_list" format, see below)
	bed_count = 0
	bed_name_list = []
	list_peak_lists = []
	# want this if bed_input in pointing at a directory -> globs files
	if dir_files_or_peaks is 'file_dir':
		for filepath in glob.iglob(os.path.join(bed_input, '*.bed')):
			bed_count += 1
			bed_name_list.append(\
				filepath.split('/')[-1].split('.')[0].split('_')[0])
			this_peak_list = []
			with open(filepath, 'r') as textfile:
				for line in textfile:
					line = line.rstrip('\n').split('\t')
					this_peak_list.append(line)
			list_peak_lists.append(this_peak_list)
	# want this if bed_input is a file list
	elif dir_files_or_peaks is 'file_list':
		for filepath in bed_input:
			bed_count += 1
			bed_name_list.append(\
				filepath.split('/')[-1].split('.')[0].split('_')[0])
			this_peak_list = []
			with open(filepath, 'r') as textfile:
				for line in textfile:
					line = line.rstrip('\n').split('\t')
					this_peak_list.append(line)
			list_peak_lists.append(this_peak_list)
	# want this is bed_input is a list of peak lists (aka, generated beds
		# but don't want to have to write to beds before calling this fnxn)
	elif dir_files_or_peaks is 'peak_list':
		for list_loop, peak_list in enumerate(bed_input):
			list_peak_lists.append(peak_list)
			bed_name_list.append(str(list_loop))
			bed_count += 1
	else:
		raise ValueError(\
			'Should not have made it to this option. \
			\n\ttheres an issue with dir_files_or_peaks for peak_merge_filecount')
	
	# want to sort lists from longest -> shortest list (and sort bed name list)
	file_lens = [len(list_peak_lists[file_num]) for file_num in \
		range(len(list_peak_lists))]
	sorted_array = [(len_col, name_col, peaks_col) for len_col, \
		name_col, peaks_col in \
		sorted(zip(file_lens,bed_name_list,list_peak_lists), \
			key=operator.itemgetter(0), reverse=True)]
	# regenerating input variables after zip + sorting
	file_lens = [array_row[0] for array_row in sorted_array]
	bed_name_list = [array_row[1] for array_row in sorted_array]
	list_peak_lists = [array_row[2] for array_row in sorted_array]
	# making a sorted flat "all_peaks_list" for peak overlap 
		# counting in the next step
		# each peak is given a "file_num" that will be used later to ensure that
		# peaks counts are only increased if peaks from different files overlap
	# expected format of input peaks:
		# cols 1-3: peak location
		# the rest are up to the user

	all_peaks_list = []
	# need to convert cols 2/3 to int for sorting
	for file_num, peak_list in enumerate(list_peak_lists):
		for peak_data in peak_list:
			if len(peak_data) > 0:
				if len(peak_data) > 3:
					all_peaks_list.append([peak_data[0]] + \
						[int(peak_data[1])] + [int(peak_data[2])] + \
						[str(file_num)]+peak_data[3:])
				else:
					all_peaks_list.append([peak_data[0]] + \
						[int(peak_data[1])] + [int(peak_data[2])] + \
						[str(file_num)])
	all_peaks_list = sorted(all_peaks_list, \
		key=operator.itemgetter(0,1))

	# identifying columns that need to be handled specially 
		# aka, if a single file has multiple peaks, this will keep track of
		# the values for that column for each peak -> can handle the values
		# as requested (get avg, min, max, or a provided custom fnxn)
	in_file_dict, bw_file_dict = set_col_dicts(
				special_in_file_handling, special_file_bw_handling, 
				default_file_bw_file)

	special_col_list = list(in_file_dict.keys())
	for col_num, col_val in enumerate(special_col_list):
		try:
			special_col_list[col_num] = int(col_val)
		except:
			raise ValueError('Keys of special_in_file_handling should be int or be\
				\n\tto turn into int')
	special_col_list = sorted(special_col_list)

	# turn columns into strings + make sure all rows have enough columns
	for line_loop in range(len(all_peaks_list)):
		all_peaks_list[line_loop] = [str(col) for col in \
			all_peaks_list[line_loop]]
		if len(all_peaks_list[line_loop]) < 3:
			raise ValueError('Peak {} from all_peaks_list does not have required\
				\n\t3 columns'.format(all_peaks_list[line_loop]))
		if len(special_col_list) > 0:
			if len(all_peaks_list[line_loop]) <= special_col_list[-1]:
				if len(all_peaks_list[line_loop]) > 4:
					print_line = all_peaks_list[line_loop][:3]+\
						all_peaks_list[line_loop][4:]
				else:
					print_line = all_peaks_list[line_loop][:3]
				raise ValueError('Peak {} of bed_input does not have\
					\n\tall cols referenced in special_in_file_handling. Should have\
					\n\tat least {} cols'
					.format(print_line, special_col_list[-1]))
		# if any([val_sep_str in col_val for col_val in 
		# 	all_peaks_list[line_loop]]):
		# 	raise ValueError('val_set_str found in column values,')

	col_types = [1 if col_num in special_col_list else 0 for col_num in 
						range(len(all_peaks_list[0]))]

	# general strategy, gonna loop through all_peaks_list -> any time peaks overlap
		# merge them with current_peak_val peak and, if the peaks come from
		# multiple different files -> add to the count of peaks that overlap 
		# in that location. 
	# if the next peak does not overlap w/ current_peak_val, then current peak val
		# is added to merged_peak_list and the fnxn moves to the next peak
	current_peak_val = None
	merged_peak_list = []
	# make_len_dense_avg = False
	for peak in all_peaks_list:
		peak_len = len(peak)
		file_num = int(peak[3])
		# adding a new current_peak_val is there currently isn't one
			# format:
				# cols 1-3: peak location
				# col 4: which files have peaks come from?
				# cols 5+ handled based on special_in_file_handling dict
		if current_peak_val is None:
			current_peak_val = new_current_peak(
							peak, bed_count, file_num, col_types)
		else:

			# enter this block if peak overlaps w/ current_peak_val
				# append values from special cols to the appropriate col
					# in current_peak_val
			if (int(peak[1]) < int(current_peak_val[2])) and \
				(peak[0] == current_peak_val[0]):
				current_peak_val[3][file_num] = 1
				for col_num, col_val in enumerate(col_types):
					if col_num > 3:
						if col_val == 1:
							current_peak_val[col_num][file_num]\
								.append(peak[col_num])
				# if new peak extends further right than current_peak_val
					# extend current_peak_val to the end of new peak
				if int(peak[2]) > int(current_peak_val[2]):
					current_peak_val[2] = peak[2]

			else:
				# enter this block if it does not overlap
					# need to finish handling special columns
					# then append to merged_peak_list

				which_files = []
				num_files = 0
				for name_loop in range(len(bed_name_list)):
					if current_peak_val[3][name_loop] == 1:
						which_files.append(bed_name_list[name_loop])
						num_files += 1

				return_peak = current_peak_val[:3] + [num_files]
				all_val_list = []
				# for col_loop, this_col in enumerate(current_peak_val[4:]):
				for col_num, col_val in enumerate(col_types):
					# real_ind = col_loop + 4
					if col_types[col_num] == 1:

						# try:
						file_vals = [in_file_dict.get(col_num)(\
								[float(num) for num in this_file]) for \
							this_file in current_peak_val[col_num] if \
							len(this_file) > 0]
						# except:
						# 	print(special_col_list)
						# 	print(len(list_peak_lists[0][0]), len(all_peaks_list[0]))
						# 	print(list_peak_lists[0][0])
						# 	print(current_peak_val[col_num])
						# 	print(current_peak_val)
						# 	raise

						all_val_list.append(val_sep_str.join([str(file_val) for \
							file_val in file_vals]))
						this_col_val = bw_file_dict.get(col_num)(file_vals)
						return_peak.append(str(this_col_val))
					elif col_num > 3:
						return_peak.append(current_peak_val[col_num])
				if keep_all_cols:
					return_peak.append(val_sep_str.join(which_files))
					if len(special_col_list) > 1:
						return_peak.extend(all_val_list)
					else:
						return_peak.append(all_val_list)

				merged_peak_list.append(return_peak)
				# merged_peak_list.append(', '.join(
				# 	[str(col) for col in return_peak]))

				current_peak_val = new_current_peak(
							peak, bed_count, file_num, col_types)


	if save_location is not False:
		write_bed(save_location, merged_peak_list)
	return merged_peak_list

def subpro_to_list(to_write):
	to_write = to_write.decode()
	to_write = to_write.split('\n')
	for i in range(len(to_write)):
		to_write[i] = to_write[i].split('\t')
	return to_write

# this won't add a peak if all of its matched genes were matched in the last 
	# "num_to_check" peaks
def check_overlap_sliding_window(window_index, item, search_list, \
	output_list, chr_index=0, peak_min_index=1, peak_max_index=2, \
	indices_to_keep=None):
	finished_list = False
	current_index = window_index
	while True:
		num_to_check = 20
		if len(output_list) > 0:
			if len(output_list) <= num_to_check:
				check_num = len(output_list)
			else: check_num = num_to_check
			if indices_to_keep == None:
				add_index = len(output_list[0]) - 1
			else: add_index = indices_to_keep[1]
			try:
				thing = output_list[-(check_num-1):]
				thing = search_list[current_index][3]
				thing = output_list[-1][add_index]
			except:
				print('output list len:', \
					len(output_list), -(check_num-1))
				print('search list len:', \
					len(search_list[current_index]), 3)
				print('output thing len:', \
					len(output_list[-1]), add_index, '\n')
				raise
			if (search_list[current_index][3] in [thing[add_index] for \
				thing in output_list[-(check_num-1):]]):
				current_index += 1
				continue
		peak_min = int(item[peak_min_index])
		peak_max = int(item[peak_max_index])
		tss_min = int(search_list[current_index][1]) - 10000
		tss_max = int(search_list[current_index][1]) + 10000
		if item[chr_index] == search_list[current_index][0]:
			if ((peak_min <= tss_max) and (peak_max >= tss_min)):
				if indices_to_keep == None:
					output_list.append(\
						item + search_list[current_index][2:])
				else:
					output_list.append(\
						item[indices_to_keep[0]:indices_to_keep[1]] + \
						search_list[current_index][2:])
				current_index += 1
			elif peak_min > tss_max:
				current_index += 1
				window_index += 1
			elif peak_max < tss_min:
				break
			else:
				raise ValueError('missed something')
		elif item[chr_index] > search_list[current_index][0]:
			current_index += 1
			window_index += 1
		elif item[chr_index] < search_list[current_index][0]:
			break
		else:
			raise ValueError('missed something')
		if (current_index >= len(search_list)) or \
			(len(search_list[current_index]) < 3):
			finished_list = True
			break
	return window_index, output_list, finished_list

def progbar(loop_num, total_loop_num, start_time, to_show, \
	how_often=1, message='', bar_length=50, display=True):
	if start_time == None:
		start_time = time.time()
	j = (loop_num % total_loop_num) + 1
	end_epoch = j > total_loop_num - how_often
	if display:
		perc = int(100. * j / total_loop_num)
		prog = ''.join(['='] * (bar_length * perc // 100))
		template = "\r[{:" + str(bar_length) + "s}] {:3d}%. {:s}"
		string = template.format(prog, perc, message)
		total_time = time.time() - start_time
		if total_time > 3600:
			total_time_string = \
				str(int(round((total_time)/3600,0)))+'hrs total'
		elif total_time > 60:
			total_time_string = \
				str(int(round((total_time)/60,0)))+'min total'
		else:
			total_time_string = \
				str(int(round((total_time),0)))+'s total'
		if loop_num > 0:
			time_per_iter = total_time/loop_num
		else:
			time_per_iter = total_time
		if time_per_iter > 3600:
			time_per_iter_string = \
				str(int(round((time_per_iter)/3600,0)))+'hrs per iter'
		elif time_per_iter > 60:
			time_per_iter_string = \
				str(int(round((time_per_iter)/60,0)))+'min per iter'
		else:
			time_per_iter_string = \
				str(int(round((time_per_iter),0)))+'s per iter'
		expected_total = time_per_iter*total_loop_num
		if expected_total > 3600:
			expected_total_string = \
				'expect: '+str(int(round((expected_total)/3600,0)))+'hrs'
		elif expected_total > 60:
			expected_total_string = \
				'expect: '+str(int(round((expected_total)/60,0)))+'min'
		else:
			expected_total_string = \
				'expect: '+str(int(round((expected_total),0)))+'s'
		if to_show == None:
			if expected_total > 2:
				to_show = True
			else:
				to_show = False
		if to_show == True:
			sys.stdout.write(\
				string+'{:<20s} {:<15s} {:<15s} {:<15s}'\
				.format(str(loop_num)+'/'+str(total_loop_num), \
					str(total_time_string), 
				str(time_per_iter_string), str(expected_total_string)))
			sys.stdout.flush()
			if end_epoch:
				sys.stdout.write('\r{:150s}\r'.format(''))
				sys.stdout.flush()
	return start_time, to_show

def grab_samples_for_analysis_step(load_dir, save_dir, data_dict, \
	which_group, which_step, input_common_format, output_common_format, \
	saving_in_load_dir, loading_fused, use_input_groups, print_dropped_types, **kwargs):
	output_object = {}

	if which_group is 'test':
		fuse_groups = kwargs.get('fuse_test')
	else:
		fuse_groups = kwargs.get('fuse_control')
	
	group_list = sorted(list(data_dict.keys()))
		
	for group in group_list:
		group_files = []
		for this_filename in data_dict.get(group):
			group_files.append([this_filename[0], \
				os.path.join(load_dir, this_filename[1])])
			if not os.path.isfile(group_files[-1][1]) and not \
				input_common_format and not saving_in_load_dir:
				print('/'.join(group_files[-1][1].split('/')[:-2] + \
					['all.bed']))
				if os.path.exists('/'.join(group_files[-1][1].split('/')[:-2] + \
					['all.bed'])):
					raise ValueError('Loading from directory {}. \n\tDid not find file {} \
						\n\tbut did find an all common file. Need to change settings for \
						\n\tprevious step to output common, or set "input_common_format"\
						\n\tto true for this step'\
						.format(load_dir, this_filename))
				else:
					if not os.path.exists(os.path.dirname(group_files[-1][1])) and \
						use_input_groups is True:
						print('Not including file {} because the encompassing input sample \
							\ntype does not exist and use_input_groups is True'\
							.format(group_files[-1][1]))
						del group_files[-1]
					else:
						raise ValueError(\
							'File to load: {} for sample {} does not exist'\
							.format(group_files[-1][1], this_filename))

		if save_dir is not None:
			file_convert_list = []
			if not os.path.exists(save_dir):
				raise ValueError(\
					'''Planning to save files in {} but it does not exist. \
					\nIncluded this error in case the file_handler tries to save files in the wrong directory'''\
					.format(save_dir))
			for file in group_files:
				file_convert_list.append([file[0], file[1], \
					file[1].replace(load_dir, save_dir)])
				if fuse_groups is True:
					filename = file_convert_list[-1][2].split('/')
					filename[-2] = 'all'
					file_convert_list[-1][2] = '/'.join(filename)
				if not input_common_format is True:
					if not saving_in_load_dir:
						if not os.path.isfile(file_convert_list[-1][1]):
							all_input = \
								file_convert_list[-1][1].split('/')
							all_input[-2] = 'all'
							if not os.path.isfile(all_input):
								raise ValueError(\
									'There is no input file at:\n\t{}\n\tor\n\t{}'\
									.format(file_convert_list[-1][1], \
										all_input))
							else:
								file_convert_list[-1][1] = all_input
					else:
						if loading_fused is None:
							raise ValueError(\
								'saving_in_load_dir is True, but loading_fused is not given. It is unclear what directory "load" files should be in')
						elif loading_fused is True:
							all_input = \
								file_convert_list[-1][1].split('/')
							all_input[-2] = 'all'
							file_convert_list[-1][1] = all_input

			if output_common_format is not True:
				os.makedirs(os.path.dirname(file_convert_list[-1][2]), \
					exist_ok=True)
			if input_common_format is not True and saving_in_load_dir is \
				True:
				os.makedirs(os.path.dirname(file_convert_list[-1][1]), \
					exist_ok=True)
		else:
			file_convert_list = group_files

		if input_common_format is not True and output_common_format is \
			not True:
			if fuse_groups is True:
				if output_object.get('all') is None:
					output_object['all'] = []
				output_object['all'].extend(file_convert_list)
			elif fuse_groups is False:
				output_object[group] = file_convert_list
		else:
			all_format = '/'.join(list(data_dict.get(group))[0][1]\
				.split('/')[:-2])+'/all.bed'
			group_format = '/'.join(list(data_dict.get(group))[0][1]\
				.split('/')[:-1])+'.bed'
			if fuse_groups is True:
				output_format = all_format
				output_key = 'all'
			else:
				output_format = group_format
				output_key = group
			if input_common_format is True:
				input_value = os.path.join(load_dir, output_format)
				if saving_in_load_dir is True:
					os.makedirs(os.path.dirname(input_value), \
						exist_ok=True)
				else:
					if not os.path.isfile(input_value):
						if use_input_groups is True:
							if print_dropped_types:
								print('Removing sample type {} for step {} because \
									\n\tit is not found in the \
									\n\tinput sample and use_input_groups is True'\
									.format(group, which_step))
							continue
						else:
							raise ValueError(\
								'Input file {} does not exist.'\
									.format(input_value))
			else:
				input_value = [[sample[0], sample[1]] for sample in \
					file_convert_list]
			if save_dir is not None:		
				if output_common_format is True:
					output_value = os.path.join(save_dir, output_format)
					os.makedirs(os.path.dirname(output_value), \
						exist_ok=True)
				else:
					output_value = [[sample[0], sample[2]] for \
						sample in file_convert_list]
			if fuse_groups is True:
				if save_dir is not None:		
					if output_object.get(output_key) is None:
						output_object[output_key] = [input_value, \
							output_value]
					else:
						if input_common_format is not True:
							output_object[output_key][0]\
								.extend(input_value)
						if output_common_format is not True:
							output_object[output_key][1].extend(output_value)
				else:
					if output_object.get(output_key) is None:
						output_object[output_key] = [input_value]
					else:
						if input_common_format is not True:
							output_object[output_key][0]\
								.extend(input_value)
			else:
				if save_dir is not None:		
					output_object[output_key] = [input_value, \
						output_value]
				else:
					output_object[output_key] = [input_value]

	return output_object
	

def make_file_handler(file_settings_dict, test_data_dict, \
	control_data_dict, test_group_name, control_group_name, print_dropped_types):
	# This is a factory function that generates a file_handler 
		# function for handling files through the pipeline
	# explanation of arguments:
		# file_settings_dict: this is from the settings dictionary, 
			# it allows the user to supply settings for certain function
			# eg, whether or not to include control samples in 
			# certain plots, and whether or not to consider all 
			# control samples as a single group
			# this dictionary only concerns settings from the 
			# "settings json" that the user is allowed to change. 
			# This is over-ridden in some steps of the
			# pipeline that have implications on downstream analysis
		# test_data_dict: this supplies sample names and 
			# relative file paths for every sample. These relative 
			# paths will be joined with a path to the directory
			# they're in for each pipeline step
		# control_data_dict: same as above but for control samples

	if print_dropped_types is None:
		print_dropped_types = True

	def file_handler(which_step, load_dir, save_dir = None, **kwargs):
		# this is the file handler function that should take care 
			# of generating a file dictionary for each step
			# part of the goal here is that if file dictionaries 
			# are generated as the input for every step, then 
			# these functions can be
			# easily re-purposed for other analysis in the future. 
			# I don't want the argument of any function to be a 
			# pipeline-specific settings dictionary
		# explanation of arguments:
			# which_step just says which pipeline step we're 
				# loading for. Some loading decisions are up to the 
				# user (in the settings file), and this just
				# tells the function which step to check from the 
				# settings file
			# load_dir: passes the filepath of a directory to the 
				# function for generating filepaths. This lets the 
				# save_dirs dictionary be changed
				# and send its paths to this function dynamically 
				# without issues
			# save_dir: same as for load_dir, except save_dir is 
				# allowed to be "None". If save_dir is None, this 
				# function will only return 1 filepath per sample/type
			# input_common_format: lets the function if the inputs 
				# are from common files (eg, a single common file 
				# for all BRCA common peaks rather than 
				# individual files for the peaks for every sample)
			# output_common_format: lets the function know if the 
				# output should be a common file (eg, the output of 
				# the "common" step, or most following steps)
			# all_common: lets the function know if it should return 
				# an "all common" filepath as well. This is relevant 
				# because we do produce a peak list based on
				# first: common peaks w/in a cancer type -> second: 
				# common peaks b/w cancer types
				# can take "None", "False", "True", or "only". 
				# None and False don't produce an all common filepath. 
				# True produces all common filepath and 
				# the other filepaths, while "only" produces only 
				# the all-common filepath
			# fuse_test: fuse the directories for all test files. 
				# helpful if there are a small number of test files 
				# and you want to treat them as a single group
			# fuse_control: same for control samples. We used this 
				# in our analysis because we have about 11 control 
				# samples (across 8 tissues) compared to 85 test samples
			# drop_test: allows you to only return files for control 
				# samples. Helpful in some analyses where we wanted 
				# to only look at control samples for certain steps
			# drop_control: same as drop_test, but drops control 
				# samples to only look at test samples. We used this 
				# for some plots.
			# saving_in_load_dir: in certain steps, we needed to 
				# reference >2 directories, (eg, match_tss_gene_names), 
				# so we needed to call this multiple times
				# to generate all of the required filepaths. 
				# This command tells the function to perform certain 
				# actions for the loading directory as though
				# it is the saving directory (eg, it won't error 
				# if a "loading" file doesn't exist, and it will 
				# create the directory to save "loading" files in)
			# loading_fused: this is required only if 
				# saving_in_load_dir is True. If we're loading 
				# from the loading directory, then it can infer if
				# we're loading from a fused directory or an unfused 
				# directory (by checking where files exist)
				# if we're saving in the "loading" directory, 
				# then the function must be told if those files 
				# should be in a fused or unfused directory
				# fused/unfused here means the same thing as it 
				# does for fuse_test/fuse_control above

		file_settings_dict[which_step].update(kwargs)

		if file_settings_dict.get(which_step).get('drop_control') is \
			True and file_settings_dict.get(which_step)\
			.get('drop_test') is True:
			raise ValueError(\
				'Both drop control and drop test are True. This will not return any files.')
		if save_dir is None and file_settings_dict.get(which_step)\
			.get('output_common_format') is not None:
			raise ValueError(\
				'If save_dir is None for file_handler, output_common_format must be None')

		required_keys = ['input_common_format', 'output_common_format', \
			'saving_in_load_dir', 'loading_fused', 'use_input_groups']
		for this_key in required_keys:
			if file_settings_dict.get(which_step).get(this_key) is None:
				file_settings_dict[which_step][this_key] = None

		file_settings_dict[which_step]['print_dropped_types'] = print_dropped_types

		output_dict = {}

		if file_settings_dict.get(which_step).get('all_common') != 'only':
			if file_settings_dict.get(which_step).get('drop_control') \
				is not True:
				output_dict[control_group_name] = \
					grab_samples_for_analysis_step(load_dir, \
					save_dir, control_data_dict, \
					'control', which_step, **file_settings_dict.get(which_step))
			if file_settings_dict.get(which_step).get('drop_test') \
				is not True:
				output_dict[test_group_name] = \
					grab_samples_for_analysis_step(load_dir, \
					save_dir, test_data_dict, \
					'test', which_step, **file_settings_dict.get(which_step))
		if file_settings_dict.get(which_step).get('all_common') is True:
			output_dict['all'] = {'all': \
				[os.path.join(load_dir, 'all.bed')]}
			if save_dir is not None:
				output_dict['all']['all'].append(\
					os.path.join(save_dir, 'all.bed'))

		return output_dict

	return file_handler






















































