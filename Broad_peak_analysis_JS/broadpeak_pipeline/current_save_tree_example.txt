{
  "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E",
  "loaded_data_dir": {
    "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/loaded_data"
  },
  "data_for_figures": {
    "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/data_for_figures"
  },
  "merged_dir": {
    "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/merged_beds"
  },
  "core_pipeline_dirs": {
    "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/core_pipeline_dirs",
    "top_plot_dir": {
      "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/core_pipeline_dirs/general_plots"
    },
    "analyses": {
      "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/core_pipeline_dirs/analyses",
      "4kb_broad_25_50": {
        "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/core_pipeline_dirs/4kb_broad_25_50",
        "split_dir": {
          "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/core_pipeline_dirs/4kb_broad_25_50/split_beds"
        },
        "common_save": {
          "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/core_pipeline_dirs/4kb_broad_25_50/common_beds"
        },
        "tss_gene_dir": {
          "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/core_pipeline_dirs/4kb_broad_25_50/tss_intersect_gene_names"
        },
        "gene_dir": {
          "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/core_pipeline_dirs/4kb_broad_25_50/gene_lists"
        },
        "plot_dir": {
          "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/core_pipeline_dirs/4kb_broad_25_50/plots"
        }
      },
      "1kb_sharp_25_50": {
        "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/core_pipeline_dirs/1kb_sharp_25_50",
        "split_dir": {
          "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/core_pipeline_dirs/1kb_sharp_25_50/split_beds"
        },
        "common_save": {
          "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/core_pipeline_dirs/1kb_sharp_25_50/common_beds"
        },
        "tss_gene_dir": {
          "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/core_pipeline_dirs/1kb_sharp_25_50/tss_intersect_gene_names"
        },
        "gene_dir": {
          "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/core_pipeline_dirs/1kb_sharp_25_50/gene_lists"
        },
        "plot_dir": {
          "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/core_pipeline_dirs/1kb_sharp_25_50/plots"
        }
      }
    }
  },
  "comparison_pipeline_dirs": {
    "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/comparison_pipeline_dirs",
    "comp_analysis_0": {
      "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/comparison_pipeline_dirs/4kb_broad_25_50_v_1kb_sharp_25_50",
      "unique_common": {
        "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/comparison_pipeline_dirs/4kb_broad_25_50_v_1kb_sharp_25_50/unique_common_beds_for_comparison",
        "normal_common_beds": {
          "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/comparison_pipeline_dirs/4kb_broad_25_50_v_1kb_sharp_25_50/unique_common_beds_for_comparison/normal_common_beds",
          "group1_normal_common": {
            "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/comparison_pipeline_dirs/4kb_broad_25_50_v_1kb_sharp_25_50/unique_common_beds_for_comparison/group1_normal_common"
          },
          "group2_normal_common": {
            "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/comparison_pipeline_dirs/4kb_broad_25_50_v_1kb_sharp_25_50/unique_common_beds_for_comparison/group2_normal_common"
          }
        },
        "unique_peak_beds": {
          "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/comparison_pipeline_dirs/4kb_broad_25_50_v_1kb_sharp_25_50/unique_common_beds_for_comparison/unique_peak_beds",
          "g1_control_subtract_g2_control": {
            "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/comparison_pipeline_dirs/4kb_broad_25_50_v_1kb_sharp_25_50/unique_common_beds_for_comparison/g1_control_subtract_g2_control"
          },
          "g1_test_subtract_g2_test": {
            "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/comparison_pipeline_dirs/4kb_broad_25_50_v_1kb_sharp_25_50/unique_common_beds_for_comparison/g1_test_subtract_g2_test"
          },
          "g2_control_subtract_g1_control": {
            "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/comparison_pipeline_dirs/4kb_broad_25_50_v_1kb_sharp_25_50/unique_common_beds_for_comparison/g2_control_subtract_g1_control"
          },
          "g2_test_subtract_g1_test": {
            "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/comparison_pipeline_dirs/4kb_broad_25_50_v_1kb_sharp_25_50/unique_common_beds_for_comparison/g2_test_subtract_g1_test"
          }
        }
      },
      "peak_sizes": {
        "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/comparison_pipeline_dirs/4kb_broad_25_50_v_1kb_sharp_25_50/unique_peak_size_beds_for_comparison",
        "unique_g1_control_vs_test": {
          "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/comparison_pipeline_dirs/4kb_broad_25_50_v_1kb_sharp_25_50/unique_peak_size_beds_for_comparison/unique_g1_control_vs_test",
          "reference_name": {
            "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/comparison_pipeline_dirs/4kb_broad_25_50_v_1kb_sharp_25_50/unique_peak_size_beds_for_comparison/unique_g1_control_vs_test/unique_g1_peak_lengths"
          },
          "comparison_name": {
            "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/comparison_pipeline_dirs/4kb_broad_25_50_v_1kb_sharp_25_50/unique_peak_size_beds_for_comparison/unique_g1_control_vs_test/comparison_test_peak_lengths"
          }
        },
        "unique_g2_control_vs_test": {
          "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/comparison_pipeline_dirs/4kb_broad_25_50_v_1kb_sharp_25_50/unique_peak_size_beds_for_comparison/unique_g2_control_vs_test",
          "reference_name": {
            "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/comparison_pipeline_dirs/4kb_broad_25_50_v_1kb_sharp_25_50/unique_peak_size_beds_for_comparison/unique_g2_control_vs_test/unique_g2_peak_lengths"
          },
          "comparison_name": {
            "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/comparison_pipeline_dirs/4kb_broad_25_50_v_1kb_sharp_25_50/unique_peak_size_beds_for_comparison/unique_g2_control_vs_test/comparison_test_peak_lengths"
          }
        },
        "unique_g1_test_vs_control": {
          "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/comparison_pipeline_dirs/4kb_broad_25_50_v_1kb_sharp_25_50/unique_peak_size_beds_for_comparison/unique_g1_test_vs_control",
          "reference_name": {
            "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/comparison_pipeline_dirs/4kb_broad_25_50_v_1kb_sharp_25_50/unique_peak_size_beds_for_comparison/unique_g1_test_vs_control/unique_g1_peak_lengths"
          },
          "comparison_name": {
            "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/comparison_pipeline_dirs/4kb_broad_25_50_v_1kb_sharp_25_50/unique_peak_size_beds_for_comparison/unique_g1_test_vs_control/comparison_control_peak_lengths"
          }
        },
        "unique_g2_test_vs_control": {
          "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/comparison_pipeline_dirs/4kb_broad_25_50_v_1kb_sharp_25_50/unique_peak_size_beds_for_comparison/unique_g2_test_vs_control",
          "reference_name": {
            "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/comparison_pipeline_dirs/4kb_broad_25_50_v_1kb_sharp_25_50/unique_peak_size_beds_for_comparison/unique_g2_test_vs_control/unique_g2_peak_lengths"
          },
          "comparison_name": {
            "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/comparison_pipeline_dirs/4kb_broad_25_50_v_1kb_sharp_25_50/unique_peak_size_beds_for_comparison/unique_g2_test_vs_control/comparison_control_peak_lengths"
          }
        }
      },
      "comparison": {
        "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/comparison_pipeline_dirs/4kb_broad_25_50_v_1kb_sharp_25_50/peak_size_comparison_beds",
        "g1_control_vs_test_comparsion": {
          "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/comparison_pipeline_dirs/4kb_broad_25_50_v_1kb_sharp_25_50/peak_size_comparison_beds/g1_control_vs_test_comparsion"
        },
        "g2_control_vs_test_comparsion": {
          "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/comparison_pipeline_dirs/4kb_broad_25_50_v_1kb_sharp_25_50/peak_size_comparison_beds/g2_control_vs_test_comparsion"
        },
        "g1_test_vs_control_comparsion": {
          "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/comparison_pipeline_dirs/4kb_broad_25_50_v_1kb_sharp_25_50/peak_size_comparison_beds/g1_test_vs_control_comparsion"
        },
        "g2_test_vs_control_comparsion": {
          "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/comparison_pipeline_dirs/4kb_broad_25_50_v_1kb_sharp_25_50/peak_size_comparison_beds/g2_test_vs_control_comparsion"
        }
      }
    }
  },
  "length_change_pipeline_dirs": {
    "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/length_change_pipeline_dirs",
    "control": {
      "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/length_change_pipeline_dirs/referencing_control",
      "comparison_setup": {
        "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/length_change_pipeline_dirs/referencing_control/comparison_setup",
        "common_beds": {
          "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/length_change_pipeline_dirs/referencing_control/comparison_setup/common_beds"
        },
        "peak_size_beds_for_comparison": {
          "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/length_change_pipeline_dirs/referencing_control/comparison_setup/peak_size_beds_for_comparison"
        },
        "size_comparison_beds": {
          "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/length_change_pipeline_dirs/referencing_control/comparison_setup/size_comparison_beds"
        }
      },
      "length_change_analysis": {
        "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/length_change_pipeline_dirs/referencing_control/length_change_analysis",
        "split_dir": {
          "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/length_change_pipeline_dirs/referencing_control/length_change_analysis/split_beds"
        },
        "common_save": {
          "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/length_change_pipeline_dirs/referencing_control/length_change_analysis/common_beds"
        },
        "tss_gene_dir": {
          "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/length_change_pipeline_dirs/referencing_control/length_change_analysis/tss_intersect_gene_names"
        },
        "gene_dir": {
          "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/length_change_pipeline_dirs/referencing_control/length_change_analysis/gene_lists"
        },
        "plot_dir": {
          "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/length_change_pipeline_dirs/referencing_control/length_change_analysis/plots"
        }
      },
      "length_and_height_change_analysis": {
        "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/length_change_pipeline_dirs/referencing_control/length_and_height_change_analysis",
        "split_dir": {
          "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/length_change_pipeline_dirs/referencing_control/length_and_height_change_analysis/split_beds"
        },
        "common_save": {
          "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/length_change_pipeline_dirs/referencing_control/length_and_height_change_analysis/common_beds"
        },
        "tss_gene_dir": {
          "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/length_change_pipeline_dirs/referencing_control/length_and_height_change_analysis/tss_intersect_gene_names"
        },
        "gene_dir": {
          "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/length_change_pipeline_dirs/referencing_control/length_and_height_change_analysis/gene_lists"
        },
        "plot_dir": {
          "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/length_change_pipeline_dirs/referencing_control/length_and_height_change_analysis/plots"
        }
      }
    },
    "test": {
      "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/length_change_pipeline_dirs/referencing_test",
      "comparison_setup": {
        "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/length_change_pipeline_dirs/referencing_test/comparison_setup",
        "common_beds": {
          "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/length_change_pipeline_dirs/referencing_test/comparison_setup/common_beds"
        },
        "peak_size_beds_for_comparison": {
          "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/length_change_pipeline_dirs/referencing_test/comparison_setup/peak_size_beds_for_comparison"
        },
        "size_comparison_beds": {
          "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/length_change_pipeline_dirs/referencing_test/comparison_setup/size_comparison_beds"
        }
      },
      "length_change_analysis": {
        "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/length_change_pipeline_dirs/referencing_test/length_change_analysis",
        "split_dir": {
          "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/length_change_pipeline_dirs/referencing_test/length_change_analysis/split_beds"
        },
        "common_save": {
          "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/length_change_pipeline_dirs/referencing_test/length_change_analysis/common_beds"
        },
        "tss_gene_dir": {
          "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/length_change_pipeline_dirs/referencing_test/length_change_analysis/tss_intersect_gene_names"
        },
        "gene_dir": {
          "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/length_change_pipeline_dirs/referencing_test/length_change_analysis/gene_lists"
        },
        "plot_dir": {
          "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/length_change_pipeline_dirs/referencing_test/length_change_analysis/plots"
        }
      },
      "length_and_height_change_analysis": {
        "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/length_change_pipeline_dirs/referencing_test/length_and_height_change_analysis",
        "split_dir": {
          "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/length_change_pipeline_dirs/referencing_test/length_and_height_change_analysis/split_beds"
        },
        "common_save": {
          "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/length_change_pipeline_dirs/referencing_test/length_and_height_change_analysis/common_beds"
        },
        "tss_gene_dir": {
          "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/length_change_pipeline_dirs/referencing_test/length_and_height_change_analysis/tss_intersect_gene_names"
        },
        "gene_dir": {
          "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/length_change_pipeline_dirs/referencing_test/length_and_height_change_analysis/gene_lists"
        },
        "plot_dir": {
          "path": "/Users/jschulz1/Documents/project_data/ccle_project/analyses/test_2020_01_28/E/length_change_pipeline_dirs/referencing_test/length_and_height_change_analysis/plots"
        }
      }
    }
  }
}