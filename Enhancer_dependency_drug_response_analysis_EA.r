{
  library(DESeq2)
  deseq_comp <- function(matrix_counts, class_cl, de_genes=TRUE, get_counts=FALSE){
    sensitive <- which(class_cl == 1)
    resistant <- which(class_cl == 0)
    if(any(c(length(sensitive), length(resistant)) == 0)){
      sensitive <- 1:2
      resistant <- 3:ncol(matrix_counts)
    }
    aux_data <- matrix_counts[,c(sensitive,resistant)]
    aux_desc <- data.frame(condition=as.factor(c(rep("sensitive",length(sensitive)),rep("resistant",length(resistant)))), type=rep("paired-end",c(length(sensitive)+length(resistant))))
    aux_dds <- DESeqDataSetFromMatrix(countData = aux_data, colData = aux_desc, design = ~condition)
    aux_dds <- DESeq(aux_dds, fitType="mean")
    res_list <- list()
    if(de_genes){
      aux_dds_res <- as.data.frame(results(aux_dds))
      res_list[["deseq"]] <- aux_dds_res
    }
    else{
      res_list[["deseq"]] <- NA
    }
    aux_norm_count <- assay(normTransform(aux_dds))
    aux_count_norm <- c()
    if(get_counts){
      aux_count_norm <- counts(aux_dds, normalized=TRUE)
      colnames(aux_count_norm) <- colnames(aux_data)
    }
    res_list[["counts_norm"]] <- aux_count_norm
    colnames(aux_norm_count) <- colnames(aux_data)
    res_list[["norm_counts"]] <- aux_norm_count
    res_list
  }

  ic50_class <- function(ic50_samples){
    ic50_samples_bin <- ifelse(ic50_samples <= 0.2, 1, 0)
    ic50_samples_bin_table <- table(ic50_samples_bin)
    ic50_comp <- FALSE
    if(length(ic50_samples_bin_table) > 1 & all(ic50_samples_bin_table > 1))
      ic50_comp <- TRUE
    ic50_list <- list()
    ic50_list[["boolean"]] <- ic50_comp
    ic50_list[["classes"]] <- ic50_samples_bin
    ic50_list
  }

  cor_p_val <- function(x,y){
    aux <- cor.test(x, y)
    aux_vec <- c(aux$estimate, aux$p.value)
    names(aux_vec) <- c("Cor","p_value")
    aux_vec
  }

  auc_calc <- function(count_mat, auc_vec){
    aux_cor <- t(apply(count_mat, 1, function(x) cor_p_val(x, auc_vec)))
    aux_cor <- cbind(aux_cor, p.adjust(aux_cor[,2], method="fdr"))
    colnames(aux_cor)[3] <- "FDR"
    aux_cor <- aux_cor[order(aux_cor[,2]),]
    aux_cor
  }

  feature_variation <- function(feature){
    aux_mean <- mean(feature)
    aux_sd <- sd(feature)
    coef_var <- aux_sd/aux_mean
    aux_mad <- mad(feature)
    aux_min <- min(feature)
    aux_max <- max(feature)
    aux_len_high <- length(which(feature >= aux_mean))
    aux_len_low <- length(which(feature < aux_mean))
    c(aux_mean, aux_min, aux_max, aux_len_low, aux_len_high, coef_var, aux_mad)
  }


  getDESeqData2 <- function(count_m, ic50_m=NULL, auc_m=NULL, compound, tissue=NULL, tissue_set, get_counts=FALSE){
    aux_counts <- c()
    aux_auc <- c()
    if(!is.null(auc_m)){
      aux_auc <- auc_m[compound,]
      aux_auc <- aux_auc[which(!is.na(aux_auc))]
      if(!is.null(tissue)){
        aux_auc <- auc_m[compound, which(tissue_set == tissue)]
        aux_auc <- aux_auc[which(!is.na(aux_auc))]
      }
      aux_counts <- count_m[,match(names(aux_auc), colnames(count_m))]
    }
    aux_ic50 <- c()
    ic50_obj <- c()
    if(!is.null(ic50_m)){
      aux_ic50 <- ic50_m[compound,]
      aux_ic50 <- aux_ic50[which(!is.na(aux_ic50))]
      if(!is.null(tissue)){
        aux_ic50 <- ic50_m[compound, which(tissue_set == tissue)]
        aux_ic50 <- aux_ic50[which(!is.na(aux_ic50))]
      }
      aux_counts <- count_m[,match(names(aux_ic50), colnames(count_m))]
      ic50_obj <- ic50_class(aux_ic50)
    }
    aux_deseq <- deseq_comp(aux_counts, ic50_obj$classes, ic50_obj$boolean, get_counts)
    list(deseq = aux_deseq$deseq,norm_counts=aux_deseq$norm_counts, ic50=aux_ic50, auc_vec=aux_auc, counts_norm=aux_deseq$counts_norm)
  }



  # most current function
  getDESeqData <- function(ccle_chip_counts, ctrp_auc, cytrpv2auc_samples_bin,de_genes=TRUE, get_counts=TRUE){
    count_s = ccle_chip_counts[,names(ctrp_auc)[which(cytrpv2auc_samples_bin == 1)]]
    count_r = ccle_chip_counts[,names(ctrp_auc)[which(cytrpv2auc_samples_bin == 0)]]

    aux_data <- cbind(count_s,count_r)
    aux_desc <- data.frame(condition=as.factor(c(rep("sensitive",dim(count_s)[2]),rep("resistant",dim(count_r)[2]))), type=rep("paired-end",c(dim(count_s)[2]+dim(count_r)[2])))
    aux_dds <- DESeqDataSetFromMatrix(countData = aux_data, colData = aux_desc, design = ~condition)
    aux_dds <- DESeq(aux_dds, fitType="mean")
    res_list <- list()
    if(de_genes){
      aux_dds_res <- as.data.frame(results(aux_dds))
      res_list[["deseq"]] <- aux_dds_res
    }
    else{
      res_list[["deseq"]] <- NA
    }
    aux_norm_count <- assay(normTransform(aux_dds))
    aux_count_norm <- c()
    if(get_counts){
      aux_count_norm <- counts(aux_dds, normalized=TRUE)
      colnames(aux_count_norm) <- colnames(aux_data)
    }
    res_list[["counts_norm"]] <- aux_count_norm
    colnames(aux_norm_count) <- colnames(aux_data)
    res_list[["norm_counts"]] <- aux_norm_count
    res_list
    aux_deseq = res_list

    list(deseq = aux_deseq$deseq,norm_counts=aux_deseq$norm_counts, counts_norm=aux_deseq$counts_norm)
  }


  avana_func <- function(sens,res, up_th,lo_th, method = 'majority'){

    if(method == 'all'){
      less_ind_sens <- apply(sens,1,function(x) all(x < lo_th))
      high_ind_sens <- apply(sens,1,function(x) all(x > up_th))
      less_ind_res <- apply(res,1,function(x) all(x < lo_th))
      high_ind_res <- apply(res,1,function(x) all(x > up_th))
    }
    if(method == 'majority'){
      less_ind_sens <- apply(sens,1,function(x) sum(x < lo_th) > (0.5 * length(x)))
      high_ind_sens <- apply(sens,1,function(x) sum(x > up_th) > (0.5 * length(x)))
      less_ind_res <- apply(res,1,function(x) sum(x < lo_th) > (0.5 * length(x)))
      high_ind_res <- apply(res,1,function(x) sum(x > up_th) > (0.5 * length(x)))
    }

    thres_filtered <- names(c(which(less_ind_sens * high_ind_res == 1), which(high_ind_sens * less_ind_res == 1)))

    return(thres_filtered)
  }

  avana_func2 <- function(sens,res){
    dta <- cbind(sens,res)
    n_s <- dim(sens)[2]
    n_r <- dim(res)[2]
    t.result <- apply(dta, 1, function (x) t.test(x[1:n_s],x[(n_s+1):(n_s + n_r)]))

    names(which(unlist(lapply(t.result, function(x) x$p.value)) < 0.01))
  }

}



{
  setwd("~/Box Sync//Working Projects/CCLE Drug Response Analysis/Datasets/")

  loadRData <- function(fileName){
    #loads an RData file, and returns it
    load(fileName)
    get(ls()[ls() != "fileName"])
  }

  dta <- loadRData('CCLE_H3K27ac_READS.RData')


  H3K4me1_rpkm<- dba.peakset(dta, bRetrieve=TRUE,
                             DataType=DBA_DATA_FRAME)
  dat.enhancer<- H3K4me1_rpkm[,c(4:ncol(H3K4me1_rpkm))]
  rownames(dat.enhancer) <- paste0(H3K4me1_rpkm[,1],":",H3K4me1_rpkm[,2],"-",
                                   H3K4me1_rpkm[,3])
  colnames(dat.enhancer) = str_replace_all(colnames(dat.enhancer),'\\.','-')


  cell_info <- read.csv('CCLE_Chip.csv')

}
{
  H3K27ac.peaks.GR<- GRanges(seq = H3K4me1_rpkm$CHR,
                             IRanges(start = H3K4me1_rpkm$START,
                                     end = H3K4me1_rpkm$END))

  ## get all the TSS
  library(TxDb.Hsapiens.UCSC.hg19.knownGene)
  UCSC.hg19.genes<- genes(TxDb.Hsapiens.UCSC.hg19.knownGene)
  UCSC.hg19.promoters<- promoters(UCSC.hg19.genes, upstream = 2500, downstream = 2500)

  library(AnnotationDbi)
  library("org.Hs.eg.db")

  ## note that dplyr and AnnotationDbi both have a function called select
  ## use dplyr::select when use dplyr

  gene_symbol<- AnnotationDbi::select(org.Hs.eg.db, keys=UCSC.hg19.promoters$gene_id,
                                      columns="SYMBOL", keytype="ENTREZID")

  all.equal(UCSC.hg19.promoters$gene_id, gene_symbol$ENTREZID)

  UCSC.hg19.promoters$gene_id<- gene_symbol$SYMBOL

  ## I just want to exclude H3K27ac peaks overlap with promoters, so I set select ="first"
  ## and type = "any"
  H3K27ac.promoters.overlaps<- findOverlaps(H3K27ac.peaks.GR, UCSC.hg19.promoters,
                                            type = "any", select = "first", ignore.strand= TRUE)
  H3K27ac.peaks.GR

  ## index of GRanges which do not overlap with any known TSSs
  enhancer.index<- which(is.na(H3K27ac.promoters.overlaps))
  ## index of prmoters
  promoter.index<- which(!is.na(H3K27ac.promoters.overlaps))

  ## subset the reads_CPM by this index

  reads.CPM.enhancers<- H3K4me1_rpkm[enhancer.index,]

  reads.CPM.promoters<- H3K4me1_rpkm[promoter.index, ]



  dat.enhancer<- reads.CPM.enhancers[,c(4:ncol(reads.CPM.enhancers))]

  rownames(dat.enhancer) <- paste0(reads.CPM.enhancers[,1],":",reads.CPM.enhancers[,2],"-",
                                   reads.CPM.enhancers[,3])

}



{

  setwd("~/Documents/projects/Drug Response/Dataset/")
  ###Response data curation:
  ResponseData <- read.csv("CCLE_NP24.2009_Drug_data_2015.02.24.csv")

  cell_lines <- sapply(ResponseData$CCLE.Cell.Line.Name,function(x) strsplit(as.character(x),split = '_')[[1]][1])

  ResponseData = ResponseData[!is.na(match(as.character(ResponseData$CCLE.Cell.Line.Name),cell_info$CCLE_ID)),]

  ResponseData$CCLE.Cell.Line.Name <- droplevels(ResponseData$CCLE.Cell.Line.Name)



  # Act Area – the area above the fitted dose response curve (inverse measure of AUC in Sanger);
  # IC50 – the same as in Sanger; EC50 – the concentration at which the compound reaches 50% of
  # its maximum reduction in cell viability.

  ## We rearrange drug responses (IC50, AUC, RMSE, ...) into a matrix, with cell lines as rows and drugs as columns:
  #Initializing drug response matrices:
  CCLE_IC50 <- CCLE_EC50 <- CCLE_ActArea <- CCLE_Amax <- matrix(NA, nrow = nlevels(ResponseData$CCLE.Cell.Line.Name),
                                                                ncol = nlevels(ResponseData$Compound),
                                                                dimnames = list(levels(ResponseData$CCLE.Cell.Line.Name),levels(ResponseData$Compound)))

  #We use progress package for making a more sophisticated progress bar for our loops:

  pb <- progress_bar$new(format = "(:spin) [:bar] :percent eta: :eta",
                         total = length(CCLE_IC50), clear = FALSE)

  #These loops fill drug response matrices we initialized above:
  for(CellLine in levels(ResponseData$CCLE.Cell.Line.Name)){
    for(DrUg in levels(ResponseData$Compound)){
      bullseyeIndex <- ResponseData$Compound %in% DrUg & ResponseData$CCLE.Cell.Line.Name %in% CellLine
      if(sum(bullseyeIndex)>1) stop("Something's wrong: couldn't match properly!!")
      if(sum(bullseyeIndex)==1){
        CCLE_IC50[CellLine, DrUg] <- ResponseData$"IC50..uM."[bullseyeIndex]
        CCLE_EC50[CellLine, DrUg] <- ResponseData$"EC50..uM."[bullseyeIndex]
        CCLE_ActArea[CellLine, DrUg] <- ResponseData$ActArea[bullseyeIndex]
        CCLE_Amax[CellLine, DrUg] <- ResponseData$Amax[bullseyeIndex]
      }
      pb$tick()
    }
  }


  colnames(CCLE_IC50) = droplevels(cell_info[match(colnames(CCLE_IC50),cell_info$CCLE_ID),]$ChIP.seq_ID)

  # Percentages of IC50
  threshold <- 1.2
  apply(CCLE_IC50,2,function(x)
    table(ifelse(x <= threshold, 'Sensitive', 'Resistant'))/length(x[!is.na(x)])
  )

  apply(CCLE_EC50,2,function(x)
    table(ifelse(x <= threshold, 'Sensitive', 'Resistant'))/length(x[!is.na(x)])
  )


  ggplot(gather(as.data.frame(CCLE_IC50)), aes(value)) +
    geom_histogram(bins = 10,col="white",aes(fill=..count..)) +
    facet_wrap(~key, scales = 'free_x') + xlab("Drug Sensitivity") +
    ylab("Counts") +
    scale_fill_gradient("Count", low="darkblue", high="green")

  # geom_text(stat='count', aes(label=..count..), position = position_stack(vjust = 0.5),size=4)
  # stat_bin(geom="text", aes(label=..count..) ,
  #          vjust = -1)
  # geom_text(data=as.data.frame(IC50), aes(label=count, y=ypos), colour="white", size=3.5)
  # stat_bin(aes(y=..count.., label=..count..), geom="text", vjust=-.5)


  ggplot(gather(as.data.frame(CCLE_EC50)), aes(value)) +
    geom_histogram(bins = 10,col="white",aes(fill=..count..)) +
    facet_wrap(~key, scales = 'free_x') + xlab("Drug Sensitivity") +
    ylab("Counts") +
    scale_fill_gradient("Count", low="darkblue", high="green")


  ggplot(gather(as.data.frame(CCLE_ActArea)), aes(value)) +
    geom_histogram(bins = 10,col="white",aes(fill=..count..)) +
    facet_wrap(~key, scales = 'free_x') + xlab("Drug Sensitivity") +
    ylab("Counts") +
    scale_fill_gradient("Count", low="darkblue", high="green")


}


{
  gdc_drug <- read.xlsx('GDC_dose_response.xlsx')
  gdc_drug$CELL_LINE_NAME <- as.factor(gdc_drug$CELL_LINE_NAME)
  gdc_drug$DRUG_NAME <- as.factor(gdc_drug$DRUG_NAME)
  GDC_IC50 <- GDC_AUC <- matrix(NA, nrow = nlevels(gdc_drug$CELL_LINE_NAME),
                                ncol = nlevels(gdc_drug$DRUG_NAME),
                                dimnames = list(levels(gdc_drug$CELL_LINE_NAME),levels(gdc_drug$DRUG_NAME)))

  #We use progress package for making a more sophisticated progress bar for our loops:

  pb <- progress_bar$new(format = "(:spin) [:bar] :percent eta: :eta",
                         total = length(GDC_IC50), clear = FALSE)

  #These loops fill drug response matrices we initialized above:
  for(CellLine in levels(gdc_drug$CELL_LINE_NAME)){
    for(DrUg in levels(gdc_drug$DRUG_NAME)){
      bullseyeIndex <- gdc_drug$DRUG_NAME %in% DrUg & gdc_drug$CELL_LINE_NAME %in% CellLine
      if(sum(bullseyeIndex)>1) stop("Something's wrong: couldn't match properly!!")
      if(sum(bullseyeIndex)==1){
        GDC_IC50[CellLine, DrUg] <- gdc_drug$LN_IC50[bullseyeIndex]
        GDC_AUC[CellLine, DrUg] <- gdc_drug$AUC[bullseyeIndex]
      }
      pb$tick()
    }
  }



}



{
  # setwd("~/Documents/projects/Drug Response/Dataset/")
  CTRPv2 <- downloadPSet("CTRPv2")
  ctrp <- summarizeSensitivityProfiles(CTRPv2, sensitivity.measure='auc_recomputed')
  # ctrp <- readRDS('CTRPv2_SensitivityMat_AUC_recomputed.rds')

  colnames(ctrp) = str_replace_all(colnames(ctrp),' ','')
  colnames(ctrp) = str_replace_all(colnames(ctrp),'-','')
  colnames(ctrp) = str_replace_all(colnames(ctrp),'\\.','')
  colnames(ctrp) = str_replace_all(colnames(ctrp),'\\(','')
  colnames(ctrp) = str_replace_all(colnames(ctrp),'\\)','')
  colnames(ctrp) = str_replace_all(colnames(ctrp),'/','')
  colnames(ctrp) = toupper(colnames(ctrp))

  cell_lines <- sapply(cell_info$CCLE_ID,function(x) strsplit(as.character(x),split = '_')[[1]][1])

  ctrpv2 <- ctrp[,!is.na( match( colnames(ctrp), cell_lines) ) ]

  colnames(ctrpv2) = droplevels(cell_info$ChIP.seq_ID[match(colnames(ctrp)[which(!is.na( match( colnames(ctrp), cell_lines) ))], cell_lines)])

}

{
  gdsc <- readRDS('GDSC1000_SensitivityMat_AUC_recomputed.rds')

  colnames(gdsc) = str_replace_all(colnames(gdsc),' ','')
  colnames(gdsc) = str_replace_all(colnames(gdsc),'-','')
  colnames(gdsc) = str_replace_all(colnames(gdsc),'\\.','')
  colnames(gdsc) = str_replace_all(colnames(gdsc),'\\(','')
  colnames(gdsc) = str_replace_all(colnames(gdsc),'\\)','')
  colnames(gdsc) = str_replace_all(colnames(gdsc),'/','')
  colnames(gdsc) = toupper(colnames(gdsc))

  cell_lines <- sapply(cell_info$CCLE_ID,function(x) strsplit(as.character(x),split = '_')[[1]][1])

  gdsc1 <- gdsc[,!is.na( match( colnames(gdsc), cell_lines) ) ]

  colnames(gdsc1) = droplevels(cell_info$ChIP.seq_ID[match(colnames(gdsc)[which(!is.na( match( colnames(gdsc), cell_lines) ))], cell_lines)])
}



{
  json_file <- "tommy_data.json"
  enhancer_gene <- fromJSON("tommy_data.json", flatten=TRUE)

  enhancer_gene.GR<- GRanges(seq = paste0('chr',enhancer_gene[,1],''),
                             IRanges(start = as.numeric(enhancer_gene[,2]),
                                     end = as.numeric(enhancer_gene[,3])),
                             gene_id = enhancer_gene[,9])
}



{
  avana <- read.delim('qbf_Avanadata_2018.txt')
  row.names(avana) <- avana$GENE
  avana = avana[,-1]
  #avana <- avana[apply(avana,1, function(x) sum(x>5) > length(x)/2 ),]


  colnames(avana) <- str_replace_all(colnames(avana),'\\.','-')

  IDs <- read.csv('sample_info.csv')
  CCLE_IDs <- read.csv('CCLE_Chip.csv')

  c_ids <- intersect(IDs$DepMap_ID,colnames(avana))
  IDs <- IDs[IDs$DepMap_ID %in% c_ids,]
  IDs <- IDs[match(colnames(avana), IDs$DepMap_ID),]


  c_ids <- match(IDs$CCLE.Name, CCLE_IDs$CCLE_ID)

  IDs <- IDs[!is.na(c_ids),]
  CCLE_IDs <- CCLE_IDs[c_ids[!is.na(c_ids)],]
  IDs$ChIP.seq_ID <- CCLE_IDs$ChIP.seq_ID

  avana <- avana[,!is.na(match(colnames(avana),IDs$DepMap_ID))]
  colnames(avana) <- IDs$ChIP.seq_ID
}

lst <- list()
threshold <- 0.1

ccle_chip_counts <- c()
ccle_chip_counts <- sapply(dta$peaks,function(x) cbind(ccle_chip_counts,x[,6]))
colnames(ccle_chip_counts) = dta$class[1,]
rownames(ccle_chip_counts) = paste0(dta$peaks[[1]][,1],":",dta$peaks[[1]][,2],"-",
                                    dta$peaks[[1]][,3])
ccle_chip_counts = ccle_chip_counts[rownames(dat.enhancer),]
colnames(dat.enhancer) = str_replace_all(colnames(dat.enhancer),'\\.','-')

ctrpv2 = ctrpv2[,intersect(colnames(avana),colnames(ctrpv2))]
ccle_chip_counts = ccle_chip_counts[,intersect(colnames(ctrpv2),colnames(ccle_chip_counts))]
dat.enhancer = dat.enhancer[,intersect(colnames(ctrpv2),colnames(dat.enhancer))]
avana = avana[,intersect(colnames(avana),colnames(ctrpv2))]

tissue_set = unname(sapply(colnames(ctrpv2),function(x) strsplit(x,'-')[[1]][1]))

all_sign_compounds <- c()


i = 0



while(i < length(unique(tissue_set))){
  i <- i+1
  tissue = unique(tissue_set)[i]
  j <- 0



  while(j < length(rownames(ctrpv2))  ){
    j <- j+1
    compound <- rownames(ctrpv2)[j]
    cat(j,'\n')

    ctrp_auc <- ctrpv2[compound, which(tissue_set == tissue)]
    ctrp_auc <- ctrp_auc[!is.na(ctrp_auc)]


    ctrpv2auc_samples_bin <- ctrp_auc <= threshold

    ctauc_samples_bin_table <- table(ctrpv2auc_samples_bin)


    if( length(ctauc_samples_bin_table) > 1 & all(ctauc_samples_bin_table > 1) ){
      res_tis <- getDESeqData(ccle_chip_counts, ctrp_auc, ctrpv2auc_samples_bin,de_genes=TRUE, get_counts=TRUE)
      deseq_regions <- rownames(res_tis$deseq)
      regions <- GRanges(seq = sapply(deseq_regions,function(x) strsplit(x,':')[[1]][1]),
                         IRanges(start = as.numeric(sapply(sapply(deseq_regions,function(x) strsplit(x,':')[[1]][2]),function(x) strsplit(x,'-')[[1]][1])),
                                 end = as.numeric(sapply(sapply(deseq_regions,function(x) strsplit(x,':')[[1]][2]),function(x) strsplit(x,'-')[[1]][2]))))

      enh_gene_overlaps <- findOverlaps(enhancer_gene.GR, regions, minoverlap = 100,
                                        type = "any", select = "first", ignore.strand= TRUE)

      detected_genes <- enhancer_gene.GR[which(!is.na(enh_gene_overlaps)),]$gene_id

      c_genes <- intersect(rownames(avana), detected_genes)
      av_set <- avana[row.names(avana)%in%c_genes,names(ctrpv2auc_samples_bin)]

      diff_genes <- avana_func(av_set[,which(!ctrpv2auc_samples_bin)],av_set[,which(ctrpv2auc_samples_bin)],5,3,method = 'all')

      fit1 <- lmFit(as.matrix(av_set), model.matrix(~as.factor(ctrpv2auc_samples_bin)))
      fit1 <- eBayes(fit1)
      tab <- topTable(fit1,number=nrow(av_set))

      gene_m <- data.frame(genes = c_genes,
                           Avana_pval = -log10(tab[rownames(av_set),]$P.Value),
                           Counts_log2Fold = sapply(c_genes,function(x) mean(res_tis$deseq[which(!is.na(findOverlaps(regions,
                                                                                                                     enhancer_gene.GR[enhancer_gene.GR$gene_id == x,], minoverlap = 100,
                                                                                                                     type = "any", select = "first", ignore.strand= TRUE))),]$log2FoldChange)))
      gene_m$Significant <- ifelse(gene_m$Avana_pval > 2 & abs(gene_m$Counts_log2Fold) > 1, "Sig", "Not Sig")
      gene_m$av_diff <- ifelse(gene_m$genes %in% diff_genes,'Esentiallity Difference','No Difference')
      gene_m$compound <- compound



      if(dim(subset(gene_m, Avana_pval > 2 & abs(gene_m$Counts_log2Fold) > 1 &
                    gene_m$av_diff == 'Esentiallity Difference'))[1] == 0 ) next


      name <- paste(tissue,'__', compound,'.pdf',sep = '')
      name <- gsub(" ", "", name, fixed = TRUE)
      name<- str_remove(name, "/")


      cat('............','\n')
      cat(compound,'\n')
      cat(tissue,'\n')

      all_sign_compounds <- rbind(all_sign_compounds,gene_m)

      imp_genes <- subset(gene_m, Avana_pval > 2 & abs(gene_m$Counts_log2Fold) > 1 &
                            gene_m$av_diff == 'Esentiallity Difference')$genes
      linked_region2gene <- enhancer_gene.GR[which(!is.na(enh_gene_overlaps)),][which(enhancer_gene.GR[which(!is.na(enh_gene_overlaps)),]$gene_id %in% imp_genes)]

      r<- unique(regions[findOverlaps(linked_region2gene, regions, minoverlap = 100,
                                      type = "any", select = "first", ignore.strand= TRUE),])
      lst[[paste(tissue, compound,sep = '__')]] <-list('Sensitive' = names(which(ctrpv2auc_samples_bin == 0)),
                                                       'Resistant' = names(which(ctrpv2auc_samples_bin == 1)),
                                                       'Sensitive_avana' = av_set[(row.names(av_set) %in% imp_genes),which(!ctrpv2auc_samples_bin)],
                                                       'Resistant_avana' = av_set[(row.names(av_set) %in% imp_genes),which(ctrpv2auc_samples_bin)],
                                                       'diff_genes' = imp_genes,
                                                       "linked_region2gene" = unique(linked_region2gene),
                                                       "Enhancer_Count_log2Fold" =  res_tis$deseq[paste(r@seqnames,':',r@ranges,sep = ''),][,2,drop=FALSE])


    }
  }

}
