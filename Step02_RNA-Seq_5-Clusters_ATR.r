################################################################################
# @Author: Ayush T Raman
# Rai Lab, Genome Medicine, MD Anderson
# Date: April 09th, 2018; Updated: May 20th, 2018
#
# Program is used for:
# 1. Clusters based on CCLE RNA-seq dataset (CCLE Broad Ver. 2018)
################################################################################

rm(list =ls())

## Library
library(CancerSubtypes)
library(DESeq2)
library(dplyr)
library(genefilter)
library(ggplot2)
library(GenomicRanges)
library(limma)
library(NMF)
library(org.Hs.eg.db)
library(pheatmap)
library(RColorBrewer)
library(Rtsne)
library(tibble)
library(TxDb.Hsapiens.UCSC.hg19.knownGene)

## functions
makeLab = function(x,pc) {
    paste0("PC",pc,": ",x,"% variance")
}

gg_color_hue <- function(n) {
    hues = seq(15, 375, length = n + 1)
    hcl(h = hues, l = 65, c = 100)[1:n]
}

# cols <- RColorBrewer::brewer.pal(n = 8, name = "Set1")
# cols <- c("#4575B4","#77A6CE","#ABD0E4","#FFFFFF","#FEE090", "#FCA86B",
#           "#EF6E48","#D73027")
cols <- RColorBrewer::brewer.pal(n = 8, name = "Set1")
cols[1] <- gg_color_hue(8)[8]
cols[2] <- "dodgerblue"
cols[6] <- "#E41A1C"
cols[8] <- "royalblue4"

## Mapping CCLE ids to Rai Lab ids
CCLE2ID <- read.table(file = "dat/CCLE2RaiLabIDs.txt", header = T, 
                      sep = "\t", quote = "", stringsAsFactors = F)
CCLE2ID$harmonize <- CCLE2ID$CCLE_ID
CCLE2ID$harmonize <- gsub(pattern = "(.*)_(.*)+", replacement = "\\1",
                          CCLE2ID$harmonize)
CCLE2ID.sub <- CCLE2ID[which(!CCLE2ID$harmonize %in% ""),]

##################################################
#
## CCLE Dataset -- Newer Dataset Ver 2018
#
##################################################

dat.count <- read.table(file = "../dat/CCLE_DepMap_18Q1_RNAseq_reads_20180214.gct", 
                        header = T, sep = "\t", quote = "", stringsAsFactors = F, 
                        skip = 2, row.names = 1)
rownames(dat.count) <- paste(dat.count[,1],"_",rownames(dat.count),sep = "")
colnames(dat.count) <- gsub(pattern = "(.*)_(.*)+", replacement = "\\1",
                            colnames(dat.count))
dat.count <- dat.count[,-1]
dat.count <- dat.count[,colnames(dat.count) %in% CCLE2ID$harmonize]
sum(colnames(dat.count) %in% CCLE2ID$harmonize)

## DESeq2 normalization for CCLE dataset only
colData <- CCLE2ID[CCLE2ID$harmonize %in% colnames(dat.count),]
colData <- colData[match(colnames(dat.count),colData$harmonize),]
rownames(colData) <- colData$harmonize
colData$Tissue <- factor(colData$Tissue)

## Filtering non-coding genes, MT and RP11
dat.count <- dat.count %>% rownames_to_column(var = "genes")
dat.count1 <- dplyr::filter(dat.count, 
    !grepl("RP11(.*)|MT-(.*)|RNU6-(.*)|RP([0-9])+-(.*)|5S_rRNA(.*)|7SK_(.*)|(A|B)(C|D|E|F|L|P|X)([0-9])+.([0-9])|Y_RNA(.*)|sno(.*)_", genes))
rownames(dat.count1) <- dat.count1$genes 
dat.count1 <- dat.count1[,-1]
dat.count <- dat.count1

## DESeq2
dds <- DESeqDataSetFromMatrix(countData = as.matrix(dat.count), colData = colData, 
                              design = ~ Tissue)
dds <- estimateSizeFactors(dds)
dat <- counts(dds, normalized=TRUE)
print(dim(dat))
print(dim(dds))

## sizefactor plot 
sizeFactorRatio <- sizeFactors(dds)
readCounts <- colSums(counts(dds))
p1 <- qplot(x = sizeFactors(dds), y = colSums(counts(dds))/1e6) + 
    geom_point(aes(colour = dds$Tissue), size = 5) + scale_colour_manual(values = cols) +
    geom_smooth(method = "lm", se = TRUE, colour = "grey30") + 
    xlab("Size Factor") + ylab("Number of Aligned Reads (in million)") +
    theme(axis.title = element_text(size = 16, face = "bold"),
          axis.text.x = element_text(size = 16, face = "bold", color = "black"),
          axis.text.y = element_text(size = 16, face = "bold", color = "black"),
          plot.margin = unit(c(0.25,0.25,0.25,0.25), "cm"), 
          legend.position = c(.85, .2),
          legend.title = element_blank(), 
          legend.text = element_text(size = 14, face = "bold", color = "black"))
ggsave(filename = "Plots_RNA-Seq/SizeFactorPlot.tiff",
       plot = p1, width = 10, height = 8, dpi = 600, type = "cairo")

## PCA plot
mat <- FSbyMAD(dat, cut.type="topk",value=5000)
pcs <- prcomp(t(log2(mat+1)), center = TRUE)
percentVar <- round(((pcs$sdev) ^ 2 / sum((pcs$sdev) ^ 2)* 100), 2) 
p2 <- ggplot(as.data.frame(pcs$x), aes(PC1,PC2, color = colData$Tissue)) + 
          xlab(makeLab(percentVar[1],1)) + ylab(makeLab(percentVar[2],2)) + 
          geom_point(size = 8) +  scale_colour_manual(values = cols) + theme_classic() +
          theme(legend.text = element_text(size = 16, face = "bold"),
                legend.title = element_text(size = 16, colour = "black", face = "bold"),
                plot.title = element_text(size = 0, face ="bold"),
                axis.title = element_text(size = 18, face = "bold"),
                axis.text.x = element_text(size = 16, face = "bold", color = "black"),
                axis.text.y = element_text(size = 16, face = "bold", color = "black"),
                plot.margin = unit(c(0.5,0.5,0.5,0.5), "cm"))
ggsave(filename = "Plots_RNA-Seq/PCAPlot.tiff",
       plot = p2, width = 10, height = 8, dpi = 600, type = "cairo")

## MDS Plot
dist.matrix <- dist(t(log2(mat+1)))
loc <- cmdscale(dist.matrix)
p3 <- ggplot(data = data.frame(loc), aes(loc[,1],loc[,2], color = colData$Tissue)) + 
        scale_colour_manual(values = cols) + geom_point(size = 8) + 
        xlab("Coord 1") + ylab("Coord 2") + theme_classic() +
        theme(legend.text = element_text(size = 16, face = "bold"),
              legend.title = element_blank(),
              plot.title = element_text(size = 0, face ="bold"),
              axis.title = element_text(size = 18, face = "bold"),
              axis.text.x = element_text(size = 16, face = "bold", color = "black"),
              axis.text.y = element_text(size = 16, face = "bold", color = "black"),
              plot.margin = unit(c(0.5,0.5,0.5,0.5), "cm"))        
ggsave(filename = "Plots_RNA-Seq/MDSPlot.tiff",
       plot = p3, width = 10, height = 8, dpi = 600, type = "cairo")

## t-SNE Plot
tsne <- Rtsne(dist.matrix, dims = 2, is_distance = T, perplexity=30, theta = 0.5,
              verbose=TRUE, pca = F, max_iter = 10000)
p4 <- ggplot(data = data.frame(tsne$Y), aes(tsne$Y[,1],tsne$Y[,2], color = colData$Tissue)) +
          scale_colour_manual(values = cols) + geom_point(size = 8) + 
          xlab("Coord 1") + ylab("Coord 2") + theme_classic() +
          theme(legend.text = element_text(size = 16, face = "bold"),
                legend.title = element_blank(),
                plot.title = element_text(size = 0, face ="bold"),
                axis.title = element_text(size = 18, face = "bold"),
                axis.text.x = element_text(size = 16, face = "bold", color = "black"),
                axis.text.y = element_text(size = 16, face = "bold", color = "black"),
                plot.margin = unit(c(0.5,0.5,0.5,0.5), "cm"))
ggsave(filename = "Plots_RNA-Seq/TSNE_Plot.tiff",
       plot = p4, width = 10, height = 8, dpi = 600, type = "cairo")

## correlation plot
annot2 <- data.frame(row.names = colnames(mat), Tissue = colData$Tissue)
names(cols) <- levels(colData$Tissue)
annot_cols <- list(Tissue = cols)
cols1 <- colorRampPalette(rev(brewer.pal(n = 6, name = "RdYlBu")))(5)
breaks <- c(0,0.2,0.4,0.6,0.8,1)
pheatmap(cor(log2(mat+1)), cluster_rows = T, cluster_cols = T, show_rownames = F, 
         height = 10, width = 16, show_colnames = T, legend = T, fontsize = 12,
         scale = "none", fontface="bold",clustering_distance_rows = "correlation",
         breaks = breaks, color = cols1, clustering_distance_cols = "correlation", 
         annotation = annot2, annotation_colors = annot_cols, border_color = "white")#, 
         #filename = "../results/01Plots-results/Plots_RNA-Seq/CorrelationPlot_CCLE.tiff")

## NMF Run
dim(mat)
genes <- gsub(pattern = "(.*)_(.*)", replacement = "\\1", x = rownames(mat))
write.table(x = data.frame(genes = genes), quote = F, sep = "\t", row.names = F,
            col.names = F, file = "Plots_RNA-Seq/Genelist.txt")

# did NMF and saved as a RObject
#save(list = ls(), file = "../results/01Plots-results/Plots_RNA-Seq/CCLE.Seq-subset.RData")
#res <- nmf(mat, rank = 2:15, nrun = 1000,.options="pv",seed=9999)

## NMF Results
load("dat/CCLE-Seq_5K-Regions_NMF-1000.RData")
res <- res4
pdf(file = "Plots_RNA-Seq/NMFSurvey-1000.pdf", width = 15, height = 10)
plot(res)
dev.off()

pdf(file = "Plots_RNA-Seq/Consensus_NMF-1000.pdf", width = 15, height = 10)
consensusmap(res, annCol = NA, tracks = c("basis:", "consensus:"), annRow = NA)
dev.off()

## Best Cluster
table(predict(res$fit$`5`, what = "consensus"))
cols.subtype <- RColorBrewer::brewer.pal(name = "Set1", n = 5)
cols2 <- list("consensus" = cols.subtype, "Tissue" = cols)
pdf(file = "Plots_RNA-Seq/ConsensusPlot_Cluster5.pdf", width = 15, height = 10)
consensusmap(res$fit$'5', tracks = c("consensus:"), annColors = cols2,
             annCol = list("Tissue" = colData$Tissue), annRow = NA, fontsize = 14)
dev.off()

## pheatmap
clusters <- data.frame(Sample = names(predict(res$fit$`5`, what = "consensus")),
                       Clust.Number = predict(res$fit$`5`, what = "consensus"), 
                       stringsAsFactors = F)
clusters <- clusters[order(clusters$Clust.Number),]
clusters <- inner_join(x = clusters, y = colData, by = c("Sample"="harmonize"))
clusters$Clust.Number <- paste("Cluster_",clusters$Clust.Number,sep = "")
mat1 <- data.frame(consensus(res$fit$`5`))
mat1 <- mat1[,match(clusters$Sample, colnames(mat1))]
annot1 <- data.frame(Clusters = clusters$Clust.Number, Tissue = clusters$Tissue,
                     row.names = clusters$Sample)
rna.cluster <- rev(RColorBrewer::brewer.pal(name = "Set3", n = 11)[c(3,4,6,7,8)])
names(rna.cluster) <- levels(factor(clusters$Clust.Number))
names(cols) <- levels(clusters$Tissue)
cols.subtype1 <- list(Clusters = rna.cluster, Tissue = cols)
pheatmap(mat1, cluster_rows = T, cluster_cols = T, show_rownames = F,
         show_colnames = T, legend = TRUE, 
         #filename = "Plots_RNA-Seq/ConsensusPlot_Cluster5_1000.tiff",
         fontsize_col = 16, scale = "none", fontface="bold", fontsize = 16,
         border_color = "grey50", annotation = annot1,
         annotation_colors = cols.subtype1, height = 18, width = 30)

## Clusters from RNA-Seq
# write.table(x = clusters[order(clusters$Clust.Number),],
#             file = "../results/01Plots-results/Plots_RNA-Seq/Cluster_RNA-seq_Counts.txt", 
#             quote = F, sep = "\t", row.names = F, col.names = T)

## Clusters from ChIP-Seq
# cluster.chip <- read.table(file = "../results/DiffBind_dba/CCLE_Clusters-4_NMF-1000.txt",
#                            header = T, sep = "\t", stringsAsFactors = F, quote = "F")
# cluster.chip.rna <- inner_join(x = cluster.chip, y = clusters, 
#                                by = c("Sample" = "ChIP_seq_ID"))
# colnames(cluster.chip.rna)[c(2,3,4)] <- c("ChIP-seq Cluster", "Alt.SampleID", 
#                                           "RNA-seq Cluster")
# cluster.chip.rna$`RNA-seq Cluster` <- paste("Cluster_",cluster.chip.rna$`RNA-seq Cluster`,
#                                             sep = "")
# 
# ## RNA-seq NMF with ChIP-Seq clusters
# annot1 <- data.frame(`RNAseq Clusters` = cluster.chip.rna$`RNA-seq Cluster`, 
#                      `ChIPseq Clusters` = cluster.chip.rna$`ChIP-seq Cluster`,
#                      Tissue = cluster.chip.rna$Tissue, row.names = 
#                                                 cluster.chip.rna$Alt.SampleID)
# mat2 <- mat1[rownames(mat1) %in% cluster.chip.rna$Alt.SampleID,
#              rownames(mat1) %in% cluster.chip.rna$Alt.SampleID]
# annot1 <- annot1[match(rownames(mat2), rownames(annot1)),]
# sum(rownames(annot1) == rownames(mat2))
# 
# ## pheatmap using all the clusters
# chip.cluster <- gg_color_hue(5)[c(1,2,4,5)]
# #rna.cluster <- rev(RColorBrewer::brewer.pal(name = "Set3", n = 11)[c(3,4,6,7,8)])
# names(chip.cluster) <- levels(factor(cluster.chip.rna$`ChIP-seq Cluster`))
# names(rna.cluster) <- levels(factor(cluster.chip.rna$`RNA-seq Cluster`))
# cols.subtype3 <- list(RNAseq.Clusters = rna.cluster, 
#                      ChIPseq.Clusters = chip.cluster, Tissue = cols)
# pheatmap(mat2, cluster_rows = T, cluster_cols = T, show_rownames = F,
#          show_colnames = T, legend = TRUE, 
#          #filename = "../results/CCLE_Seq/ConsensusPlot_RNA-ChIP_84Samples.tiff",
#          fontsize_col = 16, scale = "none", fontface="bold", fontsize = 16,
#          border_color = "grey50", annotation = annot1, height = 18, width = 30,
#          annotation_colors = cols.subtype3)
# #save(list = ls(), file = "../results/CCLE_Seq/CCLE-Seq_Workspace.RData")
# load("../results/CCLE_Seq/CCLE-Seq_Workspace.RData")

####################################
#
## Merging the dataset
#
####################################

genes <- gsub(pattern="(.*)_(.*)", replacement="\\1", rownames(dat.count))
idx <- which(duplicated(genes) ==  TRUE)
dat.count2 <- dat.count[-idx,]
rownames(dat.count2) <- gsub(pattern = "(.*)_(.*)", replacement = "\\1", 
                             x = rownames(dat.count2))
dim(dat.count2)

# ## head and neck RNA-seq dataset
# head.neck.dat <- read.csv(file = "../dat/CCLE/Head_Neck/rnaseq_hn_rawcount-Data.csv",
#                           header = T, sep = ",", quote = "\"", dec = ".", 
#                           fill = TRUE, stringsAsFactors = F, row.names = 2)
# head.neck.dat <- head.neck.dat[,-c(1,9,10)]
# dat.count2 <- inner_join(x = dat.count2 %>% rownames_to_column(var = "genes"),
#                          y = head.neck.dat %>% rownames_to_column(var = "genes"), 
#                          by = "genes")
# dim(dat.count2)

## sarcoma dataset
sarcoma.dat <- read.csv(file = "dat/Sarcoma_RNA-seq.txt", header = T, 
                        sep = "\t", quote = "", stringsAsFactors = F, 
                        na.strings = F)
dat.count3 <- inner_join(x = dat.count2 %>% rownames_to_column(var = "genes"), 
                         y = sarcoma.dat, by = "genes") 
dat.count3 <- dat.count3 %>% column_to_rownames(var = "genes")
#rm(sarcoma.dat, head.neck.dat, dat.count1, dat.count2)

## metadata for analysis
colData <- CCLE2ID
idx <- which(colData$harmonize == "")
colData$harmonize[idx] <- gsub(pattern = "-", replacement = "", x = colData$Cell.Line[idx])
sum(colnames(dat.count3) %in% colData$harmonize)
idx <- which(!colnames(dat.count3) %in% colData$harmonize)

## changing the colnames
colnames(dat.count3)[idx] <- c("1483","183","584A2","PECAPJ34",
                               "SqCC/Y1","UMSCC25","UMSCC4","ST88",
                               "ST88_2","ST88_3","ST26","ST26_2","ST26_3",
                               "M724", "M724_2","M724_3","S462","S462_2",
                               "S462_3","M642","M642_2","M642_3","M381","M381_2",
                               "M381_3") 
sum(colnames(dat.count3) %in% colData$harmonize)
colData <- colData[colData$harmonize %in% colnames(dat.count3),]
dat.count3 <- dat.count3[,colnames(dat.count3) %in% colData$harmonize]
dat.count3 <- dat.count3[,match(colData$harmonize,colnames(dat.count3))]
sum(colnames(dat.count3) == colData$harmonize)
colData$Tissue <- factor(colData$Tissue)

## DESeq normalization
dds <- DESeqDataSetFromMatrix(countData = as.matrix(dat.count3), colData = colData, 
                              design = ~ Tissue)
dds <- estimateSizeFactors(dds)
dat <- counts(dds, normalized=TRUE)
print(dim(dat))
print(dim(dds))

## sizefactor plot 
sizeFactorRatio <- sizeFactors(dds)
readCounts <- colSums(counts(dds))
cols.tissue <- RColorBrewer::brewer.pal(n = 9, name = "Set1")
cols.tissue[1] <- gg_color_hue(8)[8]
cols.tissue[2] <- "dodgerblue"
cols.tissue[6] <- "#E41A1C"
cols.tissue[8] <- "royalblue4"
p5 <- qplot(x = sizeFactors(dds), y = colSums(counts(dds))/1e6) + 
    geom_point(aes(colour = dds$Tissue), size = 5) + 
    scale_colour_manual(values = cols.tissue) +
    geom_smooth(method = "lm", se = TRUE, colour = "grey30") + 
    xlab("Size Factor") + ylab("Number of Aligned Reads (in million)") +
    theme(axis.title = element_text(size = 16, face = "bold"),
          axis.text.x = element_text(size = 16, face = "bold", color = "black"),
          axis.text.y = element_text(size = 16, face = "bold", color = "black"),
          plot.margin = unit(c(0.25,0.25,0.25,0.25), "cm"), 
          legend.title = element_blank(), legend.position = c(.85, .2),
          legend.text = element_text(size = 14, face = "bold", color = "black"))

## PCA plot
mat_new <- FSbyMAD(dat, cut.type="topk",value=5000)
pcs <- prcomp(t(log2(mat_new+1)), center = TRUE)
percentVar <- round(((pcs$sdev) ^ 2 / sum((pcs$sdev) ^ 2)* 100), 2) 
p6 <- ggplot(as.data.frame(pcs$x), aes(PC1,PC2, color = colData$Tissue)) + 
    xlab(makeLab(percentVar[1],1)) + ylab(makeLab(percentVar[2],2)) + 
    geom_point(size = 8) +  scale_colour_manual(values = cols.tissue) + theme_classic() +
    theme(legend.text = element_text(size = 16, face = "bold"),
          legend.title = element_text(size = 16, colour = "black", face = "bold"),
          plot.title = element_text(size = 0, face ="bold"),
          axis.title = element_text(size = 18, face = "bold"),
          axis.text.x = element_text(size = 16, face = "bold", color = "black"),
          axis.text.y = element_text(size = 16, face = "bold", color = "black"),
          plot.margin = unit(c(0.5,0.5,0.5,0.5), "cm"))

## MDS Plot
dist.matrix <- dist(t(log2(mat_new+1)))
loc <- cmdscale(dist.matrix)
p7 <- ggplot(data = data.frame(loc), aes(loc[,1],loc[,2], color = colData$Tissue)) + 
    scale_colour_manual(values = cols.tissue) + geom_point(size = 8) + 
    xlab("Coord 1") + ylab("Coord 2") + theme_classic() +
    theme(legend.text = element_text(size = 16, face = "bold"),
          legend.title = element_blank(),
          plot.title = element_text(size = 0, face ="bold"),
          axis.title = element_text(size = 18, face = "bold"),
          axis.text.x = element_text(size = 16, face = "bold", color = "black"),
          axis.text.y = element_text(size = 16, face = "bold", color = "black"),
          plot.margin = unit(c(0.5,0.5,0.5,0.5), "cm"))        

## t-SNE Plot
tsne <- Rtsne(dist.matrix, dims = 2, is_distance = T, perplexity=30, theta = 0.5,
              verbose=TRUE, pca = F, max_iter = 10000)
p8 <- ggplot(data = data.frame(tsne$Y), aes(tsne$Y[,1],tsne$Y[,2], color = colData$Tissue)) +
    scale_colour_manual(values = cols.tissue) + geom_point(size = 8) + 
    xlab("Coord 1") + ylab("Coord 2") + theme_classic() +
    theme(legend.text = element_text(size = 16, face = "bold"),
          legend.title = element_blank(),
          plot.title = element_text(size = 0, face ="bold"),
          axis.title = element_text(size = 18, face = "bold"),
          axis.text.x = element_text(size = 16, face = "bold", color = "black"),
          axis.text.y = element_text(size = 16, face = "bold", color = "black"),
          plot.margin = unit(c(0.5,0.5,0.5,0.5), "cm"))

## correlation plot
annot2 <- data.frame(row.names = colnames(mat_new), Tissue = colData$Tissue)
names(cols.tissue) <- levels(colData$Tissue)
annot_cols <- list(Tissue = cols.tissue)
cols1 <- colorRampPalette(rev(brewer.pal(n = 6, name = "RdYlBu")))(5)
breaks <- c(0,0.2,0.4,0.6,0.8,1)
pheatmap(cor(log2(mat_new+1)), cluster_rows = T, cluster_cols = T, show_rownames = F, 
         height = 10, width = 16, show_colnames = T, legend = T, fontsize = 12,
         scale = "none", fontface="bold",clustering_distance_rows = "correlation",
         breaks = breaks, color = cols1, clustering_distance_cols = "correlation", 
         annotation = annot2, annotation_colors = annot_cols, 
         border_color = "white")