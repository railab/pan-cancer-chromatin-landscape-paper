#####################################################
# @Author: Ayush T. Raman
# Rai Lab, Genome Center, MD Anderson
# Date: Feb 17th, 2018
#
# Program is used for:
# 1. Running for k=15 with 100 iterations 
#####################################################

rm(list =ls())

## Library
library(DiffBind)
library(NMF)

## NMF
load("dat/CCLE.counts-subset.RData")
res1 <- nmf(mat, 2:15, nrun=100, .options="pv", seed=123459)
save(res1, file = "dat/CCLE_10000-Regions_NMF-100.RData")