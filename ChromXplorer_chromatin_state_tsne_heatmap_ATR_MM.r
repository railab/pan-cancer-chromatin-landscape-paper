##ChromHMM HeatMaps Unsupervised Clustering##

## Clear workspace
rm(list =ls())

## function and working directory
setwd("/Volumes/Mahinur/CCLE_All_145CellLines_15milDown_pipeline_result/12chromHMM/MYOUTPUT")
source("/Volumes/Mahinur/CCLE_All_145CellLines_15milDown_pipeline_result/12chromHMM/plots.R")

## Library
library(circlize)
library(ComplexHeatmap)
library(dplyr)
library(gplots)
library(ggplot2)
library(genefilter)
library(pheatmap)
library(Rtsne)
library(RColorBrewer)
library(reshape2)
library(vegan)

## combined matrix state file from chromStatesMatrixforClustering_particularStates.pl
comp.grp <- function(i){
  dat <- read.table(paste("CombinedMatrix-",i,"-10000bps.txt",sep =""),header = FALSE,sep = "\t",quote = "",
                    row.names = 1, na.strings = FALSE, stringsAsFactors = FALSE)
  colnames(dat) <- c("BRCA-B001-15","BRCA-B014-15","BRCA-B015-15","BRCA-B019-15",
                     "BRCA-B020-15","BRCA-B021-15","BRCA-B028-15","BRCA-B034-15",
                     "BRCA-B035-15","BRCA-B038-15","BRCA-B047-15","BRCA-B052-15",
                     "COAD-C003-15","COAD-C012-15","COAD-C016-15","COAD-C022-15", 
                     "GBMC-G008-15","GBMC-G017-15","GBMC-G018-15","GBMC-G029-15",
                     "GBMC-G033-15","GBMC-G044-15","HNSC-183X-15","HNSC-HN4X-15",
                     "HNSC-HN30-15","HNSC-HN31-15","HNSC-M19L-15","HNSC-M68L-15",
                     "HNSC-PJ34-15","HNSC-U6XX-15","HNSC-U14B-15","HNSC-U17B-15",
                     "HNSC-U25X-15","LUNG-L009-15","LUNG-L011-15", "LUNG-L029-15",
                     "LUNG-L046-15","LUNG-L062-15","M-12-15","M-22-15","M-48-15",
                     "M-52-15" ,"M-57-15","M-61-15","M-84-15","M-96-15","M-940-15",
                     "M-A2058-15","M-CHL1-15","M-G361-15","M-MEWO-15",
                     "M-UACC62-15","M-WM88-15","M-WM793B-15","OVCA-V007-15",
                     "OVCA-V008-15","OVCA-V009-15","OVCA-V010-15","OVCA-V013-15",
                     "OVCA-V020-15","OVCA-V023-15","OVCA-V026-15","OVCA-V037-15",
                     "PAAD-P001-15","PAAD-P002-15","PAAD-P008-15","PAAD-P010-15",
                     "PAAD-P011-15","PAAD-P013-15","PAAD-P016-15","PAAD-P021-15",
                     "PAAD-P033-15","PAAD-P034-15","PAAD-P053-15","SCRC-M381-15",
                     "SCRC-M642-15","SCRC-M724-15","SCRC-S462-15","SCRC-ST26-15",
                     "SCRC-ST88-15")
  
  ## sample type labels for plot
  Cancer.type <- factor(c(rep("BRCA",12), rep("COAD",4),rep("GBMC",6), rep("HNSC",11), rep("LUNG",5), rep("SKCM",16), rep("OVCA",9), rep("PAAD",11), rep("SARC",6)))
  mycols <- c("red", "darkorange","#BCDB22", "darkorchid1", "dodgerblue", "hotpink1", "seagreen3","brown", "blue3")

  ## filtering low variable regions
  dat.state1 <- varFilter(as.matrix(dat), var.cutoff = 0.75)
  print(dim(dat.state1))
  
  ## tsne plot
  d <- dist(t(dat.state1), method = "binary")
  mds.plot <- cmdscale(d = d, eig = T, k = 50)
  mdsDist <- data.frame(x = mds.plot$points)
  set.seed(26)
  tsne <- Rtsne(X = mdsDist, dim =2, theta = 0,perplexity = 5, max_iter = 10000)
  mycols <- c("red", "darkorange","#BCDB22", "darkorchid1", "dodgerblue", "hotpink1", "seagreen3","brown", "blue3")
  cols <- mycols[1:9]
  print(ggplot(as.data.frame(tsne$Y), aes(tsne$Y[,1],tsne$Y[,2], color = Cancer.type)) +
    xlab("tSNE Dimention 1") + ylab("tSNE Dimention 2") + scale_colour_manual(values = cols) + 
      geom_point(size = 4) + 
    theme(panel.background = element_rect(fill = "white", colour = "grey50"),
          legend.text = element_text(size = 25, face = "plain"),
          legend.title = element_text(size = 25, colour = "black", face = "plain"),
          plot.title = element_text(size = 0, face ="plain"),
          axis.title = element_text(size = 25, face = "plain"),
          axis.text.x = element_text(size = 22, face = "plain", color = "black"),
          axis.text.y = element_text(size = 22, face = "plain", color = "black"),
          plot.margin = unit(c(0.5,0.5,0.5,0.5), "cm")))
  
  ## Annotation Data frame
  df1 <- data.frame(Tumor = Tumor.type) 
  Tumor.type.cols <- cols[1:9]
  Tumor.cols.assigned <- setNames(Tumor.type.cols, unique(levels(df1$Tumor)))
  annot1 <- HeatmapAnnotation(df = df1, col = list(Tumor = Tumor.cols.assigned)) 
  ## Heatmap -- 1; Plot the top variable regions - Unsupervised
  print(Heatmap(log2(dat.state1+1), name = "Frequency", show_row_names = FALSE, show_column_names = TRUE,
                row_dend_reorder = TRUE, column_dend_reorder = TRUE, clustering_distance_rows = "pearson",
                clustering_distance_columns = "euclidean", clustering_method_rows = "complete",
                clustering_method_columns = "complete", top_annotation = annot1))
  
  ### Supervised Clustering
  annot1 <- data.frame(Tumors = df1$Tumor, row.names = colnames(dat.state1))
  idx3 <- apply(dat.state1, 1, function(r) {pvalue = kruskal.test(r ~ annot1$Tumor)$p.value}) < 1e-3
  dat.state3 <- dat.state1[idx3,]
  dat.state3 <- dat.state3[!is.na(rownames(dat.state3)),]
  print(dim(dat.state3))
  pheatmap(mat = log2(dat.state3+1),cluster_rows = TRUE, cluster_cols = TRUE, clustering_distance_rows = "correlation",
           clustering_distance_cols = "euclidean", clustering_method = "complete", fontsize_number = 12, show_rownames = FALSE,
           annotation = annot1, annotation_colors = anno_colors)
  print(Heatmap(log2(dat.state3+1), name = "Frequency", show_row_names = FALSE, show_column_names = TRUE,
                row_dend_reorder = TRUE, column_dend_reorder = TRUE, clustering_distance_rows = "pearson",
                clustering_distance_columns = "euclidean", clustering_method_rows = "complete",
                clustering_method_columns = "complete", top_annotation = annot1))
  
  ## Bed files for GREAT analysis -- Change dat.state3 for "Supervised" and dat.state1 for "Unsupervised"
  file1 = paste("~/Desktop/GREAT",i,"_selected.txt",sep = "") ## change the directory if you want
  #file2 = paste("~/Desktop/Overall_frequency",i,"_selected.txt",sep = "") ## change the directory if you want
  bedfile = data.frame()
  for(j in 1:nrow(dat.state3)){
    bedfile[j, 1] = noquote(strsplit(rownames(dat.state3)[j], split = ":|-")[[1]])[1]
    bedfile[j, 2] = noquote(strsplit(rownames(dat.state3)[j], split = ":|-")[[1]])[2]
    bedfile[j, 3] = noquote(strsplit(rownames(dat.state3)[j], split = ":|-")[[1]])[3]
    bedfile[j, 4] = paste("ChromState",i,"-",j,sep = "")
    bedfile[j, 5] = sum(dat.state3[j,])
    bedfile[j, 6] = sum(dat.state3[j,c(1:8)] > 0)
    bedfile[j, 7] = sum(dat.state3[j,c(9:11)] > 0)
    bedfile[j, 8] = mean(dat.state3[j,c(1:8)])
    bedfile[j, 9] = mean(dat.state3[j,c(9:11)])
    bedfile[j, 10] = log2(mean(dat.state3[j,c(1:8)]+1)/mean(dat.state3[j,c(9:11)]+1))
  }
  #colnames(bedfile) = c("chrom", "chromStart", "chromEnd", "name", "Total Frequency", "# Samples (NR-T0)", "# Samples (R-T0)",
  #                      "Mean Occurence (NR-T0)", "Mean Occurence (R-T0)", "log2FC")
  write.table(x = bedfile[,c(1:5)], file = file1, sep="\t", quote=F, row.names = FALSE, col.names = FALSE)
  #write.table(x = bedfile, file = file2, sep="\t", quote=F, row.names = FALSE, col.names = FALSE)
  return(p1)
}

## calling the functions
bed1 <- comp.grp(i = 1)
bed2 <- comp.grp(i = 2)
bed3 <- comp.grp(i = 3)
bed4 <- comp.grp(i = 4)
bed5 <- comp.grp(i = 5)
bed6 <- comp.grp(i = 6)
bed7 <- comp.grp(i = 7)
bed8 <- comp.grp(i = 8)
bed9 <- comp.grp(i = 9)
bed10 <- comp.grp(i = 10)
bed11 <- comp.grp(i = 11)
bed12 <- comp.grp(i = 12)
bed14 <- comp.grp(i = 14)
bed15 <- comp.grp(i = 15)