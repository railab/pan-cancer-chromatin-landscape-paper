### first pass at looking at data from Kunal Rai
#this is using the 4 NMF clusters rather than the original 8

## things need to do - set up data to work for project nomenclature
#row meta data 
#column meta data - done
##set working directory
rm(list = ls())
setwd("/Users/ssrinivasan2/Rai")

###########single file test
#read in CCLE file
library(readxl)
ccle_names=read_excel("Achills_cell_name_and_ChIP_ID.xlsx", sheet=1)
#read in one cluster file
clusterfile=read.table("CCLE_Clusters-4.txt", header = T, stringsAsFactors = F)

###############column meta ###################

#use this file to get column_meta for dset0001
cluster1_read=read.table("Archive/CCLE_124Samples_Enhancer_H3K27Ac_Cluster1_reads_minus.txt", header=T, stringsAsFactors = F)
#creating new data frame with the column names without the "genomic.coord." column
cluster_colnames=as.data.frame(colnames(cluster1_read[-1]))
colnames(cluster_colnames)="Orig"
cluster_colnames=unique(cluster_colnames)

cluster_colnames$short=gsub(".", "-",cluster_colnames$Orig, fixed = T) #change colnames
cluster_colnames$short=sub("-$", "", cluster_colnames$short)

#match these to CCLE names in a new dataframe
library(dplyr)
column_meta=ccle_names
column_meta=right_join(x=clusterfile, y=cluster_colnames, by=c("Sample" = "short"))
column_meta=right_join(x=ccle_names, y=column_meta, by=c("ChIP_seq_ID" = "Sample"))

#column_meta$Sample=ifelse(is.na(column_meta$Achills_cell_name), NA, column_meta$ChIP_seq_ID)
#column_meta=column_meta[complete.cases(column_meta),]
#column_meta=column_meta[order(column_meta$Clust.Number),]
#column meta is done - write to file

write.table(column_meta, "/Users/ssrinivasan2/Rai/regnet_dset0003_columnmeta.txt", sep="\t",row.names = F, quote = F)


############row meta ###################

#take all the files, create new column and call it cluster 1 or cancer_cluster1

##############creating for loop to change all read file column names

filenames = list.files("/Users/ssrinivasan2/Rai/Archive", pattern="*reads_minus.txt", full.names=TRUE)

row_meta=data.frame()
avg_enh=data.frame()
##creating column_meta - 
for (file in filenames){
  temp=as.data.frame(read.table(file, header=T, stringsAsFactors = F)) #read in file as temp
  varname=as.character(sapply(strsplit(sapply(strsplit(file, "Samples_"), "[", 2), "_reads"), "[", 1))
  if (ncol(temp) == 111) {
    avg_temp1=as.data.frame(temp)
    avg_enh=rbind(avg_enh, avg_temp1)
  }
  rm(temp)
}

rownames(avg_enh)=avg_enh$genomic_coord.
avg_enh$genomic_coord.=NULL

avg_enh=as.data.frame(t(avg_enh[,column_meta$Orig]))
column_meta=column_meta[order(column_meta$Orig),]

avg_enh=avg_enh[order(rownames(avg_enh)),]

avg_enh_cluster=cbind(Cluster=column_meta$Clust.Number, avg_enh)

avg_enh_cluster=avg_enh_cluster[order(avg_enh_cluster$Cluster),]

#column_meta$Sample=ifelse(is.na(column_meta$Achills_cell_name), NA, column_meta$ChIP_seq_ID)
column_meta=column_meta[complete.cases(column_meta),]
column_meta=column_meta[order(column_meta$Clust.Number),]
#column meta is done - write to file


#write out colmeans files
write.table(avg_enh_cluster, "/Users/ssrinivasan2/Rai/regnet_dset0003_merged.txt", sep = "\t", row.names = F, quote = F)

############heatmap from ColMeans ###############
cluster_col= c(rep("indianred3", sum(column_meta$Clust.Number=="Cluster_1")), 
               rep("slateblue3", sum(column_meta$Clust.Number=="Cluster_2")), 
               rep("mediumspringgreen", sum(column_meta$Clust.Number=="Cluster_3")), 
               rep("yellow2", sum(column_meta$Clust.Number=="Cluster_4")))

library(gplots)
setEPS()
postscript("regnet_dset0003_heatmap.eps")

enh_heatmap=heatmap.2(as.matrix(t(avg_enh_cluster[,-1])),Colv = F,labRow = sapply(strsplit(rownames(avg_enh), "_"), "[", 3),
                      cexCol = 0.3, trace="none",density.info = "none",  col= colorRampPalette(c("dodgerblue4","white","firebrick3"))(999), 
                      ColSideColors = cluster_col, margins = c(6,6), cexRow = 0.7)
dev.off()


########## Integrating CERES data ###############


## what to do

library(readxl)
ccle_names=read_excel("Achills_cell_name_and_ChIP_ID.xlsx", sheet=1)
#read in one cluster file
clusterfile=read.table("CCLE_Clusters-4.txt", header = T, stringsAsFactors = F)
clusterfile=clusterfile[(clusterfile$Sample %in% ccle_names$ChIP_seq_ID),]
#There are 


#arrange your cell lines by cluster - no longer look at the enhancer cluster scores
#read in CERES - AVANA data

avana=read.table("/Users/ssrinivasan2/Rai/CERES_supptables/s3_avana_KOeffects.csv", row.names = 1, check.names=F,
                 sep=",",header = T, stringsAsFactors = F)

#limiting Avana to the Achilles Lines
length(intersect(colnames(avana), ccle_names$Achills_cell_name))
## both are showing the same cell lines, so can proceed with cutting avana down
avana=avana[,intersect(colnames(avana), ccle_names$Achills_cell_name)]

avana=as.matrix(t(avana))
enh_for_corr=avg_enh[column_meta$Orig[which(ccle_names$Achills_cell_name %in% rownames(avana))],]



## edit - add in FDR, take out values that are below -0.4, above 0.4
enh_corr_list=list()
top_enh_list=list()
for (i in 1:40) {
  sp_enh=as.data.frame(apply(avana, 2, function(x){ A=cor.test(x, enh_for_corr[,i], method="spearman", na.omit=T, exact=F); c(A$estimate,A$p.value)}))
  row.names(sp_enh)=c("rho","P")
  sp_enh=as.data.frame(t(sp_enh))
  sp_enh$adj_P=p.adjust(sp_enh$P, method = "fdr")
  sp_enh=as.data.frame(sp_enh[order(sp_enh$rho),])
  top=as.data.frame(sp_enh[which(sp_enh$rho>0.4 | sp_enh$rho < -0.4),])
  enh_corr_list[[i]]=(sp_enh)
  names(enh_corr_list)[[i]]=colnames(enh_for_corr)[i]
  top_enh_list[[i]]=top
  
}



####### regenerate heatmap with these 32 cell lines #########
setEPS()
postscript("regnet_dset0003_heatmap_avana.eps")

enh_heatmap=heatmap.2(as.matrix(t(enh_for_corr)),Rowv = T,labRow = sapply(strsplit(colnames(enh_for_corr), "_"), "[", 3), Colv = T,
                      trace="none",density.info = "none",  col= colorRampPalette(c("dodgerblue4","white","firebrick3"))(999), 
                      margins = c(6,6), cexRow = 0.7)
dev.off()


####### visualize each cluster ##########
hist(enh_corr_list$Enhancer_H3K27Ac_Cluster1$rho)

######### get TF list ##########
#read in Riken

rikentf=read.table("/Users/ssrinivasan2/Regulatory_Networks/FANTOMRiken/Browse Transcription Factors hg19 - resource_browser.csv", header=T, sep=",", stringsAsFactors = F)


########### extract TF's from each cluster #######
tf_enh_list=list()
top_tf=list()
for (i in 1:length(enh_corr_list)) {
  tf_enh=as.data.frame(enh_corr_list[[i]][intersect(rownames(enh_corr_list[[i]]), rikentf$Symbol),])
  tf_enh_list[[i]]=tf_enh
  names(tf_enh_list)[[i]]=names(enh_corr_list)[i]
  toptf=as.data.frame(tf_enh[which(tf_enh$rho>0.4 | tf_enh$rho < -0.4),])
  top_tf[[i]]=toptf
  
}

#repeat for the cancer enhancer cluters

tf_cancenh_list=list()
top_canctf=list()
for (i in 1:length(canc_enh_corr_list)) {
  tf_enh=as.data.frame(canc_enh_corr_list[[i]][intersect(rownames(canc_enh_corr_list[[i]]), rikentf$Symbol),])
  tf_cancenh_list[[i]]=tf_enh
  names(tf_cancenh_list)[[i]]=names(canc_enh_corr_list)[i]
  toptf=as.data.frame(tf_enh[which(tf_enh$rho>0.4 | tf_enh$rho < -0.4),])
  top_canctf[[i]]=toptf
  
}


###### make plots to see if correlations among transcription factors is better than all genes #######

library(ggplot2)
library(cowplot)
library(gridExtra)
ggplot() + 
  geom_histogram(aes(x=enh_corr_list$Enhancer_H3K27Ac_Cluster2$rho, y=(..count../sum(..count..))*100, alpha=0.4, fill="red"))+
  geom_histogram(aes(x=tf_enh_list$Enhancer_H3K27Ac_Cluster2$rho, y=(..count../sum(..count..))*100, alpha=0.4, fill="blue"))
p_list=list()
for (i in 1:8) {
  p = ggplot() + 
    geom_histogram(aes(x=enh_corr_list[[i]]$rho, fill="All Genes"))+
    geom_histogram(aes(x=tf_enh_list[[i]]$rh, fill="TF")) +
    xlab("Rho") + 
    ylab("Count") + 
    #labs(fill = "Genes") +
    theme(legend.position="none")+
    ggtitle(sapply(strsplit(names(enh_corr_list[i]), "_"), "[", 3))
  p_list[[i]]=p
}

do.call(grid.arrange, c(p_list, list(ncol=4)))

p_list=list()
for (i in 1:8) {
  p = ggplot() + 
    geom_density(aes(x=enh_corr_list[[i]]$rho))+
    xlab("Rho") + 
    ylab("Density") + 
    #labs(fill = "Genes") +
    theme(legend.position="none")+
    ggtitle(sapply(strsplit(names(enh_corr_list[i]), "_"), "[", 3))
  p_list[[i]]=p
}
do.call(grid.arrange, c(p_list, list(ncol=2)))


p_list=list()
for (i in 1:8) {
  p = ggplot() + 
    geom_density(aes(x=canc_enh_corr_list[[i]]$rho))+
    xlab("Rho") + 
    ylab("Density") + 
    #labs(fill = "Genes") +
    theme(legend.position="none")+
    ggtitle(sapply(strsplit(names(enh_corr_list[i]), "_"), "[", 3))
  p_list[[i]]=p
}
do.call(grid.arrange, c(p_list, list(ncol=2)))
### plot of all the rho values ### 
rho_list=list()
for (i in 1:8) {
  r = ggplot() + 
    geom_point(aes(x=seq_along(enh_corr_list[[i]]$rho), enh_corr_list[[i]]$rho))+
    xlab("Gene") + 
    ylab("Rho") + 
    #labs(fill = "Genes") +
    theme(legend.position="none")+
    ggtitle(sapply(strsplit(names(enh_corr_list[i]), "_"), "[", 3))
  rho_list[[i]]=r
}

do.call(grid.arrange, c(rho_list, list(ncol=4)))

### box plots of the average expression per cluster ###
avgenh.m=melt(t(avg_enh))
avgenh.m$avana=ifelse(avgenh.m$Var1 %in% rownames(enh_for_corr), "Yes", "No")

ggplot(avgenh.m, aes(x=Var2, y=value)) + geom_boxplot()+
  #geom_dotplot(binaxis='y', stackdir='center', dotsize = 0.25)+
  xlab("Cluster")+
  ylab("Average Expression")+
  scale_y_continuous(limits = c(-5, 5))+
  scale_x_discrete(labels=c(as.character(seq(1:8))))

cancaven.m=melt(t(avg_cancenh))
ggplot(cancaven.m, aes(x=Var2, y=value)) + geom_boxplot()+
  #geom_dotplot(binaxis='y', stackdir='center', dotsize = 0.25)
  xlab("Cluster")+
  ylab("Average Expression")+
  scale_y_continuous(limits = c(-5, 5))+
  scale_x_discrete(labels=c(as.character(seq(1:8))))

### box plots for only avana lines ###

avgeng_avana.m=melt(enh_for_corr)

ggplot(avgeng_avana.m, aes(x=Var2, y=value)) + geom_boxplot()+
  #geom_dotplot(binaxis='y', stackdir='center', dotsize = 0.25)+
  xlab("Cluster")+
  ylab("Average Expression")+
  scale_y_continuous(limits = c(-5, 5))+
  scale_x_discrete(labels=c(as.character(seq(1:8))))

##things to do
######enhancer
#get rownames of all genes that are passing the rho threshold across all clusters
genes_passingthresh=unlist(lapply(top_enh_list, rownames))
genes_passingthresh=unique(genes_passingthresh)
#with all these rownames, get one full matrix of all clusters and their Rho and p values
topgene_matrix=lapply(enh_corr_list,function(x) x[genes_passingthresh,])
topgene_write=do.call(cbind, lapply(topgene_matrix, `[`, 1))
colnames(topgene_write)=paste(sapply(strsplit(names(topgene_matrix), '_'), '[', 3), colnames(topgene_write), sep="_")

#write this to file (on excel - use p value to color the Rho)
write.table(topgene_write, "/Users/ssrinivasan2/Rai/regnet_dset0001_topgenes.txt", row.names = T, sep="\t", quote = F)

######cancer enhancer
genes_passingthresh_canc=unlist(lapply(top_cancenh_list, rownames))
genes_passingthresh_canc=unique(genes_passingthresh_canc)
#with all these rownames, get one full matrix of all clusters and their Rho and p values
topgene_matrix_canc=lapply(canc_enh_corr_list,function(x) x[genes_passingthresh_canc,])
topgene_write_canc=do.call(cbind, lapply(topgene_matrix_canc, `[`, 1))
colnames(topgene_write_canc)=paste(sapply(strsplit(names(topgene_matrix_canc), '_'), '[', 4), colnames(topgene_write_canc), sep="_")

#write this to file (on excel - use p value to color the Rho)
write.table(topgene_write_canc, "/Users/ssrinivasan2/Rai/regnet_dset0002_topgenes.txt", row.names = T, sep="\t", quote = F)


#make bar graph - number up rho, number down rho - per cluster
top_bar=data.frame()
for (i in 1:8) {
  up=sum(top_enh_list[[i]]$rho > 0)
  down=sum(top_enh_list[[i]]$rho < 0)
  top_bar[i,1]=up
  top_bar[i, 2]=down
  top_bar[i,3]=paste("Cluster", i, sep="_")
  colnames(top_bar)=c("Up", "Down", "Cluster")
}

top_bar=melt(top_bar)

ggplot(data=top_bar) + geom_bar(aes(x=as.factor(Cluster), y=value, fill=as.factor(variable)), stat="identity", position="dodge") +
  #geom_text(aes(label = value), position=position_dodge(width=0.9), vjust=-0.25)+
  xlab("Cluster")+
  ylab("# of Genes")+
  theme(legend.position="none")+
  scale_y_continuous(limits = c(0,600))+
  scale_x_discrete(labels=c(as.character(seq(1:8))))

top_bar=data.frame()
for (i in 1:8) {
  up=sum(top_cancenh_list[[i]]$rho > 0)
  down=sum(top_cancenh_list[[i]]$rho < 0)
  top_bar[i,1]=up
  top_bar[i, 2]=down
  top_bar[i,3]=paste("Cluster", i, sep="_")
  colnames(top_bar)=c("Up", "Down", "Cluster")
}

top_bar=melt(top_bar)

ggplot(data=top_bar) + geom_bar(aes(x=as.factor(Cluster), y=value, fill=as.factor(variable)), stat="identity", position="dodge") +
  #geom_text(aes(label = value), position=position_dodge(width=0.9), vjust=-0.25)+
  xlab("Cluster")+
  ylab("# of Genes")+
  theme(legend.position="none")+
  scale_y_continuous(limits = c(0,600))+
  scale_x_discrete(labels=c(as.character(seq(1:8))))

top_bar=data.frame()
for (i in 1:8) {
  up=sum(top_enh_list[[i]]$rho > 0)
  down=sum(top_enh_list[[i]]$rho < 0)
  top_bar[i,1]=up
  top_bar[i, 2]=down
  top_bar[i,3]=paste("Cluster", i, sep="_")
  colnames(top_bar)=c("Up", "Down", "Cluster")
}

top_bar=melt(top_bar)

top_bar=data.frame()
for (i in 1:8) {
  up=sum(top_tf[[i]]$rho > 0)
  down=sum(top_tf[[i]]$rho < 0)
  top_bar[i,1]=up
  top_bar[i, 2]=down
  top_bar[i,3]=paste("Cluster", i, sep="_")
  colnames(top_bar)=c("Up", "Down", "Cluster")
}

top_bar=melt(top_bar)
ggplot(data=top_bar) + geom_bar(aes(x=as.factor(Cluster), y=value, fill=as.factor(variable)), stat="identity", position="dodge") +
  #geom_text(aes(label = value), position=position_dodge(width=0.9), vjust=-0.25)+
  xlab("Cluster")+
  ylab("# of Genes")+
  theme(legend.position="none")+
  scale_y_continuous(limits = c(0,50))+
  scale_x_discrete(labels=c(as.character(seq(1:8))))

top_bar=data.frame()
for (i in 1:8) {
  up=sum(top_canctf[[i]]$rho > 0)
  down=sum(top_canctf[[i]]$rho < 0)
  top_bar[i,1]=up
  top_bar[i, 2]=down
  top_bar[i,3]=paste("Cluster", i, sep="_")
  colnames(top_bar)=c("Up", "Down", "Cluster")
}

top_bar=melt(top_bar)
ggplot(data=top_bar) + geom_bar(aes(x=as.factor(Cluster), y=value, fill=as.factor(variable)), stat="identity", position="dodge") +
  #geom_text(aes(label = value), position=position_dodge(width=0.9), vjust=-0.25)+
  xlab("Cluster")+
  ylab("# of Genes")+
  theme(legend.position="none")+
  scale_y_continuous(limits = c(0,50))+
  scale_x_discrete(labels=c(as.character(seq(1:8))))




#in ppt - make venn diagram of all lines, avana lines, achilles lines

## make seperate list - TF's that are passing the rho threshold per cluster

enhancer=data.frame()
for (i in 1:8) {
  enhancer[i,1]=(nrow(top_tf[[i]])/1533)
  enhancer[i,2]=(nrow(top_enh_list[[i]])/17670)
  enhancer[i,3]=paste("Cluster", i, sep="_")
  colnames(enhancer)=c("TF", "All", "Cluster")
}

canc_enhancer=data.frame()
for (i in 1:8) {
  canc_enhancer[i,1]=(nrow(top_canctf[[i]])/1533)
  canc_enhancer[i,2]=(nrow(top_cancenh_list[[i]])/17670)
  canc_enhancer[i,3]=paste("Cluster", i, sep="_")
  colnames(enhancer)=c("TF", "All", "Cluster")
}


tf_passingthresh=unlist(lapply(top_tf, rownames))
tf_passingthresh=unique(tf_passingthresh)
#with all these rownames, get one full matrix of all clusters and their Rho and p values
topgene_matrix=lapply(enh_corr_list,function(x) x[tf_passingthresh,])
topgene_write=do.call(cbind, lapply(topgene_matrix, `[`, 1))
colnames(topgene_write)=paste(sapply(strsplit(names(topgene_matrix), '_'), '[', 3), colnames(topgene_write), sep="_")

#write this to file (on excel - use p value to color the Rho)
write.table(topgene_write, "/Users/ssrinivasan2/Rai/regnet_dset0001_toptf.txt", row.names = T, sep="\t", quote = F)


ctf_passingthresh=unlist(lapply(top_canctf, rownames))
ctf_passingthresh=unique(ctf_passingthresh)
#with all these rownames, get one full matrix of all clusters and their Rho and p values
topgene_matrix=lapply(canc_enh_corr_list,function(x) x[ctf_passingthresh,])
topgene_write=do.call(cbind, lapply(topgene_matrix, `[`, 1))
colnames(topgene_write)=paste(sapply(strsplit(names(topgene_matrix), '_'), '[', 4), colnames(topgene_write), sep="_")

#write this to file (on excel - use p value to color the Rho)
write.table(topgene_write, "/Users/ssrinivasan2/Rai/regnet_dset0002_toptf.txt", row.names = T, sep="\t", quote = F)


for (i in 1:8) {
  tf_names=(rownames(top_tf[[i]]))
  write.table(tf_names, paste(paste("/Users/ssrinivasan2/Rai/regnet_dset0001_toptf", i, sep = "_"), ".txt", sep=""), row.names = F, sep="\t", quote = F)
}

for (i in 1:8) {
  tf_names=(rownames(top_canctf[[i]]))
  write.table(tf_names, paste(paste("/Users/ssrinivasan2/Rai/regnet_dset0002_toptf", i, sep = "_"), ".txt", sep=""), row.names = F, sep="\t", quote = F)
}

for (i in 1:8) {
  print(summary(top_canctf[[i]]))
}