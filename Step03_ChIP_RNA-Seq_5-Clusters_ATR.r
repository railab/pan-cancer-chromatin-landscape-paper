################################################################################
# @Author: Ayush T Raman
# Rai Lab, Genome Medicine, MD Anderson
# Date: April 30th, 2018
# Update: March 18th, 2019
#
# Program is used for:
# 1. Clusters based on CCLE ChIP-Seq and RNA-seq dataset
################################################################################

rm(list =ls())

## Library
library(CancerSubtypes)
library(DESeq2)
library(dplyr)
library(genefilter)
library(ggplot2)
library(GenomicRanges)
library(limma)
library(NMF)
library(org.Hs.eg.db)
library(pheatmap)
library(RColorBrewer)
library(Rtsne)
library(tibble)
library(TxDb.Hsapiens.UCSC.hg19.knownGene)

## functions
makeLab = function(x,pc) {
    paste0("PC",pc,": ",x,"% variance")
}

gg_color_hue <- function(n) {
    hues = seq(15, 375, length = n + 1)
    hcl(h = hues, l = 65, c = 100)[1:n]
}

##########################################
#
## ChIP-Seq Dataset with k = 5 clusters
#
##########################################

load("dat/CCLE.counts-subset.RData")
load("dat/CCLE_10000-Regions_NMF-1000.RData")

## NMF Plot for 5 clusters
res <- res1
cols.subtype <- RColorBrewer::brewer.pal(name = "Set1", n = 5)
cols <- list("consensus" = cols.subtype)

## Clusters
table(predict(res$fit$`5`, what = "consensus"))
clusters <- data.frame(Sample = names(predict(res$fit$`5`, what = "consensus")),
                       Clust.Number = predict(res$fit$`5`, what = "consensus"))
clusters <- clusters[order(clusters$Clust.Number),]
clusters$Clust.Number <- factor(paste("Cluster_",clusters$Clust.Number,sep = ""))
clusters$Sample <- gsub(pattern = "_C", replacement = "", x = clusters$Sample)
clusters$Sample <- gsub(pattern = "\\.", replacement = "-", x = clusters$Sample)
rownames(clusters) <- clusters$Samples

## Clusters from RNA-seq
cluster.rnaseq <- read.table(file = "dat/Cluster_RNA-seq_Counts.txt", 
                           header = T, sep = "\t", stringsAsFactors = F, 
                           quote = "", na.strings = F)
cluster.chip.rna <- left_join(x = clusters, y = cluster.rnaseq, ## cluster4
                               by = c("Sample" = "ChIP_seq_ID"))
colnames(cluster.chip.rna)[c(1,2,3,4)] <- c("ChIP_seq_ID", "ChIP-seq Cluster",
                                          "Alt.SampleID", "RNA-seq Cluster")
cluster.chip.rna$`RNA-seq Cluster` <- paste("Cluster_",
                                            cluster.chip.rna$`RNA-seq Cluster`,
                                            sep = "")
idx <- which(is.na(cluster.chip.rna$Tissue))
cluster.chip.rna[idx, "Tissue"] <- gsub(pattern = "(.*)-(.*)", replacement = "\\1",
                                        cluster.chip.rna$ChIP_seq_ID[idx])
for(i in 1:nrow(cluster.chip.rna)){
    if(cluster.chip.rna$Tissue[i] == "M"){
        cluster.chip.rna$Tissue[i] <- "SKCM"   
    }
    if(cluster.chip.rna$Tissue[i] == "MELANOMA"){
        cluster.chip.rna$Tissue[i] <- "SKCM"   
    }
    if(cluster.chip.rna$Tissue[i] == "GBM & Glioma"){
        cluster.chip.rna$Tissue[i] <- "GBMC"
    }
    if(cluster.chip.rna$Tissue[i] == "BREAST"){
        cluster.chip.rna$Tissue[i] <- "BRCA"
    }
    if(cluster.chip.rna$Tissue[i] == "COLON"){
        cluster.chip.rna$Tissue[i] <- "COAD"
    }
    if(cluster.chip.rna$Tissue[i] == "PANCREAS"){
        cluster.chip.rna$Tissue[i] <- "PAAD"
    }
    if(cluster.chip.rna$Tissue[i] == "OVARIAN"){
        cluster.chip.rna$Tissue[i] <- "OVCA"
    }
    if(cluster.chip.rna$Tissue[i] == "HEAD&NEAK"){
        cluster.chip.rna$Tissue[i] <- "HNSC"
    }
  

    # if(cluster.chip.rna$Tissue[i] == "PAAD"){
    #     cluster.chip.rna$Tissue[i] <- "PANCREAS"   
    # }
    # if(cluster.chip.rna$Tissue[i] == "HNSC"){
    #     cluster.chip.rna$Tissue[i] <- "HEAD&NEAK"   
    # }
    # if(cluster.chip.rna$Tissue[i] == "SCRC"){
    #     cluster.chip.rna$Tissue[i] <- "SARCOMA"   
    # }
    # if(cluster.chip.rna$Tissue[i] == "OVCA"){
    #     cluster.chip.rna$Tissue[i] <- "OVARIAN"   
    # }
}
dim(cluster.chip.rna)
samples <- c("BRCA-B003", "GBMC-G018", "LUNG-L062", "LUNG-L069", "OVCA-V007", 
             "OVCA-V013", "OVCA-V023", "OVCA-V026", "PAAD-P002")

## RNA-seq NMF with ChIP-Seq clusters
annot1 <- data.frame(`ChIPseq Clusters` = cluster.chip.rna$`ChIP-seq Cluster`,
                     `RNAseq Clusters` = cluster.chip.rna$`RNA-seq Cluster`,
                     Tissue = cluster.chip.rna$Tissue, 
                     row.names = cluster.chip.rna$ChIP_seq_ID)
mat1 <- data.frame(consensus(res$fit$`5`))
colnames(mat1) <- gsub(pattern = "(.*)\\.(.*)_C", replacement = "\\1-\\2", 
                       colnames(mat1))
rownames(mat1) <- gsub(pattern = "(.*)\\.(.*)_C", replacement = "\\1-\\2", 
                       rownames(mat1))
mat1 <- mat1[which(colnames(mat1) %in% cluster.chip.rna$ChIP_seq_ID),
             which(colnames(mat1) %in% cluster.chip.rna$ChIP_seq_ID)]
dim(mat1)
mat1 <- mat1[match(cluster.chip.rna$ChIP_seq_ID, colnames(mat1)),
             match(cluster.chip.rna$ChIP_seq_ID, colnames(mat1))]
sum(rownames(annot1) == rownames(mat1))

## pheatmap
chip.cluster <- RColorBrewer::brewer.pal(name = "Set2", n = 5)
names(chip.cluster) <- levels(factor(cluster.chip.rna$`ChIP-seq Cluster`))
cols.tissue <- c("red","darkorange","#BCDB22","darkorchid1","dodgerblue",
                 "hotpink1","seagreen3","brown","blue3")  
names(cols.tissue) <- levels(factor(cluster.chip.rna$Tissue))
rna.cluster <- RColorBrewer::brewer.pal(name = "Accent", n = 6)
rna.cluster[6] <- "#d8d8d8"
names(rna.cluster) <- levels(factor(cluster.chip.rna$`RNA-seq Cluster`))
cols.subtype1 <- list(RNAseq.Clusters = rna.cluster, Tissue = cols.tissue,
                      ChIPseq.Clusters = chip.cluster)
pheatmap(mat1, cluster_rows = T, cluster_cols = T, show_rownames = F,
         show_colnames = T, legend = TRUE, 
         filename = "Plots/ConsensusPlot_Cluster5_1000-new-1.tiff",
         fontsize_col = 16, scale = "none", fontface="bold", fontsize = 16,
         border_color = "grey50", annotation = annot1,
         annotation_colors = cols.subtype1, height = 18, width = 30)

pheatmap(mat1, cluster_rows = F, cluster_cols = F, show_rownames = F,
         show_colnames = T, legend = TRUE, 
         filename = "Plots/ConsensusPlot_Cluster5_1000-new.tiff",
         fontsize_col = 16, scale = "none", fontface="bold", fontsize = 16,
         border_color = "grey50", annotation = annot1,
         annotation_colors = cols.subtype1, height = 18, width = 30)

## Extracting features
mat2 <- mat
dim(mat2)
colnames(mat2) <- gsub(pattern = "_C", replacement = "", x = colnames(mat2))
colnames(mat2) <- gsub(pattern = "\\.", replacement = "-", x = colnames(mat2))
mat2 <- mat2[,clusters$Sample]
clusters$Sample == colnames(mat2)
clusters$subtype <- gsub(pattern = "(.*)-(.*)", replacement = "\\1", 
                         clusters$Sample)
clusters$subtype[clusters$subtype %in% "M"] <- "SKCM"

########################################
#
## limma results
#
########################################

source("function_limma4clustering.R")
background <- data.frame(as(rownames(dat.mat), "GRanges"))
write.table(x = background[,c(1:3)], file = "DERs/Background.bed", sep = "\t", 
            row.names = F, col.names = F, quote = F)
background1 <- data.frame(as(rownames(mat), "GRanges"))
write.table(x = background1[,c(1:3)], file = "DERs/Background_10k.bed", 
            sep = "\t", row.names = F, col.names = F, quote = F)

## clustering results
clusters$Clust.Number <- relevel(clusters$Clust.Number, ref = "Cluster_1")
cluster1 <- function_limma4clustering(mat2 = mat2, clusters = clusters, 
                                      file = "DERs/Cluster1.bed")
clusters$Clust.Number <- relevel(clusters$Clust.Number, ref = "Cluster_2")
cluster2 <- function_limma4clustering(mat2 = mat2, clusters = clusters, 
                                      file = "DERs/Cluster2.bed")
clusters$Clust.Number <- relevel(clusters$Clust.Number, ref = "Cluster_3")
cluster3 <- function_limma4clustering(mat2 = mat2, clusters = clusters, 
                                      file = "DERs/Cluster3.bed")
clusters$Clust.Number <- relevel(clusters$Clust.Number, ref = "Cluster_4")
cluster4 <- function_limma4clustering(mat2 = mat2, clusters = clusters, 
                                      file = "DERs/Cluster4.bed")
clusters$Clust.Number <- relevel(clusters$Clust.Number, ref = "Cluster_5")
cluster5 <- function_limma4clustering(mat2 = mat2, clusters = clusters, 
                                      file = "DERs/Cluster5.bed")
Reduce(intersect, list(cluster1,cluster2,cluster3,cluster4,cluster5))

## heatmaps
mat.clust1 <- mat2[as(rownames(mat),"GRanges") %in% cluster1,]
dim(mat.clust1)
pheatmap(mat.clust1, cluster_rows = T, cluster_cols = F, show_rownames = F,
         show_colnames = T, legend = TRUE, filename = "DERs/Cluster1_DERs.tiff",
         fontsize_col = 16, scale = "none", fontface="bold", fontsize = 16,
         border_color = "grey50", clustering_distance_rows = "correlation", 
         annotation = annot1, height = 18, width = 30, 
         annotation_colors = cols.subtype1)

mat.clust2 <- mat2[as(rownames(mat),"GRanges") %in% cluster2,]
dim(mat.clust2)
pheatmap(mat.clust2, cluster_rows = T, cluster_cols = F, show_rownames = F,
         show_colnames = T, legend = TRUE, filename = "DERs/Cluster2_DERs.tiff",
         fontsize_col = 16, scale = "none", fontface="bold", fontsize = 16,
         border_color = "grey50", clustering_distance_rows = "correlation", 
         annotation = annot1, height = 18, width = 30,
         annotation_colors = cols.subtype1)

mat.clust3 <- mat2[as(rownames(mat),"GRanges") %in% cluster3,]
dim(mat.clust3)
pheatmap(mat.clust3, cluster_rows = T, cluster_cols = F, show_rownames = F,
         show_colnames = T, legend = TRUE, filename = "DERs/Cluster3_DERs.tiff",
         fontsize_col = 16, scale = "none", fontface="bold", fontsize = 16,
         border_color = "grey50", clustering_distance_rows = "correlation", 
         annotation = annot1, height = 18, width = 30,
         annotation_colors = cols.subtype1)

mat.clust4 <- mat2[as(rownames(mat),"GRanges") %in% cluster4,]
dim(mat.clust4)
pheatmap(mat.clust4, cluster_rows = T, cluster_cols = F, show_rownames = F,
         show_colnames = T, legend = TRUE, filename = "DERs/Cluster4_DERs.tiff",
         fontsize_col = 16, scale = "none", fontface="bold", fontsize = 16,
         border_color = "grey50", clustering_distance_rows = "correlation", 
         annotation = annot1, height = 18, width = 30,
         annotation_colors = cols.subtype1)

mat.clust5 <- mat2[as(rownames(mat),"GRanges") %in% cluster5,]
dim(mat.clust5)
pheatmap(mat.clust5, cluster_rows = T, cluster_cols = F, show_rownames = F,
         show_colnames = T, legend = TRUE, filename = "DERs/Cluster5_DERs.tiff",
         fontsize_col = 16, scale = "none", fontface="bold", fontsize = 16,
         border_color = "grey50", clustering_distance_rows = "correlation", 
         annotation = annot1, height = 18, width = 30,
         annotation_colors = cols.subtype1)

mat.clust <- rbind(mat.clust1, mat.clust2, mat.clust3, mat.clust4, mat.clust5)
pheatmap(mat.clust, cluster_rows = F, cluster_cols = F, show_rownames = F,
         show_colnames = T, legend = TRUE,filename = "DERs/ClusterAll_DERs.tiff",
         fontsize_col = 16, scale = "none", fontface="bold", fontsize = 16,
         border_color = "grey50", #clustering_distance_rows = "correlation", 
         annotation = annot1, height = 18, width = 30,
         annotation_colors = cols.subtype1)

save(list = ls(), file = "DERs/ClusteringResults_limma--new.RData")

##########################################################################
#
## Homer Results and MEME results using command line or web interface
#
##########################################################################