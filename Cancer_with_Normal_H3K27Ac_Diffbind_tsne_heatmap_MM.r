### Heatmap for Single ChIP-seq Mark ##

rm(list = ls())

library(DiffBind)
library(ComplexHeatmap)
library(dplyr)
library(genefilter)
library(ggplot2)
library(ggrepel)
library(RColorBrewer)

load("/Volumes/Mahinur_2/CCLE_Pipeline_result/04aln_downsample_All/CCLE_229Samples_withNormal_H3K27Ac_READS_MINUS.RData")

## function
subset_most_variable_rows<- function(x, center, scale, top_n){
  log2.counts<- t(scale(t(log2(x+1)),center= center,scale= scale))
  rv<- rowVars(log2.counts)
  ## select the top n most variable regions for clustering
  idx<- order(-rv)[1:top_n]
  log2.counts[idx,]
}

## from dba count object
reads.RPKM <- dba.peakset(CCLE_229Samples_withNormal_H3K27Ac_READS_MINUS, bRetrieve = TRUE, DataType = DBA_DATA_FRAME)
name <- "RPKM"
rownames(reads.RPKM) <- paste(reads.RPKM$CHR,":",reads.RPKM$START,"-",reads.RPKM$END,sep = "")
reads.RPKM.mat <- reads.RPKM[, 4:ncol(reads.RPKM)] %>% as.matrix()

## Exclude promoter regions
## add the peak coordinates as the rownames 
## select one column out as vector http://stackoverflow.com/questions/21618423/extract-a-dplyr-tbl-column-as-a-vector
peakID<- reads.RPKM %>% mutate(peak=paste(CHR, START, END, sep=":")) %>% .[["peak"]]
rownames(reads.RPKM.mat)<- peakID


## split the diffbind H3K27ac results to promoters and enhancers
library(GenomicRanges)

H3K27ac.peaks.GR<- GRanges(seq = reads.RPKM$CHR, 
                           IRanges(start = reads.RPKM$START, end = reads.RPKM$END))

## get all the TSS 
library(TxDb.Hsapiens.UCSC.hg19.knownGene)
UCSC.hg19.genes<- genes(TxDb.Hsapiens.UCSC.hg19.knownGene)

## you can change downstream and upstream to 5000
UCSC.hg19.promoters<- promoters(UCSC.hg19.genes, upstream = 2500, downstream = 2500)

library(AnnotationDbi)
library("org.Hs.eg.db")

## note that dplyr and AnnotationDbi both have a function called select
## use dplyr::select when use dplyr

gene_symbol<- AnnotationDbi::select(org.Hs.eg.db, keys=UCSC.hg19.promoters$gene_id, 
                                    columns="SYMBOL", keytype="ENTREZID")

all.equal(UCSC.hg19.promoters$gene_id, gene_symbol$ENTREZID)

UCSC.hg19.promoters$gene_id<- gene_symbol$SYMBOL

## I just want to exclude H3K27ac peaks overlap with promoters, so I set select ="first"
## and type = "any"
H3K27ac.promoters.overlaps<- findOverlaps(H3K27ac.peaks.GR, UCSC.hg19.promoters, 
                                          type = "any", select = "first", ignore.strand= TRUE)
H3K27ac.peaks.GR

## index of GRanges which do not overlap with any known TSSs
enhancer.index<- which(is.na(H3K27ac.promoters.overlaps))
## index of prmoters
promoter.index<- which(!is.na(H3K27ac.promoters.overlaps))

## subset the RPKM.reads.mat by this index

reads.RPKM.mat.enhancers<- reads.RPKM.mat[enhancer.index,]

reads.RPKM.mat.promoters<- reads.RPKM.mat[promoter.index, ]


## Top regions
ind <- which(reads.RPKM.mat.enhancers <  0)
reads.RPKM.mat.enhancers[ind] <- 0
hmcols <- colorRampPalette(brewer.pal(9,"GnBu"))(100)
all.peaks <- subset_most_variable_rows(reads.RPKM.mat.enhancers, TRUE, FALSE, 5000) #1000

all.peaks1 <- data.frame(all.peaks) %>% tibble::rownames_to_column(var = "genomic_coord")
write.table(x = all.peaks1, "~/Desktop/CCLE_110Samples_H3K27Ac_Enhancer_top2000.txt", sep="\t",  quote=T, row.names = FALSE, col.names = TRUE)

## Features and annotation
feature.mat <- data.frame(CCLE_229Samples_withNormal_H3K27Ac_READS_MINUS$class)
Cancer.subtype <- factor(unlist(feature.mat["Tissue",]))
annot <- data.frame(Tissue = Cancer.subtype)

annot1 <- annot
annot1$Type <- factor(c(rep("Cancer", 136), rep("Normal", 93)), levels = c("Cancer", "Normal")) 
annot1$Tissues <- c(rep("PANCREAS", 12), rep("OVARY", 12), rep("SKIN", 20), rep("LUNG", 11), rep("BRAIN", 10), 
                   rep("GI_COLON", 12), rep("BREAST", 18), rep("HEAD&NECK", 27), rep("SARCOMA", 7), rep("MYELOMA", 2),
                   rep("LYMPHOMA",5), rep("ADRENAL", 1), rep("BLOOD",19), rep("BONE", 1), rep("BRAIN", 8),
                   rep("BREAST", 1), rep("ESC", 5), rep("ESC_DERIVED", 7), rep("FAT", 1), rep("GI_COLON", 3), 
                   rep("GI_DUODENUM", 1), rep("GI_ESOPHAGUS", 1),rep("GI_INTESTINE", 3),rep("GI_RECTUM", 3),
                   rep("GI_STOMACH", 3), rep("HEART", 3), rep("IPSC", 4), rep("LIVER", 1), rep("LUNG", 3),
                   rep("MUSCLE", 6), rep("OVARY", 1), rep("PANCREAS", 2), rep("PLACENTA", 2), rep("SKIN", 7),
                   rep("SPLEEN", 1), rep("STROMAL_CONNECTIVE", 2), rep("THYMUS", 2), rep("VASCULAR", 2)) 

library(Rtsne)
d <- dist(t(reads.RPKM.mat.enhancers), method = "binary")
mds.plot <- cmdscale(d = d, eig = T, k = 50)
mdsDist <- data.frame(x = mds.plot$points)
tsne <- Rtsne(X = mdsDist, dim =2, theta = 0, perplexity = 20, max_iter = 10000)
## Colors and its assignment
Type <- annot1$Type
mycols <- c("green4", "#F781BF", "#FF7F00", "skyblue4","#FFFF33","royalblue3",
            "#E41A1C", "#984EA3", "#A65628", "lavenderblush3", " lightcyan3",
            "#8DD3C7", "#FFFFB3", "#BEBADA", "#FB8072", "#80B1D3","#FDB462",
           "#B3DE69", "#FCCDE5", "#D9D9D9", "#BC80BD","#CCEBC5", "#FFED6F",
           "#FBB4AE", "#B3CDE3","#DECBE4", "#FED9A6", "#FFFFCC","#E5D8BD", "#FDDAEC","lemonchiffon2")
cols <- mycols[1:31]

ggplot(as.data.frame(tsne$Y), aes(tsne$Y[,1],tsne$Y[,2], color = Tissue, shape = Type)) +
  xlab("tSNE Dimention 1") + ylab("tSNE Dimention 2") + geom_point(size = 4) + 
  scale_colour_manual(values = cols)  + 
  theme(panel.background = element_rect(fill = "white", colour = "grey50"),
        legend.text = element_text(size = 25, face = "plain"),
        legend.title = element_text(size = 25, colour = "black", face = "plain"),
        plot.title = element_text(size = 0, face ="plain"),
        axis.title = element_text(size = 25, face = "plain"),
        axis.text.x = element_text(size = 22, face = "plain", color = "black"),
        axis.text.y = element_text(size = 22, face = "plain", color = "black"),
        plot.margin = unit(c(0.5,0.5,0.5,0.5), "cm"))

## Colors and its assignment
mycols <- c("green4", "#F781BF", "#FF7F00", "skyblue4","#FFFF33","royalblue3",
            "#E41A1C", "#984EA3", "#A65628", "lavenderblush3", " lightcyan3",
            "#8DD3C7", "#FFFFB3", "#BEBADA", "#FB8072", "#80B1D3","#FDB462",
            "#B3DE69", "#FCCDE5", "#D9D9D9", "#BC80BD","#CCEBC5", "#FFED6F",
            "#FBB4AE", "#B3CDE3","#DECBE4", "#FED9A6", "#FFFFCC","#E5D8BD", "#FDDAEC","lemonchiffon2")
Tissue.cols <- mycols[1:31]
Tissue.cols.assigned <- setNames(Tissue.cols, unique(levels(annot$Tissue)))

## Heatmap
heat.annot <- HeatmapAnnotation(df = annot, col = list(Tissue = Tissue.cols.assigned),
                                show_legend = TRUE, annotation_legend_param = list(title_gp = gpar(fontsize = 14,fontface = "bold"), 
                                                                                   labels_gp = gpar(fontsize = 14, fontface = "bold")))
h1 <- Heatmap(all.peaks, show_row_names = FALSE, show_column_names = TRUE, row_dend_reorder = TRUE, 
              column_dend_reorder = TRUE, clustering_distance_rows = "pearson",
              clustering_distance_columns = "euclidean", clustering_method_rows = "complete", 
              column_names_gp = gpar(fontsize = 16, fontface = "bold"), 
              clustering_method_columns = "complete", top_annotation = heat.annot,
              heatmap_legend_param = list(title = "READS_MINUS", title_gp = gpar(fontsize = 14, fontface = "bold"), 
                                          labels_gp = gpar(fontsize = 14, fontface = "bold")), split = 8)
h1
h1 <- draw(h1)
row_dend(h1)

for(i in 1:8){
  clust <- data.frame(regions = rownames(all.peaks[row_order(h1)[[i]],]))
  clust$genomic.loci <- gsub(pattern = ":|-", replacement = " ", x = clust$regions)
  clust$chr <- gsub(pattern = "(.*):(.*)-(.*)", replacement = "\\1", x = clust$regions)
  clust$start <- gsub(pattern = "(.*):(.*)-(.*)", replacement = "\\2", x = clust$regions)
  clust$end <- gsub(pattern = "(.*):(.*)-(.*)", replacement = "\\3", x = clust$regions)
  file.name <- paste("~/Desktop/CCLE_94Samples_H3K27me3_Cluster",i,".txt",sep = "")
  write.table(x = clust, file = file.name, quote = FALSE, sep = "\t", row.names = FALSE)
}