rm(list = ls())
library(rtracklayer)
library(dplyr)
library(readr)
library(TxDb.Hsapiens.UCSC.hg19.knownGene)
library(org.Hs.eg.db)
txdb <- TxDb.Hsapiens.UCSC.hg19.knownGene

##Cluster1
setwd("/Volumes/Mahinur_2/2019.2.27_Enhancer_analysis/CCLE_Cluster1_C_vs_CCLE_Cluster1_G-super")
Cluster1_super<- import("CCLE_Cluster1_Merged_H3K27Ac_peaks_Gateway_SuperEnhancers.bed", format = "BED")

# I used the overlapping for annotation of genes, we can try others as well
Cluster1_super_anno<- annotatePeak(Cluster1_super, tssRegion=c(-3000, 3000),
                                  TxDb=txdb, annoDb="org.Hs.eg.db", level = "gene", overlap = "all")

Cluster1_super_table<- read_tsv("CCLE_Cluster1_Merged_H3K27Ac_peaks_AllEnhancers.table.txt", col_names = T, comment = "#")

Cluster1_super_ann<- left_join(Cluster1_super_table, as.data.frame(Cluster1_super_anno), by = c("CHROM" = "seqnames", "STOP" = "end"))
write.table(Cluster1_super_ann,  "CCLE_Cluster#1_super_anno.tsv", quote =F, sep = "\t", row.names =F, col.names = T)
#superenhancer plot
pdf(here("CCLE_Cluster1_superEnhancer.pdf"), 6, 6)
library(ggrepel)

# Cluster1_color = #13d396   #yintercept = 4332.0684     #xintercept =21961
# Cluster2_color = #f7662e    #yintercept = 7815.1331     #xintercept =23598
# Cluster3_color = #4c7eef    #yintercept = 9670.456     #xintercept =21630
# Cluster4_color = #f952b9    #yintercept = 4401.3952     #xintercept =20543
# Cluster5_color = #94d824    #yintercept = 2843.3419     #xintercept =19383
             
# your y will be different, check your dataframe
### plot without annotation of any gene
ggplot(Cluster1_super_ann, aes(x = rev(enhancerRank), y = CCLE_Cluster1_C_merged.sorted.bam)) +
  geom_line(color = "grey") +
  geom_point(alpha = 1, color = "#13d396") +
  geom_hline(yintercept = 4332.0684 , linetype = 2, color = "black") + 
  geom_vline(xintercept =21961, linetype = 2, color = "black") +
  theme(panel.border = element_rect(colour="black",fill=NA, size=5, linetype = 1)) +
  theme_classic(base_size = 25) +
  ylab("H3K27ac signal at enhancer")
dev.off()  