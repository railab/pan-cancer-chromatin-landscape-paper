setwd("/Users/ssrinivasan2/Rai")
library(readxl)
library(tidyverse)
ccle_names=read_excel("Achills_cell_name_and_ChIP_ID.xlsx", sheet=1)
colnames(ccle_names)[2]="Sample"
#read in one cluster file
clusterfile=read.table("ChIP-SeqClusters2Sanjana.txt", header = T, stringsAsFactors = F)
#clusterfile=clusterfile[(clusterfile$ChIP_seq_ID %in% ccle_names$Sample),]

#There are 56 cell lines of the 124 ChIP Seq files that are assigned to clusters that have corresponding CCLE lines.

table(clusterfile$ChIP.seq)

avana=read.table("/Users/ssrinivasan2/Rai/portal-Avana-2018-06-08.csv", row.names = 1, check.names=F, sep=",",header = T, stringsAsFactors = F)

## Limit avana to cell lines that we have within the clusters, and also have ccle names

#clusterfile=inner_join(clusterfile, ccle_names, c("ChIP_seq_ID" = "Sample"))
length(intersect(colnames(avana), clusterfile$CCLE_ID))
avana=avana[,intersect(colnames(avana), clusterfile$CCLE_ID)]
cluster_avana=clusterfile[which(clusterfile$CCLE_ID %in% colnames(avana)),]

table(cluster_avana$ChIP.seq)

#After sorting through and limiting the avana dataset and the cluster file to shared cell lines, therea re only 6 cell lines belonging to cluster 1.

## perform wilcoxon rank sum on the 4 clusters to identify differentially essential genes per cluster
cluster_avana=cluster_avana[order(cluster_avana$ChIP.seq),]

table(cluster_avana$ChIP.seq)
write.table(cluster_avana,"rnaclusters_column_metadata.txt", sep="\t", quote=F, col.names = T, row.names = F)
avana=avana[,cluster_avana$CCLE_ID]

clusters=c(rep("cluster1", 6), rep("cluster2", 7), rep("cluster3", 7), rep("cluster4", 12), rep("cluster5", 16))

###wilcox rank sum test to identify differentially essential genes 
gene_p=data.frame()
for (i in 1:nrow(avana)) {
  test=wilcox.test(as.numeric(avana[i,1:6]), as.numeric(avana[i,7:ncol(avana)]) )
  test2=wilcox.test(as.numeric(avana[i,7:13]), as.numeric(avana[i,c(1:6, 14:ncol(avana))]))
  test3=wilcox.test(as.numeric(avana[i,14:20]), as.numeric(avana[i,c(1:13, 21:ncol(avana))]))
  test4=wilcox.test(as.numeric(avana[i,21:32]), as.numeric(avana[i,c(1:20, 33:ncol(avana))]))
  test5=wilcox.test(as.numeric(avana[i,33:48]), as.numeric(avana[i,c(1:33)]))
  gene_p[i,1]=rownames(avana)[i]
  gene_p[i,2]=test$p.value
  gene_p[i,3]=test2$p.value
  gene_p[i,4]=test3$p.value
  gene_p[i,5]=test4$p.value
  gene_p[i,6]=test5$p.value
}

colnames(gene_p)=c("Gene", "Cluster1", "Cluster2", "Cluster3", "Cluster4", "Cluster5")
#limiting to genes that are significantly different in only one cluster
cluster1=(which(gene_p$Cluster1 < 0.05 & gene_p$Cluster2 > 0.05 & gene_p$Cluster3 > 0.05 & gene_p$Cluster4 > 0.05 & gene_p$Cluster5 > 0.05))
cluster2=(which(gene_p$Cluster1 > 0.05 & gene_p$Cluster2 < 0.05 & gene_p$Cluster3 > 0.05 & gene_p$Cluster4 > 0.05 & gene_p$Cluster5 > 0.05))
cluster3=(which(gene_p$Cluster1 > 0.05 & gene_p$Cluster2 > 0.05 & gene_p$Cluster3 < 0.05 & gene_p$Cluster4 > 0.05 & gene_p$Cluster5 > 0.05))
cluster4=(which(gene_p$Cluster1 > 0.05 & gene_p$Cluster2 > 0.05 & gene_p$Cluster3 > 0.05 & gene_p$Cluster4 < 0.05 & gene_p$Cluster5 > 0.05))
cluster5=(which(gene_p$Cluster1 > 0.05 & gene_p$Cluster2 > 0.05 & gene_p$Cluster3 > 0.05 & gene_p$Cluster4 > 0.05 & gene_p$Cluster5 < 0.05))


cluster1=(which(gene_p$Cluster1 < 0.05))
cluster2=(which(gene_p$Cluster2 < 0.05))
cluster3=(which(gene_p$Cluster3 < 0.05))
cluster4=(which(gene_p$Cluster4 < 0.05))
cluster5=(which(gene_p$Cluster5 < 0.05))

sig_genes=gene_p[c(cluster1, cluster2, cluster3, cluster4, cluster5),]

##make dataframe with each cluster that the gene is significant in
sig_1=data.frame(cbind(gene=gene_p$Gene[cluster1], cluster=rep(1, length(cluster1))))
sig_2=data.frame(cbind(gene=gene_p$Gene[cluster2], cluster=rep(2, length(cluster2))))
sig_3=data.frame(cbind(gene=gene_p$Gene[cluster3], cluster=rep(3, length(cluster3))))
sig_4=data.frame(cbind(gene=gene_p$Gene[cluster4], cluster=rep(4, length(cluster4))))
sig_5=data.frame(cbind(gene=gene_p$Gene[cluster5], cluster=rep(5, length(cluster5))))

sig=rbind(sig_1, sig_2, sig_3, sig_4, sig_5)
#write.table(sig, "/Users/ssrinivasan2/Rai/rnaseqclusters_significantgenes_ceres.txt", sep="\t", row.names = F, quote = F)


##make heatmap of gene_p genes

avana_sig=(avana[sig_genes$Gene,])

#write.table("/Users/ssrinivasan2/Rai/rnaseqclusters_significantgenes_ceres.txt", sep="\t", row.names = T, quote = F)


cluster_col= c(rep("indianred3",length(which(clusters=="cluster1"))), 
               rep("slateblue3", length(which(clusters=="cluster2"))), 
               rep("mediumspringgreen", length(which(clusters=="cluster3"))),
               rep("yellow2", length(which(clusters=="cluster4"))),
               rep("darkmagenta", length(which(clusters=="cluster5"))))

cluster_row=c(rep("lightpink", length(cluster1)),
              rep("lightskyblue", length(cluster2)),
              rep("darkseagreen1", length(cluster3)),
              rep("khaki2", length(cluster4)),
              rep("salmon", length(cluster5)))

avana_sig=cbind(cluster_row, avana_sig)
library(gplots)
#setEPS()
#postscript("regnet_dset0004_heatmap.eps")

avana_heatmap=heatmap.2(as.matrix(avana_sig[,-1]),Colv = F, labCol = colnames(avana_sig),
                        cexCol = 0.3, trace="none",density.info = "none",  dendrogram = "none", col= colorRampPalette(c("dodgerblue4","white","firebrick3"))(999), 
                        ColSideColors = cluster_col, RowSideColors = as.character(avana_sig$cluster_row), 
                        margins = c(6,6), cexRow = 0.3)
#dev.off()

## need to look at directionality

##need TF

rikentf=read.table("/Users/ssrinivasan2/Regulatory_Networks/FANTOMRiken/Browse Transcription Factors hg19 - resource_browser.csv", header=T, sep=",", stringsAsFactors = F)

sig_tf=sig[which(sig$gene %in% rikentf$Symbol),]

#write.table(sig_tf, "/Users/ssrinivasan2/Rai/rnaseqclusters_significanttf_ceres.txt", sep="\t", row.names = T, quote = F)

avana_med=as.data.frame(apply(avana, 1, median))
avana_medadj=as.data.frame(apply(avana, 2, function(x) x-avana_med))

avg_cluster=data.frame()
for (i in 1:nrow(avana)) {
  avg_cluster[i,1]=as.numeric(apply(avana[i,1:6], 1, median))-as.numeric(apply(avana[i,7:ncol(avana)], 1, median))
  avg_cluster[i,2]=as.numeric(apply(avana[i,7:13], 1, median))-as.numeric(apply(avana[i,c(1:6, 14:ncol(avana))], 1, median))
  avg_cluster[i,3]=as.numeric(apply(avana[i,14:20], 1, median))-as.numeric(apply(avana[i,c(1:13, 21:ncol(avana))], 1, median))
  avg_cluster[i,4]=as.numeric(apply(avana[i,21:32], 1, median))-as.numeric(apply(avana[i,c(1:20, 33:ncol(avana))], 1, median))
  avg_cluster[i,5]=as.numeric(apply(avana[i,33:48], 1, median))-as.numeric(apply(avana[i,c(1:33)], 1, median)) 
}

rownames(avg_cluster)=rownames(avana)
colnames(avg_cluster)[1:5]=c("Cluster1", "Cluster2", "Cluster3", "Cluster4", "Cluster5")

library(ggplot2)
library(waterfalls)



avg_cluster=avg_cluster[order(avg_cluster$Cluster1),]
avg_cluster$clust1_sig=ifelse(rownames(avg_cluster) %in% sig_1$gene, 1, 0)
avg_cluster$clust2_sig=ifelse(rownames(avg_cluster) %in% sig_2$gene, 1, 0)
avg_cluster$clust3_sig=ifelse(rownames(avg_cluster) %in% sig_3$gene, 1, 0)
avg_cluster$clust4_sig=ifelse(rownames(avg_cluster) %in% sig_4$gene, 1, 0)
avg_cluster$clust5_sig=ifelse(rownames(avg_cluster) %in% sig_5$gene, 1, 0)

col = ifelse(avg_cluster$clust1_sig==1 , "#BC5A42", "#009296")

#setEPS()
#postscript("/Users/ssrinivasan2/Rai/ceres_rnaseq_differential_cluster1.eps")
barplot(avg_cluster$Cluster1, col=col, border=col, space=0.5, ylim=c(-0.75,0.75),
        main = "Waterfall plot for differential CERES score in Cluster1", ylab="Differential CERES Score",
        cex.axis=0.5, cex.lab=0.5)
#dev.off()


avg_cluster=avg_cluster[order(avg_cluster$Cluster2),]
#setEPS()
#postscript("/Users/ssrinivasan2/Rai/ceres_rnaseq_differential_cluster2.eps")
barplot(avg_cluster$Cluster2, col=col, border=col, space=0.5, ylim=c(-0.75,0.75),
        main = "Waterfall plot for differential CERES score in Cluster2", ylab="Differential CERES Score",
        cex.axis=0.5, cex.lab=0.5)
#dev.off()

#setEPS()
#postscript("/Users/ssrinivasan2/Rai/ceres_rnaseq_differential_cluster3.eps")
avg_cluster=avg_cluster[order(avg_cluster$Cluster3),]
barplot(avg_cluster$Cluster3, col=col, border=col, space=0.5, ylim=c(-0.75,0.75),
        main = "Waterfall plot for differential CERES score in Cluster3", ylab="Differential CERES Score",
        cex.axis=0.5, cex.lab=0.5)
#dev.off()

#setEPS()
#postscript("/Users/ssrinivasan2/Rai/ceres_rnaseq_differential_cluster4.eps")
avg_cluster=avg_cluster[order(avg_cluster$Cluster4),]
barplot(avg_cluster$Cluster4, col=col, border=col, space=0.5, ylim=c(-0.75,0.75),
        main = "Waterfall plot for differential CERES score in Cluster4", ylab="Differential CERES Score",
        cex.axis=0.5, cex.lab=0.5)
#dev.off()

#setEPS()
#postscript("/Users/ssrinivasan2/Rai/ceres_rnaseq_differential_cluster5.eps")
avg_cluster=avg_cluster[order(avg_cluster$Cluster5),]
barplot(avg_cluster$Cluster5, col=col, border=col, space=0.5, ylim=c(-0.75,0.75),
        main = "Waterfall plot for differential CERES score in Cluster4", ylab="Differential CERES Score",
        cex.axis=0.5, cex.lab=0.5)
#dev.off()

sig_1_proper=rownames(avg_cluster)[which(avg_cluster$clust1_sig ==1)]
sig_2_proper=rownames(avg_cluster)[which(avg_cluster$clust2_sig ==1)]
sig_3_proper=rownames(avg_cluster)[which(avg_cluster$clust3_sig ==1)]
sig_4_proper=rownames(avg_cluster)[which(avg_cluster$clust4_sig ==1)]
sig_5_proper=rownames(avg_cluster)[which(avg_cluster$clust5_sig ==1)]

all_sigs=as.data.frame(rbind(cbind(sig_1_proper, rep(1, length(sig_1_proper))), cbind(sig_2_proper, rep(2, length(sig_2_proper))), cbind(sig_3_proper, rep(3, length(sig_3_proper))), cbind(sig_4_proper, rep(4, length(sig_4_proper))), cbind(sig_5_proper, rep(5, length(sig_5_proper)))))  
colnames(all_sigs)=c("Gene", "Cluster")

#make a vector that has the difference from mean for each
temp=data.frame(c(avg_cluster[sig_1_proper,1], avg_cluster[sig_2_proper,2], avg_cluster[sig_3_proper,3], avg_cluster[sig_4_proper,4], avg_cluster[sig_5_proper,5]))
rownames(temp)=c(sig_1_proper, sig_2_proper, sig_3_proper, sig_4_proper, sig_5_proper)

rownames(all_sigs)=all_sigs$Gene

temp2=merge(all_sigs, temp, by=0)
rownames(temp2)=temp2$Row.names
temp2$Row.names=NULL

temp3=data.frame(c(gene_p$Cluster1[which(gene_p$Gene %in% sig_1_proper)], gene_p$Cluster2[which(gene_p$Gene %in% sig_2_proper)], gene_p$Cluster3[which(gene_p$Gene %in% sig_3_proper)], gene_p$Cluster4[which(gene_p$Gene %in% sig_4_proper)], gene_p$Cluster5[which(gene_p$Gene %in% sig_5_proper)]))
temp2=cbind(temp2, temp3)

all_sigs=temp2
colnames(all_sigs)=c("Gene", "Cluster_Significant", "Median_Diff", "p_value")
#write.table(all_sigs, "/Users/ssrinivasan2/Rai/rnaseqclusters_significantgenes_meta.txt", sep="\t", row.names = F, quote = F)

avana_sig_volcano=all_sigs
avana_sig=(avana[all_sigs$Gene,])
avana_sig=rbind(clusters, avana_sig)
avana_sig=(t(avana_sig))
colnames(avana_sig)[1]="Cluster"
#write.table(avana_sig, "/Users/ssrinivasan2/Rai/rnaseqclusters_significantgenes_ceres.txt", sep="\t", row.names = T, quote = F)


####
#Lower DEMETER scores indicates more dependency

#Repeating above analysis with shRNA data

clusterfile=read.table("ChIP-SeqClusters2Sanjana.txt", header = T, stringsAsFactors = F)
#clusterfile=clusterfile[(clusterfile$ChIP_seq_ID %in% ccle_names$Sample),]

#There are 59 cell lines of the 124 ChIP Seq files that are assigned to clusters that have corresponding CCLE lines.

table(clusterfile$ChIP.seq)

#rnai achilles
avana=read.table("/Users/ssrinivasan2/Rai/portal-RNAi_Ach-2018-06-08.csv", row.names = 1, check.names=F, sep=",",header = T, stringsAsFactors = F)
avana=na.omit(avana)

## Limit avana to cell lines that we have within the clusters, and also have ccle names

#clusterfile=inner_join(clusterfile, ccle_names, c("ChIP_seq_ID" = "Sample"))
length(intersect(colnames(avana), clusterfile$CCLE_ID))
avana=avana[,intersect(colnames(avana), clusterfile$CCLE_ID)]
cluster_avana=clusterfile[which(clusterfile$CCLE_ID %in% colnames(avana)),]

table(cluster_avana$ChIP.seq)

#
## perform wilcoxon rank sum on the 4 clusters to identify differentially essential genes per cluster
cluster_avana=cluster_avana[order(cluster_avana$ChIP.seq),]

table(cluster_avana$ChIP.seq)

avana=avana[,cluster_avana$CCLE_ID]

clusters=c(rep("cluster1", 8), rep("cluster2", 11), rep("cluster3", 7), rep("cluster4", 15), rep("cluster5", 18))

###wilcox rank sum test to identify differentially essential genes 
gene_p=data.frame()
for (i in 1:nrow(avana)) {
  test=wilcox.test(as.numeric(avana[i,1:8]), as.numeric(avana[i,9:ncol(avana)]) )
  test2=wilcox.test(as.numeric(avana[i,9:19]), as.numeric(avana[i,c(1:8, 20:ncol(avana))]))
  test3=wilcox.test(as.numeric(avana[i,20:26]), as.numeric(avana[i,c(1:19, 27:ncol(avana))]))
  test4=wilcox.test(as.numeric(avana[i,27:41]), as.numeric(avana[i,c(1:26, 42:ncol(avana))]))
  test5=wilcox.test(as.numeric(avana[i,42:59]), as.numeric(avana[i,c(1:41)]))
  gene_p[i,1]=rownames(avana)[i]
  gene_p[i,2]=test$p.value
  gene_p[i,3]=test2$p.value
  gene_p[i,4]=test3$p.value
  gene_p[i,5]=test4$p.value
  gene_p[i,6]=test5$p.value
}

colnames(gene_p)=c("Gene", "Cluster1", "Cluster2", "Cluster3", "Cluster4", "Cluster5")
#limiting to genes that are significantly different in only one cluster
cluster1=(which(gene_p$Cluster1 < 0.05 & gene_p$Cluster2 > 0.05 & gene_p$Cluster3 > 0.05 & gene_p$Cluster4 > 0.05 & gene_p$Cluster5 > 0.05))
cluster2=(which(gene_p$Cluster1 > 0.05 & gene_p$Cluster2 < 0.05 & gene_p$Cluster3 > 0.05 & gene_p$Cluster4 > 0.05 & gene_p$Cluster5 > 0.05))
cluster3=(which(gene_p$Cluster1 > 0.05 & gene_p$Cluster2 > 0.05 & gene_p$Cluster3 < 0.05 & gene_p$Cluster4 > 0.05 & gene_p$Cluster5 > 0.05))
cluster4=(which(gene_p$Cluster1 > 0.05 & gene_p$Cluster2 > 0.05 & gene_p$Cluster3 > 0.05 & gene_p$Cluster4 < 0.05 & gene_p$Cluster5 > 0.05))
cluster5=(which(gene_p$Cluster1 > 0.05 & gene_p$Cluster2 > 0.05 & gene_p$Cluster3 > 0.05 & gene_p$Cluster4 > 0.05 & gene_p$Cluster5 < 0.05))


cluster1=(which(gene_p$Cluster1 < 0.05))
cluster2=(which(gene_p$Cluster2 < 0.05))
cluster3=(which(gene_p$Cluster3 < 0.05))
cluster4=(which(gene_p$Cluster4 < 0.05))
cluster5=(which(gene_p$Cluster5 < 0.05))

sig_genes=gene_p[c(cluster1, cluster2, cluster3, cluster4, cluster5),]

##make dataframe with each cluster that the gene is significant in
sig_1=data.frame(cbind(gene=gene_p$Gene[cluster1], cluster=rep(1, length(cluster1))))
sig_2=data.frame(cbind(gene=gene_p$Gene[cluster2], cluster=rep(2, length(cluster2))))
sig_3=data.frame(cbind(gene=gene_p$Gene[cluster3], cluster=rep(3, length(cluster3))))
sig_4=data.frame(cbind(gene=gene_p$Gene[cluster4], cluster=rep(4, length(cluster4))))
sig_5=data.frame(cbind(gene=gene_p$Gene[cluster5], cluster=rep(5, length(cluster5))))

sig=rbind(sig_1, sig_2, sig_3, sig_4, sig_5)
#write.table(sig, "/Users/ssrinivasan2/Rai/rnaseqclusters_significantgenes_achilles.txt", sep="\t", row.names = F, quote = F)


##make heatmap of gene_p genes

avana_sig=(avana[sig_genes$Gene,])

#write.table("/Users/ssrinivasan2/Rai/rnaseqclusters_significantgenes_achilles.txt", sep="\t", row.names = T, quote = F)


cluster_col= c(rep("indianred3",length(which(clusters=="cluster1"))), 
               rep("slateblue3", length(which(clusters=="cluster2"))), 
               rep("mediumspringgreen", length(which(clusters=="cluster3"))),
               rep("yellow2", length(which(clusters=="cluster4"))),
               rep("darkmagenta", length(which(clusters=="cluster5"))))

cluster_row=c(rep("lightpink", length(cluster1)),
              rep("lightskyblue", length(cluster2)),
              rep("darkseagreen1", length(cluster3)),
              rep("khaki2", length(cluster4)),
              rep("salmon", length(cluster5)))

avana_sig=cbind(cluster_row, avana_sig)
library(gplots)
#setEPS()
#postscript("regnet_dset0004_heatmap.eps")

avana_heatmap=heatmap.2(as.matrix(avana_sig[,-1]),Colv = F, labCol = colnames(avana_sig),
                        cexCol = 0.3, trace="none",density.info = "none",  dendrogram = "none", col= colorRampPalette(c("dodgerblue4","white","firebrick3"))(999), 
                        ColSideColors = cluster_col, RowSideColors = as.character(avana_sig$cluster_row), 
                        margins = c(6,6), cexRow = 0.3)
#dev.off()

## need to look at directionality

##need TF

rikentf=read.table("/Users/ssrinivasan2/Regulatory_Networks/FANTOMRiken/Browse Transcription Factors hg19 - resource_browser.csv", header=T, sep=",", stringsAsFactors = F)


sig_tf=sig[which(sig$gene %in% rikentf$Symbol),]

#write.table(sig_tf, "/Users/ssrinivasan2/Rai/rnaseqclusters_significanttf_achilles.txt", sep="\t", row.names = T, quote = F)

avana_med=as.data.frame(apply(avana, 1, median))
avana_medadj=as.data.frame(apply(avana, 2, function(x) x-avana_med))


avg_cluster=data.frame()
for (i in 1:nrow(avana)) {
  avg_cluster[i,1]=as.numeric(apply(avana[i,1:8], 1, median))-as.numeric(apply(avana[i,9:ncol(avana)], 1, median))
  avg_cluster[i,2]=as.numeric(apply(avana[i,9:19], 1, median))-as.numeric(apply(avana[i,c(1:8, 20:ncol(avana))], 1, median))
  avg_cluster[i,3]=as.numeric(apply(avana[i,20:26], 1, median))-as.numeric(apply(avana[i,c(1:19, 27:ncol(avana))], 1, median))
  avg_cluster[i,4]=as.numeric(apply(avana[i,27:41], 1, median))-as.numeric(apply(avana[i,c(1:26, 42:ncol(avana))], 1, median))
  avg_cluster[i,5]=as.numeric(apply(avana[i,42:59], 1, median))-as.numeric(apply(avana[i,c(1:41)], 1, median)) 
}

rownames(avg_cluster)=rownames(avana)
colnames(avg_cluster)=c("Cluster1", "Cluster2", "Cluster3", "Cluster4", "Cluster5")

library(ggplot2)
library(waterfalls)



avg_cluster=avg_cluster[order(avg_cluster$Cluster1),]
avg_cluster$clust1_sig=ifelse(rownames(avg_cluster) %in% sig_1$gene, 1, 0)
avg_cluster$clust2_sig=ifelse(rownames(avg_cluster) %in% sig_2$gene, 1, 0)
avg_cluster$clust3_sig=ifelse(rownames(avg_cluster) %in% sig_3$gene, 1, 0)
avg_cluster$clust4_sig=ifelse(rownames(avg_cluster) %in% sig_4$gene, 1, 0)
avg_cluster$clust5_sig=ifelse(rownames(avg_cluster) %in% sig_5$gene, 1, 0)

col = ifelse(avg_cluster$clust1_sig==1 , "#BC5A42", "#009296")

#setEPS()
#postscript("/Users/ssrinivasan2/Rai/ceres_rnaseq_differential_cluster1.eps")
barplot(avg_cluster$Cluster1, col=col, border=col, space=0.5,
        main = "Waterfall plot for differential CERES score in Cluster1", ylab="Differential CERES Score",
        cex.axis=0.5, cex.lab=0.5)
#dev.off()


#avg_cluster=avg_cluster[order(avg_cluster$Cluster2),]
#setEPS()
#postscript("/Users/ssrinivasan2/Rai/ceres_rnaseq_differential_cluster2.eps")
barplot(avg_cluster$Cluster2, col=col, border=col, space=0.5, ylim=c(-0.75,0.75),
        main = "Waterfall plot for differential CERES score in Cluster2", ylab="Differential CERES Score",
        cex.axis=0.5, cex.lab=0.5)
#dev.off()

#setEPS()
#postscript("/Users/ssrinivasan2/Rai/ceres_rnaseq_differential_cluster3.eps")
avg_cluster=avg_cluster[order(avg_cluster$Cluster3),]
barplot(avg_cluster$Cluster3, col=col, border=col, space=0.5, ylim=c(-0.75,0.75),
        main = "Waterfall plot for differential CERES score in Cluster3", ylab="Differential CERES Score",
        cex.axis=0.5, cex.lab=0.5)
#dev.off()

#setEPS()
#postscript("/Users/ssrinivasan2/Rai/ceres_rnaseq_differential_cluster4.eps")
avg_cluster=avg_cluster[order(avg_cluster$Cluster4),]
barplot(avg_cluster$Cluster4, col=col, border=col, space=0.5, ylim=c(-0.75,0.75),
        main = "Waterfall plot for differential CERES score in Cluster4", ylab="Differential CERES Score",
        cex.axis=0.5, cex.lab=0.5)
#dev.off()

#setEPS()
#postscript("/Users/ssrinivasan2/Rai/ceres_rnaseq_differential_cluster5.eps")
avg_cluster=avg_cluster[order(avg_cluster$Cluster5),]
barplot(avg_cluster$Cluster5, col=col, border=col, space=0.5, ylim=c(-0.75,0.75),
        main = "Waterfall plot for differential CERES score in Cluster4", ylab="Differential CERES Score",
        cex.axis=0.5, cex.lab=0.5)
#dev.off()

sig_1_proper=rownames(avg_cluster)[which(avg_cluster$clust1_sig ==1)]
sig_2_proper=rownames(avg_cluster)[which(avg_cluster$clust2_sig ==1)]
sig_3_proper=rownames(avg_cluster)[which(avg_cluster$clust3_sig ==1)]
sig_4_proper=rownames(avg_cluster)[which(avg_cluster$clust4_sig ==1)]
sig_5_proper=rownames(avg_cluster)[which(avg_cluster$clust5_sig ==1)]

all_sigs=as.data.frame(rbind(cbind(sig_1_proper, rep(1, length(sig_1_proper))), cbind(sig_2_proper, rep(2, length(sig_2_proper))), cbind(sig_3_proper, rep(3, length(sig_3_proper))), cbind(sig_4_proper, rep(4, length(sig_4_proper))), cbind(sig_5_proper, rep(5, length(sig_5_proper)))))  
colnames(all_sigs)=c("Gene", "Cluster")

#make a vector that has the difference from mean for each
temp=data.frame(c(avg_cluster[sig_1_proper,1], avg_cluster[sig_2_proper,2], avg_cluster[sig_3_proper,3], avg_cluster[sig_4_proper,4], avg_cluster[sig_5_proper,5]))
rownames(temp)=c(sig_1_proper, sig_2_proper, sig_3_proper, sig_4_proper, sig_5_proper)

rownames(all_sigs)=all_sigs$Gene


temp2=merge(all_sigs, temp, by=0)
rownames(temp2)=temp2$Row.names
temp2$Row.names=NULL

temp3=data.frame(c(gene_p$Cluster1[which(gene_p$Gene %in% sig_1_proper)], gene_p$Cluster2[which(gene_p$Gene %in% sig_2_proper)], gene_p$Cluster3[which(gene_p$Gene %in% sig_3_proper)], gene_p$Cluster4[which(gene_p$Gene %in% sig_4_proper)], gene_p$Cluster5[which(gene_p$Gene %in% sig_5_proper)]))
temp2=cbind(temp2, temp3)

all_sigs=temp2
colnames(all_sigs)=c("Gene", "Cluster_Significant", "Median_Diff", "p_value")
#write.table(all_sigs, "/Users/ssrinivasan2/Rai/rnaseqclusters_significantgenes_achilles_meta.txt", sep="\t", row.names = F, quote = F)

achilles_sig_volcano = all_sigs
  
avana_sig=(avana[all_sigs$Gene,])
avana_sig=rbind(clusters, avana_sig)
avana_sig=(t(avana_sig))
colnames(avana_sig)[1]="Cluster"
#write.table(avana_sig, "/Users/ssrinivasan2/Rai/rnaseqclusters_significantgenes_achilles.txt", sep="\t", row.names = T, quote = F)

#rnai Novartis will not evaluate - upon omitting NA's - only 496 genes

length(intersect(achilles_sig_volcano$Gene, avana_sig_volcano$Gene))
library(cowplot)
plt_volcano= ggplot(data=avana_sig_volcano, aes(x=Median_Diff, y=-log10(p_value), colour=as.factor(Cluster_Significant))) +
  geom_point(alpha=1, size=1) +
  theme(legend.position = "none") +
  xlim(c(min(avana_sig_volcano$Median_Diff), max(avana_sig_volcano$Median_Diff))) + ylim(c(0, 15)) +
  xlab("Median Difference") + ylab("-log10 p-value")

plt_volcano= ggplot(data=achilles_sig_volcano, aes(x=Median_Diff, y=-log10(p_value), colour=as.factor(Cluster_Significant))) +
  geom_point(alpha=1, size=1) +
  theme(legend.position = "none") +
  xlim(c(min(achilles_sig_volcano$Median_Diff), max(achilles_sig_volcano$Median_Diff))) + ylim(c(0, 15)) +
  xlab("Median Difference") + ylab("-log10 p-value")

lines_int=intersect(achilles_sig_volcano$Gene, avana_sig_volcano$Gene)
library(tidyverse)
new_data=merge(achilles_sig_volcano, avana_sig_volcano, by="Gene")
new_data[,1]=lines_int

new_data[,2]=achilles_sig_volcano[which(achilles_sig_volcano$Gene), 3]
