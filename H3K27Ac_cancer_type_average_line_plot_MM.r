rm(list = ls())
###### CCLE data H3K27Ac Enhancer
setwd("/rsrch2/genomic_med/mmaitituoheti/CCLE/snakemake_ChIPseq_pipeline/")
library(rtracklayer)
library(ComplexHeatmap)
H3K27Ac_bed<- import("CCLE_125_samples_merged_H3K27Ac_exclude_+-2.5kbTSS.bed", format = "BED")
H3K27Ac.5kb<- resize(H3K27Ac_bed, width = 10000, fix = "center")

## only read in a subset of the bigwig files using which argument!
#BRCA
B001.H3K27Ac<- import("07bigwig/BRCA-B001_C.bw", format = "bigWig", which = H3K27Ac.5kb)
B003.H3K27Ac<- import("07bigwig/BRCA-B003_C.bw", format = "bigWig", which = H3K27Ac.5kb)
B008.H3K27Ac<- import("07bigwig/BRCA-B008_C.bw", format = "bigWig", which = H3K27Ac.5kb)
B010.H3K27Ac<- import("07bigwig/BRCA-B010_C.bw", format = "bigWig", which = H3K27Ac.5kb)
B013.H3K27Ac<- import("07bigwig/BRCA-B013_C.bw", format = "bigWig", which = H3K27Ac.5kb)
B014.H3K27Ac<- import("07bigwig/BRCA-B014_C.bw", format = "bigWig", which = H3K27Ac.5kb)
B015.H3K27Ac<- import("07bigwig/BRCA-B015_C.bw", format = "bigWig", which = H3K27Ac.5kb)
B019.H3K27Ac<- import("07bigwig/BRCA-B019_C.bw", format = "bigWig", which = H3K27Ac.5kb)
B020.H3K27Ac<- import("07bigwig/BRCA-B020_C.bw", format = "bigWig", which = H3K27Ac.5kb)
B021.H3K27Ac<- import("07bigwig/BRCA-B021_C.bw", format = "bigWig", which = H3K27Ac.5kb)
B028.H3K27Ac<- import("07bigwig/BRCA-B028_C.bw", format = "bigWig", which = H3K27Ac.5kb)
B034.H3K27Ac<- import("07bigwig/BRCA-B034_C.bw", format = "bigWig", which = H3K27Ac.5kb)
B035.H3K27Ac<- import("07bigwig/BRCA-B035_C.bw", format = "bigWig", which = H3K27Ac.5kb)
B038.H3K27Ac<- import("07bigwig/BRCA-B038_C.bw", format = "bigWig", which = H3K27Ac.5kb)
B047.H3K27Ac<- import("07bigwig/BRCA-B047_C.bw", format = "bigWig", which = H3K27Ac.5kb)
B050.H3K27Ac<- import("07bigwig/BRCA-B050_C.bw", format = "bigWig", which = H3K27Ac.5kb)
B052.H3K27Ac<- import("07bigwig/BRCA-B052_C.bw", format = "bigWig", which = H3K27Ac.5kb)
#COAD
C002.H3K27Ac<- import("07bigwig/COAD-C002_C.bw", format = "bigWig", which = H3K27Ac.5kb)
C003.H3K27Ac<- import("07bigwig/COAD-C003_C.bw", format = "bigWig", which = H3K27Ac.5kb)
C004.H3K27Ac<- import("07bigwig/COAD-C004_C.bw", format = "bigWig", which = H3K27Ac.5kb)
C007.H3K27Ac<- import("07bigwig/COAD-C007_C.bw", format = "bigWig", which = H3K27Ac.5kb)
C012.H3K27Ac<- import("07bigwig/COAD-C012_C.bw", format = "bigWig", which = H3K27Ac.5kb)
C013.H3K27Ac<- import("07bigwig/COAD-C013_C.bw", format = "bigWig", which = H3K27Ac.5kb)
C015.H3K27Ac<- import("07bigwig/COAD-C015_C.bw", format = "bigWig", which = H3K27Ac.5kb)
C016.H3K27Ac<- import("07bigwig/COAD-C016_C.bw", format = "bigWig", which = H3K27Ac.5kb)
C020.H3K27Ac<- import("07bigwig/COAD-C020_C.bw", format = "bigWig", which = H3K27Ac.5kb)
C021.H3K27Ac<- import("07bigwig/COAD-C021_C.bw", format = "bigWig", which = H3K27Ac.5kb)
C022.H3K27Ac<- import("07bigwig/COAD-C022_C.bw", format = "bigWig", which = H3K27Ac.5kb)
C027.H3K27Ac<- import("07bigwig/COAD-C027_C.bw", format = "bigWig", which = H3K27Ac.5kb)
#GBMC
G001.H3K27Ac<- import("07bigwig/GBMC-G001_C.bw", format = "bigWig", which = H3K27Ac.5kb)
G002.H3K27Ac<- import("07bigwig/GBMC-G002_C.bw", format = "bigWig", which = H3K27Ac.5kb)
G008.H3K27Ac<- import("07bigwig/GBMC-G008_C.bw", format = "bigWig", which = H3K27Ac.5kb)
G010.H3K27Ac<- import("07bigwig/GBMC-G010_C.bw", format = "bigWig", which = H3K27Ac.5kb)
G017.H3K27Ac<- import("07bigwig/GBMC-G017_C.bw", format = "bigWig", which = H3K27Ac.5kb)
G018.H3K27Ac<- import("07bigwig/GBMC-G018_C.bw", format = "bigWig", which = H3K27Ac.5kb)
G028.H3K27Ac<- import("07bigwig/GBMC-G028_C.bw", format = "bigWig", which = H3K27Ac.5kb)
G029.H3K27Ac<- import("07bigwig/GBMC-G029_C.bw", format = "bigWig", which = H3K27Ac.5kb)
G033.H3K27Ac<- import("07bigwig/GBMC-G033_C.bw", format = "bigWig", which = H3K27Ac.5kb)
G044.H3K27Ac<- import("07bigwig/GBMC-G044_C.bw", format = "bigWig", which = H3K27Ac.5kb)
#HNSC
H183X.H3K27Ac<- import("07bigwig/HNSC-183X_C.bw", format = "bigWig", which = H3K27Ac.5kb)
H584A.H3K27Ac<- import("07bigwig/HNSC-584A_C.bw", format = "bigWig", which = H3K27Ac.5kb)
H1483.H3K27Ac<- import("07bigwig/HNSC-1483_C.bw", format = "bigWig", which = H3K27Ac.5kb)
H1686.H3K27Ac<- import("07bigwig/HNSC-1686_C.bw", format = "bigWig", which = H3K27Ac.5kb)
HDETR.H3K27Ac<- import("07bigwig/HNSC-DETR_C.bw", format = "bigWig", which = H3K27Ac.5kb)
HHN4X.H3K27Ac<- import("07bigwig/HNSC-HN4X_C.bw", format = "bigWig", which = H3K27Ac.5kb)
HHN5X.H3K27Ac<- import("07bigwig/HNSC-HN5X_C.bw", format = "bigWig", which = H3K27Ac.5kb)
HHN30.H3K27Ac<- import("07bigwig/HNSC-HN30_C.bw", format = "bigWig", which = H3K27Ac.5kb)
HHN31.H3K27Ac<- import("07bigwig/HNSC-HN31_C.bw", format = "bigWig", which = H3K27Ac.5kb)
HJH22.H3K27Ac<- import("07bigwig/HNSC-JH22_C.bw", format = "bigWig", which = H3K27Ac.5kb)
HJHU2.H3K27Ac<- import("07bigwig/HNSC-JHU2_C.bw", format = "bigWig", which = H3K27Ac.5kb)
HM13L.H3K27Ac<- import("07bigwig/HNSC-M13L_C.bw", format = "bigWig", which = H3K27Ac.5kb)
HM19L.H3K27Ac<- import("07bigwig/HNSC-M19L_C.bw", format = "bigWig", which = H3K27Ac.5kb)
HM68L.H3K27Ac<- import("07bigwig/HNSC-M68L_C.bw", format = "bigWig", which = H3K27Ac.5kb)
HMD1T.H3K27Ac<- import("07bigwig/HNSC-MD1T_C.bw", format = "bigWig", which = H3K27Ac.5kb)
HMD8L.H3K27Ac<- import("07bigwig/HNSC-MD8L_C.bw", format = "bigWig", which = H3K27Ac.5kb)
HOS19.H3K27Ac<- import("07bigwig/HNSC-OS19_C.bw", format = "bigWig", which = H3K27Ac.5kb)
HPC5A.H3K27Ac<- import("07bigwig/HNSC-PC5A_C.bw", format = "bigWig", which = H3K27Ac.5kb)
HPC5B.H3K27Ac<- import("07bigwig/HNSC-PC5B_C.bw", format = "bigWig", which = H3K27Ac.5kb)
HPJ34.H3K27Ac<- import("07bigwig/HNSC-PJ34_C.bw", format = "bigWig", which = H3K27Ac.5kb)
HSC61.H3K27Ac<- import("07bigwig/HNSC-SC61_C.bw", format = "bigWig", which = H3K27Ac.5kb)
HU6XX.H3K27Ac<- import("07bigwig/HNSC-U6XX_C.bw", format = "bigWig", which = H3K27Ac.5kb)
HU10B.H3K27Ac<- import("07bigwig/HNSC-U10B_C.bw", format = "bigWig", which = H3K27Ac.5kb)
HU14B.H3K27Ac<- import("07bigwig/HNSC-U14B_C.bw", format = "bigWig", which = H3K27Ac.5kb)
HU17A.H3K27Ac<- import("07bigwig/HNSC-U17A_C.bw", format = "bigWig", which = H3K27Ac.5kb)
HU17B.H3K27Ac<- import("07bigwig/HNSC-U17B_C.bw", format = "bigWig", which = H3K27Ac.5kb)
HU25X.H3K27Ac<- import("07bigwig/HNSC-U25X_C.bw", format = "bigWig", which = H3K27Ac.5kb)
#LUNG
L009.H3K27Ac<- import("07bigwig/LUNG-L009_C.bw", format = "bigWig", which = H3K27Ac.5kb)
L011.H3K27Ac<- import("07bigwig/LUNG-L011_C.bw", format = "bigWig", which = H3K27Ac.5kb)
L020.H3K27Ac<- import("07bigwig/LUNG-L020_C.bw", format = "bigWig", which = H3K27Ac.5kb)
L027.H3K27Ac<- import("07bigwig/LUNG-L027_C.bw", format = "bigWig", which = H3K27Ac.5kb)
L029.H3K27Ac<- import("07bigwig/LUNG-L029_C.bw", format = "bigWig", which = H3K27Ac.5kb)
L036.H3K27Ac<- import("07bigwig/LUNG-L036_C.bw", format = "bigWig", which = H3K27Ac.5kb)
L044.H3K27Ac<- import("07bigwig/LUNG-L044_C.bw", format = "bigWig", which = H3K27Ac.5kb)
L046.H3K27Ac<- import("07bigwig/LUNG-L046_C.bw", format = "bigWig", which = H3K27Ac.5kb)
L062.H3K27Ac<- import("07bigwig/LUNG-L062_C.bw", format = "bigWig", which = H3K27Ac.5kb)
L069.H3K27Ac<- import("07bigwig/LUNG-L069_C.bw", format = "bigWig", which = H3K27Ac.5kb)
# OVCA
V007.H3K27Ac<- import("07bigwig/OVCA-V007_C.bw", format = "bigWig", which = H3K27Ac.5kb)
V008.H3K27Ac<- import("07bigwig/OVCA-V008_C.bw", format = "bigWig", which = H3K27Ac.5kb)
V009.H3K27Ac<- import("07bigwig/OVCA-V009_C.bw", format = "bigWig", which = H3K27Ac.5kb)
V010.H3K27Ac<- import("07bigwig/OVCA-V010_C.bw", format = "bigWig", which = H3K27Ac.5kb)
V013.H3K27Ac<- import("07bigwig/OVCA-V013_C.bw", format = "bigWig", which = H3K27Ac.5kb)
V017.H3K27Ac<- import("07bigwig/OVCA-V017_C.bw", format = "bigWig", which = H3K27Ac.5kb)
V020.H3K27Ac<- import("07bigwig/OVCA-V020_C.bw", format = "bigWig", which = H3K27Ac.5kb)
V023.H3K27Ac<- import("07bigwig/OVCA-V023_C.bw", format = "bigWig", which = H3K27Ac.5kb)
V026.H3K27Ac<- import("07bigwig/OVCA-V026_C.bw", format = "bigWig", which = H3K27Ac.5kb)
V027.H3K27Ac<- import("07bigwig/OVCA-V027_C.bw", format = "bigWig", which = H3K27Ac.5kb)
V037.H3K27Ac<- import("07bigwig/OVCA-V037_C.bw", format = "bigWig", which = H3K27Ac.5kb)
#PAAD
P001.H3K27Ac<- import("07bigwig/PAAD-P001_C.bw", format = "bigWig", which = H3K27Ac.5kb)
P002.H3K27Ac<- import("07bigwig/PAAD-P002_C.bw", format = "bigWig", which = H3K27Ac.5kb)
P008.H3K27Ac<- import("07bigwig/PAAD-P008_C.bw", format = "bigWig", which = H3K27Ac.5kb)
P010.H3K27Ac<- import("07bigwig/PAAD-P010_C.bw", format = "bigWig", which = H3K27Ac.5kb)
P011.H3K27Ac<- import("07bigwig/PAAD-P011_C.bw", format = "bigWig", which = H3K27Ac.5kb)
P013.H3K27Ac<- import("07bigwig/PAAD-P013_C.bw", format = "bigWig", which = H3K27Ac.5kb)
P016.H3K27Ac<- import("07bigwig/PAAD-P016_C.bw", format = "bigWig", which = H3K27Ac.5kb)
P021.H3K27Ac<- import("07bigwig/PAAD-P021_C.bw", format = "bigWig", which = H3K27Ac.5kb)
P033.H3K27Ac<- import("07bigwig/PAAD-P033_C.bw", format = "bigWig", which = H3K27Ac.5kb)
P034.H3K27Ac<- import("07bigwig/PAAD-P034_C.bw", format = "bigWig", which = H3K27Ac.5kb)
P053.H3K27Ac<- import("07bigwig/PAAD-P053_C.bw", format = "bigWig", which = H3K27Ac.5kb)
#SKCM
M12.H3K27Ac<- import("07bigwig/M-12_C.bw", format = "bigWig", which = H3K27Ac.5kb)
M22.H3K27Ac<- import("07bigwig/M-22_C.bw", format = "bigWig", which = H3K27Ac.5kb)
M48.H3K27Ac<- import("07bigwig/M-48_C.bw", format = "bigWig", which = H3K27Ac.5kb)
M52.H3K27Ac<- import("07bigwig/M-52_C.bw", format = "bigWig", which = H3K27Ac.5kb)
M57.H3K27Ac<- import("07bigwig/M-57_C.bw", format = "bigWig", which = H3K27Ac.5kb)
M61.H3K27Ac<- import("07bigwig/M-61_C.bw", format = "bigWig", which = H3K27Ac.5kb)
M84.H3K27Ac<- import("07bigwig/M-84_C.bw", format = "bigWig", which = H3K27Ac.5kb)
M96.H3K27Ac<- import("07bigwig/M-96_C.bw", format = "bigWig", which = H3K27Ac.5kb)
M940.H3K27Ac<- import("07bigwig/M-940_C.bw", format = "bigWig", which = H3K27Ac.5kb)
MA2058.H3K27Ac<- import("07bigwig/M-A2058_C.bw", format = "bigWig", which = H3K27Ac.5kb)
MCHL1.H3K27Ac<- import("07bigwig/M-CHL1_C.bw", format = "bigWig", which = H3K27Ac.5kb)
MG361.H3K27Ac<- import("07bigwig/M-G361_C.bw", format = "bigWig", which = H3K27Ac.5kb)
MHS934.H3K27Ac<- import("07bigwig/M-HS934_C.bw", format = "bigWig", which = H3K27Ac.5kb)
MMEWO.H3K27Ac<- import("07bigwig/M-MEWO_C.bw", format = "bigWig", which = H3K27Ac.5kb)
MSKMEL5.H3K27Ac<- import("07bigwig/M-SKMEL5_C.bw", format = "bigWig", which = H3K27Ac.5kb)
MUACC62.H3K27Ac<- import("07bigwig/M-UACC62_C.bw", format = "bigWig", which = H3K27Ac.5kb)
MWM88.H3K27Ac<- import("07bigwig/M-WM88_C.bw", format = "bigWig", which = H3K27Ac.5kb)
MWM239B.H3K27Ac<- import("07bigwig/M-WM239B_C.bw", format = "bigWig", which = H3K27Ac.5kb)
MWM793B.H3K27Ac<- import("07bigwig/M-WM793B_C.bw", format = "bigWig", which = H3K27Ac.5kb)
MA375.H3K27Ac<- import("07bigwig/M-A375_C.bw", format = "bigWig", which = H3K27Ac.5kb)
#SARC
SM381.H3K27Ac<- import("07bigwig/SCRC-M381_C.bw", format = "bigWig", which = H3K27Ac.5kb)
SM642.H3K27Ac<- import("07bigwig/SCRC-M642_C.bw", format = "bigWig", which = H3K27Ac.5kb)
SS462.H3K27Ac<- import("07bigwig/SCRC-S462_C.bw", format = "bigWig", which = H3K27Ac.5kb)
ST265.H3K27Ac<- import("07bigwig/SCRC-T265_C.bw", format = "bigWig", which = H3K27Ac.5kb)
SST26.H3K27Ac<- import("07bigwig/SCRC-ST26_C.bw", format = "bigWig", which = H3K27Ac.5kb)
SM724.H3K27Ac<- import("07bigwig/SCRC-M724_C.bw", format = "bigWig", which = H3K27Ac.5kb)
SST88.H3K27Ac<- import("07bigwig/SCRC-ST88_C.bw", format = "bigWig", which = H3K27Ac.5kb)

library(EnrichedHeatmap)

H3K27Ac.center<- resize(H3K27Ac.5kb, width =1, fix = "center")

B001.mat1<- normalizeToMatrix(B001.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
B003.mat1<- normalizeToMatrix(B003.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
B008.mat1<- normalizeToMatrix(B008.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
B010.mat1<- normalizeToMatrix(B010.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
B013.mat1<- normalizeToMatrix(B013.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
B014.mat1<- normalizeToMatrix(B014.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
B015.mat1<- normalizeToMatrix(B015.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
B019.mat1<- normalizeToMatrix(B019.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
B020.mat1<- normalizeToMatrix(B020.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
B021.mat1<- normalizeToMatrix(B021.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
B028.mat1<- normalizeToMatrix(B028.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
B034.mat1<- normalizeToMatrix(B034.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
B035.mat1<- normalizeToMatrix(B035.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
B038.mat1<- normalizeToMatrix(B038.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
B047.mat1<- normalizeToMatrix(B047.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
B050.mat1<- normalizeToMatrix(B050.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
B052.mat1<- normalizeToMatrix(B052.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
#COAD
C002.mat1<- normalizeToMatrix(C002.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
C003.mat1<- normalizeToMatrix(C003.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
C004.mat1<- normalizeToMatrix(C004.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
C007.mat1<- normalizeToMatrix(C007.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
C012.mat1<- normalizeToMatrix(C012.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
C013.mat1<- normalizeToMatrix(C013.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
C015.mat1<- normalizeToMatrix(C015.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
C016.mat1<- normalizeToMatrix(C016.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
C020.mat1<- normalizeToMatrix(C020.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
C021.mat1<- normalizeToMatrix(C021.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
C022.mat1<- normalizeToMatrix(C022.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
C027.mat1<- normalizeToMatrix(C027.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
#GBMC
G001.mat1<- normalizeToMatrix(G001.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
G002.mat1<- normalizeToMatrix(G002.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
G008.mat1<- normalizeToMatrix(G008.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
G010.mat1<- normalizeToMatrix(G010.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
G017.mat1<- normalizeToMatrix(G017.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
G018.mat1<- normalizeToMatrix(G018.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
G028.mat1<- normalizeToMatrix(G028.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
G029.mat1<- normalizeToMatrix(G029.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
G033.mat1<- normalizeToMatrix(G033.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
G044.mat1<- normalizeToMatrix(G044.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
#HNSC
H183X.mat1<- normalizeToMatrix(H183X.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
H584A.mat1<- normalizeToMatrix(H584A.H3K27Ac, H3K27Ac.center, value_column = "score",
                               mean_mode="w0", w=100, extend = 5000)
H1483.mat1<- normalizeToMatrix(H1483.H3K27Ac, H3K27Ac.center, value_column = "score",
                               mean_mode="w0", w=100, extend = 5000)
H1686.mat1<- normalizeToMatrix(H1686.H3K27Ac, H3K27Ac.center, value_column = "score",
                               mean_mode="w0", w=100, extend = 5000)
HDETR.mat1<- normalizeToMatrix(HDETR.H3K27Ac, H3K27Ac.center, value_column = "score",
                               mean_mode="w0", w=100, extend = 5000)
HHN4X.mat1<- normalizeToMatrix(HHN4X.H3K27Ac, H3K27Ac.center, value_column = "score",
                               mean_mode="w0", w=100, extend = 5000)
HHN5X.mat1<- normalizeToMatrix(HHN5X.H3K27Ac, H3K27Ac.center, value_column = "score",
                               mean_mode="w0", w=100, extend = 5000)
HHN30.mat1<- normalizeToMatrix(HHN30.H3K27Ac, H3K27Ac.center, value_column = "score",
                               mean_mode="w0", w=100, extend = 5000)
HHN31.mat1<- normalizeToMatrix(HHN31.H3K27Ac, H3K27Ac.center, value_column = "score",
                               mean_mode="w0", w=100, extend = 5000)
HJH22.mat1<- normalizeToMatrix(HJH22.H3K27Ac, H3K27Ac.center, value_column = "score",
                               mean_mode="w0", w=100, extend = 5000)
HJHU2.mat1<- normalizeToMatrix(HJHU2.H3K27Ac, H3K27Ac.center, value_column = "score",
                               mean_mode="w0", w=100, extend = 5000)
HM13L.mat1<- normalizeToMatrix(HM13L.H3K27Ac, H3K27Ac.center, value_column = "score",
                               mean_mode="w0", w=100, extend = 5000)
HM19L.mat1<- normalizeToMatrix(HM19L.H3K27Ac, H3K27Ac.center, value_column = "score",
                               mean_mode="w0", w=100, extend = 5000)
HM68L.mat1<- normalizeToMatrix(HM68L.H3K27Ac, H3K27Ac.center, value_column = "score",
                               mean_mode="w0", w=100, extend = 5000)
HMD1T.mat1<- normalizeToMatrix(HMD1T.H3K27Ac, H3K27Ac.center, value_column = "score",
                               mean_mode="w0", w=100, extend = 5000)
HMD8L.mat1<- normalizeToMatrix(HMD8L.H3K27Ac, H3K27Ac.center, value_column = "score",
                               mean_mode="w0", w=100, extend = 5000)
HOS19.mat1<- normalizeToMatrix(HOS19.H3K27Ac, H3K27Ac.center, value_column = "score",
                               mean_mode="w0", w=100, extend = 5000)
HPC5A.mat1<- normalizeToMatrix(HPC5A.H3K27Ac, H3K27Ac.center, value_column = "score",
                               mean_mode="w0", w=100, extend = 5000)
HPC5B.mat1<- normalizeToMatrix(HPC5B.H3K27Ac, H3K27Ac.center, value_column = "score",
                               mean_mode="w0", w=100, extend = 5000)
HPJ34.mat1<- normalizeToMatrix(HPJ34.H3K27Ac, H3K27Ac.center, value_column = "score",
                               mean_mode="w0", w=100, extend = 5000)
HSC61.mat1<- normalizeToMatrix(HSC61.H3K27Ac, H3K27Ac.center, value_column = "score",
                               mean_mode="w0", w=100, extend = 5000)
HU6XX.mat1<- normalizeToMatrix(HU6XX.H3K27Ac, H3K27Ac.center, value_column = "score",
                               mean_mode="w0", w=100, extend = 5000)
HU10B.mat1<- normalizeToMatrix(HU10B.H3K27Ac, H3K27Ac.center, value_column = "score",
                               mean_mode="w0", w=100, extend = 5000)
HU14B.mat1<- normalizeToMatrix(HU14B.H3K27Ac, H3K27Ac.center, value_column = "score",
                               mean_mode="w0", w=100, extend = 5000)
HU17A.mat1<- normalizeToMatrix(HU17A.H3K27Ac, H3K27Ac.center, value_column = "score",
                               mean_mode="w0", w=100, extend = 5000)
HU17B.mat1<- normalizeToMatrix(HU17B.H3K27Ac, H3K27Ac.center, value_column = "score",
                               mean_mode="w0", w=100, extend = 5000)
HU25X.mat1<- normalizeToMatrix(HU25X.H3K27Ac, H3K27Ac.center, value_column = "score",
                               mean_mode="w0", w=100, extend = 5000)
#LUNG
L009.mat1<- normalizeToMatrix(L009.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
L011.mat1<- normalizeToMatrix(L011.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
L020.mat1<- normalizeToMatrix(L020.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
L027.mat1<- normalizeToMatrix(L027.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
L029.mat1<- normalizeToMatrix(L029.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
L036.mat1<- normalizeToMatrix(L036.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
L044.mat1<- normalizeToMatrix(L044.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
L046.mat1<- normalizeToMatrix(L046.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
L062.mat1<- normalizeToMatrix(L062.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
L069.mat1<- normalizeToMatrix(L069.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
#OVCA
V007.mat1<- normalizeToMatrix(V007.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
V008.mat1<- normalizeToMatrix(V008.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
V009.mat1<- normalizeToMatrix(V009.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
V010.mat1<- normalizeToMatrix(V010.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
V013.mat1<- normalizeToMatrix(V013.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
V017.mat1<- normalizeToMatrix(V017.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
V020.mat1<- normalizeToMatrix(V020.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
V023.mat1<- normalizeToMatrix(V023.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
V026.mat1<- normalizeToMatrix(V026.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
V027.mat1<- normalizeToMatrix(V027.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
V037.mat1<- normalizeToMatrix(V037.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
#PAAD
P001.mat1<- normalizeToMatrix(P001.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
P002.mat1<- normalizeToMatrix(P002.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
P008.mat1<- normalizeToMatrix(P008.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
P010.mat1<- normalizeToMatrix(P010.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
P011.mat1<- normalizeToMatrix(P011.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
P013.mat1<- normalizeToMatrix(P013.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
P016.mat1<- normalizeToMatrix(P016.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
P021.mat1<- normalizeToMatrix(P021.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
P033.mat1<- normalizeToMatrix(P033.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
P034.mat1<- normalizeToMatrix(P034.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
P053.mat1<- normalizeToMatrix(P053.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
#SKCM
M12.mat1<- normalizeToMatrix(M12.H3K27Ac, H3K27Ac.center, value_column = "score",
                              mean_mode="w0", w=100, extend = 5000)
M22.mat1<- normalizeToMatrix(M22.H3K27Ac, H3K27Ac.center, value_column = "score",
                             mean_mode="w0", w=100, extend = 5000)
M48.mat1<- normalizeToMatrix(M48.H3K27Ac, H3K27Ac.center, value_column = "score",
                             mean_mode="w0", w=100, extend = 5000)
M52.mat1<- normalizeToMatrix(M52.H3K27Ac, H3K27Ac.center, value_column = "score",
                             mean_mode="w0", w=100, extend = 5000)
M57.mat1<- normalizeToMatrix(M57.H3K27Ac, H3K27Ac.center, value_column = "score",
                             mean_mode="w0", w=100, extend = 5000)
M61.mat1<- normalizeToMatrix(M61.H3K27Ac, H3K27Ac.center, value_column = "score",
                             mean_mode="w0", w=100, extend = 5000)
M84.mat1<- normalizeToMatrix(M84.H3K27Ac, H3K27Ac.center, value_column = "score",
                             mean_mode="w0", w=100, extend = 5000)
M96.mat1<- normalizeToMatrix(M96.H3K27Ac, H3K27Ac.center, value_column = "score",
                             mean_mode="w0", w=100, extend = 5000)
M940.mat1<- normalizeToMatrix(M940.H3K27Ac, H3K27Ac.center, value_column = "score",
                             mean_mode="w0", w=100, extend = 5000)
MA2058.mat1<- normalizeToMatrix(MA2058.H3K27Ac, H3K27Ac.center, value_column = "score",
                             mean_mode="w0", w=100, extend = 5000)
MCHL1.mat1<- normalizeToMatrix(MCHL1.H3K27Ac, H3K27Ac.center, value_column = "score",
                             mean_mode="w0", w=100, extend = 5000)
MG361.mat1<- normalizeToMatrix(MG361.H3K27Ac, H3K27Ac.center, value_column = "score",
                             mean_mode="w0", w=100, extend = 5000)
MHS934.mat1<- normalizeToMatrix(MHS934.H3K27Ac, H3K27Ac.center, value_column = "score",
                             mean_mode="w0", w=100, extend = 5000)
MMEWO.mat1<- normalizeToMatrix(MMEWO.H3K27Ac, H3K27Ac.center, value_column = "score",
                             mean_mode="w0", w=100, extend = 5000)
MSKMEL5.mat1<- normalizeToMatrix(MSKMEL5.H3K27Ac, H3K27Ac.center, value_column = "score",
                             mean_mode="w0", w=100, extend = 5000)
MUACC62.mat1<- normalizeToMatrix(MUACC62.H3K27Ac, H3K27Ac.center, value_column = "score",
                             mean_mode="w0", w=100, extend = 5000)
MWM88.mat1<- normalizeToMatrix(MWM88.H3K27Ac, H3K27Ac.center, value_column = "score",
                             mean_mode="w0", w=100, extend = 5000)
MWM239B.mat1<- normalizeToMatrix(MWM239B.H3K27Ac, H3K27Ac.center, value_column = "score",
                             mean_mode="w0", w=100, extend = 5000)
MWM793B.mat1<- normalizeToMatrix(MWM793B.H3K27Ac, H3K27Ac.center, value_column = "score",
                             mean_mode="w0", w=100, extend = 5000)
MA375.mat1<- normalizeToMatrix(MA375.H3K27Ac, H3K27Ac.center, value_column = "score",
                             mean_mode="w0", w=100, extend = 5000)
#SARC
SM381.mat1<- normalizeToMatrix(SM381.H3K27Ac, H3K27Ac.center, value_column = "score",
                             mean_mode="w0", w=100, extend = 5000)
SM642.mat1<- normalizeToMatrix(SM642.H3K27Ac, H3K27Ac.center, value_column = "score",
                               mean_mode="w0", w=100, extend = 5000)
SS462.mat1<- normalizeToMatrix(SS462.H3K27Ac, H3K27Ac.center, value_column = "score",
                               mean_mode="w0", w=100, extend = 5000)
ST265.mat1<- normalizeToMatrix(ST265.H3K27Ac, H3K27Ac.center, value_column = "score",
                               mean_mode="w0", w=100, extend = 5000)
SST26.mat1<- normalizeToMatrix(SST26.H3K27Ac, H3K27Ac.center, value_column = "score",
                               mean_mode="w0", w=100, extend = 5000)
SM724.mat1<- normalizeToMatrix(SM724.H3K27Ac, H3K27Ac.center, value_column = "score",
                               mean_mode="w0", w=100, extend = 5000)
SST88.mat1<- normalizeToMatrix(SST88.H3K27Ac, H3K27Ac.center, value_column = "score",
                               mean_mode="w0", w=100, extend = 5000)
set.seed(123)


#BRCA
B001_mean<- as.data.frame(colMeans(B001.mat1))
B003_mean<- as.data.frame(colMeans(B003.mat1))
B008_mean<- as.data.frame(colMeans(B008.mat1))
B010_mean<- as.data.frame(colMeans(B010.mat1))
B013_mean<- as.data.frame(colMeans(B013.mat1))
B014_mean<- as.data.frame(colMeans(B014.mat1))
B015_mean<- as.data.frame(colMeans(B015.mat1))
B019_mean<- as.data.frame(colMeans(B019.mat1))
B020_mean<- as.data.frame(colMeans(B020.mat1))
B021_mean<- as.data.frame(colMeans(B021.mat1))
B028_mean<- as.data.frame(colMeans(B028.mat1))
B034_mean<- as.data.frame(colMeans(B034.mat1))
B035_mean<- as.data.frame(colMeans(B035.mat1))
B038_mean<- as.data.frame(colMeans(B038.mat1))
B047_mean<- as.data.frame(colMeans(B047.mat1))
B050_mean<- as.data.frame(colMeans(B050.mat1))
B052_mean<- as.data.frame(colMeans(B052.mat1))
#COAD
C002_mean<- as.data.frame(colMeans(C002.mat1))
C003_mean<- as.data.frame(colMeans(C003.mat1))
C004_mean<- as.data.frame(colMeans(C004.mat1))
C007_mean<- as.data.frame(colMeans(C007.mat1))
C012_mean<- as.data.frame(colMeans(C012.mat1))
C013_mean<- as.data.frame(colMeans(C013.mat1))
C015_mean<- as.data.frame(colMeans(C015.mat1))
C016_mean<- as.data.frame(colMeans(C016.mat1))
C020_mean<- as.data.frame(colMeans(C020.mat1))
C021_mean<- as.data.frame(colMeans(C021.mat1))
C022_mean<- as.data.frame(colMeans(C022.mat1))
C027_mean<- as.data.frame(colMeans(C027.mat1))
#GBMC
G001_mean<- as.data.frame(colMeans(G001.mat1))
G002_mean<- as.data.frame(colMeans(G002.mat1))
G008_mean<- as.data.frame(colMeans(G008.mat1))
G010_mean<- as.data.frame(colMeans(G010.mat1))
G017_mean<- as.data.frame(colMeans(G017.mat1))
G018_mean<- as.data.frame(colMeans(G018.mat1))
G028_mean<- as.data.frame(colMeans(G028.mat1))
G029_mean<- as.data.frame(colMeans(G029.mat1))
G033_mean<- as.data.frame(colMeans(G033.mat1))
G044_mean<- as.data.frame(colMeans(G044.mat1))
#HNSC
H183X_mean<- as.data.frame(colMeans(H183X.mat1))
H584A_mean<- as.data.frame(colMeans(H584A.mat1))
H1483_mean<- as.data.frame(colMeans(H1483.mat1))
H1686_mean<- as.data.frame(colMeans(H1686.mat1))
HDETR_mean<- as.data.frame(colMeans(HDETR.mat1))
HHN4X_mean<- as.data.frame(colMeans(HHN4X.mat1))
HHN5X_mean<- as.data.frame(colMeans(HHN5X.mat1))
HHN30_mean<- as.data.frame(colMeans(HHN30.mat1))
HHN31_mean<- as.data.frame(colMeans(HHN31.mat1))
HJH22_mean<- as.data.frame(colMeans(HJH22.mat1))
HJHU2_mean<- as.data.frame(colMeans(HJHU2.mat1))
HM13L_mean<- as.data.frame(colMeans(HM13L.mat1))
HM19L_mean<- as.data.frame(colMeans(HM19L.mat1))
HM68L_mean<- as.data.frame(colMeans(HM68L.mat1))
HMD1T_mean<- as.data.frame(colMeans(HMD1T.mat1))
HMD8L_mean<- as.data.frame(colMeans(HMD8L.mat1))
HOS19_mean<- as.data.frame(colMeans(HOS19.mat1))
HPC5A_mean<- as.data.frame(colMeans(HPC5A.mat1))
HPC5B_mean<- as.data.frame(colMeans(HPC5B.mat1))
HPJ34_mean<- as.data.frame(colMeans(HPJ34.mat1))
HSC61_mean<- as.data.frame(colMeans(HSC61.mat1))
HU6XX_mean<- as.data.frame(colMeans(HU6XX.mat1))
HU10B_mean<- as.data.frame(colMeans(HU10B.mat1))
HU14B_mean<- as.data.frame(colMeans(HU14B.mat1))
HU17A_mean<- as.data.frame(colMeans(HU17A.mat1))
HU17B_mean<- as.data.frame(colMeans(HU17B.mat1))
HU25X_mean<- as.data.frame(colMeans(HU25X.mat1))
#LUNG
L009_mean<- as.data.frame(colMeans(L009.mat1))
L011_mean<- as.data.frame(colMeans(L011.mat1))
L020_mean<- as.data.frame(colMeans(L020.mat1))
L027_mean<- as.data.frame(colMeans(L027.mat1))
L029_mean<- as.data.frame(colMeans(L029.mat1))
L036_mean<- as.data.frame(colMeans(L036.mat1))
L044_mean<- as.data.frame(colMeans(L044.mat1))
L046_mean<- as.data.frame(colMeans(L046.mat1))
L062_mean<- as.data.frame(colMeans(L062.mat1))
L069_mean<- as.data.frame(colMeans(L069.mat1))
#OVCA
V007_mean<- as.data.frame(colMeans(V007.mat1))
V008_mean<- as.data.frame(colMeans(V008.mat1))
V009_mean<- as.data.frame(colMeans(V009.mat1))
V010_mean<- as.data.frame(colMeans(V010.mat1))
V013_mean<- as.data.frame(colMeans(V013.mat1))
V017_mean<- as.data.frame(colMeans(V017.mat1))
V020_mean<- as.data.frame(colMeans(V020.mat1))
V023_mean<- as.data.frame(colMeans(V023.mat1))
V026_mean<- as.data.frame(colMeans(V026.mat1))
V027_mean<- as.data.frame(colMeans(V027.mat1))
V037_mean<- as.data.frame(colMeans(V037.mat1))
#PAAD
P001_mean<- as.data.frame(colMeans(P001.mat1))
P002_mean<- as.data.frame(colMeans(P002.mat1))
P008_mean<- as.data.frame(colMeans(P008.mat1))
P010_mean<- as.data.frame(colMeans(P010.mat1))
P011_mean<- as.data.frame(colMeans(P011.mat1))
P013_mean<- as.data.frame(colMeans(P013.mat1))
P016_mean<- as.data.frame(colMeans(P016.mat1))
P021_mean<- as.data.frame(colMeans(P021.mat1))
P033_mean<- as.data.frame(colMeans(P033.mat1))
P034_mean<- as.data.frame(colMeans(P034.mat1))
P053_mean<- as.data.frame(colMeans(P053.mat1))
#SKCM
M12_mean<- as.data.frame(colMeans(M12.mat1))
M22_mean<- as.data.frame(colMeans(M22.mat1))
M48_mean<- as.data.frame(colMeans(M48.mat1))
M52_mean<- as.data.frame(colMeans(M52.mat1))
M57_mean<- as.data.frame(colMeans(M57.mat1))
M61_mean<- as.data.frame(colMeans(M61.mat1))
M84_mean<- as.data.frame(colMeans(M84.mat1))
M96_mean<- as.data.frame(colMeans(M96.mat1))
M940_mean<- as.data.frame(colMeans(M940.mat1))
MA2058_mean<- as.data.frame(colMeans(MA2058.mat1))
MCHL1_mean<- as.data.frame(colMeans(MCHL1.mat1))
MG361_mean<- as.data.frame(colMeans(MG361.mat1))
MHS934_mean<- as.data.frame(colMeans(MHS934.mat1))
MMEWO_mean<- as.data.frame(colMeans(MMEWO.mat1))
MSKMEL5_mean<- as.data.frame(colMeans(MSKMEL5.mat1))
MUACC62_mean<- as.data.frame(colMeans(MUACC62.mat1))
MWM88_mean<- as.data.frame(colMeans(MWM88.mat1))
MWM239B_mean<- as.data.frame(colMeans(MWM239B.mat1))
MWM793B_mean<- as.data.frame(colMeans(MWM793B.mat1))
MA375_mean<- as.data.frame(colMeans(MA375.mat1))
#SCRC
SM381_mean<- as.data.frame(colMeans(SM381.mat1))
SM642_mean<- as.data.frame(colMeans(SM642.mat1))
SS462_mean<- as.data.frame(colMeans(SS462.mat1))
ST265_mean<- as.data.frame(colMeans(ST265.mat1))
SST26_mean<- as.data.frame(colMeans(SST26.mat1))
SM724_mean<- as.data.frame(colMeans(SM724.mat1))
SST88_mean<- as.data.frame(colMeans(SST88.mat1))

library(magrittr)
library(dplyr)
library(lazyeval)
library(tidyr)
library(ggplot2)

H3K27Ac_all<- cbind(B001_mean, B003_mean, B008_mean, B010_mean,B013_mean, B014_mean,B015_mean, B019_mean,
                    B020_mean, B021_mean,B028_mean, B034_mean,B035_mean, B038_mean,B047_mean, B050_mean,
                    B052_mean, C002_mean, C003_mean, C004_mean, C007_mean, C012_mean,
                    C013_mean, C015_mean, C016_mean, C020_mean, C021_mean, C022_mean, C027_mean,
                    G001_mean, G002_mean, G008_mean, G010_mean, G017_mean, G018_mean, G028_mean, 
                    G029_mean, G033_mean, G044_mean, H183X_mean, H584A_mean, H1483_mean, H1686_mean, 
                    HDETR_mean, HHN4X_mean, HHN5X_mean, HHN30_mean, HHN31_mean, HJH22_mean, HJHU2_mean, 
                    HM13L_mean, HM19L_mean, HM68L_mean, HMD1T_mean, HMD8L_mean, HOS19_mean, HPC5A_mean, 
                    HPC5B_mean, HPJ34_mean, HSC61_mean, HU6XX_mean, HU10B_mean, HU14B_mean, HU17A_mean, 
                    HU17B_mean, HU25X_mean, L009_mean, L011_mean, L020_mean,  L027_mean, L029_mean, L036_mean, L044_mean, 
                    L046_mean, L062_mean, L069_mean, V007_mean, V008_mean, V009_mean, V010_mean, V013_mean, 
                    V017_mean, V020_mean, V023_mean, V026_mean, V027_mean, V037_mean, P001_mean, P002_mean, P008_mean, 
                    P010_mean, P011_mean, P013_mean, P016_mean, P021_mean, P033_mean, P034_mean, P053_mean,
                    M12_mean, M22_mean, M48_mean, M52_mean, M57_mean, M61_mean, M84_mean, M96_mean,
                    M940_mean, MA2058_mean,MCHL1_mean, MG361_mean, MHS934_mean, MMEWO_mean, MSKMEL5_mean, 
                    MUACC62_mean, MWM88_mean, MWM239B_mean, MWM793B_mean, MA375_mean, 
                    SM381_mean, SM642_mean, SS462_mean, ST265_mean, SST26_mean, 
                    SM724_mean, SST88_mean) %>%
  mutate(pos = rownames(P002_mean)) %>%
  dplyr::rename(BRCA_B001 = `colMeans(B001.mat1)`, BRCA_B003 = `colMeans(B003.mat1)`,
                BRCA_B008 = `colMeans(B008.mat1)`,BRCA_B010 = `colMeans(B010.mat1)`,
                BRCA_B013 = `colMeans(B013.mat1)`,BRCA_B014 = `colMeans(B014.mat1)`,
                BRCA_B015 = `colMeans(B015.mat1)`,BRCA_B019 = `colMeans(B019.mat1)`,
                BRCA_B020 = `colMeans(B020.mat1)`,BRCA_B021 = `colMeans(B021.mat1)`,
                BRCA_B028 = `colMeans(B028.mat1)`,BRCA_B034 = `colMeans(B034.mat1)`,
                BRCA_B035 = `colMeans(B035.mat1)`,BRCA_B038 = `colMeans(B038.mat1)`,
                BRCA_B047 = `colMeans(B047.mat1)`,BRCA_B050 = `colMeans(B050.mat1)`,
                BRCA_B052 = `colMeans(B052.mat1)`,COAD_C002 = `colMeans(C002.mat1)`,
                COAD_C003 = `colMeans(C003.mat1)`,COAD_C004 = `colMeans(C004.mat1)`,
                COAD_C007 = `colMeans(C007.mat1)`,COAD_C012 = `colMeans(C012.mat1)`,
                COAD_C013= `colMeans(C013.mat1)`,COAD_C015 = `colMeans(C015.mat1)`,
                COAD_C016 = `colMeans(C016.mat1)`,COAD_C020 = `colMeans(C020.mat1)`,
                COAD_C021 = `colMeans(C021.mat1)`,COAD_C022 = `colMeans(C022.mat1)`,
                COAD_C027 = `colMeans(C027.mat1)`,GBMC_G001 = `colMeans(G001.mat1)`,
                GBMC_G002 = `colMeans(G002.mat1)`,GBMC_G008 = `colMeans(G008.mat1)`,
                GBMC_G010 = `colMeans(G010.mat1)`,GBMC_G017 = `colMeans(G017.mat1)`,
                GBMC_G018 = `colMeans(G018.mat1)`,GBMC_G028 = `colMeans(G028.mat1)`,
                GBMC_G029 = `colMeans(G029.mat1)`,GBMC_G033 = `colMeans(G033.mat1)`,
                GBMC_G044 = `colMeans(G044.mat1)`,HNSC_183X = `colMeans(H183X.mat1)`,
                HNSC_584A = `colMeans(H584A.mat1)`,HNSC_1483 = `colMeans(H1483.mat1)`,
                HNSC_1686 = `colMeans(H1686.mat1)`,HNSC_DETR = `colMeans(HDETR.mat1)`,
                HNSC_HN4X = `colMeans(HHN4X.mat1)`,HNSC_HN5X = `colMeans(HHN5X.mat1)`,
                HNSC_HN30 = `colMeans(HHN30.mat1)`,HNSC_HN31 = `colMeans(HHN31.mat1)`,
                HNSC_JH22 = `colMeans(HJH22.mat1)`,HNSC_JHU2 = `colMeans(HJHU2.mat1)`,
                HNSC_M13L = `colMeans(HM13L.mat1)`,HNSC_M19L = `colMeans(HM19L.mat1)`,
                HNSC_M68L = `colMeans(HM68L.mat1)`,HNSC_MD1T = `colMeans(HMD1T.mat1)`,
                HNSC_MD8L = `colMeans(HMD8L.mat1)`,HNSC_OS19 = `colMeans(HOS19.mat1)`,
                HNSC_PC5A = `colMeans(HPC5A.mat1)`,HNSC_PC5B = `colMeans(HPC5B.mat1)`,
                HNSC_PJ34 = `colMeans(HPJ34.mat1)`,HNSC_SC61 = `colMeans(HSC61.mat1)`,
                HNSC_U6XX = `colMeans(HU6XX.mat1)`,HNSC_U10B = `colMeans(HU10B.mat1)`,
                HNSC_U14B = `colMeans(HU14B.mat1)`,HNSC_U17A = `colMeans(HU17A.mat1)`,
                HNSC_U17B = `colMeans(HU17B.mat1)`,HNSC_U25X = `colMeans(HU25X.mat1)`,
                LUNG_L009 = `colMeans(L009.mat1)`,LUNG_L011 = `colMeans(L011.mat1)`,
                LUNG_L020 = `colMeans(L020.mat1)`,LUNG_L027 = `colMeans(L027.mat1)`,
                LUNG_L029 = `colMeans(L029.mat1)`,LUNG_L036 = `colMeans(L036.mat1)`,
                LUNG_L044 = `colMeans(L044.mat1)`,LUNG_L046 = `colMeans(L046.mat1)`,
                LUNG_L062 = `colMeans(L062.mat1)`,LUNG_L069 = `colMeans(L069.mat1)`,
                OVCA_V007 = `colMeans(V007.mat1)`,OVCA_V008 = `colMeans(V008.mat1)`,
                OVCA_V009 = `colMeans(V009.mat1)`,OVCA_V010 = `colMeans(V010.mat1)`,
                OVCA_V013 = `colMeans(V013.mat1)`,OVCA_V017 = `colMeans(V007.mat1)`,
                OVCA_V020 = `colMeans(V020.mat1)`,OVCA_V027 = `colMeans(V027.mat1)`,
                OVCA_V023 = `colMeans(V023.mat1)`,OVCA_V026 = `colMeans(V026.mat1)`,
                OVCA_V037 = `colMeans(V037.mat1)`,PAAD_P001 = `colMeans(P001.mat1)`,
                PAAD_P002 = `colMeans(P002.mat1)`,PAAD_P008 = `colMeans(P008.mat1)`,
                PAAD_P010 = `colMeans(P010.mat1)`,PAAD_P011 = `colMeans(P011.mat1)`,
                PAAD_P013 = `colMeans(P013.mat1)`,PAAD_P016 = `colMeans(P016.mat1)`,
                PAAD_P021 = `colMeans(P021.mat1)`,PAAD_P033 = `colMeans(P033.mat1)`,
                PAAD_P034 = `colMeans(P034.mat1)`,PAAD_P053 = `colMeans(P053.mat1)`,
                SKCM_M12 = `colMeans(M12.mat1)`,SKCM_M22 = `colMeans(M22.mat1)`,
                SKCM_M48 = `colMeans(M48.mat1)`,SKCM_M52 = `colMeans(M52.mat1)`,
                SKCM_M57 = `colMeans(M57.mat1)`,SKCM_M61 = `colMeans(M61.mat1)`,
                SKCM_M84 = `colMeans(M84.mat1)`,SKCM_M96 = `colMeans(M96.mat1)`,
                SKCM_M940 = `colMeans(M940.mat1)`,SKCM_A2058 = `colMeans(MA2058.mat1)`,
                SKCM_CHL1 = `colMeans(MCHL1.mat1)`,SKCM_G361 = `colMeans(MG361.mat1)`,
                SKCM_HS934 = `colMeans(MHS934.mat1)`,SKCM_MEWO = `colMeans(MMEWO.mat1)`,
                SKCM_SKMEL5 = `colMeans(MSKMEL5.mat1)`,SKCM_UACC62 = `colMeans(MUACC62.mat1)`,
                SKCM_WM88 = `colMeans(MWM88.mat1)`,SKCM_WM239B = `colMeans(MWM239B.mat1)`,
                SKCM_WM793B = `colMeans(MWM793B.mat1)`,SKCM_A375 = `colMeans(MA375.mat1)`,
                SARC_M381 = `colMeans(SM381.mat1)`,SARC_M642 = `colMeans(SM642.mat1)`,
                SARC_S462 = `colMeans(SS462.mat1)`,SARC_T265 = `colMeans(ST265.mat1)`,
                SARC_ST26 = `colMeans(SST26.mat1)`,SARC_M724 = `colMeans(SM724.mat1)`,
                SARC_ST88 = `colMeans(SST88.mat1)`) %>%
  gather(sample, value, 1:125)

write.table(x = H3K27Ac_all, file = "H3K27Ac_all.tsv", quote = FALSE, sep = "\t", row.names = FALSE)

H3K27Ac_all$pos<- factor(H3K27Ac_all$pos, levels= rownames(L027_mean))

pdf("CCLE_H3K27Ac_lineplot.pdf")
ggplot(H3K27Ac_all, aes(x = pos,y = value, group = sample)) + geom_line(aes(color = sample), size = 1.2) +
  theme_classic(base_size = 18) +
  theme(axis.ticks.x = element_blank()) +
  scale_x_discrete(breaks = c("u1", "d1", "d50"), labels =c ("-5kb", "center", "5kb")) +
  xlab(NULL) +
  ggtitle("CCLE H3K27Ac signal")
theme(panel.spacing = unit(2, "lines"))
dev.off()

H3K27Ac_average<- H3K27Ac_all %>% separate(sample, c("sample", "replicate")) %>%
  group_by(pos, sample) %>%
  summarise(value = mean(value))

pdf("CCLE_H3K27Ac_average_line.pdf")
ggplot(H3K27Ac_average, aes(x = pos,y = value, group = sample)) + geom_line(aes(color = sample), size = 1.2) +
  theme_classic(base_size = 18) +
  theme(axis.ticks.x = element_blank()) +
  scale_x_discrete(breaks = c("u1", "d1", "d50"), labels =c ("-5kb", "center", "5kb")) +
  xlab(NULL) +
  ggtitle("CCLE H3K27Ac_average Enhancer signal") +
  theme(panel.spacing = unit(2, "lines"))
dev.off() 
