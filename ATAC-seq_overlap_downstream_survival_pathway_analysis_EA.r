require(TCGAbiolinks)
require(stringr)
require(SummarizedExperiment)
require(TCGAutils)
require(survminer)
library(plyr)
require(survival)
require(stringi)

dta <-  readRDS('~/Box Sync//Working Projects/ATACSeq_Cluster4/Datasets/Samples_percentages_def_19.rds')


n=50
top_n <- names(sort(dta[,4],decreasing = T)[1:n])
bottom_n <- names(sort(dta[,4],decreasing = F)[1:n])


# Naming
{
  gdc.file <- "https://api.gdc.cancer.gov/data/7a3d7067-09d6-4acf-82c8-a1a81febf72c"
  samples.ids <- readr::read_tsv(gdc.file, col_types = readr::cols())
  
  top_n_rename <- top_n
  top_n_barcodes <- top_n
  for (i in 1:length(top_n)){
    ind <- which(str_detect(samples.ids$bam_prefix,gsub("_","-",top_n[i])))
    top_n_rename[i] <- samples.ids$Case_ID[ind[1]]
    top_n_barcodes[i] <- UUIDtoBarcode(samples.ids$Case_UUID[ind[1]], from_type = "case_id")$submitter_id
  }
  
  bottom_n_rename <- bottom_n
  bottom_n_barcodes <- bottom_n
  for (i in 1:length(bottom_n)){
    ind <- which(str_detect(samples.ids$bam_prefix,gsub("_","-",bottom_n[i])))
    bottom_n_rename[i] <- samples.ids$Case_ID[ind[1]]
    bottom_n_barcodes[i] <- UUIDtoBarcode(samples.ids$Case_UUID[ind[1]], from_type = "case_id")$submitter_id
  }
  

}




# Preprocessing of Gene Expression
{
  
  # Query platform Illumina HiSeq with a list of barcode 
  query <- GDCquery(project = c("TCGA-ACC","TCGA-ESCA","TCGA-GBM","TCGA-HNSC","TCGA-MESO", 
                                "TCGA-BRCA", "TCGA-KIRP", "TCGA-KIRC","TCGA-STAD","TCGA-COAD",
                                "TCGA-PRAD"),
                    data.category = "Gene expression",
                    data.type = "Gene expression quantification",
                    experimental.strategy = "RNA-Seq",
                    platform = "Illumina HiSeq",
                    file.type = "results",
                    barcode = c(top_n_barcodes, bottom_n_barcodes), 
                    legacy = TRUE)
  
  
  # Download a list of barcodes with platform IlluminaHiSeq_RNASeqV2
  GDCdownload(query)
  
  # Prepare expression matrix with geneID in the rows and samples (barcode) in the columns
  # rsem.genes.results as values
  RnaseqSE <- GDCprepare(query)
  
  Gene_Matrix <- assay(RnaseqSE,"raw_count") # or Gene_Matrix <- assay(RnaseqSE,"raw_count")
  
  Gene_Matrix<- cbind(Gene_Matrix_t,Gene_Matrix_b)
  # For gene expression if you need to see a boxplot correlation and AAIC plot to define outliers you can run
  Rnaseq_CorOutliers <- TCGAanalyze_Preprocessing(RnaseqSE)
}






# Differentially Expressed Genes
{

  # normalization of genes
  dataNorm <- TCGAanalyze_Normalization(tabDF = Gene_Matrix, geneInfo =  geneInfo)
  
  # quantile filter of genes
  dataFilt <- TCGAanalyze_Filtering(tabDF = dataNorm,
                                    method = "quantile", 
                                    qnt.cut =  0.25)
  
  # selection of normal samples "NT"
  samples_Top <- colnames(dataFilt)[1:50]
  
  # selection of tumor samples "TP"
  samples_Bottom <- colnames(dataFilt)[51:100]
  
  # Diff.expr.analysis (DEA)
  dataDEGs <- TCGAanalyze_DEA(mat1 = dataFilt[,samples_Top],
                              mat2 = dataFilt[,samples_Bottom],
                              Cond1type = "Top",
                              Cond2type = "Bottom",
                              fdr.cut = 0.01 ,
                              logFC.cut = 1,
                              method = "glmLRT")
  
  # DEGs table with expression values in top and bottom samples
  dataDEGsFiltLevel <- TCGAanalyze_LevelTab(dataDEGs,"Top","Bottom",
                                            dataFilt[,samples_Top],dataFilt[,samples_Bottom])
}


# Pathway Analysis

#function
{
  own_survival <-function (data, clusterCol = NULL, legend = "Legend", labels = NULL, 
                           risk.table = TRUE, xlim = NULL, main = "Kaplan-Meier Overall Survival Curves", 
                           ylab = "Probability of survival", xlab = "Time since diagnosis (days)", 
                           filename = "survival.pdf", color = NULL, height = 8, width = 12, 
                           dpi = 300, pvalue = TRUE, conf.int = TRUE, ...) 
  {
    .e <- environment()
    if (!all(c("vital_status", "days_to_death", "days_to_last_follow_up") %in% 
             colnames(data))) 
      stop("Columns vital_status, days_to_death and  days_to_last_follow_up should be in data frame")
    if (is.null(color)) {
      color <- rainbow(length(unique(data[, clusterCol])))
    }
    group <- NULL
    if (is.null(clusterCol)) {
      stop("Please provide the clusterCol argument")
    }
    else if (length(unique(data[, clusterCol])) == 1) {
      stop(paste0("Sorry, but I'm expecting at least two groups\n", 
                  "  Only this group found: ", unique(data[, clusterCol])))
    }
    notDead <- is.na(data$days_to_death)
    if (any(notDead == TRUE)) {
      data[notDead, "days_to_death"] <- data[notDead, "days_to_last_follow_up"]
    }
    if (length(data[which((data[, "days_to_death"] < 0) == T), 
                    "sample"]) > 0 & "sample" %in% colnames(data)) {
      message("Incosistencies in the data were found. The following samples have a negative days_to_death value:")
      message(paste(data[which((data[, "days_to_death"] < 0) == 
                                 T), "sample"], collapse = ", "))
    }
    if (any(is.na(data[, "days_to_death"])) & "sample" %in% colnames(data)) {
      message("Incosistencies in the data were found. The following samples have a NA days_to_death value:")
      message(paste(data[is.na(data[, "days_to_death"]), "sample"], 
                    collapse = ", "))
    }
    data$s <- grepl("dead|deceased", data$vital_status, ignore.case = TRUE)
    data$type <- as.factor(data[, clusterCol])
    data <- data[, c("days_to_death", "s", "type")]
    f.m <- formula(Surv(as.numeric(data$days_to_death), event = data$s) ~ 
                     data$type)
    fit <- do.call(survfit, list(formula = f.m, data = data))
    label.add.n <- function(x) {
      na.idx <- is.na(data[, "days_to_death"])
      negative.idx <- data[, "days_to_death"] < 0
      idx <- !(na.idx | negative.idx)
      return(paste0(x, " (n = ", sum(data[idx, "type"] == x), 
                    ")"))
    }
    if (is.null(labels)) {
      d <- survminer::surv_summary(fit, data = data)
      order <- unname(sapply(levels(d$strata), function(x) unlist(str_split(x, 
                                                                            "="))[2]))
      labels <- sapply(order, label.add.n)
    }
    if (length(xlim) == 1) {
      xlim <- c(0, xlim)
    }
    suppressWarnings({
      surv <- ggsurvplot(fit, risk.table = risk.table, pval = pvalue, 
                         conf.int = conf.int, xlim = xlim, main = main, xlab = xlab, 
                         legend.title = legend, legend.labs = labels, palette = color, 
                         font.x = c(35, "bold", "black"), font.y = c(35, "bold", "black"),
                         font.tickslab = c(22, "bold", "black"),
                         font.legend = c(28, "bold", "black"),
                         pval.size = 15,
                         ...)
    })
    if (!is.null(filename)) {
      ggsave(surv$plot, filename = filename, width = width, 
             height = height, dpi = dpi)
      message(paste0("File saved as: ", filename))
      if (risk.table) {
        g1 <- ggplotGrob(surv$plot)
        g2 <- ggplotGrob(surv$table)
        min_ncol <- min(ncol(g2), ncol(g1))
        g <- gtable_rbind(g1[, 1:min_ncol], g2[, 1:min_ncol], 
                          size = "last")
        ggsave(g, filename = filename, width = width, height = height, 
               dpi = dpi)
      }
    }
    else {
      return(surv)
    }
  }
  
}
{
  # Enrichment Analysis EA
  # Gene Ontology (GO) and Pathway enrichment by DEGs list
  Genelist <- rownames(dataDEGsFiltLevel)
  
  system.time(ansEA <- TCGAanalyze_EAcomplete(TFname="DEA genes Top Vs Bottom",Genelist))
  
  # Enrichment Analysis EA (TCGAVisualize)
  # Gene Ontology (GO) and Pathway enrichment barPlot
  
  TCGAvisualize_EAbarplot(tf = rownames(ansEA$ResBP), 
                          GOBPTab = ansEA$ResBP,
                          GOCCTab = ansEA$ResCC,
                          GOMFTab = ansEA$ResMF,
                          PathTab = ansEA$ResPat,
                          nRGTab = Genelist, 
                          nBar = 10)
  
}



# Survivor Analysis
{
  table(sapply(top_n, function(x) strsplit(x,'_')[[1]][1]))
  table(sapply(bottom_n, function(x) strsplit(x,'_')[[1]][1]))
  
  acc_clinic <- GDCquery_clinic("TCGA-ACC", "clinical")
  meso_clinic <- GDCquery_clinic("TCGA-MESO", "clinical")
  tgct_clinic <- GDCquery_clinic("TCGA-TGCT", "clinical")
  blca_clinic <- GDCquery_clinic("TCGA-BLCA", "clinical")
  brca_clinic <- GDCquery_clinic("TCGA-BRCA", "clinical")
  cesc_clinic <- GDCquery_clinic("TCGA-CESC", "clinical")
  chol_clinic <- GDCquery_clinic("TCGA-CHOL", "clinical")
  coad_clinic <- GDCquery_clinic("TCGA-COAD", "clinical")
  gbmx_clinic <- GDCquery_clinic("TCGA-GBM", "clinical")
  hnsc_clinic <- GDCquery_clinic("TCGA-HNSC", "clinical")
  kirc_clinic <- GDCquery_clinic("TCGA-KIRC", "clinical")
  kirp_clinic <- GDCquery_clinic("TCGA-KIRP", "clinical")
  lggx_clinic <- GDCquery_clinic("TCGA-LGG", "clinical")
  luad_clinic <- GDCquery_clinic("TCGA-LUAD", "clinical")
  stad_clinic <- GDCquery_clinic("TCGA-STAD", "clinical")
  lihc_clinic <- GDCquery_clinic("TCGA-LIHC", "clinical")
  lusc_clinic <- GDCquery_clinic("TCGA-LUSC", "clinical")
  pcpg_clinic <- GDCquery_clinic("TCGA-PCPG", "clinical")
  skcm_clinic <- GDCquery_clinic("TCGA-SKCM", "clinical")
  esca_clinic <- GDCquery_clinic("TCGA-ESCA", "clinical")
  prad_clinic <- GDCquery_clinic("TCGA-PRAD", "clinical")
  thca_clinic <- GDCquery_clinic("TCGA-THCA", "clinical")
  ucec_clinic <- GDCquery_clinic("TCGA-UCEC", "clinical")
  pcpg_clinic <- GDCquery_clinic("TCGA-PCPG", "clinical")
  skcm_clinic <- GDCquery_clinic("TCGA-SKCM", "clinical")
  
  
  top_clinic <- c()
  
  c_columns <- Reduce(intersect, list(colnames(acc_clinic),
                                      colnames(meso_clinic),
                                      colnames(tgct_clinic),
                                      colnames(brca_clinic),
                                      colnames(kirp_clinic),
                                      colnames(esca_clinic),
                                      colnames(prad_clinic),
                                      colnames(thca_clinic),
                                      colnames(thca_clinic),
                                      colnames(chol_clinic),
                                      colnames(lggx_clinic)))
  ind <- c()
  for (i in 1:length(top_n_rename)){
    ind <- c(ind, which(str_detect(top_n_rename[i], acc_clinic$submitter_id)))
  }
  top_clinic <- acc_clinic[ind,c_columns]
  ind <- c()
  for (i in 1:length(top_n_rename)){
    ind <- c(ind, which(str_detect(top_n_rename[i], brca_clinic$submitter_id)))
  }
  top_clinic <- rbind(top_clinic, brca_clinic[ind,c_columns])
  ind <- c()
  for (i in 1:length(top_n_rename)){
    ind <- c(ind, which(str_detect(top_n_rename[i], blca_clinic$submitter_id)))
  }
  top_clinic <- rbind(top_clinic, blca_clinic[ind,c_columns])
  ind <- c()
  for (i in 1:length(top_n_rename)){
    ind <- c(ind, which(str_detect(top_n_rename[i], coad_clinic$submitter_id)))
  }
  top_clinic <- rbind(top_clinic, coad_clinic[ind,c_columns])
  ind <- c()
  for (i in 1:length(top_n_rename)){
    ind <- c(ind, which(str_detect(top_n_rename[i], esca_clinic$submitter_id)))
  }
  top_clinic <- rbind(top_clinic, esca_clinic[ind,c_columns])
  ind <- c()
  for (i in 1:length(top_n_rename)){
    ind <- c(ind, which(str_detect(top_n_rename[i], lggx_clinic$submitter_id)))
  }
  top_clinic <- rbind(top_clinic, lggx_clinic[ind,c_columns])
  ind <- c()
  for (i in 1:length(top_n_rename)){
    ind <- c(ind, which(str_detect(top_n_rename[i], gbmx_clinic$submitter_id)))
  }
  top_clinic <- rbind(top_clinic, gbmx_clinic[ind,c_columns])
  ind <- c()
  for (i in 1:length(top_n_rename)){
    ind <- c(ind, which(str_detect(top_n_rename[i], hnsc_clinic$submitter_id)))
  }
  top_clinic <- rbind(top_clinic, hnsc_clinic[ind,c_columns])
  ind <- c()
  for (i in 1:length(top_n_rename)){
    ind <- c(ind, which(str_detect(top_n_rename[i], luad_clinic$submitter_id)))
  }
  top_clinic <- rbind(top_clinic, luad_clinic[ind,c_columns])
  ind <- c()
  for (i in 1:length(top_n_rename)){
    ind <- c(ind, which(str_detect(top_n_rename[i], lusc_clinic$submitter_id)))
  }
  top_clinic <- rbind(top_clinic, lusc_clinic[ind,c_columns])
  ind <- c()
  for (i in 1:length(top_n_rename)){
    ind <- c(ind, which(str_detect(top_n_rename[i], kirc_clinic$submitter_id)))
  }
  top_clinic <- rbind(top_clinic, kirc_clinic[ind,c_columns])
  ind <- c()
  for (i in 1:length(top_n_rename)){
    ind <- c(ind, which(str_detect(top_n_rename[i], kirp_clinic$submitter_id)))
  }
  top_clinic <- rbind(top_clinic, kirp_clinic[ind,c_columns])
  ind <- c()
  for (i in 1:length(top_n_rename)){
    ind <- c(ind, which(str_detect(top_n_rename[i], tgct_clinic$submitter_id)))
  }
  top_clinic <- rbind(top_clinic, tgct_clinic[ind,c_columns])
  ind <- c()
  for (i in 1:length(top_n_rename)){
    ind <- c(ind, which(str_detect(top_n_rename[i], meso_clinic$submitter_id)))
  }
  top_clinic <- rbind(top_clinic, meso_clinic[ind,c_columns])
  ind <- c()
  for (i in 1:length(top_n_rename)){
    ind <- c(ind, which(str_detect(top_n_rename[i], pcpg_clinic$submitter_id)))
  }
  top_clinic <- rbind(top_clinic, pcpg_clinic[ind,c_columns])
  ind <- c()
  for (i in 1:length(top_n_rename)){
    ind <- c(ind, which(str_detect(top_n_rename[i], skcm_clinic$submitter_id)))
  }
  top_clinic <- rbind(top_clinic, skcm_clinic[ind,c_columns])
  ind <- c()
  for (i in 1:length(top_n_rename)){
    ind <- c(ind, which(str_detect(top_n_rename[i], stad_clinic$submitter_id)))
  }
  top_clinic <- rbind(top_clinic, stad_clinic[ind,c_columns])
  ind <- c()
  for (i in 1:length(top_n_rename)){
    ind <- c(ind, which(str_detect(top_n_rename[i], prad_clinic$submitter_id)))
  }
  top_clinic <- rbind(top_clinic, prad_clinic[ind,c_columns])
  
  
  ind <- c()
  for (i in 1:length(bottom_n_rename)){
    ind <- c(ind, which(str_detect(bottom_n_rename[i], acc_clinic$submitter_id)))
  }
  top_clinic <- rbind(top_clinic, acc_clinic[ind,c_columns])
  ind <- c()
  for (i in 1:length(bottom_n_rename)){
    ind <- c(ind, which(str_detect(bottom_n_rename[i], blca_clinic$submitter_id)))
  }
  top_clinic <- rbind(top_clinic, blca_clinic[ind,c_columns])
  ind <- c()
  for (i in 1:length(bottom_n_rename)){
    ind <- c(ind, which(str_detect(bottom_n_rename[i], brca_clinic$submitter_id)))
  }
  top_clinic <- rbind(top_clinic, brca_clinic[ind,c_columns])
  ind <- c()
  for (i in 1:length(bottom_n_rename)){
    ind <- c(ind, which(str_detect(bottom_n_rename[i], coad_clinic$submitter_id)))
  }
  top_clinic <- rbind(top_clinic, coad_clinic[ind,c_columns])
  ind <- c()
  for (i in 1:length(bottom_n_rename)){
    ind <- c(ind, which(str_detect(bottom_n_rename[i], cesc_clinic$submitter_id)))
  }
  top_clinic <- rbind(top_clinic, cesc_clinic[ind,c_columns])
  ind <- c()
  for (i in 1:length(bottom_n_rename)){
    ind <- c(ind, which(str_detect(bottom_n_rename[i], esca_clinic$submitter_id)))
  }
  top_clinic <- rbind(top_clinic, esca_clinic[ind,c_columns])
  ind <- c()
  for (i in 1:length(bottom_n_rename)){
    ind <- c(ind, which(str_detect(bottom_n_rename[i], lggx_clinic$submitter_id)))
  }
  top_clinic <- rbind(top_clinic, lggx_clinic[ind,c_columns])
  ind <- c()
  for (i in 1:length(bottom_n_rename)){
    ind <- c(ind, which(str_detect(bottom_n_rename[i], kirp_clinic$submitter_id)))
  }
  top_clinic <- rbind(top_clinic, kirp_clinic[ind,c_columns])
  ind <- c()
  for (i in 1:length(bottom_n_rename)){
    ind <- c(ind, which(str_detect(bottom_n_rename[i], luad_clinic$submitter_id)))
  }
  top_clinic <- rbind(top_clinic, luad_clinic[ind,c_columns])
  ind <- c()
  for (i in 1:length(bottom_n_rename)){
    ind <- c(ind, which(str_detect(bottom_n_rename[i], lusc_clinic$submitter_id)))
  }
  top_clinic <- rbind(top_clinic, lusc_clinic[ind,c_columns])
  ind <- c()
  for (i in 1:length(bottom_n_rename)){
    ind <- c(ind, which(str_detect(bottom_n_rename[i], meso_clinic$submitter_id)))
  }
  top_clinic <- rbind(top_clinic, meso_clinic[ind,c_columns])
  ind <- c()
  for (i in 1:length(bottom_n_rename)){
    ind <- c(ind, which(str_detect(bottom_n_rename[i], gbmx_clinic$submitter_id)))
  }
  top_clinic <- rbind(top_clinic, gbmx_clinic[ind,c_columns])
  ind <- c()
  for (i in 1:length(bottom_n_rename)){
    ind <- c(ind, which(str_detect(bottom_n_rename[i], prad_clinic$submitter_id)))
  }
  top_clinic <- rbind(top_clinic, prad_clinic[ind,c_columns])
  ind <- c()
  for (i in 1:length(bottom_n_rename)){
    ind <- c(ind, which(str_detect(bottom_n_rename[i], pcpg_clinic$submitter_id)))
  }
  top_clinic <- rbind(top_clinic, pcpg_clinic[ind,c_columns])
  ind <- c()
  for (i in 1:length(bottom_n_rename)){
    ind <- c(ind, which(str_detect(bottom_n_rename[i], skcm_clinic$submitter_id)))
  }
  top_clinic <- rbind(top_clinic, skcm_clinic[ind,c_columns])
  ind <- c()
  for (i in 1:length(bottom_n_rename)){
    ind <- c(ind, which(str_detect(bottom_n_rename[i], stad_clinic$submitter_id)))
  }
  top_clinic <- rbind(top_clinic, stad_clinic[ind,c_columns])
  ind <- c()
  for (i in 1:length(bottom_n_rename)){
    ind <- c(ind, which(str_detect(bottom_n_rename[i], hnsc_clinic$submitter_id)))
  }
  top_clinic <- rbind(top_clinic, hnsc_clinic[ind,c_columns])
  ind <- c()
  for (i in 1:length(bottom_n_rename)){
    ind <- c(ind, which(str_detect(bottom_n_rename[i], ucec_clinic$submitter_id)))
  }
  top_clinic <- rbind(top_clinic, ucec_clinic[ind,c_columns])
  
  
  top_clinic$cluster4 <- c(rep(c("top","bottom"),c(50,50)))
  
  
  
  
  own_survival(top_clinic,
               "cluster4",
               main = "TCGA Set\n Cluster 4",
               risk.table = F,
               conf.int = F,
               color = c("Blue",'Red'),xlim = c(0,4000),dpi=600)
  
  
}