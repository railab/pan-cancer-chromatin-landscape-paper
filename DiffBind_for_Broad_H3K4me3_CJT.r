DiffBind for CCLE Broad H3K4me3#

library(DiffBind)
library(DESeq2)
library(edgeR)
library(ComplexHeatmap)
library(dplyr)
library(genefilter)
library(RColorBrewer)

#Set working directory
setwd("/Volumes/Terranova1/CCLE_145CellLines_ChIP-seq/04aln_downsample/")

#create cell mark files for DiffBind -- intial .txt file contains ChIP/Control Bam files and Peak files
CCLE_Broad_H3K4me3 <- read.table("CCLE_Broad_H3K4me3.txt", sep = "\t", header = F)
names(CCLE_Broad_H3K4me3) <- c("bamReads", "bamControl", "Peaks")
CCLE_Broad_H3K4me3 <- CCLE_Broad_H3K4me3 %>% mutate(Peakcaller = "bed") %>% mutate(Factor = "BroadH3K4me3") %>% mutate(SampleID = gsub("^(.{4}).*", "\\1", bamReads)) %>% mutate(ControlID=paste0(SampleID,"c"))
write.table(x = CCLE_Broad_H3K4me3, file = "/Volumes/Terranova1/CCLE_145CellLines_ChIP-seq/04aln_downsample/CCLE_Broad_H3K4me3.tsv", sep = "\t")

CCLE_Sharp_H3K4me3 <- read.table("CCLE_Sharp_H3K4me3.txt", sep = "\t", header = F)
names(CCLE_Sharp_H3K4me3) <- c("bamReads", "bamControl", "Peaks")
CCLE_Sharp_H3K4me3 <- CCLE_Sharp_H3K4me3 %>% mutate(Peakcaller = "bed") %>% mutate(Factor = "SharpH3K4me3") %>% mutate(SampleID = gsub("^(.{4}).*", "\\1", bamReads)) %>% mutate(ControlID=paste0(SampleID,"c"))
write.table(x = CCLE_Sharp_H3K4me3, file = "/Volumes/Terranova1/CCLE_145CellLines_ChIP-seq/04aln_downsample/CCLE_Sharp_H3K4me3.tsv", sep = "\t")

#create a dba.object#
CCLE_Broad_H3K4me3 <- dba(sampleSheet = "CCLE_Broad_H3K4me3_Roadmap_NormalvsTumor_CommonSites_Matched.csv", minOverlap = 1)
CCLE_Sharp_H3K4me3 <- dba(sampleSheet = "CCLE_Sharp_H3K4me3_Roadmap_NormalvsTumor_CommonSites_Matched.csv", minOverlap = 1)

#count reads in binding site intervals: Scoring metric can be changed#
CCLE_Broad_H3K4me3_CommonSites_TumorvsNormal_matched <- dba.count(CCLE_Broad_H3K4me3, minOverlap = 1, score = DBA_SCORE_RPKM, fragmentSize = 200)
CCLE_Sharp_H3K4me3_CommonSites_TumorvsNormal_matched <- dba.count(CCLE_Sharp_H3K4me3, minOverlap = 1, score = DBA_SCORE_RPKM, fragmentSize = 200)

#count reads in binding site intervals: Scoring metric can be changed#
CCLE_Broad_H3K4me3_READS_MINUS <- dba.count(CCLE_Broad_H3K4me3, minOverlap = 50, score = DBA_SCORE_READS_MINUS, fragmentSize = 200)
CCLE_Sharp_H3K4me3_READS_MINUS <- dba.count(CCLE_Sharp_H3K4me3, minOverlap = 50, score = DBA_SCORE_READS_MINUS, fragmentSize = 200)

#PCA Plot
dba.plotPCA(CCLE_Broad_H3K4me3_CommonSites_TumorvsNormal_matched, DBA_ID)
dba.plotPCA(CCLE_Sharp_H3K4me3_Roadmap_noHNSC_CommonSites_RPKM_paired)
