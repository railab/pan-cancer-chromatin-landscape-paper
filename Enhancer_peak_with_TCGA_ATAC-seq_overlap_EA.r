# big wig analysis 

# Load required libraries
library(karyoploteR)
library(TxDb.Hsapiens.UCSC.hg38.knownGene)
library(ELMER)
library(rtracklayer)
require(tidyr)
require(ggplot2)
require(wesanderson)
require(stringr)

# ACC threshold = 1.4
{
  #Choosing the threshold - 0.5
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/ACC_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    
    
    cluster4 <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Cluster4.bed',header = F)
    regions_cl4 <- GRanges(seq = cluster4$V1,IRanges(start = cluster4$V2,end = cluster4$V3))
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl4,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    
    peaks_ov <- counts_regions[which(!is.na(peaks_overlaps)),]
    
    
    
    # bigwig plotting
    {
      # Get transcrupts annotation to get HNF4A regions
      tssAnnot <- ELMER::getTSS(genome = "hg38")
      
      # Plot parameters, only to look better
      pp <- getDefaultPlotParams(plot.type = 1)
      pp$leftmargin <- 0.15
      pp$topmargin <- 5
      pp$bottommargin <- 5
      pp$ideogramheight <- 5
      pp$data1inmargin <- 10
      pp$data1outmargin <- 0
      
      # Start to plot bigwig files
      big.wig.files <- dir(path = "~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/ACC bigwigs",
                           pattern = ".bw",
                           all.files = T,
                           full.names = T)
      # Reserve area to plot the bigwig files
      out.at <- autotrack(1:length(big.wig.files), 
                          length(big.wig.files), 
                          margin = 0.3, 
                          r0 = 0,
                          r1 = 1)
      
      setwd('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/ACC bigwigs/plots/th_0.5/')
      threshold = 0.5
      
      for (rgn in 1:length(peaks_ov)){
        cat(rgn,'\n')
        tiff(paste0(peaks_ov[rgn]@seqnames,'__',peaks_ov[rgn]@ranges,'.tiff'),
             height = 10, width = 4, units = 'in', res=300)
        
        # plot will be at  the HNF4A range +- 10Kb
        plt.region <- range(c(peaks_ov[rgn])) + 10000
        
        # Start by plotting gene tracks
        kp <- plotKaryotype(zoom = plt.region,genome = "hg38", cex = 0.5, plot.params = pp)
        genes.data <- makeGenesDataFromTxDb(TxDb.Hsapiens.UCSC.hg38.knownGene,
                                            karyoplot = kp,
                                            plot.transcripts = TRUE, 
                                            plot.transcripts.structure = TRUE)
        genes.data <- addGeneNames(genes.data)
        
        
        
        genes.data <- mergeTranscripts(genes.data)
        
        kp <- plotKaryotype(zoom = plt.region,genome = "hg38", cex = 0.5, plot.params = pp)
        kpAddBaseNumbers(kp, tick.dist = 20000, minor.tick.dist = 5000,
                         add.units = TRUE, cex = 0.5, tick.len = 1)
        #if we would like to put genes under ATAC peaks
        #kpPlotGenes(kp, data = genes.data, r0 = 0, r1 = 0.25, gene.name.cex = 0.5)
        
        
        # Add ATAC-seq label from 0.3 to 1 which should cover all ATAC-seq tracks
        kpAddLabels(kp, 
                    labels = "ATAC-Seq", 
                    r0 = out.at$r0, 
                    r1 = out.at$r1, 
                    side = "left",
                    cex = 0.25,
                    srt = 90, 
                    pos = 3, 
                    label.margin = 0.1)
        
        
        for(i in seq_len(length(big.wig.files))) {
          bigwig.file <- big.wig.files[i]
          
          # just getting the name
          tec_rep_name <- strsplit(strsplit(bigwig.file,split = './')[[1]][9],split = '\\.')[[1]][1]
          cnt <- counts_m[which(!is.na(peaks_overlaps))[rgn],colnames(counts_m) %in% tec_rep_name]
          
          # adaptive threshold by using the score for each region
          # threshold <-counts_m[which(!is.na(peaks_overlaps))[rgn],5]
          
          # Define where the track will be ploted
          # autotrack will simple get the reserved space (from out.at$r0 up to out.at$r1)
          # and split in equal sizes for each bigwifile, i the index, will control which 
          # one is being plotted
          at <- autotrack(i, length(big.wig.files), r0 = out.at$r0, r1 = out.at$r1, margin = 0.2)
          
          
          # Plot bigwig
          kp <- kpPlotBigWig(kp, 
                             data = bigwig.file, 
                             ymax = "visible.region", 
                             r0 = at$r0, 
                             col = ifelse(cnt>threshold,"#0000FF","#FF0000"), 
                             r1 = at$r1)
          computed.ymax <- ceiling(kp$latest.plot$computed.values$ymax)
          
          # Add track axis
          kpAxis(kp, 
                 ymin = 0, 
                 ymax = 150, 
                 numticks = 2,
                 r0 = at$r0, 
                 r1 = at$r1,
                 cex = 0.15)
          
          # Add track label
          kpAddLabels(kp, 
                      labels = ifelse(cnt>threshold,"Peak","No Peak"),
                      r0 = at$r0, 
                      r1 = at$r1, 
                      cex = 0.25, 
                      label.margin = 0.01)
          
          
        }
        
        kpPlotRegions(kp, data=peaks_ov[rgn], col="#FFFCCC50", border="#00000044")
        kpPlotRegions(kp, data=peaks_ov[rgn], col="#FFFCCC50", border="#00000044")
        
        dev.off()
        
      }
      
    }
    
    
    
  }
  
  # Peak intersection for each cluster
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/ACC_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    
    peak_m <- (counts_m[,(6:ncol(counts_m))] > 0.5)*1
    
    
    #Cluster1
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster1.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl1 <- ov_peaks
    
    
    
    #Cluster2
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster2.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl2 <- ov_peaks
    
    
    #Cluster3
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster3.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl3 <- ov_peaks
    
    
    #Cluster4
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl4 <- ov_peaks
    
    
    #Cluster5
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster5.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl5 <- ov_peaks
  }
  
  #plotting
  {
    n_cl1 <-  1157
    n_cl2 <-  741
    n_cl3 <-  919
    n_cl4 <-  476
    n_cl5 <-  424
    
    
    rt_cl1 <- colSums(ov_cl1) / n_cl1
    rt_cl2 <- colSums(ov_cl2) / n_cl2
    rt_cl3 <- colSums(ov_cl3) / n_cl3
    rt_cl4 <- colSums(ov_cl4) / n_cl4
    rt_cl5 <- colSums(ov_cl5) / n_cl5
    
    df <- data.frame(Cluster = c('Cluster 1','Cluster 2','Cluster 3','Cluster 4','Cluster 5'))
    df$Overlaps <- c(mean(rt_cl1),mean(rt_cl2),mean(rt_cl3),mean(rt_cl4),mean(rt_cl5))
    df$Ratio = df$Overlaps/sum(df$Overlaps)
    
    
    my.cols <- brewer.pal(n = 8, name = "Dark2")
    my.cols[4] <- my.cols[7]
    my.cols[5] <- my.cols[8]
    
    setwd('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/ACC bigwigs/plots/')
    tiff("ACC_ov_0.3.tiff",
         height = 5, width = 10, units = 'in', res=300)
    ggplot(data=df, aes(x=Cluster, y=Ratio, fill = Cluster)) +
      geom_bar(stat="identity")+
      theme_minimal() + labs(fill = "Percentages") + 
      theme(legend.title = element_text(size = 18),
            legend.text = element_text(size = 15)) +
      scale_fill_manual(values=my.cols) +
      ylim(c(0,max(df$Ratio + 0.1))) + ggtitle("ACC") + 
      xlab("") + ylab("Ratio  of Overlapped Peaks") +
      geom_text(aes(label=round(Ratio,digits = 3)), position=position_stack(), vjust=-0.5,size = 8) +
      theme(legend.position = "none",axis.text.x = element_text(face="bold", size=14))
    dev.off()
    
  }
  
  # Sample-wise cluster ditribution
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/ACC_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    IDs <- colnames(counts_m)[6:ncol(counts_m)]
    samples <- unique(sapply(IDs, function(x) paste0(strsplit(x,split = '_')[[1]][1:8],collapse = '_') ))
    length(samples)
    peak_m <- (counts_m[,(6:ncol(counts_m))] > 1.4)*1
    colnames(peak_m) <- IDs
    lst_tech_rep <- lapply(samples, function(x) which(str_detect(IDs,x)))
    sample_peaks<- matrix(0,nrow(peak_m),length(lst_tech_rep))
    colnames(sample_peaks) <- samples
    for (i in 1:length(lst_tech_rep)){
      #checking if we have multiple technical replicates
      if(!is.null(dim(counts_m[,lst_tech_rep[[i]]]))){
        sample_peaks[,i] = (rowSums(peak_m[,lst_tech_rep[[i]]]) != 0) * 1
      }else{
        sample_peaks[,i] = peak_m[,lst_tech_rep[[i]]]
      }
    }
    # All analysis use peak_m, I didnot  want to rewrite all
    peak_m <- sample_peaks
    
    
    #Cluster1
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster1.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl1 <- ov_peaks
    
    #Cluster2
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster2.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl2 <- ov_peaks
    
    #Cluster3
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster3.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl3 <- ov_peaks
    
    
    #Cluster4
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl4 <- ov_peaks
    
    #Cluster5
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster5.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    
    ov_cl5 <- ov_peaks
    
    
    ACC_sample_matrix <- matrix(0,length(samples),5)
    
    n_cl1 <-  1157
    n_cl2 <-  741
    n_cl3 <-  919
    n_cl4 <-  476
    n_cl5 <-  424
    
    rt_cl1 <- colSums(ov_cl1) / n_cl1
    rt_cl2 <- colSums(ov_cl2) / n_cl2
    rt_cl3 <- colSums(ov_cl3) / n_cl3
    rt_cl4 <- colSums(ov_cl4) / n_cl4
    rt_cl5 <- colSums(ov_cl5) / n_cl5
    all_rt <- rt_cl1 + rt_cl2 + rt_cl3 + rt_cl4 + rt_cl5
    
    ACC_sample_matrix[,1] <- rt_cl1/all_rt
    ACC_sample_matrix[,2] <- rt_cl2/all_rt
    ACC_sample_matrix[,3] <- rt_cl3/all_rt
    ACC_sample_matrix[,4] <- rt_cl4/all_rt
    ACC_sample_matrix[,5] <- rt_cl5/all_rt
    
    rownames(ACC_sample_matrix) <- samples
  }
  
}


# BLCA threshold = 1.8
{
  #Choosing the threshold 
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/BLCA_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    
    
    cluster4 <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl4 <- GRanges(seq = cluster4$V1,IRanges(start = cluster4$V2,end = cluster4$V3))
    
    #1 bp overlaps
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl4,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    
    peaks_ov <- counts_regions[which(!is.na(peaks_overlaps)),]
    
    
    
    # bigwig plotting
    {
      # Get transcrupts annotation to get HNF4A regions
      tssAnnot <- ELMER::getTSS(genome = "hg38")
      
      # Plot parameters, only to look better
      pp <- getDefaultPlotParams(plot.type = 1)
      pp$leftmargin <- 0.15
      pp$topmargin <- 5
      pp$bottommargin <- 5
      pp$ideogramheight <- 5
      pp$data1inmargin <- 10
      pp$data1outmargin <- 0
      
      # Start to plot bigwig files
      big.wig.files <- dir(path = "~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/BLCA bigwigs",
                           pattern = ".bw",
                           all.files = T,
                           full.names = T)
      # Reserve area to plot the bigwig files
      out.at <- autotrack(1:length(big.wig.files), 
                          length(big.wig.files), 
                          margin = 0.3, 
                          r0 = 0,
                          r1 = 1)
      
      setwd('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/BLCA bigwigs/plots/th_0.6/')
      threshold = 0.6
      
      for (rgn in 1:length(peaks_ov)){
        cat(rgn,'\n')
        tiff(paste0(peaks_ov[rgn]@seqnames,'__',peaks_ov[rgn]@ranges,'.tiff'),
             height = 10, width = 4, units = 'in', res=300)
        
        # plot will be at  the HNF4A range +- 10Kb
        plt.region <- range(c(peaks_ov[rgn])) + 10000
        
        # Start by plotting gene tracks
        kp <- plotKaryotype(zoom = plt.region,genome = "hg38", cex = 0.5, plot.params = pp)
        genes.data <- makeGenesDataFromTxDb(TxDb.Hsapiens.UCSC.hg38.knownGene,
                                            karyoplot = kp,
                                            plot.transcripts = TRUE, 
                                            plot.transcripts.structure = TRUE)
        genes.data <- addGeneNames(genes.data)
        
        
        
        genes.data <- mergeTranscripts(genes.data)
        
        kp <- plotKaryotype(zoom = plt.region,genome = "hg38", cex = 0.5, plot.params = pp)
        kpAddBaseNumbers(kp, tick.dist = 20000, minor.tick.dist = 5000,
                         add.units = TRUE, cex = 0.5, tick.len = 1)
        #if we would like to put genes under ATAC peaks
        #kpPlotGenes(kp, data = genes.data, r0 = 0, r1 = 0.25, gene.name.cex = 0.5)
        
        
        # Add ATAC-seq label from 0.3 to 1 which should cover all ATAC-seq tracks
        kpAddLabels(kp, 
                    labels = "ATAC-Seq", 
                    r0 = out.at$r0, 
                    r1 = out.at$r1, 
                    side = "left",
                    cex = 0.25,
                    srt = 90, 
                    pos = 3, 
                    label.margin = 0.1)
        
        
        for(i in seq_len(length(big.wig.files))) {
          bigwig.file <- big.wig.files[i]
          
          # just getting the name
          tec_rep_name <- strsplit(strsplit(bigwig.file,split = './')[[1]][9],split = '\\.')[[1]][1]
          cnt <- counts_m[which(!is.na(peaks_overlaps))[rgn],colnames(counts_m) %in% tec_rep_name]
          
          # adaptive threshold by using the score for each region
          # threshold <-counts_m[which(!is.na(peaks_overlaps))[rgn],5]
          
          # Define where the track will be ploted
          # autotrack will simple get the reserved space (from out.at$r0 up to out.at$r1)
          # and split in equal sizes for each bigwifile, i the index, will control which 
          # one is being plotted
          at <- autotrack(i, length(big.wig.files), r0 = out.at$r0, r1 = out.at$r1, margin = 0.2)
          
          
          # Plot bigwig
          kp <- kpPlotBigWig(kp, 
                             data = bigwig.file, 
                             ymax = "visible.region", 
                             r0 = at$r0, 
                             col = ifelse(cnt>threshold,"#0000FF","#FF0000"), 
                             r1 = at$r1)
          computed.ymax <- ceiling(kp$latest.plot$computed.values$ymax)
          
          # Add track axis
          kpAxis(kp, 
                 ymin = 0, 
                 ymax = 150, 
                 numticks = 2,
                 r0 = at$r0, 
                 r1 = at$r1,
                 cex = 0.15)
          
          # Add track label
          kpAddLabels(kp, 
                      labels = ifelse(cnt>threshold,"Peak","No Peak"),
                      r0 = at$r0, 
                      r1 = at$r1, 
                      cex = 0.25, 
                      label.margin = 0.01)
          
          
        }
        
        kpPlotRegions(kp, data=peaks_ov[rgn], col="#FFFCCC50", border="#00000044")
        kpPlotRegions(kp, data=peaks_ov[rgn], col="#FFFCCC50", border="#00000044")
        
        dev.off()
        
      }
      
    }
    
    
    
  }
  
  # Peak intersection for each cluster
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/BLCA_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    
    peak_m <- (counts_m[,(6:ncol(counts_m))] > 0.7)*1
    
    
    #Cluster1
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster1.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl1 <- ov_peaks
    
    
    
    #Cluster2
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster2.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl2 <- ov_peaks
    
    
    #Cluster3
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster3.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl3 <- ov_peaks
    
    
    #Cluster4
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl4 <- ov_peaks
    
    
    #Cluster5
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster5.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl5 <- ov_peaks
  }
  
  #plotting
  {
    n_cl1 <-  1157
    n_cl2 <-  741
    n_cl3 <-  919
    n_cl4 <-  476
    n_cl5 <-  424
    
    
    rt_cl1 <- colSums(ov_cl1) / n_cl1
    rt_cl2 <- colSums(ov_cl2) / n_cl2
    rt_cl3 <- colSums(ov_cl3) / n_cl3
    rt_cl4 <- colSums(ov_cl4) / n_cl4
    rt_cl5 <- colSums(ov_cl5) / n_cl5
    
    df <- data.frame(Cluster = c('Cluster 1','Cluster 2','Cluster 3','Cluster 4','Cluster 5'))
    df$Overlaps <- c(mean(rt_cl1),mean(rt_cl2),mean(rt_cl3),mean(rt_cl4),mean(rt_cl5))
    df$Ratio = df$Overlaps/sum(df$Overlaps)
    
    
    my.cols <- brewer.pal(n = 8, name = "Dark2")
    my.cols[4] <- my.cols[7]
    my.cols[5] <- my.cols[8]
    
    setwd('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/BLCA bigwigs/plots/')
    tiff("BLCA_ov_0.7.tiff",
         height = 5, width = 10, units = 'in', res=300)
    ggplot(data=df, aes(x=Cluster, y=Ratio, fill = Cluster)) +
      geom_bar(stat="identity")+
      theme_minimal() + labs(fill = "Percentages") + 
      theme(legend.title = element_text(size = 18),
            legend.text = element_text(size = 15)) +
      scale_fill_manual(values=my.cols) +
      ylim(c(0,max(df$Ratio + 0.1))) + ggtitle("BLCA") + 
      xlab("") + ylab("Ratio  of Overlapped Peaks") +
      geom_text(aes(label=round(Ratio,digits = 3)), position=position_stack(), vjust=-0.5,size = 8) +
      theme(legend.position = "none",axis.text.x = element_text(face="bold", size=14))
    dev.off()
    
  }
  
  # Sample-wise cluster ditribution
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/BLCA_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    IDs <- colnames(counts_m)[6:ncol(counts_m)]
    samples <- unique(sapply(IDs, function(x) paste0(strsplit(x,split = '_')[[1]][1:8],collapse = '_') ))
    length(samples)
    peak_m <- (counts_m[,(6:ncol(counts_m))] > 1.8)*1
    colnames(peak_m) <- IDs
    lst_tech_rep <- lapply(samples, function(x) which(str_detect(IDs,x)))
    sample_peaks<- matrix(0,nrow(peak_m),length(lst_tech_rep))
    colnames(sample_peaks) <- samples
    for (i in 1:length(lst_tech_rep)){
      #checking if we have multiple technical replicates
      if(!is.null(dim(counts_m[,lst_tech_rep[[i]]]))){
        sample_peaks[,i] = (rowSums(peak_m[,lst_tech_rep[[i]]]) != 0) * 1
      }else{
        sample_peaks[,i] = peak_m[,lst_tech_rep[[i]]]
      }
    }
    # All analysis use peak_m, I didnot  want to rewrite all
    peak_m <- sample_peaks
    
    
    #Cluster1
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster1.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl1 <- ov_peaks
    
    #Cluster2
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster2.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl2 <- ov_peaks
    
    #Cluster3
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster3.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl3 <- ov_peaks
    
    
    #Cluster4
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl4 <- ov_peaks
    
    #Cluster5
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster5.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    
    ov_cl5 <- ov_peaks
    
    
    BLCA_sample_matrix <- matrix(0,length(samples),5)
    
    n_cl1 <-  1157
    n_cl2 <-  741
    n_cl3 <-  919
    n_cl4 <-  476
    n_cl5 <-  424
    
    rt_cl1 <- colSums(ov_cl1) / n_cl1
    rt_cl2 <- colSums(ov_cl2) / n_cl2
    rt_cl3 <- colSums(ov_cl3) / n_cl3
    rt_cl4 <- colSums(ov_cl4) / n_cl4
    rt_cl5 <- colSums(ov_cl5) / n_cl5
    all_rt <- rt_cl1 + rt_cl2 + rt_cl3 + rt_cl4 + rt_cl5
    
    BLCA_sample_matrix[,1] <- rt_cl1/all_rt
    BLCA_sample_matrix[,2] <- rt_cl2/all_rt
    BLCA_sample_matrix[,3] <- rt_cl3/all_rt
    BLCA_sample_matrix[,4] <- rt_cl4/all_rt
    BLCA_sample_matrix[,5] <- rt_cl5/all_rt
    
    rownames(BLCA_sample_matrix) <- samples
  }
  
}


# BRCA threshold = 2.2
{
  #Choosing the threshold 
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/BRCA_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    
    
    cluster4 <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Cluster4.bed',header = F)
    regions_cl4 <- GRanges(seq = cluster4$V1,IRanges(start = cluster4$V2,end = cluster4$V3))
    
    #1 bp overlaps
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl4,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    
    peaks_ov <- counts_regions[which(!is.na(peaks_overlaps)),]
    
    
    
    # bigwig plotting
    {
      # Get transcrupts annotation to get HNF4A regions
      tssAnnot <- ELMER::getTSS(genome = "hg38")
      
      # Plot parameters, only to look better
      pp <- getDefaultPlotParams(plot.type = 1)
      pp$leftmargin <- 0.15
      pp$topmargin <- 5
      pp$bottommargin <- 5
      pp$ideogramheight <- 5
      pp$data1inmargin <- 10
      pp$data1outmargin <- 0
      
      # Start to plot bigwig files
      big.wig.files <- dir(path = "~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/BRCA bigwigs",
                           pattern = ".bw",
                           all.files = T,
                           full.names = T)
      # Reserve area to plot the bigwig files
      out.at <- autotrack(1:length(big.wig.files), 
                          length(big.wig.files), 
                          margin = 0.3, 
                          r0 = 0,
                          r1 = 1)
      
      setwd('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/BRCA bigwigs/plots/score_threshold/')
      threshold = 3
      
      for (rgn in 1:length(peaks_ov)){
        tiff(paste0(peaks_ov[rgn]@seqnames,'__',peaks_ov[rgn]@ranges,'.tiff'),
             height = 10, width = 4, units = 'in', res=300)
        
        # plot will be at  the HNF4A range +- 10Kb
        plt.region <- range(c(peaks_ov[rgn])) + 10000
        
        # Start by plotting gene tracks
        kp <- plotKaryotype(zoom = plt.region,genome = "hg38", cex = 0.5, plot.params = pp)
        genes.data <- makeGenesDataFromTxDb(TxDb.Hsapiens.UCSC.hg38.knownGene,
                                            karyoplot = kp,
                                            plot.transcripts = TRUE, 
                                            plot.transcripts.structure = TRUE)
        genes.data <- addGeneNames(genes.data)
        
        
        
        genes.data <- mergeTranscripts(genes.data)
        
        kp <- plotKaryotype(zoom = plt.region,genome = "hg38", cex = 0.5, plot.params = pp)
        kpAddBaseNumbers(kp, tick.dist = 20000, minor.tick.dist = 5000,
                         add.units = TRUE, cex = 0.5, tick.len = 1)
        #if we would like to put genes under ATAC peaks
        #kpPlotGenes(kp, data = genes.data, r0 = 0, r1 = 0.25, gene.name.cex = 0.5)
        
        
        # Add ATAC-seq label from 0.3 to 1 which should cover all ATAC-seq tracks
        kpAddLabels(kp, 
                    labels = "ATAC-Seq", 
                    r0 = out.at$r0, 
                    r1 = out.at$r1, 
                    side = "left",
                    cex = 0.25,
                    srt = 90, 
                    pos = 3, 
                    label.margin = 0.1)
        
        
        for(i in seq_len(length(big.wig.files))) {
          bigwig.file <- big.wig.files[i]
          
          # just getting the name
          tec_rep_name <- strsplit(strsplit(bigwig.file,split = './')[[1]][9],split = '\\.')[[1]][1]
          cnt <- counts_m[which(!is.na(peaks_overlaps))[rgn],colnames(counts_m) %in% tec_rep_name]
          
          # adaptive threshold by using the score for each region
          threshold <-counts_m[which(!is.na(peaks_overlaps))[rgn],5]
          
          # Define where the track will be ploted
          # autotrack will simple get the reserved space (from out.at$r0 up to out.at$r1)
          # and split in equal sizes for each bigwifile, i the index, will control which 
          # one is being plotted
          at <- autotrack(i, length(big.wig.files), r0 = out.at$r0, r1 = out.at$r1, margin = 0.2)
          
          
          # Plot bigwig
          kp <- kpPlotBigWig(kp, 
                             data = bigwig.file, 
                             ymax = "visible.region", 
                             r0 = at$r0, 
                             col = ifelse(cnt>threshold,"#0000FF","#FF0000"), 
                             r1 = at$r1)
          computed.ymax <- ceiling(kp$latest.plot$computed.values$ymax)
          
          # Add track axis
          kpAxis(kp, 
                 ymin = 0, 
                 ymax = 150, 
                 numticks = 2,
                 r0 = at$r0, 
                 r1 = at$r1,
                 cex = 0.15)
          
          # Add track label
          kpAddLabels(kp, 
                      labels = ifelse(cnt>threshold,"Peak","No Peak"),
                      r0 = at$r0, 
                      r1 = at$r1, 
                      cex = 0.25, 
                      label.margin = 0.01)
          
          
        }
        
        kpPlotRegions(kp, data=peaks_ov[rgn], col="#FFFCCC50", border="#00000044")
        kpPlotRegions(kp, data=peaks_ov[rgn], col="#FFFCCC50", border="#00000044")
        
        dev.off()
        
      }
      
    }
    
    
    
  }
  
  # Peak intersection for each cluster
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/BRCA_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    
    peak_m <- (counts_m[,(6:ncol(counts_m))] > 1.2)*1
    
    
    #Cluster1
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster1.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl1 <- ov_peaks
    
    
    
    #Cluster2
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster2.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl2 <- ov_peaks
    
    
    #Cluster3
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster3.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl3 <- ov_peaks
    
    
    #Cluster4
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl4 <- ov_peaks
    
    
    #Cluster5
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster5.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl5 <- ov_peaks
  }
  
  #plotting
  {
    n_cl1 <-  1157
    n_cl2 <-  741
    n_cl3 <-  919
    n_cl4 <-  476
    n_cl5 <-  424
    
    
    rt_cl1 <- colSums(ov_cl1) / n_cl1
    rt_cl2 <- colSums(ov_cl2) / n_cl2
    rt_cl3 <- colSums(ov_cl3) / n_cl3
    rt_cl4 <- colSums(ov_cl4) / n_cl4
    rt_cl5 <- colSums(ov_cl5) / n_cl5
    
    df <- data.frame(Cluster = c('Cluster 1','Cluster 2','Cluster 3','Cluster 4','Cluster 5'))
    df$Overlaps <- c(mean(rt_cl1),mean(rt_cl2),mean(rt_cl3),mean(rt_cl4),mean(rt_cl5))
    df$Ratio = df$Overlaps/sum(df$Overlaps)
    
    
    my.cols <- brewer.pal(n = 8, name = "Dark2")
    my.cols[4] <- my.cols[7]
    my.cols[5] <- my.cols[8]
    
    setwd('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/BRCA bigwigs/plots/')
    tiff("BRCA_ov_1.2.tiff",
         height = 5, width = 10, units = 'in', res=300)
    ggplot(data=df, aes(x=Cluster, y=Ratio, fill = Cluster)) +
      geom_bar(stat="identity")+
      theme_minimal() + labs(fill = "Percentages") + 
      theme(legend.title = element_text(size = 18),
            legend.text = element_text(size = 15)) +
      scale_fill_manual(values=my.cols) +
      ylim(c(0,max(df$Ratio + 0.1))) + ggtitle("BRCA") + 
      xlab("") + ylab("Ratio  of Overlapped Peaks") +
      geom_text(aes(label=round(Ratio,digits = 3)), position=position_stack(), vjust=-0.5,size = 8) +
      theme(legend.position = "none",axis.text.x = element_text(face="bold", size=14))
    dev.off()
    
  }
  
  # Sample-wise cluster ditribution
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/BRCA_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    IDs <- colnames(counts_m)[6:ncol(counts_m)]
    samples <- unique(sapply(IDs, function(x) paste0(strsplit(x,split = '_')[[1]][1:8],collapse = '_') ))
    length(samples)
    peak_m <- (counts_m[,(6:ncol(counts_m))] > 2.2)*1
    colnames(peak_m) <- IDs
    lst_tech_rep <- lapply(samples, function(x) which(str_detect(IDs,x)))
    sample_peaks<- matrix(0,nrow(peak_m),length(lst_tech_rep))
    colnames(sample_peaks) <- samples
    for (i in 1:length(lst_tech_rep)){
      #checking if we have multiple technical replicates
      if(!is.null(dim(counts_m[,lst_tech_rep[[i]]]))){
        sample_peaks[,i] = (rowSums(peak_m[,lst_tech_rep[[i]]]) != 0) * 1
      }else{
        sample_peaks[,i] = peak_m[,lst_tech_rep[[i]]]
      }
    }
    # All analysis use peak_m, I didnot  want to rewrite all
    peak_m <- sample_peaks
    
    
    #Cluster1
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster1.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl1 <- ov_peaks
    
    #Cluster2
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster2.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl2 <- ov_peaks
    
    #Cluster3
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster3.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl3 <- ov_peaks
    
    
    #Cluster4
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl4 <- ov_peaks
    
    #Cluster5
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster5.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    
    ov_cl5 <- ov_peaks
    
    
    BRCA_sample_matrix <- matrix(0,length(samples),5)
    
    n_cl1 <-  1157
    n_cl2 <-  741
    n_cl3 <-  919
    n_cl4 <-  476
    n_cl5 <-  424
    
    rt_cl1 <- colSums(ov_cl1) / n_cl1
    rt_cl2 <- colSums(ov_cl2) / n_cl2
    rt_cl3 <- colSums(ov_cl3) / n_cl3
    rt_cl4 <- colSums(ov_cl4) / n_cl4
    rt_cl5 <- colSums(ov_cl5) / n_cl5
    all_rt <- rt_cl1 + rt_cl2 + rt_cl3 + rt_cl4 + rt_cl5
    
    BRCA_sample_matrix[,1] <- rt_cl1/all_rt
    BRCA_sample_matrix[,2] <- rt_cl2/all_rt
    BRCA_sample_matrix[,3] <- rt_cl3/all_rt
    BRCA_sample_matrix[,4] <- rt_cl4/all_rt
    BRCA_sample_matrix[,5] <- rt_cl5/all_rt
    
    rownames(BRCA_sample_matrix) <- samples
  }
}


# CESC threshold = 1.8
{
  #Choosing the threshold 
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/CESC_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    
    
    cluster4 <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl4 <- GRanges(seq = cluster4$V1,IRanges(start = cluster4$V2,end = cluster4$V3))
    
    #1 bp overlaps
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl4,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    
    peaks_ov <- counts_regions[which(!is.na(peaks_overlaps)),]
    
    
    
    # bigwig plotting
    {
      # Get transcrupts annotation to get HNF4A regions
      tssAnnot <- ELMER::getTSS(genome = "hg38")
      
      # Plot parameters, only to look better
      pp <- getDefaultPlotParams(plot.type = 1)
      pp$leftmargin <- 0.15
      pp$topmargin <- 5
      pp$bottommargin <- 5
      pp$ideogramheight <- 5
      pp$data1inmargin <- 10
      pp$data1outmargin <- 0
      
      # Start to plot bigwig files
      big.wig.files <- dir(path = "~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/CESC bigwigs",
                           pattern = ".bw",
                           all.files = T,
                           full.names = T)
      # Reserve area to plot the bigwig files
      out.at <- autotrack(1:length(big.wig.files), 
                          length(big.wig.files), 
                          margin = 0.3, 
                          r0 = 0,
                          r1 = 1)
      
      setwd('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/CESC bigwigs/plots/th_2.2/')
      threshold = 2.2
      
      for (rgn in 1:length(peaks_ov)){
        tiff(paste0(peaks_ov[rgn]@seqnames,'__',peaks_ov[rgn]@ranges,'.tiff'),
             height = 10, width = 4, units = 'in', res=300)
        
        # plot will be at  the HNF4A range +- 10Kb
        plt.region <- range(c(peaks_ov[rgn])) + 10000
        
        # Start by plotting gene tracks
        kp <- plotKaryotype(zoom = plt.region,genome = "hg38", cex = 0.5, plot.params = pp)
        genes.data <- makeGenesDataFromTxDb(TxDb.Hsapiens.UCSC.hg38.knownGene,
                                            karyoplot = kp,
                                            plot.transcripts = TRUE, 
                                            plot.transcripts.structure = TRUE)
        genes.data <- addGeneNames(genes.data)
        
        
        
        genes.data <- mergeTranscripts(genes.data)
        
        kp <- plotKaryotype(zoom = plt.region,genome = "hg38", cex = 0.5, plot.params = pp)
        kpAddBaseNumbers(kp, tick.dist = 20000, minor.tick.dist = 5000,
                         add.units = TRUE, cex = 0.5, tick.len = 1)
        #if we would like to put genes under ATAC peaks
        #kpPlotGenes(kp, data = genes.data, r0 = 0, r1 = 0.25, gene.name.cex = 0.5)
        
        
        # Add ATAC-seq label from 0.3 to 1 which should cover all ATAC-seq tracks
        kpAddLabels(kp, 
                    labels = "ATAC-Seq", 
                    r0 = out.at$r0, 
                    r1 = out.at$r1, 
                    side = "left",
                    cex = 0.25,
                    srt = 90, 
                    pos = 3, 
                    label.margin = 0.1)
        
        
        for(i in seq_len(length(big.wig.files))) {
          bigwig.file <- big.wig.files[i]
          
          # just getting the name
          tec_rep_name <- strsplit(strsplit(bigwig.file,split = './')[[1]][9],split = '\\.')[[1]][1]
          cnt <- counts_m[which(!is.na(peaks_overlaps))[rgn],colnames(counts_m) %in% tec_rep_name]
          
          # adaptive threshold by using the score for each region
          #threshold <-counts_m[which(!is.na(peaks_overlaps))[rgn],5]
          
          # Define where the track will be ploted
          # autotrack will simple get the reserved space (from out.at$r0 up to out.at$r1)
          # and split in equal sizes for each bigwifile, i the index, will control which 
          # one is being plotted
          at <- autotrack(i, length(big.wig.files), r0 = out.at$r0, r1 = out.at$r1, margin = 0.2)
          
          
          # Plot bigwig
          kp <- kpPlotBigWig(kp, 
                             data = bigwig.file, 
                             ymax = "visible.region", 
                             r0 = at$r0, 
                             col = ifelse(cnt>threshold,"#0000FF","#FF0000"), 
                             r1 = at$r1)
          computed.ymax <- ceiling(kp$latest.plot$computed.values$ymax)
          
          # Add track axis
          kpAxis(kp, 
                 ymin = 0, 
                 ymax = 150, 
                 numticks = 2,
                 r0 = at$r0, 
                 r1 = at$r1,
                 cex = 0.15)
          
          # Add track label
          kpAddLabels(kp, 
                      labels = ifelse(cnt>threshold,"Peak","No Peak"),
                      r0 = at$r0, 
                      r1 = at$r1, 
                      cex = 0.25, 
                      label.margin = 0.01)
          
          
        }
        
        kpPlotRegions(kp, data=peaks_ov[rgn], col="#FFFCCC50", border="#00000044")
        kpPlotRegions(kp, data=peaks_ov[rgn], col="#FFFCCC50", border="#00000044")
        
        dev.off()
        
      }
      
    }
    
    
    
  }
  
  # Peak intersection for each cluster
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/CESC_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    
    peak_m <- (counts_m[,(6:ncol(counts_m))] > 0.7)*1
    
    
    #Cluster1
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster1.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl1 <- ov_peaks
    
    
    
    #Cluster2
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster2.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl2 <- ov_peaks
    
    
    #Cluster3
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster3.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl3 <- ov_peaks
    
    
    #Cluster4
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl4 <- ov_peaks
    
    
    #Cluster5
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster5.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl5 <- ov_peaks
  }
  
  #plotting
  {
    n_cl1 <-  1157
    n_cl2 <-  741
    n_cl3 <-  919
    n_cl4 <-  476
    n_cl5 <-  424
    
    
    rt_cl1 <- colSums(ov_cl1) / n_cl1
    rt_cl2 <- colSums(ov_cl2) / n_cl2
    rt_cl3 <- colSums(ov_cl3) / n_cl3
    rt_cl4 <- colSums(ov_cl4) / n_cl4
    rt_cl5 <- colSums(ov_cl5) / n_cl5
    
    df <- data.frame(Cluster = c('Cluster 1','Cluster 2','Cluster 3','Cluster 4','Cluster 5'))
    df$Overlaps <- c(mean(rt_cl1),mean(rt_cl2),mean(rt_cl3),mean(rt_cl4),mean(rt_cl5))
    df$Ratio = df$Overlaps/sum(df$Overlaps)
    
    
    my.cols <- brewer.pal(n = 8, name = "Dark2")
    my.cols[4] <- my.cols[7]
    my.cols[5] <- my.cols[8]
    
    setwd('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/CESC bigwigs/plots/')
    tiff("CESC_ov_1.2.tiff",
         height = 5, width = 10, units = 'in', res=300)
    ggplot(data=df, aes(x=Cluster, y=Ratio, fill = Cluster)) +
      geom_bar(stat="identity")+
      theme_minimal() + labs(fill = "Percentages") + 
      theme(legend.title = element_text(size = 18),
            legend.text = element_text(size = 15)) +
      scale_fill_manual(values=my.cols) +
      ylim(c(0,max(df$Ratio + 0.1))) + ggtitle("CESC") + 
      xlab("") + ylab("Ratio of Overlapped Peaks") +
      geom_text(aes(label=round(Ratio,digits = 3)), position=position_stack(), vjust=-0.5,size = 8) +
      theme(legend.position = "none",axis.text.x = element_text(face="bold", size=14))
    dev.off()
    
  }
  
  # Sample-wise cluster ditribution
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/CESC_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    IDs <- colnames(counts_m)[6:ncol(counts_m)]
    samples <- unique(sapply(IDs, function(x) paste0(strsplit(x,split = '_')[[1]][1:8],collapse = '_') ))
    length(samples)
    peak_m <- (counts_m[,(6:ncol(counts_m))] > 1.8)*1
    colnames(peak_m) <- IDs
    lst_tech_rep <- lapply(samples, function(x) which(str_detect(IDs,x)))
    sample_peaks<- matrix(0,nrow(peak_m),length(lst_tech_rep))
    colnames(sample_peaks) <- samples
    for (i in 1:length(lst_tech_rep)){
      #checking if we have multiple technical replicates
      if(!is.null(dim(counts_m[,lst_tech_rep[[i]]]))){
        sample_peaks[,i] = (rowSums(peak_m[,lst_tech_rep[[i]]]) != 0) * 1
      }else{
        sample_peaks[,i] = peak_m[,lst_tech_rep[[i]]]
      }
    }
    # All analysis use peak_m, I didnot  want to rewrite all
    peak_m <- sample_peaks
    
    
    #Cluster1
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster1.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl1 <- ov_peaks
    
    #Cluster2
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster2.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl2 <- ov_peaks
    
    #Cluster3
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster3.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl3 <- ov_peaks
    
    
    #Cluster4
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl4 <- ov_peaks
    
    #Cluster5
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster5.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    
    ov_cl5 <- ov_peaks
    
    
    CESC_sample_matrix <- matrix(0,length(samples),5)
    
    n_cl1 <-  1157
    n_cl2 <-  741
    n_cl3 <-  919
    n_cl4 <-  476
    n_cl5 <-  424
    
    rt_cl1 <- colSums(ov_cl1) / n_cl1
    rt_cl2 <- colSums(ov_cl2) / n_cl2
    rt_cl3 <- colSums(ov_cl3) / n_cl3
    rt_cl4 <- colSums(ov_cl4) / n_cl4
    rt_cl5 <- colSums(ov_cl5) / n_cl5
    all_rt <- rt_cl1 + rt_cl2 + rt_cl3 + rt_cl4 + rt_cl5
    
    CESC_sample_matrix[,1] <- rt_cl1/all_rt
    CESC_sample_matrix[,2] <- rt_cl2/all_rt
    CESC_sample_matrix[,3] <- rt_cl3/all_rt
    CESC_sample_matrix[,4] <- rt_cl4/all_rt
    CESC_sample_matrix[,5] <- rt_cl5/all_rt
    
    rownames(CESC_sample_matrix) <- samples
  }
}


# CHOL - threshold =1.5
{
  #Choosing the threshold 
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/CHOL_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    
    
    cluster4 <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl4 <- GRanges(seq = cluster4$V1,IRanges(start = cluster4$V2,end = cluster4$V3))
    
    #1 bp overlaps
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl4,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    
    peaks_ov <- counts_regions[which(!is.na(peaks_overlaps)),]
    
    
    
    # bigwig plotting
    {
      # Get transcrupts annotation to get HNF4A regions
      tssAnnot <- ELMER::getTSS(genome = "hg38")
      
      # Plot parameters, only to look better
      pp <- getDefaultPlotParams(plot.type = 1)
      pp$leftmargin <- 0.15
      pp$topmargin <- 5
      pp$bottommargin <- 5
      pp$ideogramheight <- 5
      pp$data1inmargin <- 10
      pp$data1outmargin <- 0
      
      # Start to plot bigwig files
      big.wig.files <- dir(path = "~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/CHOL bigwigs",
                           pattern = ".bw",
                           all.files = T,
                           full.names = T)
      # Reserve area to plot the bigwig files
      out.at <- autotrack(1:length(big.wig.files), 
                          length(big.wig.files), 
                          margin = 0.3, 
                          r0 = 0,
                          r1 = 1)
      
      setwd('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/CHOL bigwigs/plots/th_3/')
      threshold = 3
      
      for (rgn in 1:length(peaks_ov)){
        tiff(paste0(peaks_ov[rgn]@seqnames,'__',peaks_ov[rgn]@ranges,'.tiff'),
             height = 10, width = 4, units = 'in', res=300)
        
        # plot will be at  the HNF4A range +- 10Kb
        plt.region <- range(c(peaks_ov[rgn])) + 10000
        
        # Start by plotting gene tracks
        kp <- plotKaryotype(zoom = plt.region,genome = "hg38", cex = 0.5, plot.params = pp)
        genes.data <- makeGenesDataFromTxDb(TxDb.Hsapiens.UCSC.hg38.knownGene,
                                            karyoplot = kp,
                                            plot.transcripts = TRUE, 
                                            plot.transcripts.structure = TRUE)
        genes.data <- addGeneNames(genes.data)
        
        
        
        genes.data <- mergeTranscripts(genes.data)
        
        kp <- plotKaryotype(zoom = plt.region,genome = "hg38", cex = 0.5, plot.params = pp)
        kpAddBaseNumbers(kp, tick.dist = 20000, minor.tick.dist = 5000,
                         add.units = TRUE, cex = 0.5, tick.len = 1)
        #if we would like to put genes under ATAC peaks
        #kpPlotGenes(kp, data = genes.data, r0 = 0, r1 = 0.25, gene.name.cex = 0.5)
        
        
        # Add ATAC-seq label from 0.3 to 1 which should cover all ATAC-seq tracks
        kpAddLabels(kp, 
                    labels = "ATAC-Seq", 
                    r0 = out.at$r0, 
                    r1 = out.at$r1, 
                    side = "left",
                    cex = 0.25,
                    srt = 90, 
                    pos = 3, 
                    label.margin = 0.1)
        
        
        for(i in seq_len(length(big.wig.files))) {
          bigwig.file <- big.wig.files[i]
          
          # just getting the name
          tec_rep_name <- strsplit(strsplit(bigwig.file,split = './')[[1]][9],split = '\\.')[[1]][1]
          cnt <- counts_m[which(!is.na(peaks_overlaps))[rgn],colnames(counts_m) %in% tec_rep_name]
          
          # adaptive threshold by using the score for each region
          # threshold <-counts_m[which(!is.na(peaks_overlaps))[rgn],5]
          
          # Define where the track will be ploted
          # autotrack will simple get the reserved space (from out.at$r0 up to out.at$r1)
          # and split in equal sizes for each bigwifile, i the index, will control which 
          # one is being plotted
          at <- autotrack(i, length(big.wig.files), r0 = out.at$r0, r1 = out.at$r1, margin = 0.2)
          
          
          # Plot bigwig
          kp <- kpPlotBigWig(kp, 
                             data = bigwig.file, 
                             ymax = "visible.region", 
                             r0 = at$r0, 
                             col = ifelse(cnt>threshold,"#0000FF","#FF0000"), 
                             r1 = at$r1)
          computed.ymax <- ceiling(kp$latest.plot$computed.values$ymax)
          
          # Add track axis
          kpAxis(kp, 
                 ymin = 0, 
                 ymax = 150, 
                 numticks = 2,
                 r0 = at$r0, 
                 r1 = at$r1,
                 cex = 0.15)
          
          # Add track label
          kpAddLabels(kp, 
                      labels = ifelse(cnt>threshold,"Peak","No Peak"),
                      r0 = at$r0, 
                      r1 = at$r1, 
                      cex = 0.25, 
                      label.margin = 0.01)
          
          
        }
        
        kpPlotRegions(kp, data=peaks_ov[rgn], col="#FFFCCC50", border="#00000044")
        kpPlotRegions(kp, data=peaks_ov[rgn], col="#FFFCCC50", border="#00000044")
        
        dev.off()
        
      }
      
    }
    
    
    
  }
  
  # Peak intersection for each cluster
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/CHOL_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    
    peak_m <- (counts_m[,(6:ncol(counts_m))] > 0.7)*1
    
    
    #Cluster1
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster1.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl1 <- ov_peaks
    
    
    
    #Cluster2
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster2.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl2 <- ov_peaks
    
    
    #Cluster3
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster3.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl3 <- ov_peaks
    
    
    #Cluster4
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl4 <- ov_peaks
    
    
    #Cluster5
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster5.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl5 <- ov_peaks
  }
  
  #plotting
  {
    n_cl1 <-  1157
    n_cl2 <-  741
    n_cl3 <-  919
    n_cl4 <-  476
    n_cl5 <-  424
    
    
    rt_cl1 <- colSums(ov_cl1) / n_cl1
    rt_cl2 <- colSums(ov_cl2) / n_cl2
    rt_cl3 <- colSums(ov_cl3) / n_cl3
    rt_cl4 <- colSums(ov_cl4) / n_cl4
    rt_cl5 <- colSums(ov_cl5) / n_cl5
    
    df <- data.frame(Cluster = c('Cluster 1','Cluster 2','Cluster 3','Cluster 4','Cluster 5'))
    df$Overlaps <- c(mean(rt_cl1),mean(rt_cl2),mean(rt_cl3),mean(rt_cl4),mean(rt_cl5))
    df$Ratio = df$Overlaps/sum(df$Overlaps)
    
    
    my.cols <- brewer.pal(n = 8, name = "Dark2")
    my.cols[4] <- my.cols[7]
    my.cols[5] <- my.cols[8]
    
    setwd('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/CHOL bigwigs/plots/')
    tiff("CHOL_ov_1.2.tiff",
         height = 5, width = 10, units = 'in', res=300)
    ggplot(data=df, aes(x=Cluster, y=Ratio, fill = Cluster)) +
      geom_bar(stat="identity")+
      theme_minimal() + labs(fill = "Percentages") + 
      theme(legend.title = element_text(size = 18),
            legend.text = element_text(size = 15)) +
      scale_fill_manual(values=my.cols) +
      ylim(c(0,max(df$Ratio + 0.1))) + ggtitle("CHOL") + 
      xlab("") + ylab("Ratio of Overlapped Peaks") +
      geom_text(aes(label=round(Ratio,digits = 3)), position=position_stack(), vjust=-0.5,size = 8) +
      theme(legend.position = "none",axis.text.x = element_text(face="bold", size=14))
    dev.off()
    
  }
  
  # Sample-wise cluster ditribution
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/CHOL_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    IDs <- colnames(counts_m)[6:ncol(counts_m)]
    samples <- unique(sapply(IDs, function(x) paste0(strsplit(x,split = '_')[[1]][1:8],collapse = '_') ))
    length(samples)
    peak_m <- (counts_m[,(6:ncol(counts_m))] > 1.5)*1
    colnames(peak_m) <- IDs
    lst_tech_rep <- lapply(samples, function(x) which(str_detect(IDs,x)))
    sample_peaks<- matrix(0,nrow(peak_m),length(lst_tech_rep))
    colnames(sample_peaks) <- samples
    for (i in 1:length(lst_tech_rep)){
      #checking if we have multiple technical replicates
      if(!is.null(dim(counts_m[,lst_tech_rep[[i]]]))){
        sample_peaks[,i] = (rowSums(peak_m[,lst_tech_rep[[i]]]) != 0) * 1
      }else{
        sample_peaks[,i] = peak_m[,lst_tech_rep[[i]]]
      }
    }
    # All analysis use peak_m, I didnot  want to rewrite all
    peak_m <- sample_peaks
    
    
    #Cluster1
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster1.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl1 <- ov_peaks
    
    #Cluster2
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster2.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl2 <- ov_peaks
    
    #Cluster3
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster3.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl3 <- ov_peaks
    
    
    #Cluster4
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl4 <- ov_peaks
    
    #Cluster5
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster5.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    
    ov_cl5 <- ov_peaks
    
    
    CHOL_sample_matrix <- matrix(0,length(samples),5)
    
    n_cl1 <-  1157
    n_cl2 <-  741
    n_cl3 <-  919
    n_cl4 <-  476
    n_cl5 <-  424
    
    rt_cl1 <- colSums(ov_cl1) / n_cl1
    rt_cl2 <- colSums(ov_cl2) / n_cl2
    rt_cl3 <- colSums(ov_cl3) / n_cl3
    rt_cl4 <- colSums(ov_cl4) / n_cl4
    rt_cl5 <- colSums(ov_cl5) / n_cl5
    all_rt <- rt_cl1 + rt_cl2 + rt_cl3 + rt_cl4 + rt_cl5
    
    CHOL_sample_matrix[,1] <- rt_cl1/all_rt
    CHOL_sample_matrix[,2] <- rt_cl2/all_rt
    CHOL_sample_matrix[,3] <- rt_cl3/all_rt
    CHOL_sample_matrix[,4] <- rt_cl4/all_rt
    CHOL_sample_matrix[,5] <- rt_cl5/all_rt
    
    rownames(CHOL_sample_matrix) <- samples
  }
}


# COAD - threshold = 0.75 / 1.75
{
  #Choosing the threshold 
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/COAD_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    
    
    cluster4 <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl4 <- GRanges(seq = cluster4$V1,IRanges(start = cluster4$V2,end = cluster4$V3))
    
    #1 bp overlaps
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl4,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    
    peaks_ov <- counts_regions[which(!is.na(peaks_overlaps)),]
    
    
    
    # bigwig plotting
    {
      # Get transcrupts annotation to get HNF4A regions
      tssAnnot <- ELMER::getTSS(genome = "hg38")
      
      # Plot parameters, only to look better
      pp <- getDefaultPlotParams(plot.type = 1)
      pp$leftmargin <- 0.15
      pp$topmargin <- 5
      pp$bottommargin <- 5
      pp$ideogramheight <- 5
      pp$data1inmargin <- 10
      pp$data1outmargin <- 0
      
      # Start to plot bigwig files
      big.wig.files <- dir(path = "~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/COAD bigwigs",
                           pattern = ".bw",
                           all.files = T,
                           full.names = T)
      # Reserve area to plot the bigwig files
      out.at <- autotrack(1:length(big.wig.files), 
                          length(big.wig.files), 
                          margin = 0.3, 
                          r0 = 0,
                          r1 = 1)
      
      setwd('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/COAD bigwigs/plots/th_2.5/')
      threshold = 2.5
      
      for (rgn in 1:length(peaks_ov)){
        cat(rgn,'\n')
        tiff(paste0(peaks_ov[rgn]@seqnames,'__',peaks_ov[rgn]@ranges,'.tiff'),
             height = 10, width = 4, units = 'in', res=300)
        
        # plot will be at  the HNF4A range +- 10Kb
        plt.region <- range(c(peaks_ov[rgn])) + 10000
        
        # Start by plotting gene tracks
        kp <- plotKaryotype(zoom = plt.region,genome = "hg38", cex = 0.5, plot.params = pp)
        genes.data <- makeGenesDataFromTxDb(TxDb.Hsapiens.UCSC.hg38.knownGene,
                                            karyoplot = kp,
                                            plot.transcripts = TRUE, 
                                            plot.transcripts.structure = TRUE)
        genes.data <- addGeneNames(genes.data)
        
        
        
        genes.data <- mergeTranscripts(genes.data)
        
        kp <- plotKaryotype(zoom = plt.region,genome = "hg38", cex = 0.5, plot.params = pp)
        kpAddBaseNumbers(kp, tick.dist = 20000, minor.tick.dist = 5000,
                         add.units = TRUE, cex = 0.5, tick.len = 1)
        #if we would like to put genes under ATAC peaks
        #kpPlotGenes(kp, data = genes.data, r0 = 0, r1 = 0.25, gene.name.cex = 0.5)
        
        
        # Add ATAC-seq label from 0.3 to 1 which should cover all ATAC-seq tracks
        kpAddLabels(kp, 
                    labels = "ATAC-Seq", 
                    r0 = out.at$r0, 
                    r1 = out.at$r1, 
                    side = "left",
                    cex = 0.25,
                    srt = 90, 
                    pos = 3, 
                    label.margin = 0.1)
        
        
        for(i in seq_len(length(big.wig.files))) {
          bigwig.file <- big.wig.files[i]
          
          # just getting the name
          tec_rep_name <- strsplit(strsplit(bigwig.file,split = './')[[1]][9],split = '\\.')[[1]][1]
          cnt <- counts_m[which(!is.na(peaks_overlaps))[rgn],colnames(counts_m) %in% tec_rep_name]
          
          # adaptive threshold by using the score for each region
          # threshold <-counts_m[which(!is.na(peaks_overlaps))[rgn],5]
          
          # Define where the track will be ploted
          # autotrack will simple get the reserved space (from out.at$r0 up to out.at$r1)
          # and split in equal sizes for each bigwifile, i the index, will control which 
          # one is being plotted
          at <- autotrack(i, length(big.wig.files), r0 = out.at$r0, r1 = out.at$r1, margin = 0.2)
          
          
          # Plot bigwig
          kp <- kpPlotBigWig(kp, 
                             data = bigwig.file, 
                             ymax = "visible.region", 
                             r0 = at$r0, 
                             col = ifelse(cnt>threshold,"#0000FF","#FF0000"), 
                             r1 = at$r1)
          computed.ymax <- ceiling(kp$latest.plot$computed.values$ymax)
          
          # Add track axis
          kpAxis(kp, 
                 ymin = 0, 
                 ymax = 150, 
                 numticks = 2,
                 r0 = at$r0, 
                 r1 = at$r1,
                 cex = 0.15)
          
          # Add track label
          kpAddLabels(kp, 
                      labels = ifelse(cnt>threshold,"Peak","No Peak"),
                      r0 = at$r0, 
                      r1 = at$r1, 
                      cex = 0.25, 
                      label.margin = 0.01)
          
          
        }
        
        kpPlotRegions(kp, data=peaks_ov[rgn], col="#FFFCCC50", border="#00000044")
        kpPlotRegions(kp, data=peaks_ov[rgn], col="#FFFCCC50", border="#00000044")
        
        dev.off()
        
      }
      
    }
    
    
    
  }
  
  # Peak intersection for each cluster
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/COAD_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    
    peak_m <- (counts_m[,(6:ncol(counts_m))] > 0.7)*1
    
    
    #Cluster1
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster1.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl1 <- ov_peaks
    
    
    
    #Cluster2
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster2.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl2 <- ov_peaks
    
    
    #Cluster3
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster3.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl3 <- ov_peaks
    
    
    #Cluster4
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl4 <- ov_peaks
    
    
    #Cluster5
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster5.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl5 <- ov_peaks
  }
  
  #plotting
  {
    n_cl1 <-  1157
    n_cl2 <-  741
    n_cl3 <-  919
    n_cl4 <-  476
    n_cl5 <-  424
    
    
    rt_cl1 <- colSums(ov_cl1) / n_cl1
    rt_cl2 <- colSums(ov_cl2) / n_cl2
    rt_cl3 <- colSums(ov_cl3) / n_cl3
    rt_cl4 <- colSums(ov_cl4) / n_cl4
    rt_cl5 <- colSums(ov_cl5) / n_cl5
    
    df <- data.frame(Cluster = c('Cluster 1','Cluster 2','Cluster 3','Cluster 4','Cluster 5'))
    df$Overlaps <- c(mean(rt_cl1),mean(rt_cl2),mean(rt_cl3),mean(rt_cl4),mean(rt_cl5))
    df$Ratio = df$Overlaps/sum(df$Overlaps)
    
    
    my.cols <- brewer.pal(n = 8, name = "Dark2")
    my.cols[4] <- my.cols[7]
    my.cols[5] <- my.cols[8]
    
    setwd('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/COAD bigwigs/plots/')
    tiff("COAD_ov_1.2.tiff",
         height = 5, width = 10, units = 'in', res=300)
    ggplot(data=df, aes(x=Cluster, y=Ratio, fill = Cluster)) +
      geom_bar(stat="identity")+
      theme_minimal() + labs(fill = "Percentages") + 
      theme(legend.title = element_text(size = 18),
            legend.text = element_text(size = 15)) +
      scale_fill_manual(values=my.cols) +
      ylim(c(0,max(df$Ratio + 0.1))) + ggtitle("COAD") + 
      xlab("") + ylab("Ratio of Overlapped Peaks") +
      geom_text(aes(label=round(Ratio,digits = 3)), position=position_stack(), vjust=-0.5,size = 8) +
      theme(legend.position = "none",axis.text.x = element_text(face="bold", size=14))
    dev.off()
    
  }
  
  # Sample-wise cluster ditribution
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/COAD_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    IDs <- colnames(counts_m)[6:ncol(counts_m)]
    samples <- unique(sapply(IDs, function(x) paste0(strsplit(x,split = '_')[[1]][1:8],collapse = '_') ))
    length(samples)
    peak_m <- (counts_m[,(6:ncol(counts_m))] > 1.75)*1
    colnames(peak_m) <- IDs
    lst_tech_rep <- lapply(samples, function(x) which(str_detect(IDs,x)))
    sample_peaks<- matrix(0,nrow(peak_m),length(lst_tech_rep))
    colnames(sample_peaks) <- samples
    for (i in 1:length(lst_tech_rep)){
      #checking if we have multiple technical replicates
      if(!is.null(dim(counts_m[,lst_tech_rep[[i]]]))){
        sample_peaks[,i] = (rowSums(peak_m[,lst_tech_rep[[i]]]) != 0) * 1
      }else{
        sample_peaks[,i] = peak_m[,lst_tech_rep[[i]]]
      }
    }
    # All analysis use peak_m, I didnot  want to rewrite all
    peak_m <- sample_peaks
    
    
    #Cluster1
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster1.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl1 <- ov_peaks
    
    #Cluster2
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster2.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl2 <- ov_peaks
    
    #Cluster3
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster3.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl3 <- ov_peaks
    
    
    #Cluster4
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl4 <- ov_peaks
    
    #Cluster5
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster5.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    
    ov_cl5 <- ov_peaks
    
    
    COAD_sample_matrix <- matrix(0,length(samples),5)
    
    n_cl1 <-  1157
    n_cl2 <-  741
    n_cl3 <-  919
    n_cl4 <-  476
    n_cl5 <-  424
    
    rt_cl1 <- colSums(ov_cl1) / n_cl1
    rt_cl2 <- colSums(ov_cl2) / n_cl2
    rt_cl3 <- colSums(ov_cl3) / n_cl3
    rt_cl4 <- colSums(ov_cl4) / n_cl4
    rt_cl5 <- colSums(ov_cl5) / n_cl5
    all_rt <- rt_cl1 + rt_cl2 + rt_cl3 + rt_cl4 + rt_cl5
    
    COAD_sample_matrix[,1] <- rt_cl1/all_rt
    COAD_sample_matrix[,2] <- rt_cl2/all_rt
    COAD_sample_matrix[,3] <- rt_cl3/all_rt
    COAD_sample_matrix[,4] <- rt_cl4/all_rt
    COAD_sample_matrix[,5] <- rt_cl5/all_rt
    
    rownames(COAD_sample_matrix) <- samples
  }
  
}


# ESCA - threshold = 1.4
{
  #Choosing the threshold 
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/ESCA_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    
    
    cluster4 <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl4 <- GRanges(seq = cluster4$V1,IRanges(start = cluster4$V2,end = cluster4$V3))
    
    #1 bp overlaps
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl4,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    
    peaks_ov <- counts_regions[which(!is.na(peaks_overlaps)),]
    
    
    
    # bigwig plotting
    {
      # Get transcrupts annotation to get HNF4A regions
      tssAnnot <- ELMER::getTSS(genome = "hg38")
      
      # Plot parameters, only to look better
      pp <- getDefaultPlotParams(plot.type = 1)
      pp$leftmargin <- 0.15
      pp$topmargin <- 5
      pp$bottommargin <- 5
      pp$ideogramheight <- 5
      pp$data1inmargin <- 10
      pp$data1outmargin <- 0
      
      # Start to plot bigwig files
      big.wig.files <- dir(path = "~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/ESCA bigwigs",
                           pattern = ".bw",
                           all.files = T,
                           full.names = T)
      # Reserve area to plot the bigwig files
      out.at <- autotrack(1:length(big.wig.files), 
                          length(big.wig.files), 
                          margin = 0.3, 
                          r0 = 0,
                          r1 = 1)
      
      setwd('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/ESCA bigwigs/plots/th_1.25/')
      threshold = 1.25
      
      for (rgn in 1:length(peaks_ov)){
        cat(rgn,'\n')
        tiff(paste0(peaks_ov[rgn]@seqnames,'__',peaks_ov[rgn]@ranges,'.tiff'),
             height = 10, width = 4, units = 'in', res=300)
        
        # plot will be at  the HNF4A range +- 10Kb
        plt.region <- range(c(peaks_ov[rgn])) + 10000
        
        # Start by plotting gene tracks
        kp <- plotKaryotype(zoom = plt.region,genome = "hg38", cex = 0.5, plot.params = pp)
        genes.data <- makeGenesDataFromTxDb(TxDb.Hsapiens.UCSC.hg38.knownGene,
                                            karyoplot = kp,
                                            plot.transcripts = TRUE, 
                                            plot.transcripts.structure = TRUE)
        genes.data <- addGeneNames(genes.data)
        
        
        
        genes.data <- mergeTranscripts(genes.data)
        
        kp <- plotKaryotype(zoom = plt.region,genome = "hg38", cex = 0.5, plot.params = pp)
        kpAddBaseNumbers(kp, tick.dist = 20000, minor.tick.dist = 5000,
                         add.units = TRUE, cex = 0.5, tick.len = 1)
        #if we would like to put genes under ATAC peaks
        #kpPlotGenes(kp, data = genes.data, r0 = 0, r1 = 0.25, gene.name.cex = 0.5)
        
        
        # Add ATAC-seq label from 0.3 to 1 which should cover all ATAC-seq tracks
        kpAddLabels(kp, 
                    labels = "ATAC-Seq", 
                    r0 = out.at$r0, 
                    r1 = out.at$r1, 
                    side = "left",
                    cex = 0.25,
                    srt = 90, 
                    pos = 3, 
                    label.margin = 0.1)
        
        
        for(i in seq_len(length(big.wig.files))) {
          bigwig.file <- big.wig.files[i]
          
          # just getting the name
          tec_rep_name <- strsplit(strsplit(bigwig.file,split = './')[[1]][9],split = '\\.')[[1]][1]
          cnt <- counts_m[which(!is.na(peaks_overlaps))[rgn],colnames(counts_m) %in% tec_rep_name]
          
          # adaptive threshold by using the score for each region
          # threshold <-counts_m[which(!is.na(peaks_overlaps))[rgn],5]
          
          # Define where the track will be ploted
          # autotrack will simple get the reserved space (from out.at$r0 up to out.at$r1)
          # and split in equal sizes for each bigwifile, i the index, will control which 
          # one is being plotted
          at <- autotrack(i, length(big.wig.files), r0 = out.at$r0, r1 = out.at$r1, margin = 0.2)
          
          
          # Plot bigwig
          kp <- kpPlotBigWig(kp, 
                             data = bigwig.file, 
                             ymax = "visible.region", 
                             r0 = at$r0, 
                             col = ifelse(cnt>threshold,"#0000FF","#FF0000"), 
                             r1 = at$r1)
          computed.ymax <- ceiling(kp$latest.plot$computed.values$ymax)
          
          # Add track axis
          kpAxis(kp, 
                 ymin = 0, 
                 ymax = 150, 
                 numticks = 2,
                 r0 = at$r0, 
                 r1 = at$r1,
                 cex = 0.15)
          
          # Add track label
          kpAddLabels(kp, 
                      labels = ifelse(cnt>threshold,"Peak","No Peak"),
                      r0 = at$r0, 
                      r1 = at$r1, 
                      cex = 0.25, 
                      label.margin = 0.01)
          
          
        }
        
        kpPlotRegions(kp, data=peaks_ov[rgn], col="#FFFCCC50", border="#00000044")
        kpPlotRegions(kp, data=peaks_ov[rgn], col="#FFFCCC50", border="#00000044")
        
        dev.off()
        
      }
      
    }
    
    
    
  }
  
  # Peak intersection for each cluster
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/ESCA_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    
    peak_m <- (counts_m[,(6:ncol(counts_m))] > 0.7)*1
    
    
    #Cluster1
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster1.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl1 <- ov_peaks
    
    
    
    #Cluster2
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster2.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl2 <- ov_peaks
    
    
    #Cluster3
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster3.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl3 <- ov_peaks
    
    
    #Cluster4
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl4 <- ov_peaks
    
    
    #Cluster5
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster5.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl5 <- ov_peaks
  }
  
  #plotting
  {
    n_cl1 <-  1157
    n_cl2 <-  741
    n_cl3 <-  919
    n_cl4 <-  476
    n_cl5 <-  424
    
    
    rt_cl1 <- colSums(ov_cl1) / n_cl1
    rt_cl2 <- colSums(ov_cl2) / n_cl2
    rt_cl3 <- colSums(ov_cl3) / n_cl3
    rt_cl4 <- colSums(ov_cl4) / n_cl4
    rt_cl5 <- colSums(ov_cl5) / n_cl5
    
    df <- data.frame(Cluster = c('Cluster 1','Cluster 2','Cluster 3','Cluster 4','Cluster 5'))
    df$Overlaps <- c(mean(rt_cl1),mean(rt_cl2),mean(rt_cl3),mean(rt_cl4),mean(rt_cl5))
    df$Ratio = df$Overlaps/sum(df$Overlaps)
    
    
    my.cols <- brewer.pal(n = 8, name = "Dark2")
    my.cols[4] <- my.cols[7]
    my.cols[5] <- my.cols[8]
    
    setwd('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/ESCA bigwigs/plots/')
    tiff("ESCA_ov_1.2.tiff",
         height = 5, width = 10, units = 'in', res=300)
    ggplot(data=df, aes(x=Cluster, y=Ratio, fill = Cluster)) +
      geom_bar(stat="identity")+
      theme_minimal() + labs(fill = "Percentages") + 
      theme(legend.title = element_text(size = 18),
            legend.text = element_text(size = 15)) +
      scale_fill_manual(values=my.cols) +
      ylim(c(0,max(df$Ratio + 0.1))) + ggtitle("ESCA") + 
      xlab("") + ylab("Ratio of Overlapped Peaks") +
      geom_text(aes(label=round(Ratio,digits = 3)), position=position_stack(), vjust=-0.5,size = 8) +
      theme(legend.position = "none",axis.text.x = element_text(face="bold", size=14))
    dev.off()
    
  }
  
  # Sample-wise cluster ditribution
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/ESCA_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    IDs <- colnames(counts_m)[6:ncol(counts_m)]
    samples <- unique(sapply(IDs, function(x) paste0(strsplit(x,split = '_')[[1]][1:8],collapse = '_') ))
    length(samples)
    peak_m <- (counts_m[,(6:ncol(counts_m))] > 1.4)*1
    colnames(peak_m) <- IDs
    lst_tech_rep <- lapply(samples, function(x) which(str_detect(IDs,x)))
    sample_peaks<- matrix(0,nrow(peak_m),length(lst_tech_rep))
    colnames(sample_peaks) <- samples
    for (i in 1:length(lst_tech_rep)){
      #checking if we have multiple technical replicates
      if(!is.null(dim(counts_m[,lst_tech_rep[[i]]]))){
        sample_peaks[,i] = (rowSums(peak_m[,lst_tech_rep[[i]]]) != 0) * 1
      }else{
        sample_peaks[,i] = peak_m[,lst_tech_rep[[i]]]
      }
    }
    # All analysis use peak_m, I didnot  want to rewrite all
    peak_m <- sample_peaks
    
    
    #Cluster1
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster1.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl1 <- ov_peaks
    
    #Cluster2
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster2.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl2 <- ov_peaks
    
    #Cluster3
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster3.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl3 <- ov_peaks
    
    
    #Cluster4
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl4 <- ov_peaks
    
    #Cluster5
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster5.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    
    ov_cl5 <- ov_peaks
    
    
    ESCA_sample_matrix <- matrix(0,length(samples),5)
    
    n_cl1 <-  1157
    n_cl2 <-  741
    n_cl3 <-  919
    n_cl4 <-  476
    n_cl5 <-  424
    
    rt_cl1 <- colSums(ov_cl1) / n_cl1
    rt_cl2 <- colSums(ov_cl2) / n_cl2
    rt_cl3 <- colSums(ov_cl3) / n_cl3
    rt_cl4 <- colSums(ov_cl4) / n_cl4
    rt_cl5 <- colSums(ov_cl5) / n_cl5
    all_rt <- rt_cl1 + rt_cl2 + rt_cl3 + rt_cl4 + rt_cl5
    
    ESCA_sample_matrix[,1] <- rt_cl1/all_rt
    ESCA_sample_matrix[,2] <- rt_cl2/all_rt
    ESCA_sample_matrix[,3] <- rt_cl3/all_rt
    ESCA_sample_matrix[,4] <- rt_cl4/all_rt
    ESCA_sample_matrix[,5] <- rt_cl5/all_rt
    
    rownames(ESCA_sample_matrix) <- samples
  }
  
}


# GBM - threshold = 0.9
{
  #Choosing the threshold 
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/GBM_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    
    
    cluster4 <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl4 <- GRanges(seq = cluster4$V1,IRanges(start = cluster4$V2,end = cluster4$V3))
    
    #1 bp overlaps
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl4,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    
    peaks_ov <- counts_regions[which(!is.na(peaks_overlaps)),]
    
    
    
    # bigwig plotting
    {
      # Get transcrupts annotation to get HNF4A regions
      tssAnnot <- ELMER::getTSS(genome = "hg38")
      
      # Plot parameters, only to look better
      pp <- getDefaultPlotParams(plot.type = 1)
      pp$leftmargin <- 0.15
      pp$topmargin <- 5
      pp$bottommargin <- 5
      pp$ideogramheight <- 5
      pp$data1inmargin <- 10
      pp$data1outmargin <- 0
      
      # Start to plot bigwig files
      big.wig.files <- dir(path = "~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/GBMC bigwig",
                           pattern = ".bw",
                           all.files = T,
                           full.names = T)
      # Reserve area to plot the bigwig files
      out.at <- autotrack(1:length(big.wig.files), 
                          length(big.wig.files), 
                          margin = 0.3, 
                          r0 = 0,
                          r1 = 1)
      
      setwd('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/GBMC bigwig/plots/th_1.5/')
      threshold = 1.5
      
      for (rgn in 1:length(peaks_ov)){
        cat(rgn,'\n')
        tiff(paste0(peaks_ov[rgn]@seqnames,'__',peaks_ov[rgn]@ranges,'.tiff'),
             height = 10, width = 4, units = 'in', res=300)
        
        # plot will be at  the HNF4A range +- 10Kb
        plt.region <- range(c(peaks_ov[rgn])) + 10000
        
        # Start by plotting gene tracks
        kp <- plotKaryotype(zoom = plt.region,genome = "hg38", cex = 0.5, plot.params = pp)
        genes.data <- makeGenesDataFromTxDb(TxDb.Hsapiens.UCSC.hg38.knownGene,
                                            karyoplot = kp,
                                            plot.transcripts = TRUE, 
                                            plot.transcripts.structure = TRUE)
        genes.data <- addGeneNames(genes.data)
        
        
        
        genes.data <- mergeTranscripts(genes.data)
        
        kp <- plotKaryotype(zoom = plt.region,genome = "hg38", cex = 0.5, plot.params = pp)
        kpAddBaseNumbers(kp, tick.dist = 20000, minor.tick.dist = 5000,
                         add.units = TRUE, cex = 0.5, tick.len = 1)
        #if we would like to put genes under ATAC peaks
        # kpPlotGenes(kp, data = genes.data, r0 = 0, r1 = 0.25, gene.name.cex = 0.5)
        
        
        # Add ATAC-seq label from 0.3 to 1 which should cover all ATAC-seq tracks
        kpAddLabels(kp, 
                    labels = "ATAC-Seq", 
                    r0 = out.at$r0, 
                    r1 = out.at$r1, 
                    side = "left",
                    cex = 0.25,
                    srt = 90, 
                    pos = 3, 
                    label.margin = 0.1)
        
        
        for(i in seq_len(length(big.wig.files))) {
          bigwig.file <- big.wig.files[i]
          
          # just getting the name
          tec_rep_name <- strsplit(strsplit(bigwig.file,split = './')[[1]][9],split = '\\.')[[1]][1]
          cnt <- counts_m[which(!is.na(peaks_overlaps))[rgn],colnames(counts_m) %in% tec_rep_name]
          
          # adaptive threshold by using the score for each region
          # threshold <-counts_m[which(!is.na(peaks_overlaps))[rgn],5]
          
          # Define where the track will be ploted
          # autotrack will simple get the reserved space (from out.at$r0 up to out.at$r1)
          # and split in equal sizes for each bigwifile, i the index, will control which 
          # one is being plotted
          at <- autotrack(i, length(big.wig.files), r0 = out.at$r0, r1 = out.at$r1, margin = 0.2)
          
          
          # Plot bigwig
          kp <- kpPlotBigWig(kp, 
                             data = bigwig.file, 
                             ymax = "visible.region", 
                             r0 = at$r0, 
                             col = ifelse(cnt>threshold,"#0000FF","#FF0000"), 
                             r1 = at$r1)
          computed.ymax <- ceiling(kp$latest.plot$computed.values$ymax)
          
          # Add track axis
          kpAxis(kp, 
                 ymin = 0, 
                 ymax = 150, 
                 numticks = 2,
                 r0 = at$r0, 
                 r1 = at$r1,
                 cex = 0.15)
          
          # Add track label
          kpAddLabels(kp, 
                      labels = ifelse(cnt>threshold,"Peak","No Peak"),
                      r0 = at$r0, 
                      r1 = at$r1, 
                      cex = 0.25, 
                      label.margin = 0.01)
          
          
        }
        
        kpPlotRegions(kp, data=peaks_ov[rgn], col="#FFFCCC50", border="#00000044")
        kpPlotRegions(kp, data=peaks_ov[rgn], col="#FFFCCC50", border="#00000044")
        
        dev.off()
        
      }
      
    }
    
    
    
  }
  
  # Peak intersection for each cluster
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/GBM_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    
    peak_m <- (counts_m[,(6:ncol(counts_m))] > 0.7)*1
    
    
    #Cluster1
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster1.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl1 <- ov_peaks
    
    
    
    #Cluster2
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster2.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl2 <- ov_peaks
    
    
    #Cluster3
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster3.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl3 <- ov_peaks
    
    
    #Cluster4
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl4 <- ov_peaks
    
    
    #Cluster5
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster5.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl5 <- ov_peaks
  }
  
  #plotting
  {
    n_cl1 <-  1157
    n_cl2 <-  741
    n_cl3 <-  919
    n_cl4 <-  476
    n_cl5 <-  424
    
    
    rt_cl1 <- colSums(ov_cl1) / n_cl1
    rt_cl2 <- colSums(ov_cl2) / n_cl2
    rt_cl3 <- colSums(ov_cl3) / n_cl3
    rt_cl4 <- colSums(ov_cl4) / n_cl4
    rt_cl5 <- colSums(ov_cl5) / n_cl5
    
    df <- data.frame(Cluster = c('Cluster 1','Cluster 2','Cluster 3','Cluster 4','Cluster 5'))
    df$Overlaps <- c(mean(rt_cl1),mean(rt_cl2),mean(rt_cl3),mean(rt_cl4),mean(rt_cl5))
    df$Ratio = df$Overlaps/sum(df$Overlaps)
    
    
    my.cols <- brewer.pal(n = 8, name = "Dark2")
    my.cols[4] <- my.cols[7]
    my.cols[5] <- my.cols[8]
    
    setwd('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/GBM bigwigs/plots/')
    tiff("GBM_ov_1.2.tiff",
         height = 5, width = 10, units = 'in', res=300)
    ggplot(data=df, aes(x=Cluster, y=Ratio, fill = Cluster)) +
      geom_bar(stat="identity")+
      theme_minimal() + labs(fill = "Percentages") + 
      theme(legend.title = element_text(size = 18),
            legend.text = element_text(size = 15)) +
      scale_fill_manual(values=my.cols) +
      ylim(c(0,max(df$Ratio + 0.1))) + ggtitle("GBM") + 
      xlab("") + ylab("Ratio of Overlapped Peaks") +
      geom_text(aes(label=round(Ratio,digits = 3)), position=position_stack(), vjust=-0.5,size = 8) +
      theme(legend.position = "none",axis.text.x = element_text(face="bold", size=14))
    dev.off()
    
  }
  
  # Sample-wise cluster ditribution
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/GBM_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    IDs <- colnames(counts_m)[6:ncol(counts_m)]
    samples <- unique(sapply(IDs, function(x) paste0(strsplit(x,split = '_')[[1]][1:8],collapse = '_') ))
    length(samples)
    peak_m <- (counts_m[,(6:ncol(counts_m))] > 0.9)*1
    colnames(peak_m) <- IDs
    lst_tech_rep <- lapply(samples, function(x) which(str_detect(IDs,x)))
    sample_peaks<- matrix(0,nrow(peak_m),length(lst_tech_rep))
    colnames(sample_peaks) <- samples
    for (i in 1:length(lst_tech_rep)){
      #checking if we have multiple technical replicates
      if(!is.null(dim(counts_m[,lst_tech_rep[[i]]]))){
        sample_peaks[,i] = (rowSums(peak_m[,lst_tech_rep[[i]]]) != 0) * 1
      }else{
        sample_peaks[,i] = peak_m[,lst_tech_rep[[i]]]
      }
    }
    # All analysis use peak_m, I didnot  want to rewrite all
    peak_m <- sample_peaks
    
    
    #Cluster1
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster1.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl1 <- ov_peaks
    
    #Cluster2
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster2.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl2 <- ov_peaks
    
    #Cluster3
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster3.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl3 <- ov_peaks
    
    
    #Cluster4
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl4 <- ov_peaks
    
    #Cluster5
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster5.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    
    ov_cl5 <- ov_peaks
    
    
    GBM_sample_matrix <- matrix(0,length(samples),5)
    
    n_cl1 <-  1157
    n_cl2 <-  741
    n_cl3 <-  919
    n_cl4 <-  476
    n_cl5 <-  424
    
    rt_cl1 <- colSums(ov_cl1) / n_cl1
    rt_cl2 <- colSums(ov_cl2) / n_cl2
    rt_cl3 <- colSums(ov_cl3) / n_cl3
    rt_cl4 <- colSums(ov_cl4) / n_cl4
    rt_cl5 <- colSums(ov_cl5) / n_cl5
    all_rt <- rt_cl1 + rt_cl2 + rt_cl3 + rt_cl4 + rt_cl5
    
    GBM_sample_matrix[,1] <- rt_cl1/all_rt
    GBM_sample_matrix[,2] <- rt_cl2/all_rt
    GBM_sample_matrix[,3] <- rt_cl3/all_rt
    GBM_sample_matrix[,4] <- rt_cl4/all_rt
    GBM_sample_matrix[,5] <- rt_cl5/all_rt
    
    rownames(GBM_sample_matrix) <- samples
  }
}


# HNSC - threshold = 1.2
{
  #Choosing the threshold 
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/HNSC_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    
    
    cluster4 <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl4 <- GRanges(seq = cluster4$V1,IRanges(start = cluster4$V2,end = cluster4$V3))
    
    #1 bp overlaps
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl4,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    
    peaks_ov <- counts_regions[which(!is.na(peaks_overlaps)),]
    
    
    # bigwig plotting
    {
      # Get transcrupts annotation to get HNF4A regions
      tssAnnot <- ELMER::getTSS(genome = "hg38")
      
      # Plot parameters, only to look better
      pp <- getDefaultPlotParams(plot.type = 1)
      pp$leftmargin <- 0.15
      pp$topmargin <- 5
      pp$bottommargin <- 5
      pp$ideogramheight <- 5
      pp$data1inmargin <- 10
      pp$data1outmargin <- 0
      
      # Start to plot bigwig files
      big.wig.files <- dir(path = "~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/HNSC bigwigs",
                           pattern = ".bw",
                           all.files = T,
                           full.names = T)
      # Reserve area to plot the bigwig files
      out.at <- autotrack(1:length(big.wig.files), 
                          length(big.wig.files), 
                          margin = 0.3, 
                          r0 = 0,
                          r1 = 1)
      
      setwd('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/HNSC bigwigs/plots/th_1.25/')
      threshold = 1.25
      
      for (rgn in 1:length(peaks_ov)){
        cat(rgn,'\n')
        tiff(paste0(peaks_ov[rgn]@seqnames,'__',peaks_ov[rgn]@ranges,'.tiff'),
             height = 10, width = 4, units = 'in', res=300)
        
        # plot will be at  the HNF4A range +- 10Kb
        plt.region <- range(c(peaks_ov[rgn])) + 10000
        
        # Start by plotting gene tracks
        kp <- plotKaryotype(zoom = plt.region,genome = "hg38", cex = 0.5, plot.params = pp)
        genes.data <- makeGenesDataFromTxDb(TxDb.Hsapiens.UCSC.hg38.knownGene,
                                            karyoplot = kp,
                                            plot.transcripts = TRUE, 
                                            plot.transcripts.structure = TRUE)
        genes.data <- addGeneNames(genes.data)
        
        
        
        genes.data <- mergeTranscripts(genes.data)
        
        kp <- plotKaryotype(zoom = plt.region,genome = "hg38", cex = 0.5, plot.params = pp)
        kpAddBaseNumbers(kp, tick.dist = 20000, minor.tick.dist = 5000,
                         add.units = TRUE, cex = 0.5, tick.len = 1)
        #if we would like to put genes under ATAC peaks
        # kpPlotGenes(kp, data = genes.data, r0 = 0, r1 = 0.25, gene.name.cex = 0.5)
        
        
        # Add ATAC-seq label from 0.3 to 1 which should cover all ATAC-seq tracks
        kpAddLabels(kp, 
                    labels = "ATAC-Seq", 
                    r0 = out.at$r0, 
                    r1 = out.at$r1, 
                    side = "left",
                    cex = 0.25,
                    srt = 90, 
                    pos = 3, 
                    label.margin = 0.1)
        
        
        for(i in seq_len(length(big.wig.files))) {
          bigwig.file <- big.wig.files[i]
          
          # just getting the name
          tec_rep_name <- strsplit(strsplit(bigwig.file,split = './')[[1]][9],split = '\\.')[[1]][1]
          cnt <- counts_m[which(!is.na(peaks_overlaps))[rgn],colnames(counts_m) %in% tec_rep_name]
          
          # adaptive threshold by using the score for each region
          # threshold <-counts_m[which(!is.na(peaks_overlaps))[rgn],5]
          
          # Define where the track will be ploted
          # autotrack will simple get the reserved space (from out.at$r0 up to out.at$r1)
          # and split in equal sizes for each bigwifile, i the index, will control which 
          # one is being plotted
          at <- autotrack(i, length(big.wig.files), r0 = out.at$r0, r1 = out.at$r1, margin = 0.2)
          
          
          # Plot bigwig
          kp <- kpPlotBigWig(kp, 
                             data = bigwig.file, 
                             ymax = "visible.region", 
                             r0 = at$r0, 
                             col = ifelse(cnt>threshold,"#0000FF","#FF0000"), 
                             r1 = at$r1)
          computed.ymax <- ceiling(kp$latest.plot$computed.values$ymax)
          
          # Add track axis
          kpAxis(kp, 
                 ymin = 0, 
                 ymax = 150, 
                 numticks = 2,
                 r0 = at$r0, 
                 r1 = at$r1,
                 cex = 0.15)
          
          # Add track label
          kpAddLabels(kp, 
                      labels = ifelse(cnt>threshold,"Peak","No Peak"),
                      r0 = at$r0, 
                      r1 = at$r1, 
                      cex = 0.25, 
                      label.margin = 0.01)
          
          
        }
        
        kpPlotRegions(kp, data=peaks_ov[rgn], col="#FFFCCC50", border="#00000044")
        kpPlotRegions(kp, data=peaks_ov[rgn], col="#FFFCCC50", border="#00000044")
        
        dev.off()
        
      }
      
    }
    
    
    
  }
  
  # Peak intersection for each cluster
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/HNSC_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    
    peak_m <- (counts_m[,(6:ncol(counts_m))] > 0.7)*1
    
    
    #Cluster1
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster1.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl1 <- ov_peaks
    
    
    
    #Cluster2
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster2.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl2 <- ov_peaks
    
    
    #Cluster3
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster3.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl3 <- ov_peaks
    
    
    #Cluster4
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl4 <- ov_peaks
    
    
    #Cluster5
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster5.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl5 <- ov_peaks
  }
  
  #plotting
  {
    n_cl1 <-  1157
    n_cl2 <-  741
    n_cl3 <-  919
    n_cl4 <-  476
    n_cl5 <-  424
    
    
    rt_cl1 <- colSums(ov_cl1) / n_cl1
    rt_cl2 <- colSums(ov_cl2) / n_cl2
    rt_cl3 <- colSums(ov_cl3) / n_cl3
    rt_cl4 <- colSums(ov_cl4) / n_cl4
    rt_cl5 <- colSums(ov_cl5) / n_cl5
    
    df <- data.frame(Cluster = c('Cluster 1','Cluster 2','Cluster 3','Cluster 4','Cluster 5'))
    df$Overlaps <- c(mean(rt_cl1),mean(rt_cl2),mean(rt_cl3),mean(rt_cl4),mean(rt_cl5))
    df$Ratio = df$Overlaps/sum(df$Overlaps)
    
    
    my.cols <- brewer.pal(n = 8, name = "Dark2")
    my.cols[4] <- my.cols[7]
    my.cols[5] <- my.cols[8]
    
    setwd('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/HNSC bigwigs/plots/')
    tiff("HNSC_ov_1.2.tiff",
         height = 5, width = 10, units = 'in', res=300)
    ggplot(data=df, aes(x=Cluster, y=Ratio, fill = Cluster)) +
      geom_bar(stat="identity")+
      theme_minimal() + labs(fill = "Percentages") + 
      theme(legend.title = element_text(size = 18),
            legend.text = element_text(size = 15)) +
      scale_fill_manual(values=my.cols) +
      ylim(c(0,max(df$Ratio + 0.1))) + ggtitle("HNSC") + 
      xlab("") + ylab("Ratio of Overlapped Peaks") +
      geom_text(aes(label=round(Ratio,digits = 3)), position=position_stack(), vjust=-0.5,size = 8) +
      theme(legend.position = "none",axis.text.x = element_text(face="bold", size=14))
    dev.off()
    
  }
  
  # Sample-wise cluster ditribution
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/HNSC_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    IDs <- colnames(counts_m)[6:ncol(counts_m)]
    samples <- unique(sapply(IDs, function(x) paste0(strsplit(x,split = '_')[[1]][1:8],collapse = '_') ))
    length(samples)
    peak_m <- (counts_m[,(6:ncol(counts_m))] > 1.2)*1
    colnames(peak_m) <- IDs
    lst_tech_rep <- lapply(samples, function(x) which(str_detect(IDs,x)))
    sample_peaks<- matrix(0,nrow(peak_m),length(lst_tech_rep))
    colnames(sample_peaks) <- samples
    for (i in 1:length(lst_tech_rep)){
      #checking if we have multiple technical replicates
      if(!is.null(dim(counts_m[,lst_tech_rep[[i]]]))){
        sample_peaks[,i] = (rowSums(peak_m[,lst_tech_rep[[i]]]) != 0) * 1
      }else{
        sample_peaks[,i] = peak_m[,lst_tech_rep[[i]]]
      }
    }
    # All analysis use peak_m, I didnot  want to rewrite all
    peak_m <- sample_peaks
    
    
    #Cluster1
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster1.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl1 <- ov_peaks
    
    #Cluster2
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster2.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl2 <- ov_peaks
    
    #Cluster3
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster3.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl3 <- ov_peaks
    
    
    #Cluster4
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl4 <- ov_peaks
    
    #Cluster5
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster5.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    
    ov_cl5 <- ov_peaks
    
    
    HNSC_sample_matrix <- matrix(0,length(samples),5)
    
    n_cl1 <-  1157
    n_cl2 <-  741
    n_cl3 <-  919
    n_cl4 <-  476
    n_cl5 <-  424
    
    rt_cl1 <- colSums(ov_cl1) / n_cl1
    rt_cl2 <- colSums(ov_cl2) / n_cl2
    rt_cl3 <- colSums(ov_cl3) / n_cl3
    rt_cl4 <- colSums(ov_cl4) / n_cl4
    rt_cl5 <- colSums(ov_cl5) / n_cl5
    all_rt <- rt_cl1 + rt_cl2 + rt_cl3 + rt_cl4 + rt_cl5
    
    HNSC_sample_matrix[,1] <- rt_cl1/all_rt
    HNSC_sample_matrix[,2] <- rt_cl2/all_rt
    HNSC_sample_matrix[,3] <- rt_cl3/all_rt
    HNSC_sample_matrix[,4] <- rt_cl4/all_rt
    HNSC_sample_matrix[,5] <- rt_cl5/all_rt
    
    rownames(HNSC_sample_matrix) <- samples
  }
  
}

 
# KIRC - threshold = 0.9
{
  #Choosing the threshold 
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/KIRC_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    
    
    cluster4 <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl4 <- GRanges(seq = cluster4$V1,IRanges(start = cluster4$V2,end = cluster4$V3))
    
    #1 bp overlaps
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl4,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    
    peaks_ov <- counts_regions[which(!is.na(peaks_overlaps)),]
    
    
    # bigwig plotting
    {
      # Get transcrupts annotation to get HNF4A regions
      tssAnnot <- ELMER::getTSS(genome = "hg38")
      
      # Plot parameters, only to look better
      pp <- getDefaultPlotParams(plot.type = 1)
      pp$leftmargin <- 0.15
      pp$topmargin <- 5
      pp$bottommargin <- 5
      pp$ideogramheight <- 5
      pp$data1inmargin <- 10
      pp$data1outmargin <- 0
      
      # Start to plot bigwig files
      big.wig.files <- dir(path = "~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/KIRC bigwigs",
                           pattern = ".bw",
                           all.files = T,
                           full.names = T)
      # Reserve area to plot the bigwig files
      out.at <- autotrack(1:length(big.wig.files), 
                          length(big.wig.files), 
                          margin = 0.3, 
                          r0 = 0,
                          r1 = 1)
      
      setwd('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/KIRC bigwigs/plots/th_1.5//')
      threshold = 1.5
      
      for (rgn in 1:length(peaks_ov)){
        cat(rgn,'\n')
        tiff(paste0(peaks_ov[rgn]@seqnames,'__',peaks_ov[rgn]@ranges,'.tiff'),
             height = 10, width = 4, units = 'in', res=300)
        
        # plot will be at  the HNF4A range +- 10Kb
        plt.region <- range(c(peaks_ov[rgn])) + 10000
        
        # Start by plotting gene tracks
        kp <- plotKaryotype(zoom = plt.region,genome = "hg38", cex = 0.5, plot.params = pp)
        genes.data <- makeGenesDataFromTxDb(TxDb.Hsapiens.UCSC.hg38.knownGene,
                                            karyoplot = kp,
                                            plot.transcripts = TRUE, 
                                            plot.transcripts.structure = TRUE)
        genes.data <- addGeneNames(genes.data)
        
        
        
        genes.data <- mergeTranscripts(genes.data)
        
        kp <- plotKaryotype(zoom = plt.region,genome = "hg38", cex = 0.5, plot.params = pp)
        kpAddBaseNumbers(kp, tick.dist = 20000, minor.tick.dist = 5000,
                         add.units = TRUE, cex = 0.5, tick.len = 1)
        #if we would like to put genes under ATAC peaks
        # kpPlotGenes(kp, data = genes.data, r0 = 0, r1 = 0.25, gene.name.cex = 0.5)
        
        
        # Add ATAC-seq label from 0.3 to 1 which should cover all ATAC-seq tracks
        kpAddLabels(kp, 
                    labels = "ATAC-Seq", 
                    r0 = out.at$r0, 
                    r1 = out.at$r1, 
                    side = "left",
                    cex = 0.25,
                    srt = 90, 
                    pos = 3, 
                    label.margin = 0.1)
        
        
        for(i in seq_len(length(big.wig.files))) {
          bigwig.file <- big.wig.files[i]
          
          # just getting the name
          tec_rep_name <- strsplit(strsplit(bigwig.file,split = './')[[1]][9],split = '\\.')[[1]][1]
          cnt <- counts_m[which(!is.na(peaks_overlaps))[rgn],colnames(counts_m) %in% tec_rep_name]
          
          # adaptive threshold by using the score for each region
          # threshold <-counts_m[which(!is.na(peaks_overlaps))[rgn],5]
          
          # Define where the track will be ploted
          # autotrack will simple get the reserved space (from out.at$r0 up to out.at$r1)
          # and split in equal sizes for each bigwifile, i the index, will control which 
          # one is being plotted
          at <- autotrack(i, length(big.wig.files), r0 = out.at$r0, r1 = out.at$r1, margin = 0.2)
          
          
          # Plot bigwig
          kp <- kpPlotBigWig(kp, 
                             data = bigwig.file, 
                             ymax = "visible.region", 
                             r0 = at$r0, 
                             col = ifelse(cnt>threshold,"#0000FF","#FF0000"), 
                             r1 = at$r1)
          computed.ymax <- ceiling(kp$latest.plot$computed.values$ymax)
          
          # Add track axis
          kpAxis(kp, 
                 ymin = 0, 
                 ymax = 150, 
                 numticks = 2,
                 r0 = at$r0, 
                 r1 = at$r1,
                 cex = 0.15)
          
          # Add track label
          kpAddLabels(kp, 
                      labels = ifelse(cnt>threshold,"Peak","No Peak"),
                      r0 = at$r0, 
                      r1 = at$r1, 
                      cex = 0.25, 
                      label.margin = 0.01)
          
          
        }
        
        kpPlotRegions(kp, data=peaks_ov[rgn], col="#FFFCCC50", border="#00000044")
        kpPlotRegions(kp, data=peaks_ov[rgn], col="#FFFCCC50", border="#00000044")
        
        dev.off()
        
      }
      
    }
    
    
    
  }
  
  # Peak intersection for each cluster
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/KIRC_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    
    peak_m <- (counts_m[,(6:ncol(counts_m))] > 0.7)*1
    
    
    #Cluster1
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster1.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl1 <- ov_peaks
    
    
    
    #Cluster2
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster2.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl2 <- ov_peaks
    
    
    #Cluster3
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster3.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl3 <- ov_peaks
    
    
    #Cluster4
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl4 <- ov_peaks
    
    
    #Cluster5
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster5.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl5 <- ov_peaks
  }
  
  #plotting
  {
    n_cl1 <-  1157
    n_cl2 <-  741
    n_cl3 <-  919
    n_cl4 <-  476
    n_cl5 <-  424
    
    
    rt_cl1 <- colSums(ov_cl1) / n_cl1
    rt_cl2 <- colSums(ov_cl2) / n_cl2
    rt_cl3 <- colSums(ov_cl3) / n_cl3
    rt_cl4 <- colSums(ov_cl4) / n_cl4
    rt_cl5 <- colSums(ov_cl5) / n_cl5
    
    df <- data.frame(Cluster = c('Cluster 1','Cluster 2','Cluster 3','Cluster 4','Cluster 5'))
    df$Overlaps <- c(mean(rt_cl1),mean(rt_cl2),mean(rt_cl3),mean(rt_cl4),mean(rt_cl5))
    df$Ratio = df$Overlaps/sum(df$Overlaps)
    
    
    my.cols <- brewer.pal(n = 8, name = "Dark2")
    my.cols[4] <- my.cols[7]
    my.cols[5] <- my.cols[8]
    
    setwd('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/KIRC bigwigs/plots/')
    tiff("KIRC_ov_0.7.tiff",
         height = 5, width = 10, units = 'in', res=300)
    ggplot(data=df, aes(x=Cluster, y=Ratio, fill = Cluster)) +
      geom_bar(stat="identity")+
      theme_minimal() + labs(fill = "Percentages") + 
      theme(legend.title = element_text(size = 18),
            legend.text = element_text(size = 15)) +
      scale_fill_manual(values=my.cols) +
      ylim(c(0,max(df$Ratio + 0.1))) + ggtitle("KIRC") + 
      xlab("") + ylab("Ratio of Overlapped Peaks") +
      geom_text(aes(label=round(Ratio,digits = 3)), position=position_stack(), vjust=-0.5,size = 8) +
      theme(legend.position = "none",axis.text.x = element_text(face="bold", size=14))
    dev.off()
    
  }
  
  # Sample-wise cluster ditribution
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/KIRC_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    IDs <- colnames(counts_m)[6:ncol(counts_m)]
    samples <- unique(sapply(IDs, function(x) paste0(strsplit(x,split = '_')[[1]][1:8],collapse = '_') ))
    length(samples)
    peak_m <- (counts_m[,(6:ncol(counts_m))] > 0.9)*1
    colnames(peak_m) <- IDs
    lst_tech_rep <- lapply(samples, function(x) which(str_detect(IDs,x)))
    sample_peaks<- matrix(0,nrow(peak_m),length(lst_tech_rep))
    colnames(sample_peaks) <- samples
    for (i in 1:length(lst_tech_rep)){
      #checking if we have multiple technical replicates
      if(!is.null(dim(counts_m[,lst_tech_rep[[i]]]))){
        sample_peaks[,i] = (rowSums(peak_m[,lst_tech_rep[[i]]]) != 0) * 1
      }else{
        sample_peaks[,i] = peak_m[,lst_tech_rep[[i]]]
      }
    }
    # All analysis use peak_m, I didnot  want to rewrite all
    peak_m <- sample_peaks
    
    
    #Cluster1
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster1.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl1 <- ov_peaks
    
    #Cluster2
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster2.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl2 <- ov_peaks
    
    #Cluster3
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster3.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl3 <- ov_peaks
    
    
    #Cluster4
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl4 <- ov_peaks
    
    #Cluster5
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster5.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    
    ov_cl5 <- ov_peaks
    
    
    KIRC_sample_matrix <- matrix(0,length(samples),5)
    
    n_cl1 <-  1157
    n_cl2 <-  741
    n_cl3 <-  919
    n_cl4 <-  476
    n_cl5 <-  424
    
    rt_cl1 <- colSums(ov_cl1) / n_cl1
    rt_cl2 <- colSums(ov_cl2) / n_cl2
    rt_cl3 <- colSums(ov_cl3) / n_cl3
    rt_cl4 <- colSums(ov_cl4) / n_cl4
    rt_cl5 <- colSums(ov_cl5) / n_cl5
    all_rt <- rt_cl1 + rt_cl2 + rt_cl3 + rt_cl4 + rt_cl5
    
    KIRC_sample_matrix[,1] <- rt_cl1/all_rt
    KIRC_sample_matrix[,2] <- rt_cl2/all_rt
    KIRC_sample_matrix[,3] <- rt_cl3/all_rt
    KIRC_sample_matrix[,4] <- rt_cl4/all_rt
    KIRC_sample_matrix[,5] <- rt_cl5/all_rt
    
    rownames(KIRC_sample_matrix) <- samples
  }
  
}


# KIRP - threshold = 0.6
{
  #Choosing the threshold 
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/KIRP_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    
    
    cluster4 <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl4 <- GRanges(seq = cluster4$V1,IRanges(start = cluster4$V2,end = cluster4$V3))
    
    #1 bp overlaps
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl4,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    
    peaks_ov <- counts_regions[which(!is.na(peaks_overlaps)),]
    
    
    # bigwig plotting
    {
      # Get transcrupts annotation to get HNF4A regions
      tssAnnot <- ELMER::getTSS(genome = "hg38")
      
      # Plot parameters, only to look better
      pp <- getDefaultPlotParams(plot.type = 1)
      pp$leftmargin <- 0.15
      pp$topmargin <- 5
      pp$bottommargin <- 5
      pp$ideogramheight <- 5
      pp$data1inmargin <- 10
      pp$data1outmargin <- 0
      
      # Start to plot bigwig files
      big.wig.files <- dir(path = "~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/KIRP bigwigs",
                           pattern = ".bw",
                           all.files = T,
                           full.names = T)
      # Reserve area to plot the bigwig files
      out.at <- autotrack(1:length(big.wig.files), 
                          length(big.wig.files), 
                          margin = 0.3, 
                          r0 = 0,
                          r1 = 1)
      
      setwd('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/KIRP bigwigs/plots/th_1/')
      threshold = 1
      
      for (rgn in 1:length(peaks_ov)){
        cat(rgn,'\n')
        tiff(paste0(peaks_ov[rgn]@seqnames,'__',peaks_ov[rgn]@ranges,'.tiff'),
             height = 10, width = 4, units = 'in', res=300)
        
        # plot will be at  the HNF4A range +- 10Kb
        plt.region <- range(c(peaks_ov[rgn])) + 10000
        
        # Start by plotting gene tracks
        kp <- plotKaryotype(zoom = plt.region,genome = "hg38", cex = 0.5, plot.params = pp)
        genes.data <- makeGenesDataFromTxDb(TxDb.Hsapiens.UCSC.hg38.knownGene,
                                            karyoplot = kp,
                                            plot.transcripts = TRUE, 
                                            plot.transcripts.structure = TRUE)
        genes.data <- addGeneNames(genes.data)
        
        
        
        genes.data <- mergeTranscripts(genes.data)
        
        kp <- plotKaryotype(zoom = plt.region,genome = "hg38", cex = 0.5, plot.params = pp)
        kpAddBaseNumbers(kp, tick.dist = 20000, minor.tick.dist = 5000,
                         add.units = TRUE, cex = 0.5, tick.len = 1)
        #if we would like to put genes under ATAC peaks
        # kpPlotGenes(kp, data = genes.data, r0 = 0, r1 = 0.25, gene.name.cex = 0.5)
        
        
        # Add ATAC-seq label from 0.3 to 1 which should cover all ATAC-seq tracks
        kpAddLabels(kp, 
                    labels = "ATAC-Seq", 
                    r0 = out.at$r0, 
                    r1 = out.at$r1, 
                    side = "left",
                    cex = 0.25,
                    srt = 90, 
                    pos = 3, 
                    label.margin = 0.1)
        
        
        for(i in seq_len(length(big.wig.files))) {
          bigwig.file <- big.wig.files[i]
          
          # just getting the name
          tec_rep_name <- strsplit(strsplit(bigwig.file,split = './')[[1]][9],split = '\\.')[[1]][1]
          cnt <- counts_m[which(!is.na(peaks_overlaps))[rgn],colnames(counts_m) %in% tec_rep_name]
          
          # adaptive threshold by using the score for each region
          # threshold <-counts_m[which(!is.na(peaks_overlaps))[rgn],5]
          
          # Define where the track will be ploted
          # autotrack will simple get the reserved space (from out.at$r0 up to out.at$r1)
          # and split in equal sizes for each bigwifile, i the index, will control which 
          # one is being plotted
          at <- autotrack(i, length(big.wig.files), r0 = out.at$r0, r1 = out.at$r1, margin = 0.2)
          
          
          # Plot bigwig
          kp <- kpPlotBigWig(kp, 
                             data = bigwig.file, 
                             ymax = "visible.region", 
                             r0 = at$r0, 
                             col = ifelse(cnt>threshold,"#0000FF","#FF0000"), 
                             r1 = at$r1)
          computed.ymax <- ceiling(kp$latest.plot$computed.values$ymax)
          
          # Add track axis
          kpAxis(kp, 
                 ymin = 0, 
                 ymax = 150, 
                 numticks = 2,
                 r0 = at$r0, 
                 r1 = at$r1,
                 cex = 0.15)
          
          # Add track label
          kpAddLabels(kp, 
                      labels = ifelse(cnt>threshold,"Peak","No Peak"),
                      r0 = at$r0, 
                      r1 = at$r1, 
                      cex = 0.25, 
                      label.margin = 0.01)
          
          
        }
        
        kpPlotRegions(kp, data=peaks_ov[rgn], col="#FFFCCC50", border="#00000044")
        kpPlotRegions(kp, data=peaks_ov[rgn], col="#FFFCCC50", border="#00000044")
        
        dev.off()
        
      }
      
    }
    
    
    
  }
  
  # Peak intersection for each cluster
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/KIRP_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    
    peak_m <- (counts_m[,(6:ncol(counts_m))] > 0.7)*1
    
    
    #Cluster1
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster1.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl1 <- ov_peaks
    
    
    
    #Cluster2
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster2.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl2 <- ov_peaks
    
    
    #Cluster3
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster3.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl3 <- ov_peaks
    
    
    #Cluster4
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl4 <- ov_peaks
    
    
    #Cluster5
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster5.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl5 <- ov_peaks
  }
  
  #plotting
  {
    n_cl1 <-  1157
    n_cl2 <-  741
    n_cl3 <-  919
    n_cl4 <-  476
    n_cl5 <-  424
    
    
    rt_cl1 <- colSums(ov_cl1) / n_cl1
    rt_cl2 <- colSums(ov_cl2) / n_cl2
    rt_cl3 <- colSums(ov_cl3) / n_cl3
    rt_cl4 <- colSums(ov_cl4) / n_cl4
    rt_cl5 <- colSums(ov_cl5) / n_cl5
    
    df <- data.frame(Cluster = c('Cluster 1','Cluster 2','Cluster 3','Cluster 4','Cluster 5'))
    df$Overlaps <- c(mean(rt_cl1),mean(rt_cl2),mean(rt_cl3),mean(rt_cl4),mean(rt_cl5))
    df$Ratio = df$Overlaps/sum(df$Overlaps)
    
    
    my.cols <- brewer.pal(n = 8, name = "Dark2")
    my.cols[4] <- my.cols[7]
    my.cols[5] <- my.cols[8]
    
    setwd('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/KIRP bigwigs/plots/')
    tiff("KIRP_ov_0.7.tiff",
         height = 5, width = 10, units = 'in', res=300)
    ggplot(data=df, aes(x=Cluster, y=Ratio, fill = Cluster)) +
      geom_bar(stat="identity")+
      theme_minimal() + labs(fill = "Percentages") + 
      theme(legend.title = element_text(size = 18),
            legend.text = element_text(size = 15)) +
      scale_fill_manual(values=my.cols) +
      ylim(c(0,max(df$Ratio + 0.1))) + ggtitle("KIRP") + 
      xlab("") + ylab("Ratio of Overlapped Peaks") +
      geom_text(aes(label=round(Ratio,digits = 3)), position=position_stack(), vjust=-0.5,size = 8) +
      theme(legend.position = "none",axis.text.x = element_text(face="bold", size=14))
    dev.off()
    
  }
  
  # Sample-wise cluster ditribution
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/KIRP_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    IDs <- colnames(counts_m)[6:ncol(counts_m)]
    samples <- unique(sapply(IDs, function(x) paste0(strsplit(x,split = '_')[[1]][1:8],collapse = '_') ))
    length(samples)
    peak_m <- (counts_m[,(6:ncol(counts_m))] > 0.6)*1
    colnames(peak_m) <- IDs
    lst_tech_rep <- lapply(samples, function(x) which(str_detect(IDs,x)))
    sample_peaks<- matrix(0,nrow(peak_m),length(lst_tech_rep))
    colnames(sample_peaks) <- samples
    for (i in 1:length(lst_tech_rep)){
      #checking if we have multiple technical replicates
      if(!is.null(dim(counts_m[,lst_tech_rep[[i]]]))){
        sample_peaks[,i] = (rowSums(peak_m[,lst_tech_rep[[i]]]) != 0) * 1
      }else{
        sample_peaks[,i] = peak_m[,lst_tech_rep[[i]]]
      }
    }
    # All analysis use peak_m, I didnot  want to rewrite all
    peak_m <- sample_peaks
    
    
    #Cluster1
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster1.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl1 <- ov_peaks
    
    #Cluster2
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster2.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl2 <- ov_peaks
    
    #Cluster3
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster3.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl3 <- ov_peaks
    
    
    #Cluster4
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl4 <- ov_peaks
    
    #Cluster5
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster5.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    
    ov_cl5 <- ov_peaks
    
    
    KIRP_sample_matrix <- matrix(0,length(samples),5)
    
    n_cl1 <-  1157
    n_cl2 <-  741
    n_cl3 <-  919
    n_cl4 <-  476
    n_cl5 <-  424
    
    rt_cl1 <- colSums(ov_cl1) / n_cl1
    rt_cl2 <- colSums(ov_cl2) / n_cl2
    rt_cl3 <- colSums(ov_cl3) / n_cl3
    rt_cl4 <- colSums(ov_cl4) / n_cl4
    rt_cl5 <- colSums(ov_cl5) / n_cl5
    all_rt <- rt_cl1 + rt_cl2 + rt_cl3 + rt_cl4 + rt_cl5
    
    KIRP_sample_matrix[,1] <- rt_cl1/all_rt
    KIRP_sample_matrix[,2] <- rt_cl2/all_rt
    KIRP_sample_matrix[,3] <- rt_cl3/all_rt
    KIRP_sample_matrix[,4] <- rt_cl4/all_rt
    KIRP_sample_matrix[,5] <- rt_cl5/all_rt
    
    rownames(KIRP_sample_matrix) <- samples
  }
  
}


# LGG - threshold = 2.5
{
  #Choosing the threshold 
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/LGG_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    
    
    cluster4 <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl4 <- GRanges(seq = cluster4$V1,IRanges(start = cluster4$V2,end = cluster4$V3))
    
    #1 bp overlaps
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl4,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    
    peaks_ov <- counts_regions[which(!is.na(peaks_overlaps)),]
    
    
    # bigwig plotting
    {
      # Get transcrupts annotation to get HNF4A regions
      tssAnnot <- ELMER::getTSS(genome = "hg38")
      
      # Plot parameters, only to look better
      pp <- getDefaultPlotParams(plot.type = 1)
      pp$leftmargin <- 0.15
      pp$topmargin <- 5
      pp$bottommargin <- 5
      pp$ideogramheight <- 5
      pp$data1inmargin <- 10
      pp$data1outmargin <- 0
      
      # Start to plot bigwig files
      big.wig.files <- dir(path = "~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/LGG bigwigs",
                           pattern = ".bw",
                           all.files = T,
                           full.names = T)
      # Reserve area to plot the bigwig files
      out.at <- autotrack(1:length(big.wig.files), 
                          length(big.wig.files), 
                          margin = 0.3, 
                          r0 = 0,
                          r1 = 1)
      
      setwd('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/LGG bigwigs/plots/th_2.5/')
      threshold = 2.5
      
      for (rgn in 1:length(peaks_ov)){
        cat(rgn,'\n')
        tiff(paste0(peaks_ov[rgn]@seqnames,'__',peaks_ov[rgn]@ranges,'.tiff'),
             height = 10, width = 4, units = 'in', res=300)
        
        # plot will be at  the HNF4A range +- 10Kb
        plt.region <- range(c(peaks_ov[rgn])) + 10000
        
        # Start by plotting gene tracks
        kp <- plotKaryotype(zoom = plt.region,genome = "hg38", cex = 0.5, plot.params = pp)
        genes.data <- makeGenesDataFromTxDb(TxDb.Hsapiens.UCSC.hg38.knownGene,
                                            karyoplot = kp,
                                            plot.transcripts = TRUE, 
                                            plot.transcripts.structure = TRUE)
        genes.data <- addGeneNames(genes.data)
        
        
        
        genes.data <- mergeTranscripts(genes.data)
        
        kp <- plotKaryotype(zoom = plt.region,genome = "hg38", cex = 0.5, plot.params = pp)
        kpAddBaseNumbers(kp, tick.dist = 20000, minor.tick.dist = 5000,
                         add.units = TRUE, cex = 0.5, tick.len = 1)
        #if we would like to put genes under ATAC peaks
        # kpPlotGenes(kp, data = genes.data, r0 = 0, r1 = 0.25, gene.name.cex = 0.5)
        
        
        # Add ATAC-seq label from 0.3 to 1 which should cover all ATAC-seq tracks
        kpAddLabels(kp, 
                    labels = "ATAC-Seq", 
                    r0 = out.at$r0, 
                    r1 = out.at$r1, 
                    side = "left",
                    cex = 0.25,
                    srt = 90, 
                    pos = 3, 
                    label.margin = 0.1)
        
        
        for(i in seq_len(length(big.wig.files))) {
          bigwig.file <- big.wig.files[i]
          
          # just getting the name
          tec_rep_name <- strsplit(strsplit(bigwig.file,split = './')[[1]][9],split = '\\.')[[1]][1]
          cnt <- counts_m[which(!is.na(peaks_overlaps))[rgn],colnames(counts_m) %in% tec_rep_name]
          
          # adaptive threshold by using the score for each region
          # threshold <-counts_m[which(!is.na(peaks_overlaps))[rgn],5]
          
          # Define where the track will be ploted
          # autotrack will simple get the reserved space (from out.at$r0 up to out.at$r1)
          # and split in equal sizes for each bigwifile, i the index, will control which 
          # one is being plotted
          at <- autotrack(i, length(big.wig.files), r0 = out.at$r0, r1 = out.at$r1, margin = 0.2)
          
          
          # Plot bigwig
          kp <- kpPlotBigWig(kp, 
                             data = bigwig.file, 
                             ymax = "visible.region", 
                             r0 = at$r0, 
                             col = ifelse(cnt>threshold,"#0000FF","#FF0000"), 
                             r1 = at$r1)
          computed.ymax <- ceiling(kp$latest.plot$computed.values$ymax)
          
          # Add track axis
          kpAxis(kp, 
                 ymin = 0, 
                 ymax = 150, 
                 numticks = 2,
                 r0 = at$r0, 
                 r1 = at$r1,
                 cex = 0.15)
          
          # Add track label
          kpAddLabels(kp, 
                      labels = ifelse(cnt>threshold,"Peak","No Peak"),
                      r0 = at$r0, 
                      r1 = at$r1, 
                      cex = 0.25, 
                      label.margin = 0.01)
          
          
        }
        
        kpPlotRegions(kp, data=peaks_ov[rgn], col="#FFFCCC50", border="#00000044")
        kpPlotRegions(kp, data=peaks_ov[rgn], col="#FFFCCC50", border="#00000044")
        
        dev.off()
        
      }
      
    }
    
    
    
  }
  
  # Peak intersection for each cluster
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/LGG_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    
    peak_m <- (counts_m[,(6:ncol(counts_m))] > 0.7)*1
    
    
    #Cluster1
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster1.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl1 <- ov_peaks
    
    
    
    #Cluster2
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster2.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl2 <- ov_peaks
    
    
    #Cluster3
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster3.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl3 <- ov_peaks
    
    
    #Cluster4
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl4 <- ov_peaks
    
    
    #Cluster5
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster5.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl5 <- ov_peaks
  }
  
  #plotting
  {
    n_cl1 <-  1157
    n_cl2 <-  741
    n_cl3 <-  919
    n_cl4 <-  476
    n_cl5 <-  424
    
    
    rt_cl1 <- colSums(ov_cl1) / n_cl1
    rt_cl2 <- colSums(ov_cl2) / n_cl2
    rt_cl3 <- colSums(ov_cl3) / n_cl3
    rt_cl4 <- colSums(ov_cl4) / n_cl4
    rt_cl5 <- colSums(ov_cl5) / n_cl5
    
    df <- data.frame(Cluster = c('Cluster 1','Cluster 2','Cluster 3','Cluster 4','Cluster 5'))
    df$Overlaps <- c(mean(rt_cl1),mean(rt_cl2),mean(rt_cl3),mean(rt_cl4),mean(rt_cl5))
    df$Ratio = df$Overlaps/sum(df$Overlaps)
    
    
    my.cols <- brewer.pal(n = 8, name = "Dark2")
    my.cols[4] <- my.cols[7]
    my.cols[5] <- my.cols[8]
    
    setwd('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/LGG bigwigs/plots/')
    tiff("LGG_ov_0.7.tiff",
         height = 5, width = 10, units = 'in', res=300)
    ggplot(data=df, aes(x=Cluster, y=Ratio, fill = Cluster)) +
      geom_bar(stat="identity")+
      theme_minimal() + labs(fill = "Percentages") + 
      theme(legend.title = element_text(size = 18),
            legend.text = element_text(size = 15)) +
      scale_fill_manual(values=my.cols) +
      ylim(c(0,max(df$Ratio + 0.1))) + ggtitle("LGG") + 
      xlab("") + ylab("Ratio of Overlapped Peaks") +
      geom_text(aes(label=round(Ratio,digits = 3)), position=position_stack(), vjust=-0.5,size = 8) +
      theme(legend.position = "none",axis.text.x = element_text(face="bold", size=14))
    dev.off()
    
  }
  
  # Sample-wise cluster ditribution
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/LGG_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    IDs <- colnames(counts_m)[6:ncol(counts_m)]
    samples <- unique(sapply(IDs, function(x) paste0(strsplit(x,split = '_')[[1]][1:8],collapse = '_') ))
    length(samples)
    peak_m <- (counts_m[,(6:ncol(counts_m))] > 2.5)*1
    colnames(peak_m) <- IDs
    lst_tech_rep <- lapply(samples, function(x) which(str_detect(IDs,x)))
    sample_peaks<- matrix(0,nrow(peak_m),length(lst_tech_rep))
    colnames(sample_peaks) <- samples
    for (i in 1:length(lst_tech_rep)){
      #checking if we have multiple technical replicates
      if(!is.null(dim(counts_m[,lst_tech_rep[[i]]]))){
        sample_peaks[,i] = (rowSums(peak_m[,lst_tech_rep[[i]]]) != 0) * 1
      }else{
        sample_peaks[,i] = peak_m[,lst_tech_rep[[i]]]
      }
    }
    # All analysis use peak_m, I didnot  want to rewrite all
    peak_m <- sample_peaks
    
    
    #Cluster1
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster1.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl1 <- ov_peaks
    
    #Cluster2
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster2.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl2 <- ov_peaks
    
    #Cluster3
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster3.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl3 <- ov_peaks
    
    
    #Cluster4
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl4 <- ov_peaks
    
    #Cluster5
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster5.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    
    ov_cl5 <- ov_peaks
    
    
    LGG_sample_matrix <- matrix(0,length(samples),5)
    
    n_cl1 <-  1157
    n_cl2 <-  741
    n_cl3 <-  919
    n_cl4 <-  476
    n_cl5 <-  424
    
    rt_cl1 <- colSums(ov_cl1) / n_cl1
    rt_cl2 <- colSums(ov_cl2) / n_cl2
    rt_cl3 <- colSums(ov_cl3) / n_cl3
    rt_cl4 <- colSums(ov_cl4) / n_cl4
    rt_cl5 <- colSums(ov_cl5) / n_cl5
    all_rt <- rt_cl1 + rt_cl2 + rt_cl3 + rt_cl4 + rt_cl5
    
    LGG_sample_matrix[,1] <- rt_cl1/all_rt
    LGG_sample_matrix[,2] <- rt_cl2/all_rt
    LGG_sample_matrix[,3] <- rt_cl3/all_rt
    LGG_sample_matrix[,4] <- rt_cl4/all_rt
    LGG_sample_matrix[,5] <- rt_cl5/all_rt
    
    rownames(LGG_sample_matrix) <- samples
  }
  
}


# LIHC - threshold = 1.1
{
  #Choosing the threshold 
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/LIHC_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    
    
    cluster4 <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl4 <- GRanges(seq = cluster4$V1,IRanges(start = cluster4$V2,end = cluster4$V3))
    
    #1 bp overlaps
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl4,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    
    peaks_ov <- counts_regions[which(!is.na(peaks_overlaps)),]
    
    
    # bigwig plotting
    {
      # Get transcrupts annotation to get HNF4A regions
      tssAnnot <- ELMER::getTSS(genome = "hg38")
      
      # Plot parameters, only to look better
      pp <- getDefaultPlotParams(plot.type = 1)
      pp$leftmargin <- 0.15
      pp$topmargin <- 5
      pp$bottommargin <- 5
      pp$ideogramheight <- 5
      pp$data1inmargin <- 10
      pp$data1outmargin <- 0
      
      # Start to plot bigwig files
      big.wig.files <- dir(path = "~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/LIHC bigwigs",
                           pattern = ".bw",
                           all.files = T,
                           full.names = T)
      # Reserve area to plot the bigwig files
      out.at <- autotrack(1:length(big.wig.files), 
                          length(big.wig.files), 
                          margin = 0.3, 
                          r0 = 0,
                          r1 = 1)
      
      setwd('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/LIHC bigwigs/plots/th_2/')
      threshold = 2
      
      for (rgn in 1:length(peaks_ov)){
        cat(rgn,'\n')
        tiff(paste0(peaks_ov[rgn]@seqnames,'__',peaks_ov[rgn]@ranges,'.tiff'),
             height = 10, width = 4, units = 'in', res=300)
        
        # plot will be at  the HNF4A range +- 10Kb
        plt.region <- range(c(peaks_ov[rgn])) + 10000
        
        # Start by plotting gene tracks
        kp <- plotKaryotype(zoom = plt.region,genome = "hg38", cex = 0.5, plot.params = pp)
        genes.data <- makeGenesDataFromTxDb(TxDb.Hsapiens.UCSC.hg38.knownGene,
                                            karyoplot = kp,
                                            plot.transcripts = TRUE, 
                                            plot.transcripts.structure = TRUE)
        genes.data <- addGeneNames(genes.data)
        
        
        
        genes.data <- mergeTranscripts(genes.data)
        
        kp <- plotKaryotype(zoom = plt.region,genome = "hg38", cex = 0.5, plot.params = pp)
        kpAddBaseNumbers(kp, tick.dist = 20000, minor.tick.dist = 5000,
                         add.units = TRUE, cex = 0.5, tick.len = 1)
        #if we would like to put genes under ATAC peaks
        # kpPlotGenes(kp, data = genes.data, r0 = 0, r1 = 0.25, gene.name.cex = 0.5)
        
        
        # Add ATAC-seq label from 0.3 to 1 which should cover all ATAC-seq tracks
        kpAddLabels(kp, 
                    labels = "ATAC-Seq", 
                    r0 = out.at$r0, 
                    r1 = out.at$r1, 
                    side = "left",
                    cex = 0.25,
                    srt = 90, 
                    pos = 3, 
                    label.margin = 0.1)
        
        
        for(i in seq_len(length(big.wig.files))) {
          bigwig.file <- big.wig.files[i]
          
          # just getting the name
          tec_rep_name <- strsplit(strsplit(bigwig.file,split = './')[[1]][9],split = '\\.')[[1]][1]
          cnt <- counts_m[which(!is.na(peaks_overlaps))[rgn],colnames(counts_m) %in% tec_rep_name]
          
          # adaptive threshold by using the score for each region
          # threshold <-counts_m[which(!is.na(peaks_overlaps))[rgn],5]
          
          # Define where the track will be ploted
          # autotrack will simple get the reserved space (from out.at$r0 up to out.at$r1)
          # and split in equal sizes for each bigwifile, i the index, will control which 
          # one is being plotted
          at <- autotrack(i, length(big.wig.files), r0 = out.at$r0, r1 = out.at$r1, margin = 0.2)
          
          
          # Plot bigwig
          kp <- kpPlotBigWig(kp, 
                             data = bigwig.file, 
                             ymax = "visible.region", 
                             r0 = at$r0, 
                             col = ifelse(cnt>threshold,"#0000FF","#FF0000"), 
                             r1 = at$r1)
          computed.ymax <- ceiling(kp$latest.plot$computed.values$ymax)
          
          # Add track axis
          kpAxis(kp, 
                 ymin = 0, 
                 ymax = 150, 
                 numticks = 2,
                 r0 = at$r0, 
                 r1 = at$r1,
                 cex = 0.15)
          
          # Add track label
          kpAddLabels(kp, 
                      labels = ifelse(cnt>threshold,"Peak","No Peak"),
                      r0 = at$r0, 
                      r1 = at$r1, 
                      cex = 0.25, 
                      label.margin = 0.01)
          
          
        }
        
        kpPlotRegions(kp, data=peaks_ov[rgn], col="#FFFCCC50", border="#00000044")
        kpPlotRegions(kp, data=peaks_ov[rgn], col="#FFFCCC50", border="#00000044")
        
        dev.off()
        
      }
      
    }
    
    
    
  }
  
  # Peak intersection for each cluster
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/LIHC_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    
    peak_m <- (counts_m[,(6:ncol(counts_m))] > 0.7)*1
    
    
    #Cluster1
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster1.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl1 <- ov_peaks
    
    
    
    #Cluster2
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster2.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl2 <- ov_peaks
    
    
    #Cluster3
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster3.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl3 <- ov_peaks
    
    
    #Cluster4
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl4 <- ov_peaks
    
    
    #Cluster5
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster5.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl5 <- ov_peaks
  }
  
  #plotting
  {
    n_cl1 <-  1157
    n_cl2 <-  741
    n_cl3 <-  919
    n_cl4 <-  476
    n_cl5 <-  424
    
    
    rt_cl1 <- colSums(ov_cl1) / n_cl1
    rt_cl2 <- colSums(ov_cl2) / n_cl2
    rt_cl3 <- colSums(ov_cl3) / n_cl3
    rt_cl4 <- colSums(ov_cl4) / n_cl4
    rt_cl5 <- colSums(ov_cl5) / n_cl5
    
    df <- data.frame(Cluster = c('Cluster 1','Cluster 2','Cluster 3','Cluster 4','Cluster 5'))
    df$Overlaps <- c(mean(rt_cl1),mean(rt_cl2),mean(rt_cl3),mean(rt_cl4),mean(rt_cl5))
    df$Ratio = df$Overlaps/sum(df$Overlaps)
    
    
    my.cols <- brewer.pal(n = 8, name = "Dark2")
    my.cols[4] <- my.cols[7]
    my.cols[5] <- my.cols[8]
    
    setwd('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/LIHC bigwigs/plots/')
    tiff("LIHC_ov_0.7.tiff",
         height = 5, width = 10, units = 'in', res=300)
    ggplot(data=df, aes(x=Cluster, y=Ratio, fill = Cluster)) +
      geom_bar(stat="identity")+
      theme_minimal() + labs(fill = "Percentages") + 
      theme(legend.title = element_text(size = 18),
            legend.text = element_text(size = 15)) +
      scale_fill_manual(values=my.cols) +
      ylim(c(0,max(df$Ratio + 0.1))) + ggtitle("LIHC") + 
      xlab("") + ylab("Ratio of Overlapped Peaks") +
      geom_text(aes(label=round(Ratio,digits = 3)), position=position_stack(), vjust=-0.5,size = 8) +
      theme(legend.position = "none",axis.text.x = element_text(face="bold", size=14))
    dev.off()
    
  }
  
  # Sample-wise cluster ditribution
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/LIHC_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    IDs <- colnames(counts_m)[6:ncol(counts_m)]
    samples <- unique(sapply(IDs, function(x) paste0(strsplit(x,split = '_')[[1]][1:8],collapse = '_') ))
    length(samples)
    peak_m <- (counts_m[,(6:ncol(counts_m))] > 1.1)*1
    colnames(peak_m) <- IDs
    lst_tech_rep <- lapply(samples, function(x) which(str_detect(IDs,x)))
    sample_peaks<- matrix(0,nrow(peak_m),length(lst_tech_rep))
    colnames(sample_peaks) <- samples
    for (i in 1:length(lst_tech_rep)){
      #checking if we have multiple technical replicates
      if(!is.null(dim(counts_m[,lst_tech_rep[[i]]]))){
        sample_peaks[,i] = (rowSums(peak_m[,lst_tech_rep[[i]]]) != 0) * 1
      }else{
        sample_peaks[,i] = peak_m[,lst_tech_rep[[i]]]
      }
    }
    # All analysis use peak_m, I didnot  want to rewrite all
    peak_m <- sample_peaks
    
    
    #Cluster1
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster1.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl1 <- ov_peaks
    
    #Cluster2
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster2.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl2 <- ov_peaks
    
    #Cluster3
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster3.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl3 <- ov_peaks
    
    
    #Cluster4
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl4 <- ov_peaks
    
    #Cluster5
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster5.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    
    ov_cl5 <- ov_peaks
    
    
    LIHC_sample_matrix <- matrix(0,length(samples),5)
    
    n_cl1 <-  1157
    n_cl2 <-  741
    n_cl3 <-  919
    n_cl4 <-  476
    n_cl5 <-  424
    
    rt_cl1 <- colSums(ov_cl1) / n_cl1
    rt_cl2 <- colSums(ov_cl2) / n_cl2
    rt_cl3 <- colSums(ov_cl3) / n_cl3
    rt_cl4 <- colSums(ov_cl4) / n_cl4
    rt_cl5 <- colSums(ov_cl5) / n_cl5
    all_rt <- rt_cl1 + rt_cl2 + rt_cl3 + rt_cl4 + rt_cl5
    
    LIHC_sample_matrix[,1] <- rt_cl1/all_rt
    LIHC_sample_matrix[,2] <- rt_cl2/all_rt
    LIHC_sample_matrix[,3] <- rt_cl3/all_rt
    LIHC_sample_matrix[,4] <- rt_cl4/all_rt
    LIHC_sample_matrix[,5] <- rt_cl5/all_rt
    
    rownames(LIHC_sample_matrix) <- samples
  }
  
}


# LUAD - threshold = 1.5
{
  #Choosing the threshold 
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/LUAD_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    
    
    cluster4 <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl4 <- GRanges(seq = cluster4$V1,IRanges(start = cluster4$V2,end = cluster4$V3))
    
    #1 bp overlaps
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl4,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    
    peaks_ov <- counts_regions[which(!is.na(peaks_overlaps)),]
    
    
    # bigwig plotting
    {
      # Get transcrupts annotation to get HNF4A regions
      tssAnnot <- ELMER::getTSS(genome = "hg38")
      
      # Plot parameters, only to look better
      pp <- getDefaultPlotParams(plot.type = 1)
      pp$leftmargin <- 0.15
      pp$topmargin <- 5
      pp$bottommargin <- 5
      pp$ideogramheight <- 5
      pp$data1inmargin <- 10
      pp$data1outmargin <- 0
      
      # Start to plot bigwig files
      big.wig.files <- dir(path = "~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/LUAD bigwigs",
                           pattern = ".bw",
                           all.files = T,
                           full.names = T)
      # Reserve area to plot the bigwig files
      out.at <- autotrack(1:length(big.wig.files), 
                          length(big.wig.files), 
                          margin = 0.3, 
                          r0 = 0,
                          r1 = 1)
      
      setwd('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/LUAD bigwigs/plots/th_2/')
      threshold = 2
      
      for (rgn in 1:length(peaks_ov)){
        cat(rgn,'\n')
        tiff(paste0(peaks_ov[rgn]@seqnames,'__',peaks_ov[rgn]@ranges,'.tiff'),
             height = 10, width = 4, units = 'in', res=300)
        
        # plot will be at  the HNF4A range +- 10Kb
        plt.region <- range(c(peaks_ov[rgn])) + 10000
        
        # Start by plotting gene tracks
        kp <- plotKaryotype(zoom = plt.region,genome = "hg38", cex = 0.5, plot.params = pp)
        genes.data <- makeGenesDataFromTxDb(TxDb.Hsapiens.UCSC.hg38.knownGene,
                                            karyoplot = kp,
                                            plot.transcripts = TRUE, 
                                            plot.transcripts.structure = TRUE)
        genes.data <- addGeneNames(genes.data)
        
        
        
        genes.data <- mergeTranscripts(genes.data)
        
        kp <- plotKaryotype(zoom = plt.region,genome = "hg38", cex = 0.5, plot.params = pp)
        kpAddBaseNumbers(kp, tick.dist = 20000, minor.tick.dist = 5000,
                         add.units = TRUE, cex = 0.5, tick.len = 1)
        #if we would like to put genes under ATAC peaks
        # kpPlotGenes(kp, data = genes.data, r0 = 0, r1 = 0.25, gene.name.cex = 0.5)
        
        
        # Add ATAC-seq label from 0.3 to 1 which should cover all ATAC-seq tracks
        kpAddLabels(kp, 
                    labels = "ATAC-Seq", 
                    r0 = out.at$r0, 
                    r1 = out.at$r1, 
                    side = "left",
                    cex = 0.25,
                    srt = 90, 
                    pos = 3, 
                    label.margin = 0.1)
        
        
        for(i in seq_len(length(big.wig.files))) {
          bigwig.file <- big.wig.files[i]
          
          # just getting the name
          tec_rep_name <- strsplit(strsplit(bigwig.file,split = './')[[1]][9],split = '\\.')[[1]][1]
          cnt <- counts_m[which(!is.na(peaks_overlaps))[rgn],colnames(counts_m) %in% tec_rep_name]
          
          # adaptive threshold by using the score for each region
          # threshold <-counts_m[which(!is.na(peaks_overlaps))[rgn],5]
          
          # Define where the track will be ploted
          # autotrack will simple get the reserved space (from out.at$r0 up to out.at$r1)
          # and split in equal sizes for each bigwifile, i the index, will control which 
          # one is being plotted
          at <- autotrack(i, length(big.wig.files), r0 = out.at$r0, r1 = out.at$r1, margin = 0.2)
          
          
          # Plot bigwig
          kp <- kpPlotBigWig(kp, 
                             data = bigwig.file, 
                             ymax = "visible.region", 
                             r0 = at$r0, 
                             col = ifelse(cnt>threshold,"#0000FF","#FF0000"), 
                             r1 = at$r1)
          computed.ymax <- ceiling(kp$latest.plot$computed.values$ymax)
          
          # Add track axis
          kpAxis(kp, 
                 ymin = 0, 
                 ymax = 150, 
                 numticks = 2,
                 r0 = at$r0, 
                 r1 = at$r1,
                 cex = 0.15)
          
          # Add track label
          kpAddLabels(kp, 
                      labels = ifelse(cnt>threshold,"Peak","No Peak"),
                      r0 = at$r0, 
                      r1 = at$r1, 
                      cex = 0.25, 
                      label.margin = 0.01)
          
          
        }
        
        kpPlotRegions(kp, data=peaks_ov[rgn], col="#FFFCCC50", border="#00000044")
        kpPlotRegions(kp, data=peaks_ov[rgn], col="#FFFCCC50", border="#00000044")
        
        dev.off()
        
      }
      
    }
    
    
    
  }
  
  # Peak intersection for each cluster
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/LUAD_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    
    peak_m <- (counts_m[,(6:ncol(counts_m))] > 0.7)*1
    
    
    #Cluster1
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster1.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl1 <- ov_peaks
    
    
    
    #Cluster2
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster2.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl2 <- ov_peaks
    
    
    #Cluster3
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster3.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl3 <- ov_peaks
    
    
    #Cluster4
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl4 <- ov_peaks
    
    
    #Cluster5
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster5.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl5 <- ov_peaks
  }
  
  #plotting
  {
    n_cl1 <-  1157
    n_cl2 <-  741
    n_cl3 <-  919
    n_cl4 <-  476
    n_cl5 <-  424
    
    
    rt_cl1 <- colSums(ov_cl1) / n_cl1
    rt_cl2 <- colSums(ov_cl2) / n_cl2
    rt_cl3 <- colSums(ov_cl3) / n_cl3
    rt_cl4 <- colSums(ov_cl4) / n_cl4
    rt_cl5 <- colSums(ov_cl5) / n_cl5
    
    df <- data.frame(Cluster = c('Cluster 1','Cluster 2','Cluster 3','Cluster 4','Cluster 5'))
    df$Overlaps <- c(mean(rt_cl1),mean(rt_cl2),mean(rt_cl3),mean(rt_cl4),mean(rt_cl5))
    df$Ratio = df$Overlaps/sum(df$Overlaps)
    
    
    my.cols <- brewer.pal(n = 8, name = "Dark2")
    my.cols[4] <- my.cols[7]
    my.cols[5] <- my.cols[8]
    
    setwd('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/LUAD bigwigs/plots/')
    tiff("LUAD_ov_0.7.tiff",
         height = 5, width = 10, units = 'in', res=300)
    ggplot(data=df, aes(x=Cluster, y=Ratio, fill = Cluster)) +
      geom_bar(stat="identity")+
      theme_minimal() + labs(fill = "Percentages") + 
      theme(legend.title = element_text(size = 18),
            legend.text = element_text(size = 15)) +
      scale_fill_manual(values=my.cols) +
      ylim(c(0,max(df$Ratio + 0.1))) + ggtitle("LUAD") + 
      xlab("") + ylab("Ratio of Overlapped Peaks") +
      geom_text(aes(label=round(Ratio,digits = 3)), position=position_stack(), vjust=-0.5,size = 8) +
      theme(legend.position = "none",axis.text.x = element_text(face="bold", size=14))
    dev.off()
    
  }
  
  # Sample-wise cluster ditribution
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/LUAD_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    IDs <- colnames(counts_m)[6:ncol(counts_m)]
    samples <- unique(sapply(IDs, function(x) paste0(strsplit(x,split = '_')[[1]][1:8],collapse = '_') ))
    length(samples)
    peak_m <- (counts_m[,(6:ncol(counts_m))] > 1.5)*1
    colnames(peak_m) <- IDs
    lst_tech_rep <- lapply(samples, function(x) which(str_detect(IDs,x)))
    sample_peaks<- matrix(0,nrow(peak_m),length(lst_tech_rep))
    colnames(sample_peaks) <- samples
    for (i in 1:length(lst_tech_rep)){
      #checking if we have multiple technical replicates
      if(!is.null(dim(counts_m[,lst_tech_rep[[i]]]))){
        sample_peaks[,i] = (rowSums(peak_m[,lst_tech_rep[[i]]]) != 0) * 1
      }else{
        sample_peaks[,i] = peak_m[,lst_tech_rep[[i]]]
      }
    }
    # All analysis use peak_m, I didnot  want to rewrite all
    peak_m <- sample_peaks
    
    
    #Cluster1
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster1.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl1 <- ov_peaks
    
    #Cluster2
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster2.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl2 <- ov_peaks
    
    #Cluster3
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster3.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl3 <- ov_peaks
    
    
    #Cluster4
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl4 <- ov_peaks
    
    #Cluster5
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster5.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    
    ov_cl5 <- ov_peaks
    
    
    LUAD_sample_matrix <- matrix(0,length(samples),5)
    
    n_cl1 <-  1157
    n_cl2 <-  741
    n_cl3 <-  919
    n_cl4 <-  476
    n_cl5 <-  424
    
    rt_cl1 <- colSums(ov_cl1) / n_cl1
    rt_cl2 <- colSums(ov_cl2) / n_cl2
    rt_cl3 <- colSums(ov_cl3) / n_cl3
    rt_cl4 <- colSums(ov_cl4) / n_cl4
    rt_cl5 <- colSums(ov_cl5) / n_cl5
    all_rt <- rt_cl1 + rt_cl2 + rt_cl3 + rt_cl4 + rt_cl5
    
    LUAD_sample_matrix[,1] <- rt_cl1/all_rt
    LUAD_sample_matrix[,2] <- rt_cl2/all_rt
    LUAD_sample_matrix[,3] <- rt_cl3/all_rt
    LUAD_sample_matrix[,4] <- rt_cl4/all_rt
    LUAD_sample_matrix[,5] <- rt_cl5/all_rt
    
    rownames(LUAD_sample_matrix) <- samples
  }
  
}


# LUSC - threshold = 1.9
{
  #Choosing the threshold 
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/LUSC_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    
    
    cluster4 <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl4 <- GRanges(seq = cluster4$V1,IRanges(start = cluster4$V2,end = cluster4$V3))
    
    #1 bp overlaps
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl4,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    
    peaks_ov <- counts_regions[which(!is.na(peaks_overlaps)),]
    
    
    # bigwig plotting
    {
      # Get transcrupts annotation to get HNF4A regions
      tssAnnot <- ELMER::getTSS(genome = "hg38")
      
      # Plot parameters, only to look better
      pp <- getDefaultPlotParams(plot.type = 1)
      pp$leftmargin <- 0.15
      pp$topmargin <- 5
      pp$bottommargin <- 5
      pp$ideogramheight <- 5
      pp$data1inmargin <- 10
      pp$data1outmargin <- 0
      
      # Start to plot bigwig files
      big.wig.files <- dir(path = "~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/LUSC bigwigs",
                           pattern = ".bw",
                           all.files = T,
                           full.names = T)
      # Reserve area to plot the bigwig files
      out.at <- autotrack(1:length(big.wig.files), 
                          length(big.wig.files), 
                          margin = 0.3, 
                          r0 = 0,
                          r1 = 1)
      
      setwd('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/LUSC bigwigs/plots/th_2/')
      threshold = 2
      
      for (rgn in 1:length(peaks_ov)){
        cat(rgn,'\n')
        tiff(paste0(peaks_ov[rgn]@seqnames,'__',peaks_ov[rgn]@ranges,'.tiff'),
             height = 10, width = 4, units = 'in', res=300)
        
        # plot will be at  the HNF4A range +- 10Kb
        plt.region <- range(c(peaks_ov[rgn])) + 10000
        
        # Start by plotting gene tracks
        kp <- plotKaryotype(zoom = plt.region,genome = "hg38", cex = 0.5, plot.params = pp)
        genes.data <- makeGenesDataFromTxDb(TxDb.Hsapiens.UCSC.hg38.knownGene,
                                            karyoplot = kp,
                                            plot.transcripts = TRUE, 
                                            plot.transcripts.structure = TRUE)
        genes.data <- addGeneNames(genes.data)
        
        
        
        genes.data <- mergeTranscripts(genes.data)
        
        kp <- plotKaryotype(zoom = plt.region,genome = "hg38", cex = 0.5, plot.params = pp)
        kpAddBaseNumbers(kp, tick.dist = 20000, minor.tick.dist = 5000,
                         add.units = TRUE, cex = 0.5, tick.len = 1)
        #if we would like to put genes under ATAC peaks
        # kpPlotGenes(kp, data = genes.data, r0 = 0, r1 = 0.25, gene.name.cex = 0.5)
        
        
        # Add ATAC-seq label from 0.3 to 1 which should cover all ATAC-seq tracks
        kpAddLabels(kp, 
                    labels = "ATAC-Seq", 
                    r0 = out.at$r0, 
                    r1 = out.at$r1, 
                    side = "left",
                    cex = 0.25,
                    srt = 90, 
                    pos = 3, 
                    label.margin = 0.1)
        
        
        for(i in seq_len(length(big.wig.files))) {
          bigwig.file <- big.wig.files[i]
          
          # just getting the name
          tec_rep_name <- strsplit(strsplit(bigwig.file,split = './')[[1]][9],split = '\\.')[[1]][1]
          cnt <- counts_m[which(!is.na(peaks_overlaps))[rgn],colnames(counts_m) %in% tec_rep_name]
          
          # adaptive threshold by using the score for each region
          # threshold <-counts_m[which(!is.na(peaks_overlaps))[rgn],5]
          
          # Define where the track will be ploted
          # autotrack will simple get the reserved space (from out.at$r0 up to out.at$r1)
          # and split in equal sizes for each bigwifile, i the index, will control which 
          # one is being plotted
          at <- autotrack(i, length(big.wig.files), r0 = out.at$r0, r1 = out.at$r1, margin = 0.2)
          
          
          # Plot bigwig
          kp <- kpPlotBigWig(kp, 
                             data = bigwig.file, 
                             ymax = "visible.region", 
                             r0 = at$r0, 
                             col = ifelse(cnt>threshold,"#0000FF","#FF0000"), 
                             r1 = at$r1)
          computed.ymax <- ceiling(kp$latest.plot$computed.values$ymax)
          
          # Add track axis
          kpAxis(kp, 
                 ymin = 0, 
                 ymax = 150, 
                 numticks = 2,
                 r0 = at$r0, 
                 r1 = at$r1,
                 cex = 0.15)
          
          # Add track label
          kpAddLabels(kp, 
                      labels = ifelse(cnt>threshold,"Peak","No Peak"),
                      r0 = at$r0, 
                      r1 = at$r1, 
                      cex = 0.25, 
                      label.margin = 0.01)
          
          
        }
        
        kpPlotRegions(kp, data=peaks_ov[rgn], col="#FFFCCC50", border="#00000044")
        kpPlotRegions(kp, data=peaks_ov[rgn], col="#FFFCCC50", border="#00000044")
        
        dev.off()
        
      }
      
    }
    
    
    
  }
  
  # Peak intersection for each cluster
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/LUSC_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    
    peak_m <- (counts_m[,(6:ncol(counts_m))] > 0.7)*1
    
    
    #Cluster1
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster1.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl1 <- ov_peaks
    
    
    
    #Cluster2
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster2.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl2 <- ov_peaks
    
    
    #Cluster3
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster3.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl3 <- ov_peaks
    
    
    #Cluster4
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl4 <- ov_peaks
    
    
    #Cluster5
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster5.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl5 <- ov_peaks
  }
  
  #plotting
  {
    n_cl1 <-  1157
    n_cl2 <-  741
    n_cl3 <-  919
    n_cl4 <-  476
    n_cl5 <-  424
    
    
    rt_cl1 <- colSums(ov_cl1) / n_cl1
    rt_cl2 <- colSums(ov_cl2) / n_cl2
    rt_cl3 <- colSums(ov_cl3) / n_cl3
    rt_cl4 <- colSums(ov_cl4) / n_cl4
    rt_cl5 <- colSums(ov_cl5) / n_cl5
    
    df <- data.frame(Cluster = c('Cluster 1','Cluster 2','Cluster 3','Cluster 4','Cluster 5'))
    df$Overlaps <- c(mean(rt_cl1),mean(rt_cl2),mean(rt_cl3),mean(rt_cl4),mean(rt_cl5))
    df$Ratio = df$Overlaps/sum(df$Overlaps)
    
    
    my.cols <- brewer.pal(n = 8, name = "Dark2")
    my.cols[4] <- my.cols[7]
    my.cols[5] <- my.cols[8]
    
    setwd('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/LUSC bigwigs/plots/')
    tiff("LUSC_ov_0.7.tiff",
         height = 5, width = 10, units = 'in', res=300)
    ggplot(data=df, aes(x=Cluster, y=Ratio, fill = Cluster)) +
      geom_bar(stat="identity")+
      theme_minimal() + labs(fill = "Percentages") + 
      theme(legend.title = element_text(size = 18),
            legend.text = element_text(size = 15)) +
      scale_fill_manual(values=my.cols) +
      ylim(c(0,max(df$Ratio + 0.1))) + ggtitle("LUSC") + 
      xlab("") + ylab("Ratio of Overlapped Peaks") +
      geom_text(aes(label=round(Ratio,digits = 3)), position=position_stack(), vjust=-0.5,size = 8) +
      theme(legend.position = "none",axis.text.x = element_text(face="bold", size=14))
    dev.off()
    
  }
  
  # Sample-wise cluster ditribution
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/LUSC_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    IDs <- colnames(counts_m)[6:ncol(counts_m)]
    samples <- unique(sapply(IDs, function(x) paste0(strsplit(x,split = '_')[[1]][1:8],collapse = '_') ))
    length(samples)
    peak_m <- (counts_m[,(6:ncol(counts_m))] > 1.9)*1
    colnames(peak_m) <- IDs
    lst_tech_rep <- lapply(samples, function(x) which(str_detect(IDs,x)))
    sample_peaks<- matrix(0,nrow(peak_m),length(lst_tech_rep))
    colnames(sample_peaks) <- samples
    for (i in 1:length(lst_tech_rep)){
      #checking if we have multiple technical replicates
      if(!is.null(dim(counts_m[,lst_tech_rep[[i]]]))){
        sample_peaks[,i] = (rowSums(peak_m[,lst_tech_rep[[i]]]) != 0) * 1
      }else{
        sample_peaks[,i] = peak_m[,lst_tech_rep[[i]]]
      }
    }
    # All analysis use peak_m, I didnot  want to rewrite all
    peak_m <- sample_peaks
    
    
    #Cluster1
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster1.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl1 <- ov_peaks
    
    #Cluster2
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster2.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl2 <- ov_peaks
    
    #Cluster3
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster3.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl3 <- ov_peaks
    
    
    #Cluster4
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl4 <- ov_peaks
    
    #Cluster5
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster5.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    
    ov_cl5 <- ov_peaks
    
    
    LUSC_sample_matrix <- matrix(0,length(samples),5)
    
    n_cl1 <-  1157
    n_cl2 <-  741
    n_cl3 <-  919
    n_cl4 <-  476
    n_cl5 <-  424
    
    rt_cl1 <- colSums(ov_cl1) / n_cl1
    rt_cl2 <- colSums(ov_cl2) / n_cl2
    rt_cl3 <- colSums(ov_cl3) / n_cl3
    rt_cl4 <- colSums(ov_cl4) / n_cl4
    rt_cl5 <- colSums(ov_cl5) / n_cl5
    all_rt <- rt_cl1 + rt_cl2 + rt_cl3 + rt_cl4 + rt_cl5
    
    LUSC_sample_matrix[,1] <- rt_cl1/all_rt
    LUSC_sample_matrix[,2] <- rt_cl2/all_rt
    LUSC_sample_matrix[,3] <- rt_cl3/all_rt
    LUSC_sample_matrix[,4] <- rt_cl4/all_rt
    LUSC_sample_matrix[,5] <- rt_cl5/all_rt
    
    rownames(LUSC_sample_matrix) <- samples
  }
  
}


# MESO - threshold = 2
{
  #Choosing the threshold 
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/MESO_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    
    
    cluster4 <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl4 <- GRanges(seq = cluster4$V1,IRanges(start = cluster4$V2,end = cluster4$V3))
    
    #1 bp overlaps
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl4,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    
    peaks_ov <- counts_regions[which(!is.na(peaks_overlaps)),]
    
    
    # bigwig plotting
    {
      # Get transcrupts annotation to get HNF4A regions
      tssAnnot <- ELMER::getTSS(genome = "hg38")
      
      # Plot parameters, only to look better
      pp <- getDefaultPlotParams(plot.type = 1)
      pp$leftmargin <- 0.15
      pp$topmargin <- 5
      pp$bottommargin <- 5
      pp$ideogramheight <- 5
      pp$data1inmargin <- 10
      pp$data1outmargin <- 0
      
      # Start to plot bigwig files
      big.wig.files <- dir(path = "~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/MESO bigwigs",
                           pattern = ".bw",
                           all.files = T,
                           full.names = T)
      # Reserve area to plot the bigwig files
      out.at <- autotrack(1:length(big.wig.files), 
                          length(big.wig.files), 
                          margin = 0.3, 
                          r0 = 0,
                          r1 = 1)
      
      setwd('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/MESO bigwigs/plots/th_2.5/')
      threshold = 2.5
      
      for (rgn in 1:length(peaks_ov)){
        cat(rgn,'\n')
        tiff(paste0(peaks_ov[rgn]@seqnames,'__',peaks_ov[rgn]@ranges,'.tiff'),
             height = 10, width = 4, units = 'in', res=300)
        
        # plot will be at  the HNF4A range +- 10Kb
        plt.region <- range(c(peaks_ov[rgn])) + 10000
        
        # Start by plotting gene tracks
        kp <- plotKaryotype(zoom = plt.region,genome = "hg38", cex = 0.5, plot.params = pp)
        genes.data <- makeGenesDataFromTxDb(TxDb.Hsapiens.UCSC.hg38.knownGene,
                                            karyoplot = kp,
                                            plot.transcripts = TRUE, 
                                            plot.transcripts.structure = TRUE)
        genes.data <- addGeneNames(genes.data)
        
        
        
        genes.data <- mergeTranscripts(genes.data)
        
        kp <- plotKaryotype(zoom = plt.region,genome = "hg38", cex = 0.5, plot.params = pp)
        kpAddBaseNumbers(kp, tick.dist = 20000, minor.tick.dist = 5000,
                         add.units = TRUE, cex = 0.5, tick.len = 1)
        #if we would like to put genes under ATAC peaks
        # kpPlotGenes(kp, data = genes.data, r0 = 0, r1 = 0.25, gene.name.cex = 0.5)
        
        
        # Add ATAC-seq label from 0.3 to 1 which should cover all ATAC-seq tracks
        kpAddLabels(kp, 
                    labels = "ATAC-Seq", 
                    r0 = out.at$r0, 
                    r1 = out.at$r1, 
                    side = "left",
                    cex = 0.25,
                    srt = 90, 
                    pos = 3, 
                    label.margin = 0.1)
        
        
        for(i in seq_len(length(big.wig.files))) {
          bigwig.file <- big.wig.files[i]
          
          # just getting the name
          tec_rep_name <- strsplit(strsplit(bigwig.file,split = './')[[1]][9],split = '\\.')[[1]][1]
          cnt <- counts_m[which(!is.na(peaks_overlaps))[rgn],colnames(counts_m) %in% tec_rep_name]
          
          # adaptive threshold by using the score for each region
          # threshold <-counts_m[which(!is.na(peaks_overlaps))[rgn],5]
          
          # Define where the track will be ploted
          # autotrack will simple get the reserved space (from out.at$r0 up to out.at$r1)
          # and split in equal sizes for each bigwifile, i the index, will control which 
          # one is being plotted
          at <- autotrack(i, length(big.wig.files), r0 = out.at$r0, r1 = out.at$r1, margin = 0.2)
          
          
          # Plot bigwig
          kp <- kpPlotBigWig(kp, 
                             data = bigwig.file, 
                             ymax = "visible.region", 
                             r0 = at$r0, 
                             col = ifelse(cnt>threshold,"#0000FF","#FF0000"), 
                             r1 = at$r1)
          computed.ymax <- ceiling(kp$latest.plot$computed.values$ymax)
          
          # Add track axis
          kpAxis(kp, 
                 ymin = 0, 
                 ymax = 150, 
                 numticks = 2,
                 r0 = at$r0, 
                 r1 = at$r1,
                 cex = 0.15)
          
          # Add track label
          kpAddLabels(kp, 
                      labels = ifelse(cnt>threshold,"Peak","No Peak"),
                      r0 = at$r0, 
                      r1 = at$r1, 
                      cex = 0.25, 
                      label.margin = 0.01)
          
          
        }
        
        kpPlotRegions(kp, data=peaks_ov[rgn], col="#FFFCCC50", border="#00000044")
        kpPlotRegions(kp, data=peaks_ov[rgn], col="#FFFCCC50", border="#00000044")
        
        dev.off()
        
      }
      
    }
    
    
    
  }
  
  # Peak intersection for each cluster
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/MESO_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    
    peak_m <- (counts_m[,(6:ncol(counts_m))] > 0.7)*1
    
    
    #Cluster1
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster1.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl1 <- ov_peaks
    
    
    
    #Cluster2
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster2.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl2 <- ov_peaks
    
    
    #Cluster3
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster3.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl3 <- ov_peaks
    
    
    #Cluster4
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl4 <- ov_peaks
    
    
    #Cluster5
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster5.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl5 <- ov_peaks
  }
  
  #plotting
  {
    n_cl1 <-  1157
    n_cl2 <-  741
    n_cl3 <-  919
    n_cl4 <-  476
    n_cl5 <-  424
    
    
    rt_cl1 <- colSums(ov_cl1) / n_cl1
    rt_cl2 <- colSums(ov_cl2) / n_cl2
    rt_cl3 <- colSums(ov_cl3) / n_cl3
    rt_cl4 <- colSums(ov_cl4) / n_cl4
    rt_cl5 <- colSums(ov_cl5) / n_cl5
    
    df <- data.frame(Cluster = c('Cluster 1','Cluster 2','Cluster 3','Cluster 4','Cluster 5'))
    df$Overlaps <- c(mean(rt_cl1),mean(rt_cl2),mean(rt_cl3),mean(rt_cl4),mean(rt_cl5))
    df$Ratio = df$Overlaps/sum(df$Overlaps)
    
    
    my.cols <- brewer.pal(n = 8, name = "Dark2")
    my.cols[4] <- my.cols[7]
    my.cols[5] <- my.cols[8]
    
    setwd('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/MESO bigwigs/plots/')
    tiff("MESO_ov_0.7.tiff",
         height = 5, width = 10, units = 'in', res=300)
    ggplot(data=df, aes(x=Cluster, y=Ratio, fill = Cluster)) +
      geom_bar(stat="identity")+
      theme_minimal() + labs(fill = "Percentages") + 
      theme(legend.title = element_text(size = 18),
            legend.text = element_text(size = 15)) +
      scale_fill_manual(values=my.cols) +
      ylim(c(0,max(df$Ratio + 0.1))) + ggtitle("MESO") + 
      xlab("") + ylab("Ratio of Overlapped Peaks") +
      geom_text(aes(label=round(Ratio,digits = 3)), position=position_stack(), vjust=-0.5,size = 8) +
      theme(legend.position = "none",axis.text.x = element_text(face="bold", size=14))
    dev.off()
    
  }
  
  # Sample-wise cluster ditribution
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/MESO_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    IDs <- colnames(counts_m)[6:ncol(counts_m)]
    samples <- unique(sapply(IDs, function(x) paste0(strsplit(x,split = '_')[[1]][1:8],collapse = '_') ))
    length(samples)
    peak_m <- (counts_m[,(6:ncol(counts_m))] > 2)*1
    colnames(peak_m) <- IDs
    lst_tech_rep <- lapply(samples, function(x) which(str_detect(IDs,x)))
    sample_peaks<- matrix(0,nrow(peak_m),length(lst_tech_rep))
    colnames(sample_peaks) <- samples
    for (i in 1:length(lst_tech_rep)){
      #checking if we have multiple technical replicates
      if(!is.null(dim(counts_m[,lst_tech_rep[[i]]]))){
        sample_peaks[,i] = (rowSums(peak_m[,lst_tech_rep[[i]]]) != 0) * 1
      }else{
        sample_peaks[,i] = peak_m[,lst_tech_rep[[i]]]
      }
    }
    # All analysis use peak_m, I didnot  want to rewrite all
    peak_m <- sample_peaks
    
    
    #Cluster1
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster1.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl1 <- ov_peaks
    
    #Cluster2
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster2.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl2 <- ov_peaks
    
    #Cluster3
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster3.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl3 <- ov_peaks
    
    
    #Cluster4
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl4 <- ov_peaks
    
    #Cluster5
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster5.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    
    ov_cl5 <- ov_peaks
    
    
    MESO_sample_matrix <- matrix(0,length(samples),5)
    
    n_cl1 <-  1157
    n_cl2 <-  741
    n_cl3 <-  919
    n_cl4 <-  476
    n_cl5 <-  424
    
    rt_cl1 <- colSums(ov_cl1) / n_cl1
    rt_cl2 <- colSums(ov_cl2) / n_cl2
    rt_cl3 <- colSums(ov_cl3) / n_cl3
    rt_cl4 <- colSums(ov_cl4) / n_cl4
    rt_cl5 <- colSums(ov_cl5) / n_cl5
    all_rt <- rt_cl1 + rt_cl2 + rt_cl3 + rt_cl4 + rt_cl5
    
    MESO_sample_matrix[,1] <- rt_cl1/all_rt
    MESO_sample_matrix[,2] <- rt_cl2/all_rt
    MESO_sample_matrix[,3] <- rt_cl3/all_rt
    MESO_sample_matrix[,4] <- rt_cl4/all_rt
    MESO_sample_matrix[,5] <- rt_cl5/all_rt
    
    rownames(MESO_sample_matrix) <- samples
  }
  
}


# PCPG - threshold = 2.1
{
  #Choosing the threshold 
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/PCPG_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    
    
    cluster4 <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl4 <- GRanges(seq = cluster4$V1,IRanges(start = cluster4$V2,end = cluster4$V3))
    
    #1 bp overlaps
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl4,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    
    peaks_ov <- counts_regions[which(!is.na(peaks_overlaps)),]
    
    
    # bigwig plotting
    {
      # Get transcrupts annotation to get HNF4A regions
      tssAnnot <- ELMER::getTSS(genome = "hg38")
      
      # Plot parameters, only to look better
      pp <- getDefaultPlotParams(plot.type = 1)
      pp$leftmargin <- 0.15
      pp$topmargin <- 5
      pp$bottommargin <- 5
      pp$ideogramheight <- 5
      pp$data1inmargin <- 10
      pp$data1outmargin <- 0
      
      # Start to plot bigwig files
      big.wig.files <- dir(path = "~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/PCPG bigwigs",
                           pattern = ".bw",
                           all.files = T,
                           full.names = T)
      # Reserve area to plot the bigwig files
      out.at <- autotrack(1:length(big.wig.files), 
                          length(big.wig.files), 
                          margin = 0.3, 
                          r0 = 0,
                          r1 = 1)
      
      setwd('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/PCPG bigwigs/plots/th_2/')
      threshold = 2
      
      for (rgn in 1:length(peaks_ov)){
        cat(rgn,'\n')
        tiff(paste0(peaks_ov[rgn]@seqnames,'__',peaks_ov[rgn]@ranges,'.tiff'),
             height = 10, width = 4, units = 'in', res=300)
        
        # plot will be at  the HNF4A range +- 10Kb
        plt.region <- range(c(peaks_ov[rgn])) + 10000
        
        # Start by plotting gene tracks
        kp <- plotKaryotype(zoom = plt.region,genome = "hg38", cex = 0.5, plot.params = pp)
        genes.data <- makeGenesDataFromTxDb(TxDb.Hsapiens.UCSC.hg38.knownGene,
                                            karyoplot = kp,
                                            plot.transcripts = TRUE, 
                                            plot.transcripts.structure = TRUE)
        genes.data <- addGeneNames(genes.data)
        
        
        
        genes.data <- mergeTranscripts(genes.data)
        
        kp <- plotKaryotype(zoom = plt.region,genome = "hg38", cex = 0.5, plot.params = pp)
        kpAddBaseNumbers(kp, tick.dist = 20000, minor.tick.dist = 5000,
                         add.units = TRUE, cex = 0.5, tick.len = 1)
        #if we would like to put genes under ATAC peaks
        # kpPlotGenes(kp, data = genes.data, r0 = 0, r1 = 0.25, gene.name.cex = 0.5)
        
        
        # Add ATAC-seq label from 0.3 to 1 which should cover all ATAC-seq tracks
        kpAddLabels(kp, 
                    labels = "ATAC-Seq", 
                    r0 = out.at$r0, 
                    r1 = out.at$r1, 
                    side = "left",
                    cex = 0.25,
                    srt = 90, 
                    pos = 3, 
                    label.margin = 0.1)
        
        
        for(i in seq_len(length(big.wig.files))) {
          bigwig.file <- big.wig.files[i]
          
          # just getting the name
          tec_rep_name <- strsplit(strsplit(bigwig.file,split = './')[[1]][9],split = '\\.')[[1]][1]
          cnt <- counts_m[which(!is.na(peaks_overlaps))[rgn],colnames(counts_m) %in% tec_rep_name]
          
          # adaptive threshold by using the score for each region
          # threshold <-counts_m[which(!is.na(peaks_overlaps))[rgn],5]
          
          # Define where the track will be ploted
          # autotrack will simple get the reserved space (from out.at$r0 up to out.at$r1)
          # and split in equal sizes for each bigwifile, i the index, will control which 
          # one is being plotted
          at <- autotrack(i, length(big.wig.files), r0 = out.at$r0, r1 = out.at$r1, margin = 0.2)
          
          
          # Plot bigwig
          kp <- kpPlotBigWig(kp, 
                             data = bigwig.file, 
                             ymax = "visible.region", 
                             r0 = at$r0, 
                             col = ifelse(cnt>threshold,"#0000FF","#FF0000"), 
                             r1 = at$r1)
          computed.ymax <- ceiling(kp$latest.plot$computed.values$ymax)
          
          # Add track axis
          kpAxis(kp, 
                 ymin = 0, 
                 ymax = 150, 
                 numticks = 2,
                 r0 = at$r0, 
                 r1 = at$r1,
                 cex = 0.15)
          
          # Add track label
          kpAddLabels(kp, 
                      labels = ifelse(cnt>threshold,"Peak","No Peak"),
                      r0 = at$r0, 
                      r1 = at$r1, 
                      cex = 0.25, 
                      label.margin = 0.01)
          
          
        }
        
        kpPlotRegions(kp, data=peaks_ov[rgn], col="#FFFCCC50", border="#00000044")
        kpPlotRegions(kp, data=peaks_ov[rgn], col="#FFFCCC50", border="#00000044")
        
        dev.off()
        
      }
      
    }
    
    
    
  }
  
  # Peak intersection for each cluster
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/PCPG_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    
    peak_m <- (counts_m[,(6:ncol(counts_m))] > 0.7)*1
    
    
    #Cluster1
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster1.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl1 <- ov_peaks
    
    
    
    #Cluster2
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster2.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl2 <- ov_peaks
    
    
    #Cluster3
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster3.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl3 <- ov_peaks
    
    
    #Cluster4
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl4 <- ov_peaks
    
    
    #Cluster5
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster5.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl5 <- ov_peaks
  }
  
  #plotting
  {
    n_cl1 <-  1157
    n_cl2 <-  741
    n_cl3 <-  919
    n_cl4 <-  476
    n_cl5 <-  424
    
    
    rt_cl1 <- colSums(ov_cl1) / n_cl1
    rt_cl2 <- colSums(ov_cl2) / n_cl2
    rt_cl3 <- colSums(ov_cl3) / n_cl3
    rt_cl4 <- colSums(ov_cl4) / n_cl4
    rt_cl5 <- colSums(ov_cl5) / n_cl5
    
    df <- data.frame(Cluster = c('Cluster 1','Cluster 2','Cluster 3','Cluster 4','Cluster 5'))
    df$Overlaps <- c(mean(rt_cl1),mean(rt_cl2),mean(rt_cl3),mean(rt_cl4),mean(rt_cl5))
    df$Ratio = df$Overlaps/sum(df$Overlaps)
    
    
    my.cols <- brewer.pal(n = 8, name = "Dark2")
    my.cols[4] <- my.cols[7]
    my.cols[5] <- my.cols[8]
    
    setwd('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/PCPG bigwigs/plots/')
    tiff("PCPG_ov_0.7.tiff",
         height = 5, width = 10, units = 'in', res=300)
    ggplot(data=df, aes(x=Cluster, y=Ratio, fill = Cluster)) +
      geom_bar(stat="identity")+
      theme_minimal() + labs(fill = "Percentages") + 
      theme(legend.title = element_text(size = 18),
            legend.text = element_text(size = 15)) +
      scale_fill_manual(values=my.cols) +
      ylim(c(0,max(df$Ratio + 0.1))) + ggtitle("PCPG") + 
      xlab("") + ylab("Ratio of Overlapped Peaks") +
      geom_text(aes(label=round(Ratio,digits = 3)), position=position_stack(), vjust=-0.5,size = 8) +
      theme(legend.position = "none",axis.text.x = element_text(face="bold", size=14))
    dev.off()
    
  }
  
  # Sample-wise cluster ditribution
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/PCPG_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    IDs <- colnames(counts_m)[6:ncol(counts_m)]
    samples <- unique(sapply(IDs, function(x) paste0(strsplit(x,split = '_')[[1]][1:8],collapse = '_') ))
    length(samples)
    peak_m <- (counts_m[,(6:ncol(counts_m))] > 2.1)*1
    colnames(peak_m) <- IDs
    lst_tech_rep <- lapply(samples, function(x) which(str_detect(IDs,x)))
    sample_peaks<- matrix(0,nrow(peak_m),length(lst_tech_rep))
    colnames(sample_peaks) <- samples
    for (i in 1:length(lst_tech_rep)){
      #checking if we have multiple technical replicates
      if(!is.null(dim(counts_m[,lst_tech_rep[[i]]]))){
        sample_peaks[,i] = (rowSums(peak_m[,lst_tech_rep[[i]]]) != 0) * 1
      }else{
        sample_peaks[,i] = peak_m[,lst_tech_rep[[i]]]
      }
    }
    # All analysis use peak_m, I didnot  want to rewrite all
    peak_m <- sample_peaks
    
    
    #Cluster1
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster1.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl1 <- ov_peaks
    
    #Cluster2
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster2.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl2 <- ov_peaks
    
    #Cluster3
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster3.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl3 <- ov_peaks
    
    
    #Cluster4
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl4 <- ov_peaks
    
    #Cluster5
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster5.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    
    ov_cl5 <- ov_peaks
    
    
    PCPG_sample_matrix <- matrix(0,length(samples),5)
    
    n_cl1 <-  1157
    n_cl2 <-  741
    n_cl3 <-  919
    n_cl4 <-  476
    n_cl5 <-  424
    
    rt_cl1 <- colSums(ov_cl1) / n_cl1
    rt_cl2 <- colSums(ov_cl2) / n_cl2
    rt_cl3 <- colSums(ov_cl3) / n_cl3
    rt_cl4 <- colSums(ov_cl4) / n_cl4
    rt_cl5 <- colSums(ov_cl5) / n_cl5
    all_rt <- rt_cl1 + rt_cl2 + rt_cl3 + rt_cl4 + rt_cl5
    
    PCPG_sample_matrix[,1] <- rt_cl1/all_rt
    PCPG_sample_matrix[,2] <- rt_cl2/all_rt
    PCPG_sample_matrix[,3] <- rt_cl3/all_rt
    PCPG_sample_matrix[,4] <- rt_cl4/all_rt
    PCPG_sample_matrix[,5] <- rt_cl5/all_rt
    
    rownames(PCPG_sample_matrix) <- samples
  }
  
}


# PRAD - threshold = 1.9
{
  #Choosing the threshold 
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/PRAD_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    
    
    cluster4 <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl4 <- GRanges(seq = cluster4$V1,IRanges(start = cluster4$V2,end = cluster4$V3))
    
    #1 bp overlaps
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl4,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    
    peaks_ov <- counts_regions[which(!is.na(peaks_overlaps)),]
    
    
    # bigwig plotting
    {
      # Get transcrupts annotation to get HNF4A regions
      tssAnnot <- ELMER::getTSS(genome = "hg38")
      
      # Plot parameters, only to look better
      pp <- getDefaultPlotParams(plot.type = 1)
      pp$leftmargin <- 0.15
      pp$topmargin <- 5
      pp$bottommargin <- 5
      pp$ideogramheight <- 5
      pp$data1inmargin <- 10
      pp$data1outmargin <- 0
      
      # Start to plot bigwig files
      big.wig.files <- dir(path = "~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/PRAD bigwigs",
                           pattern = ".bw",
                           all.files = T,
                           full.names = T)
      # Reserve area to plot the bigwig files
      out.at <- autotrack(1:length(big.wig.files), 
                          length(big.wig.files), 
                          margin = 0.3, 
                          r0 = 0,
                          r1 = 1)
      
      setwd('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/PRAD bigwigs/plots/th_2.5/')
      threshold = 2.5
      
      for (rgn in 1:length(peaks_ov)){
        cat(rgn,'\n')
        tiff(paste0(peaks_ov[rgn]@seqnames,'__',peaks_ov[rgn]@ranges,'.tiff'),
             height = 10, width = 4, units = 'in', res=300)
        
        # plot will be at  the HNF4A range +- 10Kb
        plt.region <- range(c(peaks_ov[rgn])) + 10000
        
        # Start by plotting gene tracks
        kp <- plotKaryotype(zoom = plt.region,genome = "hg38", cex = 0.5, plot.params = pp)
        genes.data <- makeGenesDataFromTxDb(TxDb.Hsapiens.UCSC.hg38.knownGene,
                                            karyoplot = kp,
                                            plot.transcripts = TRUE, 
                                            plot.transcripts.structure = TRUE)
        genes.data <- addGeneNames(genes.data)
        
        
        
        genes.data <- mergeTranscripts(genes.data)
        
        kp <- plotKaryotype(zoom = plt.region,genome = "hg38", cex = 0.5, plot.params = pp)
        kpAddBaseNumbers(kp, tick.dist = 20000, minor.tick.dist = 5000,
                         add.units = TRUE, cex = 0.5, tick.len = 1)
        #if we would like to put genes under ATAC peaks
        # kpPlotGenes(kp, data = genes.data, r0 = 0, r1 = 0.25, gene.name.cex = 0.5)
        
        
        # Add ATAC-seq label from 0.3 to 1 which should cover all ATAC-seq tracks
        kpAddLabels(kp, 
                    labels = "ATAC-Seq", 
                    r0 = out.at$r0, 
                    r1 = out.at$r1, 
                    side = "left",
                    cex = 0.25,
                    srt = 90, 
                    pos = 3, 
                    label.margin = 0.1)
        
        
        for(i in seq_len(length(big.wig.files))) {
          bigwig.file <- big.wig.files[i]
          
          # just getting the name
          tec_rep_name <- strsplit(strsplit(bigwig.file,split = './')[[1]][9],split = '\\.')[[1]][1]
          cnt <- counts_m[which(!is.na(peaks_overlaps))[rgn],colnames(counts_m) %in% tec_rep_name]
          
          # adaptive threshold by using the score for each region
          # threshold <-counts_m[which(!is.na(peaks_overlaps))[rgn],5]
          
          # Define where the track will be ploted
          # autotrack will simple get the reserved space (from out.at$r0 up to out.at$r1)
          # and split in equal sizes for each bigwifile, i the index, will control which 
          # one is being plotted
          at <- autotrack(i, length(big.wig.files), r0 = out.at$r0, r1 = out.at$r1, margin = 0.2)
          
          
          # Plot bigwig
          kp <- kpPlotBigWig(kp, 
                             data = bigwig.file, 
                             ymax = "visible.region", 
                             r0 = at$r0, 
                             col = ifelse(cnt>threshold,"#0000FF","#FF0000"), 
                             r1 = at$r1)
          computed.ymax <- ceiling(kp$latest.plot$computed.values$ymax)
          
          # Add track axis
          kpAxis(kp, 
                 ymin = 0, 
                 ymax = 150, 
                 numticks = 2,
                 r0 = at$r0, 
                 r1 = at$r1,
                 cex = 0.15)
          
          # Add track label
          kpAddLabels(kp, 
                      labels = ifelse(cnt>threshold,"Peak","No Peak"),
                      r0 = at$r0, 
                      r1 = at$r1, 
                      cex = 0.25, 
                      label.margin = 0.01)
          
          
        }
        
        kpPlotRegions(kp, data=peaks_ov[rgn], col="#FFFCCC50", border="#00000044")
        kpPlotRegions(kp, data=peaks_ov[rgn], col="#FFFCCC50", border="#00000044")
        
        dev.off()
        
      }
      
    }
    
    
    
  }
  
  # Peak intersection for each cluster
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/PRAD_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    
    peak_m <- (counts_m[,(6:ncol(counts_m))] > 0.7)*1
    
    
    #Cluster1
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster1.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl1 <- ov_peaks
    
    
    
    #Cluster2
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster2.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl2 <- ov_peaks
    
    
    #Cluster3
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster3.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl3 <- ov_peaks
    
    
    #Cluster4
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl4 <- ov_peaks
    
    
    #Cluster5
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster5.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl5 <- ov_peaks
  }
  
  #plotting
  {
    n_cl1 <-  1157
    n_cl2 <-  741
    n_cl3 <-  919
    n_cl4 <-  476
    n_cl5 <-  424
    
    
    rt_cl1 <- colSums(ov_cl1) / n_cl1
    rt_cl2 <- colSums(ov_cl2) / n_cl2
    rt_cl3 <- colSums(ov_cl3) / n_cl3
    rt_cl4 <- colSums(ov_cl4) / n_cl4
    rt_cl5 <- colSums(ov_cl5) / n_cl5
    
    df <- data.frame(Cluster = c('Cluster 1','Cluster 2','Cluster 3','Cluster 4','Cluster 5'))
    df$Overlaps <- c(mean(rt_cl1),mean(rt_cl2),mean(rt_cl3),mean(rt_cl4),mean(rt_cl5))
    df$Ratio = df$Overlaps/sum(df$Overlaps)
    
    
    my.cols <- brewer.pal(n = 8, name = "Dark2")
    my.cols[4] <- my.cols[7]
    my.cols[5] <- my.cols[8]
    
    setwd('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/PRAD bigwigs/plots/')
    tiff("PRAD_ov_0.7.tiff",
         height = 5, width = 10, units = 'in', res=300)
    ggplot(data=df, aes(x=Cluster, y=Ratio, fill = Cluster)) +
      geom_bar(stat="identity")+
      theme_minimal() + labs(fill = "Percentages") + 
      theme(legend.title = element_text(size = 18),
            legend.text = element_text(size = 15)) +
      scale_fill_manual(values=my.cols) +
      ylim(c(0,max(df$Ratio + 0.1))) + ggtitle("PRAD") + 
      xlab("") + ylab("Ratio of Overlapped Peaks") +
      geom_text(aes(label=round(Ratio,digits = 3)), position=position_stack(), vjust=-0.5,size = 8) +
      theme(legend.position = "none",axis.text.x = element_text(face="bold", size=14))
    dev.off()
    
  }
  
  # Sample-wise cluster ditribution
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/PRAD_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    IDs <- colnames(counts_m)[6:ncol(counts_m)]
    samples <- unique(sapply(IDs, function(x) paste0(strsplit(x,split = '_')[[1]][1:8],collapse = '_') ))
    length(samples)
    peak_m <- (counts_m[,(6:ncol(counts_m))] > 1.9)*1
    colnames(peak_m) <- IDs
    lst_tech_rep <- lapply(samples, function(x) which(str_detect(IDs,x)))
    sample_peaks<- matrix(0,nrow(peak_m),length(lst_tech_rep))
    colnames(sample_peaks) <- samples
    for (i in 1:length(lst_tech_rep)){
      #checking if we have multiple technical replicates
      if(!is.null(dim(counts_m[,lst_tech_rep[[i]]]))){
        sample_peaks[,i] = (rowSums(peak_m[,lst_tech_rep[[i]]]) != 0) * 1
      }else{
        sample_peaks[,i] = peak_m[,lst_tech_rep[[i]]]
      }
    }
    # All analysis use peak_m, I didnot  want to rewrite all
    peak_m <- sample_peaks
    
    
    #Cluster1
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster1.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl1 <- ov_peaks
    
    #Cluster2
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster2.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl2 <- ov_peaks
    
    #Cluster3
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster3.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl3 <- ov_peaks
    
    
    #Cluster4
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl4 <- ov_peaks
    
    #Cluster5
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster5.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    
    ov_cl5 <- ov_peaks
    
    
    PRAD_sample_matrix <- matrix(0,length(samples),5)
    
    n_cl1 <-  1157
    n_cl2 <-  741
    n_cl3 <-  919
    n_cl4 <-  476
    n_cl5 <-  424
    
    rt_cl1 <- colSums(ov_cl1) / n_cl1
    rt_cl2 <- colSums(ov_cl2) / n_cl2
    rt_cl3 <- colSums(ov_cl3) / n_cl3
    rt_cl4 <- colSums(ov_cl4) / n_cl4
    rt_cl5 <- colSums(ov_cl5) / n_cl5
    all_rt <- rt_cl1 + rt_cl2 + rt_cl3 + rt_cl4 + rt_cl5
    
    PRAD_sample_matrix[,1] <- rt_cl1/all_rt
    PRAD_sample_matrix[,2] <- rt_cl2/all_rt
    PRAD_sample_matrix[,3] <- rt_cl3/all_rt
    PRAD_sample_matrix[,4] <- rt_cl4/all_rt
    PRAD_sample_matrix[,5] <- rt_cl5/all_rt
    
    rownames(PRAD_sample_matrix) <- samples
  }
  
}


# SKCM - threshold = 2
{
  #Choosing the threshold 
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/SKCM_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    
    
    cluster4 <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl4 <- GRanges(seq = cluster4$V1,IRanges(start = cluster4$V2,end = cluster4$V3))
    
    #1 bp overlaps
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl4,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    
    peaks_ov <- counts_regions[which(!is.na(peaks_overlaps)),]
    
    
    # bigwig plotting
    {
      # Get transcrupts annotation to get HNF4A regions
      tssAnnot <- ELMER::getTSS(genome = "hg38")
      
      # Plot parameters, only to look better
      pp <- getDefaultPlotParams(plot.type = 1)
      pp$leftmargin <- 0.15
      pp$topmargin <- 5
      pp$bottommargin <- 5
      pp$ideogramheight <- 5
      pp$data1inmargin <- 10
      pp$data1outmargin <- 0
      
      # Start to plot bigwig files
      big.wig.files <- dir(path = "~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/SKCM bigwigs",
                           pattern = ".bw",
                           all.files = T,
                           full.names = T)
      # Reserve area to plot the bigwig files
      out.at <- autotrack(1:length(big.wig.files), 
                          length(big.wig.files), 
                          margin = 0.3, 
                          r0 = 0,
                          r1 = 1)
      
      setwd('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/SKCM bigwigs/plots/th_0.8/')
      threshold = 0.8
      
      for (rgn in 1:length(peaks_ov)){
        cat(rgn,'\n')
        tiff(paste0(peaks_ov[rgn]@seqnames,'__',peaks_ov[rgn]@ranges,'.tiff'),
             height = 10, width = 4, units = 'in', res=300)
        
        # plot will be at  the HNF4A range +- 10Kb
        plt.region <- range(c(peaks_ov[rgn])) + 10000
        
        # Start by plotting gene tracks
        kp <- plotKaryotype(zoom = plt.region,genome = "hg38", cex = 0.5, plot.params = pp)
        genes.data <- makeGenesDataFromTxDb(TxDb.Hsapiens.UCSC.hg38.knownGene,
                                            karyoplot = kp,
                                            plot.transcripts = TRUE, 
                                            plot.transcripts.structure = TRUE)
        genes.data <- addGeneNames(genes.data)
        
        
        
        genes.data <- mergeTranscripts(genes.data)
        
        kp <- plotKaryotype(zoom = plt.region,genome = "hg38", cex = 0.5, plot.params = pp)
        kpAddBaseNumbers(kp, tick.dist = 20000, minor.tick.dist = 5000,
                         add.units = TRUE, cex = 0.5, tick.len = 1)
        #if we would like to put genes under ATAC peaks
        # kpPlotGenes(kp, data = genes.data, r0 = 0, r1 = 0.25, gene.name.cex = 0.5)
        
        
        # Add ATAC-seq label from 0.3 to 1 which should cover all ATAC-seq tracks
        kpAddLabels(kp, 
                    labels = "ATAC-Seq", 
                    r0 = out.at$r0, 
                    r1 = out.at$r1, 
                    side = "left",
                    cex = 0.25,
                    srt = 90, 
                    pos = 3, 
                    label.margin = 0.1)
        
        
        for(i in seq_len(length(big.wig.files))) {
          bigwig.file <- big.wig.files[i]
          
          # just getting the name
          tec_rep_name <- strsplit(strsplit(bigwig.file,split = './')[[1]][9],split = '\\.')[[1]][1]
          cnt <- counts_m[which(!is.na(peaks_overlaps))[rgn],colnames(counts_m) %in% tec_rep_name]
          
          # adaptive threshold by using the score for each region
          # threshold <-counts_m[which(!is.na(peaks_overlaps))[rgn],5]
          
          # Define where the track will be ploted
          # autotrack will simple get the reserved space (from out.at$r0 up to out.at$r1)
          # and split in equal sizes for each bigwifile, i the index, will control which 
          # one is being plotted
          at <- autotrack(i, length(big.wig.files), r0 = out.at$r0, r1 = out.at$r1, margin = 0.2)
          
          
          # Plot bigwig
          kp <- kpPlotBigWig(kp, 
                             data = bigwig.file, 
                             ymax = "visible.region", 
                             r0 = at$r0, 
                             col = ifelse(cnt>threshold,"#0000FF","#FF0000"), 
                             r1 = at$r1)
          computed.ymax <- ceiling(kp$latest.plot$computed.values$ymax)
          
          # Add track axis
          kpAxis(kp, 
                 ymin = 0, 
                 ymax = 150, 
                 numticks = 2,
                 r0 = at$r0, 
                 r1 = at$r1,
                 cex = 0.15)
          
          # Add track label
          kpAddLabels(kp, 
                      labels = ifelse(cnt>threshold,"Peak","No Peak"),
                      r0 = at$r0, 
                      r1 = at$r1, 
                      cex = 0.25, 
                      label.margin = 0.01)
          
          
        }
        
        kpPlotRegions(kp, data=peaks_ov[rgn], col="#FFFCCC50", border="#00000044")
        kpPlotRegions(kp, data=peaks_ov[rgn], col="#FFFCCC50", border="#00000044")
        
        dev.off()
        
      }
      
    }
    
    
    
  }
  
  # Peak intersection for each cluster
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/SKCM_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    
    peak_m <- (counts_m[,(6:ncol(counts_m))] > 0.7)*1
    
    
    #Cluster1
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster1.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl1 <- ov_peaks
    
    
    
    #Cluster2
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster2.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl2 <- ov_peaks
    
    
    #Cluster3
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster3.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl3 <- ov_peaks
    
    
    #Cluster4
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl4 <- ov_peaks
    
    
    #Cluster5
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster5.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl5 <- ov_peaks
  }
  
  #plotting
  {
    n_cl1 <-  1157
    n_cl2 <-  741
    n_cl3 <-  919
    n_cl4 <-  476
    n_cl5 <-  424
    
    
    rt_cl1 <- colSums(ov_cl1) / n_cl1
    rt_cl2 <- colSums(ov_cl2) / n_cl2
    rt_cl3 <- colSums(ov_cl3) / n_cl3
    rt_cl4 <- colSums(ov_cl4) / n_cl4
    rt_cl5 <- colSums(ov_cl5) / n_cl5
    
    df <- data.frame(Cluster = c('Cluster 1','Cluster 2','Cluster 3','Cluster 4','Cluster 5'))
    df$Overlaps <- c(mean(rt_cl1),mean(rt_cl2),mean(rt_cl3),mean(rt_cl4),mean(rt_cl5))
    df$Ratio = df$Overlaps/sum(df$Overlaps)
    
    
    my.cols <- brewer.pal(n = 8, name = "Dark2")
    my.cols[4] <- my.cols[7]
    my.cols[5] <- my.cols[8]
    
    setwd('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/SKCM bigwigs/plots/')
    tiff("SKCM_ov_0.7.tiff",
         height = 5, width = 10, units = 'in', res=300)
    ggplot(data=df, aes(x=Cluster, y=Ratio, fill = Cluster)) +
      geom_bar(stat="identity")+
      theme_minimal() + labs(fill = "Percentages") + 
      theme(legend.title = element_text(size = 18),
            legend.text = element_text(size = 15)) +
      scale_fill_manual(values=my.cols) +
      ylim(c(0,max(df$Ratio + 0.1))) + ggtitle("SKCM") + 
      xlab("") + ylab("Ratio of Overlapped Peaks") +
      geom_text(aes(label=round(Ratio,digits = 3)), position=position_stack(), vjust=-0.5,size = 8) +
      theme(legend.position = "none",axis.text.x = element_text(face="bold", size=14))
    dev.off()
    
  }
  
  # Sample-wise cluster ditribution
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/SKCM_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    IDs <- colnames(counts_m)[6:ncol(counts_m)]
    samples <- unique(sapply(IDs, function(x) paste0(strsplit(x,split = '_')[[1]][1:8],collapse = '_') ))
    length(samples)
    peak_m <- (counts_m[,(6:ncol(counts_m))] > 2)*1
    colnames(peak_m) <- IDs
    lst_tech_rep <- lapply(samples, function(x) which(str_detect(IDs,x)))
    sample_peaks<- matrix(0,nrow(peak_m),length(lst_tech_rep))
    colnames(sample_peaks) <- samples
    for (i in 1:length(lst_tech_rep)){
      #checking if we have multiple technical replicates
      if(!is.null(dim(counts_m[,lst_tech_rep[[i]]]))){
        sample_peaks[,i] = (rowSums(peak_m[,lst_tech_rep[[i]]]) != 0) * 1
      }else{
        sample_peaks[,i] = peak_m[,lst_tech_rep[[i]]]
      }
    }
    # All analysis use peak_m, I didnot  want to rewrite all
    peak_m <- sample_peaks
    
    
    #Cluster1
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster1.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl1 <- ov_peaks
    
    #Cluster2
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster2.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl2 <- ov_peaks
    
    #Cluster3
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster3.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl3 <- ov_peaks
    
    
    #Cluster4
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl4 <- ov_peaks
    
    #Cluster5
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster5.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    
    ov_cl5 <- ov_peaks
    
    
    SKCM_sample_matrix <- matrix(0,length(samples),5)
    
    n_cl1 <-  1157
    n_cl2 <-  741
    n_cl3 <-  919
    n_cl4 <-  476
    n_cl5 <-  424
    
    rt_cl1 <- colSums(ov_cl1) / n_cl1
    rt_cl2 <- colSums(ov_cl2) / n_cl2
    rt_cl3 <- colSums(ov_cl3) / n_cl3
    rt_cl4 <- colSums(ov_cl4) / n_cl4
    rt_cl5 <- colSums(ov_cl5) / n_cl5
    all_rt <- rt_cl1 + rt_cl2 + rt_cl3 + rt_cl4 + rt_cl5
    
    SKCM_sample_matrix[,1] <- rt_cl1/all_rt
    SKCM_sample_matrix[,2] <- rt_cl2/all_rt
    SKCM_sample_matrix[,3] <- rt_cl3/all_rt
    SKCM_sample_matrix[,4] <- rt_cl4/all_rt
    SKCM_sample_matrix[,5] <- rt_cl5/all_rt
    
    rownames(SKCM_sample_matrix) <- samples
  }
  
  # scores
  {
    
    # Loading bw files
    
    setwd("~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/SKCM bigwigs")
    files <- list.files()
    
    dta_bw <- import(files[1])
    
    ov_regions <- c_regions_brca[which(!is.na(brca_c_overlaps))]
    
    int_bw_regions <- which(!is.na(findOverlaps(dta_bw, ov_regions[1],
                                                type = "any", select = "first",
                                                minoverlap = 1,
                                                ignore.strand= TRUE)))
    plot(dta_bw[int_bw_regions])
    
    
    
    
    
    which(rowSums(peaks_ov) == 0)
    which(!is.na(brca_c_overlaps))
    summary(as.numeric(c_brca[19124,6:29]))
    
    
    4012, score = 10.84
    Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
    1.958   2.584   3.120   3.113   3.484   4.148 
    chr1:78,003,883-78,004,382
    
    
    5213, score = 10.915008
    Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
    -1.233   0.444   1.477   1.589   2.599   4.399 
    chr1:115,678,982-115,679,481
    
    
    19124, score = 7.04
    Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
    -0.4647  0.8431  1.4511  1.4284  2.2806  3.8249 
    chr3:16,166,831-16,167,330
    
    
    52268, score = 5.463
    Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
    0.8166  1.2226  1.5364  1.7365  2.0894  3.6555 
    chr8:120,084,418-120,084,917
    
    
    56660, score = 19.78
    Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
    3.863   4.426   4.656   4.732   5.076   5.512 
    chr9:114,505,313-114,505,812
    
    85725, score = 23.61
    Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
    4.132   4.452   4.788   4.717   4.892   5.267 
    chr17:42,678,147-42,678,646
    
    95687, score = 8.88
    Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
    1.443   2.706   3.074   3.114   3.657   4.504 
    chr20:36,845,334-36,845,833
    
  }
  
}


# STAD - threshold = 2.25
{
  #Choosing the threshold 
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/STAD_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    
    
    cluster4 <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl4 <- GRanges(seq = cluster4$V1,IRanges(start = cluster4$V2,end = cluster4$V3))
    
    #1 bp overlaps
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl4,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    
    peaks_ov <- counts_regions[which(!is.na(peaks_overlaps)),]
    
    
    # bigwig plotting
    {
      # Get transcrupts annotation to get HNF4A regions
      tssAnnot <- ELMER::getTSS(genome = "hg38")
      
      # Plot parameters, only to look better
      pp <- getDefaultPlotParams(plot.type = 1)
      pp$leftmargin <- 0.15
      pp$topmargin <- 5
      pp$bottommargin <- 5
      pp$ideogramheight <- 5
      pp$data1inmargin <- 10
      pp$data1outmargin <- 0
      
      # Start to plot bigwig files
      big.wig.files <- dir(path = "~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/STAD bigwigs",
                           pattern = ".bw",
                           all.files = T,
                           full.names = T)
      # Reserve area to plot the bigwig files
      out.at <- autotrack(1:length(big.wig.files), 
                          length(big.wig.files), 
                          margin = 0.3, 
                          r0 = 0,
                          r1 = 1)
      
      setwd('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/STAD bigwigs/plots/th_2.5/')
      threshold = 2.5
      
      for (rgn in 1:length(peaks_ov)){
        cat(rgn,'\n')
        tiff(paste0(peaks_ov[rgn]@seqnames,'__',peaks_ov[rgn]@ranges,'.tiff'),
             height = 10, width = 4, units = 'in', res=300)
        
        # plot will be at  the HNF4A range +- 10Kb
        plt.region <- range(c(peaks_ov[rgn])) + 10000
        
        # Start by plotting gene tracks
        kp <- plotKaryotype(zoom = plt.region,genome = "hg38", cex = 0.5, plot.params = pp)
        genes.data <- makeGenesDataFromTxDb(TxDb.Hsapiens.UCSC.hg38.knownGene,
                                            karyoplot = kp,
                                            plot.transcripts = TRUE, 
                                            plot.transcripts.structure = TRUE)
        genes.data <- addGeneNames(genes.data)
        
        
        
        genes.data <- mergeTranscripts(genes.data)
        
        kp <- plotKaryotype(zoom = plt.region,genome = "hg38", cex = 0.5, plot.params = pp)
        kpAddBaseNumbers(kp, tick.dist = 20000, minor.tick.dist = 5000,
                         add.units = TRUE, cex = 0.5, tick.len = 1)
        #if we would like to put genes under ATAC peaks
        # kpPlotGenes(kp, data = genes.data, r0 = 0, r1 = 0.25, gene.name.cex = 0.5)
        
        
        # Add ATAC-seq label from 0.3 to 1 which should cover all ATAC-seq tracks
        kpAddLabels(kp, 
                    labels = "ATAC-Seq", 
                    r0 = out.at$r0, 
                    r1 = out.at$r1, 
                    side = "left",
                    cex = 0.25,
                    srt = 90, 
                    pos = 3, 
                    label.margin = 0.1)
        
        
        for(i in seq_len(length(big.wig.files))) {
          bigwig.file <- big.wig.files[i]
          
          # just getting the name
          tec_rep_name <- strsplit(strsplit(bigwig.file,split = './')[[1]][9],split = '\\.')[[1]][1]
          cnt <- counts_m[which(!is.na(peaks_overlaps))[rgn],colnames(counts_m) %in% tec_rep_name]
          
          # adaptive threshold by using the score for each region
          # threshold <-counts_m[which(!is.na(peaks_overlaps))[rgn],5]
          
          # Define where the track will be ploted
          # autotrack will simple get the reserved space (from out.at$r0 up to out.at$r1)
          # and split in equal sizes for each bigwifile, i the index, will control which 
          # one is being plotted
          at <- autotrack(i, length(big.wig.files), r0 = out.at$r0, r1 = out.at$r1, margin = 0.2)
          
          
          # Plot bigwig
          kp <- kpPlotBigWig(kp, 
                             data = bigwig.file, 
                             ymax = "visible.region", 
                             r0 = at$r0, 
                             col = ifelse(cnt>threshold,"#0000FF","#FF0000"), 
                             r1 = at$r1)
          computed.ymax <- ceiling(kp$latest.plot$computed.values$ymax)
          
          # Add track axis
          kpAxis(kp, 
                 ymin = 0, 
                 ymax = 150, 
                 numticks = 2,
                 r0 = at$r0, 
                 r1 = at$r1,
                 cex = 0.15)
          
          # Add track label
          kpAddLabels(kp, 
                      labels = ifelse(cnt>threshold,"Peak","No Peak"),
                      r0 = at$r0, 
                      r1 = at$r1, 
                      cex = 0.25, 
                      label.margin = 0.01)
          
          
        }
        
        kpPlotRegions(kp, data=peaks_ov[rgn], col="#FFFCCC50", border="#00000044")
        kpPlotRegions(kp, data=peaks_ov[rgn], col="#FFFCCC50", border="#00000044")
        
        dev.off()
        
      }
      
    }
    
    
    
  }
  
  # Peak intersection for each cluster
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/STAD_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    
    peak_m <- (counts_m[,(6:ncol(counts_m))] > 0.7)*1
    
    
    #Cluster1
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster1.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl1 <- ov_peaks
    
    
    
    #Cluster2
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster2.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl2 <- ov_peaks
    
    
    #Cluster3
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster3.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl3 <- ov_peaks
    
    
    #Cluster4
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl4 <- ov_peaks
    
    
    #Cluster5
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster5.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl5 <- ov_peaks
  }
  
  #plotting
  {
    n_cl1 <-  1157
    n_cl2 <-  741
    n_cl3 <-  919
    n_cl4 <-  476
    n_cl5 <-  424
    
    
    rt_cl1 <- colSums(ov_cl1) / n_cl1
    rt_cl2 <- colSums(ov_cl2) / n_cl2
    rt_cl3 <- colSums(ov_cl3) / n_cl3
    rt_cl4 <- colSums(ov_cl4) / n_cl4
    rt_cl5 <- colSums(ov_cl5) / n_cl5
    
    df <- data.frame(Cluster = c('Cluster 1','Cluster 2','Cluster 3','Cluster 4','Cluster 5'))
    df$Overlaps <- c(mean(rt_cl1),mean(rt_cl2),mean(rt_cl3),mean(rt_cl4),mean(rt_cl5))
    df$Ratio = df$Overlaps/sum(df$Overlaps)
    
    
    my.cols <- brewer.pal(n = 8, name = "Dark2")
    my.cols[4] <- my.cols[7]
    my.cols[5] <- my.cols[8]
    
    setwd('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/STAD bigwigs/plots/')
    tiff("STAD_ov_0.7.tiff",
         height = 5, width = 10, units = 'in', res=300)
    ggplot(data=df, aes(x=Cluster, y=Ratio, fill = Cluster)) +
      geom_bar(stat="identity")+
      theme_minimal() + labs(fill = "Percentages") + 
      theme(legend.title = element_text(size = 18),
            legend.text = element_text(size = 15)) +
      scale_fill_manual(values=my.cols) +
      ylim(c(0,max(df$Ratio + 0.1))) + ggtitle("STAD") + 
      xlab("") + ylab("Ratio of Overlapped Peaks") +
      geom_text(aes(label=round(Ratio,digits = 3)), position=position_stack(), vjust=-0.5,size = 8) +
      theme(legend.position = "none",axis.text.x = element_text(face="bold", size=14))
    dev.off()
    
  }
  
  # Sample-wise cluster ditribution
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/STAD_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    IDs <- colnames(counts_m)[6:ncol(counts_m)]
    samples <- unique(sapply(IDs, function(x) paste0(strsplit(x,split = '_')[[1]][1:8],collapse = '_') ))
    length(samples)
    peak_m <- (counts_m[,(6:ncol(counts_m))] > 2.25)*1
    colnames(peak_m) <- IDs
    lst_tech_rep <- lapply(samples, function(x) which(str_detect(IDs,x)))
    sample_peaks<- matrix(0,nrow(peak_m),length(lst_tech_rep))
    colnames(sample_peaks) <- samples
    for (i in 1:length(lst_tech_rep)){
      #checking if we have multiple technical replicates
      if(!is.null(dim(counts_m[,lst_tech_rep[[i]]]))){
        sample_peaks[,i] = (rowSums(peak_m[,lst_tech_rep[[i]]]) != 0) * 1
      }else{
        sample_peaks[,i] = peak_m[,lst_tech_rep[[i]]]
      }
    }
    # All analysis use peak_m, I didnot  want to rewrite all
    peak_m <- sample_peaks
    
    
    #Cluster1
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster1.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl1 <- ov_peaks
    
    #Cluster2
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster2.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl2 <- ov_peaks
    
    #Cluster3
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster3.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl3 <- ov_peaks
    
    
    #Cluster4
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl4 <- ov_peaks
    
    #Cluster5
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster5.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    
    ov_cl5 <- ov_peaks
    
    
    STAD_sample_matrix <- matrix(0,length(samples),5)
    
    n_cl1 <-  1157
    n_cl2 <-  741
    n_cl3 <-  919
    n_cl4 <-  476
    n_cl5 <-  424
    
    rt_cl1 <- colSums(ov_cl1) / n_cl1
    rt_cl2 <- colSums(ov_cl2) / n_cl2
    rt_cl3 <- colSums(ov_cl3) / n_cl3
    rt_cl4 <- colSums(ov_cl4) / n_cl4
    rt_cl5 <- colSums(ov_cl5) / n_cl5
    all_rt <- rt_cl1 + rt_cl2 + rt_cl3 + rt_cl4 + rt_cl5
    
    STAD_sample_matrix[,1] <- rt_cl1/all_rt
    STAD_sample_matrix[,2] <- rt_cl2/all_rt
    STAD_sample_matrix[,3] <- rt_cl3/all_rt
    STAD_sample_matrix[,4] <- rt_cl4/all_rt
    STAD_sample_matrix[,5] <- rt_cl5/all_rt
    
    rownames(STAD_sample_matrix) <- samples
  }
  
}


# TGCT - threshold = 1.7
{
  #Choosing the threshold 
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/TGCT_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    
    
    cluster4 <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl4 <- GRanges(seq = cluster4$V1,IRanges(start = cluster4$V2,end = cluster4$V3))
    
    #1 bp overlaps
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl4,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    
    peaks_ov <- counts_regions[which(!is.na(peaks_overlaps)),]
    
    
    # bigwig plotting
    {
      # Get transcrupts annotation to get HNF4A regions
      tssAnnot <- ELMER::getTSS(genome = "hg38")
      
      # Plot parameters, only to look better
      pp <- getDefaultPlotParams(plot.type = 1)
      pp$leftmargin <- 0.15
      pp$topmargin <- 5
      pp$bottommargin <- 5
      pp$ideogramheight <- 5
      pp$data1inmargin <- 10
      pp$data1outmargin <- 0
      
      # Start to plot bigwig files
      big.wig.files <- dir(path = "~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/TGCT bigwigs",
                           pattern = ".bw",
                           all.files = T,
                           full.names = T)
      # Reserve area to plot the bigwig files
      out.at <- autotrack(1:length(big.wig.files), 
                          length(big.wig.files), 
                          margin = 0.3, 
                          r0 = 0,
                          r1 = 1)
      
      setwd('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/TGCT bigwigs/plots/th_2.5/')
      threshold = 2.5
      
      for (rgn in 1:length(peaks_ov)){
        cat(rgn,'\n')
        tiff(paste0(peaks_ov[rgn]@seqnames,'__',peaks_ov[rgn]@ranges,'.tiff'),
             height = 10, width = 4, units = 'in', res=300)
        
        # plot will be at  the HNF4A range +- 10Kb
        plt.region <- range(c(peaks_ov[rgn])) + 10000
        
        # Start by plotting gene tracks
        kp <- plotKaryotype(zoom = plt.region,genome = "hg38", cex = 0.5, plot.params = pp)
        genes.data <- makeGenesDataFromTxDb(TxDb.Hsapiens.UCSC.hg38.knownGene,
                                            karyoplot = kp,
                                            plot.transcripts = TRUE, 
                                            plot.transcripts.structure = TRUE)
        genes.data <- addGeneNames(genes.data)
        
        
        
        genes.data <- mergeTranscripts(genes.data)
        
        kp <- plotKaryotype(zoom = plt.region,genome = "hg38", cex = 0.5, plot.params = pp)
        kpAddBaseNumbers(kp, tick.dist = 20000, minor.tick.dist = 5000,
                         add.units = TRUE, cex = 0.5, tick.len = 1)
        #if we would like to put genes under ATAC peaks
        # kpPlotGenes(kp, data = genes.data, r0 = 0, r1 = 0.25, gene.name.cex = 0.5)
        
        
        # Add ATAC-seq label from 0.3 to 1 which should cover all ATAC-seq tracks
        kpAddLabels(kp, 
                    labels = "ATAC-Seq", 
                    r0 = out.at$r0, 
                    r1 = out.at$r1, 
                    side = "left",
                    cex = 0.25,
                    srt = 90, 
                    pos = 3, 
                    label.margin = 0.1)
        
        
        for(i in seq_len(length(big.wig.files))) {
          bigwig.file <- big.wig.files[i]
          
          # just getting the name
          tec_rep_name <- strsplit(strsplit(bigwig.file,split = './')[[1]][9],split = '\\.')[[1]][1]
          cnt <- counts_m[which(!is.na(peaks_overlaps))[rgn],colnames(counts_m) %in% tec_rep_name]
          
          # adaptive threshold by using the score for each region
          # threshold <-counts_m[which(!is.na(peaks_overlaps))[rgn],5]
          
          # Define where the track will be ploted
          # autotrack will simple get the reserved space (from out.at$r0 up to out.at$r1)
          # and split in equal sizes for each bigwifile, i the index, will control which 
          # one is being plotted
          at <- autotrack(i, length(big.wig.files), r0 = out.at$r0, r1 = out.at$r1, margin = 0.2)
          
          
          # Plot bigwig
          kp <- kpPlotBigWig(kp, 
                             data = bigwig.file, 
                             ymax = "visible.region", 
                             r0 = at$r0, 
                             col = ifelse(cnt>threshold,"#0000FF","#FF0000"), 
                             r1 = at$r1)
          computed.ymax <- ceiling(kp$latest.plot$computed.values$ymax)
          
          # Add track axis
          kpAxis(kp, 
                 ymin = 0, 
                 ymax = 150, 
                 numticks = 2,
                 r0 = at$r0, 
                 r1 = at$r1,
                 cex = 0.15)
          
          # Add track label
          kpAddLabels(kp, 
                      labels = ifelse(cnt>threshold,"Peak","No Peak"),
                      r0 = at$r0, 
                      r1 = at$r1, 
                      cex = 0.25, 
                      label.margin = 0.01)
          
          
        }
        
        kpPlotRegions(kp, data=peaks_ov[rgn], col="#FFFCCC50", border="#00000044")
        kpPlotRegions(kp, data=peaks_ov[rgn], col="#FFFCCC50", border="#00000044")
        
        dev.off()
        
      }
      
    }
    
    
    
  }
  
  # Peak intersection for each cluster
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/TGCT_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    
    peak_m <- (counts_m[,(6:ncol(counts_m))] > 0.7)*1
    
    
    #Cluster1
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster1.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl1 <- ov_peaks
    
    
    
    #Cluster2
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster2.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl2 <- ov_peaks
    
    
    #Cluster3
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster3.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl3 <- ov_peaks
    
    
    #Cluster4
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl4 <- ov_peaks
    
    
    #Cluster5
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster5.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl5 <- ov_peaks
  }
  
  #plotting
  {
    n_cl1 <-  1157
    n_cl2 <-  741
    n_cl3 <-  919
    n_cl4 <-  476
    n_cl5 <-  424
    
    
    rt_cl1 <- colSums(ov_cl1) / n_cl1
    rt_cl2 <- colSums(ov_cl2) / n_cl2
    rt_cl3 <- colSums(ov_cl3) / n_cl3
    rt_cl4 <- colSums(ov_cl4) / n_cl4
    rt_cl5 <- colSums(ov_cl5) / n_cl5
    
    df <- data.frame(Cluster = c('Cluster 1','Cluster 2','Cluster 3','Cluster 4','Cluster 5'))
    df$Overlaps <- c(mean(rt_cl1),mean(rt_cl2),mean(rt_cl3),mean(rt_cl4),mean(rt_cl5))
    df$Ratio = df$Overlaps/sum(df$Overlaps)
    
    
    my.cols <- brewer.pal(n = 8, name = "Dark2")
    my.cols[4] <- my.cols[7]
    my.cols[5] <- my.cols[8]
    
    setwd('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/TGCT bigwigs/plots/')
    tiff("TGCT_ov_0.7.tiff",
         height = 5, width = 10, units = 'in', res=300)
    ggplot(data=df, aes(x=Cluster, y=Ratio, fill = Cluster)) +
      geom_bar(stat="identity")+
      theme_minimal() + labs(fill = "Percentages") + 
      theme(legend.title = element_text(size = 18),
            legend.text = element_text(size = 15)) +
      scale_fill_manual(values=my.cols) +
      ylim(c(0,max(df$Ratio + 0.1))) + ggtitle("TGCT") + 
      xlab("") + ylab("Ratio of Overlapped Peaks") +
      geom_text(aes(label=round(Ratio,digits = 3)), position=position_stack(), vjust=-0.5,size = 8) +
      theme(legend.position = "none",axis.text.x = element_text(face="bold", size=14))
    dev.off()
    
  }
  
  # Sample-wise cluster ditribution
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/TGCT_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    IDs <- colnames(counts_m)[6:ncol(counts_m)]
    samples <- unique(sapply(IDs, function(x) paste0(strsplit(x,split = '_')[[1]][1:8],collapse = '_') ))
    length(samples)
    peak_m <- (counts_m[,(6:ncol(counts_m))] > 1.7)*1
    colnames(peak_m) <- IDs
    lst_tech_rep <- lapply(samples, function(x) which(str_detect(IDs,x)))
    sample_peaks<- matrix(0,nrow(peak_m),length(lst_tech_rep))
    colnames(sample_peaks) <- samples
    for (i in 1:length(lst_tech_rep)){
      #checking if we have multiple technical replicates
      if(!is.null(dim(counts_m[,lst_tech_rep[[i]]]))){
        sample_peaks[,i] = (rowSums(peak_m[,lst_tech_rep[[i]]]) != 0) * 1
      }else{
        sample_peaks[,i] = peak_m[,lst_tech_rep[[i]]]
      }
    }
    # All analysis use peak_m, I didnot  want to rewrite all
    peak_m <- sample_peaks
    
    
    #Cluster1
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster1.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl1 <- ov_peaks
    
    #Cluster2
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster2.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl2 <- ov_peaks
    
    #Cluster3
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster3.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl3 <- ov_peaks
    
    
    #Cluster4
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl4 <- ov_peaks
    
    #Cluster5
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster5.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    
    ov_cl5 <- ov_peaks
    
    
    TGCT_sample_matrix <- matrix(0,length(samples),5)
    
    n_cl1 <-  1157
    n_cl2 <-  741
    n_cl3 <-  919
    n_cl4 <-  476
    n_cl5 <-  424
    
    rt_cl1 <- colSums(ov_cl1) / n_cl1
    rt_cl2 <- colSums(ov_cl2) / n_cl2
    rt_cl3 <- colSums(ov_cl3) / n_cl3
    rt_cl4 <- colSums(ov_cl4) / n_cl4
    rt_cl5 <- colSums(ov_cl5) / n_cl5
    all_rt <- rt_cl1 + rt_cl2 + rt_cl3 + rt_cl4 + rt_cl5
    
    TGCT_sample_matrix[,1] <- rt_cl1/all_rt
    TGCT_sample_matrix[,2] <- rt_cl2/all_rt
    TGCT_sample_matrix[,3] <- rt_cl3/all_rt
    TGCT_sample_matrix[,4] <- rt_cl4/all_rt
    TGCT_sample_matrix[,5] <- rt_cl5/all_rt
    
    rownames(TGCT_sample_matrix) <- samples
  }
  
}


# THCA - threshold = 1.4
{
  #Choosing the threshold 
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/THCA_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    
    
    cluster4 <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl4 <- GRanges(seq = cluster4$V1,IRanges(start = cluster4$V2,end = cluster4$V3))
    
    #1 bp overlaps
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl4,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    
    peaks_ov <- counts_regions[which(!is.na(peaks_overlaps)),]
    
    
    # bigwig plotting
    {
      # Get transcrupts annotation to get HNF4A regions
      tssAnnot <- ELMER::getTSS(genome = "hg38")
      
      # Plot parameters, only to look better
      pp <- getDefaultPlotParams(plot.type = 1)
      pp$leftmargin <- 0.15
      pp$topmargin <- 5
      pp$bottommargin <- 5
      pp$ideogramheight <- 5
      pp$data1inmargin <- 10
      pp$data1outmargin <- 0
      
      # Start to plot bigwig files
      big.wig.files <- dir(path = "~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/THCA bigwigs",
                           pattern = ".bw",
                           all.files = T,
                           full.names = T)
      # Reserve area to plot the bigwig files
      out.at <- autotrack(1:length(big.wig.files), 
                          length(big.wig.files), 
                          margin = 0.3, 
                          r0 = 0,
                          r1 = 1)
      
      setwd('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/THCA bigwigs/plots/th_2.5/')
      threshold = 2.5
      
      for (rgn in 1:length(peaks_ov)){
        cat(rgn,'\n')
        tiff(paste0(peaks_ov[rgn]@seqnames,'__',peaks_ov[rgn]@ranges,'.tiff'),
             height = 10, width = 4, units = 'in', res=300)
        
        # plot will be at  the HNF4A range +- 10Kb
        plt.region <- range(c(peaks_ov[rgn])) + 10000
        
        # Start by plotting gene tracks
        kp <- plotKaryotype(zoom = plt.region,genome = "hg38", cex = 0.5, plot.params = pp)
        genes.data <- makeGenesDataFromTxDb(TxDb.Hsapiens.UCSC.hg38.knownGene,
                                            karyoplot = kp,
                                            plot.transcripts = TRUE, 
                                            plot.transcripts.structure = TRUE)
        genes.data <- addGeneNames(genes.data)
        
        
        
        genes.data <- mergeTranscripts(genes.data)
        
        kp <- plotKaryotype(zoom = plt.region,genome = "hg38", cex = 0.5, plot.params = pp)
        kpAddBaseNumbers(kp, tick.dist = 20000, minor.tick.dist = 5000,
                         add.units = TRUE, cex = 0.5, tick.len = 1)
        #if we would like to put genes under ATAC peaks
        # kpPlotGenes(kp, data = genes.data, r0 = 0, r1 = 0.25, gene.name.cex = 0.5)
        
        
        # Add ATAC-seq label from 0.3 to 1 which should cover all ATAC-seq tracks
        kpAddLabels(kp, 
                    labels = "ATAC-Seq", 
                    r0 = out.at$r0, 
                    r1 = out.at$r1, 
                    side = "left",
                    cex = 0.25,
                    srt = 90, 
                    pos = 3, 
                    label.margin = 0.1)
        
        
        for(i in seq_len(length(big.wig.files))) {
          bigwig.file <- big.wig.files[i]
          
          # just getting the name
          tec_rep_name <- strsplit(strsplit(bigwig.file,split = './')[[1]][9],split = '\\.')[[1]][1]
          cnt <- counts_m[which(!is.na(peaks_overlaps))[rgn],colnames(counts_m) %in% tec_rep_name]
          
          # adaptive threshold by using the score for each region
          # threshold <-counts_m[which(!is.na(peaks_overlaps))[rgn],5]
          
          # Define where the track will be ploted
          # autotrack will simple get the reserved space (from out.at$r0 up to out.at$r1)
          # and split in equal sizes for each bigwifile, i the index, will control which 
          # one is being plotted
          at <- autotrack(i, length(big.wig.files), r0 = out.at$r0, r1 = out.at$r1, margin = 0.2)
          
          
          # Plot bigwig
          kp <- kpPlotBigWig(kp, 
                             data = bigwig.file, 
                             ymax = "visible.region", 
                             r0 = at$r0, 
                             col = ifelse(cnt>threshold,"#0000FF","#FF0000"), 
                             r1 = at$r1)
          computed.ymax <- ceiling(kp$latest.plot$computed.values$ymax)
          
          # Add track axis
          kpAxis(kp, 
                 ymin = 0, 
                 ymax = 150, 
                 numticks = 2,
                 r0 = at$r0, 
                 r1 = at$r1,
                 cex = 0.15)
          
          # Add track label
          kpAddLabels(kp, 
                      labels = ifelse(cnt>threshold,"Peak","No Peak"),
                      r0 = at$r0, 
                      r1 = at$r1, 
                      cex = 0.25, 
                      label.margin = 0.01)
          
          
        }
        
        kpPlotRegions(kp, data=peaks_ov[rgn], col="#FFFCCC50", border="#00000044")
        kpPlotRegions(kp, data=peaks_ov[rgn], col="#FFFCCC50", border="#00000044")
        
        dev.off()
        
      }
      
    }
    
    
    
  }
  
  # Peak intersection for each cluster
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/THCA_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    
    peak_m <- (counts_m[,(6:ncol(counts_m))] > 0.7)*1
    
    
    #Cluster1
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster1.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl1 <- ov_peaks
    
    
    
    #Cluster2
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster2.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl2 <- ov_peaks
    
    
    #Cluster3
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster3.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl3 <- ov_peaks
    
    
    #Cluster4
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl4 <- ov_peaks
    
    
    #Cluster5
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster5.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl5 <- ov_peaks
  }
  
  #plotting
  {
    n_cl1 <-  1157
    n_cl2 <-  741
    n_cl3 <-  919
    n_cl4 <-  476
    n_cl5 <-  424
    
    
    rt_cl1 <- colSums(ov_cl1) / n_cl1
    rt_cl2 <- colSums(ov_cl2) / n_cl2
    rt_cl3 <- colSums(ov_cl3) / n_cl3
    rt_cl4 <- colSums(ov_cl4) / n_cl4
    rt_cl5 <- colSums(ov_cl5) / n_cl5
    
    df <- data.frame(Cluster = c('Cluster 1','Cluster 2','Cluster 3','Cluster 4','Cluster 5'))
    df$Overlaps <- c(mean(rt_cl1),mean(rt_cl2),mean(rt_cl3),mean(rt_cl4),mean(rt_cl5))
    df$Ratio = df$Overlaps/sum(df$Overlaps)
    
    
    my.cols <- brewer.pal(n = 8, name = "Dark2")
    my.cols[4] <- my.cols[7]
    my.cols[5] <- my.cols[8]
    
    setwd('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/THCA bigwigs/plots/')
    tiff("THCA_ov_0.7.tiff",
         height = 5, width = 10, units = 'in', res=300)
    ggplot(data=df, aes(x=Cluster, y=Ratio, fill = Cluster)) +
      geom_bar(stat="identity")+
      theme_minimal() + labs(fill = "Percentages") + 
      theme(legend.title = element_text(size = 18),
            legend.text = element_text(size = 15)) +
      scale_fill_manual(values=my.cols) +
      ylim(c(0,max(df$Ratio + 0.1))) + ggtitle("THCA") + 
      xlab("") + ylab("Ratio of Overlapped Peaks") +
      geom_text(aes(label=round(Ratio,digits = 3)), position=position_stack(), vjust=-0.5,size = 8) +
      theme(legend.position = "none",axis.text.x = element_text(face="bold", size=14))
    dev.off()
    
  }
  
  # Sample-wise cluster ditribution
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/THCA_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    IDs <- colnames(counts_m)[6:ncol(counts_m)]
    samples <- unique(sapply(IDs, function(x) paste0(strsplit(x,split = '_')[[1]][1:8],collapse = '_') ))
    length(samples)
    peak_m <- (counts_m[,(6:ncol(counts_m))] > 1.4)*1
    colnames(peak_m) <- IDs
    lst_tech_rep <- lapply(samples, function(x) which(str_detect(IDs,x)))
    sample_peaks<- matrix(0,nrow(peak_m),length(lst_tech_rep))
    colnames(sample_peaks) <- samples
    for (i in 1:length(lst_tech_rep)){
      #checking if we have multiple technical replicates
      if(!is.null(dim(counts_m[,lst_tech_rep[[i]]]))){
        sample_peaks[,i] = (rowSums(peak_m[,lst_tech_rep[[i]]]) != 0) * 1
      }else{
        sample_peaks[,i] = peak_m[,lst_tech_rep[[i]]]
      }
    }
    # All analysis use peak_m, I didnot  want to rewrite all
    peak_m <- sample_peaks
    
    
    #Cluster1
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster1.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl1 <- ov_peaks
    
    #Cluster2
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster2.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl2 <- ov_peaks
    
    #Cluster3
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster3.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl3 <- ov_peaks
    
    
    #Cluster4
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl4 <- ov_peaks
    
    #Cluster5
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster5.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    
    ov_cl5 <- ov_peaks
    
    
    THCA_sample_matrix <- matrix(0,length(samples),5)
    
    n_cl1 <-  1157
    n_cl2 <-  741
    n_cl3 <-  919
    n_cl4 <-  476
    n_cl5 <-  424
    
    rt_cl1 <- colSums(ov_cl1) / n_cl1
    rt_cl2 <- colSums(ov_cl2) / n_cl2
    rt_cl3 <- colSums(ov_cl3) / n_cl3
    rt_cl4 <- colSums(ov_cl4) / n_cl4
    rt_cl5 <- colSums(ov_cl5) / n_cl5
    all_rt <- rt_cl1 + rt_cl2 + rt_cl3 + rt_cl4 + rt_cl5
    
    THCA_sample_matrix[,1] <- rt_cl1/all_rt
    THCA_sample_matrix[,2] <- rt_cl2/all_rt
    THCA_sample_matrix[,3] <- rt_cl3/all_rt
    THCA_sample_matrix[,4] <- rt_cl4/all_rt
    THCA_sample_matrix[,5] <- rt_cl5/all_rt
    
    rownames(THCA_sample_matrix) <- samples
  }
  
}


# UCEC - threshold = 1.3
{
  #Choosing the threshold 
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/UCEC_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    
    
    cluster4 <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl4 <- GRanges(seq = cluster4$V1,IRanges(start = cluster4$V2,end = cluster4$V3))
    
    #1 bp overlaps
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl4,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    
    peaks_ov <- counts_regions[which(!is.na(peaks_overlaps)),]
    
    
    # bigwig plotting
    {
      # Get transcrupts annotation to get HNF4A regions
      tssAnnot <- ELMER::getTSS(genome = "hg38")
      
      # Plot parameters, only to look better
      pp <- getDefaultPlotParams(plot.type = 1)
      pp$leftmargin <- 0.15
      pp$topmargin <- 5
      pp$bottommargin <- 5
      pp$ideogramheight <- 5
      pp$data1inmargin <- 10
      pp$data1outmargin <- 0
      
      # Start to plot bigwig files
      big.wig.files <- dir(path = "~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/UCEC bigwigs",
                           pattern = ".bw",
                           all.files = T,
                           full.names = T)
      # Reserve area to plot the bigwig files
      out.at <- autotrack(1:length(big.wig.files), 
                          length(big.wig.files), 
                          margin = 0.3, 
                          r0 = 0,
                          r1 = 1)
      
      setwd('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/UCEC bigwigs/plots/th_2.5/')
      threshold = 2.5
      
      for (rgn in 1:length(peaks_ov)){
        cat(rgn,'\n')
        tiff(paste0(peaks_ov[rgn]@seqnames,'__',peaks_ov[rgn]@ranges,'.tiff'),
             height = 10, width = 4, units = 'in', res=300)
        
        # plot will be at  the HNF4A range +- 10Kb
        plt.region <- range(c(peaks_ov[rgn])) + 10000
        
        # Start by plotting gene tracks
        kp <- plotKaryotype(zoom = plt.region,genome = "hg38", cex = 0.5, plot.params = pp)
        genes.data <- makeGenesDataFromTxDb(TxDb.Hsapiens.UCSC.hg38.knownGene,
                                            karyoplot = kp,
                                            plot.transcripts = TRUE, 
                                            plot.transcripts.structure = TRUE)
        genes.data <- addGeneNames(genes.data)
        
        
        
        genes.data <- mergeTranscripts(genes.data)
        
        kp <- plotKaryotype(zoom = plt.region,genome = "hg38", cex = 0.5, plot.params = pp)
        kpAddBaseNumbers(kp, tick.dist = 20000, minor.tick.dist = 5000,
                         add.units = TRUE, cex = 0.5, tick.len = 1)
        #if we would like to put genes under ATAC peaks
        # kpPlotGenes(kp, data = genes.data, r0 = 0, r1 = 0.25, gene.name.cex = 0.5)
        
        
        # Add ATAC-seq label from 0.3 to 1 which should cover all ATAC-seq tracks
        kpAddLabels(kp, 
                    labels = "ATAC-Seq", 
                    r0 = out.at$r0, 
                    r1 = out.at$r1, 
                    side = "left",
                    cex = 0.25,
                    srt = 90, 
                    pos = 3, 
                    label.margin = 0.1)
        
        
        for(i in seq_len(length(big.wig.files))) {
          bigwig.file <- big.wig.files[i]
          
          # just getting the name
          tec_rep_name <- strsplit(strsplit(bigwig.file,split = './')[[1]][9],split = '\\.')[[1]][1]
          cnt <- counts_m[which(!is.na(peaks_overlaps))[rgn],colnames(counts_m) %in% tec_rep_name]
          
          # adaptive threshold by using the score for each region
          # threshold <-counts_m[which(!is.na(peaks_overlaps))[rgn],5]
          
          # Define where the track will be ploted
          # autotrack will simple get the reserved space (from out.at$r0 up to out.at$r1)
          # and split in equal sizes for each bigwifile, i the index, will control which 
          # one is being plotted
          at <- autotrack(i, length(big.wig.files), r0 = out.at$r0, r1 = out.at$r1, margin = 0.2)
          
          
          # Plot bigwig
          kp <- kpPlotBigWig(kp, 
                             data = bigwig.file, 
                             ymax = "visible.region", 
                             r0 = at$r0, 
                             col = ifelse(cnt>threshold,"#0000FF","#FF0000"), 
                             r1 = at$r1)
          computed.ymax <- ceiling(kp$latest.plot$computed.values$ymax)
          
          # Add track axis
          kpAxis(kp, 
                 ymin = 0, 
                 ymax = 150, 
                 numticks = 2,
                 r0 = at$r0, 
                 r1 = at$r1,
                 cex = 0.15)
          
          # Add track label
          kpAddLabels(kp, 
                      labels = ifelse(cnt>threshold,"Peak","No Peak"),
                      r0 = at$r0, 
                      r1 = at$r1, 
                      cex = 0.25, 
                      label.margin = 0.01)
          
          
        }
        
        kpPlotRegions(kp, data=peaks_ov[rgn], col="#FFFCCC50", border="#00000044")
        kpPlotRegions(kp, data=peaks_ov[rgn], col="#FFFCCC50", border="#00000044")
        
        dev.off()
        
      }
      
    }
    
    
    
  }
  
  # Peak intersection for each cluster
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/UCEC_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    
    peak_m <- (counts_m[,(6:ncol(counts_m))] > 0.7)*1
    
    
    #Cluster1
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster1.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl1 <- ov_peaks
    
    
    
    #Cluster2
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster2.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl2 <- ov_peaks
    
    
    #Cluster3
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster3.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl3 <- ov_peaks
    
    
    #Cluster4
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl4 <- ov_peaks
    
    
    #Cluster5
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster5.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    
    
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,(ncol(counts_m)-5))
    
    
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
      
    } 
    
    ov_cl5 <- ov_peaks
  }
  
  #plotting
  {
    n_cl1 <-  1157
    n_cl2 <-  741
    n_cl3 <-  919
    n_cl4 <-  476
    n_cl5 <-  424
    
    
    rt_cl1 <- colSums(ov_cl1) / n_cl1
    rt_cl2 <- colSums(ov_cl2) / n_cl2
    rt_cl3 <- colSums(ov_cl3) / n_cl3
    rt_cl4 <- colSums(ov_cl4) / n_cl4
    rt_cl5 <- colSums(ov_cl5) / n_cl5
    
    df <- data.frame(Cluster = c('Cluster 1','Cluster 2','Cluster 3','Cluster 4','Cluster 5'))
    df$Overlaps <- c(mean(rt_cl1),mean(rt_cl2),mean(rt_cl3),mean(rt_cl4),mean(rt_cl5))
    df$Ratio = df$Overlaps/sum(df$Overlaps)
    
    
    my.cols <- brewer.pal(n = 8, name = "Dark2")
    my.cols[4] <- my.cols[7]
    my.cols[5] <- my.cols[8]
    
    setwd('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/bigwigs/UCEC bigwigs/plots/')
    tiff("UCEC_ov_0.7.tiff",
         height = 5, width = 10, units = 'in', res=300)
    ggplot(data=df, aes(x=Cluster, y=Ratio, fill = Cluster)) +
      geom_bar(stat="identity")+
      theme_minimal() + labs(fill = "Percentages") + 
      theme(legend.title = element_text(size = 18),
            legend.text = element_text(size = 15)) +
      scale_fill_manual(values=my.cols) +
      ylim(c(0,max(df$Ratio + 0.1))) + ggtitle("UCEC") + 
      xlab("") + ylab("Ratio of Overlapped Peaks") +
      geom_text(aes(label=round(Ratio,digits = 3)), position=position_stack(), vjust=-0.5,size = 8) +
      theme(legend.position = "none",axis.text.x = element_text(face="bold", size=14))
    dev.off()
    
  }
  
  # Sample-wise cluster ditribution
  {
    counts_m <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/counts/Matrix_counts/TCGA-ATAC_Cancer_Type-specific_Count_Matrices_log2norm_counts/UCEC_log2norm.txt')
    counts_regions <- GRanges(seq = counts_m[,1],IRanges(start = counts_m[,2],end = counts_m[,3]))
    IDs <- colnames(counts_m)[6:ncol(counts_m)]
    samples <- unique(sapply(IDs, function(x) paste0(strsplit(x,split = '_')[[1]][1:8],collapse = '_') ))
    length(samples)
    peak_m <- (counts_m[,(6:ncol(counts_m))] > 1.3)*1
    colnames(peak_m) <- IDs
    lst_tech_rep <- lapply(samples, function(x) which(str_detect(IDs,x)))
    sample_peaks<- matrix(0,nrow(peak_m),length(lst_tech_rep))
    colnames(sample_peaks) <- samples
    for (i in 1:length(lst_tech_rep)){
      #checking if we have multiple technical replicates
      if(!is.null(dim(counts_m[,lst_tech_rep[[i]]]))){
        sample_peaks[,i] = (rowSums(peak_m[,lst_tech_rep[[i]]]) != 0) * 1
      }else{
        sample_peaks[,i] = peak_m[,lst_tech_rep[[i]]]
      }
    }
    # All analysis use peak_m, I didnot  want to rewrite all
    peak_m <- sample_peaks
    
    
    #Cluster1
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster1.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl1 <- ov_peaks
    
    #Cluster2
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster2.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl2 <- ov_peaks
    
    #Cluster3
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster3.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl3 <- ov_peaks
    
    
    #Cluster4
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster4.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    ov_cl4 <- ov_peaks
    
    #Cluster5
    cluster <- read.delim('~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Clusters/Cluster5.bed',header = F)
    regions_cl <- GRanges(seq = cluster$V1,IRanges(start = cluster$V2,end = cluster$V3))
    #1 bp overlaps-atac-region
    peaks_overlaps <- findOverlaps(counts_regions, regions_cl,
                                   type = "any", select = "first",
                                   minoverlap = 1,
                                   ignore.strand= TRUE)
    # unique  overlapsreturns indices we hit in cluster, there might be more than one loci from count matrix
    # hits the same point from cluster region. We remove NA by subtracting 1
    n_overlaps <-length(unique(peaks_overlaps))-1
    ov_peaks<- matrix(0,n_overlaps,length(samples))
    for (i in 1:nrow(ov_peaks)){
      if(!is.null(dim(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),])[1])){
        ov_peaks[i,] =(colSums(peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]) != 0) * 1
      }else{
        ov_peaks[i,] =peak_m[which(peaks_overlaps == unique(peaks_overlaps)[i+1]),]
      }
    }
    
    ov_cl5 <- ov_peaks
    
    
    UCEC_sample_matrix <- matrix(0,length(samples),5)
    
    n_cl1 <-  1157
    n_cl2 <-  741
    n_cl3 <-  919
    n_cl4 <-  476
    n_cl5 <-  424
    
    rt_cl1 <- colSums(ov_cl1) / n_cl1
    rt_cl2 <- colSums(ov_cl2) / n_cl2
    rt_cl3 <- colSums(ov_cl3) / n_cl3
    rt_cl4 <- colSums(ov_cl4) / n_cl4
    rt_cl5 <- colSums(ov_cl5) / n_cl5
    all_rt <- rt_cl1 + rt_cl2 + rt_cl3 + rt_cl4 + rt_cl5
    
    UCEC_sample_matrix[,1] <- rt_cl1/all_rt
    UCEC_sample_matrix[,2] <- rt_cl2/all_rt
    UCEC_sample_matrix[,3] <- rt_cl3/all_rt
    UCEC_sample_matrix[,4] <- rt_cl4/all_rt
    UCEC_sample_matrix[,5] <- rt_cl5/all_rt
    
    rownames(UCEC_sample_matrix) <- samples
  }
  
}






All_samples <- rbind(ACC_sample_matrix,BLCA_sample_matrix, BRCA_sample_matrix,
                     CESC_sample_matrix, CHOL_sample_matrix, COAD_sample_matrix,
                     ESCA_sample_matrix, GBM_sample_matrix, HNSC_sample_matrix,
                     KIRC_sample_matrix, KIRP_sample_matrix, LGG_sample_matrix,
                     LIHC_sample_matrix, LUAD_sample_matrix, LUSC_sample_matrix,
                     MESO_sample_matrix, PCPG_sample_matrix,  PRAD_sample_matrix,
                     SKCM_sample_matrix, STAD_sample_matrix,TGCT_sample_matrix,
                     THCA_sample_matrix, UCEC_sample_matrix)

colnames(All_samples) <- c('Cluster1','Cluster2','Cluster3','Cluster4','Cluster5')
saveRDS(All_samples,'~/Documents/Working Projects/ATACSeq_Cluster4/Datasets/Samples_percentages_def.rds')