## CCLE NMF cluster TF comparison ##

rm(list = ls())
library(tidyverse)
library(dplyr)
library(ggplot2)
require(reshape2)
library(biomaRt)
library(ggpubr)
library(ggrepel)
library(preprocessCore)

setwd("/Volumes/Mahinur_2/2019.2.27_Enhancer_analysis/")

#read in CCLE samples
CCLE_TPM <- read.table(file = "CCLE_NMF124_GeneExpression.txt", sep = '\t',header = TRUE, stringsAsFactors = FALSE,
                       quote = "")
CCLE_TPM$ensembl_gene_id <- sapply(strsplit(as.character(CCLE_TPM$ensembl_gene_id), ".", fixed=T), function(x) x[1])
CCLE_TPM2 <- CCLE_TPM[,-1]
rownames(CCLE_TPM2) <- CCLE_TPM[,1]


## Get ENSGenes using biomaRt ##
ENSGenes <- useEnsembl(biomart = "ensembl", dataset="hsapiens_gene_ensembl", GRCh=37)
ENSGenes <- getBM(attributes = c('ensembl_gene_id', 'hgnc_symbol'), mart = ENSGenes)
CCLE_TPM_ENSGenes <- inner_join(x = ENSGenes, y = as.data.frame(CCLE_TPM2) %>% rownames_to_column(), by = c("ensembl_gene_id" = "rowname"))

counts.all.matrix <- as.matrix(CCLE_TPM_ENSGenes[,-c(1,2)])
counts.all.matrix.normalized <- normalize.quantiles(counts.all.matrix)
rownames(counts.all.matrix.normalized) <- CCLE_TPM_ENSGenes$hgnc_symbol
colnames(counts.all.matrix.normalized) <- colnames(CCLE_TPM_ENSGenes[,-c(1,2)])
counts.all.matrix.normalized <- as.data.frame(counts.all.matrix.normalized)

# Extract Normal samples
#counts.all.2.matrix.normalized <- counts.all.2.matrix.normalized[,c(1:7,16:58)]
counts.all.matrix.normalized <- rownames_to_column(counts.all.matrix.normalized, var = "gene_name")

# boxplot for mean of total samples
cols <- c("Cluster1" = "#2ee8ad", "Cluster2" = "#f7662e", "Cluster3" = "#4c7eef",
          "Cluster4" = "#f952b9", "Cluster5" = "#b0f73b")
counts.all.matrix.normalized %>%
  dplyr::filter(gene_name == "MITF") %>%
  gather(-gene_name, key = "sample", value = "TPM") %>%
  mutate(type = case_when(
    sample %in% c("WM793_SKIN","SKMEL28_SKIN","IGR1_SKIN","MALME3M_SKIN","COLO829_SKIN","COLO792_SKIN","A2058_SKIN",
                  "G361_SKIN","MEWO_SKIN","SKMEL5_SKIN","UACC62_SKIN","WM88_SKIN") ~ "Cluster1",
    sample %in% c("ZR751_BREAST","HCC38_BREAST","HCC1419_BREAST","HCC1428_BREAST","HCC1500_BREAST","AU565_BREAST",
                  "BT20_BREAST","CAMA1_BREAST","MDAMB175VII_BREAST","EFM19_BREAST","NCIH1781_LUNG","NIHOVCAR3_OVARY",
                  "HCC1143_BREAST","OVCAR4_OVARY","FUOV1_OVARY") ~ "Cluster2",
    sample %in% c("T84_LARGE_INTESTINE","SW403_LARGE_INTESTINE","SW948_LARGE_INTESTINE","HCC1806_BREAST","SW837_LARGE_INTESTINE",
                  "SNUC5_LARGE_INTESTINE","CAPAN2_PANCREAS","PANC0203_PANCREAS","BXPC3_PANCREAS","HPAFII_PANCREAS",
                  "PK59_PANCREAS","SW480_LARGE_INTESTINE","DLD1_LARGE_INTESTINE","SW620_LARGE_INTESTINE","CW2_LARGE_INTESTINE",
                  "HCT15_LARGE_INTESTINE","PATU8902_PANCREAS","SNU324_PANCREAS") ~ "Cluster3",
    sample %in% c("SW1088_CENTRAL_NERVOUS_SYSTEM","U178_CENTRAL_NERVOUS_SYSTEM","RPMI7951_SKIN","HS839T_SKIN","A375_SKIN",
                  "HCC1937_BREAST","SCC25_UPPER_AERODIGESTIVE_TRACT","SKMES1_LUNG","NCIH1650_LUNG","HS766T_PANCREAS",
                  "MDAMB231_BREAST","HCC1395_BREAST","CAL120_BREAST","RKO_LARGE_INTESTINE","NCIH1975_LUNG","NCIH1975_LUNG",
                  "IGROV1_OVARY","TOV21G_OVARY","OVTOKO_OVARY","PSN1_PANCREAS") ~ "Cluster4",
    sample %in% c("U343_CENTRAL_NERVOUS_SYSTEM","LN382_CENTRAL_NERVOUS_SYSTEM","KNS60_CENTRAL_NERVOUS_SYSTEM",
                  "U87MG_CENTRAL_NERVOUS_SYSTEM","LNZ308_CENTRAL_NERVOUS_SYSTEM","LN229_CENTRAL_NERVOUS_SYSTEM",
                  "SKBR3_BREAST","DETROIT562_UPPER_AERODIGESTIVE_TRACT","PECAPJ34CLONEC12_UPPER_AERODIGESTIVE_TRACT",
                  "NCIH1373_LUNG","NCIH2073_LUNG","NCIH3255_LUNG","CAOV3_OVARY","EFO27_OVARY","EFO21_OVARY",
                  "HCT116_LARGE_INTESTINE","LN18_CENTRAL_NERVOUS_SYSTEM","NCIH460_LUNG","NCIH1944_LUNG","OVCAR8_OVARY",
                  "MIAPACA2_PANCREAS","CALU1_LUNG") ~ "Cluster5")) %>%
  mutate(type = factor(type, levels = c("Cluster1","Cluster2","Cluster3","Cluster4","Cluster5"))) %>%
  ggplot(aes(x = type, y = log2(TPM +1))) +
  geom_boxplot(aes(col = type)) + 
  scale_colour_manual(values = cols) +
  geom_jitter(width = 0.2, aes(col = type)) +
  theme_classic(base_size = 32) +
  ggtitle(paste0("expression level of ", "MITF"))