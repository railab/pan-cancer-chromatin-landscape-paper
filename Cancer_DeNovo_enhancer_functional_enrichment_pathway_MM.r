rm(list = ls())
library(ChIPseeker)
library(rtracklayer)
library(dplyr)
library(tidyr)
library(ReactomePA)
library(ggplot2)
library(clusterProfiler)
library(DOSE)
library(TxDb.Hsapiens.UCSC.hg19.knownGene)
library(org.Hs.eg.db)
txdb <- TxDb.Hsapiens.UCSC.hg19.knownGene

## see https://github.com/GuangchuangYu/ChIPseeker/issues/55
## ChIPseeker annotation
setwd("/Volumes/Mahinur_2/2019.2.27_Enhancer_analysis/Denovo_enhancer_gain_during_development/Cancer_Unique_or_ESC_shared_Enhancer_EP_annotation/CCLE_DeNovo/")
bed<- import("BRCA_DeNovo.bed", format= "BED")
peakAnnoList <- annotatePeak(bed, tssRegion=c(-3000, 3000), 
                             TxDb=txdb, annoDb="org.Hs.eg.db", level = "gene", overlap = "TSS",
                             addFlankGeneInfo = TRUE,  flankDistance = 5000)
genes <- as.data.frame(peakAnnoList)$geneId
names(genes) = sub("_", "\n", names(genes))
## Functional enrichment analysis for E-P annotated De-Novo gain enhancers
setwd("/Volumes/Mahinur_2/2019.2.27_Enhancer_analysis/Denovo_enhancer_gain_during_development/Cancer_Unique_or_ESC_shared_Enhancer_EP_annotation/CCLE_DeNovo_EP_annotation/")
CancerType <- read.table(file = "BRCA_DeNovo_EP_annotation.tsv", sep = '\t', header = TRUE, stringsAsFactors = FALSE,
                         quote = "")
genes <- CancerType$geneID
names(genes) = sub("_", "\n", names(genes))
### GO analysis
GO <- enrichGO(gene         = genes,
               OrgDb         = org.Hs.eg.db,
               ont           = "ALL",
               pAdjustMethod = "BH",
               pvalueCutoff  = 0.01,
               qvalueCutoff  = 0.05,
               readable=TRUE)
pathway <- as.data.frame(GO)
write.table(x = pathway, file = "BRCA_DeNovo_gain_enhancer_GO_pathways.txt", quote = FALSE, sep = "\t", row.names = FALSE)
dotplot(GO, showCategory = 10, split="ONTOLOGY", font.size = 24, title = "GO Pathway Enrichment Analysis") + facet_grid(ONTOLOGY~., scale="free")
barplot(GO, showCategory = 10, split="ONTOLOGY",font.size = 24, title = "GO Pathway Enrichment Analysis") + facet_grid(ONTOLOGY~., scale="free")
### KEGG analysis
KEGG <- enrichKEGG(genes, organism = "hsa", keyType = "kegg", pvalueCutoff = 0.05,
                   pAdjustMethod = "BH", qvalueCutoff = 0.1, minGSSize = 10,
                   maxGSSize = 500)
pathway <- as.data.frame(KEGG)
write.table(x = pathway, file = "BRCA_DeNovo_gain_enhancer_KEGG_pathways.txt", quote = FALSE, sep = "\t", row.names = FALSE)
dotplot(KEGG, showCategory = 10, font.size = 24, title = "KEGG Pathway Enrichment Analysis")
barplot(KEGG, showCategory = 10, font.size = 24, title = "KEGG Pathway Enrichment Analysis")
### Hallmark pathway analysis
library(msigdbr)
Hallmark <- msigdbr(species = "Homo sapiens", category = "H") %>% 
  dplyr::select(gs_name, entrez_gene)
#Hallmark <- msigdbr(species = "Homo sapiens", category = "H") %>% 
# msigdbr(species = "Homo sapiens", category = "C7") %>% 
# dplyr::select(gs_name, entrez_gene) %>% 
# dplyr::bind_rows()
Hallmark <- enricher(gene = genes,
                     pAdjustMethod = "BH",
                     pvalueCutoff  = 0.05,
                     qvalueCutoff  = 0.1,
                     TERM2GENE = Hallmark)
pathway <- as.data.frame(Hallmark)
write.table(x = pathway, file = "BRCA_DeNovo_gain_enhancer_Hallmark_pathways.txt", quote = FALSE, sep = "\t", row.names = FALSE)
dotplot(Hallmark, showCategory = 10, font.size = 24,title = "Hallmark Pathway Enrichment Analysis")
### Wiki
gmtfile <- system.file("extdata/wikipathways-20180810-gmt-Homo_sapiens.gmt", package="clusterProfiler")
Wiki <- read.gmt(gmtfile)
#Wiki <- read.gmt("~/Downloads/wikipathways-20200110-gmt-Homo_sapiens.gmt.txt")
Wiki <- Wiki %>% tidyr::separate(ont, c("name","version","wpid","org"), "%")
Wiki2gene <- Wiki %>% dplyr::select(wpid, gene) #TERM2GENE
Wiki2name <- Wiki %>% dplyr::select(wpid, name) #TERM2NAME
Wiki <- enricher(gene = genes, 
                 pAdjustMethod = "BH",
                 pvalueCutoff  = 0.05,
                 qvalueCutoff  = 0.1,
                 TERM2GENE=Wiki2gene, 
                 TERM2NAME=Wiki2name)
pathway <- as.data.frame(Wiki)
write.table(x = pathway, file = "BRCA_DeNovo_gain_enhancer_WiKi_pathways.txt", quote = FALSE, sep = "\t", row.names = FALSE)
dotplot(Wiki, showCategory = 10, font.size = 24, title = "Wiki Pathway Enrichment Analysis")
barplot(Wiki, showCategory = 10, font.size = 24, title = "Wiki Pathway Enrichment Analysis")

#### Functional profiles comparison 
library(clusterProfiler)
library(DOSE)
setwd("/Volumes/Mahinur_2/2019.2.27_Enhancer_analysis/Denovo_enhancer_gain_during_development/Cancer_Unique_or_ESC_shared_Enhancer_EP_annotation/CCLE_DeNovo/")
files <- list.files()
print(files)
peakAnnoList <- annotatePeak(bed, tssRegion=c(-3000, 3000), 
                             TxDb=txdb, annoDb="org.Hs.eg.db", level = "gene", overlap = "TSS",
                             addFlankGeneInfo = TRUE,  flankDistance = 5000)
names(peakAnnoList) <- basename(files)
plotAnnoBar(peakAnnoList)
genes = lapply(peakAnnoList, function(i) as.data.frame(i)$geneId)
names(genes) = sub("_", "\n", names(genes))
### KEGG pathway comparision
compKEGG <- compareCluster(geneCluster   = genes,
                           fun           = "enrichKEGG", 
                           pvalueCutoff  = 0.05,
                           pAdjustMethod = "BH")
### fun = One of "groupGO", "enrichGO", "enrichKEGG", "enrichDO" or "enrichPathway" 
p1 <- dotplot(compKEGG, showCategory = 5, includeAll = FALSE, title = "KEGG Pathway Enrichment Analysis")
p1  + scale_color_gradientn(colours = rainbow(3))
### GO pathway comparision
compGO <- compareCluster(geneCluster   = genes,
                         fun           = "enrichGO",  OrgDb='org.Hs.eg.db',
                         pvalueCutoff  = 0.05,
                         pAdjustMethod = "BH")
dotplot(compGO, showCategory = 5,  includeAll = FALSE, title = "GO Pathway Enrichment Analysis")
### Hallmark pathway comparision
library(msigdbr)
Hallmark <- msigdbr(species = "Homo sapiens", category = "H") %>% 
  dplyr::select(gs_name, entrez_gene)
compHallmark <- compareCluster(geneCluster   = genes,
                               fun           = "enricher",
                               pvalueCutoff  = 0.05,
                               pAdjustMethod = "BH",
                               TERM2GENE = Hallmark)
dotplot(compHallmark, showCategory = 5,  includeAll = FALSE, title = "Hallmark Pathway Enrichment Analysis")
### Wiki pathway comparision
gmtfile <- system.file("extdata", "wikipathways-20180810-gmt-Homo_sapiens.gmt", package="clusterProfiler")
Wiki <- read.gmt(gmtfile)
compWiki <- compareCluster(geneCluster   = genes,
                           fun           = "enricher",
                           pvalueCutoff  = 0.05,
                           pAdjustMethod = "BH",
                           TERM2GENE = Wiki2gene,
                           TERM2NAME = Wiki2name)
dotplot(compWiki, showCategory = 5,  includeAll = FALSE, title = "Wiki Pathway Enrichment Analysis")
dotplot(compWiki, showCategory = 5, split= ".bed", includeAll = FALSE, title = "Wiki Pathway Enrichment Analysis") + facet_grid(.~.bed)

#### Functional profiles comparison for EP annotation
library(clusterProfiler)
library(DOSE)
setwd("/Volumes/Mahinur_2/2019.2.27_Enhancer_analysis/Denovo_enhancer_gain_during_development/Cancer_Unique_or_ESC_shared_Enhancer_EP_annotation/CCLE_DeNovo_EP_annotation/")
files <- list.files(pattern=".tsv")
print(files)
datalist <- lapply(files, function(x)read.table(x, header=T)) 
names(datalist) <- basename(files)
genes = lapply(datalist, function(i) as.data.frame(i)$geneID)
names(genes) = sub("_", "\n", names(genes))
### KEGG pathway comparision
compKEGG <- compareCluster(geneCluster   = genes,
                           fun           = "enrichKEGG", 
                           pvalueCutoff  = 0.05,
                           pAdjustMethod = "BH")
### fun = One of "groupGO", "enrichGO", "enrichKEGG", "enrichDO" or "enrichPathway" 
p1 <- dotplot(compKEGG, showCategory = 5, includeAll = FALSE, title = "KEGG Pathway Enrichment Analysis")
p1  + scale_color_gradientn(colours = rainbow(3))
### GO pathway comparision
compGO <- compareCluster(geneCluster   = genes,
                         fun           = "enrichGO",  OrgDb='org.Hs.eg.db',
                         pvalueCutoff  = 0.05,
                         pAdjustMethod = "BH")
dotplot(compGO, showCategory = 5,  includeAll = FALSE, title = "GO Pathway Enrichment Analysis")
### Hallmark pathway comparision
library(msigdbr)
Hallmark <- msigdbr(species = "Homo sapiens", category = "H") %>% 
  dplyr::select(gs_name, entrez_gene)
compHallmark <- compareCluster(geneCluster   = genes,
                               fun           = "enricher",
                               pvalueCutoff  = 0.05,
                               pAdjustMethod = "BH",
                               TERM2GENE = Hallmark)
dotplot(compHallmark, showCategory = 5,  includeAll = FALSE, title = "Hallmark Pathway Enrichment Analysis")
### Wiki pathway comparision
gmtfile <- system.file("extdata/wikipathways-20180810-gmt-Homo_sapiens.gmt", package="clusterProfiler")
Wiki <- read.gmt(gmtfile)
Wiki <- Wiki %>% tidyr::separate(ont, c("name","version","wpid","org"), "%")
Wiki2gene <- Wiki %>% dplyr::select(wpid, gene) #TERM2GENE
Wiki2name <- Wiki %>% dplyr::select(wpid, name) #TERM2NAME
compWiki <- compareCluster(geneCluster   = genes,
                           fun           = "enricher",
                           pvalueCutoff  = 0.05,
                           pAdjustMethod = "BH",
                           TERM2GENE = Wiki2gene,
                           TERM2NAME = Wiki2name)
dotplot(compWiki, showCategory = 5,  includeAll = FALSE, title = "Wiki Pathway Enrichment Analysis")